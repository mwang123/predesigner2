/*
 * Averysoft.java Created on Jun 13, 2005 by leeb
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery;

import java.net.URL;

/**
 * @author leeb
 *
 */
public class Averysoft
{
  private static String averysoftURI = "http://print.avery.com";

  /**
	 * @return Returns the averysoftUri.
	 */
	public static String getAverysoftURI()
	{
		return averysoftURI;
	}
	
	public static URL getSchemaURL()
	{
		return Averysoft.class.getResource("averysoft.xsd");
	}
  
  /**
   * Normally, AveryPanelField objects marks as print="false" or visible="false" 
   * are not included in PDF output files.  An Avery artist creating Predesigns
   * might, however, need to include those objects to show a proof to a client.
   * 
   * This boolean should not be toggled in a multi-threaded program, as it affects
   * all PDF output.  It is a global - use accordingly.
   */
  public static boolean includeAllPanelFieldsInPDF = false;
}
