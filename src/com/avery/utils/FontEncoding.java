/**
 * Title:        FontEncoding<p>
 * Description:  <p>
 * Copyright:    Copyright (c)2003 Avery Dennison<p>
 * Company:      Avery Dennison<p>
 * @author Brad Nelson
 * @version 1.0
 */

package com.avery.utils;
import java.util.*;
import java.io.*;

/**
 * Creates a hash table of font family - encoding pairs.
 */

public class FontEncoding
{
  static HashMap encodingMap = null;
  static HashMap javaFontMap = null;
  static HashMap fontFileMap = null;
  static HashMap minSizeMap = null;
  static ArrayList javaNames = null;

  // default to property file for Avery Print
  static String propertyFile = "AveryPrint.properties";
  // this is the file that contains the encodings
  static String encodingFile = null;
  
  private static double MIN_FONT_SIZE = 6.;	// established 4/2016, also in AveryTextfield

  /**
   * returns an encoding.
   * @param fontFamily - the font family name
   *
   * @return String font family encoding
  */
  public static String getEncoding(String fontFamily)
  {
    if (encodingMap == null)
    {
      createHash();
    }

    String encoding = (String)encodingMap.get(fontFamily);
//    System.out.println("fontfamily, encoding= " + fontFamily + ", " + encoding);
    if (encoding == null)
      encoding = "8859_1";
//    System.out.println("return encoding= " + encoding);

    return encoding;
  }

  public static double getMinFontSize(String fontFamily)
  {
    if (minSizeMap == null)
    {
      createHash();
    }

    double minimumSize = Double.parseDouble((String)minSizeMap.get(fontFamily));
//    System.out.println("fontfamily, encoding= " + fontFamily + ", " + encoding);
    if (minimumSize < 2.)
      minimumSize = 2.;
//    System.out.println("return encoding= " + encoding);

    return minimumSize;
  }

  /**
   * returns the java name, if blank, is same as full name.
   * @param fontFamily - the font family name
   *
   * @return String javafontname
  */
  public static String getJavaFontName(String fontFamily)
  {
    if (javaFontMap == null)
    {
      createHash();
    }

    String javaFontName = (String)javaFontMap.get(fontFamily);
//    System.out.println("fontfamily, encoding= " + fontFamily + ", " + encoding);
    if (javaFontName == null)
      javaFontName = "";
//    System.out.println("return encoding= " + encoding);

    return javaFontName;
  }

  /**
   * returns the font file name.
   * @param fontFamily - the font family name
   *
   * @return String fontfilename
  */
  public static String getFontFileName(String fontFamily)
  {
    if (fontFileMap == null)
    {
      createHash();
    }

    String fontFileName = (String)fontFileMap.get(fontFamily);
//    System.out.println("fontfamily, fontFileName= " + fontFamily + ", " + fontFileName);
    if (fontFileName == null)
      fontFileName = "";
//    System.out.println("return fontFileName= " + fontFileName);

    return fontFileName;
  }

  /**
   * sets the properties file name.
   * @param newPropertyFile - the properties file name
   *
   * sets the static propertyFile for any application
   * default is AveryPrint.properties
  */
  public static void setPropertyFile(String newPropertyFile)
  {
    propertyFile = newPropertyFile;
  }

  /**
   * Sets the encoding file.  If this is set, the properties file is ignored.
   * @param newEncodingFile
   */
  public static void setEncodingFile(String newEncodingFile)
  {
    encodingFile = newEncodingFile;
    
    // reset everything else
    encodingMap = null;
    javaFontMap = null;
    fontFileMap = null;
    javaNames = null;
  }

  /**
   * creates the hash map for font encodings except for 8859_1 which should
   * never be added here, since that is the default encoding.
  */
  private static void createHash()
  {

    // read the encoding file 'FontEncodings.txt'
    encodingMap = new HashMap();
    javaFontMap = new HashMap();
    fontFileMap = new HashMap();
    minSizeMap = new HashMap();
    // unsorted display names preserves order of FontEncodings file
    javaNames = new ArrayList();

    try
    {
      if (encodingFile == null)
      {
        java.util.Properties properties = new java.util.Properties();
        FileInputStream in = new FileInputStream(propertyFile);
        properties.load(in);
        in.close();
        encodingFile = properties.getProperty("fontencoding");
      }
      BufferedReader br = new BufferedReader(new FileReader(encodingFile));
      String line;

      // sanity check
      int lines = 0;
      // parse lines containing font entries
      while ((line = br.readLine()) != null && lines++ < 500)
      {
        line = line.trim();
        if (!line.startsWith("#") && line.length() > 3)
        {
          // parse fullname, javaname, and encoding
          StringTokenizer st = new StringTokenizer(line, ",");
          if (st.hasMoreTokens())
          {
            String familyName = st.nextToken();
            familyName = familyName.trim();
            if (st.hasMoreTokens())
            {
              String javaFontName = st.nextToken();
              javaFontName = javaFontName.trim();
              if (javaFontName.length() < 3)
                javaFontName = "TYPE1";
              if (st.hasMoreTokens())
              {
                String encoding = st.nextToken();
                encoding = encoding.trim();
                if (st.hasMoreTokens())
                {
                  String fontFileName = st.nextToken();
                  fontFileName = fontFileName.trim();
//                System.out.println("full name, java name, encoding, fileName= " + familyName + "|" + javaFontName + "|" + encoding + "|" + fontFileName + "|");
                  double minimumSize = MIN_FONT_SIZE;
                  if (st.hasMoreTokens())
                  {
                    String ms = st.nextToken();
                    ms = ms.trim();
                    minimumSize = Double.parseDouble(ms);                 	
                  }
                  // display list order currently used only by APE
                  javaNames.add(javaFontName);

                  // add entries to maps
                  javaFontMap.put(familyName, javaFontName);
                  //System.out.println(familyName + "," + javaFontName);
                  encodingMap.put(familyName, encoding);
                  fontFileMap.put(familyName, fontFileName);
                  minSizeMap.put(familyName, String.valueOf(minimumSize));
               }
              }
            }
          }
          st = null;
        }
      }

    }
    catch (IOException ioe)
    {
      System.err.println(ioe);
    }

  }

  /**
   * Constructs a list of the font families named in the FontEncodings
   * file, ignoring the TYPE1 files.
   * @return a list without duplicates
   */
  public static List getTTFFamilies()
  {
    if (javaFontMap == null)
    {
      createHash();
    }

    ArrayList source = new ArrayList(javaNames);
    ArrayList target = new ArrayList();

    Iterator iterator = source.iterator();
    while (iterator.hasNext())
    {
      String font = (String)iterator.next();
      if (target.contains(font) || font.equalsIgnoreCase("TYPE1"))
      {
        continue;
      }

      target.add(font);
    }
    return target;
  }
  
  public static List getJavaFontStyles(String familyName)
  {
  	Set keySet = javaFontMap.keySet();
  	Iterator fontNames = keySet.iterator();
  	
  	List styles = new ArrayList();
  	Collection values = javaFontMap.values();
  	Iterator iter = values.iterator();
  	int i = 0;
  	while (iter.hasNext() && fontNames.hasNext())
  	{
  		String family = (String)iter.next();
  		String fontName = (String)fontNames.next();
  		if (family.equals(familyName))
  		{
  			if (!(family.equals(fontName)))
  			{
  				// last check is the file name the same?
  				String familyFontFile = getFontFileName(family);
  				String familyStyleFontFile = getFontFileName(fontName);
          //System.out.println(familyFontFile + "," + familyStyleFontFile);
          if (!(familyFontFile.equals(familyStyleFontFile))) 				
          {
          	styles.add(fontName);
          }
  				//System.out.println(fontName);
  			}
  		}
  	}
  	return styles;
  }

}

