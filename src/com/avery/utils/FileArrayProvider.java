package com.avery.utils;

import java.io.BufferedReader; 
import java.io.FileReader; 
import java.io.IOException; 
import java.util.ArrayList; 
import java.util.List; 
 
public class FileArrayProvider
{ 
 
	public ArrayList readLines(String filename) throws IOException
	{ 
		FileReader fileReader = new FileReader(filename); 
		BufferedReader bufferedReader = new BufferedReader(fileReader); 
		ArrayList lines = new ArrayList(); 
		String line = null; 
		while ((line = bufferedReader.readLine()) != null)
		{ 
		    lines.add(line); 
		} 
		bufferedReader.close();
		
		return lines; 
	} 
} 
