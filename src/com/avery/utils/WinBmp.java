/**
 * Title:        WinBmp<p>
 * Description:  Reads a Windows or OS2 BMP image file<p>
 * Copyright:    Copyright (c)2001 Avery Dennison Office Products<p>
 * Company:      Avery Dennison<p>
 * Credit:       This code is based on an example given in the book<br>
 *               "Developing Professional Java Applets"<br>
 *               by  K.C. Hopson and Stephen E. Ingram </p>
 */

package com.avery.utils;

import java.io.*;
import java.awt.image.*;
import java.awt.Color;
import com.sun.image.codec.jpeg.ImageFormatException;

public class WinBmp
{
  private String bmpFileName;
  private boolean imageProcessed;
  private boolean windowsStyle;
  private ColorModel colorModel = ColorModel.getRGBdefault();
  private int pix[];

  private byte bmpFileType[];
  private int bmpFileSize;
  private int bmpFileOffset;
  private int biSize;
  private int biWidth;
  private int biHeight;
  private int biPlanes;
  private int biBitCount;
  private int biCompression;
  private int biSizeImage;
  private int biXPelsPerMeter;
  private int biYPelsPerMeter;
  private int biClrUsed;
  private int biClrImportant;

  // Constants indicating how the data is stored
  private static final int BI_RGB = 0;
  private static final int BI_RLE8 = 1;
  private static final int BI_RLE4 = 2;

  public WinBmp(String name)
  {
    bmpFileName = name;
    bmpFileType = new byte[2];
    imageProcessed = false;
  }

  /**
   * Retrieves the width stored in the Windows BITMAPINFOHEADER
   * @return width of the bitmap in pixels
   */
  public int getWidth()
  {
    return biWidth;
  }

  /**
   * Retrieves the height stored in the Windows BITMAPINFOHEADER
   * @return height of the bitmap in pixels
   */
  public int getHeight()
  {
    return biHeight;
  }

  /**
  * A private method for extracting little endian
  * quantities from a input stream.
  * @param is contains the input stream
  * @param len is the number of bytes in the quantity
  * @returns the result as an integer
  */
  private int pullVal(DataInputStream is, int len)
  throws IOException
  {
    int value = 0;
    int temp = 0;

    for ( int x = 0; x < len; x++ )
    {
      temp = is.readUnsignedByte();
      value += (temp << (x * 8));
    }
    return value;
  }

  /**
  * A private method for extracting the file header
  * portion of a BMP file.
  * @param is contains the input stream
  */
  private void extractFileHeader(DataInputStream is)
  throws IOException, ImageFormatException
  {
    is.read(bmpFileType);
    if ( bmpFileType[0] != 'B' || bmpFileType[1] != 'M' )
    {
      throw new ImageFormatException("Not BMP format");
    }
    bmpFileSize = pullVal(is, 4);
    is.skipBytes(4);
    bmpFileOffset = pullVal(is, 4);
  }

  /**
  * A private method for extracting the color table from
  * a BMP type file.
  * @param is contains the input stream
  * @param numColors contains the biClrUsed (for Windows) or zero
  */
  private void extractColorMap(DataInputStream is, int numColors)
  throws IOException, ImageFormatException
  {
    byte blues[], reds[], greens[];

    // if passed count is zero, then determine the
    // number of entries from bits per pixel.
    if ( numColors == 0 )
    {
      switch ( biBitCount )
      {
      case 1: numColors = 2;
        throw new ImageFormatException("Bilevel BMP files are not supported.");

      case 4: numColors = 16;
        break;

      case 8: numColors = 256;
        break;

      case 0:   // common error in file is biBitCount = 0
      case 24: numColors = 0;
        break;

      default:
        numColors = -1;
        throw new ImageFormatException("BMP file has invalid bits per pixel: " + biBitCount);
      }
    }
    if ( numColors == 0 )
    {
      colorModel = DirectColorModel.getRGBdefault();
    }
    else
    {
      reds = new byte[numColors];
      blues = new byte[numColors];
      greens = new byte[numColors];
      for ( int x = 0; x < numColors; x++ )
      {
        blues[x] = is.readByte();
        greens[x] = is.readByte();
        reds[x] = is.readByte();
        if ( windowsStyle )
        {
          is.skipBytes(1);
        }
      }

      if (biBitCount == 24)   // odd case of truecolor with colormap
      {
        colorModel = DirectColorModel.getRGBdefault();
      }
      else  // normal colormapped image
      {
        colorModel = new IndexColorModel( biBitCount, numColors, reds, greens, blues );
      }
    }
  }

  /**
  * A private method for extracting an OS/2 style
  * bitmap header.
  * @param is contains the input stream
  */
  private void extractOS2Style(DataInputStream is)
  throws IOException, ImageFormatException
  {
    windowsStyle = false;
    biWidth = pullVal(is, 2);
    biHeight = pullVal(is, 2);
    biPlanes = pullVal(is, 2);
    biBitCount = pullVal(is, 2);
    if (biBitCount == 1)
      throw new ImageFormatException("1-bit BMP not supported");
    extractColorMap(is, 0);
  }

  /**
  * A private method for extracting a Windows style
  * bitmap header.
  * @param is contains the input stream
  */
  private void extractWindowsStyle(DataInputStream is)
  throws IOException, ImageFormatException
  {
    windowsStyle = true;
    biWidth = pullVal(is, 4);
    biHeight = pullVal(is, 4);
    biPlanes = pullVal(is, 2);
    biBitCount = pullVal(is, 2);
    if (biBitCount == 1)
      throw new ImageFormatException("1-bit BMP not supported");
    biCompression = pullVal(is, 4);
    biSizeImage = pullVal(is, 4);
    biXPelsPerMeter = pullVal(is, 4);
    biYPelsPerMeter = pullVal(is, 4);
    biClrUsed = pullVal(is, 4);
    biClrImportant = pullVal(is, 4);
    extractColorMap(is, biClrUsed);
  }

  /**
  * A private method for extracting the bitmap header.
  * This method determines the header type (OS/2 or Windows)
  * and calls the appropriate routine.
  * @param is contains the input stream
  */
  private void extractBitmapHeader(DataInputStream is)
  throws IOException, ImageFormatException
  {
    biSize = pullVal(is, 4);
    if ( biSize == 12 )
    {
      extractOS2Style(is);
    }
    else
    {
      extractWindowsStyle(is);
    }
  }

  /**
  * A private method for extracting 4 bit per pixel
  * image data.
  * @param is contains the input stream
  */
  private void extract4BitData( DataInputStream is )
  throws IOException
  {
    pix = new int[biHeight * biWidth];

    if (biCompression == 0)
    {
      int padding = 0;
      int overage = ((biWidth + 1)/ 2) % 4;
      if (overage != 0)
      {
        padding = 4 - overage;
      }

      int temp = 0;
      for (int y = biHeight - 1; y >= 0; y--)
      {
        int index = y * biWidth;
        for (int x = 0; x < biWidth; x++)
        {
          // if on an even byte, read new 8 bit quantity
          // use low nibble of previous read for odd bytes
          if ((x % 2) == 0)
          {
            temp = is.readUnsignedByte();
            pix[index++] = temp >> 4;
          }
          else
          {
            pix[index++] = temp & 0x0f;
          }
        }

        if (padding != 0)
        {
          is.skipBytes(padding);
        }
      }
    }
    else
    {
      extract4BitRLE(is);
    }
  }

  /**
  * A private method for extracting 8 bit per pixel RLE image data.
  * @param is contains the input stream
  */
  private void extract4BitRLE(DataInputStream is)
    throws IOException
  {
    int x = 0, bytesRead = 0;
    int lineIndex = biWidth * (biHeight - 1);

    // read all bytes in the image
    while (bytesRead < biSizeImage)
    {
      if (x > biWidth || lineIndex < 0)   // safety
      {
        System.out.println("Premature end of 8bitRLE data?");
        return;
      }

      // RLE encoding is defined by two bytes
      int byte1 = is.readUnsignedByte();
      int byte2 = is.readUnsignedByte();
      bytesRead += 2;
      // if byte1 is 0, this is an escape code
      if (byte1 == 0)
      {
        switch (byte2)
        {
        case 0:   // end of line
          x = 0;
          lineIndex -= biWidth;
          break;

        case 1:   // end of bitmap
          return;

        case 2:   // offset
          x += is.readUnsignedByte();
          lineIndex -= is.readUnsignedByte() * biWidth;
          bytesRead += 2;
          break;

        default:  // uncompressed data follows
          int current = 0;
          for (int count = 0; count < byte2; count++)
          {
            if ((count & 1) == 0)
            {
              current = is.readUnsignedByte();
              ++bytesRead;
              pix[lineIndex + x++] = (current >> 4) & 0x0f;
            }
            else pix[lineIndex + x++] = current & 0x0f;
          }

          if ((byte2 & 3) == 1 || ((byte2 & 3) == 2))  // we must read an even number of bytes
          {
            is.readUnsignedByte();    // throw away excess
            ++bytesRead;
          }
          break;
        }
      }
      else for (int count = 0; count < byte1; count++)  // run length
      {
        pix[lineIndex + x++] = ((count & 1) == 0) ? ((byte2 >> 4) & 0x0f) : (byte2 & 0x0f);
      }
    }
  }

  /**
  * A private method for extracting 8 bit per pixel
  * image data.
  * @param is contains the input stream
  */
  private void extract8BitData( DataInputStream is )
    throws IOException
  {
    pix = new int[biHeight * biWidth];
    if (biCompression == 0)
    {
      int index;
      int padding = 0;
      int overage = biWidth % 4;
      if (overage != 0)
      {
        padding = 4 - overage;
      }

      for (int y = biHeight - 1; y >= 0; y--)
      {
        index = y * biWidth;
        for (int x = 0; x < biWidth; x++)
        {
          pix[index++] = is.readUnsignedByte();
        }

        if (padding != 0)
        {
          is.skipBytes(padding);
        }
      }
    }
    else  // RLE compressed
    {
      extract8BitRLE(is);
    }
  }

  /**
  * A private method for extracting 8 bit per pixel RLE image data.
  * @param is contains the input stream
  */
  private void extract8BitRLE(DataInputStream is)
    throws IOException
  {
    int x = 0, bytesRead = 0;
    int lineIndex = biWidth * (biHeight - 1);

    // read all bytes in the image
    while (bytesRead < biSizeImage)
    {
      if (x > biWidth || lineIndex < 0)   // safety
      {
        System.out.println("Premature end of 8bitRLE data?");
        return;
      }

      // RLE encoding is defined by two bytes
      int byte1 = is.readUnsignedByte();
      int byte2 = is.readUnsignedByte();
      bytesRead += 2;
      // if byte1 is 0, this is an escape code
      if (byte1 == 0)
      {
        switch (byte2)
        {
        case 0:   // end of line
          x = 0;
          lineIndex -= biWidth;
          break;

        case 1:   // end of bitmap
          return;

        case 2:   // offset
          x += is.readUnsignedByte();
          lineIndex -= is.readUnsignedByte() * biWidth;
          bytesRead += 2;
          break;

        default:  // uncompressed data follows
          for (int count = 0; count < byte2; count++)
          {
            pix[lineIndex + x++] = is.readUnsignedByte();
            ++bytesRead;
          }

          if ((byte2 & 1) != 0)  // we must read an even number of bytes
          {
            is.readUnsignedByte();    // throw it away
            ++bytesRead;
          }
          break;
        }
      }
      else for (int count = 0; count < byte1; count++)  // run length
      {
        pix[lineIndex + x++] = byte2;
      }
    }
  }

  /**
  * A private method for extracting 24 bit per pixel
  * image data.
  * @param is contains the input stream
  */
  private void extract24BitData( DataInputStream is )
  throws IOException
  {
    if (biCompression == 0)
    {
      int padding = 0;
      int overage = (biWidth * 3) % 4;  // 3 bytes/pixel, DWORD aligned
      if (overage != 0)
      {
        padding = 4 - overage;
      }

      pix = new int[biHeight * biWidth];

      int red;
      int green;
      int blue;
      int index;

      for (int y = biHeight - 1; y >= 0; y--)
      {
        index = y * biWidth;    // otherwise, it would be upside down!

        for (int x = 0; x < biWidth; x++)
        {
          blue  = is.readUnsignedByte();
          green = is.readUnsignedByte();
          red   = is.readUnsignedByte();

          // use Color class to convert it to RGBDefault color model
          pix[index++] = new Color(red, green, blue).getRGB();
        }

        if (padding != 0)
        {
          is.skipBytes(padding);
        }
      }
    }
    else
    {
      throw new IOException("Compressed images not supported");
    }
  }

  /**
  * A private method for extracting the image data from
  * a input stream.
  * @param is contains the input stream
  */
  private void extractImageData( DataInputStream is )
  throws IOException, ImageFormatException
  {
    switch ( biBitCount )
    {
      case 1:
        throw new ImageFormatException("Bilevel BMP files are not supported.");
      case 4:
        extract4BitData(is);
        break;
      case 8:
        extract8BitData(is);
        break;
      case 24:
      case 0:
        extract24BitData(is);
        break;
      default:
        throw new ImageFormatException("BMP file has invalid bits per pixel: " + biBitCount);
    }
  }

  /**
  * Given an input stream, create an ImageProducer from
  * the BMP info contained in the stream.
  * @param is contains the input stream to use
  * @return the ImageProducer
  */
  public ImageProducer extractImage( DataInputStream is )
    throws IOException, ImageFormatException
  {
    MemoryImageSource img = null;
    extractFileHeader(is);
    extractBitmapHeader(is);
    extractImageData(is);
    img = new MemoryImageSource(biWidth, biHeight, colorModel, pix, 0, biWidth);
    imageProcessed = true;
    return img;
  }

  /**
  * Describe the image as a string
  */
  public String toString()
  {
    StringBuffer buf = new StringBuffer("");
    if (imageProcessed)
    {
      buf.append(" name: " + bmpFileName + "\n");
      buf.append(" size: " + bmpFileSize + "\n");
      buf.append(" img offset: " + bmpFileOffset + "\n");
      buf.append("header size: " + biSize + "\n");
      buf.append(" width: " + biWidth + "\n");
      buf.append(" height: " + biHeight + "\n");
      buf.append(" clr planes: " + biPlanes + "\n");
      buf.append(" bits/pixel: " + biBitCount + "\n");

      if ( windowsStyle )
      {
        buf.append("compression: " + biCompression + "\n");
        buf.append(" image size: " + biSizeImage + "\n");
        buf.append("Xpels/meter: " + biXPelsPerMeter + "\n");
        buf.append("Ypels/meter: " + biYPelsPerMeter + "\n");
        buf.append("colors used: " + biClrUsed + "\n");
        buf.append("primary clr: " + biClrImportant + "\n");
      }
    }
    else
    {
      buf.append("Image not read yet.");
    }

    return buf.toString();
  }

  /**
  * Decodes a BMP file
  * @param name contains the filename
  * @return an ImageProducer containing the deoded image
  * @throws IOException or ImageFormatException
  */
  public static ImageProducer getImageProducer(String name)
    throws IOException, ImageFormatException
  {
    ImageProducer producer = null;
    InputStream is = null;
    try
    {
      WinBmp winbmp = new WinBmp(name);
      is = new FileInputStream(name);
      DataInputStream input = new DataInputStream(is);
      producer = winbmp.extractImage(input);
      is.close();
    }
    catch (IOException e)
    {
      if (is != null)
      {
        is.close();
      }
      throw e;
    }
    catch (ImageFormatException e)
    {
      if (is != null)
      {
        is.close();
      }
      throw e;
    }
    return producer;
  }

  /**
   * This method decodes a BMP file and then throws away the result.  If no
   * exception was thrown, the BMP can be considered to be valid.
   * @param filename Fully qualified path to the BMP file
   * @return nothing
   * @throws IOException if there was a problem reading the file
   * @throws ImageFormatException if the file was not a BMP file that we can
   *          decode.  Note that currently 1 bit files will throw this
   *          exception.
   */
  public static void validateBMP(String filename)
    throws IOException, ImageFormatException
  {
    ImageProducer p = getImageProducer(filename);
    p = null;   // discard decoded image
  }

  /**
   * This method returns a buffer of 32 bit pixels formatted in the default RGB color model
   */
  public int[] getIntPixels()
  {
    if (imageProcessed == false)
    {
      DataInputStream input = null;
      try
      {
        input = new DataInputStream(new FileInputStream(bmpFileName));
        extractImage(input);
        input.close();
      }
      catch (Exception e)
      {
        if (input != null)
        {
          try { input.close();      }
          catch (IOException e2)  { }
        }
        System.err.println(AveryUtils.timeStamp() + e.toString());
        return null;
      }
    }

    // expand indexed color model buffer if necessary
    if (colorModel instanceof IndexColorModel)
    {
      int colors[] = new int[256];
      ((IndexColorModel)colorModel).getRGBs(colors);

      int nPixels = biWidth * biHeight;
      for (int i = 0; i < nPixels; ++i)
      {
        pix[i] = colors[pix[i]];
      }
      // set colorModel to match the new data format
      colorModel = ColorModel.getRGBdefault();
    }

    return pix;
  }
}
// end WinBmp class