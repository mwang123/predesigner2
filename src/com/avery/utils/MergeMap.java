/*
 * MergeMap.java Created on May 25, 2005 by Bob Lee
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

/**
 * Java representation of the mergeMap XML element,
 * used to define mail merge protocols for applications
 */
public class MergeMap
{
	private String name;
	private ArrayList lines = new ArrayList();

	public MergeMap(Element element)
	{
		name = element.getAttributeValue("name");
		Iterator iterator = element.getChildren("line", getNamespace()).iterator();
		while (iterator.hasNext())
		{
			lines.add(new Line((Element)iterator.next()));
		}
	}
	
	public String getName()
	{
		return name;
	}
	
	private List getLines()
	{
		return lines;
	}
	
	public List getLines(String language)
	{
		ArrayList list = new ArrayList();
		Iterator i = getLines().iterator();
		while (i.hasNext())
		{
			Line line = (Line)i.next();
			if (line.getLanguage().equals(language))
			{
				list.add(line);
			}
		}
		return list;
	}
	
	public boolean supportsLanguage(String language)
	{
		Iterator i = getLines().iterator();
		while (i.hasNext())
		{
			if (language.equals(((Line)i.next()).getLanguage()))
			{
					return true;
			}
		}
		return false;
	}
	
  private static String averysoftUri = "http://print.avery.com";
  private static Namespace averysoftNamespace = null;
  
  /**
   * merge uses the "http://print.avery.com avery" namespace
   * @return our JDOM Namespace 
   */
  public static Namespace getNamespace()
  {
    if (averysoftNamespace == null)
    {
      averysoftNamespace = Namespace.getNamespace("avery", averysoftUri);
    }
    return averysoftNamespace;
  }
	
	/**
	 * Constructs an English language description string suitable for
	 * selecting this MergeMap from a list
	 * @return a resonably unique description string
	 */
	public String toString()
	{
		String linestr;
		if (getLines().size() == 1)
		{
			linestr = " (1 line)";
		}
		else
		{
			linestr = " (" + getLines().size() + " lines)";
		}
		
		return getName() + linestr;  
	}
	
	/**
	 * This method constructs a Hashtable of required lines in the target language.
	 * The keys in the table are line names ("AF0", "AF1", etc.), and the objects
	 * in the table contain MergeMap.Line objects.
	 * @param language
	 * @return
	 */
	public Hashtable getRequiredLines(String language)
	{
		Hashtable hash = new Hashtable();
		Iterator i = getLines().iterator();
		while (i.hasNext())
		{
			Line line = (Line)i.next();
			if (line.getLanguage().equals(language) && line.isRequired())
			{
				hash.put(line.getName(), line);
			}
		}
		return hash;		
	}
	
	public List getBarcodeLines(String language)
	{
		List list = new ArrayList();
		Iterator i = getLines().iterator();
		while (i.hasNext())
		{
			Line line = (Line)i.next();
			if (line.allowInBarcode() && line.getLanguage().equals(language))
			{
				list.add(line);
			}
		}

		return list;
	}
	
	public class Line
	{
		private String name;				// "AF0", "AF1", "AF2" etc.
		private String language;		// "en", "fr", etc.
		private boolean required;
		private boolean barcode;
		private String text;				// display text
		private ArrayList mappings = new ArrayList();
		
		Line(Element element)
		{
			name = element.getAttributeValue("name");
			language = element.getAttributeValue("language");
			required = element.getAttributeValue("required").equals("true");
			if (element.getAttribute("allowInBarcode") != null)
			{
				barcode = element.getAttributeValue("allowInBarcode").equals("true");
			}
			text = element.getChildText("text", getNamespace());
			Iterator iterator = element.getChildren("mapping", getNamespace()).iterator();
			while (iterator.hasNext())
			{
				mappings.add(new Mapping((Element)iterator.next()));
			}
		}
		
		public String getName()
		{
			return name;
		}
		
		public String getLanguage()
		{
			return language;
		}
		
		public boolean isRequired()
		{
			return required;
		}
		
		public boolean allowInBarcode()
		{
			return barcode;
		}
		
		public String getText()
		{
			return text;
		}
		
		public String toString()
		{
			return getName() + (isRequired() ? " * " : "   ") + getText();
		}
		
		private ArrayList getMappings()
		{
			return mappings;
		}		
		
		/**
		 * @param locale "en-US" for example
		 * @param includeDefaults if <code>true</code>, the mappings that
		 * are not locale-specific will be included
		 * @return a list of datafield-to-textline Mapping objects
		 */
		public List getMappings(String locale, boolean includeDefaults)
		{
			ArrayList list = new ArrayList();
			Iterator i = getMappings().iterator();
			while (i.hasNext())
			{
				Mapping map = (Mapping)i.next();
				if (map.getLocale().equals(locale) && locale != null)
				{
					list.add(map);
				}
				else if (includeDefaults && map.getLocale() == null)
				{
					list.add(map);
				}
			}
			return mappings;
		}
	}
	
	public class Mapping
	{
		private String locale;
		private String content;
		
		Mapping(Element element)
		{
			locale = element.getAttributeValue("locale");
			content = element.getText();
		}
		
		public String getLocale()
		{
			return locale;
		}
		
		public String getContent()
		{
			return content;
		}
	}
}
