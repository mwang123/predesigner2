/**
 * Title:        AveryPrint<p>
 * Description:  The Print From The Web solution for printing on Avery products.<p>
 * Copyright:    Copyright (c) Wynn Bailey, Brad Nelson, Bob Lee<p>
 * Company:      Avery Dennison Corp.<p>
 * @author Wynn Bailey, Brad Nelson, Bob Lee
 * @version 1.0
 */
package com.avery.utils;

import java.util.*;
import java.awt.Image;
import java.io.*;
import com.sun.image.codec.jpeg.*;

public class GallerySorter
{
  private String imageBrowserRoot;

  public GallerySorter(String baseDir)
  {
    imageBrowserRoot = baseDir.trim();
    if (imageBrowserRoot.endsWith("/") == false)
    {
      imageBrowserRoot += "/";
    }
  }

  public void sortem()
  {
    // read gallery list index.txt
    ArrayList galleries = new ArrayList(100);
    try
    {
      String filename = imageBrowserRoot + "index.txt";
      LineNumberReader in = new LineNumberReader(new FileReader(filename));
      String line = in.readLine();
      while (line != null)
      {
        galleries.add(line.substring(line.indexOf(',')));
      }
    }
    catch (Exception e)
    {
      System.err.println(e.toString());
      if (galleries.size() < 1)
        return;
    }

    Iterator i = galleries.iterator();
    // for each gallery
    while (i.hasNext())
    {
      ArrayList images = new ArrayList(50);
      String galleryPath = imageBrowserRoot + i.next();
      try
      {
        // open index.txt
        LineNumberReader in = new LineNumberReader(new FileReader(galleryPath + "/index.txt"));
        String line = in.readLine();
        String firstLine = line;
        // for each image listed
        while (line != null)
        {
          int comma = line.indexOf(',');
          if (comma > -1)   // old format included comma separated fields
          {
            line = line.substring(0, comma);
          }
          // add image to the array
          images.add(new ImageAspect(galleryPath, line.trim()));
        }
      }
      catch (Exception e)
      {
        System.err.println(e.toString());
        if (images.size() < 2)
          continue;
      }

      // sort the images array

      // output the resulting order as sorted.txt

    }
  } // end of sortem()

  /**
   * This neat little package holds a gallery image spec and the aspect of
   * its thumbnail.
   */
  private class ImageAspect
  {
    ImageAspect(String path, String name)
    {
      galleryPath = path;
      imageName = name;
    }

    private String galleryPath;
    private String imageName;
    private double aspect = 0.0;

    public String getImageName()   { return imageName; }

    public double getAspect()
    { try
      {
        if (aspect < 0.00001)
        {
          FileInputStream stream = new FileInputStream(getThumbFilespec());
          Image img = JPEGCodec.createJPEGDecoder(stream).decodeAsBufferedImage();
          aspect = (double)(img.getWidth(null)) / (double)(img.getHeight(null));
        }
      }
      catch (Exception e)
      {
        System.err.println("GallerySorter problem with thumbnail " + getThumbFilespec());
        System.err.println(e.toString());
      }
      return aspect;
    }

    private String getThumbFilespec()
    {
      String filespec = galleryPath + "/Thumbnails/" + imageName;
      return (filespec.toLowerCase().endsWith(".jpg")) ? filespec : filespec + ".jpg";
    }
  } // end of ImageAspect internal class
}