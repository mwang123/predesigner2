/**
 * Title:        DiscoverType1Fonts<p>
 * Description:  <p>
 * Copyright:    Copyright (c)2002 Avery Dennison<p>
 * Company:      Avery Dennison<p>
 * This class searches for all Type1 fonts and creates
 * an output file that contains font information starting with
 * full font name, ending with font file name
 * @author Brad Nelson
 * @version 1.0
 */

package com.avery.utils;
import java.io.*;
import java.util.*;

/* usage
 * creates a list of Type1 full font name - filename pairs
 * and places the sorted list into the output file Type1Fonts.txt
 * located in the same folder as type1Dir.
 * the list can be used to look up a font file from the
 * Type1 fullname of the font.
 * Searches all .afm files in the type1Dir to find the font fullname and
 * other relevant font information
 */

public class DiscoverType1Fonts
{
  public String lineSeparator = "\r\n";
//  public String lineSeparator = System.getProperty("line.separator", "\r\n");

  // make a collection of strings to sort
  private ArrayList fileList = new ArrayList();

  private String type1Dir = "c:/winnt/fonts";

  /**
   * constructor.
  */
  public DiscoverType1Fonts()
  {
    // searches default type1Dir
    SearchFonts();
  }

  /**
   * constructor.
   * @param newType1Dir - Directory to search
  */
  public DiscoverType1Fonts(String newType1Dir)
  {
    // searches type1Dir passed into constructor
    type1Dir = newType1Dir;
    SearchFonts();
  }

  /**
   * search for all type1 fonts in type1Dir.
  */
  private void SearchFonts()
  {
    // set up directory search
    File readDir = new File(type1Dir);
    String[] type1Files = readDir.list();
    int fileCount = 0;

    while (fileCount < type1Files.length)
    {
      String sType1FileName = "Empty";
//      System.out.println(type1Files[fileCount]);
      File fileName = new File(type1Files[fileCount]);
      String sType1FileNameOnly = fileName.getName();
      sType1FileName = sType1FileNameOnly;

      fileName = null;

      if (sType1FileName.endsWith(".afm") == true)
      {
        String sFullName = readType1File(sType1FileName);

        if (sFullName != null)
        {
          // store the full name with the file name
          String nameValuePair = sFullName + '\t' + sType1FileNameOnly;
          fileList.add((Object)nameValuePair);
        }
      }
      fileCount++;
    }


    // sort the lines by full font name
    Collections.sort(fileList);

    try
    {
      // write out the results
      String outFile = type1Dir + "/Type1Fonts.txt";
      BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
      Iterator i = fileList.iterator();
      while (i.hasNext())
      {
        String entry = (String)(i.next()) + lineSeparator;
        bw.write(entry);
      }
      bw.close();
    }
    catch (IOException ioe) {}

    // now create the styles list
    // NOTE: the purpose of the created file is to
    // allow mapping between font styles and full font names.
    // this would be used by a GUI that displays only the font family
    // and enables appropriate bold, italic controls depending on
    // the capabilities of the members of the font family.
    // another way to deal with this is to use the FONT_BOLD and
    // FONT_ITALIC flags when creating a PLAIN font.

    // disabled because we don't use this UI currently
    //createStylesList();

  }


  String readType1File(String sType1FileName)
  {
    String fullNameEtc = null;

    try
    {
      String fileName = type1Dir + '/' + sType1FileName;
      BufferedReader br = new BufferedReader(new FileReader(fileName));
      String line = "";
      while ((line = br.readLine()) != null)
      {
        if (line.startsWith("FullName"))
        {
          fullNameEtc = line.substring(9);
//          System.out.println(line);
        }
        else if (line.startsWith("FamilyName"))
        {
          fullNameEtc += '\t' + line.substring(11);
//          System.out.println(line);
        }
        else if (line.startsWith("Weight"))
        {
          String weight = line.substring(7);
          String bold = " ";
          weight = weight.toLowerCase();
          if (weight.indexOf("bold") >= 0 ||
              weight.indexOf("demi") >= 0 ||
              weight.indexOf("black") >= 0 ||
              weight.indexOf("heavy") >= 0)
            bold = "bold";

          fullNameEtc += '\t' + bold;
//          System.out.println(line);
        }
        else if (line.startsWith("ItalicAngle"))
        {
          String italicAngle = line.substring(12);
          String italic = " ";
          float f = Float.parseFloat(italicAngle);
          if (f != 0.0)
            italic = "italic";
          fullNameEtc += '\t' + italic;
//          System.out.println(line);
          break;
        }
      }
      br.close();
    }
    catch (IOException ioe) {}

    return fullNameEtc;
  }

  void createStylesList()
  {
    try
    {
      // write out the results
      String outFile = type1Dir + "/Type1FontStyles.txt";
      BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
      Iterator i = fileList.iterator();

      String currentFamily = "";
      String currentNone = "";
      String currentBold = "";
      String currentItalic = "";
      String currentBoldItalic = "";

      String oldNone = "";
      String oldBold = "";
      String oldItalic = "";
      String oldBoldItalic = "";

      String currentCombo = "";

      boolean clearCurrent = false;

      while (i.hasNext())
      {
        String entry = (String)(i.next());
        StringTokenizer st = new StringTokenizer(entry, "\t");
        int count = 0;

        String fullname = "";
        String familyName = "";
        String bold = "";
        String italic = "";
        String fileName = "";
        String junk = "";

        while (st.hasMoreTokens())
        {
          switch (count)
          {
            case 0: fullname = st.nextToken(); break;
            case 1: familyName = st.nextToken(); break;
            case 2: bold = st.nextToken(); break;
            case 3: italic = st.nextToken(); break;
            case 4: fileName = st.nextToken(); break;
            default: junk = st.nextToken();
          }
          count++;
        }

//        System.out.println("Entry = " + fullname + "|" + familyName + "|" + bold + "|" + italic + "|" + fileName);

        // reset for next font family information
        currentNone = "";
        currentBold = "";
        currentItalic = "";
        currentBoldItalic = "";

        // figure out which style current entry belongs to
        if (bold.length() < 2)
        {
          if (italic.length() < 2)
          {
            // none
            currentNone = fullname;
          }
          else
          {
            // italic
            currentItalic = fullname;
          }
        }
        else
        {
          if (italic.length() < 2)
          {
            // bold
            currentBold = fullname;
          }
          else
          {
            // bold and italic
            currentBoldItalic = fullname;
          }
        }

        if (currentFamily.length() <= 0)
          currentFamily = familyName;
        if (!familyName.equals(currentFamily))
        {
          String result = currentFamily + '\t' + currentCombo;

          bw.write(result + lineSeparator);
          currentFamily = familyName;

          oldNone = "";
          oldBold = "";
          oldItalic = "";
          oldBoldItalic = "";
        }

        if (currentNone.length() > 0)
          oldNone = currentNone;
        if (currentBold.length() > 0)
          oldBold = currentBold;
        if (currentItalic.length() > 0)
          oldItalic = currentItalic;
        if (currentBoldItalic.length() > 0)
          oldBoldItalic = currentBoldItalic;

        currentCombo =  oldNone + '\t' +
                        oldBold + '\t' +
                        oldItalic + '\t' +
                        oldBoldItalic;

//        bw.write(familyName + lineSeparator);
      }
      String result = currentFamily + '\t' + currentCombo;
      bw.write(result + lineSeparator);
      bw.close();
    }
    catch (IOException ioe) {}
  }


} // end of DiscoverType1Fonts class


