/**
 * Title:        AveryPolygon<p>
 * Description:  <p>
 * Copyright:    Copyright (c)2001 Avery Dennison<p>
 * Company:      Avery Dennison<p>
 * @author Brad Nelson
 * @version 1.0
 */

package com.avery.utils;
import java.awt.Polygon;
import java.awt.Point;
import java.util.*;

/**
 * Creates a polygon defines by a set of points, main purpose of the class is a
 * method that returns a scaled Polygon.
 */
public class AveryPolygon extends Polygon
{
	private static final long serialVersionUID = 7861655987809982162L;

	/**
   * constructor creates the super.Polygon from a string representation of points.
   * @param points - (x,y)(x1,y1)... list of integer points that form a polygon
  */
  public AveryPolygon(String points)
  {
    StringTokenizer stParen = new StringTokenizer(points, ")");
    while (stParen.hasMoreTokens())
    {
      String point = stParen.nextToken();
      String sX = point.substring(1, point.indexOf(","));
      String sY = point.substring(point.indexOf(",")+1, point.length());
//      System.out.println(sX + ", " + sY);
      int polyx = Integer.parseInt(sX);
      int polyy = Integer.parseInt(sY);
      this.addPoint(polyx, polyy);
    }
  }

  /**
   * constructor creates the super.Polygon from a list of points.
   * @param polypoints - ArrayList of points
  */
  public AveryPolygon(ArrayList polypoints)
  {
    if (!polypoints.isEmpty())
    {
      Iterator iterator = polypoints.iterator();
      while (iterator.hasNext())
      {
        Point point = (Point)(iterator.next());
        this.addPoint(point.x, point.y);
      }
    }
  }

  /**
   * accessor returns this object.
  */
  public Polygon getPolygon()
  {
    return this;
  }

  /**
   * creates a scaled Polygon.
   * @param scalar - scale factor to scale the original Polygon by
   *
   * @return scaled Polygon
  */
  public Polygon createScaledPolygon(double scalar)
  {
    Polygon scaledPolygon = null;
    if (this.npoints > 0)
    {
      scaledPolygon = new Polygon();
      for (int i = 0; i < this.npoints; i++)
      {
        int x = this.xpoints[i];
        int y = this.ypoints[i];
        scaledPolygon.addPoint((int)((double)x * scalar),
                               (int)((double)y * scalar));
      }
    }
    return scaledPolygon;
  }

}

