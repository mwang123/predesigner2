/**
 * AveryConstants interface.  A class that implements this interface has
 * access to compile time constants.  No methods need be implemented.
 *
 * @author Brad Nelson
 * @version 1.000
 */

package com.avery.utils;

/**
 * Interface contains conditional flags.
 */
public interface AveryConstants
{
  // set DEBUG to true to turn on conditional debug statements
  // throughout the code
  public static final boolean DEBUG = false;
  //  public static final boolean DEBUG = true;

  // the AveryPrint code build number
  public static final String buildNumber = "Walrus 1";

  // the date of the AveryPrint code build
  public static final String buildDate = "July 17, 2002";
}