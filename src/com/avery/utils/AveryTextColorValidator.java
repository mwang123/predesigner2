/**
 * Title:        Avery Text Color Validator<p>
 * Description:  Validates a color for use in an AveryTextField<p>
 * Copyright:    Copyright (c)2001 Avery Dennison Corp., All Rights Reserved<p>
 * @author Bob Lee
 * @version 1.0
 */

package com.avery.utils;
import java.awt.Color;

/**
 * returns the original color or a suitable replacement.
 */
public interface AveryTextColorValidator
{
  /**
   * Given a color, this routine returns either the same color or a suitable
   * replacement.
   * @param color the color to test
   * @return the color that should be used
   */
  public Color validTextColor(Color color);
}