/**
 * Avery utility class.
 *
 * @author Brad Nelson
 * @version 1.000
 */

package com.avery.utils;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Contains several general purpose methods
 */
abstract public class AveryUtils
{
  // we don't want to lock all of AveryUtils...
  static Object Lock = new Object();  // lock used when generate unique date time stamp
  static Object Lock2 = new Object(); // lock used when generate unique 24 hour time stamp

  protected void finalize() throws Throwable
  {
    Lock = null;
  }

  /**
   * create unique xml file name.
   * @param sDir - output directory
   * @param sComputerName - first part of prefix of filename
   * @param sPrefix - second part of prefix of filename
   *
   * @return complete unique xml pathname
  */
  static public String generateXMLName(String sDir, String sComputerName,
    String sPrefix)
  {
    String sFullPrefix = sComputerName + "_" + sPrefix;
    String xmlName = AveryUtils.generateFileName(sDir, sFullPrefix) + ".xml";
    return xmlName;
  }

  /**
   * create unique file name.
   * @param sDir - output directory
   *
   * @return complete unique file pathname, without extension
  */
  static public String generateFileName(String sDir, String filePrefix)
  {
    synchronized(Lock)
    {
      // create a unique file name prefixed with YMDHms 24 hour clock
      Date d = new Date();
      // important that date portion is unique for several years
      // for saved projects
      SimpleDateFormat sdf = new SimpleDateFormat("yMdHms");
//      String sPrefix = sdf.format(d);
      String sPrefix = filePrefix + "_" + sdf.format(d);
      String sName;

      try
      {
        File tempFile = File.createTempFile(sPrefix, "", new File(sDir));
        sName = tempFile.getAbsolutePath();
        tempFile.delete();
        tempFile = null;
      }
      catch (IOException ioe)
      {
        // if fails punt back to old clock file name
        SimpleDateFormat sdf1 = new SimpleDateFormat("MdyyHmsSSS");
        sName = sDir + "/" + sdf1.format(d);
      }
      return sName.replace('\\', '/');
    }
  }

  /**
   * create 24 hour time stamp.
   * @return 24 hour clock time Hms
  */
  static public String generate24TimeStamp()
  {
    synchronized(Lock2)
    {
      // create a unique string consisting of 24 hour clock time HmsM
      Date d = new Date();
      SimpleDateFormat sdf = new SimpleDateFormat("HmsM");
      return sdf.format(d);
    }
  }

  /**
   * rename a file within same directory.
   * @param sFullPath - old full pathname
   * @param sNewName - new filename
   *
   * @return complete new pathname
  */
  static public String renameFile(String sFullPath, String sNewName)
  {
    String sExt = null;

    // parse existing name
    File getParts = new File(sFullPath);
    String sPath = getParts.getParent();
    String sNewFullName = sPath + "/" + sNewName;
    int index = sFullPath.lastIndexOf(".");
    if (index > 0)
    {
      sExt = sFullPath.substring(index);
      if (sExt != null)
        sNewFullName += sExt;
    }

    // rename to the new name
    File rename = new File(sNewFullName);
    getParts.renameTo(rename);
    return sNewFullName;
  }

  /**
   * parses a string representation of float value.
   * @param valIn - string representation of a float value
   *
   * @return parsed float
  */
  static public float parseFloat(String valIn)
  {
    if(valIn.equals(""))
      return 0;
    try
    {
      Float f = new Float(valIn);
      return f.floatValue();
    }
    catch(Exception e)
    {
      System.out.println("Problem with parseFloat\n");
      System.out.println(e);
      return 0;
    }
  }


  /**
   * copy a file to a string.
   * @param sInputFileName - input file to read
   *
   * @return file as a string
  */
  static public String copyFile(String sInputFileName)
  {
    String sOutput = "";

    try
    {
      FileReader fr = new FileReader(sInputFileName);
      while (true)
      {
        int i = fr.read();
        if (i == -1)
          break;

        sOutput += (char)i;
      }
      fr.close();
    }
    catch (IOException ioe)
    {
      sOutput = "";
    }

    return sOutput;
  }


  /**
   * Returns the contents as a String of the specified text file.
   * @param fspec - Full filespec of the file to be read
   * @return Contents of the specified file, or "" if I/O error
   */
  static public String readTextFileContents (String fspec)
  {
    try
    {
      // Open filespec
      File f = new File(fspec);
      FileReader in = new FileReader(f);

      int size = (int)f.length();       // get file size
      int chars_read = 0;               // counter for chars read
      char[] databuf = new char[size];  // data buffer

      // read all available characters into the buffer
      while( in.ready() )
      {
        // increment the count for each character read,
        // and accumulate them in the data buffer.
        chars_read += in.read(databuf, chars_read, size - chars_read);
      }
      in.close(); // we're done - close the file

      // convert data buffer to string and return it
      return new String(databuf, 0, chars_read);
    }
    catch (IOException e)
    {
      System.err.println(AveryUtils.timeStamp() + e.toString() );
      return "";
    }
  }

  /**
   * copies a file.
   * @param sInputFileName - input file
   * @param sOutputFileName - output file
  */
  static public void copyFile(String sInputFileName, String sOutputFileName)
    throws IOException
  {
    FileInputStream fin = null;
    FileOutputStream fout = null;
    fin = new FileInputStream(sInputFileName);
    fout = new FileOutputStream(sOutputFileName);
    AveryUtils.streamCopy(fin, fout);
    fin.close();
    fout.close();
  }

  /**
   * copies a stream to another stream.
   * @param in - input stream
   * @param out - output stream
  */
  static public void streamCopy(FileInputStream in, FileOutputStream out)
    throws IOException
  {
    // do not allow other threads to read from the input
    // or write to the output while copying is taking place
    synchronized (in)
    {
      synchronized (out)
      {
        byte buffer[] = new byte[256];
        int nBytesCopied = 0;
        while (true)
        {
          int bytesRead = in.read(buffer);
          nBytesCopied += bytesRead;
          // limits the size of file to 10 million bytes
          // to prevent infinite loop
          if (bytesRead == -1 || nBytesCopied > 10000000)
            break;
          out.write(buffer, 0, bytesRead);
        }
        buffer = null;
      }
    }
  }

  /**
   * replaces space character with URL %20 string.
   * @param sText - text to perform replacement on
   * @return HTML - replacement text
  */
  static public String replaceCharsWithURLChars(String sText)
  {
    String sReplace = "";
    for (int i = 0; i < sText.length(); i++)
    {
      char c = sText.charAt(i);
      if (c <= ' ')
      {
        int ic = (int)c;
        String replace = '%' + Integer.toHexString(ic);
//        System.out.println("replace=" + replace);
        sReplace += replace;
      }
      else
        sReplace += c;
    }
    return sReplace;
  }

  /**
   * filters text for HTML text control.
   * @param sText - text to filter
   * @return HTML - filtered text
  */
  static public String textToHTMLString(String sText)
  {
//    String sResult = "";
//    System.out.println(convertUTF8ToUTF8DecimalUnicode(sText));
    return (convertUTF8ToDecimalEncodedUnicode(sText));
  }

  static public String convertUTF8ToDecimalEncodedUnicode(String sXML)
  {
    String sResult = "";

    // Convert to bytes
    byte[] utf8 = new byte[1];
    try
    {
      utf8 = sXML.getBytes("UTF-8");
    }
    catch (Exception e)
    { utf8 = null; }

    int i = utf8.length;
    int j = 0;

    // each UTF character is between 1 and 3 bytes long
    // see "O'Reilly Java I/O Chapter 14 UTF-8 section for encoding of UTF-8
    while (j < i)
    {
      // concat values that already are decimal coded
      String decimalEncoded = validateDecimalEncoded(utf8, j, i);
      if (decimalEncoded.length() > 0)
      {
        j += decimalEncoded.length();
//        System.out.println("decimalEncoded=" + decimalEncoded);
        sResult += decimalEncoded;
      }
      else
      {
        // a new value to convert to decimal encoded unicode
        int unicodeValue = 0;
        int firstByte = utf8[j++];
        if (firstByte < 0)
          firstByte += 256;
        // is it one, two, or three byte character?
        if (firstByte < 0x80)
        {
          // one byte
          char c = (char)firstByte;
          if (c != '"')
            sResult += c;
          else
          {
            unicodeValue = firstByte;
            sResult += "&#" + String.valueOf(unicodeValue) + ";";
          }
        }
        else if ((firstByte & 0xe0) == 0xc0)
        {
          // two bytes
          int secondByte = utf8[j++];
          if (secondByte < 0)
            secondByte += 256;

          unicodeValue = (firstByte & 0x1f) * 64 +
                         (secondByte & 0x3f);

          sResult += "&#" + String.valueOf(unicodeValue) + ";";
        }
        else
        {
          // three bytes
          int secondByte = utf8[j++];
          int thirdByte = utf8[j++];
          if (secondByte < 0)
            secondByte += 256;
          if (thirdByte < 0)
            thirdByte += 256;

          unicodeValue = ((firstByte & 0x0f) << 4) * 256 +
                         (secondByte & 0x3f) * 64 +
                         (thirdByte & 0x03f);

          sResult += "&#" + String.valueOf(unicodeValue) + ";";
        }
      }
    }

    return sResult;
  }

  // if sequence of bytes matches a decimal encoded unicode value
  // return the encoded string else return empty string
  static private String validateDecimalEncoded(byte[] utf8, int j, int i)
  {
    int j1 = j;

    String decimalEncoded = "";
    String inProgress = "";

    // decimal encoded string has format &#n...;
    if (utf8[j1++] == '&')
    {
      if (j1 < i && utf8[j1] == '#')
      {
        inProgress = "&#";
        j1++;
        while (j1 < i && (utf8[j1] >= '0' && utf8[j1] <= '9'))
        {
//    System.out.println("validateDecimalEncoded utf8[j1]=" + (char)utf8[j1]);
          inProgress += (char)utf8[j1];
          j1++;
        }
        if (j1 < i && utf8[j1] == ';')
        {
          decimalEncoded = inProgress + ";";
        }
      }
    }

    return decimalEncoded;
  }

  // remove control character that somehow appear in some PDF files on live server
  static public String removeControlChars(String sXML)
  {  	
    return sXML.replaceAll("[\\x00-\\x08\\x0b\\x0c\\x0e-\\x1f]", "");
  }
  
  // decimal encode unicode
  // this is used to translate the project xml uploaded
  // to the browser to decimal encoded before it is
  // saved so that Project can load the unicode chars successfully
  static public String convertUnicodeToUnicodeDE(String sXML)
  {
//    System.out.println("sXMLIn=" + sXMLIn);
    String sResult = "";

    int i = 0;
		while (i < sXML.length())
		{
      int unicodeValue = 0;

			int c = (int)(sXML.charAt(i++));
//      System.out.println("c. dec=" + c + "," + String.valueOf(c));

      if (c < 128)
        sResult += (char)c;
      else
      {
        // decimal encode all chars above 127
        sResult += "&#" + String.valueOf(c) + ";";
      }
    }
//    System.out.println("sResult=" + sResult);
    return sResult.replaceAll("[\\x00-\\x08\\x0b\\x0c\\x0e-\\x1f]", "");
  }

  /**
   * append title inside of HTML title tags, if tags are present.
   * @param sHTMLText - original text
   * @param sTitle - new title
   * @return sResult - replaced text
  */
  static public String appendHTMLTitle(String sHTMLText, String sTitle)
  {
    // resort to original string if things go bad
    String sResult = sHTMLText;

    // remove leading HTML, if any, from title
    String sPureTitle = sTitle.trim();
    // guard against bad data - prevent going into infinite loop
    if (sPureTitle.endsWith("<"))
      sPureTitle += " ";
    while (sPureTitle.startsWith("<"))
    {
      // skip leading tag(s)
      sPureTitle = sPureTitle.substring(sPureTitle.indexOf(">") + 1);
    }
    if (sPureTitle.indexOf("<") > 0)
    {
      sPureTitle = sPureTitle.substring(0, sPureTitle.indexOf("<"));
    }

    // find the title start tag
    int p1 = sResult.indexOf("<TITLE>");
    if (p1 < 0)
      p1 = sResult.indexOf("<title>");
    if (p1 > -1)
    {
      // find the title end tag
      int p2 = sResult.indexOf("</TITLE>");
      if (p2 < 0)
        p2 = sResult.indexOf("</title>");
      // end tag must follow start tag
      if (p2 > p1)
      {
        // insert new title
        sResult = sHTMLText.substring(0, p2);
        sResult += " - " + sPureTitle;
        sResult += sHTMLText.substring(p2);
      }
    }

    return sResult;
  }

  /**
   * Replaces first occurance of search text with replace text
   * @param sText - text to search and replace
   * @param sSearchText - search for text
   * @param sReplaceText - replace
   * @return sResult - replaced text
  */
  static public String replaceSearchText(String sText, String sSearchText, String sReplaceText)
  {
    String lower = sText.toLowerCase();
    String searchLower = sSearchText.toLowerCase();

    StringBuffer sb = new StringBuffer(sText);
    int start_pos = lower.indexOf(searchLower);
    if (start_pos > -1)
    {
      int end_pos = start_pos + sSearchText.length();
      sb.replace(start_pos, end_pos, sReplaceText);
    }
    return new String(sb);
  }

  /**
   * generates a string representation of the current date & time, for use in
   * timestamping log entries
   *
   * @return formatted timestamp
   */
  static public String timeStamp()
  {
    return new SimpleDateFormat("yyyy.MM.dd HH:mm:ss ").format(new Date());
  }

  /**
   * generates a string representation of elapsed time in ms between a
   * supplied starting date and the current date.
   *
   * @param startDate - start time
   * @return elapsed time ms
   */
  static public String elapsedTime(Date startDate)
  {
    Date endDate = new Date();
    long l1 = startDate.getTime();
    long l2 = endDate.getTime();
    long difference = l2 - l1;
    // format so sorts ok
    String elapsed = "" + difference;
    String leader = "";
    int len = elapsed.length();
    while (len++ < 10)
    {
      leader += "0";
    }
    return ("ET ms: " + leader + difference + " " + timeStamp());
  }

  /**
   * finds shortest distance between two points
   * @param x1 - x coordinate of first point
   * @param y1 - y coordinate of first point
   * @param x2 - x coordinate of second point
   * @param y2 - y coordinate of second point
   *
   * @return distance
  */
  static public double findPointPointDistance(double x1, double y1, double x2, double y2)
  {
    // familiar formula distance = sqrt((x1-x2)^2 + (y1-y2)^2)
    double squared = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    return Math.sqrt(squared);
  }

  /**
   * finds shortest distance between a point and a rect
   * @param x - x coordinate of point
   * @param y - y coordinate of point
   * @param point - coordinate of upper left corner of rect
   * @param width - width of rect
   * @param height - height of rect
   *
   * @return distance
  */
  static public double findPointRectDistance(double x, double y, java.awt.Point point, double width, double height)
  {
    // think of the rect as occupying the middle of a tic tac toe game.
    // the point can be in one of the nine zones of the game.
    // find the zone the point is in and the distance calculation is easy for each zone.

    // the default distance is zone 5 - the point is in the rect
    double distance = 0;

    // the upper left coordinate of the rect
    double x1 = point.x;
    double y1 = point.y;
    // the lower right coordinate of the rect
    double x2 = x1 + width - 1.0;
    double y2 = y1 + height - 1.0;

    if (x < x1)
    {
      if (y < y1)
      {
        // zone 1 - left and above
        distance = findPointPointDistance(x, y, x1, y1);
      }
      else if (y > y2)
      {
        // zone 7 - left and below
        distance = findPointPointDistance(x, y, x1, y2);
      }
      else
      {
        // zone 4 - left
        distance = x1 - x;
      }
    }
    else if (x > x2)
    {
      if (y < y1)
      {
        // zone 3 - right and above
        distance = findPointPointDistance(x, y, x2, y1);
      }
      else if (y > y2)
      {
        // zone 9 - right and below
        distance = findPointPointDistance(x, y, x2, y2);
      }
      else
      {
        // zone 6 - right
        distance = x - x2;
      }
    }
    else
    {
      if (y < y1)
      {
        // zone 2 - above
        distance = y1 - y;
      }
      else if (y > y2)
      {
        // zone 8 - below
        distance = y - y2;
      }
    }
    return distance;
  }

  /**
   * finds shortest distance between a point and a rect
   * @param point - location in 2D space
   * @param rect - rectangle in 2D space
   * @return distance from point to rect (0.0 if rect contains point)
  */
  static public double findPointRectDistance(java.awt.Point point,
    java.awt.Rectangle rect)
  {
    if (rect.contains(point))
    {
      return 0.0;
    }

    return findPointRectDistance(point.getX(), point.getY(), rect.getLocation(),
                                  rect.getWidth(), rect.getHeight());
  }

  /**
   * Convert a UTF-8 string to MacRoman encoded string
   * Type1 fonts are MacRoman encoded, noticeably the international characters differ from ISO
   * @param sUTF8 - UTF-8/ISO encoded string
   * @return MacRoman encoded string
  */
  static public String convertUTF8StringToMacRoman(String sUTF8)
  {
    // if things go wrong return same string
    String sMacRoman = sUTF8;
//    dumpString("utf8", sUTF8);
    try
    {
      // must assume incoming string is UTF8/ISO
      byte[] me = sUTF8.getBytes("MacRoman");
      // return the MacRoman version
      sMacRoman = new String(me, "8859_1");
//      dumpString("sMacRoman", sMacRoman);
    }
    catch (UnsupportedEncodingException uee)
    {
    }
    return (sMacRoman);
  }

  /**
   * Convert a UTF-8 decimal or hex coded string to Unicode
   * @param sXML - UTF-8 decimal or hex encoded string
   * @return Unicode encoded string
  */
  static public String convertUTF8DecimalToUnicode(String sXML)
  {
    String unicode = "";
    int pos = 0;
    int length = sXML.length();

    if (length > 0)
    {
      while (pos < sXML.length())
      {
        // look for decimal/hex coded unicode bytes '&#ddd;', '&#xdd;'
        if (sXML.charAt(pos) == '&')
        {
          String amper = sXML.substring(pos);
          if (amper.length() >= 4)
          {
            if (amper.charAt(1) == '#')
            {
              amper = amper.substring(2);
              int semi = amper.indexOf(";");
              if (semi > 0)
              {
                int uni = 0;
                if (amper.charAt(0) == 'x' || amper.charAt(0) == 'X')
                {
                  amper = amper.substring(1, semi);
                  uni = Integer.parseInt(amper, 16);
                  pos += amper.length() + 4;
                }
                else
                {
                  amper = amper.substring(0, semi);
                  uni = Integer.parseInt(amper);
                  pos += amper.length() + 3;
                }
      //          System.out.println("char=" + uni);
                unicode += (char)uni;
                continue;
              }
            }
          }
        }
        // process next char unless found decimal or hex coded unicode byte above
        unicode += sXML.charAt(pos++);
      }
    }
    // return converted string
    return (unicode);
  }

  /**
   * Convert a latin1 string to UTF-8
   * @param latin1 - latin1 string
   * @return UTF-8 encoded string
  */
  static public String convertLatin1UTF8(String latin1)
  {
    String utf8 = "";

    int i = 0;
		while (i < latin1.length())
		{
			int c = (int)(latin1.charAt(i));
			if (c < 128)
				utf8 += latin1.charAt(i);
			else
			{
				if (c > 159 && c < 256)
				{
          int c1 = c;
          if (c > 191)
            c1 -= 64;
          byte[] unibytes = new byte[2];
          unibytes[0] = (byte)(0xc3);
          unibytes[1] = (byte)(c1);
          String unicode = new String(unibytes);
          utf8 += unicode;
				}
			}
			i++;
		}

    return utf8.replaceAll("[\\x00-\\x08\\x0b\\x0c\\x0e-\\x1f]", "");
  }

  /**
   * Convert a latin 1 string to UTF-8
   * @param iso8859 - ISO-8859-1 string
   * @return UTF-8 encoded string
  */
  static public String convert8859UTF8(String iso8859)
  {
    String utf8 = iso8859;

    try
    {
      byte[] utfiso = iso8859.getBytes("ISO-8859-1");
      utf8 = new String(utfiso, "UTF-8");
    }
    catch (Exception e){}

    return utf8;
  }

  /**
   * Get the machine name
   * Since Java does not provide the computer name
   * a Windows DLL is called to find it the first time only
   * the DLL is assumed to be in the servlet runner lib path
   * @return computer name
  */
  static public String getComputerName()
  {
    // if things go wrong return empty string
    String computerName = "";

    return (computerName);
  }

  /**
   * Dumps a string to System.out for debugging
   * Type1 fonts are MacRoman encoded, noticeably the international characters differ from ISO
   * @param sDump - string to dump
   * @return label and dump int values of string
  */
  static private void dumpString(String sLabel, String sDump)
  {
    String sDumpit = sLabel + " ";
    for (int i = 0; i < sDump.length(); i++)
    {
      int val = sDump.charAt(i);
      sDumpit += Integer.toString(val) + " ";
    }
    System.out.println(sDumpit);
  }

  /**
   * Returns a string containing a 4-digit number with required zero padding
   * @param num - number (int) to be padded
   * @return String containing the zero-padded 4-digit number
   */
  static public String makePaddedNumber (int num)
  {
    if ( num < 1 || num > 9999 )
      return "Null";
    else if ( num < 10 )
      return "000" + num;
    else if ( num < 100 )
      return "00" + num;
    else if ( num < 1000 )
      return "0" + num;
    else
      return "" + num;
  }


  /**
   * Determines if a string consists only of digits that are properly formatted
   * for the "int" type.
   * @param s String to be evaluated
   * @return true or false
   */
  static public boolean isNumberInteger (String s)
  {
    try
    {
      Integer.valueOf(s);
    }
    catch( NumberFormatException e)
    {
      return false;
    }
    return true;
  }

  /**
   * Extracts from a string the "int" value of any digits found.
   * @param s String to be evaluated
   * @return The number as an int; -1 if error
   */
  static public int getIntFromString (String s)
  {
    Integer iVal;

    try
    {
      iVal = Integer.valueOf(s);
    }
    catch( NumberFormatException e)
    {
      return -1;
    }
    return iVal.intValue();
  }


  static public boolean isNCName(String ncname)
  {
    char[] sChars = ncname.toCharArray();
    if (java.lang.Character.isLetter(sChars[0]) || sChars[0] == '_')
    {
      return true;
    }
    return false;
  }

	static public Point rotatePoint(Point center, Point position, double angle)
	{
		double checkAngle = angle;
		if (checkAngle >= 360)
		{
			while (checkAngle >= 360)
				checkAngle -= 360;
		}
		while (checkAngle <= -360)
			checkAngle += 360;

		if (checkAngle == 0)
			return position;

		double radians = checkAngle * Math.PI / 180;
		double x = position.x - center.x;
		double y = position.y - center.y;

		double cos = Math.cos(radians);
		double sin = Math.sin(radians);

		int xPrime = (int)(x * cos - y * sin);
		int yPrime = (int)(x * sin + y * cos);

		return new Point(center.x + xPrime, center.y + yPrime);
	}

	static public int counterRotate(int clockAngle)
	{
		int counterAngle = (int)clockAngle;

		if (counterAngle > 0)
			counterAngle = 360 - counterAngle;
		else if (counterAngle < 0)
			counterAngle = -counterAngle;

		return counterAngle;
	}

  /**
   * Sometimes we need to escape quotes as \" or \' in a string,
   * as when outputting a Javascript value.
   * @param s - the input string
   * @return a new string with single and double quotes escaped,
   * or <code>null</code> if input was <code>null</code>
   */
  public static String escapeQuotesWithBackslashes(String s)
  {
    if (s == null)
      return null;

    StringBuffer buf = new StringBuffer();
    for ( int i = 0; i < s.length(); i++ )
    {
      char c = s.charAt( i );
      if (c == '\"')
      {
        buf.append("\\\"");
      }
      else if (c == '\'')
      {
        buf.append("\\\'");
      }
      else
      {
        buf.append( c );
      }
    }
    return buf.toString();
  }

  /**
   * @param input string
   * @return string with all whitespace and puctuation removed
   */
  public static String alphaNumeric(String input)
  {
    return (input == null) ? "" : input.replaceAll("[\\W]", "");
  }
  
  public static final int TWIPS = 0;
  public static final int INCHES = 1;
  public static final int CENTIMETERS = 2;

  public static double twipsToUnits(double twips, int units)
  {
    if (units == AveryUtils.INCHES)
    {
      return twips / 1440.0;
    }
    else if (units == AveryUtils.CENTIMETERS)
    {
      return twips / (1440. / 2.54);
    }
    else
    {
      return twips;
    }
  }

  /**
   * Note that the String is rounded to a printable resolution
   * @param twips	- actual dimension in TWIPS
   * @param units	- Measurement Units: AveryUtils.INCHES, AveryUtils.CENTIMETERS, AveryUtils.TWIPS
   * @return a String of the number in the units param
   */
  public static String twipsToUnitString(Double twips, int units)
  {
    if (units == AveryUtils.INCHES)
    {
      double d = twips.doubleValue() / 1440.0;
      int i = (int)(d * 10000.0);
      d = ((double)i) / 10000.0;
      return Double.toString(d);
    }
    else if (units == AveryUtils.CENTIMETERS)
    {
      double d = twips.doubleValue() / (1440. / 2.54);
      int i = (int)(d * 10000.0);
      d = ((double)i) / 10000.0;
      return Double.toString(d);
    }
    else
    {
      double d = twips.doubleValue();
      int i = (int)(d * 1000.0);
      d = ((double)i) / 1000.0;
      return twips.toString();
    }
  }

  public static String twipsToUnitString(double twips, int units)
  {
    return twipsToUnitString(new Double(twips), units);
  }

  /**
   * The resulting Double is rounded to .01 twips
   * @param units	- Measurement Units: AveryUtils.INCHES, AveryUtils.CENTIMETERS, AveryUtils.TWIPS
   * @param str		- in units param to convert
   * @return twips
   */
  public static Double unitStringToTwips(int units, String str)
  {
    double d = Double.parseDouble(str);
    if (units == AveryUtils.INCHES)
    {
      int i = (int)(d * 144000.0);
      d = ((double)i) / 100.0;
      return new Double(d);
    }
    else if (units == AveryUtils.CENTIMETERS)
    {
      int i = (int)(d * (144000.0 / 2.54));
      d = ((double)i) / 100.0;
      return new Double(d);
    }
    else return new Double(d);
  }
}
