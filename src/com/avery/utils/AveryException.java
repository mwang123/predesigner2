package com.avery.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Title:        AveryException class<p>
 * Description:  extends Exception to add time stamping<p>
 * Copyright:    Copyright (c)2002 Avery Dennison Corp.<p>
 * @author Bob Lee
 */

public class AveryException extends Exception
{
	private static final long serialVersionUID = 3561507706589921615L;

	/**
   * The default constructor simply puts a time stamp in the message.
   */
  public AveryException()
  {
    super(timeStamp());
  }

  /**
   * Creates an AveryException with time stamp + str in the message.
   * @param str should describe the condition that caused the exception
   */
  public AveryException(String str)
  {
    super(timeStamp() + str);
  }

  static private String timeStamp()
  {
    return new SimpleDateFormat("yyyy.MM.dd HH:mm:ss - ").format(new Date());
  }
}