package com.avery.project;

import java.util.Hashtable;
import java.util.List;

import org.jdom.Element;

/*
 * File: Description.java
 * Created: Nov 16, 2004 by Bob Lee
 * Copyright (c) Avery Dennison Corporation, All Rights Reserved
 */

/**
 * com.avery.project.descriptions
 * Description object indexes localized string table.
 * <p>
 * The constructors of this class are private.  Various types of Description
 * objects can be instantiated by using the make*Description factory methods.
 * </p>
 */
public class Description implements Averysoftable, Cloneable
{
	// instance data
	private int index;
	private String customText;			// used if index == -1
	private String language = "en";	// the current language
	private Hashtable lists;  			// one of the static Hashtables from the Descriptions class
	
	private Description(Element element, Hashtable table)
	{
		String str = element.getAttributeValue("index");
		if (str == null || "-1".equals(str))
		{
			this.index = -1;
			this.customText = element.getText();
		}
		else
		{
			this.index = Integer.decode(str).intValue();
		}
		
		this.lists = table;
	}

	private Description(String text, Hashtable table)
	{
		this.lists = table;
		setText(text);
	}
	
	private Description(int index, Hashtable table)
	{
		this.index = index;
		this.lists = table;
	}
  
  Description(Description d)
  {
    index = d.index;
    customText = d.customText;
    language = d.language;
    lists = d.lists;
  }
	
	static Description makePanelDescription()
	{
		return new Description(Descriptions.DEFAULT_PANEL_INDEX, Descriptions.panelLists);
	}
	
	static Description makePanelDescription(Element element)
	{
		return new Description(element, Descriptions.panelLists);
	}

	static Description makePanelDescription(String text)
	{
		return new Description(text, Descriptions.panelLists);
	}

	static Description makePageDescription()
	{
		return new Description(Descriptions.DEFAULT_PAGE_INDEX, Descriptions.pageLists);
	}
	
	static Description makePageDescription(Element element)
	{
		return new Description(element, Descriptions.pageLists);
	}

	static Description makePageDescription(String text)
	{
		return new Description(text, Descriptions.pageLists);
	}

	static Description makeTextFieldDescription()
	{
		return new Description(Descriptions.DEFAULT_TEXT_INDEX, Descriptions.textFieldLists);
	}

	static Description makeTextFieldDescription(Element element)
	{
		return new Description(element, Descriptions.textFieldLists);
	}

	static Description makeTextFieldDescription(String text)
	{
		return new Description(text, Descriptions.textFieldLists);
	}

	static Description makeImageFieldDescription()
	{
		return new Description(Descriptions.DEFAULT_IMAGE_INDEX, Descriptions.imageFieldLists);
	}
	
	static Description makeImageFieldDescription(Element element)
	{
		return new Description(element, Descriptions.imageFieldLists);
	}

	static Description makeImageFieldDescription(String text)
	{
		return new Description(text, Descriptions.imageFieldLists);
	}
	
	static Description makeBackgroundFieldDescription()
	{
		return new Description(Descriptions.DEFAULT_BACKGROUND_INDEX, Descriptions.backgroundFieldLists);
	}

	static Description makeBackgroundFieldDescription(Element element)
	{
		return new Description(element, Descriptions.backgroundFieldLists);
	}

	static Description makeBackgroundFieldDescription(String text)
	{
		return new Description(text, Descriptions.backgroundFieldLists);
	}

	static Description makeBarcodeFieldDescription()
	{
		return new Description(Descriptions.DEFAULT_BARCODE_INDEX, Descriptions.barcodeFieldLists);
	}

	static Description makeBarcodeFieldDescription(Element element)
	{
		return new Description(element, Descriptions.barcodeFieldLists);
	}

	static Description makeBarcodeFieldDescription(String text)
	{
		return new Description(text, Descriptions.barcodeFieldLists);
	}

  static Description makeDrawingFieldDescription()
  {
    return new Description(Descriptions.DEFAULT_DRAWING_INDEX, Descriptions.drawingFieldLists);
  }

  static Description makeDrawingFieldDescription(Element element)
  {
    return new Description(element, Descriptions.drawingFieldLists);
  }

  static Description makeDrawingFieldDescription(String text)
  {
    return new Description(text, Descriptions.drawingFieldLists);
  }
  //

	/* (non-Javadoc)
	 * @see com.avery.miwok.Averysoft#getAverysoftElement()
	 */
	public Element getAverysoftElement()
	{
		Element element = new Element("description");
    element.setNamespace(AveryProject.getAverysoftNamespace());
		if (getIndex() != -1)
		{
			element.setAttribute("index", Integer.toString(index));
		}

		element.addContent(getText());

		return element;
	}

	public Element getAverysoftIBElement()
	{
		return null;
	}

  public int getIndex()
	{
		return index;
	}

	void setIndex(int index)
	{
		if (index > -1)
		{
			this.index = index;
			this.customText = null;
		}
	}

	/**
	 * Note that the custom text will automatically map to the correct indexed
	 * text if it exists in the current localization table.
	 * @param text
	 */
	void setText(String text)
	{
		if (getLocalizedStringList(getLanguage()).contains(text))
		{
			setIndex(getLocalizedStringList(getLanguage()).indexOf(text));
		}
		else
		{
			this.customText = text;
			this.index = -1;
		}
	}

	String getText()
	{
		if (getIndex() < 0)
		{
			return customText;
		}
		else
		{
			try
			{
				return lookup(getIndex());
			}
			catch (ArrayIndexOutOfBoundsException ex)
			{
        String str = customText;
        if (str == null)
          str = Integer.toString(index);
				System.err.println("Invalid Description index " + getIndex() + " encountered, using \"" + str + "\" instead");
				return str;
			}
		}
	}

	public void setLanguage(String language)
	{
		this.language = language;
	}

	public String getLanguage()
	{
		return language;
	}

	List getLocalizedStringList(String language)
	{
		if (language == null || !lists.containsKey(language))
		{
			return (List)lists.get("en");
		}
		else
		{
			return (List)lists.get(language);
		}
	}

	private String lookup(int index)
	{
		return lookup(index, getLanguage());
	}
	
	private String lookup(int index, String language)
	{
		if (language != null && lists.containsKey(language))
		{
			return (String)getLocalizedStringList(language).get(index);
		}
		else
		{
			return (String)getLocalizedStringList("en").get(index);
		}
	}
  
  /**
   * A translation application needs to know if a Description is indexed or custom  
   * @return true if the Description is not indexed for localization
   */
  boolean isCustom()
  {
    return index < 0;
  }
}