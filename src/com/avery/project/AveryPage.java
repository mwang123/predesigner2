/**
 * Title:       AveryPage<p>
 * Description: <p>
 * Copyright:   (c)Copyright 2000-2001 Avery Dennison Corp. All Rights Reserved.
 * @author      Bob Lee
 * @version     2.0
 */

package com.avery.project;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.faceless.pdf2.CMYKColorSpace;
import org.faceless.pdf2.PDF;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFStyle;
import org.faceless.pdf2.SpotColorSpace;
import org.jdom.Element;

/**
 * The AveryPage class encapsulates a printable page of Avery media.  It
 * includes a list of AveryPanels, which are often used to represent individual
 * labels on the page.
 */
public class AveryPage implements Cloneable, XMLElement, Averysoftable
{
  /**
   * This constructor does nothing.
   */
  public AveryPage()
  { }

  // properties
  private List panels = new ArrayList();
  private List guides;
  private List gridLayouts;
	private Hashtable hints;
  private List cutouts;
  private Double width;
  private Double height;
  private String paperSize = "Letter";
  private String viewOrientation = "portrait";
  private boolean doubleSided = false;
  private boolean mirrorPrint = false;
  private Color paperColor = Color.white;
  private String paperImage;
  private boolean print180 = false;
  private Description description;
  private Integer tileCountX = new Integer(0);
  private Integer tileCountY = new Integer(0);
  private Double tileOverlapX;
  private Double tileOverlapY;

  public static boolean enablePanelClipping = true;
  
  // Spot color for page
  public static float[] SPOT_COLOR_PAGE = {0.572556f, 0.941177f, 0.956863f, 0.0f};
  // inset of PDF page outline for cutter when DIFY
  public static int PDF_PAGE_OUTLINE_INSET = 3;
 
  /**
   * Constructs a page from an Avery DTD or averysoft.xsd based XML Element
   * @param element - describes the page
   * @throws Exception if element isn't up to snuff
   */
  AveryPage(Element element)
  throws Exception
  {
    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace())
    	&& element.getName().equals("page"))
    {
    	// required attributes first
      width = Double.valueOf(element.getAttributeValue("width"));
      height = Double.valueOf(element.getAttributeValue("height"));
      paperSize = element.getAttributeValue("paperSize");
      viewOrientation = element.getAttributeValue("viewOrientation");

      // optional attributes
      final String trueString = "true";
      doubleSided = trueString.equals(element.getAttributeValue("doubleSided"));
		  mirrorPrint = trueString.equals(element.getAttributeValue("mirrorPrint"));
			String colorString = element.getAttributeValue("paperColor");
			if (colorString != null)
		  {
		  	paperColor = Color.decode(colorString);
		  }
		  //paperImage = element.getAttributeValue("paperImage");
		  print180 = trueString.equals(element.getAttributeValue("print180"));

			Element descriptionElement = element.getChild("description", AveryProject.getAverysoftNamespace());
			if (descriptionElement == null)
			{
				description = Description.makePageDescription();	// default page description
			}
			else
			{
				description = Description.makePageDescription(descriptionElement);
			}

			Iterator iterator = element.getChildren("hint", AveryProject.getAverysoftNamespace()).iterator();
			while (iterator.hasNext())
			{
				Element child = (Element)iterator.next();
				addHint(child.getAttributeValue("name"), child.getAttributeValue("value"));
			}

			iterator = element.getChildren("cutout", AveryProject.getAverysoftNamespace()).iterator();
			while (iterator.hasNext())
			{
				addCutout((Element)iterator.next());
			}

			List guideElements = element.getChildren("guide", AveryProject.getAverysoftNamespace());
			if (guideElements.size() > 0)
			{
				guides = new ArrayList(guideElements.size());
				iterator = guideElements.iterator();
				while (iterator.hasNext())
				{
					guides.add(new Guide((Element)iterator.next()));
				}
			}

			// page tileAttributes
			Element tileAttributes = element.getChild("tileAttributes", AveryProject.getAverysoftNamespace());
			if (tileAttributes != null)
			{
        tileCountX = Integer.valueOf(tileAttributes.getAttributeValue("tileCountX"));
        tileCountY = Integer.valueOf(tileAttributes.getAttributeValue("tileCountY"));
        tileOverlapX = Double.valueOf(tileAttributes.getAttributeValue("tileOverlapX"));
        tileOverlapY = Double.valueOf(tileAttributes.getAttributeValue("tileOverlapY"));
			}
			
			List gridLayoutElements = element.getChildren("gridLayout", AveryProject.getAverysoftNamespace());
			if (gridLayoutElements.size() > 0)
			{
				gridLayouts = new ArrayList(gridLayoutElements.size());
				iterator = gridLayoutElements.iterator();
				while (iterator.hasNext())
				{
					gridLayouts.add(new AveryGridLayout((Element)iterator.next()));
				}
			}

			// read panels
			iterator = element.getChildren("panel", AveryProject.getAverysoftNamespace()).iterator();
			while (iterator.hasNext())
			{
				addPanel((Element)iterator.next());
			}
    }
    else if (!initFromAveryDtdElement(element))
    {
      Exception ex = new Exception("Not an Avery page element");
      throw ex;
    }
  }

  private boolean initFromAveryDtdElement(Element element)
  throws Exception
  {
		if (element.getName().equals("Avery.page"))
		{
		  setWidth(Double.valueOf(element.getAttributeValue("width")));
		  setHeight(Double.valueOf(element.getAttributeValue("height")));

		  String mirror = element.getAttributeValue("mirror");
		  setMirror("yes".equals(mirror) || "on".equals(mirror));

		  Element paperElement = element.getChild("Avery.paperimage");
		  if (paperElement != null)
		  {
				setPaperimage(paperElement.getAttributeValue("src"));
		  }

		  // read panels
		  Iterator i = element.getChildren("Avery.panel").iterator();
		  while (i.hasNext())
		  {
				addPanel((Element)i.next());
		  }

		  // use a default page description, as Avery.dtd didn't define page descriptions
			description = Description.makePageDescription();

		  // Note: The Avery.dtd technically allowed 0..n cutout objects here,
		  // but we never did get around to writing page cutouts in any files.

		  return true;
		}
		else
		{
			return false;
		}
  }



  /**
   * Creates an AveryPage clone
   * @return the AveryPage clone
   */
  public Object clone()
  {
    try
    {
      // create new page
      AveryPage ap = new AveryPage();
      // copy all attributes
      ap.width = width;
      ap.height = height;
      ap.paperSize = paperSize;
      ap.viewOrientation = viewOrientation;
      ap.doubleSided = doubleSided;
      ap.mirrorPrint = mirrorPrint;
      ap.paperColor = paperColor;
      //ap.paperImage = paperImage;
      if (tileCountX.intValue() > 0 && tileCountY.intValue() > 0)
      {
        ap.tileCountX = tileCountX;
        ap.tileCountY = tileCountY;
        ap.tileOverlapX = tileOverlapX;
        ap.tileOverlapY = tileOverlapY;
      }
      
      ap.gridLayouts = gridLayouts;

      Iterator i = panels.iterator();
      while (i.hasNext())
      {
        AveryPanel aPanel = (AveryPanel)i.next();
        // panel clones are only panel deep clones presently
        // so panel fields are shallow clones
        AveryPanel clonePanel = (AveryPanel)(aPanel.clone());
        ap.panels.add(clonePanel);
      }
      return (Object)ap;
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Generates an HTML-formatted string that describes the page.  This can be
   * useful for debugging.
   * @return HTML-formatted String
   */
  public String dump()
  {
    String str = new String("");
    str += "<P>AveryPage";
    str += "<br>width = " + width.toString();
    str += "<br>height = " + height.toString();
    str += "<br>paperimage = " + paperImage;

    Iterator iterator = panels.iterator();
    while (iterator.hasNext())
    {
      str += ((AveryPanel)iterator.next()).dump();
    }

    str += "<br>end AveryPage</P>\n";

    return str;
  }

  /**
   * Provides access to the panel list
   * @return List of AveryPanels
   */
  public List getPanels()
  {
    return panels;
  }
  
  public void setPanels(List pagePanels)
  {
  	panels = pagePanels;
  }
  
  
  public void setGridLayouts(List newGridLayouts)
  {
  	gridLayouts.clear();
  	gridLayouts = newGridLayouts;
  }
  
  /**
   * @return all of the panels on the page, sorted by their altSortOrder values
   */
  public List getAltSortedPanels(final boolean ascend)
  {
    ArrayList list = new ArrayList(panels);
    
    Collections.sort(list, new Comparator() {
      public int compare(Object o1, Object o2)  {
        Integer i1 = new Integer(((AveryPanel)o1).getAltSortOrder());
        Integer i2 = new Integer(((AveryPanel)o2).getAltSortOrder());
        return ascend ? (i1.compareTo(i2)) : (i2.compareTo(i1));
      }
    });
    
    return list;
  }
  
  /**
   * This is useful for "fill down" operations  
   * @return a list of all panels on the page, sorted by their X coordinate
   */
  List getXSortedPanels()
  {
    ArrayList list = new ArrayList(panels);
    
    Collections.sort(list, new Comparator() {
      public int compare(Object o1, Object o2)  {
        Point p1 = ((AveryPanel)o1).getPosition();
        Point p2 = ((AveryPanel)o2).getPosition();
        Double d1 = new Double(p1.getX());
        Double d2 = new Double(p2.getX());
        if (d1.equals(d2))
        {
          d1 = new Double(p1.getY());
          d2 = new Double(p2.getY());
        }
        return d1.compareTo(d2);
      }
    });
    return list;
  }
  
  /**
   * This is useful for "fill across" operations  
   * @return a list of all panels on the page, sorted by their Y coordinate
   */
  List getYSortedPanels()
  {
    ArrayList list = new ArrayList(panels);
    
    Collections.sort(list, new Comparator() {
      public int compare(Object o1, Object o2)  {
        Point p1 = ((AveryPanel)o1).getPosition();
        Point p2 = ((AveryPanel)o2).getPosition();
        Double d1 = new Double(p1.getY());
        Double d2 = new Double(p2.getY());
        if (d1.equals(d2))
        {
          d1 = new Double(p1.getX());
          d2 = new Double(p2.getX());
        }
        return d1.compareTo(d2);
      }
    });
    return list;
  }

  /**
   * Adds a panel to the page
   * @param panel - the AveryPanel to add
   */
  public void addPanel(AveryPanel panel)
  {
    panels.add(panel);
  }

  public void addPanel(Element element)
  throws Exception
  {
  	panels.add(new AveryPanel(element));
  }

  /**
   * Sets the width of the page in twips
   * @param newWidth - the desired width
   */
  public void setWidth(Double newWidth)
  {
    width = newWidth;
  }

  /**
   * Retrieves the width of the page
   * @return the width of the page in twips
   */
  public Double getWidth()
  {
    return width;
  }

  /**
   * Sets the height of the page in twips
   * @param newHeight - the desired height
   */
  public void setHeight(Double newHeight)
  {
    height = newHeight;
  }

  /**
   * Retrieves the height of the page
   * @return the height of the page in twips
   */
  public Double getHeight()
  {
    return height;
  }

  /**
   * Sets the double sided state of the page
   * @param newDoubleSided - true or false
   */
  void setDoubleSided(boolean newDoubleSided)
  {
    doubleSided = newDoubleSided;
  }

  /**
   * Retrieves the dpuble sided state of the page
   * @return the double sided state - true or false
   */
  public boolean getDoubleSided()
  {
    return doubleSided;
  }

  /**
   * Sets the mirror state of the page
   * @param newMirror - on or off
   */
  void setMirror(boolean newMirror)
  {
    mirrorPrint = newMirror;
  }

  /**
   * Retrieves the mirror state of the page
   * @return the mirror state - on or off
   */
  boolean getMirror()
  {
    return mirrorPrint;
  }

  /**
   * The Color of the page
   * @param newPaperColor - Color of the page
   */
  public void setPaperColor(Color newPaperColor)
  {
    paperColor = newPaperColor;
  }

  /**
   * Retrieves the Color of the page
   * @return Color of the page
   * @see #setPaperColor
   */
  public Color getPaperColor()
  {
    return paperColor;
  }

  /**
   * The paperimage is an image file that represents the stock color and
   * pre-printed markings on the page.  Lacking a paperimage file, we assume
   * that the page is white with no printed markings.
   * @param newPaperImage - fully qualified filename of JPEG image file
   */
  public void setPaperimage(String newPaperImage)
  {
    paperImage = newPaperImage;
  }

  /**
   * Retrieves the paperimage filespec
   * @return String containing the fully qualified filename of the paperimage
   * file, or <code>null</code>
   * @see #setPaperimage
   */
  public String getPaperimage()
  {
    return paperImage;
  }

  /**
   * newPaperSize should be one of the valid paper size strings
   * @param newPaperSize - supported paper size string
   */
  public void setPaperSize(String newPaperSize)
  {
    paperSize = newPaperSize;
  }

  /**
   * Retrieves the paperSize
   * @return String containing the paper size
   * @see #setPaperSize
   */
  public String getPaperSize()
  {
    return paperSize;
  }

  /**
   * newViewOrientation should be one of the valid orientation strings
   * @param newViewOrientation - supported view orientation string
   */
  public void setViewOrientation(String newViewOrientation)
  {
    viewOrientation = newViewOrientation;
  }

  /**
   * Retrieves the viewOrientation
   * @return String containing the view orientation
   * @see #setViewOrientation
   */
  public String getViewOrientation()
  {
    return viewOrientation;
  }

  public boolean isLandscape()
  {
  	return getViewOrientation().toLowerCase().indexOf("landscape") > -1;
  }

  /**
   * Sets the horizontal tile count for multiple page projects
   * @param newTileCountX - the desired horizontal tile count
   */
  public void setTileCountX(Integer newTileCountX)
  {
    tileCountX = newTileCountX;
  }

  /**
   * Retrieves the horizontal tile count
   * @return the horizontal tile count
   */
  public Integer getTileCountX()
  {
    return tileCountX;
  }

  /**
   * Sets the vertical tile count for multiple page projects
   * @param newTileCountY - the desired vertical tile count
   */
  public void setTileCountY(Integer newTileCountY)
  {
    tileCountY = newTileCountY;
  }

  /**
   * Retrieves the vertical tile count
   * @return the vertical tile count
   */
  public Integer getTileCountY()
  {
    return tileCountY;
  }

  /**
   * Sets the horizontal tile overlap in twips
   * @param newTileOverlapX - the desired horizontal tile overlap
   */
  public void setTileOverlapX(Double newTileOverlapX)
  {
    tileOverlapX = newTileOverlapX;
  }

  /**
   * Retrieves the horizontal tile overlap in twips
   * @return the horizontal tile overlap
   */
  public Double getTileOverlapX()
  {
    return tileOverlapX;
  }

  /**
   * Sets the vertical tile overlap in twips
   * @param newTileOverlapY - the desired vertical tile overlap
   */
  public void setTileOverlapY(Double newTileOverlapY)
  {
    tileOverlapY = newTileOverlapY;
  }

  /**
   * Retrieves the vertical tile overlap in twips
   * @return the vertical tile overlap
   */
  public Double getTileOverlapY()
  {
    return tileOverlapY;
  }
  
  boolean isTiledProject()
  {
  	return (tileCountX.intValue()) > 1 && (tileCountY.intValue() > 1);
  }
  
  /**
   * Called when the orientation of a masterpanel is about to change.
   * This rotates the panel instances on the page by 90 degrees.
   * In tiled projects (multi-page signs), the page is rotated instead.
   * @param masterID - ID of the masterpanel
   * @param clockwise - direction of rotation
   */
  void reorientPanels(String masterID, boolean clockwise)
  {
  	if (isTiledProject())
  	{
  		// For tiled projects, we rotate the page instead of the panel.
  		// Assumption: there is only one panel, and it is bigger than the page.
  		
  		// swap page width and height
  		double oldWidth = getWidth().doubleValue();
  		setWidth(getHeight());
  		setHeight(new Double(oldWidth));
  		
  		// swap tile overlaps
  		double oldOverlapX = getTileOverlapX().doubleValue();
  		setTileOverlapX(getTileOverlapY());
  		setTileOverlapY(new Double(oldOverlapX));
  		
  		// swap tile counts
  		int oldCountX = getTileCountX().intValue();
  		setTileCountX(getTileCountY());
  		setTileCountY(new Integer(oldCountX));
  		
  		// swap x and y of the panel position
  		AveryPanel panel = (AveryPanel)getPanels().get(0);
  		double oldX = panel.getPosition().getX();
  		double oldY = panel.getPosition().getY();
  		panel.getPosition().setLocation(oldY, oldX);
  	}
  	else
  	{   
      if (rotatePanels90(masterID, clockwise) == panels.size() && viewOrientation != null)
      {
    	// correct page grid layout 
    	AveryGridLayout layout = getGridLayout(masterID);
    	
        // if there are no other kinds of panels, modify the view orientation
        if (viewOrientation.equals("portrait"))
        {
          // it has to be the opposite of the panel reorientation
          viewOrientation = clockwise ? "landscape-ccw" : "landscape-cw";  
          layout.setReorient(true);
        }
        else if (viewOrientation.startsWith("landscape"))
        {
          viewOrientation = "portrait";
          layout.setReorient(false);
        }
        else if (viewOrientation.equals("preferPortrait"))
        {
          // it has to be the opposite of the panel reorientation
          viewOrientation = clockwise ? "preferLandscape-ccw" : "preferLandscape-cw";          
          layout.setReorient(true);
        }
        else if (viewOrientation.startsWith("preferLandscape"))
        {
          viewOrientation = clockwise ? "portrait" : "preferPortrait";          
          layout.setReorient(false);
        }
      }
  	}
  }

  /**
   * Adds this AveryPage (including all of its AveryPanels) to a PDF document
   * @param pdfDoc - the document under construction
 * @param nudgeX - X offset in TWIPs
 * @param nudgeY - Y offset in TWIPS
 * @param masterpanels - a List of AveryMasterpanels which may be connected
   * to the AveryPanels on this page
 * @param imageCache - collection prevents image redundancy in the PDF file
 * @param outlinePanels - true means draw outline around panels in pdf
 * @param outlineFields - true means draw outline around panel fields in pdf
   * @throws Exception if an AveryPanel doesn't have an AveryMasterpanel to
   * get its basic geometry from
   */
  void addToPDF(PDF pdfDoc, float nudgeX, float nudgeY, List masterpanels, Hashtable imageCache, boolean outlinePanels, boolean outlineFields)
    throws Exception
  {
  	boolean cutlines = false;
  	
  	boolean bSuperSize = false;
  	Double trueWidth = null;
  	Double trueHeight = null;
  	Point truePanelPosition = null;
    
    // convert page size to super page size for projects like laptops that are
    // DIFY and the project outline is cut by a machine
    if (((AveryMasterpanel)masterpanels.get(0)).isPrintOutline())
    {
    	// A3 page size increased to SRA3 (320x450 mm, larger than A3 297x420 mm ) page size
    	if (width.floatValue() == 16837.7954f && height.floatValue() == 23811.0238f)
	    {
    		// change page size and panel location while make PDF.
    		bSuperSize = true;
      	trueWidth = width;
      	trueHeight = height;
      	width = new Double(18140);
      	height = new Double(25500);
      	
      	AveryPanel ap = (AveryPanel)(panels.get(0));
      	truePanelPosition = ap.getPosition();
      	int newPosX = truePanelPosition.x + (18140 - trueWidth.intValue()) / 2;
      	int newPosY = truePanelPosition.y + (25500 - trueHeight.intValue()) / 2;
      	Point superPosition = new Point(newPosX, newPosY);
      	ap.setPosition(superPosition);
	    }
    	cutlines = true;
    }
    
    // bfo page size specified in points
    int pageWidth = (int)(width.floatValue() / 20.0f);
    int pageHeight = (int)(height.floatValue() / 20.0f);
    	
    PDFPage pdfPage = pdfDoc.newPage(pageWidth, pageHeight);
    
		// Onyx now requires page registration outline
    if (cutlines)
    {
    	// draw registration marks relative to page before applying nudge
    	drawPageOutlineRegistration(pdfPage, pageWidth, pageHeight);
    }
    
    // only transform if have nudge or mirror or print180
    if (nudgeX != 0 || nudgeY != 0 || mirrorPrint || print180)
    {
      if (print180) 	// some projects are bizarre this way
      {
      	if (mirrorPrint)	// this doesn't occur in the real world
      	{
      		nudgeY -= height.floatValue();
      	
      		pdfPage.rawWrite(AveryPanelField.formatConcat(
      				1, 0, 0, -1, nudgeX / 20f, -(nudgeY / 20f)));
      	}
      	else  // the normal print180 case
      	{
      		nudgeY -= height.floatValue();
      		nudgeX += width.floatValue();
      	
      		pdfPage.rawWrite(AveryPanelField.formatConcat(
      				-1, 0, 0, -1, nudgeX / 20f, -(nudgeY / 20f)));
      	}
      }
      else if (mirrorPrint) // account for mirroring (t-shirt transfers, etc)
      {
        // adjust mirror transform
        nudgeX += width.floatValue();
        
      	pdfPage.rawWrite(AveryPanelField.formatConcat(
        		-1, 0, 0, 1, nudgeX / 20f, -(nudgeY / 20f)));
      } 
      else // just nudge
      {
      	pdfPage.rawWrite(AveryPanelField.formatConcat(
      		1, 0, 0, 1,	nudgeX / 20f, -(nudgeY / 20f)));
      }
    }
    
		// ask panels to render themselves
	  Iterator iterator = panels.iterator();
	  while (iterator.hasNext())
	  {
	    AveryPanel panel = (AveryPanel)iterator.next();
	    AveryMasterpanel master = panel.findMasterpanel(masterpanels);
	    // must use PDF page height and convert it back to twips to keep last row of panels from
	    // running off the bottom of the page BKN 12/13/11
	    panel.addToPDF(pdfPage, new Double(pageHeight * 20), master, imageCache, outlinePanels, outlineFields);
	  }
	  
	  if (bSuperSize)
	  {
	  	// restore page size and panel location
    	width = trueWidth;
    	height = trueHeight;
    	
    	AveryPanel ap = (AveryPanel)(panels.get(0));
    	ap.setPosition(truePanelPosition);	  	
	  }
	}

  private void drawPageOutlineRegistration(PDFPage pdfPage, int pageWidth, int pageHeight)
  {
		try
		{
	    Color fallback = CMYKColorSpace.getColor(
	    		SPOT_COLOR_PAGE[0],
	    		SPOT_COLOR_PAGE[1],
	    		SPOT_COLOR_PAGE[2],
	    		SPOT_COLOR_PAGE[3]);
			SpotColorSpace magentaInk = new SpotColorSpace("CutContour_thru", fallback);
			Color magenta = magentaInk.getColor(1);
	  
		  PDFStyle style = new PDFStyle();
		  style.setLineColor(magenta);
		  style.setLineWeighting(1.0f);
		  pdfPage.setStyle(style);
		  
		  pdfPage.pathMove(PDF_PAGE_OUTLINE_INSET, PDF_PAGE_OUTLINE_INSET);
		  pdfPage.pathLine(pageWidth - PDF_PAGE_OUTLINE_INSET, PDF_PAGE_OUTLINE_INSET);
		  pdfPage.pathLine(pageWidth - PDF_PAGE_OUTLINE_INSET, pageHeight - PDF_PAGE_OUTLINE_INSET);  	
		  pdfPage.pathLine(PDF_PAGE_OUTLINE_INSET, pageHeight - PDF_PAGE_OUTLINE_INSET);  	
		  pdfPage.pathLine(PDF_PAGE_OUTLINE_INSET, PDF_PAGE_OUTLINE_INSET);
		  pdfPage.pathPaint();
		}
		catch (Exception e)
		{
			System.err.println("Unable to create registration marks in PDFPage drawPageOutlineRegistration");
		} 	
  }
  
	private void drawRegistrationMarks(PDFPage pdfPage, int pageWidth, int pageHeight)
	{
		try
		{
	    // Draw 90 degree 'angle iron' registration marks in four page corners using SPOT_COLOR
	    Color fallback = CMYKColorSpace.getColor(
	    		SPOT_COLOR_PAGE[0],
	    		SPOT_COLOR_PAGE[1],
	    		SPOT_COLOR_PAGE[2],
	    		SPOT_COLOR_PAGE[3]);
			SpotColorSpace magentaInk = new SpotColorSpace("CutContour_thru", fallback);
			Color magenta = magentaInk.getColor(1);
	  
		  PDFStyle style = new PDFStyle();
		  style.setLineColor(magenta);
		  style.setLineWeighting(1.0f);
		  pdfPage.setStyle(style);
		  
		  int startX = 20;
		  int endX = 40;
		  int startY = 40;
		  int endY = 20;
		  
		  pdfPage.pathMove(startX, startY);
		  pdfPage.pathLine(endX, startY);
		  pdfPage.pathLine(endX, endY);  	
		  
		  pdfPage.pathMove(pageWidth - startX, startY);
		  pdfPage.pathLine(pageWidth - endX, startY);
		  pdfPage.pathLine(pageWidth - endX, endY);  	
		  
		  pdfPage.pathMove(pageWidth - startX, pageHeight - startY);
		  pdfPage.pathLine(pageWidth - endX, pageHeight - startY);
		  pdfPage.pathLine(pageWidth - endX, pageHeight - endY);  	
		  
		  pdfPage.pathMove(startX, pageHeight - startY);
		  pdfPage.pathLine(endX, pageHeight - startY);
		  pdfPage.pathLine(endX, pageHeight - endY);  	
		  
		  pdfPage.pathPaint();
		}
		catch (Exception e)
		{
			System.err.println("Unable to create registration marks in PDFPage drawRegistrationMarks");
		}
	}
  
  /**
   * Finds a panel on the page
   * @param description unique to the panel
   * @return the panel found
   * @throws Exception if panel was not found
   */
  AverySuperpanel getPanel(String description)
    throws Exception
  {
    Iterator iterator = panels.iterator();
    while (iterator.hasNext())
    {
      AverySuperpanel panel = (AverySuperpanel)iterator.next();
      if (panel.getDescription().equals(description))
      {
        return panel;
      }
    }
    throw new Exception("No panel found matching description: " + description);
  }

  /**
   * Sets the masterpanel field in all of the panels,
   * according to the the panel's MasterID.
   * Also sets the ordinal numbers of the panels to reflect their
   * masterpanel instance number
   * @param masterpanels - the set masterpanels to draw from
   */
  public void resolveMasterpanels(java.util.List masterpanels)
  {
  	// first, let's make a Hashtable to hold the number of references
  	// to each masterpanel on this page
  	Hashtable mpCounts = new Hashtable(masterpanels.size());
  	Iterator mp = masterpanels.iterator();
  	while (mp.hasNext())
  	{
  		mpCounts.put(mp.next(), new Integer(0));
  	}

    Iterator iterator = getPanels().iterator();
    while (iterator.hasNext())
    {
      try
      {
      	AveryPanel panel = (AveryPanel)iterator.next();
        panel.findMasterpanel(masterpanels);

        // set panel number
        Object o = panel.getMasterpanel();
        int number = ((Integer)mpCounts.get(o)).intValue() + 1;
        panel.setNumber(number);
        // update masterpanel count
        mpCounts.put(o, new Integer(number));
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
    }

    // update ofHowMany in each panel with the final masterpanel count
    Iterator p = getPanels().iterator();
    while (p.hasNext())
    {
    	AveryPanel panel = (AveryPanel)p.next();
      Integer mpCount = (Integer)mpCounts.get(panel.getMasterpanel());
      panel.setOfHowMany(mpCount.intValue());
    }
  }

  List getMasterpanels()
  {
  	HashMap map = new HashMap();
  	
  	Iterator i = panels.iterator();
  	while (i.hasNext())
  	{
  		AveryPanel panel = (AveryPanel)i.next();
  		AveryMasterpanel master = panel.getMasterpanel();
  		String key = master.getID();
  		if (!map.containsKey(key))
  			map.put(key, master);
  	}
  	List valueList = new ArrayList();
  	valueList.addAll(map.values());
  	
  	return valueList;
  }
  
  /**
   * initializes Panel Data Linking on the page
   */
  public void intializePanelDataLinking(ProjectFieldTable fieldTable)
  {
    Iterator iterator = getPanels().iterator();
    AveryPanel previousPanel = null;
    while (iterator.hasNext())
    {
      try
      {
        AveryPanel currentPanel = (AveryPanel)iterator.next();
        if (currentPanel.intializePanelDataLinking(fieldTable) &&
            previousPanel != null)
        {
          if (currentPanel.getMasterID().equals(previousPanel.getMasterID()))
          {
            // sets content and style ids links to direct superior fields
          	// TODO: Disable this when ready to release build that changes
          	// PDL to DPO 7 links
            currentPanel.resolveLinks(previousPanel);
          }
        }
        previousPanel = currentPanel;
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
    }
  }

 /**
   * finds the panel that the field with given field id belongs to
   * @param contentID the panel field content id to return
   * @return the AveryPanelField content object matching content id or null
   */
  protected AverySuperpanel findPanel(String refID)
  {
    AverySuperpanel superPanel = null;
    // check all panels
    Iterator iterator = getPanels().iterator();
    while (iterator.hasNext())
    {
      AveryPanel panel = (AveryPanel)iterator.next();
      superPanel = panel.findPanel(refID);
      if (superPanel != null)
        break;
    }
    return superPanel;
  }

 /**
   * if field does not have a top superior field discard them
   * @param fieldTable the project field table
   */
  protected void discardPanelFields(ProjectFieldTable fieldTable)
  {
    // check all page panels
    Iterator panels = getPanels().iterator();
    while (panels.hasNext())
    {
      AveryPanel panel = (AveryPanel)panels.next();
      panel.discardPanelFields(fieldTable);
    }
  }

  /**
   * This method walks through the panels, deleting fields that match the given
   * field in Z-order.  It is used to delete fields that have been superceded
   * by an updated Masterpanel, hence the name.
   * @param field - the field to match.
   * @param flags - tells what has changed in the masterpanel
   * @return the number of field modifications made on the page
   */
  int masterUberAlles(AveryPanelField field, java.util.BitSet flags)
  {
    int modified = 0;
    Iterator iterator = getPanels().iterator();
    while (iterator.hasNext())
    {
      modified += ((AveryPanel)iterator.next()).masterUberAlles(field, flags);
    }

    return modified;
  }

  /**
   * This method returns a list of <b>all</b> of the AveryPanelFields on the
   * page.  Note that there is no indication in the fields of where they
   * come from.  Use with caution!
   * @return a list of AveryPanelFields
   */
  List getAllPanelFields()
  {
    List list = new ArrayList();
    Iterator i = getPanels().iterator();
    while (i.hasNext())
    {
      list.addAll(((AverySuperpanel)(i.next())).getPanelFields());
    }
    return list;
  }

  /**
   * This method constructs a list of all of the fully qualified names of all
   * files associated with this page.
   * @return list of filenames as strings, or <code>null</code>
   */
  List getFileList()
  {
    List list = null;

    // ask each panel for its files
    Iterator i = getPanels().iterator();
    while (i.hasNext())
    {
      java.util.List filelist = ((AveryPanel)i.next()).getFileList();
      if (filelist != null)
      {
        if (list == null)
        {
          list = new ArrayList();
        }
        list.addAll(filelist);
      }
    }

    // add the paperimage file, if it exists
    /* this caused problems in AverySaveServlet
    if (getPaperimage() != null)
    {
      if (list == null)
      {
        list = new ArrayList();
      }
      list.add(getPaperimage());
    }
    */

    return list;
  }

  /**
   * Counts the number of panels on this page that use the given masterpanel
   * @param master - the masterpanel to test for
   * @returns the number of panels using the given AveryMasterpanel
   */
  int countPanelsUsingMasterpanel(AveryMasterpanel master)
  {
    int total = 0;

    Iterator i = getPanels().iterator();
    while (i.hasNext())
    {
      if (((AveryPanel)i.next()).getMasterID().equals(master.getID()))
      {
        ++total;
      }
    }
    return total;
  }

  /**
   * Creates a BufferedImage containing a full page preview of the current
   * page. The image is set to a specified width, and contains all of the page's
   * panels in the correct positions and orientations.
   * @param width - the width of the full page preview in the BufferedImage.
   * @param masters - list of masterpanels in the project
   * @param upright180 - if <code>true</code>, 180 degree panels will be rotated
   * to an upright position, per the AP4 spec.
   * @return the BufferedImage containing the full page preview for the current
   * page, set at the specified width.
   * @throws Exception
   */
  BufferedImage makePreview(int width, List masters, boolean upright180)
    throws Exception
  {
  	double dScalar;
  	int height;

  	if (isLandscape())
  	{
  		dScalar = (double)width / this.getHeight().doubleValue();
  		// scale the view porportionally to the width specified by the caller
  		height = (int)(dScalar * this.getWidth().doubleValue());
  	}
  	else
  	{
  		dScalar = (double)width / this.getWidth().doubleValue();
  		// scale the view porportionally to the width specified by the caller
  		height = (int)(dScalar * this.getHeight().doubleValue());
  	}

    BufferedImage previewImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = previewImage.createGraphics();

    // apply landscape orientation to Graphics2D if required
    if (isLandscape())
    {
    	if (getViewOrientation().indexOf("ccw") > -1)
    	{			// landscape counter clockwise
        graphics.rotate(-0.5 * Math.PI);
        graphics.translate(-height, 0);
    	}
    	else // landscape clockwise
    	{
        graphics.rotate(0.5 * Math.PI);
        graphics.translate(0, -width);
     	}
    }

    int pageWidth = (int)(dScalar * this.getWidth().doubleValue());
    int pageHeight = (int)(dScalar * this.getHeight().doubleValue());

    if (getPaperimage() == null)
    {
      // paint background with the papercolor or white
      graphics.setBackground(getPaperColor() == null ? Color.white : getPaperColor());
      graphics.clearRect(0, 0, pageWidth, pageHeight);
    }
    else // paperimage != null)
    {
      try
      {
        BufferedImage background =
          com.sun.image.codec.jpeg.JPEGCodec.createJPEGDecoder(
            new java.io.FileInputStream(getPaperimage())).decodeAsBufferedImage();
        graphics.drawImage(background, 0, 0, pageWidth, pageHeight, null);
      }
      catch (Exception e) // paperimage failed, fill with paperColor instead
      {
        graphics.setBackground(getPaperColor() == null ? Color.white : getPaperColor());
        graphics.clearRect(0, 0, pageWidth, pageHeight);
      }
    }


    // draw the page panels
    drawPanels(graphics, dScalar, masters, upright180);
    graphics.dispose();

    return previewImage;
  }

  void drawPanels(Graphics2D graphics, double dScalar, List masters, boolean upright180)
  throws Exception
  {
    // Walk the panels array to get an image for each panel, then
    // add the panel image to the page image.
    Iterator i = this.getPanels().iterator();
    while (i.hasNext())
    {
      AveryPanel panel = (AveryPanel)i.next();
      BufferedImage panelImage = this.getPanelImage(panel, dScalar, masters);

      int rotation = panel.getRotation().intValue();
      if (rotation == 0)
      {
        int panelx = (int)(dScalar * panel.getPosition().getX());
        int panely = (int)(dScalar * panel.getPosition().getY());
        graphics.drawImage(panelImage, panelx, panely, panelImage.getWidth(), panelImage.getHeight(), null);
      }
      else if (rotation == 180 && upright180) // per AP4 spec, 180 degree rotated panels are rendered upright
      {
        // position must be adjusted to accomodate 180 rotation
        int panelx = (int)(dScalar * panel.getPosition().getX()) - panelImage.getWidth();
        int panely = (int)(dScalar * panel.getPosition().getY()) - panelImage.getHeight();
        graphics.drawImage(panelImage, panelx, panely, panelImage.getWidth(), panelImage.getHeight(), null);
      }
      else  // rotate the Graphics2D space before drawing the image.
      {
        double theta = panel.getRotation().doubleValue() * (Math.PI / 180.0);
        double x = dScalar * panel.getPosition().getX();
        double y = dScalar * panel.getPosition().getY();

        graphics.translate(x, y);
        graphics.rotate(-theta);
        graphics.drawImage(panelImage, 0, 0, panelImage.getWidth(), panelImage.getHeight(), null);
        graphics.rotate(theta);
        graphics.translate(-x, -y);
      }
    }
  }

  /**
   * gets image of a panel
   * @param panel - the panel to render
   * @param dScalar - the scale-down factor for sizing
   * @param masters - List of all masterpanels in the project
   * @return a BufferedImage of the correct size
   * @throws Exception if masterpanel couldn't be found
   */
  private BufferedImage getPanelImage(AveryPanel panel, double dScalar, List masters)
    throws Exception
  {
    if ( panel == null )
      return null;

    AveryMasterpanel master = panel.findMasterpanel(masters);
    int width = (int)(dScalar * panel.getWidth().doubleValue());
    int height = (int)(dScalar * panel.getHeight().doubleValue());

    // create a transparent BufferedImage
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = image.createGraphics();

    // all additional drawing is initiated by the panel
    AveryMasterpanel.panelStrokeWidth = 2;
    master.setNoDrawSafeBleed(true);
    panel.draw(graphics, width, height, master);
    //master.setNoDrawSafeBleed(false);
    graphics.dispose();
    return image;
  }
  
  /**
   * Creates a "wireframe" image - a white picture with black panel outlines
   * @param width - target width
   * @param masters - array of AveryMasterpanel objects
   * @return an image of TYPE_INT_RGB 
   */
  public BufferedImage makeWireframeImage(int width, List masters)
  {
  	double scale = (double)width / this.getWidth().doubleValue();
  	int height = (int)(scale * this.getHeight().doubleValue());

    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = image.createGraphics();
  	graphics.setColor(Color.white);
    graphics.fillRect(0, 0, width, height);
  	graphics.setColor(Color.black);
  	
    Iterator i = this.getPanels().iterator();
    while (i.hasNext())
    {
      AveryPanel panel = (AveryPanel)i.next();
      AveryMasterpanel master;
      try {
      	master = panel.findMasterpanel(masters);
      }
      catch (Exception e) {
      	continue;
      }
      // translate to x, y
      double x = scale * panel.getPosition().getX();
      double y = scale * panel.getPosition().getY();
      graphics.translate(x, y);
      
      if (panel.getRotation().intValue() == 0)
      {
      	// draw the wireframe
        master.drawCutouts(graphics, scale);
        master.drawShape(graphics, scale);
      }
      else  // rotate the Graphics2D space before drawing the wireframe.
      {
        double theta = panel.getRotation().doubleValue() * (Math.PI / 180.0);
        graphics.rotate(-theta);
        master.drawCutouts(graphics, scale);
        AveryMasterpanel.panelStrokeWidth = 2;
        master.drawShape(graphics, scale);
        graphics.rotate(theta);
      }
      // translate back to 0, 0
      graphics.translate(-x, -y);
    }
    return image;
  }

  /**
   * Creates an AveryPanel for the specified panel number.
   * @param panelNumber - specified page number (0 based).
   * @return the AveryPanel of the specified panel number. Null is returned
   * if specified panel number is out of the range of the panel array.
   */
  public AveryPanel getPanel (int panelNumber)
  {
    AveryPanel panel = null;

    if ( this.getNumPanels() >= panelNumber )
      panel = (AveryPanel)getPanels().get(panelNumber);

    return panel;
  }

  /**
   * Returns total number of panels in the panels array.
   * @return total panels
   */
  public int getNumPanels ()
  {
    return this.getPanels().size();
  }

  /**
   * Generates an AveryPage suitable for a .avery Zip file bundle.  Images
   * are given new, unique names starting with <code>prefix</code>, and are
   * copied to the HighRes diretory under <code>location<code>
   * @param prefix - typically the machine name
   * @param location - where the project XML will ultimately be created
   * @param images - tracks the images already processed
   * @return a page, with image paths deleted, suitable for a .avery bundled project
   */
  public AveryPage getPageForDotAveryBundle(String prefix, File location, Hashtable images)
  {
    AveryPage page = new AveryPage();
    page.setHeight(this.getHeight());
    page.setMirror(this.getMirror());
    // page.setPaperimage(this.getPaperimage());
    page.setWidth(this.getWidth());
    page.description = this.getDescriptionObject();
    
    page.cutouts = cutouts;
    page.doubleSided = doubleSided;
    page.gridLayouts = gridLayouts;
    page.hints = hints;
    page.guides = guides;
    page.paperColor = paperColor;
    page.paperSize = paperSize;
    page.print180 = print180;
    page.tileCountX = tileCountX;
    page.tileCountY = tileCountY;
    page.tileOverlapX = tileOverlapX;
    page.tileOverlapY = tileOverlapY;
    page.viewOrientation = viewOrientation;

    Iterator iterator = getPanels().iterator();
    while (iterator.hasNext())
    {
      AveryPanel panel = (AveryPanel)iterator.next();
      page.addPanel(panel.getPagePanelForDotAveryBundle(prefix, location, images));
    }

    return page;
  }

  /**
   * Gives all of the AveryImage fields on the page access to a common Image cache
   * @param list to hold AveryImage objects
   */
  void initializeImageCache(ArrayList list)
  {
    Iterator iterator = this.getPanels().iterator();
    while (iterator.hasNext())
    {
      ((AverySuperpanel)iterator.next()).initializeImageCache(list);
    }
  }

  /**
   * calculates the aspect ratio of the page in preferred view orientation
   * @return width divided by height
   */
  public double getAspect()
  {
  	if (isLandscape())
  	{
      return getHeight().doubleValue() / getWidth().doubleValue();
  	}
    else return getWidth().doubleValue() / getHeight().doubleValue();
  }

  /**
   * Get a representation of the object in the Avery-specific form
   * @return an Avery-specific Element
   */
  public org.jdom.Element getAveryElement()
  {
    Element element = new Element("Avery.page");

    element.setAttribute("width", getWidth().toString());
    element.setAttribute("height", getHeight().toString());

    if (getMirror())
    {
      element.setAttribute("mirror", "on");
    }

    // add the paperimage if it exists
    if (getPaperimage() != null)
    {
      Element paperElement  = new Element("Avery.paperimage");
      paperElement.setAttribute("src", getPaperimage());
      element.addContent(paperElement);
    }

    // add all of the panels
    Iterator iterator = this.getPanels().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((AveryPanel)iterator.next()).getAveryElement());
    }

    // Note that the XML spec allows an optional array of AveryCutouts here,
    // but we've never added them to the AveryPage object.

    return element;
  }

  void removeAllPanelFields()
  {
    Iterator iterator = this.getPanels().iterator();
    while (iterator.hasNext())
    {
      ((AveryPanel)iterator.next()).removeAllPanelFields();
    }
  }
  
  void removePanelsMatchingMasterID(String masterID)
  {
  	List pagePanels = getPanels();
  	List newPanels = new ArrayList();
  	Iterator i = pagePanels.iterator();
  	while (i.hasNext())
  	{
  		AveryPanel panel = (AveryPanel)i.next();
  		if (!panel.getMasterID().equals(masterID))
  		{
  			newPanels.add(panel);
  		}
  		else
  		{
  			// get rid of matching gridlayout
  			removeGridLayouts(panel.getMasterID());
  		}
  	}
  	setPanels(newPanels);
  }

  /**
   * rotates panels 90 degrees and adjusts their positions so that the page
   * geometry remains the same.  This is typically called when the masterpanel
   * orientation changes.
   * @param masterID - only panels that reference a specific masterpanel are rotated
   * @param clockwise - if true, clockwise, if false, counter-clocwise
   */
  private int rotatePanels90(String masterID, boolean clockwise)
  {
    int panelsRotated = 0;
    Iterator iterator = this.getPanels().iterator();
    while (iterator.hasNext())
    {
      AveryPanel panel = (AveryPanel)iterator.next();
      if (panel.getMasterID().equals(masterID))
      {
        panel.reorient90(clockwise);
        ++panelsRotated;
      }
    }
    return panelsRotated;
  }

	/* (non-Javadoc)
	 * @see com.avery.miwok.Averysoft#getAverysoftElement()
	 */
	public Element getAverysoftElement()
	{
		Element element = new Element("page");
		element.setNamespace(AveryProject.getAverysoftNamespace());

		element.setAttribute("width", getWidth().toString());
		element.setAttribute("height", getHeight().toString());
		
		element.setAttribute("paperSize", getPaperSize());
		element.setAttribute("viewOrientation", getViewOrientation());

    // optional attributes
    if (getDoubleSided())
    {
      element.setAttribute("doubleSided", "true");
    }
    if (getMirror())
    {
      element.setAttribute("mirrorPrint", "true");
    }
    
    // paperColor is not to be written nor is paperImage by Predesigner
    /*if ((getPaperColor() != null) && (getPaperColor().equals(Color.white) == false))
    {
      element.setAttribute("paperColor",
        "0x" + Integer.toHexString(getPaperColor().getRGB()).substring(2));
    }

    if (paperImage != null)
    {
    	element.setAttribute("paperImage", getPaperimage());
    }*/
    
    if (print180)
    {
    	element.setAttribute("print180", "true");
    }

		if (description != null)
		{
			element.addContent(description.getAverysoftElement());
		}

		if (hints != null && !hints.isEmpty())
		{
			java.util.Enumeration e = hints.keys();
			while (e.hasMoreElements())
			{
				Hint hint = (Hint)hints.get(e.nextElement());
				element.addContent(hint.getAverysoftElement());
			}
		}

		// output the cutouts
		Iterator iterator = getCutouts().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}

		if (guides != null)
		{
			iterator = guides.iterator();
			while (iterator.hasNext())
			{
				element.addContent(((Guide)iterator.next()).getXmlElement());
			}
		}

	    // output the page tileAttributes
	    if (tileCountX.intValue() > 0 && tileCountY.intValue() > 0)
	    {
			  Element tileAttributes = new Element("tileAttributes");
			  tileAttributes.setNamespace(AveryProject.getAverysoftNamespace());
			  tileAttributes.setAttribute("tileCountX", getTileCountX().toString());
			  tileAttributes.setAttribute("tileCountY", getTileCountY().toString());
			  tileAttributes.setAttribute("tileOverlapX", getTileOverlapX().toString());
			  tileAttributes.setAttribute("tileOverlapY", getTileOverlapY().toString());
	
	      element.addContent(tileAttributes);
	    }
	    
	    if (gridLayouts != null)
	    {
	    	iterator = gridLayouts.iterator();
	    	while (iterator.hasNext())
	    	{
				element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
	    	}
	    }

		iterator = getPanels().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}

		return element;
	}

	/* (non-Javadoc)
	 * @see com.avery.miwok.Averysoft#getAverysoftElement()
	 */
	public Element getAverysoftIBElement()
	{
		Element element = new Element("page");
		element.setNamespace(AveryProject.getAverysoftNamespace());
		
		element.setAttribute("width", AveryProject.twipsToMM(getWidth().doubleValue()));
		element.setAttribute("height", AveryProject.twipsToMM(getHeight().doubleValue()));
		
		/*element.setAttribute("paperSize", getPaperSize());
		element.setAttribute("viewOrientation", getViewOrientation());

    // optional attributes
    if (getDoubleSided())
    {
      element.setAttribute("doubleSided", "true");
    }
    if (getMirror())
    {
      element.setAttribute("mirrorPrint", "true");
    }*/
    
    // paperColor is not to be written nor is paperImage by Predesigner
    /*if ((getPaperColor() != null) && (getPaperColor().equals(Color.white) == false))
    {
      element.setAttribute("paperColor",
        "0x" + Integer.toHexString(getPaperColor().getRGB()).substring(2));
    }

    if (paperImage != null)
    {
    	element.setAttribute("paperImage", getPaperimage());
    }*/
    
    /*if (print180)
    {
    	element.setAttribute("print180", "true");
    }

		if (description != null)
		{
			element.addContent(description.getAverysoftElement());
		}

		if (hints != null && !hints.isEmpty())
		{
			java.util.Enumeration e = hints.keys();
			while (e.hasMoreElements())
			{
				Hint hint = (Hint)hints.get(e.nextElement());
				element.addContent(hint.getAverysoftElement());
			}
		}

		// output the cutouts
		Iterator iterator = getCutouts().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}

		if (guides != null)
		{
			iterator = guides.iterator();
			while (iterator.hasNext())
			{
				element.addContent(((Guide)iterator.next()).getXmlIBElement());
			}
		}

	    // output the page tileAttributes
	    if (tileCountX.intValue() > 0 && tileCountY.intValue() > 0)
	    {
			  Element tileAttributes = new Element("tileAttributes");
			  tileAttributes.setNamespace(AveryProject.getAverysoftNamespace());
			  tileAttributes.setAttribute("tileCountX", getTileCountX().toString());
			  tileAttributes.setAttribute("tileCountY", getTileCountY().toString());
			  tileAttributes.setAttribute("tileOverlapX", getTileOverlapX().toString());
			  tileAttributes.setAttribute("tileOverlapY", getTileOverlapY().toString());
	
	      element.addContent(tileAttributes);
	    }
	    
	    if (gridLayouts != null)
	    {
	    	iterator = gridLayouts.iterator();
	    	while (iterator.hasNext())
	    	{
				element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
	    	}
	    }*/

		Iterator iterator = getPanels().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftIBElement());
		}

		return element;
	}
	
	/* (non-Javadoc)
	 * @see com.avery.miwok.Averysoft#getBlankAveryTemplateElement()
	 *//*
	public Element getBlankAveryTemplateElement()
	{
		Element element = new Element("page");
		element.setNamespace(AveryProject.getAverysoftNamespace());
		element.setAttribute("width", getWidth().toString());
		element.setAttribute("height", getHeight().toString());
		element.setAttribute("paperSize", getPaperSize());
		element.setAttribute("viewOrientation", getViewOrientation());

		if (description != null)
		{
			element.addContent(description.getBlankAveryTemplateElement());
		}

		if (hints != null && !hints.isEmpty())
		{
			java.util.Enumeration enum = hints.keys();
			while (enum.hasMoreElements())
			{
				Hint hint = (Hint)hints.get(enum.nextElement());
				element.addContent(hint.getAverysoftElement());
			}
		}

		// output the cutouts
		Iterator iterator = getCutouts().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getBlankAveryTemplateElement());
		}

		if (guides != null)
		{
			iterator = guides.iterator();
			while (iterator.hasNext())
			{
				element.addContent(((Guide)iterator.next()).getXmlElement());
			}
		}

    // output the page tileAttributes
    if (tileCountX.intValue() > 0 && tileCountY.intValue() > 0)
    {
		  Element tileAttributes = new Element("tileAttributes");
		  tileAttributes.setNamespace(AveryProject.getAverysoftNamespace());
		  tileAttributes.setAttribute("tileCountX", getTileCountX().toString());
		  tileAttributes.setAttribute("tileCountY", getTileCountY().toString());
		  tileAttributes.setAttribute("tileOverlapX", getTileOverlapX().toString());
		  tileAttributes.setAttribute("tileOverlapY", getTileOverlapY().toString());

      element.addContent(tileAttributes);
    }

		iterator = getPanels().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoft)iterator.next()).getBlankAveryTemplateElement());
		}

		return element;
	}*/

	/**
	 * Hints are stored as name/value pairs in averysoft XML,
	 * and as a Hashtable in the AveryPage object.
	 * @param name
	 * @return the named Hint, or <code>null</code> if the Hint does not exist
	 */
	public Hint getHint(String name)
	{
	  if (hints != null)
	  {
		  return (Hint)hints.get(name);
	  }
	  return null;
	}

	/**
	 * This sets a Hint in the AveryPage.  There is validation of neither
	 * name nor value.  The Hint will be written into averysoft XML.
	 * @param name - the name used for the Hint
	 * @param value - the value of the Hint
	 */
	public void addHint(String name, String value)
	{
	  if (hints == null)
	  {
		  hints = new Hashtable();
	  }
	  else if (hints.contains(name))
	  {
		  hints.remove(name);
	  }

	  hints.put(name, new Hint(name, value));
	}

	/**
	 * Use this method to remove a specific Hint.
	 * @param name - the name of the Hint
	 * @return the Hint that was removed,
	 * or <code>null</code> if the Hint didn't exist
	 */
	public Hint removeHint(String name)
	{
		if (hints == null || hints.containsKey(name) == false)
		{
		 return null;
		}
		Hint hint = (Hint)hints.get(name);
		hints.remove(name);
		return hint;
	}

	private List getCutouts()
	{
		if (cutouts == null)
		{
			cutouts = new ArrayList();
		}
		return cutouts;
	}

	private void addCutout(Element element)
	{
		try
		{
			getCutouts().add(new AveryCutout(element));
		}
		catch (Exception e)
		{
			// we're not going to lose sleep over this one
			return;
		}
	}

  void translateDescriptions(String newLanguage)
  {
  	getDescriptionObject().setLanguage(newLanguage);

		Iterator iterator = getPanels().iterator();
		while (iterator.hasNext())
		{
			((AveryPanel)iterator.next()).getDescriptionObject().setLanguage(newLanguage);
		}
  }

  Description getDescriptionObject()
  {
  	return description;
  }

  public String getDescription()
  {
  	return getDescriptionObject().getText();
  }

  void removeAllFieldRefsFromPanels()
  {
		Iterator iterator = getPanels().iterator();
		while (iterator.hasNext())
		{
			((AveryPanel)iterator.next()).getFieldRefs().clear();
		}
  }
  
  public List getGridLayouts()
  {
  	return gridLayouts;
  }
  
  public AveryGridLayout getGridLayout(String masterID)
  {
    if (gridLayouts != null)
    {
      Iterator iterator = gridLayouts.iterator();
      while (iterator.hasNext())
      {
        AveryGridLayout grid = (AveryGridLayout)iterator.next();
        if (grid.master.equals(masterID))
        {
          return grid;
        }
      }
    }
    return null;
  }
  
  public void addGridLayout(AveryGridLayout gridLayout)
  {
  	gridLayouts.add(gridLayout);
  }
  
  public void removeGridLayouts(String masterID)
  {
  	if (masterID == null)
  	{
  		gridLayouts.clear();
  		gridLayouts = null;
  	}
  	else if (gridLayouts != null)
  	{
  		ArrayList newGrids = new ArrayList();
      Iterator iterator = gridLayouts.iterator();
      while (iterator.hasNext())
      {
        AveryGridLayout grid = (AveryGridLayout)iterator.next();
        if (!grid.master.equals(masterID))
        {
          newGrids.add(grid);
        }
      }
      gridLayouts.clear();
      gridLayouts = newGrids;
    }
  		
  }
  
}
