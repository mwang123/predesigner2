/**
 * Title: AveryTextline
 * <p>
 * Description: AveryTextline describes a textfield that is known to contain
 * just one line of text
 * <p>
 * Copyright: Copyright (c)2000 Avery Dennison
 * <p>
 * Company: Avery Dennison
 * <p>
 * 
 * @author Bob Lee
 * @version 1.0
 */

package com.avery.project;

import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Hashtable;
import java.util.List;

import org.faceless.pdf2.LayoutBox;
import org.faceless.pdf2.OpenTypeFont;
import org.faceless.pdf2.PDFCanvas;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFStyle;
import org.faceless.pdf2.StandardFont;
import org.jdom.Element;

import com.avery.utils.AveryUtils;

public class AveryTextline extends AveryTextfield implements Averysoftable, Mergeable
{
  public AveryTextline()
  {
  }

  private Text text = new Text();

  protected AveryTextline(Element element) throws Exception
  {
    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace()) && element.getName().equals("textLine"))
    {
      readTextfieldAverysoftAttributes(element);

      setAllDifferent("true".equals(element.getAttributeValue("allDifferent")));
      
      Element descriptionElement = element.getChild("description", AveryProject.getAverysoftNamespace());
      if (descriptionElement != null)
      {
        setDescriptionObject(Description.makeTextFieldDescription(descriptionElement));
      }

      Element textElement = element.getChild("text", AveryProject.getAverysoftNamespace());
      if (textElement != null)
      {
        text = new Text(textElement);
      }
    }
    else if (!initFromAveryDtdElement(element))
    {
      throw new Exception("Not an Avery Textline element");
    }
  }

  private boolean initFromAveryDtdElement(Element element) throws Exception
  {
    if (element.getName().equals("Avery.pf.textline"))
    {
      setTypeface(element.getAttributeValue("typeface"));
      setPointsize(Double.valueOf(element.getAttributeValue("pointsize")));
      setTextcolor(element.getAttributeValue("textcolor"));
      setJustification(element.getAttributeValue("justification"));
      setVerticalAlignment(element.getAttributeValue("valign"));
      setStyles(element.getAttributeValue("styles"));
      setContent(element.getText());

      readSuperAttributes(element.getParent());

      return true;
    }

    return false;
  }

  /**
   * The matches() method returns <code>true</code> if the given field is an
   * AveryTextline with the same content and AveryTextfield attributes.
   * 
   * @param field
   *          the field to test against
   * @return <code>true</code> if the contents of the field match the contents
   *         of <code>this</code>, <code>false</code> otherwise
   */
  public boolean matches(AveryPanelField field)
  {
    if (field instanceof AveryTextline)
    {
      AveryTextline textline = (AveryTextline) field;
      if (getContent().equals(textline.getContent()))
      {
        return super.matches(field);
      }
    }
    return false;
  }

  /**
   * The differences() method compares this field with another AveryPanelField
   * and returns a BitSet describing how they differ. The bits of the BitSet are
   * defined in the AveryPanelField base class. If the field is an instance of a
   * different class, the APPLES_AND_ORANGES bit is set and the comparison goes
   * no further.
   * 
   * @param field
   * @return a BitSet describing the differenced
   */
  public BitSet differences(AveryPanelField field)
  {
    BitSet flags = super.differences(field);

    if (flags.get(APPLES_AND_ORANGES))
    { // no sense in comparing apples and oranges
      return flags;
    }

    if (getContent().equals(((AveryTextline) field).getContent()) == false)
    {
      flags.set(TEXTCONTENT_DIFFERS);
    }

    return flags;
  }

  /**
   * Creates a deep clone
   * 
   * @return deep clone
   */
  public AveryPanelField deepClone()
  {
    AveryTextline tl = new AveryTextline();
    copyPrivateData(tl);

    tl.setTypeface(getTypeface());
    tl.setPointsize(getPointsize());
    tl.setTextcolor(getTextcolor());
    tl.setStyles(getStyles());
    tl.setJustification(getJustification());
    tl.setVerticalAlignment(getVerticalAlignment());
    tl.setOverflow(getOverflow());
    tl.setTextStyle(getTextStyle());
    tl.setAllDifferent(isAllDifferent());

    tl.setContent(getContent());

    return (AveryPanelField) tl;
  }

  /**
   * This method modifies the field, if it exists, to match recent changes in
   * another field. It is used to update fields that have been made obsolete by
   * an updated Masterpanel (hence the name).
   * 
   * @param field -
   *          the field to pull changes from.
   * @param flags -
   *          describes what attributes to update
   * @return the number of modifications made
   */
  public int masterUberAlles(AveryPanelField field, BitSet flags)
  {
    int modified = super.masterUberAlles(field, flags);

    if (flags.get(TEXTCONTENT_DIFFERS))
    {
      setContent(((AveryTextline) field).getContent());
      ++modified;
    }

    return modified;
  }

  public void setContent(String newContent)
  {
    this.text.setContent(newContent);
  }

  public String getContent()
  {
    return this.text.getContent();
  }

  public void setMergeKey(String mergeKey)
  {
    this.text.setMergeKey(mergeKey);
  }

  /**
   * access to the key for mergeMap operations
   * 
   * @return the key ("af0", "af1", etc.) or an empty string if there is no key.
   */
  public String getMergeKey()
  {
    if (this.text.hasMergeKey())
    {
      return this.text.getMergeKey();
    }
    else
    {
      return "";
    }
  }

  public String dump()
  {
    String str = new String("");
    str += "<P>AveryTextline";
    str += super.dump();
    str += "<br>content = " + getContent();
    return str;
  }

  // implement Draw() as declared in abstract superclass AveryPanelField
  public void draw(Graphics2D gr, double dScalar)
  {
    if (getContent().length() > 0)
    {
      if (getMask() != null)
      {
        getMask().clipImageMask(gr, dScalar);
      }
      drawOutlineElement(gr, dScalar);
      // super.drawText(gr, getContent(), dScalar);
      directDraw(gr, (float) dScalar); // squeezes lines that are too long

      gr.setClip(null);
      if (getMask() != null && getMask().incrementIndex())
      {
        draw(gr, dScalar);
      }
    }
  }

  public void addToPDF(PDFPage pdfPage, Double dPanelHeight, Hashtable imageCache) throws Exception
  {
    addOutlineToPDF(pdfPage, dPanelHeight);
    if (getContent().length() > 0)
    {
      addToPdf(pdfPage, dPanelHeight, 100.0 / stringSizeRatio());
    }
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAveryElement()
  {
    Element element = getParentAveryElement();
    Element content = new Element("Avery.pf.textline");

    // add attributes
    content.setAttribute("typeface", getTypeface());
    content.setAttribute("pointsize", getPointsize().toString());
    content.setAttribute("textcolor", "0x" + Integer.toHexString(getTextcolor().getRGB()).substring(2));
    content.setAttribute("justification", getJustification());
    content.setAttribute("valign", getVerticalAlignment());
    content.setAttribute("styles", getStyles());

    // add the content itself
    content.setText(getContent());

    element.addContent(content);
    return element;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("textLine");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    super.applyTextfieldAverysoftAttributes(element);

    if (isAllDifferent())
    {
    	element.setAttribute("allDifferent", "true");
    }
    
    if (getDescriptionObject() == null)
    {
      setDescriptionObject(Description.makeTextFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
	  position.setAttribute("x", Double.toString(getPosition().getX()));
	  position.setAttribute("y", Double.toString(getPosition().getY()));

    element.addContent(position);

    if (getID().equals(getStyleID()))
    {
      copyStyleAttributes();
      element.addContent(getTextStyle().getAverysoftElement());
    }
    
    if (getID().equals(getContentID()))
    {
      element.addContent(this.text.getAverysoftElement());
    }

    return element;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAverysoftIBElement()
  {
    Element element = new Element("textLine");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    super.applyTextfieldAverysoftAttributes(element);

    /*if (isAllDifferent())
    {
    	element.setAttribute("allDifferent", "true");
    }
    
    if (getDescriptionObject() == null)
    {
      setDescriptionObject(Description.makeTextFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());*/

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
    position.setAttribute("x", AveryProject.twipsToMM(getPosition().getX()));
    position.setAttribute("y", AveryProject.twipsToMM(getPosition().getY()));

    element.addContent(position);

    if (getID().equals(getStyleID()))
    {
      copyStyleAttributes();
      element.addContent(getTextStyle().getAverysoftIBElement());
    }
    
    if (getID().equals(getContentID()))
    {
      element.addContent(this.text.getAverysoftIBElement());
    }

    return element;
  }
  /**
   * Implementation of Mergeable interface
   * 
   * @return a List containing a single com.avery.miwok.Text object
   */
  public List getTextContent()
  {
    List list = new ArrayList();
    list.add(text);
    return list;
  }

  /**
   * Implementation of Mergeable interface. Only the first object in the list is
   * used.
   * 
   * @param list -
   *          of com.avery.miwok.Text objects (may be empty!!).
   */
  public void setTextContent(List list)
  {
    if (list.size() == 0)
    {
      text = new Text();
    }
    else
    {
      text = (Text) list.get(0);
    }
  }

  /**
   * @param key
   * @return <code>true</code> if the field contains a mergemap line matching
   *         the specified mergemap key ("AF0", "AF1", etc.)
   */
  boolean hasMergeKey(String key)
  {
    return text.hasMergeKey(key);
  }

  private void addToPdf(PDFPage pdfPage, Double dPanelHeight, double hscale) throws Exception
  {
    int canvasStartX = 0;
    int canvasStartY = 0;
    double heightCalibration = 0;
    if (getPosition().x < 0)
    {
      canvasStartX = (int) (getPosition().x / 20f);
    }
    if (getPosition().y < 0)
    {
      canvasStartY = (int) (getPosition().y / 20f);
      heightCalibration = - getPosition().y;
    }

    int areaSize = (int) (pdfPage.getWidth() > pdfPage.getHeight() ? pdfPage.getWidth() - canvasStartX : pdfPage.getHeight()
        - canvasStartY);
    PDFCanvas contentContainer = new PDFCanvas(areaSize, areaSize);

    if (getMask() != null)
    {
      getMask().clipMask(contentContainer, canvasStartX, canvasStartY);
    }
    // Set up font
    OpenTypeFont openFont = null;
    PDFStyle style = new PDFStyle();
    try
    {
      // use font cache
      Hashtable fontCache = AveryProject.fontCache;
      if (fontCache.containsKey(getTypeface()))
      {
        openFont = (OpenTypeFont) fontCache.get(getTypeface());
      }
      else
      {
        String fullFontName = AveryProject.fontDir + "/" + super.getFontFilename();

        // at least for now UTF8 fonts are assumed 2 byte unicode
        // to support the eastern asian fonts that work with BFO.
        int bytesPerChar = 1;
        if (getFontEncoding().equals("UTF8"))
        {
          bytesPerChar = 2;
        }

        openFont = new OpenTypeFont(new FileInputStream(fullFontName), bytesPerChar);
        fontCache.put(getTypeface(), openFont);
      }

      // notice that some asian fonts do not handle being subsetted.
      // this flag forces the entire font to be embedded.
      // this makes the pdf large but this needed explanation here.
      // if you want all chars of a font use this line of code at your peril:
      // openFont.setSubset(false);

      style.setFont(openFont, getPointsize().floatValue());
      style.setFillColor(super.getTextcolor());
    }
    catch (Exception e)
    {
      System.err.println("AveryTextline Font Exception! - changing font to Times");
      System.err.println(e.toString());
      style.setFont(new StandardFont(StandardFont.TIMES), getPointsize().floatValue());
    }

    // underline text?
    if (isUnderline())
    {
      style.setTextUnderline(true);
    }
    else if (hscale < 100.0) // apply horizontal scaling here if not underlined
    {
      contentContainer.rawWrite(" " + hscale + " Tz ");
    }

    contentContainer.setStyle(style);

    // convert position to points
    float positionx = (float) (getPosition().x) / 20.0f;
    float positiony = (float) (dPanelHeight.intValue() - getPosition().y) / 20.0f;

    // get bounding box height in points
    float fieldheight = getHeight().floatValue() / 20.0f;
    float textHeight = style.getFontSize();

    // calculate vertical offset based on valign
    // valign = top is default

    float verticalOffset = style.getFont().getAscender() * style.getFontSize();

    if (fieldheight > textHeight)
    {
      if (super.getVerticalAlignment().equals("middle"))
      {
        verticalOffset += (fieldheight - textHeight) / 2;
      }
      else if (super.getVerticalAlignment().equals("bottom"))
      {
        verticalOffset += fieldheight - textHeight;
      }
      // do nothing for top alignment
    }

    // apply vertical alignment adjustment
    positiony -= verticalOffset;

    // handle right & center justification options if text isn't compressed
    if (hscale > 100.0 && getJustification().equals("right"))
    {
      float fieldWidth = getWidth().floatValue() / 20.0f;
      float textWidth = fieldWidth / (float) (hscale / 100f);
      positionx += fieldWidth - textWidth;
    }
    else if (hscale > 100.0 && getJustification().equals("center"))
    {
      float fieldWidth = getWidth().floatValue() / 20.0f;
      float textWidth = fieldWidth / (float) (hscale / 100f);
      positionx += (fieldWidth - textWidth) / 2.0f;
    }

    String textOut = AveryUtils.convertUTF8DecimalToUnicode(getContent());

    // field rotation
    double fieldRotation = getRotation().doubleValue();

    // handle panel and/or field rotations
    float xscale = (float) hscale / 100f;
    if (fieldRotation != 0.0)
    {
      // don't let concats concat inside the rotate
      contentContainer.save();
      // set rotation matrix values
      transformPDFField(contentContainer, new Double(dPanelHeight.doubleValue() + heightCalibration), positionx - canvasStartX,
          positiony - canvasStartY, fieldRotation);

      // squeeze with underline requires extra treatment because BFO did not
      // transform the length of the underline with the Tz text transform
      // therefore the graphics transform needs to be applied to the object

      if (isUnderline() && hscale < 100.0)
      {
        contentContainer.rawWrite(AveryPanelField.formatConcat(xscale, 0f, 0f, 1f, 0f, 0f));
      }

      LayoutBox tmpBox = new LayoutBox(getWidth().floatValue() / 20.0f);

      // setup complete -- draw the text

      tmpBox.addTextNoBreak(textOut, style, null);
      contentContainer.drawLayoutBox(tmpBox, 0.0f, 0.0f);
      // contentContainer.drawText(textOut, 0.0f, 0.0f);

      // forget the rotate operations
      contentContainer.restore();
    }
    else
    {
      if (isUnderline() && hscale < 100.0)
      {

        // text with underline requires a graphics transform to shrink both the
        // text and the underline
        contentContainer.save();
        contentContainer.rawWrite(AveryPanelField.formatConcat(xscale, 0f, 0f, 1f, 0f, 0f));
        LayoutBox tmpBox = new LayoutBox(getWidth().floatValue() / 20.0f / xscale);
        tmpBox.addTextNoBreak(textOut, style, null);
        contentContainer.drawLayoutBox(tmpBox, positionx / xscale - canvasStartX, positiony - canvasStartY);
        // contentContainer.drawText(textOut, positionx / xscale, positiony);
        contentContainer.restore();
      }
      else
      // not squeezed underlined
      {
        LayoutBox tmpBox = new LayoutBox(getWidth().floatValue() / 20.0f / xscale);
        tmpBox.addTextNoBreak(textOut, style, null);
        contentContainer.drawLayoutBox(tmpBox, positionx - canvasStartX, positiony - canvasStartY);
        // contentContainer.drawText(textOut, positionx, positiony);
      }
    }

    if (hscale < 100.0 && !isUnderline()) // undo non-underlined horizontal
    // scaling
    {
      contentContainer.rawWrite(" 100 Tz "); // reset to default horizontal
      // scaling
    }
    pdfPage.drawCanvas(contentContainer, canvasStartX, canvasStartY, areaSize + canvasStartX, areaSize + canvasStartY);
    if (getMask() != null && getMask().incrementIndex())
    {
      addToPdf(pdfPage, dPanelHeight, hscale);
    }
  }

  private double stringSizeRatio()
  {
    String string = this.getContent();
    // use 1/10 point precision for this test
    int testPointSize = (int) (this.getPointsize().doubleValue() * 10.0);
    double testFieldWidth = this.getWidth().doubleValue() / 2.0;

    Font testFont = super.makePreviewFont(super.getPreviewTypeface(), testPointSize);
    double textWidth = new TextLayout(string, testFont, new FontRenderContext(null, true, true)).getAdvance();
    testFont = null; // gc

    return textWidth / testFieldWidth;
  }

  /**
   * Draws the content onto the graphics surface, squeezing the font if the
   * string is too long
   * 
   * @param graphics -
   *          the surface to draw to
   * @param scalar -
   *          scaling multiplier from TWIPS to graphics
   */
  private void directDraw(Graphics2D graphics, float scalar)
  {
    // create a font in the correct size
    Font font = super.makeFont(super.getPreviewTypeface(), 20.0f * scalar * getPointsize().floatValue());

    graphics.setFont(font);
    FontMetrics metrics = graphics.getFontMetrics();
    int maxWidth = (int) (getWidth().floatValue() * scalar);
    int stringWidth = metrics.stringWidth(getContent());

    if (stringWidth > maxWidth) // string too long?
    {
      // squeeze font to make it fit
      AffineTransform transform = new AffineTransform();
      transform.scale((double) maxWidth / ((double) stringWidth + 1), 1.0);
      font = font.deriveFont(transform);
      graphics.setFont(font);
      // repair outdated locals
      metrics = graphics.getFontMetrics();
      stringWidth = metrics.stringWidth(getContent());
    }

    // Calculate x position, starting at left of field
    int x = (int) (getPosition().x * scalar);

    if (getJustification().equals("center"))
    {
      x += (maxWidth - stringWidth) / 2;
    }
    else if (getJustification().equals("right"))
    {
      x += maxWidth - stringWidth;
    }

    // calculate y position, starting at top of field
    int y = (int) (getPosition().y * scalar);

    if (getVerticalAlignment().equals("top"))
    {
      y += metrics.getAscent();
    }
    else if (getVerticalAlignment().equals("middle"))
    {
      double textHeight = (double) (metrics.getAscent() + Math.abs(metrics.getDescent()));
      double fieldHeight = getHeight().doubleValue() * scalar;
      y += (int) ((fieldHeight - textHeight) / 2.0);
      y += metrics.getAscent();
    }
    else if (getVerticalAlignment().equals("bottom"))
    {
      y += (int) getHeight().doubleValue() * scalar;
      y -= Math.abs(metrics.getDescent());
      y -= metrics.getLeading();
    }

    float opacity = getOpacity().floatValue();
  	Composite originalComposite = graphics.getComposite();
    if (opacity >= 0f && opacity < 1f)
    {
    	graphics.setComposite(makeComposite(opacity));
    }
    if (getRotation().intValue() == 0)
    {
      super.drawString(graphics, getContent(), x, y);
    }
    else
    // rotate
    {
      double theta = getRotation().doubleValue() * (Math.PI / 180.0);

      int fieldx = (int) (getPosition().x * scalar);
      int fieldy = (int) (getPosition().y * scalar);
      // x and y relative to field position
      x -= fieldx;
      y -= fieldy;

      graphics.translate(fieldx, fieldy);
      graphics.rotate(-theta);
      super.drawString(graphics, getContent(), x, y);
      graphics.rotate(theta);
      graphics.translate(-fieldx, -fieldy);
    }
    if (opacity >= 0f && opacity < 1f)
    {
    	graphics.setComposite(originalComposite);
    }

    return;
  }

  /**
   * This calculates the maximum pointsize that will render correctly. Used by
   * Avery Print 5.0.
   * 
   * @return the maximum size in points.
   */
  public double maxPointSize()
  {
    return this.getHeight().doubleValue() / 20.0;
  }

  /**
   * @see AveryTextfield.translateText
   */
  int translateText(Hashtable hash)
  {
    return text.translateText(hash) ? 1 : 0;
  }
}
