package com.avery.project;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Enumeration;

/**
 * <p>ProjectFieldTable</p>
 * <p>Description: Indexes all project panel fields by unique id</p>
 * <p>Copyright: Copyright 2005 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Brad Nelson
 * @version 1.0
 */

class ProjectFieldTable extends Hashtable
{
  /**
	 * required by Serializable
	 */
	private static final long serialVersionUID = -5464077365510518201L;

	private int refIDCounter = 0;

  // private int firstPagePanelID = 0;
	
	// required for projects that come in from flashPrint
  HashMap flashHash = new HashMap();
  String lastFullId = "PF0";
  int flashId = 0;

  /**
   * Each project has exactly one ProjectFieldTable
   */
  public ProjectFieldTable()
  {
  }

	/**
	 * adds a field to the hashtable
	 * @param refID - key
	 * @param field - value
	 */
  public void addField(String refID, AveryPanelField field)
  {
    put(refID, field);
  }

	/**
	 * adds a field ref to the hashtable
	 * @param refID - key
	 * @param field - value
	 */
  public void addField(String refID, FieldRef field)
  {
    put(refID, field);
  }

	/**
	 * gets the field value matching the refID key
	 * @param refID - key
	 * @return field object
	 */
  public Object getField(String refID)
  {
    return (Object)get(refID);
  }

  /**
	 * removes the item matching the refID key
	 * @param refID - key
	 */
  public void removeItem(String refID)
  {
    remove(refID);
  }

   /**
	 * prepares the next sequential refID key
	 * @return next refID
	 */
  public void setRefIDCounter(int newRefIDCounter)
  {
    refIDCounter = newRefIDCounter;
  }

 /**
	 * prepares the next sequential refID key
	 * @return next refID
	 */
  public String nextRefID()
  {
    String numericPart = String.valueOf(refIDCounter++);
    return "PF" + numericPart;
  }

  /**
	 * adjusts refIDCounter when deserialize fields
   * after loading projects with PDL data counter will be correctly
   * initialized for handing out the nextrefID()
	 * @param refID - key refID
	 */
  public void adjustRefIDCounter(String refID)
  {
    int numericPart = Integer.parseInt(refID.substring(2));
    if (refIDCounter <= numericPart)
      refIDCounter = numericPart + 1;
  }

  /**
	 * gets a non editable AveryPanelField from content top superior of refID key
	 * @param refID - key refID
	 * @return AveryPanelField null indicates field not found
	 */
  public AveryPanelField getContentField(String refID)
  {
    String contentFieldID = refID;
    String fieldID = "-1";
    Object contentField = (Object)this;

    while (!(contentFieldID.equals(fieldID)) && contentField != null)
    {
      contentField = getField(contentFieldID);

      if (contentField instanceof FieldRef)
      {
        FieldRef fieldRef = (FieldRef)contentField;
        contentFieldID = fieldRef.getContentID();
        fieldID = fieldRef.getID();
      }
      else if (contentField instanceof AveryPanelField)
      {
        AveryPanelField panelField = (AveryPanelField)contentField;
        contentFieldID = panelField.getContentID();
        fieldID = panelField.getID();
        // if first pass are done - no field clone required
        if (contentFieldID.equals(refID))
          return panelField;
      }
    }

    if (contentField == null)
    {
      // no top superior, not an error condition, remove this entry
      removeItem(refID);
      return null;
    }

    return (AveryPanelField)contentField;
  }

  /**
	 * gets an editable AveryPanelField from content top superior of refID key
	 * @param refID - key refID
	 * @return AveryPanelField null indicates field not found
	 */
  public AveryPanelField findContentField(String refID)
  {
    // returns a clone of the content field with given ID
    return makeParentClone(getContentField(refID));
  }

 /**
   * finds the master top superior ID for the given content id
   * @param the content ID
   * @return the ID of the master superior or the original field id
   */
  public String findMasterContentID(String refID)
  {
    String contentFieldID = refID;
    String fieldID = "-1";
    Object contentField = (Object)this;

    if (!(contentFieldID.equals(fieldID)) && contentField != null)
    {
      contentField = getField(contentFieldID);

      if (contentField instanceof FieldRef)
      {
        FieldRef fieldRef = (FieldRef)contentField;
        contentFieldID = fieldRef.getContentID();
        fieldID = fieldRef.getID();
      }
    }
    return contentFieldID;
  }

  /**
	 * gets a non editable AveryPanelField from style top superior of refID key
	 * @param refID - key refID
	 * @return AveryPanelField null indicates field not found
	 */
  public AveryPanelField getStyleField(String refID)
  {
    String styleFieldID = refID;
    String fieldID = "-1";
    Object styleField = (Object)this;

    while (!(styleFieldID.equals(fieldID)) && styleField != null)
    {
      styleField = getField(styleFieldID);

      if (styleField instanceof FieldRef)
      {
        FieldRef fieldRef = (FieldRef)styleField;
        styleFieldID = fieldRef.getStyleID();
        fieldID = fieldRef.getID();
      }
      else if (styleField instanceof AveryPanelField)
      {
        AveryPanelField panelField = (AveryPanelField)styleField;
        styleFieldID = panelField.getStyleID();
        fieldID = panelField.getID();
        // if first pass are done - no field clone required
        if (styleFieldID.equals(refID))
          return panelField;
      }
    }

    if (styleField == null)
    {
      // no top superior, not an error condition, remove this entry
      removeItem(refID);
      return null;
    }

    return (AveryPanelField)styleField;
  }

  /**
	 * gets an editable AveryPanelField from style top superior of refID key
	 * @param refID - key refID
	 * @return AveryPanelField null indicates field not found
	 */
  public AveryPanelField findStyleField(String refID)
  {
    // returns a clone of the style field with given ID
    return makeParentClone(getStyleField(refID));
  }

 /**
   * finds the master top superior ID for the given style id
   * @param the content ID
   * @return the ID of the master superior or the original id
   */
  public String findMasterStyleID(String refID)
  {
    String styleFieldID = refID;
    String fieldID = "-1";
    Object styleField = (Object)this;

    if (!(styleFieldID.equals(fieldID)) && styleField != null)
    {
      styleField = getField(styleFieldID);

      if (styleField instanceof FieldRef)
      {
        FieldRef fieldRef = (FieldRef)styleField;
        styleFieldID = fieldRef.getStyleID();
        fieldID = fieldRef.getID();
      }
    }
    return styleFieldID;
  }

  /**
	 * gets a non editable AveryPanelField from geometry top superior of refID key
	 * @param refID - key refID
	 * @return AveryPanelField null indicates field not found
	 */
  public AveryPanelField getGeometryField(String refID)
  {
    String geometryFieldID = refID;
    String fieldID = "-1";
    Object geometryField = (Object)this;

    while (!(geometryFieldID.equals(fieldID)) && geometryField != null)
    {
    	geometryField = getField(geometryFieldID);

      if (geometryField instanceof FieldRef)
      {
        FieldRef fieldRef = (FieldRef)geometryField;
        geometryFieldID = fieldRef.getGeometryID();
        fieldID = fieldRef.getID();
      }
      else if (geometryField instanceof AveryPanelField)
      {
        AveryPanelField panelField = (AveryPanelField)geometryField;
        geometryFieldID = panelField.getStyleID();
        fieldID = panelField.getID();
        // if first pass are done - no field clone required
        if (geometryFieldID.equals(refID))
          return panelField;
      }
    }

    if (geometryField == null)
    {
      // no top superior, not an error condition, remove this entry
      removeItem(refID);
      return null;
    }

    return (AveryPanelField)geometryField;
  }

  /**
	 * gets an editable AveryPanelField from geometry top superior of refID key
	 * @param refID - key refID
	 * @return AveryPanelField null indicates field not found
	 */
  public AveryPanelField findGeometryField(String refID)
  {
    // returns a clone of the style field with given ID
    return makeParentClone(getGeometryField(refID));
  }

 /**
   * finds the master top superior ID for the given geometry id
   * @param the content ID
   * @return the ID of the master superior or the original id
   */
  public String findMasterGeometryID(String refID)
  {
    String geometryFieldID = refID;
    String fieldID = "-1";
    Object geometryField = (Object)this;

    if (!(geometryFieldID.equals(fieldID)) && geometryField != null)
    {
    	geometryField = getField(geometryFieldID);

      if (geometryField instanceof FieldRef)
      {
        FieldRef fieldRef = (FieldRef)geometryField;
        geometryFieldID = fieldRef.getGeometryID();
        fieldID = fieldRef.getID();
      }
    }
    return geometryFieldID;
  }

  // clones an AveryPanelField
  private AveryPanelField makeParentClone(AveryPanelField field)
  {
    AveryPanelField cloneField = null;

    if (field instanceof AveryTextblock)
      cloneField = (AveryPanelField)(((AveryTextblock)field).deepClone());
    else if (field instanceof AveryTextline)
      cloneField = (AveryPanelField)(((AveryTextline)field).deepClone());
    else if (field instanceof AveryImage)
      cloneField = (AveryPanelField)(((AveryImage)field).deepClone());
    else if (field instanceof AveryBackground)
      cloneField = (AveryPanelField)(((AveryBackground)field).deepClone());
    else if (field instanceof TextPath)
      cloneField = (AveryPanelField)(((TextPath)field).deepClone());
    else if (field instanceof Barcode)
      cloneField = (AveryPanelField)(((Barcode)field).deepClone());
    else if (field instanceof Drawing)
      cloneField = (AveryPanelField)(((Drawing)field).deepClone());

    return cloneField;
  }

  /**
	 * updates the content of a field, including geometry changes
	 * @param contentID - key contentID
	 * @param field - AveryPanelField
	 * @return boolean true if field promoted to top superior
	 */
  public boolean updateContentField(String contentID, AveryPanelField field)
  {
    Object contentField = getField(contentID);

    // the id and the content id must be the same, breaks PDL
    // making this a top superior to subordinates
    field.setID(contentID);
    field.setContentID(contentID);

    if (contentField instanceof AveryPanelField)
    {
      // no need to replace FieldRef by AveryPanelField

      // if contentField is same as source field no update necessary
      if (contentField == field)
        return false;

      // copy content
      if (contentField instanceof AveryTextline)
      {
        ((AveryTextline)contentField).setContent(((AveryTextline)field).getContent());
      }
      else if (contentField instanceof AveryTextblock)
      {
        ((AveryTextblock)contentField).setLines(((AveryTextblock)field).getLines());
      }
      else if (contentField instanceof AveryImage)
      {
        String source = ((AveryImage)field).getSource();
        String gallery = ((AveryImage)field).getGallery();
        ((AveryImage)contentField).setSourceAndGallery(source, gallery);
      }
      else if (contentField instanceof TextPath)
      {
        ((TextPath)contentField).setContent(((TextPath)field).getContent());
      }
      // content changes include geometry
      AveryPanelField sourceField = (AveryPanelField)field;
      AveryPanelField destField = (AveryPanelField)contentField;
      destField.setPosition(sourceField.getPosition());
      destField.setWidth(sourceField.getWidth());
      destField.setHeight(sourceField.getHeight());
      destField.setRotation(sourceField.getRotation());
      // destField is a new content top superior
      destField.setContentID(contentID);
      return false;
    }

    // retain style id from the field ref object
    String oldStyleID = ((FieldRef)contentField).getStyleID();
    field.setStyleID(oldStyleID);

    // remove FieldRef and replace with AveryPanelField
    removeItem(contentID);
    addField(contentID, field);

    return true;
  }

  /**
	 * updates the style of a field
	 * @param styleID - key styleID
	 * @param field AveryPanelField
	 * @return boolean true if field promoted to top superior
	 */
  public boolean updateStyleField(String styleID, AveryPanelField field)
  {
    Object styleField = getField(styleID);

    // the id and the style id must be the same, breaks PDL
    // making this a top superior to subordinates
    field.setID(styleID);
    field.setStyleID(styleID);

    if (styleField instanceof AveryPanelField)
    {
      // no need to replace FieldRef by AveryPanelField

      // if styleField is same as source field no update necessary
      if (styleField == field)
        return false;

      // copy style attributes
      if (styleField instanceof AveryTextfield)
      {
        AveryTextfield destField = (AveryTextfield)styleField;
        AveryTextfield sourceField = (AveryTextfield)field;
        destField.setTypeface(sourceField.getTypeface());
        destField.setPointsize(sourceField.getPointsize());
        destField.setTextcolor(sourceField.getTextcolor());
        destField.setJustification(sourceField.getJustification());
        destField.setVerticalAlignment(sourceField.getVerticalAlignment());
        destField.setStyles(sourceField.getStyles());
        destField.setOverflow(sourceField.getOverflow());
        // update text style
        destField.copyStyleAttributes();
        // destField is a new style top superior
        destField.setStyleID(styleID);
      }
      return false;
    }

    // retain content id from the field ref object
    String oldContentID = ((FieldRef)styleField).getContentID();
    field.setContentID(oldContentID);

    // remove FieldRef and replace with AveryPanelField
    removeItem(styleID);
    addField(styleID, field);

    return true;
  }

  public void dumpKeys()
  {
    Enumeration e = keys();
    while (e.hasMoreElements())
    {
      System.out.println((String)e.nextElement());
    }
  }
}