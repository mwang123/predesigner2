/**
 * Title:       AveryProject<p>
 * Description: This class encapsulates all of the objects necessary to print
 *              a project on Avery media.<p>
 * Copyright:   (c)Copyright 2013-2016 Avery Products Corp. All Rights Reserved.
 * @author      Bob Lee, Brad Nelson
 * @version     2.5
 */

package com.avery.project;  // a.k.a. Miwok

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.faceless.pdf2.Event;
import org.faceless.pdf2.PDF;
import org.faceless.pdf2.PDFAction;
import org.faceless.pdf2.OutputProfile;
//import org.faceless.pdf2.PDFAction;

import org.jdom.Element;
import org.jdom.Namespace;
//import org.jdom.output.XMLOutputter;

import com.avery.Averysoft;
import com.avery.predesign.StyleSet;
//import com.avery.predesigner.Predesigner;
import com.avery.project.AverySuperpanel;
import com.avery.project.shapes.RectangleShape;
//import com.avery.utils.AveryException;
import com.avery.utils.AveryUtils;
import com.avery.utils.FileArrayProvider;
//import com.avery.XMLOutput.Format;
//import com.avery.XMLOutput.XMLOutputter;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;

import com.avery.project.shapes.ShapeIntersection;

/**
 * The AveryProject class contains all of the objects and attributes that
 * comprise a printable project.  At this top level, we see that the project
 * is basically two lists: a list of masterpanels and a list of pages.
 * @see AveryMasterpanel
 * @see AveryPage
 */
public class AveryProject implements XMLElement, Averysoftable, Printable
{
  /**
   * default constructor
   */
  public AveryProject()
  {
    systemID = new String("Avery.dtd");

    // initialize font cache
    if (fontCache == null)
    {
      fontCache = new Hashtable();
    }

  }

  // for testing only
  //private boolean copyBack = true;
  
  // data
  private String copyright;
  private String language = "en";
  private String description;
  private String systemID = "Avery.dtd";
  private String productGroup;
  private String sku;
  private String skuDescription;
  private String baseLayout;
  private ArrayList serialNumbers = new ArrayList();
  private ArrayList masterpanels = new ArrayList();
  private ArrayList pages = new ArrayList();
  private String designCategory = null;
  private String designTheme = null;
  private Point nudge = new Point(0,0);
  private String nudgeUnits = "in";
  private int revision = 0;
  private String version = "";
  private boolean isApplyToAll = true;
  private boolean isLegacyProject = false;
  private boolean isMergable = true;
  
  private boolean stencil = false;

	MergeData mergeData = null;

  // new for averysoft.xsd
  private Hashtable hints = null;
  private ProjectFieldTable fieldTable = null;

  // font cache
  public static Hashtable fontCache = null;
  public static String fontDir = "";

  private List additionalNamespaces;

  private int BACK_PANEL = 0;
  private int FRONT_PANEL = 9;
  private int INSIDE_LEFT_PANEL = 12;
  private int INSIDE_RIGHT_PANEL = 13;
  private int INSIDE_BOTTOM_PANEL = 41;
  private int INSIDE_TOP_PANEL = 42;
  
  private int FULL_PAGE_PANEL = 18;
  private int FRONT_BACK_PANEL = 86;
  private int INSIDE_SPREAD_PANEL = 14;
  

  // hack to output project in mm for iBright when true
	public static boolean outputMM = false;

  public static String twipsToMM(double twips)
  {
  	int convert = (int)(Math.round(100.0 * twips / 56.6929138));
  	double twoDigits = (double)convert / 100;
  	return new Double(twoDigits).toString();
  }
  /**
   * Construct project from a JDOM Element
   * @param element - an Avery.project XML element
   * @throws Exception if something goes wrong
   */
  public AveryProject(Element element)
  throws Exception
  {
    if (!initFromElement(element))
    {
      Exception ex = new Exception("Not an Avery project element");
      ex.printStackTrace();
      throw ex;
    }
  }

  /**
   * Initializes the AveryProject using a JDOM element.  This is called directly from
   * the AveryProject(Element) constructor, and by the derived Ape.Project(File)
   * constructor as well.
   * @param element the Element to parse
   * @return <code>false</code> if the Element wasn't a valid averysoft.xsd-based
   * project or Avery.dtd-based Avery.project element
   * @throws Exception in case of errors in child nodes of the DOM
   */
  protected boolean initFromElement(Element element)
  throws Exception
  {
    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace()) && element.getName().equals("project"))
    {
    	additionalNamespaces = element.getAdditionalNamespaces();

      copyright      = element.getAttributeValue("copyright");
      if (copyright == null || copyright.indexOf("Dennison") >= 0)
      {
      	copyright = "Copyright 2016 Avery Products Corporation. All rights reserved. Patent Pending.";
      }
      description    = element.getAttributeValue("description");
      language       = element.getAttributeValue("language");
      designCategory = element.getAttributeValue("designCategory");
      designTheme    = element.getAttributeValue("designTheme");
      String rev     = element.getAttributeValue("revision");
      if (rev != null)
      {
      	try
				{		
      		setRevision(Integer.parseInt(rev));
				}
      	catch (NumberFormatException ex)
				{ }
      }
      
      String ver = element.getAttributeValue("version");
    	if (ver != null)
    	{
    		if (ver.length() > 0)
    			version = ver;
    	}
    	
      isApplyToAll = "true".equals(element.getAttributeValue("applyToAll"));
      isMergable = !("false".equals(element.getAttributeValue("isMergable")));

      Element child;		// reusable local item

      Iterator iterator = element.getChildren("hint", AveryProject.getAverysoftNamespace()).iterator();
      while (iterator.hasNext())
      {
        child = (Element)iterator.next();
        String name = child.getAttributeValue("name");
        String value = child.getAttributeValue("value");
        
        addHint(name, value);
        if (name.equals("PredesignerInstruction") && value.equals("Use Stencil"))
        {
        	stencil = true;
        }
      }

      if ((child = element.getChild("sku", AveryProject.getAverysoftNamespace())) != null)
      {
        sku = child.getTextTrim();
        skuDescription = child.getAttributeValue("description");
        if (skuDescription == null)
          skuDescription = "none";
      }

      if ((child = element.getChild("baseLayout", AveryProject.getAverysoftNamespace())) != null)
      {
        baseLayout = child.getTextTrim();
      }

      iterator = element.getChildren("serialNumber", AveryProject.getAverysoftNamespace()).iterator();
      while (iterator.hasNext())
      {
        addSerialNumber(new SerialNumber((Element)iterator.next()));
      }
      
      if ((child = element.getChild("nudge", AveryProject.getAverysoftNamespace())) != null)
      {
        StringTokenizer st = new StringTokenizer(child.getTextTrim(), ",");
        Double x = new Double(st.nextToken());
        Double y = new Double(st.nextToken());
        nudge.setLocation(x.doubleValue(), y.doubleValue());
        setNudgeUnits(child.getTextTrim().endsWith("mm") ? "mm" : "in");
      }

      if ((child = element.getChild("productGroup", AveryProject.getAverysoftNamespace())) != null)
      {
        setProductGroup(child.getTextTrim());
      }

      if ((child = element.getChild("panelSetOptions", AveryProject.getAverysoftNamespace())) != null)
      {
        PanelSetOptions options = new PanelSetOptions(child);
        if (options.master != null && options.panelsPerSet > 0)		// crude validity test
        {
        	setPanelSetOptions(options);
        }
      }

      iterator = element.getChildren("masterpanel", AveryProject.getAverysoftNamespace()).iterator();
      while (iterator.hasNext())
      {
        addMasterpanel(new AveryMasterpanel((Element)iterator.next()));
      }

      iterator = element.getChildren("page", AveryProject.getAverysoftNamespace()).iterator();
      while (iterator.hasNext())
      {
        addPage(new AveryPage((Element)iterator.next()));
      }
    }
    else if (element.getName().equals("Avery.project"))	// Avery.dtd-based XML
    {
      // read the Avery.project attributes
      setCopyright(element.getAttributeValue("copyright"));
      setLanguage(element.getAttributeValue("language"));
      setDescription(element.getAttributeValue("description"));
      setDesignCategory(element.getAttributeValue("projectcategory"));
      setDesignTheme(element.getAttributeValue("subcategory"));

      Element child;   // reusable Element

      if ((child = element.getChild("Avery.sku")) != null)
      {
	      setSku(child.getTextTrim());
	      setSkuDescription(child.getAttributeValue("description"));
      }

      if ((child = element.getChild("Avery.productgroup")) != null)
      {
      	setProductGroup(child.getTextTrim());
      }
      else if ((child = element.getChild("Avery.projectgroup")) != null)
      { // productgroup was called projectgroup in earlier versions
      	setProductGroup(child.getTextTrim());
      }
      else if ((child = element.getChild("Avery.templatefamily")) != null)
      { // productgroup was called templatefamily once upon a time
      	setProductGroup(child.getTextTrim());
      }

      if ((child = element.getChild("Avery.nudge")) != null)
      {
	      StringTokenizer st = new StringTokenizer(child.getTextTrim(), ",");
	      Double x = new Double(st.nextToken());
	      Double y = new Double(st.nextToken());
	      nudge.setLocation(x.doubleValue(), y.doubleValue());
	      setNudgeUnits(child.getTextTrim().endsWith("mm") ? "mm" : "in");
      }

      if ((child = element.getChild("Avery.baselayout")) != null)
      {
      	setBaseLayout(child.getTextTrim());
      }

      // instantiate masterpanels
      Iterator iterator = element.getChildren("Avery.masterpanel").iterator();
      while (iterator.hasNext())
      {
      	addMasterpanel(new AveryMasterpanel((Element)iterator.next()));
      }

      // instantiate pages
      iterator = element.getChildren("Avery.page").iterator();
      while (iterator.hasNext())
      {
      	addPage(new AveryPage((Element)iterator.next()));
      }

      // a blast from the past
      setLegacyProject(true);

      // generate PanelField IDs
			int number = 1;
			Iterator fields = getAllPanelFields().iterator();
			while (fields.hasNext())
			{
				((AveryPanelField)fields.next()).setID("PF" + number++);
			}
    }
    else
    {
      return false;		// not a valid element
    }

    // resolve the masterpanel ID in all of the page panels
    setMasterpanelOrdinals();
    resolveMasterpanels();

    // initializes panel data linking for any kind of AveryProject
    // including dtd, BAT, Averysoft
    initializePanelDataLinking();

    // initialize font cache
    if (fontCache == null)
    {
      fontCache = new Hashtable();
    }

    // set language of Description objects
    if (language == null || language.equals("ENU") )
    {
    	setLanguage("en");	// correction for legacy XML
    }

    if (getLanguage() != null)
    {
    	translateDescriptions(getLanguage());
    }

    // for testing only
    //if (isDoubleSided() && copyBack)
    	//replaceBackPage();

    
    return true;
  }

  /**
   * You can set the copyright string with this public method.  The copyright
   * string should be set to the language supplied by our legal department.
   * Its main purpose is to copyright the XML representation of the project.
   * @param newCopyright - the new copyright string
   */
  public void setCopyright(String newCopyright)
  {
    copyright = newCopyright;
  }

  /**
   * fetches the copyright string
   * @return the embedded copyright string (may be <code>null</code>)
   * @see #setCopyright
   */
  public String getCopyright()
  {
    return copyright;
  }

  /**
   * The language attribute is currently unused, as the actual strings to
   * describe languages have never been formally defined.  In the future, we
   * will be able to identify the human-readable language of the project with
   * this attribute.
   * @param newLanguage - the new language string
   */
  public void setLanguage(String newLanguage)
  {
    language = newLanguage;
    translateDescriptions(newLanguage);
  }

  /**
   * fetches the project's language attribute
   * @return the project's language string
   * @see #setLanguage
   */
  public String getLanguage()
  {
    return language;
  }

  /**
   * This sets the project's description attribute.  Early versions of LDL
   * (used for PFTW 2.0) mistakenly set this string to the <b>sku</b>
   * description.
   * @param newDescription - project description string
   */
  public void setDescription(String newDescription)
  {
    description = newDescription;
  }

  /**
   * fetches the project's description string
   * @return the description string  (may be <code>null</code>)
   * @see #setDescription
   */
  public String getDescription()
  {
    return description == null ? "" : description;
  }

  /**
   * sets the project's system ID of the DocType
   * @param newSystemID - project .dtd name
   * @deprecated nobody should need this
   */
  public void setSystemID(String newSystemID)
  {
    systemID = newSystemID;
  }

  /**
   * fetches the project's system ID (.dtd name)
   * @return the system ID  (may be <code>null</code>)
   */
  public String getSystemID()
  {
    return systemID;
  }

  /**
   * A "template family" is an identifier for a set of skus that can be used
   * with a project.  This mechanism is an aid for the application, which must
   * connect Avery product SKUs with projects somehow.
   * @param newTemplateFamily - the family identification string
   * @see #setSku
   * @deprecated use {@link #setProjectGroup} instead
   */
  public void setTemplateFamily(String newTemplateFamily)
  {
    productGroup = newTemplateFamily;
  }

  /**
   * A "project group" is an identifier for a set of skus that can be used
   * with a project.  This mechanism is an aid for the application, which must
   * connect Avery product SKUs with projects somehow.
   * @param newProjectGroup -
   */
  public void setProjectGroup(String newProjectGroup)
  {
    productGroup = newProjectGroup;
  }

  /**
   * fetches the project's associated template family
   * @return this AveryProject's template family designation, as a String
   * (may be <code>null</code>)
   * @see #setTemplateFamily
   * @deprecated use {@link #getProductGroup} instead
   */
  public String getTemplateFamily()
  {
    return productGroup;
  }

  /**
   * @return this AveryProject's projectGroup designation, as a String
   * @see #setProjectGroup
   */
  public String getProjectGroup()
  {
    return productGroup;
  }

  /**
   * fetches the project's associated template family
   * @return this AveryProject's productgroup designation, as a String
   * (may be <code>null</code>)
   */
  public String getProductGroup()
  {
    return productGroup;
  }

  /**
   * A "product group" is an identifier for a set of skus that can be used
   * with a project.  This mechanism is an aid for the application, which must
   * connect Avery product SKUs with projects somehow.
   * @param newProductGroup -
   */
  public void setProductGroup(String newProductGroup)
  {
    productGroup = newProductGroup;
  }

  /**
   * A sku (shelf keeping unit) is a unique identifier for an Avery paper
   * product.  Typically, the sku attribute of a project represents the product
   * that the project was designed for.  The template family attribute can be
   * used to connect the project to other skus, at the application level.
   * @param newSku - the Avery product identifier string
   * @see #setTemplateFamily
   */
  public void setSku(String newSku)
  {
    sku = newSku;
  }

  /**
   * A sku (shelf keeping unit) is a unique identifier for an Avery paper
   * product.  Typically, the sku attribute of a project represents the product
   * that the project was designed for.  The template family attribute can be
   * used to connect the project to other skus, at the application level.
   * @param newSkuDescription - the Avery product identifier string
   * @see #setTemplateFamily
   */
  public void setSkuDescription(String newSkuDescription)
  {
    skuDescription = newSkuDescription;
  }

  /**
   * fetches the sku that this project was designed for
   * @return this AveryProject's intended paper product indentification string
   * (may be <code>null</code>)
   * @see #setSku
   */
  public String getSku()
  {
    return sku;
  }

  /**
   * fetches the sku description that this project was designed for
   * @return this AveryProject's intended paper product indentification string
   * (may be <code>null</code>)
   * @see #setSku
   */
  public String getSkuDescription()
  {
    return skuDescription;
  }
  
  /**
   * The project maintains an ArrayList of AveryMasterpanels, which are used
   * to replicate fields on multiple panels on the printed page.  As the
   * project is being created in memory, a list of masterpanels may be created
   * externally and assigned into the project.
   * <p>Note that this is technically not necessary, as a new ArrayList is
   * created during construction, and masterpanels can be added to it by
   * {@link #addMasterpanel}.
   * @param newMasterpanels - collection of AveryMasterpanel objects
   * @see AveryMasterpanel
   * @see AverySuperpanel
   */
  public void setMasterpanels(ArrayList newMasterpanels)
  {
    masterpanels = newMasterpanels;
  }

  /**
   * provides access to the list of masterpanels
   * @return list of AveryMasterpanels associated with this project
   * @see #setMasterpanels
   */
  public ArrayList getMasterpanels()
  {
  	return masterpanels;
  }

  /**
   * adds a new AveryMasterpanel to the project
   * @param panel - the masterpanel to add
   * @return true on success
   */
  public boolean addMasterpanel(AveryMasterpanel panel)
  {
    return masterpanels.add(panel);
  }

  public boolean addSerialNumber(SerialNumber serialNumber)
  {
    return serialNumbers.add(serialNumber);
  }

  /**
   * The project maintains a list of AveryPages.  Using this method, a list of
   * pages can be constructed externally and assigned into the project.
   * @param newPages - a list of AveryPages to associate with this project
   */
  public void setPages(ArrayList newPages)
  {
    pages = newPages;
  }

  /**
   * provides access to the page array
   * @return a list of AveryPage objects
   */
  public ArrayList getPages()
  {
    return pages;
  }

  /**
   * adds a page to the project's list of pages
   * @param page - the page to add
   * @see #setPages
   */
  public void addPage(AveryPage page)
  {
    pages.add(page);
  }

  /**
   * generates an HTML representation of the data in this project
   * @return a String containing HTML suitable for inclusion in a web page
   */
  public String dump()
  {
    String str = new String("");
    str += "<P>AveryProject";
    str += "<br>copyright = " + getCopyright();
    str += "<br>language = " + getLanguage();
    str += "<br>description = " + getDescription();
    str += "<br>category = " + getDesignCategory();
    str += "<br>theme = " + getDesignTheme();
    str += "<br>projectGroup = " + getProjectGroup();
    str += "<br>baseLayout = " + getBaseLayout();
    str += "<br>sku = " + getSku();

    Iterator iterator = getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      AveryMasterpanel mp = (AveryMasterpanel)iterator.next();
      str += mp.dump();
    }

    iterator = getPages().iterator();
    while (iterator.hasNext())
    {
      AveryPage page = (AveryPage)iterator.next();
      str += page.dump();
    }

    return str;
  }

  /**
   * Shallow AveryProject clone
   * @return AveryProjectshallow clone
   */
  public void clone(AveryProject project)
	{
		project.setCopyright(copyright);
		project.setLanguage(language);
		project.setDescription(description);
    project.setDesignCategory(this.getDesignCategory());
		project.setSku(sku);
		project.setSkuDescription(skuDescription);
		project.setProductGroup(productGroup);
		project.setNudge(nudge);
		project.setNudgeUnits(nudgeUnits);
		project.setBaseLayout(baseLayout);
		project.setVersion(version);
	}

  /**
   * Nudge describes a page offset used for printer calibration.  It is
   * observed when creating a PDF file for printing.  Like all coordinates is
   * the AveryProject, nudge is expressed in twips (20ths of a point)
   * @param newNudge - the offset to use when printing
   */
  public void setNudge(Point newNudge)
  {
    nudge = newNudge;
  }

  /**
   * fetches the project's nudge offset
   * @return a point describing the offset for printer calibration
   * @see #setNudge
   */
  public Point getNudge()
  {
    return nudge;
  }

  /**
   * fetches the project's nudge units
   * @return a point describing the offset for printer calibration
   * @see #setNudgeUnits
   */
  public String getNudgeUnits()
  {
    return nudgeUnits;
  }

  /**
   * Nudge units describe the measurement units for display in UI.
   * The units are restricted to in and mm currently.
   * @param newNudgeUnits - display units
   */
  public void setNudgeUnits(String newNudgeUnits)
  {
    nudgeUnits = newNudgeUnits;
  }

  /**
   * The BaseLayout is the filename of the AveryProject XML file that this
   * project is based on.  It is maintained for metrics reporting in PFTW.
   * @param newBaseLayout - String containing XML filename
   */
  public void setBaseLayout(String newBaseLayout)
  {
    baseLayout = newBaseLayout;
  }

  /**
   * fetches the filename of the baseLayout XML file
   * @return string containing an XML filename (may be <code>null</code>)
   * @see #setBaseLayout
   */
  public String getBaseLayout()
  {
    return baseLayout;
  }

  /**
   * The version loosely is the version of Aveysoft.xsd that a 
   * project is based on.  
   * @param newVersion - String containing version
   */
  public void setVersion(String newVersion)
  {
  	version = newVersion;
  }

  /**
   * fetches the version of Aveysoft.xsd 
   * @return string containing a version (may be <code>null</code>)
   * @see #setVersion
   */
  public String getVersion()
  {
    return version;
  }
  
  public boolean isStencil()
  {
  	return stencil;
  }
  
  /**
   * Projects brought in from XML files often need to know their location on
   * disk to resolve the location of associated image files.  This method
   * iterates through all AveryImage objects in the project, setting the
   * default image location that is used when the gallery attribute is empty.
   * @param newGallery -
   * @return the number of AveryImages that still cannot be found
   */
  public int fixGalleries(String newGallery)
  {
    int counter = 0;
    Iterator i = getMasterpanels().iterator();
    while (i.hasNext())
    {
      counter += ((AveryMasterpanel)i.next()).fixGalleries(newGallery);
    }

    i = getPages().iterator();
    while (i.hasNext())
    {
      Iterator j = ((AveryPage)i.next()).getPanels().iterator();
      while (j.hasNext())
      {
        counter += ((AveryPanel)j.next()).fixGalleries(newGallery);
      }
    }
    return counter;
  }

  public List getImageFields()
  {
  	ArrayList allImages = new ArrayList();
    Iterator i = getMasterpanels().iterator();
    while (i.hasNext())
    {
      List images = ((AveryMasterpanel)i.next()).getImageFields();
      allImages.addAll(images);
    }

    i = getPages().iterator();
    while (i.hasNext())
    {
      Iterator j = ((AveryPage)i.next()).getPanels().iterator();
      while (j.hasNext())
      {
      	List images = ((AveryPanel)j.next()).getImageFields();
        allImages.addAll(images);
      }
    }
    return allImages;
  }

  /**
   * This is a "fixup" for projects that didn't have image galleries when they
   * were created.  All AveryImage fields with empty gallery attributes are
   * assigned to the new gallery.
   * <p>Also, if a /papers directory exists under <code>newGallery</code>,
   * an attempt is made to find paperimage files and attach them to the project.
   * @param newGallery path to a directory that has highres and lowres subdirectories
   * @return the number of AveryImage fields that remain broken
   * @see AveryImage
   * @deprecated use #fixGalleries and #updatePapers instead
   */
  public int fixEmptyGalleries(String newGallery)
  {
    updatePapers(newGallery + "/papers");
    return fixGalleries(newGallery);
  }

  /**
   * makePanelImage() creates a BufferedImage of the specified AverySuperpanel.
   * @param pageNumber - the page number (1 based) that contains the panel
   *                     (use 1 for masterpanels)
   * @param description - the unique string that identifies the panel
   * @param maxWidth - maximum allowable width of image in pixels
   * @param maxHeight - maximum allowable height of image in pixels
   * @return a BufferedImage of TYPE_INT_RGB (ideal for JPEG encoding)
   * @throws Exception if something goes wrong (pageNumber out of range,
   *                    master panel not found, etc.)
  */
  public BufferedImage makePanelImage(int pageNumber, String description, int maxWidth, int maxHeight)
    throws Exception
  {
    AverySuperpanel panel = getPanel(pageNumber, description);
    return makePanelImage(panel, pageNumber, maxWidth, maxHeight);
  }

  /**
   * makePanelImage() creates a BufferedImage of the specified AverySuperpanel.
   * @param panel - the panel to render
   * @param pageNumber - the page number (1 based) that contains the panel,
   *        for rendering the paperimage (use 1 for masterpanels)
   * @param maxWidth - maximum allowable width of image in pixels
   * @param maxHeight - maximum allowable height of image in pixels
   * @return a BufferedImage of TYPE_INT_RGB (ideal for JPEG encoding)
   * @throws Exception if something goes wrong (pageNumber out of range,
   *                    master panel not found, etc.)
  */
  public BufferedImage makePanelImage(AverySuperpanel panel, int pageNumber, int maxWidth, int maxHeight)
    throws Exception
  {
    AveryMasterpanel master = (panel instanceof AveryPanel)
        ? ((AveryPanel)panel).findMasterpanel(getMasterpanels())
        : (AveryMasterpanel)panel;

    // calculate scaling factor for aspect-correct size
    double Xscale = (double)maxWidth / master.getWidth().doubleValue();
    double Yscale = (double)maxHeight / master.getHeight().doubleValue();
    double scale = (Xscale < Yscale) ? Xscale : Yscale;
    int width = (int)((scale * master.getWidth().doubleValue()) + 0.5);
    int height = (int)((scale * master.getHeight().doubleValue()) + 0.5);

    // create the BufferedImage
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = image.createGraphics();

    // add the non-printable background image
    AveryPage page = (AveryPage)getPages().get(pageNumber - 1);
    String paperimage = page.getPaperimage();

    // paint it with papercolor for starters
    graphics.setBackground(page.getPaperColor());
    graphics.clearRect(0, 0, width, height);

    if (panel instanceof AveryPanel && paperimage != null)
    {
      try
      { BufferedImage background =
          JPEGCodec.createJPEGDecoder(new FileInputStream(paperimage)).decodeAsBufferedImage();

        double xPageScale = ((double)background.getWidth()) / page.getWidth().doubleValue();
        double yPageScale = ((double)background.getHeight()) / page.getHeight().doubleValue();

        BufferedImage clipped = background.getSubimage(
          (int)((((AveryPanel)panel).getPosition().getX() * xPageScale) + 0.5),
          (int)((((AveryPanel)panel).getPosition().getY() * yPageScale) + 0.5),
          (int)(master.getWidth().doubleValue() * xPageScale),
          (int)(master.getHeight().doubleValue() * yPageScale));

        graphics.drawImage(clipped, 0, 0, width, height, null);
      }
      catch (Exception e)   // report error and move on
      {
        System.err.println(AveryUtils.timeStamp() + e.toString());
        System.err.println("  AveryProject.makePanelImage couldn't render paperimage " + paperimage);
      }
    }

    // all additional drawing is initiated by the panel
    master.setNoDrawSafeBleed(true);
    panel.draw(graphics, width, height, master);
    //master.setNoDrawSafeBleed(false);
    graphics.dispose();
    return image;
  }

  /**
   * sends a PDF to an output stream, and flushes it
   * @param stream - the stream
 * @param newFontDir - where font can be found
 * @param outlinePanels - if true, outlines are drawn around all panels
 * @param outlineFields - if true, outlines are drawn around all panel fields
   * @throws Exception - if anything goes worng
   */
  public void renderPdf(OutputStream stream, String newFontDir, boolean outlinePanels, boolean outlineFields)
  throws Exception
  {
  	// the static fontDir will be accessed by AveryTextfield objects as they output PDF
    fontDir = newFontDir;

    // using PDF library from BFO
    PDF pdfDoc = new PDF();
    
    // set version number of PDF
    pdfDoc.setOutputProfile(OutputProfile.Acrobat4Compatible);
    //pdfDoc.setOutputProfile(OutputProfile.Acrobat5Compatible);

    // document information
    pdfDoc.setInfo("Author", "Avery");
    pdfDoc.setInfo("PDF Producer","Avery Products Corp.");
    pdfDoc.setInfo("Title", "Avery Design and Print Online, v6 Document");
    pdfDoc.setInfo("Subject","Web Printing");

    // invoke print dialog when document opens
    String javascript = "pp = this.getPrintParams();";
    javascript += "fv = pp.constants.flagValues;";
    javascript += "pp.flags |= (fv.suppressRotate|fv.suppressCenter);";
    javascript += "pp.pageHandling = pp.constants.handling.none;";
    // this gives the yes no dialog
    //javascript += "pp.interactive = pp.constants.interactionLevel.silent;";
    javascript += "pp.interactive = pp.constants.interactionLevel.full;";
    javascript += "this.print(pp);";
    
    
    pdfDoc.setJavaScript(javascript);
    
    // this works too for autoprint instead of javascript but does it affect scaling?
    ///pdfDoc.setAction(Event.OPEN, PDFAction.named("Print"));
    
    // this writes doc javascript and is used to show yes no dialog
    //pdfDoc.setAction(Event.OPEN, PDFAction.formJavaScript(javascript));

    /*PDFAction act = PDFAction.formSubmit("/servlet/Submit", PDFAction.METHOD_HTTP_POST);
    Form form = pdfDoc.getForm();
    FormButton button = new FormButton();
    WidgetAnnotation annot = button.addAnnotation(pdfDoc.getLastPage(), 100,100,200,120);
    annot.setValue("Submit");
    annot.setAction(Event.MOUSEUP, act);
    form.addElement("SubmitButton", button);*/
  
    // imageCache collection prevents image redundancy in the PDF file, one table per project
    Hashtable imageCache = new Hashtable();
    
    // add pages
    Iterator iterator = pages.iterator();
    while (iterator.hasNext())
    {
      // account for nudge
      float nudgeX = (float)nudge.getX();
      float nudgeY = (float)nudge.getY();

      // set the outline argument to true to see outlines of panels and fields in the pdf.
      ((AveryPage)iterator.next()).addToPDF(pdfDoc, nudgeX, nudgeY, masterpanels, imageCache, outlinePanels, outlineFields);
    }

    // PDF is constructed.  Render to output stream
    pdfDoc.render(stream);
    stream.flush();
  }
  
  /**
   * creates a printable PDF representation of the project
   * @param filename - fully qualified name of a file to create
   * @param newFontDir - path to directory containing font files
   * @param outlinePanels - for debugging, this adds outlines to the panels in the resulting PDF
   * @return true on success, false on error.
   * @throws Exception if something went wrong
   */
  public boolean makePdfFile(String filename, String newFontDir, boolean outlinePanels)
  throws Exception
  {
    return makePdfFile(filename, newFontDir, outlinePanels, false);
  }

  /**
   * creates a printable PDF representation of the project
   * @param filename - fully qualified name of a file to create
   * @param newFontDir - path to directory containing font files
   * @param outlinePanels - for debugging, this adds outlines to the panels in the resulting PDF
   * @param outlineFields - for debugging, this adds outlines to the panel fields in the resulting PDF
   * @return true on success, false on error.
   * @throws Exception if something went wrong
  */
  public boolean makePdfFile(String filename, String newFontDir, boolean outlinePanels, boolean outlineFields)
  throws Exception
  {
  	System.setProperty("org.faceless.pdf2.NoDefaultRGB", "true");
    if (isTiledProject())
    {
      AveryProject tiledProject = createTiledProject();
      // make pdf from the project that is split into multiple tiles,
      // the created PDF is not a tiled project, its a multiple page
      // project so won't get into a loop calling this method
      return tiledProject.makePdfFile(filename, newFontDir, outlinePanels, outlineFields);
    }

    try
    {
      OutputStream fo = new FileOutputStream(filename);
      renderPdf(fo, newFontDir, outlinePanels, outlineFields);
      fo.close();
    }
    catch (Exception e)
    {
      System.err.println(AveryUtils.timeStamp() + e.getMessage());
      System.err.println("  " + e.toString());
      e.printStackTrace();
      throw e;
    }

    return true;
  }

  /**
   * creates a printable PDF representation of the project
   * @param filename - fully qualified name of a file to create
   * @param fontDir - path to directory containing PS Type 1 afm & pfm files
   * @return true on success, false on error.
   * @throws Exception if something went wrong
  */
  public boolean makePdfFile(String filename, String fontDir)
  throws Exception
  {
    return makePdfFile(filename, fontDir, false, false);
  }


  /**
   * constructs a new List of Strings, one from each masterpanel
   * @return a list of description Strings
   */
  public ArrayList getMasterPanelDescriptions()
  {
    return getPanelDescriptions(getMasterpanels());
  }

  public ArrayList getMasterDescriptions(List masters)
  {
  	ArrayList masterDescriptions = new ArrayList();
  	
  	Iterator i = masters.iterator();
  	while (i.hasNext())
  	{
  		AveryMasterpanel master = (AveryMasterpanel)i.next();
  		masterDescriptions.add(master.getDescription());
  	}
  	return masterDescriptions;
  }
  
  /**
   * constructs a new List of description Strings, one from each panel on the page
   * @param pageNumber the page number (1 based)
   * @return a List of description strings
   */
  public ArrayList getPagePanelDescriptions(int pageNumber)
  {
    return getPanelDescriptions(((AveryPage)pages.get(pageNumber - 1)).getPanels());
  }

  /**
   * This finds a panel that matches a description string.  First it searches
   * the Masterpanel list, then the specified page.
   * @param pageNumber the page number that the panel is on (1-based)
   * @param description identifies the panel
   * @return the panel found
   * @exception Exception if panel was not found
   */
  public AverySuperpanel getPanel(int pageNumber, String description)
    throws Exception
  {
    // search masterpanels first
    Iterator iterator = getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      AverySuperpanel panel = (AverySuperpanel)iterator.next();
      if (panel.getDescription().equals(description))
      {
        return panel;
      }
    }
    // not a masterpanel; search the page
    return ((AveryPage)pages.get(pageNumber - 1)).getPanel(description);
  }

  protected AveryMasterpanel getMasterpanel(String id)
  {
    Iterator iterator = getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      AveryMasterpanel panel = (AveryMasterpanel)iterator.next();
      if (panel.getID().equals(id))
      {
        return panel;
      }
    }
    return null;
  }

  protected List getSerialNumbers()
  {
  	return serialNumbers;
  }
  
  public List getMasterpanels(List masters, String description)
  {
  	ArrayList list = new ArrayList();
    Iterator iterator = masters.iterator();
    while (iterator.hasNext())
    {
      AveryMasterpanel panel = (AveryMasterpanel)iterator.next();
      if (panel.getDescription().equals(description))
      {
        list.add(panel);
      }
    }
    return list;
  }

  protected List getMasterpanels(List masters, int index)
  {
  	ArrayList list = new ArrayList();
    Iterator iterator = masters.iterator();
    while (iterator.hasNext())
    {
      AveryMasterpanel panel = (AveryMasterpanel)iterator.next();
      Description description = panel.getDescriptionObject();
      
      if (description.getIndex() == index)
      {
        list.add(panel);
      }
    }
    return list;
  }
/**
   * Creates an unsorted List of the PanelFields associated with the AveryPanel
   * that matches the given description, including those from its Masterpanel.
   * Fields from the Masterpanel are discarded when their z-order conflicts
   * with fields from the real panel.
   * @param pageNumber the page number that the panel is on (1-based)
   * @param description identifies the panel
   * @return a list of AveryPanelField objects
   * @throws Exception if the panel was not found, or if the masterpanel for
   *            the panel was not found
   */
  public java.util.List getPromptSortedPanelFields(int pageNumber, String description)
    throws Exception
  {
    return getPanel(pageNumber, description).getPromptSortedPanelFields();
  }

 /**
   * Initializes Panel Data Linking
   *
   * see AveryPage initializePanelDataLinking
   */
/*  private void initializePanelDataLinking()
  {
    // create the project field table
    fieldTable = new ProjectFieldTable();

    // initialize master panel PDL
    Iterator iMaster = masterpanels.iterator();
    while (iMaster.hasNext())
    {
      AveryMasterpanel master = (AveryMasterpanel)iMaster.next();
      int totalPanelFields = master.getPanelFields().size();
      Iterator iPanelFields = master.getPanelFields().iterator();
      while (iPanelFields.hasNext())
      {
        AveryPanelField field = (AveryPanelField)iPanelFields.next();
        field.setProjectFieldTable(fieldTable);
        // create id if it doesn't exist
        if (field.getID().length() <= 0)
          field.createID();
        else
          fieldTable.adjustRefIDCounter(field.getID());

        fieldTable.addField(field.getID(), field);
        field.setContentID(field.getID());
      }
    }

    // initialize page panel PDL
    Iterator iPages = pages.iterator();
    while (iPages.hasNext())
    {
      AveryPage page = (AveryPage)iPages.next();
      page.initializePanelDataLinking(fieldTable);
    }
  } */

 /**
   * finds the panel that the field with given field id belongs to
   * @param refID the panel field id
   * @return the AverySuperpanel object or null if not found
   */
  public AverySuperpanel findPanel(String refID)
  {
    AverySuperpanel superPanel = null;

    // check all master panels
    Iterator iterator = getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      AveryMasterpanel master = (AveryMasterpanel)iterator.next();
      superPanel = master.findPanel(refID);
      if (superPanel != null)
        break;
    }

    if (superPanel == null)
    {
      // check all pages
      iterator = getPages().iterator();
      while (iterator.hasNext())
      {
        AveryPage page = (AveryPage)iterator.next();
        superPanel = page.findPanel(refID);
        if (superPanel != null)
          break;
      }
    }

    return superPanel;
  }

 /**
   * finds the AveryPanelField top superior content field
   * @param contentID the panel field content id to return
   * @return the AveryPanelField content object matching content id or null
   */
  public AveryPanelField findContentField(String contentID)
  {
    return fieldTable.findContentField(contentID);
  }

 /**
   * finds the master top superior ID for the given content id
   * @param the content ID
   * @return the ID of the master superior or null
   */
  public String findMasterContentID(String contentID)
  {
    return fieldTable.findMasterContentID(contentID);
  }

 /**
   * updates the content id field of a panel with the new field
   * @param contentID the panel field content id
   * @param newField the panel field
   */
  public void updateContentField(AveryPanel panel, String contentID,
    AveryPanelField newField)
  {
    panel.updateContentField(contentID, newField, fieldTable);
  }

 /**
   * updates the content id field of a master panel with the new field
   * @param contentID the panel field content id
   * @param newField the panel field
   */
  public void updateContentField(AveryMasterpanel panel, String contentID,
    AveryPanelField newField)
  {
    panel.updateContentField(contentID, newField, fieldTable);
  }

 /**
   * finds the AveryPanelField top superior content field
   * @param styleID the panel field style id to return
   * @return the AveryPanelField content object matching style id or null
   */
  public AveryPanelField findStyleField(String styleID)
  {
    return fieldTable.findStyleField(styleID);
  }

 /**
   * finds the master top superior ID for the given style id
   * @param the style ID
   * @return the ID of the master superior or null
   */
  public String findMasterStyleID(String styleID)
  {
    return fieldTable.findMasterStyleID(styleID);
  }
 /**
   * updates the style id field with the new panel field
   * @param panel the panel to update
   * @param styleID the panel field style id
   * @param newField the panel field
   */
  public void updateStyleField(AveryPanel panel, String styleID,
    AveryPanelField newField)
  {
    panel.updateStyleField(styleID, newField, fieldTable);
  }

 /**
   * updates the style id field with the new master panel field
   * @param panel the panel to update
   * @param styleID the panel field style id
   * @param newField the panel field
   */
  public void updateStyleField(AveryMasterpanel panel, String styleID,
    AveryPanelField newField)
  {
    panel.updateStyleField(styleID, newField, fieldTable);
  }

    // PDL tests - do not enable in runtime
    // TODO remove these tests before deploy AP 5.x
    // useful example code for various PDL operations
/*    AveryPanelField apf = findContentField("PF30");
    AveryTextline atl = (AveryTextline)apf;
    AveryPanel ap = (AveryPanel)(getPanel(1, "Card (5)"));
    // sims a style change
    atl.setBold(true);
    ap.updateStyleField("PF30", apf, fieldTable);
    // sims a content change
//    atl.setContent("Changed Content");
//    ap.updateContentField("PF30", apf, fieldTable); */

    // tests adding a field to some panel then removing it
/*    AveryPanelField apf = findContentField("PF30");
    AveryTextline atl = (AveryTextline)apf;
    AveryTextline atlclone = (AveryTextline)atl.clone();
    atlclone.setContent("Added Content");
    atlclone.setPosition(new Point(0, 0));
    AveryPanel ap = (AveryPanel)(getPanel(1, "Card (5)"));
    addField(ap, atlclone);*/
//    removeField(ap, "PF66");

 /**
   * adds the field on the panel, and adds FieldRefs to all subordinate panels
   * (panels with the same master, below the specified panel on the page)
   * @param panel the panel to add the field to
   * @param newField the AveryPanelField to add
   */
  public void addField(AveryPanel panel, AveryPanelField newField)
  {
    newField.setProjectFieldTable(fieldTable);
    panel.addField(newField, fieldTable, getSubordinatePanels(panel));
  }

  /**
   * adds the field on the panel, and adds FieldRefs to all subordinate panels
   * @param panel the panel to add the field to
   * @param newField the AveryPanelField to add
   * @param subordinates - a list of additional panels that will receive new FieldRefs pointing upwards to newField
   */
  protected void addField(AveryPanel panel, AveryPanelField newField, List subordinates)
  {
    newField.setProjectFieldTable(fieldTable);
    panel.addField(newField, fieldTable, subordinates);
  }

 /**
   * removes the field on the panel with the id
   * @param panel the target panel
   * @param refID the panel field content id
   */
  public void removeField(AveryPanel panel, String refID)
  {
    panel.removeField(refID, fieldTable);
  }

 /**
   * finds the panel that the field with given field id belongs to
   * @param refID the panel field id
   * @return the AverySuperpanel object or null if not found
   */
  private ArrayList getSubordinatePanels(AveryPanel panel)
  {
    ArrayList subordinates = new ArrayList();

    // AverySuperpanel superPanel = null;
    Iterator panelIterator = null;

    // find page containing the panel
    Iterator iterator = getPages().iterator();
    while (iterator.hasNext())
    {
      AveryPage page = (AveryPage)iterator.next();

      Iterator panels = page.getPanels().iterator();
      while (panels.hasNext())
      {
        AveryPanel nextPanel = (AveryPanel)panels.next();
        if (panel == nextPanel)
        {
          panelIterator = panels;
          break;
        }
      }
      if (panelIterator != null)
        break;
    }

    // create list of all panels with same master panel
    AveryMasterpanel master = panel.getMasterpanel();
    while (panelIterator.hasNext())
    {
      AveryPanel nextPanel = (AveryPanel)panelIterator.next();
      if (nextPanel.getMasterpanel() == master)
        subordinates.add(nextPanel);
    }

    return subordinates;
  }

 /**
   * if field does not have a top superior field discard them
   */
  public void discardPanelFields()
  {
  	if (fieldTable == null)
  		return;
  	
    // check all pages
    Iterator iterator = getPages().iterator();
    while (iterator.hasNext())
    {
      AveryPage page = (AveryPage)iterator.next();
      page.discardPanelFields(fieldTable);
    }
  }

  /**
   * updates a field in the panel indicated by the parameters
   * @param newField this field will replace the field that has the same z-order
   *        in the panel
   * @param pageNumber the page containing the panel (ignored for masterpanels)
   * @param panelDescription used to locate the panel where the operation will
   *        occur
   * @return the old field if a field was replaced, or null if the field was
   *        simply added to the panel
   * @throws Exception if something goes horribly wrong
   * @see AverySuperpanel#updatePanelField
   */
  public AveryPanelField updatePanelField(AveryPanelField newField, int pageNumber, String panelDescription)
    throws Exception
  {
    return getPanel(pageNumber, panelDescription).updatePanelField(newField);
  }

  /**
   * updates a field in the panel indicated by the parameters.  If the
   * field is in a masterpanel, it also replace the changed attribute(s) in
   * all matching fields in all page panels that use the masterpanel.
   * @param newField this field will replace the field that has the same z-order
   *        in the panel
   * @param pageNumber the page containing the panel (ignored for masterpanels)
   * @param panelDescription used to locate the panel where the operation will
   *        occur
   * @return the old field if a field was replaced, or null if the field was
   *        simply added to the panel
   * @throws Exception if something goes horribly wrong
   */
  public AveryPanelField updateFieldMasterUberAlles(AveryPanelField newField, int pageNumber, String panelDescription)
    throws Exception
  {
    AverySuperpanel panel = getPanel(pageNumber, panelDescription);
    AveryPanelField oldField = panel.updatePanelField(newField);

    if (panel instanceof AveryMasterpanel)
    {
      java.util.BitSet flags = newField.differences(oldField);
      Iterator i = getPages().iterator();
      while (i.hasNext())
      {
        ((AveryPage)i.next()).masterUberAlles(newField, flags);
      }
    }
    return oldField;
  }

  /**
   * constructs a new List of description Strings, one from each panel
   * @param panels - a List of AverySuperpanel objects
   * @return a List of description Strings
   */
  private ArrayList getPanelDescriptions(java.util.List panels)
  {
    ArrayList strings = new ArrayList(panels.size());
    for (int i = 0; i < panels.size(); )
    {
      strings.add(((AverySuperpanel)(panels.get(i++))).getDescription());
    }
    return strings;
  }

  /**
   * Given a panel description, this method tells you whether it matches the
   * description of a masterpanel in this project.
   * @param   description - panel identification string
   * @return  <code>true</code> if there is a masterpanel that matches
   *          <code>description</code>, <code>false</code> otherwise
   */
  public boolean isMasterpanel(String description)
  {
    Iterator iterator = getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      AverySuperpanel panel = (AverySuperpanel)iterator.next();
      if (panel.getDescription().equals(description))
      {
        return true;
      }
    }

    return false;
  }

  /**
   * This method is designed to give the Navaho PFTW application exactly what
   * it needs to present the user with a list of panel descriptions.  Refer to
   * the Navaho PFTW Step 4 specification for further details.
   * @param   pageNumber - number of the page being edited (1-based)
   * @return  a List of Strings in the correct order for Navaho's listbox
   */
  public ArrayList getNavahoPanelDescriptions(int pageNumber)
  {
    // always include all masterpanels
    ArrayList list = getMasterPanelDescriptions();

    boolean noPagePanelsYet = true;
    AveryPage page = (AveryPage)pages.get(pageNumber - 1);
    Iterator i = page.getPanels().iterator();
    while (i.hasNext())
    {
      AveryPanel panel = (AveryPanel)i.next();
      // we only include panels if there are multiples that use the
      // same masterpanel (rule for Navaho 3.0)
      if (countPanelsUsingMasterpanel(panel.getMasterpanel()) > 1)
      {
        if (noPagePanelsYet)
        { // add a line to separate master panels from page panels
          list.add(new String("--------"));
          noPagePanelsYet = false;
        }

        list.add(panel.getDescription());
      }
    }

    return list;
  }

  /**
   * Sets the masterpanel field in all of the panels on all of the pages,
   * according to the MasterID.  This should be called once all page panels
   * are in place.
   * @see AveryPage#resolveMasterpanels
   */
  void resolveMasterpanels()
  {
    Iterator i = getPages().iterator();
    while (i.hasNext())
    {
      ((AveryPage)i.next()).resolveMasterpanels(getMasterpanels());
    }
  }

  /**
   * empties the PDL field table, and removes all fieldRefs from page panels
   *
   * Useful for tools such as Ape that are responsble
   * for intializing PDL panel fields for use by applications.
   * Ape etc. invoke this method just before serializing averysoft
   */
  public void clearProjectFieldTable()
  {
    fieldTable.clear();

    Iterator i = getPages().iterator();
    while (i.hasNext())
    {
      ((AveryPage)i.next()).removeAllFieldRefsFromPanels();
    }
  }

  /**
   * initializes Panel Data Linking
   *
   * @see AveryPage#intializePanelDataLinking
   */
  public void initializePanelDataLinking()
  {
    // create the project field table
    fieldTable = new ProjectFieldTable();

    // add all real panelFields into the fieldTable first
    Iterator fields = getAllPanelFields().iterator();
    while (fields.hasNext())
    {
      AveryPanelField field = (AveryPanelField)fields.next();
      field.setProjectFieldTable(fieldTable);
      // create id if it doesnt exist already
      if (field.getID().length() <= 0)
      {
      	// this should never happen
      	System.err.println("PDL alert: missing field id on " + field.toString());
        field.createID();
      }
      else
      {
      	//System.out.println("ipdl currentMPID=" + field.getID());
        fieldTable.adjustRefIDCounter(field.getID());
      }

      if (field.getContentID().length() == 0)
      {
        field.setContentID(field.getID());
      }

      fieldTable.addField(field.getID(), field);
    }

    // initialize page panel PDL
    Iterator i = getPages().iterator();
    while (i.hasNext())
    {
      ((AveryPage)i.next()).intializePanelDataLinking(fieldTable);
    }

//    fieldTable.dumpKeys(); // field table intialization is complete
  }

  public String getNextProjectFieldTableId()
  {
  	return fieldTable.nextRefID();
  }
  
  public void addFieldToMasterpanel(AveryPanelField field, AveryMasterpanel master)
  {
    // initialize field ids
    String refID = fieldTable.nextRefID();
    //System.out.println("nextID =" + refID);
    field.setID(refID);
    field.setContentID(refID);
    if (field instanceof AveryTextfield)
    {
    	field.setStyleID(refID);
    }

    // bookkeep the field in the field table
    fieldTable.addField(refID, field);

    // lastly, add it to the masterpanel
  	master.addField(field);
  }

  /**
   * This method constructs a list of <b>all</b> of the AveryPanelFields in the
   * project.  Note that there is no indication in the fields of where they
   * come from.  Please use with caution, as altering fields in this list could
   * have unpredictable result.
   * @return a list of AveryPanelFields
   * @see AveryPanelField
   */
  public java.util.List getAllPanelFields()
  {
    java.util.List list = getAllMasterpanelFields();

    Iterator i = getPages().iterator();
    while (i.hasNext())
    {
      list.addAll(((AveryPage)(i.next())).getAllPanelFields());
    }

    return list;
  }

  public java.util.List getAllMasterpanelFields()
  {
    java.util.List list = new ArrayList();
    Iterator i = getMasterpanels().iterator();
    while (i.hasNext())
    {
      list.addAll(((AverySuperpanel)(i.next())).getPanelFields());
    }

    return list;
  }
  
  public java.util.List getMasterpanelPanels(String masterID)
  {
    java.util.List list = new ArrayList();
    Iterator i = getPages().iterator();
    while (i.hasNext())
    {
      AveryPage page = (AveryPage)i.next();
      Iterator j = page.getPanels().iterator();
      while (j.hasNext())
      {
      	AveryPanel panel = (AveryPanel)j.next();
      	if (panel.getMasterID() == masterID)
      		list.add(panel);
      }
    }

    return list;
  }

  /**
   * This method generates a list of the names of all files associated with
   * the project.  Note that duplicates can occur in the list.
   * @return list of file names as Strings
   */
  public java.util.List getFileList()
  {
    java.util.List list = new ArrayList();

    // loop through the masterpanels
    Iterator i = getMasterpanels().iterator();
    while (i.hasNext())
    {
      java.util.List more = ((AverySuperpanel)(i.next())).getFileList();
      if (more != null)
      {
        list.addAll(more);
      }
    }

    // loop through the pages
    i = getPages().iterator();
    while (i.hasNext())
    {
      java.util.List more = ((AveryPage)(i.next())).getFileList();
      if (more != null)
      {
        list.addAll(more);
      }
    }
    
    if (getHint("MergeDataFileName") != null)
    {
      list.add(getHintValue("MergeDataFileName"));
    }

    return list;
  }

  /**
   * Counts the number of panels in this project that use the given masterpanel
   * @param description - the masterpanel's unique identification string
   * @return the number of panels using the given AveryMasterpanel, or zero if
   * the description doesn't match a masterpanel.
   */
  public int countPanelsUsingMasterpanel(String description)
  {
    int total = 0;
    // search masterpanels looking for match
    Iterator i = getMasterpanels().iterator();
    while (i.hasNext())
    {
      AveryMasterpanel master = (AveryMasterpanel)i.next();
      if (master.getDescription().equals(description))
      {
        // found masterpanel - do the count
        total = countPanelsUsingMasterpanel(master);
        break;
      }
    }
    return total;
  }

  /**
   * Counts the number of panels on a specific page that use a specific masterpanel
   * @param pageNumber - 1-based page number
   * @param mp - the masterpanel of interest
   * @return count
   */
  public int countPanelsUsingMasterpanel(int pageNumber, AveryMasterpanel mp)
  {
    return ((AveryPage)getPages().get(pageNumber - 1)).countPanelsUsingMasterpanel(mp);
  }

  /**
   * Counts the number of panels in this project that use the given masterpanel
   * @param master - the masterpanel of interest
   * @return the number of panels using the given AveryMasterpanel
   */
  int countPanelsUsingMasterpanel(AveryMasterpanel master)
  {
    int total = 0;
    // iterate through the pages
    Iterator i = getPages().iterator();
    while (i.hasNext())
    {
      total += ((AveryPage)i.next()).countPanelsUsingMasterpanel(master);
    }
    return total;
  }

  /**
   * Creates a buffered image containing the the full page preview of
   * the specified page, with the specified width.  Also, rotates 180 panels
   * to their upright position, per AP4 specification.
   * @param pageNumber - specified page number for the preview
   * @param width - width of the buffered image
   * @return the buffered image containing the full page preview of the
   * specified page number and width. Null is returned if error is encountered.
   * @throws Exception
   */
  public BufferedImage makePagePreview (int pageNumber, int width)
  throws Exception
  {
    return makePagePreview(pageNumber, width, true);
  }

  /**
   * Creates a buffered image containing the the full page preview of
   * the specified page, with the specified width.
   * @param pageNumber - specified page number for the preview
   * @param width - width of the buffered image
   * @param upright180 - if true, 180 degree panels will be rotated
   * to an upright position, per the AP4 spec.
   * @return the buffered image containing the full page preview of the
   * specified page number and width. Null is returned if error is encountered.
   * @throws Exception
   */
  public BufferedImage makePagePreview (int pageNumber, int width, boolean upright180)
  throws Exception
  {
    BufferedImage bufimage = null;
    AveryPage page = null;

    if ( (page = this.getPage(pageNumber)) != null)
    {
      try
      {
        bufimage = page.makePreview(width, this.getMasterpanels(), upright180);
      }
      catch (Exception e)
      {
        // temp for debugging
        System.err.println(AveryUtils.timeStamp() + e.toString());
        System.err.println("  AveryProject.makePagePreview couldn't render full page preview for Page " + pageNumber);
        e.printStackTrace(System.err);
        throw(e);
      }
    }

    return bufimage;
  }

  /**
   * Creates an AveryPage for the specified page number.
   * @param pageNumber - specified page number. 1-N; i.e., "1"
   * specifies the first page.
   * @return the AveryPage of the specified page number. Null is resturned
   * if specified page number is out of the range of the page array.
   */
  public AveryPage getPage (int pageNumber)
  {
    AveryPage page = null;

    if ( this.getNumPages() >= pageNumber )
      page = (AveryPage)getPages().get(pageNumber - 1);

    return page;
  }

  /**
   * Returns total number of pages in the page array.
   * @return total pages
   */
  public int getNumPages ()
  {
    return this.getPages().size();
  }

  /**
   * Calculates the scaling multiplier necessary to scale a rectangle to
   * a specific maximum dimension, maintaining the aspect of the rectangle.
   * @param rect - the rectangle that will be scaled
   * @param maxWidth - maximum allowed width after scaling
   * @param maxHeight - maximum allowed height after scaling
   * @return a multiplier that can be applied to source dimensions and
   * positions, to get their exivalents in the target space.
   */
  public static double calcRectScalar(Rectangle rect, double maxWidth, double maxHeight)
  {
    // calculate scaling factor for aspect-correct size
    double xScale = maxWidth / rect.getWidth();
    double yScale = maxHeight / rect.getHeight();
    return (xScale < yScale) ? xScale : yScale;
  }

  /**
   * Test to see if the project fits the criteria for Auto-Style, per section
   * 3.5 of the AveryPring 4.0 PRD.
   * <ol><li>All Mastepanels are the same size
   * <li>All Masterpanels contain the same number of elements and prompt names
   * <li>Only one text line or text box is found in each Masterpanel
   * </ol>
   * @return <code>true</code> if all of the conditions are met
   */
  public boolean allowAutoStyle()
  {
    if (masterpanels.size() > 1)
    {
      Iterator mpIterator = masterpanels.iterator();
      AveryMasterpanel firstPanel = (AveryMasterpanel)mpIterator.next();
      int numFields = firstPanel.getPanelFields().size();
      List textFields = firstPanel.getTextFields();
      if (textFields.size() == 1)
      {
        String firstPrompt = ((AveryPanelField)textFields.get(0)).getPrompt();
        String firstPO = ((AveryPanelField)textFields.get(0)).getPromptOrder();

        while(true)
        {
          if (mpIterator.hasNext())
          {
            AveryMasterpanel testPanel = (AveryMasterpanel)mpIterator.next();
            textFields = testPanel.getTextFields();

            if (testPanel.getWidth().equals(firstPanel.getWidth())
              && testPanel.getHeight().equals(firstPanel.getHeight())
              && testPanel.getPanelFields().size() == numFields
              && textFields.size() == 1)
            {
              if (firstPrompt.equals(((AveryPanelField)textFields.get(0)).getPrompt())
                && firstPO.equals(((AveryPanelField)textFields.get(0)).getPromptOrder()))
              {
                continue;   // this Masterpanel passed all tests
              }
            }
          }
          else  // tested all panels without breaking
          {
            return true;
          }

          break;
        }
      }
    }

    return false;   // some test failed
  }

  /**
   *
   * @param pageNumber - where the panel resides (1-based)
   * @param description - identifies the page panel
   * @return <code>true</code> if the specified page panel is the first instance
   * of its related masterpanel in the project, <code>false</code> otherwise
   * @throws Exception if panel was not found
   */
  public boolean isFirstPanelInstance(int pageNumber, String description)
  throws Exception
  {
    if (pageNumber == 0)    // not a page panel!
    {
      return false;
    }

    // fetch the panel in question
    AveryPanel ourPanel = (AveryPanel)((AveryPage)pages.get(pageNumber - 1)).getPanel(description);

    // loop through pages, looking for a matching panel
    Iterator pageIterator = getPages().iterator();
    while (pageIterator.hasNext())
    {
      Iterator panelIterator = ((AveryPage)pageIterator.next()).getPanels().iterator();
      while (panelIterator.hasNext())
      {
        AveryPanel panel = (AveryPanel)panelIterator.next();
        if (panel.getMasterID().equals(ourPanel.getMasterID()))
        {
          return panel.equals(ourPanel);
        }
      }
    }

    return false;  // error!
  }

  /**
   * Generates an AveryProject suitable for a .avery Zip file bundle.  Images
   * are given new, unique names starting with <code>prefix</code>, and are
   * copied to the HighRes diretory under <code>location<code>
   * @param project - receives data from <code>this</code>. 
   * @param prefix - typically the machine name
   * @param location - where the project XML will ultimately be created
   */
  protected void fillProjectForDotAveryBundle(AveryProject project, String prefix, File location)
  {
    // copy basic attributes
    project.setBaseLayout(this.getBaseLayout());
    project.setCopyright(this.getCopyright());
    project.setDescription(this.getDescription());
    project.setLanguage(this.getLanguage());
    project.setNudge(this.getNudge());
    project.setNudgeUnits(this.getNudgeUnits());
    project.setDesignCategory(this.getDesignCategory());
    project.setDesignTheme(this.getDesignTheme());
    project.setProductGroup(this.getProductGroup());
    project.setSku(this.getSku());
    project.setSkuDescription(this.getSkuDescription());

    project.fieldTable = this.fieldTable;
    project.hints = this.hints;
    project.additionalNamespaces = this.additionalNamespaces;
    project.mergeData = this.mergeData;
    project.revision = this.revision;
    project.panelSetOptions = this.panelSetOptions;

    // create a hashtable to track the images used
    Hashtable images = new Hashtable();

    // iterate through masterpanels, adding them to the new project
    Iterator masterIterator = getMasterpanels().iterator();
    while (masterIterator.hasNext())
    {
      AveryMasterpanel mpanel = (AveryMasterpanel)masterIterator.next();
      project.addMasterpanel(mpanel.getMasterpanelForDotAveryBundle(prefix, location, images));
    }

    // iterate through pages, adding them to the new project
    Iterator pageIterator = getPages().iterator();
    while (pageIterator.hasNext())
    {
      AveryPage page = (AveryPage)pageIterator.next();
      project.addPage(page.getPageForDotAveryBundle(prefix, location, images));
    }
  }

  public boolean isSingleMasterPage()
  {
  	if (masterpanels.size() > 1 || pages.size() > 1)
  		return false;
  	return true;
  }

  /**
   * Check to see if there's anything in a given masterpanel that the AP4
   * ChangeStyle servlet can work with (a background or some text).
   * @param ID - the Masterpanel ID of interest
   * @return <code>true</code> if the panel with the given ID has something
   * to offer the ChangeStyle servlet.
   */
  public boolean isStylableInAveryPrint4(String ID)
  {
    Iterator iterator = masterpanels.iterator();
    while (iterator.hasNext())
    {
      AveryMasterpanel masterpanel = (AveryMasterpanel)iterator.next();
      if (masterpanel.getID().equals(ID))
      {
        return masterpanel.isStylableInAveryPrint4();
      }
    }

    // ideally we would never reach this line
    return false;
  }

  /**
   * Locates masterpanel backdrop files identified by the naming convention
   * [sku]-m[ID].jpg and places them into the project.  If no backdrops are
   * found, the method will attempt to generate them from the page backdrop.
   * @return the number of backdrop files that were found and placed
   * @param directory - the application-specific directory where backdrop files
   * are stored.
   */
  private int locateMasterpanelBackdrops(String directory)
  {
    int found = 0;

    Iterator i = getMasterpanels().iterator();
    while (i.hasNext())
    {
      AveryMasterpanel masterpanel = (AveryMasterpanel)i.next();
      File file = new File(directory + "/" + getSku() + "-m"
                            + masterpanel.getID().toString() + ".jpg");
      if (file.exists())
      {
        masterpanel.setPaperimage(file.getAbsolutePath());
        ++found;
      }
    }

    if (found == 0)  // maybe we have to generate some
    {
      found = generateMasterBackdrops(directory, false);
    }
    return found;
  }

  /**
   * Locates page backdrop "paperimage" files identified by the naming
   * convention [sku]-p[ID].jpg and places them into the project.
   * In multi-page projects, the first page's backdrop will be assigned to
   * all pages that don't have their own backdrop.
   * @return the number of backdrop files that were found and placed
   * @param directory - the application-specific directory where backdrop files
   * are stored.
   */
  private int locatePageBackdrops(String directory)
  {
    int found = 0;
    int pagenum = 0;
    String firstBackdrop = null;

    Iterator i = getPages().iterator();
    while (i.hasNext())
    {
      ++pagenum;
      AveryPage page = (AveryPage)i.next();
      File file = new File(directory + "/U" + getSku() + "-" + pagenum + ".jpg");
      File file1 = new File(directory + "/U" + getSku() + "-" + pagenum + ".png");
      if (file.exists() || file1.exists())
      {
      	String absPath = "";
      	if (file.exists())
      		absPath = file.getAbsolutePath();
      	else
      		absPath = file1.getAbsolutePath();
        page.setPaperimage(absPath);
        ++found;

        if (pagenum == 1)
        {
          firstBackdrop = absPath;
        }
      }
      else if (firstBackdrop != null)
      {
        page.setPaperimage(firstBackdrop);
        ++found;
      }
    }
    return found;
  }

  /**
   * This function generates backdrop files for masterpanels by slicing the
   * rect from the first page's backdrop corresponding to the first panel that
   * references the masterpanel's ID.  The newly created JPG is named using
   * the convention [sku]-m[ID].jpg.  If the SKU is null, -[ID] is appended to
   * the page's paperimage filename.<br>
   * Note that the method exits quickly if the first AveryPage does not
   * contain a paperimage.
   * @return the number of backdrop files that were found and placed
   * @param directory - the application-specific directory where backdrop files
   * are stored.
   */
  protected int generateMasterBackdrops(String directory, boolean regenerate)
  {
    int generated = 0;
    AveryPage page = (AveryPage)getPages().get(0);
    if (page.getPaperimage() != null)
    {
      Iterator i = getMasterpanels().iterator();
      while (i.hasNext())
      {
        AveryMasterpanel master = (AveryMasterpanel)i.next();
        if (master.getPaperimage() == null || regenerate)
        {
          // page has paperimage, master does not.  We were born to do this!
          // Find a panel that references the master.
          AveryPanel panel = null;
          Iterator j = page.getPanels().iterator();
          while (j.hasNext())
          {
            AveryPanel p = (AveryPanel)j.next();
            if (p.getMasterID() == master.getID())
            {
              panel = p;
              break;      // found matching panel - break out of loop
            }
          }

          if (panel != null)    // if we found a panel
          {
          	try
            {
	            String name;
	            if (getSku() == null || getSku().length() == 0)
	            {
	            	name = new File(page.getPaperimage()).getName();
	            	name = name.substring(0, name.lastIndexOf('.'));
	            	name += "-" + master.getID();
	            }
	            else // name generated from SKU, as in AveryPrint
	            {
	            	name = getSku() + "-m" + master.getID();
	            }

	            File file = new File(directory, name + ".jpg");
	            if (file.exists() == false || regenerate)
	            {
	              // read the page's backdrop
	              BufferedImage backdrop = JPEGCodec.createJPEGDecoder(
	                new FileInputStream(page.getPaperimage())).decodeAsBufferedImage();

	              // calculate scalars
	              double xPageScale = ((double)backdrop.getWidth()) / page.getWidth().doubleValue();
	              double yPageScale = ((double)backdrop.getHeight()) / page.getHeight().doubleValue();

	              // clip a sub-image
	              BufferedImage clipped = backdrop.getSubimage(
	                (int)((panel.getPosition().getX() * xPageScale) + 0.5),
	                (int)((panel.getPosition().getY() * yPageScale) + 0.5),
	                (int)(master.getWidth().doubleValue() * xPageScale),
	                (int)(master.getHeight().doubleValue() * yPageScale));

              	// save clipped image to new file
	              JPEGEncodeParam jep = JPEGCodec.getDefaultJPEGEncodeParam(clipped);
	              jep.setQuality(1.0f, true);  // no degradation
              	JPEGCodec.createJPEGEncoder(new FileOutputStream(file), jep).encode(clipped);

           			++generated;    // keeping score
            	}

              // point the Masterpanel to the newly created/discovered file
              master.setPaperimage(file.getAbsolutePath());
            }
            catch (Exception e)   // report error and move on
            {
              System.err.println(AveryUtils.timeStamp() + e.toString());
              System.err.println("  failed in AveryProject.generateMasterBackdrops, sku=" + sku);
            }
          }
        }
      }
    }
    else
    {
    	generateMasterPaperColor();
    }
    return generated;
  }
  
  protected void generateMasterPaperColor()
  {
    AveryPage page = (AveryPage)getPages().get(0);
    if (page.getPaperColor() != null)
    {
      Iterator i = getMasterpanels().iterator();
      while (i.hasNext())
      {
        AveryMasterpanel master = (AveryMasterpanel)i.next();
        //if (master.getPaperColor() == null)
        {
          // page has paper color, master does not.
          master.setPaperColor(page.getPaperColor());
        }
      }
    }
  }

  /**
   * This method searches for image files in the <code>paperPath</code> that
   * match the patterns [sku]-p#.jpg and [sku]-m#.jpg, and attaches those
   * files to the paperimage attributes of pages and masterpanels, respectively.
   * It even creates masterpanel paperimage files from page paperimage files,
   * if necessary.
   * @param paperPath -
   */
  public boolean updatePapers(String paperPath)
  {
    if (getSku() != null && new File(paperPath).exists())
    {
      if (locatePageBackdrops(paperPath) > 0)
      {
        locateMasterpanelBackdrops(paperPath);
        return true;
      }
    }
    return false;
  }

  /**
   * Gives all of the AveryImage fields in the project access to a common Image cache
   */
  public void initializeImageCache()
  {
    ArrayList list = new ArrayList();
    Iterator iterator = getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      ((AverySuperpanel)iterator.next()).initializeImageCache(list);
    }

    iterator = getPages().iterator();
    while (iterator.hasNext())
    {
      ((AveryPage)iterator.next()).initializeImageCache(list);
    }
  }


  /**
   * Creates an Avery.project element to use in a JDOM Document
   * @return newly created Avery.project Element
   */
  public Element getAveryElement()
  {
    Element element = new Element("Avery.project");
    // add the attributes
    element.setAttribute("copyright", getCopyright());
   	element.setAttribute("description", getDescription());

    if (getLanguage() != null && getLanguage().length() > 0)
    {
      element.setAttribute("language", getLanguage());
    }
    if (getDesignCategory() != null && getDesignCategory().length() > 0)
    {
      element.setAttribute("projectcategory", getDesignCategory());
    }
    if (getDesignTheme() != null  && getDesignTheme().length() > 0)
    {
      element.setAttribute("subcategory", getDesignTheme());
    }

    if (getSku() != null && getSku().length() > 0)
    {
      Element skuElement = new Element("Avery.sku");
      if (getSkuDescription() != null && getSkuDescription().length() > 0)
      {
        skuElement.setAttribute("description", getSkuDescription());
      }

      skuElement.addContent(getSku());
      element.addContent(skuElement);
    }

    if (getProductGroup() != null && getProductGroup().length() > 0)
    {
    	// calling it a projectgroup is more compatible with legacy code
      Element pgElement = new Element("Avery.projectgroup");
      pgElement.addContent(getProductGroup());
      element.addContent(pgElement);
    }

    // nudge element is required
    Element nudgeElement = new Element("Avery.nudge");
    nudgeElement.addContent(
      Double.toString(getNudge().getX()) + ","
      + Double.toString(getNudge().getY()) + ","
      + getNudgeUnits());
    element.addContent(nudgeElement);

    if (getBaseLayout() != null && getBaseLayout().length() > 0)
    {
      Element blElement = new Element("Avery.baselayout");
      blElement.addContent(getBaseLayout());
      element.addContent(blElement);
    }

    // add the masterpanel elements
    Iterator iterator = getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((XMLElement)iterator.next()).getAveryElement());
    }

    // add the page elements
    iterator = getPages().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((XMLElement)iterator.next()).getAveryElement());
    }

    return element;
  }

  /**
   * Strips gallery paths from image and background field elements, and
   * removes the path information from any paperimage elements.
   * @param element - an Avery.project Element
   * @throws Exception - if Element had wrong name
   */
  public static void stripImagePaths(Element element)
  throws Exception
  {
    if (element.getName().equals("Avery.project"))
    {
      Iterator i = element.getChildren("Avery.masterpanel").iterator();
      while (i.hasNext())
      {
        Element mpElement = (Element)i.next();

        Element paperElement = mpElement.getChild("Avery.paperimage");
        if (paperElement != null)
        {
          String src = paperElement.getAttributeValue("src");
          String filename = new File(src).getName();
          paperElement.getAttribute("src").setValue(filename);
        }

        Iterator i2 = mpElement.getChildren("Avery.mp.field").iterator();
        while (i2.hasNext())  // iterate through all panel fields
        {
          stripImageFieldPaths((Element)i2.next());
        }

      }

      i = element.getChildren("Avery.page").iterator();
      while (i.hasNext())
      {
        Element pageElement = (Element)i.next();
        Element paperElement = pageElement.getChild("Avery.paperimage");
        if (paperElement != null)
        {
          String src = paperElement.getAttributeValue("src");
          String filename = new File(src).getName();
          paperElement.getAttribute("src").setValue(filename);
        }

        Iterator i2 = element.getChildren("Avery.panel").iterator();
        while (i2.hasNext())
        {
          Element pElement = (Element)i.next();
          Iterator i3 = pElement.getChildren("Avery.p.field").iterator();
          while (i3.hasNext())  // iterate through all panel fields
          {
            stripImageFieldPaths((Element)i3.next());
          }
        }
      }
    }
    else throw new Exception("AveryProject.stripImagePaths: wrong kind of Element");
  }

  /**
   * Merges panel data with current project
   * @param mergeData object to pull the data from
   * @return Merged AveryProject
   */
	public AveryProject mergeProject(MergeData mergeData)
	{
		// register the interface with Miwok
	  registerMergeData(mergeData);

//		var numPanels :Number = merge.getMergeField1().length;
//		trace("numPanels=" + numPanels);

		AveryProject mergeProject = new AveryProject();

		// clone project attributes
		clone(mergeProject);
//		mergeProject.setMerge(true);

		// add all masterpanels
		for (int i = 0; i < masterpanels.size(); i++)
		{
			AveryMasterpanel masterpanel = (AveryMasterpanel)(masterpanels.get(i));
			mergeProject.addMasterpanel(masterpanel);
		}

		// NOTE: Merge is currently limited to single master panel on a single page
		// first page of merge template is reused as template for all pages of the merged project
		AveryPage page = (AveryPage)(pages.get(0));
		// get list of page panels
		List panels = page.getPanels();

		int numPanelsPerPage = panels.size();
		int panelCount = 0;

		AveryPage mergePage = null;
		ArrayList mergePanelData = mergeData.getMergePanelData();
		while (mergePanelData != null)
		{
			if (panelCount == 0)
			{
				// create new page
				mergePage = new AveryPage();
				// copy page attributes
				mergePage.setWidth(page.getWidth());
				mergePage.setHeight(page.getHeight());
				mergePage.setMirror(page.getMirror());
//				mergePage.setPaperImage(page.getPaperImage());
				mergeProject.addPage(mergePage);
			}
			// add next page panel
			AveryPanel panel = (AveryPanel)(panels.get(panelCount++));
			ArrayList nextMergePanelData = mergeData.getMergePanelData();
/*			for (var i :Number = 0; i < mergePanelData.length; i++)
			{
				trace("Project:doneLoadingXml:mergePanelData[" + i + "]=" + mergePanelData[i]);
			}*/
			AveryPanel newPanel = mergePanel(panel, mergePanelData);
//			trace("newPanel.getPosition().x=" + newPanel.getPosition().x);
			mergePanelData = nextMergePanelData;
			mergePage.addPanel(newPanel);
			if (panelCount >= numPanelsPerPage)
			{
				panelCount = 0;
			}
		}

    // init field ids for the merged project
    mergeProject.initializePanelDataLinking();

		// debug change save method to also output to stdout when use this
//		mergeProject.save("savetest");

		// load the project
		return mergeProject;
	}

	private AveryPanel mergePanel(AveryPanel panel, ArrayList mergePanelData)
	{
//		trace("Project:Textblock mergePanel:totalPanelCount=" + totalPanelCount);
//		trace("Project:Textblock mergePanel:lastPanel=" + lastPanel);
		AveryPanel newPanel = panel.shallowClone();
		List panelFields = panel.getMasterpanel().getPanelFields();

		String fieldContent = "";

		for (int i = 0; i < panelFields.size(); i++)
		{
			AveryPanelField pf = (AveryPanelField)(panelFields.get(i));

			AveryPanelField aField = null;
			fieldContent = null;

			if (pf instanceof AveryTextblock)
			{
				AveryTextblock tb = (AveryTextblock)pf;
        // TODO where get this from??
				int mfLength = 1; //tb.getMergeFormats().size();
				// String fieldIDPrefix = "MB";
				if (mfLength > 0)
				{
					// its a merge field
          // TODO where get this from?
//					fieldContent = assembleContent(tb.getMergeFormats(), mergePanelData);
					aField = (AveryPanelField)(tb.clone());
					((AveryTextblock)aField).setTextContent(fieldContent);
//					trace("Project:Textblock mergePanel:fieldContent=" + fieldContent);
				}
				else
				{
					// not a merge field
					// fieldIDPrefix = "TB";
					aField = (AveryPanelField)(tb.clone());
				}
			}
			else if (pf instanceof AveryTextline)
			{
				AveryTextline tl = (AveryTextline)pf;
        // TODO
				int mfLength = 1; //tl.getMergeFormats().length;
				// String fieldIDPrefix = "ML";
				if (mfLength > 0)
				{
					// its a merge field
          // TODO
//					fieldContent = assembleContent(tl.getMergeFormats(), mergePanelData);
					aField = (AveryPanelField)(tl.clone());
					((AveryTextline)aField).setContent(fieldContent);
				}
				else
				{
					// not a merge field
					// fieldIDPrefix = "TL";
					aField = (AveryPanelField)(tl.clone());
				}
			}
			else if (pf instanceof AveryBackground)
			{
				aField = (AveryBackground)pf.clone();
				((AveryBackground)aField).setSourceAndGallery(fieldContent, "");
			}
			else if (pf instanceof AveryImage)
			{
				aField = (AveryPanelField)pf.clone();
				((AveryImage)aField).setSourceAndGallery(fieldContent, "");
			}
			else if (pf instanceof TextPath)
			{
				aField = (TextPath)pf.clone();
				((TextPath)aField).setContent(fieldContent);
			}
			// add the processed field to the new panel
			newPanel.addField(aField);
		}
		return newPanel;
	}

  // register the application class that supplies merge panel data
	private void registerMergeData(MergeData newMergeData)
	{
		mergeData = newMergeData;
	}

	String assembleContent(ArrayList mergeFormats, ArrayList mergePanelData)
	{
		String assembly = "";
		int mfLength = mergeFormats.size();
		for (int i = 0; i < mfLength; i++)
		{
			String mergeField = String.valueOf(mergeFormats.get(i));
//			trace("Project:assembleContent:mergeField=" + mergeField);
			int index = Integer.parseInt(mergeField.substring(2));
//			trace("Project:assembleContent:index=" + index);
//			trace("Project:assembleContent:mergePanelData[index]=" + mergePanelData[index]);
			String nextLine = (String)(mergePanelData.get(index));
			if (nextLine != "")
			{
				assembly = assembly + nextLine;
				if (i < (mfLength-1))
					assembly = assembly + "\r\n";
			}
		}
		return assembly;
	}

  /**
   * Removes gallery attributes from images and backgrounds
   * @param element - assumes a panel field element
   */
  private static void stripImageFieldPaths(Element element)
  {
    Element imageElement = element.getChild("Avery.pf.image");
    if (imageElement != null)
    {
      imageElement.getAttribute("gallery").setValue("");
      return;
    }

    Element bkgElement = element.getChild("Avery.pf.background");
    if (bkgElement != null)
    {
      bkgElement.getAttribute("gallery").setValue("");
      return;
    }

    return;
  }

  /**
   * clears the panelfields array in all masterpanels and page panels
   */
  public void removeAllPanelFields()
  {
    Iterator iterator = getPages().iterator();
    while (iterator.hasNext())
    {
      ((AveryPage)iterator.next()).removeAllPanelFields();
    }

    iterator = this.getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      ((AveryMasterpanel)iterator.next()).removeAllPanelFields();
    }
    
    initializePanelDataLinking();   // remove the fieldrefs too
  }

  public boolean reorientMasterpanel(String id, boolean clockwise)
  {
    AveryMasterpanel master = getMasterpanel(id);
    if (master == null || master.allowReorientation() == false)
    {
    	// masterpanels with polygon shapes do not allow reorientation
      return false;
    }

    // rotate panels on pages
    Iterator iterator = getPages().iterator();
    while (iterator.hasNext())
    {
      ((AveryPage)iterator.next()).reorientPanels(id, clockwise);
    }

    // swap width and height on master
		master.reorient(clockwise);

    // reorient the fields (actully, only the backgrounds change)
    iterator = master.getPanelFields().iterator();
    while (iterator.hasNext())
    {
      ((AveryPanelField)iterator.next()).reorient();
    }

    return true;
  }

  public void addMasterpanel(int pageNumber, int currentPanelDescriptionIndex)
  {  	
  	// add a masterpanel to the currently selected page
  	int numberPages = getPages().size();
		boolean isDoubleSided = getPage(1).getDoubleSided();
		AveryPage page = getPage(pageNumber);
		List masters = page.getMasterpanels();
		List masterDescriptions = getMasterDescriptions(masters);
		int numMasters = masterpanels.size();
		String masterID = "MP" + String.valueOf(numMasters + 1);
		
		// get lists of front, back, master panels
		List frontMasters = getMasterpanels(masters, FRONT_PANEL);
		List backMasters = getMasterpanels(masters, BACK_PANEL);

		if (numMasters == 1 || ((frontMasters.size() == 1 && backMasters.size() == 0) || (frontMasters.size() == 0 && backMasters.size() == 1)))
		{
			// clone an existing masterpanel and insert it
			AveryMasterpanel newMaster = createMasterpanel(masters, masterID, page.getWidth().doubleValue(), page.getHeight().doubleValue(),
																		FULL_PAGE_PANEL);
			newMaster.setNoMerge(true);
			
			// create a matching page panel
			createPanel(page, newMaster);
			
			//renumber all masterpanels
			renumberMasterPanels(page);
		}
		else
		{
			// get lists of inside master panels
			List insideTopMasters = getMasterpanels(masters, INSIDE_TOP_PANEL);
			List insideBottomMasters = getMasterpanels(masters, INSIDE_BOTTOM_PANEL);
			List insideLeftMasters = getMasterpanels(masters, INSIDE_LEFT_PANEL);
			List insideRightMasters = getMasterpanels(masters, INSIDE_RIGHT_PANEL);
			
			int panelCounter = 0;
			
			if (numberPages == 1)  /* example quarter fold card */
			{
				if ((insideLeftMasters.size() > 0 && insideRightMasters.size() > 0) && 
						currentPanelDescriptionIndex == INSIDE_LEFT_PANEL || currentPanelDescriptionIndex == INSIDE_RIGHT_PANEL)
				{
				  createMasterandPanel(masters, masterID, page, insideLeftMasters, insideRightMasters, INSIDE_SPREAD_PANEL);
				}					
				else if ((insideTopMasters.size() > 0 && insideBottomMasters.size() > 0) && 
						currentPanelDescriptionIndex == INSIDE_TOP_PANEL || currentPanelDescriptionIndex == INSIDE_BOTTOM_PANEL)
				{
				  createMasterandPanel(masters, masterID, page, insideTopMasters, insideBottomMasters, INSIDE_SPREAD_PANEL);
				}					
				else if (frontMasters.size() > 0 && backMasters.size() > 0)
				{
				  createMasterandPanel(masters, masterID, page, frontMasters, backMasters, FRONT_BACK_PANEL);
				}
			}
			else
			{
				if (frontMasters.size() > 0 && backMasters.size() > 0)
				{
				  createMasterandPanel(masters, masterID, page, frontMasters, backMasters, FRONT_BACK_PANEL);
				}
				else if (insideTopMasters.size() > 0 && insideBottomMasters.size() > 0)
				{
				  createMasterandPanel(masters, masterID, page, insideTopMasters, insideBottomMasters, INSIDE_SPREAD_PANEL);
				}
				else if (insideLeftMasters.size() > 0 && insideRightMasters.size() > 0)
				{
				  createMasterandPanel(masters, masterID, page, insideLeftMasters, insideRightMasters, INSIDE_SPREAD_PANEL);
				}
			}
		}
		// remove panelsets if they exist
		setPanelSetOptions(null);
  }
 
  void createMasterandPanel(List masters, String masterID, AveryPage page, List firstMasters, List secondMasters, int panelDescriptionIndex)
  {
		int panelCounter = 0;
		
		AveryMasterpanel firstMaster = (AveryMasterpanel)firstMasters.get(0);
		AveryMasterpanel secondMaster = (AveryMasterpanel)secondMasters.get(0);
		
		List firstPanels = getPanelsMatchingMasterID(firstMaster.getID());
		List secondPanels = getPanelsMatchingMasterID(secondMaster.getID());
		
		if (firstPanels.size() > 0 && firstPanels.size() == secondPanels.size())
		{
			AveryMasterpanel newMaster = null;
			Iterator k = firstPanels.iterator();
			Iterator n = secondPanels.iterator();
	  					
			while (k.hasNext())
			{
				AveryPanel firstPanel = (AveryPanel)k.next();
				AveryPanel secondPanel = (AveryPanel)n.next();
				
		  	ShapeIntersection si = new ShapeIntersection();
		  	Area area1 = si.createArea(firstPanel.getPosition().getX(), firstPanel.getPosition().getY(),
		  			firstMaster.getWidth().doubleValue(), firstMaster.getHeight().doubleValue(),
		  			360 - firstPanel.getRotation().doubleValue());
		  	Area area2 = si.createArea(secondPanel.getPosition().getX(), secondPanel.getPosition().getY(),
		  			secondMaster.getWidth().doubleValue(), secondMaster.getHeight().doubleValue(),
		  			360 - secondPanel.getRotation().doubleValue());
		  	si.union(area1, area2);
		  	Rectangle2D rect = si.unrotate(area1, 360 -firstPanel.getRotation().doubleValue());
		  	
		  	if (panelCounter++ == 0)
		  	{
		  		// add a master
					newMaster = createMasterpanel(masters, masterID, rect.getWidth(), rect.getHeight(), panelDescriptionIndex);
					newMaster.setNoMerge(true);
		  	}
		  	// add super sized panel
		  	double x = rect.getX();
		  	double y = rect.getY();
		  	double firstAngle = firstPanel.getRotation().doubleValue();
		  	if (firstAngle == 90)
		  	{
		  		x = y;
		  		y = -rect.getX();
		  	}
		  	if (firstAngle == 180)
		  	{
		  		x = -x;
		  		y = -y;
		  	}
		  	else if (firstAngle == 270)
		  	{
		  		x = -y;
		  		y = rect.getX();
		  	}
				createPanel(page, newMaster, firstPanel, x, y);
				
				//renumber all masterpanels
				renumberMasterPanels(page);
			}
		}
	}

  private AveryMasterpanel createMasterpanel(List masters, String masterID, double width, double height, int panelDescriptionIndex)
  {
		// clone an existing masterpanel and insert it
		AveryMasterpanel firstMaster = (AveryMasterpanel)(masters.get(0));
		AveryMasterpanel newMaster = firstMaster.cloneMaster();
		newMaster.setShape(new RectangleShape(width, height, 0));
		newMaster.setID(masterID);
		Description description = Description.makePanelDescription();
		description.setIndex(panelDescriptionIndex);
		newMaster.setDescriptionObject(description);
		
		// remove unwanted elements
		newMaster.removeCombinedMasterElements();
		
		masterpanels.add(0, newMaster);
  	
		return newMaster;
  }
  
  private AveryPanel createPanel(AveryPage page, AveryMasterpanel newMaster)
  {
		AveryPanel firstPanel = page.getPanel(0);
		AveryPanel newPanel = (AveryPanel)firstPanel.clone();
		newPanel.setMasterpanel(newMaster);
		newPanel.setMasterID(newMaster.getID());
		double angle = newPanel.getRotation().doubleValue();
		double x = 0;
		double y = 0;
		if (angle == 90)
		{
			double masterX = newMaster.getShape().getWidth();
			double masterY = newMaster.getShape().getHeight();
			newMaster.getShape().setBounds(masterY, masterX);
			y = masterY;
		}
		if (angle == 270)
		{
			double masterX = newMaster.getShape().getWidth();
			double masterY = newMaster.getShape().getHeight();
			newMaster.getShape().setBounds(masterY, masterX);
			x = masterX;
		}
		newPanel.setPosition(new Point((int)x, (int)y));
		page.getPanels().add(0, newPanel);
  	
		return newPanel;
  }

  private AveryPanel createPanel(AveryPage page, AveryMasterpanel newMaster, AveryPanel clonePanel, double x, double y)
  {
		AveryPanel newPanel = (AveryPanel)clonePanel.clone();
		newPanel.setMasterpanel(newMaster);
		newPanel.setMasterID(newMaster.getID());
		newPanel.setPosition(new Point((int)x, (int)y));
		page.getPanels().add(0, newPanel);
		
		return newPanel;
  }
  
  public void removeMasterpanel(AveryMasterpanel removeMaster)
  {
  	// remove all panels with same id as removeMaster
		Iterator i = pages.iterator();
		while (i.hasNext())
		{
			AveryPage page = (AveryPage)i.next();
			page.removePanelsMatchingMasterID(removeMaster.getID());
		}

  	// remove masterpanel
		ArrayList masterList = getMasterpanels();
		ArrayList updatedMasterList = new ArrayList();
		
		Iterator masterIterator = masterList.iterator();
		while (masterIterator.hasNext())
		{
			AveryMasterpanel master = (AveryMasterpanel)masterIterator.next();
			String masterID = master.getID();
			if (!removeMaster.getID().equals(masterID))
			{
				updatedMasterList.add(master);
			}
		}
		setMasterpanels(updatedMasterList);
		
		//renumber all masterpanels
		renumberMasterPanels(null);
  }
  
  public void renumberMasterPanels(AveryPage pg)
  {
  	ArrayList masterList = getMasterpanels();
  	Iterator masterIter = masterList.iterator();
  	int masterCount = 1;
  	ArrayList masterFromTo = new ArrayList();
  	
  	while (masterIter.hasNext())
  	{
  		AveryMasterpanel master = (AveryMasterpanel)masterIter.next();
  		List mastersPanels = getMasterpanelPanels(master.getID());
  		String masterID = "MP" + String.valueOf(masterCount++);
  		String fromToID = master.getID() + "," + masterID;
  		if (masterCount > 2)
  			masterFromTo.add(fromToID);
  		master.setID(masterID);
  		Iterator panelIter = mastersPanels.iterator();
  		while(panelIter.hasNext())
    	{
  			AveryPanel panel = (AveryPanel)panelIter.next();
  			panel.setMasterID(masterID);
    	}
  	}
  	
  	Collections.reverse(masterFromTo);
  	
  	Iterator fromToIterator = masterFromTo.iterator();
  	while (fromToIterator.hasNext())
  	{
  		String toFrom = (String)fromToIterator.next();
  		String[] split = toFrom.split(",");
  		String from = split[0];
  		String to = split[1];
  		
  		Iterator pageIterator = pages.iterator();
    	while (pageIterator.hasNext())
    	{
    		AveryPage page = (AveryPage)pageIterator.next();
    		AveryGridLayout gridLayout = page.getGridLayout(from);
    		if (gridLayout != null)
    			gridLayout.setMaster(to);
    	}
  	}
  	if (pg != null)
  	{
	  	// add a grid layout for the first master to this page
			AveryGridLayout gl = (AveryGridLayout)pg.getGridLayouts().get(0);
			double minDim = Math.min(gl.getX(), gl.getY());
			AveryMasterpanel firstMaster = (AveryMasterpanel)pg.getMasterpanels().get(0);
			AveryGridLayout gridLayout = new AveryGridLayout(firstMaster.getID(), minDim, minDim, gl.getHPitch(), gl.getVPitch(), 1, 1, false);
			pg.addGridLayout(gridLayout);
  	}
  	
  }
  
  private boolean listContains(List list, String value)
  {
  	boolean contains = false;
  	Iterator i = list.iterator();
  	while (i.hasNext())
  	{
  		String listValue = (String)i.next();
  		if (listValue.equals(value))
  		{
  			contains = true;
  			break;
  		}
  	}
  	return contains;
  }
  
  public boolean isTiledProject()
  {
  	return ((AveryPage)getPages().get(0)).isTiledProject();
  }

  // The signage equations:
	// (1) Panel size PnS = N(Dimension - 2 * Margin) - (N-1)Overlap
	// (2) Paper size PgS = Pns + 2 * Margin, this is the size of the assembled project
	// (3) Average Tile size TS = PnS / N
	// (4) PageDelta dPage = (N-1)(Dimension - 2 * Margin - Overlap)

	/**
	* Creates a multiple page project from a project that is larger
  * than the size of a page suitable for printing
	*/
	public AveryProject createTiledProject()
	{
		AveryProject splitProject = null;
		try
		{
			// create split project
			splitProject = new AveryProject();
			// clone project attributes
			clone(splitProject);

			// should be a one page project
			AveryPage page = getPage(1);

			// pages in each dimension
			int pagesWide = page.getTileCountX().intValue();
			int pagesHigh = page.getTileCountY().intValue();

			// split fields by page
			ArrayList splitPageFields = splitFieldsByPage(page, pagesWide, pagesHigh);

			// create split project pages
			addSplitPages(splitProject, page, pagesWide, pagesHigh, splitPageFields);

			// this seems to be necessary to correctly display the split project...
			//splitProject.resolveMasterpanels();
			
			// debug - write the project intended for PDF
			//Element e = splitProject.getAverysoftElement();
			//XMLOutputter out = new XMLOutputter();
			//FileOutputStream fos = new FileOutputStream(new File("c:/Predesigner/poster.xml"));
			//out.output(e, fos);
		}
		catch (Exception e)
		{
			return null;
		}

		return splitProject;
	}
	/**
	* Create split project pages
	*/
	private void addSplitPages(AveryProject splitProject, AveryPage page,
    int pagesWide, int pagesHigh, ArrayList splitPageFields)
	{
		int pageCount = 0;
		int panelPosX = 0;
		int panelPosY = 0;
		int pfCounter = 0;

		boolean portraitOrientation = true;
		if (page.getWidth().doubleValue() > page.getHeight().doubleValue())
			portraitOrientation = false;

		float orientationAngle = 0;
		if (!portraitOrientation)
			orientationAngle += 270;

		ArrayList firstPagePanels = (ArrayList)(page.getPanels());
		AveryPanel firstPanel = (AveryPanel)firstPagePanels.get(0);
		float marginX = firstPanel.getPosition().x;
		float marginY = firstPanel.getPosition().y;

		// from poster equation (4)
		double deltaPageWidth = page.getWidth().doubleValue() - 2 * marginX - page.getTileOverlapX().doubleValue();
		double deltaPageHeight = page.getHeight().doubleValue() - 2 * marginY - page.getTileOverlapY().doubleValue();

		AveryMasterpanel mp = new AveryMasterpanel();
		mp.setID("MP1");
    AveryMasterpanel origmp = (AveryMasterpanel)(this.getMasterpanels().get(0));
    mp.setShape(origmp.getShape());
		mp.setDescription("SignageMasterPanel");
		splitProject.addMasterpanel(mp);
		
		for (int j = 0; j < pagesHigh; j++)
		{
			for (int i = 0; i < pagesWide; i++)
			{
				AveryPage newpage = null;
				//AveryMasterpanel mp = null;

				for (int k = 0; k < splitPageFields.size(); k++)
				{
					FieldPageStruct fp = (FieldPageStruct)(splitPageFields.get(k));

					if (fp.pageNumber == pageCount)
					{
						if (newpage == null)
						{
							newpage = new AveryPage();
              if (portraitOrientation)
              {
                newpage.setWidth(page.getWidth());
                newpage.setHeight(page.getHeight());
              }
              else
              {
                newpage.setWidth(page.getHeight());
                newpage.setHeight(page.getWidth());
              }
							newpage.setPaperSize(page.getPaperSize());
              newpage.setTileCountX(new Integer(0));
              newpage.setTileCountY(new Integer(0));

							ArrayList panels = (ArrayList)(page.getPanels());
							AveryPanel panel = (AveryPanel)(panels.get(0));
							panelPosX = panel.getPosition().x;
							panelPosY = panel.getPosition().y;
						}
					}
				}
				if (newpage != null)
				{
					// add one instance of master panel to page
					AveryPanel panel = new AveryPanel();

					// index panel position to align panel on current page
					// the index is minus the tile dimension
					panelPosX -= i * deltaPageWidth;
					panelPosY -= j * deltaPageHeight;

					int x = panelPosX;
					int y = panelPosY;

					if (!portraitOrientation)
					{
						// landscape
						int tempy = y;
						y = x;
						x = page.getHeight().intValue() - tempy;
//						trace("landscape panel pos=" + x + "," + y);
						panel.setRotation(new Double(panel.getRotation().doubleValue() + 270.));
					}

					Point panelPoint = new Point(x, y);

					panel.setPosition(panelPoint);
					panel.setMasterID("MP1"); // + String.valueOf(pageCount + 1));
					panel.setDescription(panel.getDescription());
					newpage.addPanel(panel);

					for (int k = 0; k < splitPageFields.size(); k++)
					{
						FieldPageStruct fp = (FieldPageStruct)(splitPageFields.get(k));

						if (fp.pageNumber == pageCount)
						{
							AveryPanelField pf = fp.field;
							AveryPanelField aField = null;

							if (pf instanceof AveryTextline)
							{
								aField = (AveryTextline)pf.clone();
								aField.setStyleID("PF" + String.valueOf(pfCounter));
							}
							else if (pf instanceof AveryTextblock)
							{
								aField = (AveryTextblock)pf.clone();
								aField.setStyleID("PF" + String.valueOf(pfCounter));
							}
							else if (pf instanceof AveryBackground)
								aField = (AveryBackground)pf.clone();
							else if (pf instanceof AveryImage)
								aField = (AveryImage)pf.clone();
							else if (pf instanceof Drawing)
								aField = (Drawing)pf.clone();
							else if (pf instanceof TextPath)
							{
								aField = (TextPath)pf.clone();
								aField.setStyleID("PF" + String.valueOf(pfCounter));
							}

							aField.setPosition(new Point(pf.getPosition().x, pf.getPosition().y));
							aField.setRotation(pf.getRotation());
							aField.setPromptOrder(pf.getPromptOrder());
							aField.setZLevel(pf.getZLevel());
              aField.setZLocked(pf.isZLocked());
							aField.setID("PF" + String.valueOf(pfCounter));
							aField.setContentID("PF" + String.valueOf(pfCounter));

							panel.addField(aField);
							pfCounter++;
						}
					}
					splitProject.addPage(newpage);
				}
				pageCount++;
			}
		}
	}

	/**
	* Create a list of fields by page number
	*/
	private ArrayList splitFieldsByPage(AveryPage page, int pagesWide, int pagesHigh)
	{
		// list of fields on the page
		ArrayList splitPageFields = new ArrayList();

		ArrayList panels = (ArrayList)(page.getPanels());
		AveryPanel firstPanel = (AveryPanel)panels.get(0);
    // kludge make sure panel has unique description
    firstPanel.setDescriptionObject(Description.makePanelDescription(firstPanel.getDescription() + "000000"));
		float marginX = firstPanel.getPosition().x;
		float marginY = firstPanel.getPosition().y;
		// from poster equation (4)
		double deltaPageWidth = page.getWidth().doubleValue() - 2.0 * marginX - page.getTileOverlapX().doubleValue();
		double deltaPageHeight = page.getHeight().doubleValue() - 2.0 * marginY - page.getTileOverlapY().doubleValue();

		// tiled projects only have one master and one page panel
		AveryPanel panel = (AveryPanel)(panels.get(0));
		float panelX = panel.getPosition().x;
		float panelY = panel.getPosition().y;

		// get all panel fields from master and from page panel
		List panelFields = panel.getPanelFields();		
		ArrayList list = (ArrayList)this.getMasterpanels();
		AveryMasterpanel mp = (AveryMasterpanel)(list.get(0));
		panelFields.addAll(mp.getPanelFields());

		// walk panel fields
		for (int j = 0; j < panelFields.size(); j++)
		{
			AveryPanelField pf = (AveryPanelField)(panelFields.get(j));

			// get field corners
			Point pos = pf.getPosition();
			Point p = new Point((int)(panelX + pos.x), (int)(panelY + pos.y));
			int rotation = AveryUtils.counterRotate(pf.getRotation().intValue());
			Point p1 = new Point(p.x + pf.getWidth().intValue(), p.y);
			Point p2 = new Point(p.x + pf.getWidth().intValue(), p.y + pf.getHeight().intValue());
			Point p3 = new Point(p.x, p.y + pf.getHeight().intValue());
			if (rotation != 0)
			{
				// transform three corners of panel to rotated position
				p1 = AveryUtils.rotatePoint(p, p1, rotation);
				p2 = AveryUtils.rotatePoint(p, p2, rotation);
				p3 = AveryUtils.rotatePoint(p, p3, rotation);
			}
			int minX = Math.min(p.x, p1.x);
			int minY = Math.min(p.y, p1.y);
			int maxX = Math.max(p.x, p1.x);
			int maxY = Math.max(p.y, p1.y);
			minX = Math.min(minX, p2.x);
			minX = Math.min(minX, p3.x);
			minY = Math.min(minY, p2.y);
			minY = Math.min(minY, p3.y);
			maxX = Math.max(maxX, p2.x);
			maxX = Math.max(maxX, p3.x);
			maxY = Math.max(maxY, p2.y);
			maxY = Math.max(maxY, p3.y);

			// current field rectangle
			Rectangle panelFieldRect = new Rectangle(minX, minY, maxX, maxY);

			// current page rectangle
			int pgWidth = (int)(Math.floor(page.getWidth().doubleValue()));
			int pgHeight = (int)(Math.floor(page.getHeight().doubleValue()));

			Rectangle pageRect = new Rectangle(0, 0, pgWidth, pgHeight);

			int pageCount = 0;

			// find pages panel field overlaps
			for (int l = 0; l < pagesHigh; l++)
			{
				for (int k = 0; k < pagesWide; k++)
				{
          pageRect.x = (int)(k * deltaPageWidth);
					pageRect.y = (int)(l * deltaPageHeight);
					pageRect.width = pgWidth;
					pageRect.height = pgHeight;

					if (pageRect.intersects(panelFieldRect))
					{
						// field overlaps page
						FieldPageStruct fp = new FieldPageStruct();
						fp.pageNumber = pageCount;
						fp.field = pf;

						// list of fields to add to page
						splitPageFields.add(fp);
					}
					pageCount++;
				}
			}
		}

		return splitPageFields;
	}
	
	//public void replaceBackPage()
	public void replaceBackPage(AveryProject backProject)
	{
		// this is for testing
		//File file = new File("c:/AveryPrint6/enu/projects/en.U-0306-01.10UpDoubleSidedBusinessCardWide.Personal1.0803-01.xml");
		//AveryProject backProject = openProject(file);
		// end of testing
		
		if (isDoubleSided() && backProject.isDoubleSided())
		{		
			// remove back master panel from this project
			ArrayList frontMasters = getMasterpanels();
			frontMasters.remove(1);
			
			// add backProject's back master panel to this project
			AveryMasterpanel backMaster = (AveryMasterpanel)(backProject.getMasterpanels().get(1));
			frontMasters.add(backMaster);
			
			// remove back page from this project
			ArrayList pages = getPages();
			pages.remove(1);
			
			// add backProject's back page to this project
			AveryPage page = (AveryPage)(backProject.getPages().get(1));
			pages.add(page);
			
			// reinit of PDL links for this project
			// required because this project's back was replaced by the back of another project
	    initializePanelDataLinking();

//			saveAsXml(new File("c:/from Erric/front1back2.xml"));
		}
		
	}
  
	// only for testing
	/*	AveryProject openProject(File file)
  {
    AveryProject newProject = new AveryProject();
    newProject.copyBack = false;
    
    try
    {
      if (file.exists())
      {
        org.jdom.input.SAXBuilder builder = new org.jdom.input.SAXBuilder(true);
        builder.setFeature("http://apache.org/xml/features/validation/schema", true);
        builder.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation",
        				Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL());

        newProject.initFromElement(builder.build(file).getRootElement());
      }
      else
      {
        throw new AveryException("readProject file doesn't exist: " + file.getAbsolutePath());
      }
    }
    catch (AveryException ae)
    {
      // rethrow
      //throw ae;
    }
    catch (NullPointerException nullPointerEx)
		{
    	nullPointerEx.printStackTrace();
      //throw new AveryException("Project(File) ctor failed: " + file.getAbsolutePath());
		}
    catch (Exception e)
    {
      // convert to an AveryException to get the time stamp
      //throw new AveryException(e.toString() + "\n  Project(File) ctor failed: " + file.getAbsolutePath());
    }
    return newProject;
  }
  
  void saveAsXml(File file)
 // throws AveryException
  {
    Format format = Format.getRawFormat();
    format.setIndent(" ");
    format.setLineSeparator(System.getProperty("line.separator"));
    saveAsXml(file, format);
  }
  
	void saveAsXml(File file, Format format)
  //throws AveryException
  {
    try
    {
      org.jdom.Element projectElement = this.getAverysoftElement();
      org.jdom.Document document = new org.jdom.Document();
      document.setRootElement(projectElement);

      FileOutputStream out = new FileOutputStream(file);
      new XMLOutputter(format).output(document, out);
      out.flush();
      out.close();
    }
    catch (NullPointerException nullPointerEx)
		{
    	nullPointerEx.printStackTrace();
      //throw new AveryException("Project saveAsXml(File) failed: " + file.getAbsolutePath());
		}
    catch (Exception e)
    {
      // convert to an AveryException to get the time stamp
      AveryException ae = new AveryException(e.toString()
          + "\n  Project saveAsXml(File) failed: " + file.getAbsolutePath());
      ae.setStackTrace(e.getStackTrace());
      //throw ae;
    }
  }*/

  /**
   * Build a Point from a comma-separated pair of numbers in a String.
   * Note that while the numbers are parsed as doubles, only integer
   * precision is stored in the returned Point.  The numbers are typically
   * in twips, so errors of as much as 1/2 twip (1/2880 inch) can creep into
   * the output as a result of this conversion
   * @param str - the String to be converted
   * @return a Point generated from the String, or Point(0,0) if parsing fails
   */
  static java.awt.Point buildPoint(String str)
  {
    try
    {
      int commaIndex = str.indexOf(',');
      String strX = str.substring(0, commaIndex);
      String strY = str.substring(commaIndex + 1);

      double x = Double.parseDouble(strX);
      double y = Double.parseDouble(strY);

      return new java.awt.Point((int)x, (int)y);
    }
    catch (Exception e)
    {
      return new java.awt.Point(0,0);
    }
  }

  /**
   * Hints are stored as name/value pairs in averysoft XML,
   * and as a Hashtable in the AveryProject object.
   * @param name
   * @return the named Hint, or <code>null</code> if the Hint does not exist
   */
  public Hint getHint(String name)
  {
    if (hints != null)
    {
      return (Hint)hints.get(name);
    }
    return null;
  }

  public String getHintValue(String name)
  {
  	Hint hint = getHint(name);
  	return (hint == null) ? null : hint.getValue();
  }

  /**
   * This sets a Hint in the project.  There is validation of neither
   * name nor value.  The Hint will be written into averysoft XML.
   * @param name - the name used for the Hint
   * @param value - the value of the Hint
   */
  public void addHint(String name, String value)
  {
    if (hints == null)
    {
      hints = new Hashtable();
    }
    else if (hints.contains(name))
    {
      hints.remove(name);
    }

    hints.put(name, new Hint(name, value));
  }

  /**
   * Use this method to remove a specific Hint.
   * @param name - the name of the Hint
   * @return the Hint that was removed,
   * or <code>null</code> if the Hint didn't exist
   */
  public Hint removeHint(String name)
  {
    if (hints == null || hints.containsKey(name) == false)
    {
      return null;
    }
    Hint hint = (Hint)hints.get(name);
    hints.remove(name);
    return hint;
  }

  /**
   * generates a JDOM element, suitable for BATs or for full content designs
   * @return the averysoft.xsd-compatible element
   */
  /* (non-Javadoc)
   * @see com.avery.project.Averysoftable#getAverysoftElement()
   */

  /**
   * generates a JDOM element, suitable for BATs or for full content designs
   * @return the averysoft.xsd-compatible element
   */
  /* (non-Javadoc)
   * @see com.avery.project.Averysoftable#getAverysoftElement()
   */
  public Element getAverysoftElement()
  {
    discardPanelFields();   // remove user data fields that don't have a top superior

    // Ape is saving a predesign
    // This step is now done in com.avery.predesigner.Project derived class
    // if (fieldTable.size() == 0)     // Ape is saving a predesign
    //   initializePanelDataLinking();

    Element element = new Element("project");
    element.setNamespace(getAverysoftNamespace());

    if (additionalNamespaces != null)
    {
    	Iterator namespaces = additionalNamespaces.iterator();
    	while (namespaces.hasNext())
    	{
    		element.addNamespaceDeclaration((Namespace)namespaces.next());
    	}
    }

    // add the attributes
    element.setAttribute("copyright", getCopyright());

    if (getDescription() == null)
      setDescription("none");

    element.setAttribute("description", getDescription());
    element.setAttribute("language", getLanguage());

    if (getDesignCategory() != null && getDesignCategory().length() > 0)
    {
      element.setAttribute("designCategory", getDesignCategory());
    }

    if (getDesignTheme() != null  && getDesignTheme().length() > 0)
    {
      element.setAttribute("designTheme", getDesignTheme());
    }

    if (hints != null && !hints.isEmpty())
    {
      java.util.Enumeration e = hints.keys();

      while (e.hasMoreElements())
      {
        Hint hint = (Hint)hints.get(e.nextElement());
        element.addContent(hint.getAverysoftElement());
      }
    }

    // BATs never specify the SKU
    if (getSku() != null && getSku().length() > 0)
    {
      Element skuElement = new Element("sku");
      skuElement.setNamespace(getAverysoftNamespace());
      if (getSkuDescription() != null && getSkuDescription().length() > 0)
      {
        skuElement.setAttribute("description", getSkuDescription());
      }

      skuElement.addContent(getSku());
      element.addContent(skuElement);
    }

    if (getBaseLayout() != null && getBaseLayout().length() > 0)
    {
      Element blElement = new Element("baseLayout");
      blElement.setNamespace(getAverysoftNamespace());
      blElement.addContent(getBaseLayout());
      element.addContent(blElement);
    }

    if (getNudge().getX() != 0.0 || getNudge().getY() != 0.0)
    {
      Element nudgeElement = new Element("nudge");
      nudgeElement.setNamespace(getAverysoftNamespace());
      nudgeElement.addContent(Double.toString(getNudge().getX()) + ","
        + Double.toString(getNudge().getY()) + "," + getNudgeUnits());
      element.addContent(nudgeElement);
    }

    //String productGroup = getProductGroup();

    if (productGroup != null && productGroup.length() > 0)
    {
      Element pgElement = new Element("productGroup", "avery");
      pgElement.setNamespace(getAverysoftNamespace());
      pgElement.addContent(productGroup);
      element.addContent(pgElement);
    }

    if (getPanelSetOptions() != null)
    {
    	element.addContent(getPanelSetOptions().getAverysoftElement());
    }

    // add the masterpanel elements
    Iterator iterator = getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
    }

    // add the page elements
    iterator = getPages().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
    }

    element.setAttribute("revision", Integer.toString(getRevision()));

    if (version != null && version.length() > 0)
    	element.setAttribute("version", version);
    
    if (!isApplyToAll)
    	element.setAttribute("applyToAll", "false");
    
    if (!isMergable)
    	element.setAttribute("isMergable", "false");

    return element;

	}          	// end makeAverysoftProjectElement

  public Element getAverysoftIBElement()
  {
    discardPanelFields();   // remove user data fields that don't have a top superior

    // Ape is saving a predesign
    // This step is now done in com.avery.predesigner.Project derived class
    // if (fieldTable.size() == 0)     // Ape is saving a predesign
    //   initializePanelDataLinking();

    Element element = new Element("project");
    element.setNamespace(getAverysoftNamespace());

    if (additionalNamespaces != null)
    {
    	Iterator namespaces = additionalNamespaces.iterator();
    	while (namespaces.hasNext())
    	{
    		element.addNamespaceDeclaration((Namespace)namespaces.next());
    	}
    }

    // add the attributes
    element.setAttribute("copyright", getCopyright());

    if (getDescription() == null)
      setDescription("none");

    element.setAttribute("description", getDescription());
    element.setAttribute("language", getLanguage());

    /*if (getDesignCategory() != null && getDesignCategory().length() > 0)
    {
      element.setAttribute("designCategory", getDesignCategory());
    }

    if (getDesignTheme() != null  && getDesignTheme().length() > 0)
    {
      element.setAttribute("designTheme", getDesignTheme());
    }

    if (hints != null && !hints.isEmpty())
    {
      java.util.Enumeration e = hints.keys();

      while (e.hasMoreElements())
      {
        Hint hint = (Hint)hints.get(e.nextElement());
        element.addContent(hint.getAverysoftElement());
      }
    }*/

    // BATs never specify the SKU
    if (getSku() != null && getSku().length() > 0)
    {
      Element skuElement = new Element("sku");
      skuElement.setNamespace(getAverysoftNamespace());
      if (getSkuDescription() != null && getSkuDescription().length() > 0)
      {
        skuElement.setAttribute("description", getSkuDescription());
      }

      skuElement.addContent(getSku());
      element.addContent(skuElement);
    }

    /*if (getBaseLayout() != null && getBaseLayout().length() > 0)
    {
      Element blElement = new Element("baseLayout");
      blElement.setNamespace(getAverysoftNamespace());
      blElement.addContent(getBaseLayout());
      element.addContent(blElement);
    }*/

    // add the serial number elements
    Iterator iterator = getSerialNumbers().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((SerialNumber)iterator.next()).getAverysoftIBElement());
    }
    
    /*if (getNudge().getX() != 0.0 || getNudge().getY() != 0.0)
    {
      Element nudgeElement = new Element("nudge");
      nudgeElement.setNamespace(getAverysoftNamespace());
      nudgeElement.addContent(Double.toString(getNudge().getX()) + ","
        + Double.toString(getNudge().getY()) + "," + getNudgeUnits());
      element.addContent(nudgeElement);
    }

    String productGroup = getProductGroup();*/

    if (productGroup != null && productGroup.length() > 0)
    {
      Element pgElement = new Element("layoutID", "avery");
      pgElement.setNamespace(getAverysoftNamespace());
      pgElement.addContent(productGroup);
      element.addContent(pgElement);
    }

    /*if (getPanelSetOptions() != null)
    {
    	element.addContent(getPanelSetOptions().getAverysoftElement());
    }*/

    // add the masterpanel elements
    iterator = getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((AveryMasterpanel)iterator.next()).getAverysoftIBElement());
    }

    // add the page elements
    /*iterator = getPages().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((AveryPage)iterator.next()).getAverysoftIBElement());
    }*/

    element.setAttribute("revision", Integer.toString(getRevision()));

    if (version != null && version.length() > 0)
    	element.setAttribute("version", version);
    
    /*if (!isApplyToAll)
    	element.setAttribute("applyToAll", "false");
    
    if (!isMergable)
    	element.setAttribute("isMergable", "false");*/

    return element;

	}          	// end makeAverysoftIBProjectElement

  /* (non-Javadoc)
   * @see com.avery.project.Averysoftable#getAverysoftElement()
   */

  private static Namespace averysoftNamespace = null;
  public static Namespace getAverysoftNamespace()
  {
    if (averysoftNamespace == null)
    {
      averysoftNamespace = Namespace.getNamespace("avery", Averysoft.getAverysoftURI());
    }
    return averysoftNamespace;
  }

  /**
   * The Masterpanel ID used to be an integer, and AveryPrint 4.x relied on that
   * fact.  It turns out that it's useful for a masterpanel to know its position
   * in the masterpanel list.  This method walks the masterpanel list and sets
   * an ordinal int in each.  The ordinals are one-based, mimicing the behavior
   * of LDL's ID generation when it created XML projects.
   */
  private void setMasterpanelOrdinals()
  {
    int ordinal = 1;  // LDL always started with 1
    Iterator i = getMasterpanels().iterator();
    while (i.hasNext())
    {
      ((AveryMasterpanel)i.next()).setOrdinal(ordinal++);
    }
  }

  private void translateDescriptions(String newLanguage)
  {
		Iterator iterator = this.getAllPanelFields().iterator();
		while (iterator.hasNext())
		{
			((AveryPanelField)iterator.next()).getDescriptionObject().setLanguage(newLanguage);
		}

		iterator = getMasterpanels().iterator();
		while (iterator.hasNext())
		{
			((AveryMasterpanel)iterator.next()).getDescriptionObject().setLanguage(newLanguage);
		}

		iterator = getPages().iterator();
		while (iterator.hasNext())
		{
			((AveryPage)iterator.next()).translateDescriptions(newLanguage);
		}
  }

	public String getDesignCategory()
	{
		return designCategory;
	}

	public void setDesignCategory(String designCategory)
	{
		this.designCategory = designCategory;
	}

	public String getDesignTheme()
	{
		return designTheme;
	}

	public void setDesignTheme(String designTheme)
	{
		this.designTheme = designTheme;
	}

	public int getRevision()
	{
		return revision;
	}

	public void setRevision(int revision)
	{
		this.revision = revision;
	}

	public boolean isApplyToAll()
	{
		return isApplyToAll;
	}

	public void setApplyToAll(boolean isApplyToAll)
	{
		this.isApplyToAll = isApplyToAll;
	}

	public boolean isMergable()
	{
		return isMergable;
	}

	public void setIsMergable(boolean isMergable)
	{
		this.isMergable = isMergable;
	}

	public boolean isLegacyProject()
	{
		return isLegacyProject;
	}

	public void setLegacyProject(boolean isLegacyProject)
	{
		this.isLegacyProject = isLegacyProject;
	}

	/**
	 * This class defines tab set product options for an application that must present them
	 * to a user somehow.  See averysoft.xsd for documentation of the various attributes.
	 * @author leeb
	 * May 22, 2007
	 * PanelSetOptions
	 */
	public class PanelSetOptions implements Averysoftable
	{
		private String master;
		private int panelsPerSet = 0;
		private boolean allowGroup = true;
		private String backMaster;
		private boolean reverseBackSet = false;

		PanelSetOptions(Element element)
		{
			master = element.getAttributeValue("master");
			panelsPerSet = Integer.parseInt(element.getAttributeValue("panelsPerSet"));
			allowGroup = !("false".equals(element.getAttributeValue("allowGroup")));
			backMaster = element.getAttributeValue("backMaster");
			reverseBackSet = "true".equals(element.getAttributeValue("reverseBackSet"));
		}

		// get access
		public String getMasterID() 			{	return master; 					}
		public int getPanelsPerSet()			{	return panelsPerSet;		}
		public boolean allowGrouping()		{	return allowGroup;			}
		public String getBackMasterID()		{	return backMaster;			}
		public boolean reverseBackSets()	{	return reverseBackSet;	}

		/**
		 * Get a JDOM representation of the object suitable for
		 * inclusion in an XML file based on averysoft.xsd
		 * @return a panelSetOptions JDOM Element
		 */
		public Element getAverysoftElement()
		{
			Element element = new Element("panelSetOptions", AveryProject.getAverysoftNamespace());
			element.setAttribute("master", master);
			element.setAttribute("panelsPerSet", Integer.toString(panelsPerSet));
			if (!allowGroup)
				element.setAttribute("allowGroup", "false");
			if (backMaster != null)
				element.setAttribute("backMaster", backMaster);
			if (reverseBackSet)
				element.setAttribute("reverseBackSet", "true");
			return element;
		}
		
		public Element getAverysoftIBElement()
		{
			Element element = new Element("panelSetOptions", AveryProject.getAverysoftNamespace());
			element.setAttribute("master", master);
			element.setAttribute("panelsPerSet", Integer.toString(panelsPerSet));
			if (!allowGroup)
				element.setAttribute("allowGroup", "false");
			if (backMaster != null)
				element.setAttribute("backMaster", backMaster);
			if (reverseBackSet)
				element.setAttribute("reverseBackSet", "true");
			return element;
		}
	}

	// instance
	private PanelSetOptions panelSetOptions;

  public boolean hasPanelSetOptions()
  {
    return panelSetOptions != null;
  }

	private void setPanelSetOptions(PanelSetOptions options)
	{
		panelSetOptions = options;
	}

	public PanelSetOptions getPanelSetOptions()
	{
		return panelSetOptions;
	}

  /**
   * Removes all content from page panels that reference
   * the panelSetOptions' master and backmaster panels
   */
  protected void removePanelSets()
  {
    if (panelSetOptions == null)
    {
      return;
    }

    List panels = getPanelsMatchingMasterID(panelSetOptions.getMasterID());
    panels.addAll(getPanelsMatchingMasterID(panelSetOptions.getBackMasterID()));

    Iterator i = panels.iterator();
    while (i.hasNext())
    {
      ((AveryPanel)i.next()).clearContent(fieldTable);
    }
  }


  /**
   * loops through all pages, gather panels into a List
   * @param masterID - the ID to match, or even <code>null</code>
   * @return a new List which could be empty
   */
  protected List getPanelsMatchingMasterID(String masterID)
  {
    return getPanelsMatchingMasterID(masterID, false);
  }
  
  /**
   * loops through all pages, gather panels into a List
   * @param masterID - the ID to match, or even <code>null</code>
   * @param useAltSortOrder - if true, the panels from each page are sorted according
   * to the value returned by {@link AveryPanel#getAltSortOrder()}
   * @return a list of AveryPanels (could be empty)
   */
  protected List getPanelsMatchingMasterID(String masterID, boolean useAltSortOrder)
  {
    List list = new ArrayList();
    Iterator i = pages.iterator();
    while (i.hasNext())
    {
      AveryPage page = (AveryPage)i.next();
      List panels = useAltSortOrder ? page.getAltSortedPanels(true) : page.getPanels();
      Iterator j = panels.iterator();
      
      while (j.hasNext())
      {
        AveryPanel panel = (AveryPanel)j.next();
        if (panel.getMasterID().equals(masterID))
          list.add(panel);
      }
    }
    return list;
  }
  
  /**
   * Used for adding content from a database to all matching panels
   * @param masterID the ID of the masterpanel to match
   * @param fillDown <code>true</code> for "Fill Down" ordering,
   * <code>false</code> for "Fill Across" ordering
   * @return list of matching panels to be filled, in designated order
   */
  protected List getFillOrderedPanels(String masterID, boolean fillDown)
  {
    List list = new ArrayList();
    Iterator i = pages.iterator();
    while (i.hasNext())
    {
      AveryPage page = (AveryPage)i.next();
      List panels = fillDown ? page.getXSortedPanels() : page.getYSortedPanels();
      Iterator j = panels.iterator();
      while (j.hasNext())
      {
        AveryPanel panel = (AveryPanel)j.next();
        if (panel.getMasterID().equals(masterID))
          list.add(panel);
      }
    }
    return list;
  }

  public AveryPanel getFirstPanelMatchingMasterID(String masterID)
  {
    Iterator i = pages.iterator();
    while (i.hasNext())
    {
      Iterator j = ((AveryPage)i.next()).getPanels().iterator();
      while (j.hasNext())
      {
        AveryPanel panel = (AveryPanel)j.next();
        if (panel.getMasterID().equals(masterID))
          return panel;
      }
    }
    return null;
  }

  // used to create tiled (signage) projects
  private class FieldPageStruct extends Object
  {
    public int pageNumber;
    public AveryPanelField field;
  }

  /**
   * implementation of Printable.print() interface
   */
  public int print(Graphics g, PageFormat pf, int page)
  throws PrinterException
  {
    if (page >= pages.size())
    {
      // tell the caller that it can't be done
      return Printable.NO_SUCH_PAGE;
    }
    try
    {
      getPage(page+1).drawPanels((Graphics2D)g, 0.05, masterpanels, false);
    }
    catch (Exception ex)
    {
      PrinterException pex = new PrinterException(ex.getMessage());
      pex.setStackTrace(ex.getStackTrace());
      throw pex;
    }

    // tell the caller that this page is now printed
    return Printable.PAGE_EXISTS;
  }

  public boolean hasMasterpanelFields()
  {
    Iterator i = masterpanels.iterator();
    while (i.hasNext())
    {
      if (((AveryMasterpanel)i.next()).getPanelFields().size() > 0)
      {
        return true;
      }
    }
    return false;
  }
  
  public void replaceStyle(StyleSet original, StyleSet replacement)
  {
    Iterator fields = getAllPanelFields().iterator();
    while (fields.hasNext())
    {
      ((AveryPanelField)fields.next()).replaceStyle(original, replacement);
    }
    setDesignTheme(replacement.name);
  }
  
  /**
   * This is a line-by-line translation technique, used to do translations from the SampleText spreadsheet.
   * @param hash - keys are text to translate, values are the translations
   * @return number of translation failures
   */
  public int translateText(Hashtable hash)
  {
    int failures = 0;
    Iterator fields = getAllPanelFields().iterator();
    while (fields.hasNext())
    {
      AveryPanelField field = (AveryPanelField)fields.next();
      if (field instanceof AveryTextfield)
      {
        failures += ((AveryTextfield)field).translateText(hash);
      }
    }
    return failures;
  }
  
  /**
   * Tests whether the project matches the style of the default "placeholder" StyleSet  
   * @return <code>true</code> if this is a placeholder predesign
   */
  public boolean isTruePlaceholderPredesign()
  {
    if (designTheme == null || designTheme.equals("placeholder"))
    {
      List masterFields = getAllMasterpanelFields();
      if (masterFields.size() == 0)
      {
        return false;   // not a predesign - no 
      }
      
      StyleSet placeholder = StyleSet.newStyleSet();
      
      Iterator fields = masterFields.iterator();
      while (fields.hasNext())
      {
        if (!((AveryPanelField)fields.next()).matchesStyle(placeholder))
        {
          return false;   // a panelField failed the test - no need to go on
        }
      }
      
      return true;  // all tests passed
    }
    
    return false;   // failed first test
  }
  
  /**
   * @return <code>true</code> if any of the pages have the doubleSided attribute set
   */
  public boolean isDoubleSided()
  {
    Iterator i = pages.iterator();
    while (i.hasNext())
    {
      if (((AveryPage)i.next()).getDoubleSided())
        return true;
    }
    return false;
  }
  
  // only need one barcode for QRCode case
  public void makeBarcodeImage(String outputDirURL, String projectFileName)
  {
	  String barcodeImage = outputDirURL + "/" + projectFileName;
	  barcodeImage = barcodeImage.substring(0, barcodeImage.indexOf(".xml")) + ".png";
	  
	  // start with panel fields 
	  List panelFields = getAllPanelFields();
	  for (int i = 0; i < panelFields.size(); i++)
	  {
	  	AveryPanelField panelField = (AveryPanelField)panelFields.get(i);
	  	if (panelField instanceof Barcode)
	  	{
	  		Barcode barcode = (Barcode)panelField;
	  		if (barcode.getBarcodeType().equals("Intelligent Mail") ||
	  				barcode.getBarcodeType().equals("QRCode") )
	  		{
	  			barcode.makeImage(barcodeImage);
	  			break;
	  		}
	  	}
	  }
  }
  
  //this is the IM barcode case
  public void makeBarcodeImages(String webRoot, String deploy, String projectFileName, String barcodeDataFile)
  {
  	//String webRoot = "c:/AveryPrint6/";
  	//String deploy = "enu";
  	//String baseFileName = "c:/AveryPrint6/enu/";
  	
  	if (barcodeDataFile != null)
  		mergeBarcodePages(webRoot + deploy + "/" + barcodeDataFile + ".txt");  		// testing ONLY
  	
	  // create barcode images if any barcodes exist
	  String instanceName;
	  String baseName = webRoot + "html/flash/output/" + projectFileName;
	  baseName = baseName.substring(0, baseName.indexOf(".xml"));
	  
	  // start with panel fields 
	  List panelFields = getAllPanelFields();
	  for (int i = 0; i < panelFields.size(); i++)
	  {
	  	instanceName = baseName;
	  	AveryPanelField panelField = (AveryPanelField)panelFields.get(i);
	  	if (panelField instanceof Barcode)
	  	{
	  		Barcode barcode = (Barcode)panelField;
	  		if (barcode.getBarcodeType().equals("Intelligent Mail") ||
	  				barcode.getBarcodeType().equals("QRCode") )
	  		{
	  			instanceName += barcode.getID() + ".png";
	  			barcode.makeImage(instanceName);    			
	  		}
	  	}
	  }
	  
	  // now do field refs
	  for (int i = 0; i < pages.size(); i++)
	  {
	  	AveryPage page = (AveryPage)pages.get(i);
	  	
	  	List panels = page.getPanels();
	  	for (int j = 0; j < panels.size(); j++)
	  	{
	  		AveryPanel panel = (AveryPanel)panels.get(j);
	  		List fieldRefs = panel.getFieldRefs();
	  		for (int k  = 0; k < fieldRefs.size(); k++)
	  		{
	  	  	instanceName = baseName;
	  			FieldRef field = (FieldRef)fieldRefs.get(k);
	  			AveryPanelField panelField = fieldTable.findContentField(field.getContentID());
	  	  	if (panelField instanceof Barcode)
	  	  	{
	  	  		Barcode barcode = (Barcode)panelField;
	  	  		if (barcode.getBarcodeType().equals("Intelligent Mail") ||
	  	  				barcode.getBarcodeType().equals("QRCode"))
	  	  		{
	  	  			instanceName += field.getID() + ".png";
	  	  			barcode.makeImage(instanceName);    			
	  	  		}
	  	  	}
	  			
	  		}
	  	}
	  }
  }
 
  public void mergeBarcodePages(String mergeFile)
  {
  	FileArrayProvider fap = new FileArrayProvider();
  	ArrayList bcData = null;
  	int bcCount = 0;
  	
  	try
  	{
  		bcData = fap.readLines(mergeFile);
  	}
  	catch (Exception e){ return; }
  	
	  // merge barcode images if any barcodes exist
		String barcodeData = "01234567900987654321";
		Barcode barcode = null; //new Barcode();
		//barcode.setBarcodeType("Intelligent Mail");
		//barcode.setBarcodeData(barcodeData);
	  
	  // start with panel fields - predesigns only have these in masterpanel
	  List panelFields = this.getAllMasterpanelFields(); //getAllPanelFields();
	  AveryMasterpanel mp = (AveryMasterpanel)(this.getMasterpanels().get(0));
	  AveryPage firstPage = (AveryPage)(this.getPages().get(0));
	  AveryPanel firstPanel= (AveryPanel)(firstPage.getPanels().get(0));
	  String description = mp.getDescription();
	  int found = 0;
	  for (int i = 0; i < panelFields.size(); i++)
	  {
	  	AveryPanelField panelField = (AveryPanelField)panelFields.get(i);
	  	if (panelField instanceof Barcode)
	  	{
	  		barcode = (Barcode)panelField;
	  		if (barcode.getBarcodeType().equals("Intelligent Mail") ||
	  				barcode.getBarcodeType().equals("QRCode"))
	  		{
	  			//barcodeData = barcode.getBarcodeData();
	  			//barcodeData = barcodeData.substring(0, barcodeData.length() - 1);
	  			//barcodeData += "0";
	  			/// only if have panel fields not field refs from predesign
	  			///barcode.setBarcodeData((String)(bcData.get(bcCount++)));
	  			found++;
	  		}
	  	}
	  	else
	  	{
	  		// convert all other masterpanel fields to panel fields in first panel only
	  		try
	  		{
	  			AveryPanelField apf = panelField.deepClone();
		  		List fieldRefs = firstPanel.getFieldRefs();
		  		for (int k  = 0; k < fieldRefs.size(); k++)
		  		{
		  			FieldRef field = (FieldRef)fieldRefs.get(k);
		  			if (field.getContentID().equals(panelField.getID()))
		  			{
			  			String ID = field.getID();
			  			// set panel field ids to deleted field refs to maintain pointers
	  	  			apf.setID(ID);
	  	  			apf.setContentID(ID);
	  	  			if (apf.getStyleID().length() > 0)
	  	  				apf.setStyleID(ID);
	  	  			fieldRefs.remove(k);
	  	  			break;
		  			}
		  		}
	  			firstPanel.addField(apf);
	  		}
	  		catch (Exception e){}
	  	}
	  }
	  
	  // now do field refs
	  for (int i = 0; i < pages.size(); i++)
	  {
	  	AveryPage page = (AveryPage)pages.get(i);
	  	
	  	List panels = page.getPanels();
	  	int start = 0;
	  	//if (found == 0)
	  		//start = 1;
 	  	for (int j = start; j < panels.size(); j++)
	  	{
	  		AveryPanel panel = (AveryPanel)panels.get(j);
	  		List fieldRefs = panel.getFieldRefs();
	  		for (int k  = 0; k < fieldRefs.size(); k++)
	  		{
	  			FieldRef field = (FieldRef)fieldRefs.get(k);
	  			AveryPanelField panelField = fieldTable.findContentField(field.getContentID());
	  	  	if (panelField instanceof Barcode)
	  	  	{
	  	  		Barcode barcode1 = (Barcode)panelField;
	  	  		if (barcode != null)
	  	  		{
	  	  			barcode1.setMovable(barcode.isMovable());
	  	  			barcode1.setPrint(barcode.getPrint());
	  	  		}
	  	  		
	  	  		if (barcode1.getBarcodeType().equals("Intelligent Mail"))
	  	  		{
	  	  			// convert to panel field from field ref
	  	  			
	  	  			String id = fieldTable.nextRefID();
	  	  			barcode1.setID(id);
	  	  			barcode1.setContentID(id);
	  	  			barcode1.setGeometryId(id);
	  	  			//System.out.println("id=" + id);
	  	  			
	  	  			//barcodeData = barcode.getBarcodeData();
	  	  			//barcodeData = barcodeData.substring(0, barcodeData.length() - 1);
	  	  			//barcodeData += "1";
	  	  			barcode1.setBarcodeData((String)(bcData.get(bcCount++)));	  			
	  	  			barcode1.setDescription("IMBarcode" + id);
	  	  			
	  	  			panel.addField(barcode1);
	  	  			fieldTable.addField(id, barcode1);
	  	  		}
	  	  		else if (barcode1.getBarcodeType().equals("QRCode"))
	  	  		{
	  	  			// convert to panel field from field ref
	  	  			
	  	  			String id = fieldTable.nextRefID();
	  	  			barcode1.setID(id);
	  	  			barcode1.setContentID(id);
	  	  			barcode1.setGeometryId(id);
	  	  			//System.out.println("id=" + id);
	  	  			
	  	  			//barcodeData = barcode.getBarcodeData();
	  	  			//barcodeData = barcodeData.substring(0, barcodeData.length() - 1);
	  	  			//barcodeData += "1";
	  	  			///barcode1.setBarcodeData((String)(bcData.get(bcCount++)));	  			
	  	  			barcode1.setDescription("QRCode" + id);
	  	  			
	  	  			panel.addField(barcode1);
	  	  			fieldTable.addField(id, barcode1);
	  	  		}
	  	  	}	  			
	  		}
	  	}
	  }
	  // remove barcode field refs
	  for (int i = 0; i < pages.size(); i++)
	  {
	  	AveryPage page = (AveryPage)pages.get(i);
	  	
	  	List panels = page.getPanels();
	  	for (int j = 0; j < panels.size(); j++)
	  	{
	  		AveryPanel panel = (AveryPanel)panels.get(j);
	  		List fieldRefs = panel.getFieldRefs();
	  		for (int k  = 0; k < fieldRefs.size(); k++)
	  		{
	  			FieldRef field = (FieldRef)fieldRefs.get(k);
	  			AveryPanelField panelField = fieldTable.findContentField(field.getContentID());
	  	  	if (panelField instanceof Barcode)
	  	  	{
	  	  		Barcode barcode1 = (Barcode)panelField;
	  	  		if (barcode1.getBarcodeType().equals("Intelligent Mail") ||
	  	  				barcode1.getBarcodeType().equals("QRCode"))
	  	  		{
	  	  			fieldTable.remove(field.getID());
	  	  		}
	  	  	}	  			
	  		}
	  	}
	  }
	  
	  // for flash print no master panel fields allowed because created page panel fields
	  mp.removeAllPanelFields();

  	/*File file = new File("c:/AveryPrint6/javathehut.xml");
    try
    {
      Element projectElement = this.getAverysoftElement();
      org.jdom.Document document = new org.jdom.Document();
      document.setRootElement(projectElement);

      FileOutputStream out = new FileOutputStream(file);
      new XMLOutputter(Format.getRawFormat()).output(document, out);
      out.flush();
      out.close();
    }
    catch (NullPointerException nullPointerEx)
		{
    	nullPointerEx.printStackTrace();
		}
    catch (Exception e)
    {
      // convert to an AveryException to get the time stamp
      AveryException ae = new AveryException(e.toString()
          + "\n  Project saveAsXml(File) failed: " + file.getAbsolutePath());
      ae.setStackTrace(e.getStackTrace());
     }*/
  }
  
  // add a backdrop image to a page
  // example usage for first page of project:
  //AveryBackground field = addPageBackdrop(1);
  //field.setSourceAndGallery("A-APPLE.png", "");

  public AveryBackground addPageBackdrop(int pageNum)
  {
  	AveryBackground field = null;
  	
  	// get page to add backdrop to
  	AveryPage page = getPage(pageNum);  	
  	
  	// get first master on the page
  	AveryMasterpanel firstMaster = (AveryMasterpanel)(this.getMasterpanels().get(0));
  	// create a page sized master to place the page backdrop on
  	AveryMasterpanel master = new AveryMasterpanel();
  	int masters = this.getMasterpanels().size();
  	String mpID = String.valueOf(masters + 1);
  	master.setID(mpID);
  	master.setShape(new RectangleShape(page.getWidth().doubleValue(),
  			page.getHeight().doubleValue(), 0));
  	master.setBleed(false); 	
  	((AverySuperpanel)master).setDescriptionObject(Description.makePageDescription());
  	this.addMasterpanel(master);
  	
  	// create an instance of the new masterpanel as first panel on the page
  	AveryPanel panel = new AveryPanel();
  	panel.setMasterID(mpID);
  	panel.setPosition(new Point(0, 0));
  	panel.setMasterpanel(master);
  	((AverySuperpanel)panel).setDescriptionObject(Description.makePageDescription());
  	List panels = page.getPanels();
  	panels.add(0, panel);
  	
  	// create the background
    field = new AveryBackground();
    field.translateDescription(getLanguage());
  	field.setPosition(new Point(0, 0));
  	field.setWidth(page.getWidth());
  	field.setHeight(page.getHeight());
    field.setPromptOrder("1.0");
    field.setEditable(false);
    field.setMovable(false);
    field.setZLevel(1.0);
    
    addFieldToMasterpanel(field, master);
 	
  	// allow caller to customize background including settinig source and gallery
  	return field;
  }
  
  public boolean multiplyMasters()
  {
  	ArrayList masterList = getMasterpanels();
  	int numMasters = masterList.size();
  	int IDCounter = numMasters + 1;
		ArrayList clonedMasters = new ArrayList();
	  	
		// limit to projects with four or less masterpanels
  	if (numMasters <= 4)
  	{
  		// if any master has more than 12 page panels do not proceed
  		/*for (int i = 0; i < masterList.size(); i++)
  		{
  			AveryMasterpanel master = (AveryMasterpanel)masterList.get(i);
  			List panels = getPanelsMatchingMasterID(master.getID());
  			if (panels.size() > 12)
  				return false;
  		}*/
  		
  		for (int i = 0; i < masterList.size(); i++)
  		{
  			AveryMasterpanel master = (AveryMasterpanel)masterList.get(i);
  			master.removeAllPanelFields();
  			List panels = getPanelsMatchingMasterID(master.getID());
  			if (panels.size() < 2)
  				continue;
  			for (int j = 1; j < panels.size(); j++)
  			{
  				// clone the master
  				AveryMasterpanel cloneMaster = master.cloneMaster();
  				String description = master.getDescription();
  				cloneMaster.setDescription(description + " " + (j + 1));
  				AveryPanel panel = (AveryPanel)panels.get(j);
   				Iterator iterator = panel.getFieldRefs().iterator();
					//System.out.println("j=" + j);
					while (iterator.hasNext())
  				{
  					FieldRef ref = (FieldRef)iterator.next();
  					//System.out.println("fieldRef ID=" + ref.getID());
					  panel.removeField(ref.getID(), fieldTable);		
  				}
  				String newID = "MP" + IDCounter++ + "-" + master.getID();
  				panel.removeAllPanelFields();
  				panel.setMasterID(newID);
  				cloneMaster.setID(newID);
  				//System.out.println(cloneMaster.getDescription());
  				clonedMasters.add(cloneMaster);
  			}
  		}
  		masterList.addAll(clonedMasters);
  		
  		// remove gridlayout from all pages
      /*Iterator i = getPages().iterator();
      while (i.hasNext())
      {
      	AveryPage page = (AveryPage)i.next();
      	page.removeGridLayout();
      }*/
  		
  		return true;
  	}
  	return false;
  	
  }
  
  public boolean unmultiplyMasters()
  {
  	try
  	{
  		ArrayList masterList = getMasterpanels();
  		ArrayList originalMasterList = new ArrayList();
  		
  		Iterator masterIterator = masterList.iterator();
  		while (masterIterator.hasNext())
  		{
  			AveryMasterpanel master = (AveryMasterpanel)masterIterator.next();
  			String masterID = master.getID();
  			int indexMaster = masterID.indexOf("-MP");
  			if (indexMaster <= 2)
  			{
  				originalMasterList.add(master);
  			}

  			//System.out.println(masterID);
 			
  			Iterator fieldIterator = master.getFieldIterator();
  			
  			List panels = getPanelsMatchingMasterID(master.getID());
  			if (panels.size() > 0)
  			{			
					AveryPanel panel = (AveryPanel)panels.get(0);
					while (fieldIterator.hasNext())
					{
						AveryPanelField field = (AveryPanelField)fieldIterator.next();
						AveryPanelField cloneField = field.deepClone();
					  panel.addField(cloneField);
					}
	  			if (indexMaster > 2)
	  			{
	  				String newID = master.getID().substring(indexMaster + 1);
	  				panel.setMasterID(newID);
	  			}
	  			else
	  			{
	  				master.removeAllPanelFields();
	  			}
  			}
  		}
  		setMasterpanels(originalMasterList);
  	}
  	catch (Exception e)
  	{
  		e.printStackTrace(System.out);
  	}
 		return true;

  	//return false;
  	
  	
  }
  
  // project limitations are max of two sheets and max of two master panels
  public void customPanelGroups(ArrayList groups, AveryMasterpanel master) //, int mpCount)
  {  	
  	Iterator groupIterator = groups.iterator();
  	int numberGroups = groups.size();
  	if (numberGroups < 2)
  		return;
  	
		String description = master.getDescription();
		
		// create a list of current related master panels
		ArrayList currentMasterList = new ArrayList();
		currentMasterList.add(master);
		
  	// clone master panels to end up with same number of masters as groups	
		int masterOffset = masterpanels.size();
  	for (int i = 1; i < (numberGroups); i++)
  	{
  		AveryMasterpanel newMaster = master.cloneMaster();
  		newMaster.setID("MP" + (i + masterOffset));
			newMaster.setDescription(description + " G" + (i + 1));
   		Iterator pfIterator = master.getFieldIterator();
  		while (pfIterator.hasNext())
  		{
  			AveryPanelField pf = (AveryPanelField)pfIterator.next();
  			AveryPanelField pfClone = pf.deepClone();
  			
  			String nextID = fieldTable.nextRefID();
  			pfClone.setID(nextID);
  			pfClone.setContentID(nextID);
  			if (pfClone.getStyleID() != null)
  				pfClone.setStyleID(nextID);
  			newMaster.addField(pfClone);
  		}
   		this.addMasterpanel(newMaster);
   		currentMasterList.add(newMaster);
  	}

		master.setDescription(description + " G1");
  	
  	// setup to relink all fieldrefs of first panel that references a new master panel
		
		// create list of panels that link to the original master panel
  	AveryPage page = (AveryPage)this.pages.get(0);
  	ArrayList currentPanels = new ArrayList();
  	Iterator panelIterator = page.getPanels().iterator();
  	while (panelIterator.hasNext())
  	{
  		AveryPanel panel = (AveryPanel)panelIterator.next();
  		if (master.getID().equals(panel.getMasterID()))
  		{
  			currentPanels.add(panel);
  			//System.out.println(panel.getFieldRefs().size());
  		}
  	}
  	if (getNumPages() == 2)
  	{
  		page = (AveryPage)this.pages.get(1);
  		panelIterator = page.getPanels().iterator();
    	while (panelIterator.hasNext())
    	{
    		AveryPanel panel = (AveryPanel)panelIterator.next();
    		if (master.getID().equals(panel.getMasterID()))
    			currentPanels.add(panel);
    	}
  	}
  		
  	panelIterator = currentPanels.iterator();
  		
  	ArrayList fieldRefs = new ArrayList();
  	ArrayList newRefIDs = new ArrayList();
  	  	
  	Iterator mpIterator = currentMasterList.iterator();
  	
  	int groupNumber = 0;
  	
  	// update panel master IDs to point to correct master panel
  	while (groupIterator.hasNext() && mpIterator.hasNext())
  	{
  		AveryMasterpanel mp = (AveryMasterpanel)mpIterator.next();
  		
  		Integer numberInGroup = (Integer)(groupIterator.next());
  		int j = numberInGroup.intValue();
  		
  		String mpID = mp.getID();
  		
  		for (int k = 0; k < j; k++)
  		{
  			if (panelIterator.hasNext())
  			{
	  			AveryPanel panel = (AveryPanel)panelIterator.next();
	  			panel.setMasterID(mpID);
	  			panel.setMasterpanel(mp);
	  			
	  			// make panel fields link to their new master panel
	  			// only link the first page panel to its master panel
	  			// groupNumber = 0 means original master panel skip it
	  			if (k == 0 && groupNumber != 0)
	  			{
	  				Iterator pfDest = panel.getFieldRefs().iterator();
	  				
		  			while (pfDest.hasNext())
	  				{
	  					FieldRef apfr = (FieldRef)pfDest.next();
	  	  			AveryPanelField apfp = (AveryPanelField)(fieldTable.getContentField(apfr.getID()));
	  	  			
	  					String apfpDescription = apfp.getDescription();
	  	  			//System.out.println("apfpDescription=" + apfpDescription);
	  					AveryPanelField apfs = getMasterPanelFieldByDescription(mp, apfpDescription);
	  	  			//System.out.println("apfsDescription=" + apfs.getDescription());
	  					if (apfs != null)
	  					{
	  						// save fieldrefs with their new link ids, cannot write them until process all master panels
		  					String pfID = apfs.getID();
		  					fieldRefs.add(apfr);
		  					newRefIDs.add(pfID);
		  	  			//System.out.println(pfID + ": " + apfr);
	  					} 						
	  				}
	  			}
	  		}
  		}
  		groupNumber++;		
  	}
  	
  	// update field refs of first panel that references each new master panel to link to those master panels
  	Iterator fieldRefsIterator = fieldRefs.iterator();
  	Iterator newRefIDsIterator = newRefIDs.iterator();
  	while (fieldRefsIterator.hasNext() && newRefIDsIterator.hasNext())
  	{
  		FieldRef fr = (FieldRef)fieldRefsIterator.next();
  		String linkID = (String)newRefIDsIterator.next();
			fr.setContentID(linkID);
			if (fr.getStyleID() != null && fr.getStyleID().length() > 0)
				fr.setStyleID(linkID);
		}
  }
  
  // get master panel field matching a description
  private AveryPanelField getMasterPanelFieldByDescription(AveryMasterpanel mp, String description)
  {
  	AveryPanelField pf = null;
  	
  	Iterator fieldIterator = mp.getFieldIterator();
  	while (fieldIterator.hasNext())
  	{
  		AveryPanelField mpf = (AveryPanelField)fieldIterator.next();
  		if (description.equals(mpf.getDescription()))
			{
  			pf = mpf;
  			break;
			}
  	}
  	return pf;
  }
}