/*
 * Created on Jun 4, 2004
 */
package com.avery.project;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import javax.imageio.ImageIO;

import org.faceless.pdf2.BarCode;
import org.faceless.pdf2.PDFCanvas;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFStyle;

import org.jdom.Attribute;
import org.jdom.Element;

import com.avery.predesign.StyleSet;
import com.avery.utils.AveryUtils;

/**
 * Implementation of a Barcode field
 */
public class Barcode extends AveryPanelField implements Averysoftable
{
  // Barcode-specific data serialized to XML attributes
  private String barcodeType;
  private String barcodeData;
  // private double fontPointSize = 10.0;
  // private String fontFaceName = "Arial";
  private boolean checkDigit = false;
  // private boolean displayCheckDigit;
  private boolean displayPlainText = true;
  // private boolean autoSize;
  // private double minModuleWidth;
  // private double moduleRatio;
  // private boolean displayStartStopCode39;
  // private double zoomFactor;

  private Hashtable hints = null;

  // mergeMap support
  private String mergeKey = new String();

  /**
   * creates a barcode element
   * 
   * @param type -
   *          barcode types as defined by BFO
   * @param data -
   *          the data to be encoded
   * @param position -
   *          upper left corner of the field, in TWIPS
   */
  public Barcode(String type, String data, Point position)
  {
    super();
    setBarcodeData(data);
    setBarcodeType(type);
    setPosition(position);
    BarCode tempCode = createBFOBarCode(type, data);
    setWidth(new Double(tempCode.getWidth() * 20.0f));
    setHeight(new Double(tempCode.getHeight() * 20.0f));
  }

  public Barcode()
  {
    super();
  }

  /**
   * creats a barcode from a JDOM XML element
   * 
   * @param element
   * @throws Exception
   */
  protected Barcode(Element element) throws Exception
  {
    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace()) && element.getName().equals("barcode"))
    {
      readSuperAverysoftAttributes(element);

      Attribute a;
      setBarcodeType(element.getAttribute("barcodeType").getValue());
      
      String barCodeData = "";
      if (element.getAttribute("barcodeData") != null)
      	barCodeData = element.getAttribute("barcodeData").getValue();
      setBarcodeData(barCodeData);
      a = element.getAttribute("checkDigit");
      setCheckDigit(a == null ? false : a.getBooleanValue());
      a = element.getAttribute("displayPlainText");
      setDisplayPlainText(a == null ? true : a.getBooleanValue());
      a = element.getAttribute("merge");
      setMergeKey(a == null ? new String() : a.getValue());

      Element descriptionElement = element.getChild("description", AveryProject.getAverysoftNamespace());
      if (descriptionElement != null)
      {
        setDescriptionObject(Description.makeBarcodeFieldDescription(descriptionElement));
      }
      
      Iterator iterator = element.getChildren("property", AveryProject.getAverysoftNamespace()).iterator();
      while (iterator.hasNext())
      {
        Element hintElement = (Element)iterator.next();
        String name = hintElement.getAttributeValue("name");
        String value = hintElement.getTextTrim();
        if (name.equals("data"))
        	setBarcodeData(value);
        else
        	addHint(name, value);
      }

      
      // this is for Phil - to make Intelligent Mail barcode
      //if (getBarcodeType().equals("Intelligent Mail"))
      //{
      	//makeImage("c:/AveryPrint6/html/flash/output/");
      //}
    }
    else if (!initFromAveryDtdElement(element))
    {
      throw new Exception("Not a Barcode element");
    }
  }

  private boolean initFromAveryDtdElement(Element element) throws Exception
  {
    if (element.getName().equals("Avery.pf.barcode"))
    {
      Attribute a;
      setBarcodeType(element.getAttribute("barcodeType").getValue());
      setBarcodeData(element.getAttribute("barcodeData").getValue());
      a = element.getAttribute("checkDigit");
      setCheckDigit(a == null ? false : a.getBooleanValue());
      a = element.getAttribute("displayPlainText");
      setDisplayPlainText(a == null ? true : a.getBooleanValue());

      readSuperAttributes(element.getParent());

      return true;
    }

    return false;
  }

  public void addHint(String name, String value)
  {
    if (hints == null)
    {
      hints = new Hashtable();
    }
    else if (hints.contains(name))
    {
      hints.remove(name);
    }

    hints.put(name, new Hint(name, value));
  }


  private ArrayList bars = new ArrayList();

  /**
   * @see com.avery.project.AveryPanelField#draw(java.awt.Graphics2D, double)
   */
  void draw(Graphics2D gr, double dScalar)
  {
    if (getMask() != null)
    {
      getMask().clipImageMask(gr, dScalar);
    }
    drawOutlineElement(gr, dScalar);
    // todo: real barcode screen display
    

    // draw a fake bar code
    final int WIDE = 4;
    final int SPACE = 3;
    final int NARROW = 2;
    
    final int TOPCENTER = 1;
    final int CENTER = 2;
    final int CENTERBOTTOM = 3;
    final int TOPCENTERBOTTOM = 4;

    int x0 = (int) (getPosition().getX() * dScalar);
    int y0 = (int) (getPosition().getY() * dScalar);
    int width = (int) (getWidth().doubleValue() * dScalar);
    int height = (int) (getHeight().doubleValue() * dScalar);
    double fieldRotation = getRotation().doubleValue() * Math.PI / 180.0;

    gr.setColor(Color.black);
    gr.translate(x0, y0);
    gr.rotate(-fieldRotation);
    
    if (barcodeType.equals("QRCode"))
    {
    	org.faceless.pdf2.BarCode barcode = org.faceless.pdf2.BarCode.newQRCode(barcodeData, 0.5, 2, 0);
	    BufferedImage image = barcode.getBufferedImage(1, Color.white);
	    //gr.drawString("QRCode", 0, 0);
      gr.drawImage(image, 0, 0, width, height, null);
    }
    else
    {
	    Random rand = null;
	    while (bars.size() < width / 2)
	    {
	      // make sure that we have enough bar sizes
	      if (rand == null)
	      {
	        rand = new Random(System.currentTimeMillis());
	      }
	      
	      float rando = rand.nextFloat();
	      if (!(getBarcodeType().equals("Intelligent Mail")))
	      	bars.add(new Integer((rando > 0.5) ? WIDE : NARROW));
	      else
	      {
	      	if (rando < .25)
	      		bars.add(new Integer(TOPCENTER));
	      	else if (rando < .5)
	      		bars.add(new Integer(CENTER));
	      	else if (rando < .75)
	      		bars.add(new Integer(CENTERBOTTOM));
	      	else
	      		bars.add(new Integer(TOPCENTERBOTTOM));
	      }
	    }

	    Iterator barIterator = bars.iterator();
	    int size = ((Integer) (barIterator.next())).intValue(); // size of first bar
	    if (!(getBarcodeType().equals("Intelligent Mail")))
	    {
		    // line
		    int x = size / 2;
		
		    while (x + size <= width) // while there's space to fill
		    {
		      gr.setStroke(new BasicStroke(size));
		      gr.drawLine(x, size / 2, x, height - (size / 2));
		
		      // add bar size + constant space to x position
		      x += size + SPACE;
		      // get size for next bar
		      size = ((Integer) (barIterator.next())).intValue();
		    }
	    }
	    else
	    {
	    	// line
	    	int x = NARROW / 2;
		    
	    	while (x + 1 <= width) // while there's space to fill
		    {
		      gr.setStroke(new BasicStroke(1));
		      if (size == TOPCENTER)
		      	gr.drawLine(x, height, x, height / 3);
		      else if (size == CENTER)
		      	gr.drawLine(x, 2 * height / 3, x, height / 3);
		      else if (size == CENTERBOTTOM)
		      	gr.drawLine(x, 2 * height / 3, x, 0);
		      else
		      	gr.drawLine(x, height, x, 0);
		
		
		      // add bar size + constant space to x position
		      x += NARROW / 2 + SPACE;
		      // get size for next bar
		      size = ((Integer) (barIterator.next())).intValue();
		    }
	    }
    }
    // put things back where we found them.
    gr.setStroke(new BasicStroke(1));
    gr.rotate(fieldRotation);
    gr.translate(-x0, -y0);

    gr.setClip(null);
    if (getMask() != null && getMask().incrementIndex())
    {
      draw(gr, dScalar);
    }
  }
  
  public void makeImage(String imageFilename)
  {
  	try
  	{
	    //PDF.setLicenseKey("GH34B7C5EBFFH73");
  		// double the preview size to increase its resolution when viewed in flash
	    BarCode barcode = null;
	    
	    if (barcodeType.equals("Intelligent Mail"))
	    		barcode = BarCode.newIntelligentMail(barcodeData);
	    else if (barcodeType.equals("QRCode"))
	    {
	    		double modulesize = 0.5;
	    		barcode = BarCode.newQRCode(barcodeData, modulesize, 2, 0);
	    }
	    BufferedImage image = barcode.getBufferedImage(2, Color.white);
	    ImageIO.write(image, "png", new File(imageFilename));
  	}
  	catch (Exception e){} 	
  }

  /**
   * @see com.avery.project.AveryPanelField#addToPDF(org.faceless.pdf2.PDFPage,
   *      java.awt.Point, java.lang.Double, java.lang.Double,
   *      java.util.Hashtable)
   */
  public void addToPDF(PDFPage pdfPage, Double dPanelHeight, Hashtable pdfImageCache) throws Exception
  {
    addOutlineToPDF(pdfPage, dPanelHeight);
    int canvasStartX = 0;
    int canvasStartY = 0;
    double heightCalibration = 0;
    if (getPosition().x < 0)
    {
      canvasStartX = (int) (getPosition().x / 20f);
    }
    if (getPosition().y < 0)
    {
      canvasStartY = (int) (getPosition().y / 20f);
      heightCalibration = - getPosition().y;
    }

		int areaSize = getAreaSize(pdfPage, canvasStartX, canvasStartY);
		
    PDFCanvas contentContainer = new PDFCanvas(areaSize, areaSize, 0xff);
    
    if (getMask() != null)
    {
      getMask().clipMask(contentContainer, canvasStartX, canvasStartY);
    }
    // convert position to points
    float positionx = (float) (getPosition().x) / 20.0f;
    float positiony = (float) (dPanelHeight.intValue() - getPosition().y) / 20.0f;

    float width = getWidth().floatValue() / 20f;
    float height = getHeight().floatValue() / 20f;

    // translate xml barcode type to bfo barcode int
    // int bfoBarcodeType = getBfoBarcodeType(barcodeType);

    try
    {
      // construct a BFO Barcode object
    	
      // jdom reader strips whitespace so convert special sequence back to cr lf
      String crlf = "///r///n";
    	String delimitBarcodeData = barcodeData.replaceAll(crlf, "\r\n");

      BarCode code = createBFOBarCode(barcodeType, delimitBarcodeData);
      PDFCanvas barCanvas = code.getCanvas();
      
      float codeWidth = barCanvas.getWidth();
      float codeHeight = barCanvas.getHeight();
      // notice that if set the alpha to 0 the entire barcode becomes invisible???
      Color transparentColor = new Color(0, 0, 0, 1);
      PDFStyle style = new PDFStyle();
      style.setFillColor(transparentColor);
      //style.setFillColor(Color.white);
      style.setLineColor(null);
      style.setLineWeighting(0);
      barCanvas.setStyle(style);
      barCanvas.drawRectangle(0, 0, codeWidth, codeHeight);
      barCanvas.drawCanvas(code.getCanvas(), 0, 0, codeWidth, codeHeight);
      
      // BarCode(bfoBarcodeType,
      // barcodeData);
      code.setShowText(displayPlainText);

      // field rotation
      double fieldRotation = getRotation().doubleValue();

      // handle panel and/or field rotations
      if (fieldRotation != 0.0)
      {
        // don't let concats concat inside the rotate
        contentContainer.save();
        // rotate the page
        transformPDFField(contentContainer, new Double(dPanelHeight.doubleValue() + heightCalibration), positionx - canvasStartX,
            positiony - canvasStartY, fieldRotation);
        // draw the Barcode object
        contentContainer.drawCanvas(barCanvas, 0, -height, width, 0);
        // forget the rotate operations
        contentContainer.restore();
      }
      else
      // not rotated; simply render the barcode at the correct size and position
      {
        contentContainer.drawCanvas(barCanvas, positionx - canvasStartX, positiony - height - canvasStartY, positionx
            + width - canvasStartX, positiony - canvasStartY);
      }
    }
    catch (Exception e)
    {
      System.err.println(AveryUtils.timeStamp() + e.getMessage());
      System.err.println("  Unable to write barcode " + barcodeType + " to pdf file");
    }
    pdfPage.drawCanvas(contentContainer, canvasStartX, canvasStartY, areaSize + canvasStartX, areaSize + canvasStartY);
    if (getMask() != null && getMask().incrementIndex())
    {
      addToPDF(pdfPage, dPanelHeight, pdfImageCache);
    }
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAveryElement()
  {
    Element element = super.getParentAveryElement();
    Element content = new Element("Avery.pf.barcode");
    content.setAttribute("barcodeType", getBarcodeType());
    content.setAttribute("barcodeData", getBarcodeData());
    content.setAttribute("checkDigit", checkDigit ? "true" : "false");
    content.setAttribute("displayPlainText", displayPlainText ? "true" : "false");
    element.addContent(content);
    return element;
  }

  /**
   * @return String containing data to be encoded
   */
  public String getBarcodeData()
  {
    return barcodeData;
  }

  /**
   * @return String describing the barcode
   */
  public String getBarcodeType()
  {
    return barcodeType;
  }

  /**
   * @param string
   */
  public void setBarcodeData(String string)
  {
    barcodeData = string;
  }

  /**
   * Valid barcode type strings in the XML are:
   * <ul>
   * <li>"EAN 8"</li>
   * <li>"EAN 13"</li>
   * <li>"EAN 128" *</li>
   * <li>"Code 128"</li>
   * <li>"Code 39"</li>
   * <li>"2/5 Standard"</li>
   * <li>"2/5 Interleaved"</li>
   * <li>"Codabar"</li>
   * <li>"UPC-A"</li>
   * <li>"UPC-E"</li>
   * <li>"Postnet"</li>
   * <li>"Telepen"</li>
   * <li>"LOGMARS"</li>
   * <li>"MSI"</li>
   * <li>"Code 11"</li>
   * <li>"Code 93"</li>
   * <li>"PDF417"</li>
   * <li>"Data Matrix ECC 200"</li>
   * <li>"MaxiCode"</li>
   * <li>"PLANET"</li>
   * <li>"RM4SCC"</li>
   * <li>"QRCode"</li>
   * </ul>
   * These are enumerated in Averysoft.vsd for XML serialization.
   * 
   * @param type -
   *          describes the barcode type
   */
  void setBarcodeType(String type)
  {
    barcodeType = type;
    if (!Arrays.asList(supportedBarcodeTypes).contains(type))
    {
      System.err.println("Warning - BFO PDF library doesn't supported BarCode type: \"" + type + "\"");
    }
  }

  /**
   * It's dangerous to change barcode type and data independently. By setting
   * them together, we get the benefit of BFO's validation. Width and height are
   * adjusted to match the new barcode type.
   * 
   * @param type
   *          the barcodeType
   * @param data
   *          the barcodeData
   * @throws Exception
   *           thrown if BFO's barcode validation fails
   */
  public void resetBarcode(String type, String data) throws Exception
  {
    BarCode tempCode = createBFOBarCode(type, data);
    if (tempCode != null)
    {
      if (getBarcodeType().equals(type) == false)
      {
        // adjust size for new type
        setWidth(new Double(tempCode.getWidth() * 20.0f));
        setHeight(new Double(tempCode.getHeight() * 20.0f));
      }
      setBarcodeData(data);
      setBarcodeType(type);
    }
  }

  /**
   * Invokes a BarCode factory method to generate a PDF BarCode object
   * 
   * @param type -
   *          barcodeType string from AveryDoc XML attribute
   * @param value -
   *          the data to be encoded
   * @return a new PDF Barcode object, or <code>null</code> is type is not
   *         supported
   */
  BarCode createBFOBarCode(String type, String value)
  {
    if ("Postnet".equals(type))
    {
      return BarCode.newPostnet(value);
    }

    if ("EAN 13".equals(type) || "UPC-A".equals(type))
    {
      // UPC-A is EAN-13 with the first digit set to 0
      return BarCode.newEAN13(value);
    }

    if ("Codabar".equals(type))
    {
      return BarCode.newCodabar(value, getWidth().floatValue() / 20.0f);
    }

    if ("Code 128".equals(type))
    {
      return BarCode.newCode128(value);
    }

    if ("Code 39".equals(type))
    {
      return BarCode.newCode39(value, checkDigit);
    }

    if ("Code 39X".equals(type))
    {
      return BarCode.newCode39Extended(value, checkDigit);
    }

    if ("2/5 Interleaved".equals(type))
    {
      return BarCode.newInterleaved25(value, checkDigit);
    }

    if ("2/5 Interleaved Deutschenpost".equals(type) || "DeutschePost".equals(type))
    {
      return BarCode.newDeutschePostCode(value);
    }

    if ("MaxiCode".equals(type))
    {
      boolean eec = false; // whether to use Extended Error Correction (mode
      // 5).
      return BarCode.newMaxiCode(value, eec);
    }

    if ("RM4SCC".equals(type))
    {
      return BarCode.newRM4SCC(value);
    }

    if ("PDF417".equals(type))
    {
      return BarCode.newPDF417(value);
    }

    if ("QRCode".equals(type))
    {
    	//double modulesize = 0.5;
    	// module size is in mm, assume avg of 50 modules in width and height, if below .5, make .5
    	double modulesize = 1.0; //(getWidth() / 1440.0) * 25.4 / 50;
    	try
    	{
    		//String crlf = String.valueOf(0x0d) + String.valueOf(0x0a);
    		//System.out.print(value);
    		//String newValue = value.replaceAll("&#xD;&#xA;", "\r\n");
	      return BarCode.newQRCode(value, modulesize, 2, 0);
    	}
    	catch (Exception e){}
//    	return BarCode.newQRCode("Too much data!", modulesize, 2, 0);
    	return null;
    }

    if ("Intelligent Mail".equals(type))
    {
      return BarCode.newIntelligentMail(value);
    }

    if ("Data Matrix ECC 200".equals(type))
    {
      return BarCode.newDataMatrixCode(value);
    }
    
    System.err.println("Unsupported barcode type: " + barcodeType);
    return null;
  }

  /**
   * @param b
   *          set or clear the Checksum attribute
   */
  public void setCheckDigit(boolean b)
  {
    checkDigit = b;
  }

  /**
   * @param b
   *          set or clear the plain text display
   */
  public void setDisplayPlainText(boolean b)
  {
    displayPlainText = b;
  }

  /**
   * @return <code>true</code> if a checksum digit is to be included in the
   *         barcode
   */
  public boolean isCheckDigit()
  {
    return checkDigit;
  }

  /**
   * @return <code>true</code> if plain text is to be displayed along with the
   *         barcode
   */
  public boolean isDisplayPlainText()
  {
    return displayPlainText;
  }

  // only the types supported by BFO are listed
  private static String supportedBarcodeTypes[] =
  { "EAN 13", "UPC-A", "Code 128", "Code 39", "Code 39X", "2/5 Interleaved", "DeutschePost", "Intelligent Mail", "Postnet",
      "RMS4CC", "Codabar", "PDF417", "MaxiCode", "QRCode", "Data Matrix ECC 200",};

  /**
   * Returns the barcode types currently supported.
   * 
   * @return
   */
  public static String[] getSupportedBarcodeTypes()
  {
    return supportedBarcodeTypes;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("barcode");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    applyParentAverysoftAttributes(element);

    element.setAttribute("barcodeType", getBarcodeType());
    element.setAttribute("barcodeData", getBarcodeData());

    if (isCheckDigit())
    {
      element.setAttribute("checkDigit", "true");
    }
    if (!isDisplayPlainText())
    {
      element.setAttribute("displayPlainText", "false");
    }

    if (getMergeKey().length() > 0)
    {
      element.setAttribute("merge", getMergeKey());
    }

    if (getDescriptionObject() == null)
    {
      setDescriptionObject(Description.makeBarcodeFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
	  position.setAttribute("x", Double.toString(getPosition().getX()));
	  position.setAttribute("y", Double.toString(getPosition().getY()));		
    element.addContent(position);

    return element;
  }

	public Element getAverysoftIBElement()
	{
    Element element = new Element("barcode");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    applyParentAverysoftAttributes(element);

    element.setAttribute("barcodeType", getBarcodeType());
    element.setAttribute("barcodeData", getBarcodeData());

    if (isCheckDigit())
    {
      element.setAttribute("checkDigit", "true");
    }
    if (!isDisplayPlainText())
    {
      element.setAttribute("displayPlainText", "false");
    }

    if (getMergeKey().length() > 0)
    {
      element.setAttribute("merge", getMergeKey());
    }

    // hints
    if (hints != null && !hints.isEmpty())
    {
      java.util.Enumeration e = hints.keys();

      while (e.hasMoreElements())
      {
        Hint hint = (Hint)hints.get(e.nextElement());
        element.addContent(hint.getAverysoftElement());
      }
    }


    /*if (getDescriptionObject() == null)
    {
      setDescriptionObject(Description.makeBarcodeFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());*/

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());		
		position.setAttribute("x", AveryProject.twipsToMM(getPosition().getX()));
		position.setAttribute("y", AveryProject.twipsToMM(getPosition().getY()));	
    element.addContent(position);

    return element;
	}

  protected void addDefaultDescriptionObject()
  {
    setDescriptionObject(Description.makeBarcodeFieldDescription());
  }

  public void setMergeKey(String mergeKey)
  {
    this.mergeKey = mergeKey;
  }

  /**
   * access to the key for mergeMap operations
   * 
   * @return the key ("af0", "af1", etc.) or an empty string if there is no key.
   */
  public String getMergeKey()
  {
    return mergeKey;
  }

  /**
   * there are no style attributes associated with Barcode objects, so this
   * implementation does nothing
   */
  void replaceStyle(StyleSet original, StyleSet replacement)
  {
  }

  /**
   * there are no style attributes associated with Barcode objects
   * 
   * @return <code>true</code> always
   */
  boolean matchesStyle(StyleSet ss)
  {
    return true;
  }

  /**
   * Creates a deep clone
   * 
   * @return deep clone
   */
  public AveryPanelField deepClone()
  {
    Barcode barcode = new Barcode(getBarcodeType(), getBarcodeData(), getPosition());
    copyPrivateData(barcode);

    barcode.checkDigit = checkDigit;
    barcode.displayPlainText = displayPlainText;
    barcode.mergeKey = mergeKey;
    return barcode;
  }
}
