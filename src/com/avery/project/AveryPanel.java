/**
 * Title:       AveryPanel<p>
 * Description: The AveryPanel stores AveryPanelField objects that are drawn
 *              on the printed page.  It is associated with an AveryMasterpanel
 *              which may have additional AveryPanelFields to be drawn at this
 *              AveryPanel's position.<p>
 * Copyright:   Copyright (c) 2013-2015 Avery Products Corp. All Rights Reserved.
 * @author      Bob Lee, Brad Nelson
 * @version     2.0
 */

package com.avery.project;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import org.faceless.pdf2.PDFCanvas;
import org.faceless.pdf2.PDFImage;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFStyle;
import org.jdom.Element;
//import org.omg.CORBA.portable.OutputStream;


import com.avery.Averysoft;
import com.avery.project.shapes.PolygonShape;
import com.avery.utils.AveryException;
import com.avery.utils.AveryUtils;

public class AveryPanel extends AverySuperpanel implements Cloneable, Averysoftable
{
  public AveryPanel()
  { }

  private String masterID;
  private Point position;
  private Double rotation = new Double(0.0);
  private AveryMasterpanel masterpanel;
  private int altSortOrder = 0;
  private int number;
  private int ofHowMany;
  
  // bleed in points (9 points = 170 twips = 3mm)
  public static float BLEED_DIMENSION = 9f;
  
  private List fieldRefs = new LinkedList();

  private ProjectFieldTable projectFieldTable;
  
  /**
   * constructs a panel from an Avery.panel XML Element
   * @param element - describes the panel
   * @throws Exception if element isn't up to snuff
   */
  AveryPanel(Element element)
  throws Exception
  {
		if (element.getNamespace().equals(AveryProject.getAverysoftNamespace())
			&& element.getName().equals("panel"))
		{
			setMasterID(element.getAttributeValue("master"));
			setPosition(AveryProject.buildPoint(element.getAttributeValue("position")));
			setRotation(element.getAttributeValue("rotation"));
			String str = element.getAttributeValue("altSortOrder");
			if (str != null)
			{
				altSortOrder = Integer.decode(str).intValue();
			}

			Element descriptionElement = element.getChild("description", AveryProject.getAverysoftNamespace());
			if (descriptionElement != null)
			{
				super.setDescription(descriptionElement);
			}
			
			// read panel fields
      Iterator iterator = element.getChildren().iterator();
      while (iterator.hasNext())
      {
        Element child = (Element)iterator.next();

        AveryPanelField field = null;
        if (child.getName().equals("textBlock"))
        {
          field = new AveryTextblock(child);
        }
        else if (child.getName().equals("textLine"))
        {
          field = new AveryTextline(child);
        }
        else if (child.getName().equals("image"))
        {
          field = new AveryImage(child);
        }
        else if (child.getName().equals("background"))
        {
          field = new AveryBackground(child);
        }
        else if (child.getName().equals("textPath"))
        {
          field = new TextPath(child);
        }
        else if (child.getName().equals("barcode"))
        {
          field = new Barcode(child);
        }
        else if (child.getName().equals("drawing"))
        {
          field = new Drawing(child);
        }

        if (field != null)
        {
          addField(field);
        }

        FieldRef fieldRef = null;
        if (child.getName().equals("fieldRef"))
        {
          fieldRef = new FieldRef(child);
          addFieldRef(fieldRef);
        }
      }
		}
		else if (!initFromAveryDtdElement(element))
		{
			Exception ex = new AveryException("Not an Avery panel element");
			ex.printStackTrace();
			throw ex;
		}
  }

	private boolean initFromAveryDtdElement(Element element)
	throws Exception
	{
    if (element.getName().equals("Avery.panel"))
    {
      masterID = AveryMasterpanel.convertNumericID(element.getAttributeValue("masterID"));
      setPosition(AveryProject.buildPoint(element.getAttributeValue("position")));
      setRotation(element.getAttributeValue("rotation"));
      setDescriptionObject(
      		Description.makePanelDescription(element.getAttributeValue("description")));

      // read fields
      Iterator i = element.getChildren("Avery.p.field").iterator();
      while (i.hasNext())
      {
        AveryPanelField field = AveryPanelField.getPanelField((Element)i.next());
        if (field != null)
        {
          addField(field);
        }
      }

      // read cutouts
      Iterator j = element.getChildren("Avery.cutout").iterator();
      while (j.hasNext())
      {
        addCutout(new AveryCutout((Element)j.next()));
      }

      return true;
    }
    else return false;
  }

  /**
   * Creates an AveryPanel clone
   * @return the AveryPanel clone
   */
  public Object clone()
  {
    try
    {
      // the panel clone is only AveryPanel depth currently
      // create new panel
      AveryPanel ap = new AveryPanel();
      // copy all attributes including supers
      ap.masterID = masterID;
      ap.position = position;
      ap.rotation = rotation;
      ap.masterpanel = masterpanel;
      Description newDescription = Description.makePanelDescription();
      newDescription.setIndex(getDescriptionObject().getIndex());
      newDescription.setText(getDescriptionObject().getText());
      ap.setDescriptionObject(newDescription);
      // the panel fields and cutouts are shallow clones
      java.util.List superPanelFields = getPanelFields();
      java.util.List clonePanelFields = ap.getPanelFields();
      Iterator i = superPanelFields.iterator();
      while (i.hasNext())
      {
        clonePanelFields.add(i.next());
      }
      java.util.List superCutouts = getCutouts();
      java.util.List cloneCutouts = ap.getCutouts();
      i = superCutouts.iterator();
      while (i.hasNext())
      {
        cloneCutouts.add(i.next());
      }
      return ap;
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return null;
  }

  public AveryPanel shallowClone()
	{
		try
		{
			// the shallow clone doesn't do panel fields
			// create new panel
			AveryPanel ap = new AveryPanel();

			// copy all attributes including supers
			ap.setMasterID(masterID);
			ap.setPosition(position);
			ap.setRotation(rotation);
			ap.setMasterpanel(masterpanel);
      ap.setDescriptionObject(getDescriptionObject());

			return ap;
		}
    catch (Exception e)
    {
      e.printStackTrace();
    }
		return null;
	}

  public AveryPanel positionClone()
	{
		try
		{
			AveryPanel ap = new AveryPanel();

			// copy all attributes including supers
			ap.setPosition(position);
			ap.setNumber(number);
			ap.setAltSortOrder(altSortOrder);

			return ap;
		}
    catch (Exception e)
    {
      e.printStackTrace();
    }
		return null;
	}

  public String dump()
  {
    String str = new String("");
    str += "<P>AveryPanel";
    str += "<br>MasterID = " + masterID.toString();
    str += "<br>position = " + position.toString();
    str += "<br>rotation = " + rotation.toString();
    str += super.dump() + "</P>";

    return str;
  }

  public void setMasterID(String newMasterID)
  {
    masterID = newMasterID;
  }

  public String getMasterID()
  {
    return masterID;
  }

  public void setPosition(Point newPosition)
  {
    position = newPosition;
  }

  public Point getPosition()
  {
    return position;
  }

  public void setRotation(Double newRotation)
  {
    rotation = newRotation;
  }

  public void setRotation(String str)
  {
    try
    {
      rotation = Double.valueOf(str);
    }
    catch (Exception e)
    {
      // on failure, rotation is set to the default 0.0
      rotation = new Double(0.0);
      return;
    }
  }

  public Double getRotation()
  {
    return rotation;
  }

  public void setMasterpanel(AveryMasterpanel newMasterpanel)
  {
    masterpanel = newMasterpanel;
    masterID = newMasterpanel.getID();
    // replace default description text
    if (super.usingDefaultDescription)
    {
    	getDescriptionObject().setText(newMasterpanel.getDescription());
    	super.usingDefaultDescription = false;
    }
  }

  public AveryMasterpanel getMasterpanel()
  {
    return masterpanel;
  }

  public List getFieldRefs()
  {
    return fieldRefs;
  }

  private void addFieldRef(FieldRef fieldRef)
  {
    fieldRefs.add(fieldRef);
  }

  /**
   * initializes Panel Data Linking on the panel
   */
  protected boolean intializePanelDataLinking(ProjectFieldTable fieldTable)
  {
    this.projectFieldTable = fieldTable;
    if (getPanelFields().size() <= 0 && getFieldRefs().size() <= 0)
    {
      // project only has master panel fields at this point
      Iterator iterator = getMasterpanel().getPanelFields().iterator();
      while (iterator.hasNext())
      {
        try
        {
          // create a field ref for every master panel field
          AveryPanelField field = (AveryPanelField)iterator.next();
          FieldRef fieldRef = new FieldRef();
          fieldRef.setProjectFieldTable(fieldTable);
          fieldRef.createID();

          // the content and style ids are temporarily set to
          // the master panels, acting as placeholders
          fieldRef.setContentID(field.getContentID());

          // Todo - Barcode?

          if (field instanceof AveryTextfield)
          {
         		fieldRef.setStyleID(field.getID());
          }

          fieldTable.addField(fieldRef.getID(), fieldRef);
          addFieldRef(fieldRef);
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
      }
      return true;
    }
    else	// has panelFields and/or fieldRefs
    {
      // IDs of real fields were added in AveryProject.initializePanelDataLinking()

      // add the fieldrefs
      Iterator iterator = getFieldRefs().iterator();
      while (iterator.hasNext())
      {	try
        {
          FieldRef fieldref = (FieldRef)iterator.next();
          fieldref.setProjectFieldTable(fieldTable);
          fieldTable.adjustRefIDCounter(fieldref.getID());
          fieldTable.addField(fieldref.getID(), fieldref);
      	}
       	catch (NullPointerException nullex)	// something is very, very wrong here
       	{
       		System.err.println(nullex);
       		System.err.println("\t" + nullex.getStackTrace()[0]);
       		continue;
       	}
      }
    }

    return false;
  }


  /**
   * @return a Hashtable populated with field IDs and fieldRef IDs, keyed by promptOrder
   */
  public Hashtable getIDs()
  {
    Hashtable IDs = new Hashtable();
    // put IDs of real fields
    Iterator iterator = getPanelFields().iterator();
    while (iterator.hasNext())
    {
      AveryPanelField field = (AveryPanelField)iterator.next();
      IDs.put(field.getPromptOrder(), field.getID());
    }

    // put IDs of fieldrefs
    if (projectFieldTable != null)
    {
      iterator = getFieldRefs().iterator();
      while (iterator.hasNext())
      { try
        {
          FieldRef fieldref = (FieldRef)iterator.next();
          AveryPanelField field = (AveryPanelField)(projectFieldTable.findContentField(fieldref.getID()));
          if (field == null)
            throw new NullPointerException("Can't find content field for fieldref " + fieldref.getID());
          IDs.put(field.getPromptOrder(), fieldref.getID());
        }
        catch (NullPointerException nullex) // something is very, very wrong here
        {
          System.err.println(nullex);
          System.err.println("\t" + nullex.getStackTrace()[0]);
          continue;
        }
      }
    }
    return IDs;
  }

  /**
   * set all content and style ids to point to their
   * direct superior field for all FieldRef fields
   * located on the panel
   */
  protected void resolveLinks(AveryPanel previousPanel)
  {
    try
    {
      Iterator i = getFieldRefs().iterator();
      Iterator j = previousPanel.getFieldRefs().iterator();
      while (j.hasNext())
      {
        String previousID = ((FieldRef)j.next()).getID();
        if (i.hasNext())
        {
          // set content and style ids to link to previous panel's field
          FieldRef field = (FieldRef)i.next();
          field.setContentID(previousID);
          if (field.getStyleID().length() > 0)
            field.setStyleID(previousID);
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

 /**
   * updates the field with the content id
   * @param contentID the panel field content id to return
   * @param field the panel field
   * @param fieldTable the ProjectFieldTable
   */
  protected void updateContentField(String contentID, AveryPanelField field,
    ProjectFieldTable fieldTable)
  {
    Object fieldRef = fieldTable.get(contentID);
    if (fieldTable.updateContentField(contentID, field))
    {
      fieldRefs.remove(fieldRef);
      addField(field);
    }
  }

 /**
   * updates the field with the style id
   * @param styleID the panel field style id to return
   * @param field the panel field
   * @param fieldTable the ProjectFieldTable
   */
  protected void updateStyleField(String styleID, AveryPanelField field,
    ProjectFieldTable fieldTable)
  {
    Object fieldRef = fieldTable.get(styleID);
    if (fieldTable.updateStyleField(styleID, field))
    {
      fieldRefs.remove(fieldRef);
      addField(field);
    }
  }

 /**
   * adds the field to the panel, and adds FieldRefs to subordinate panels
   * @param field the AveryPanelField to add
   * @param fieldTable the ProjectFieldTable
   * @param subordinates List of panels that will get linked fieldrefs
   */
  protected void addField(AveryPanelField field,
    ProjectFieldTable fieldTable, java.util.List subordinates)
  {
    // initialize field ids
    String refID = fieldTable.nextRefID();
    field.setID(refID);
    field.setContentID(refID);
    field.setStyleID(refID);

    if (field.getZLevel() == 0)
    {
      // Determine top Z-Order and set it
      //
      double zLevel = 0;
      Iterator iPanelFields = getPanelFields().iterator();
      while (iPanelFields.hasNext())
      {
        AveryPanelField panelField = (AveryPanelField)iPanelFields.next();
        double panelZLevel = panelField.getZLevel();
        if (panelZLevel > zLevel)
          zLevel = panelZLevel;
      }

      Iterator iFieldRefs = getFieldRefs().iterator();
      while (iFieldRefs.hasNext())
      {
        // FindContentField removes project table entry if no top superior found
        FieldRef fieldRef = (FieldRef)iFieldRefs.next();
        Object tableField = fieldTable.findContentField(fieldRef.getID());
        AveryPanelField panelField = (AveryPanelField)tableField;
        if (panelField != null)
        {
          double panelZLevel = panelField.getZLevel();
          if (panelZLevel > zLevel)
            zLevel = panelZLevel;
        }
      }

      zLevel += 1.0;
      field.setZLevel(zLevel);
    }

    // add field to the panel field table
    fieldTable.addField(refID, field);

    // add field to the panel
    addField(field);

    // add field to all subordinate panels
    String currentID = refID;
    Iterator iterator = subordinates.iterator();

    while (iterator.hasNext())
    {
      AveryPanel nextPanel = (AveryPanel)iterator.next();

      // create new field ref and init it
      FieldRef fieldRef = new FieldRef();
      fieldRef.setID(fieldTable.nextRefID());
      fieldRef.setContentID(currentID);
      if (!(field instanceof AveryImage || field instanceof Drawing))
        fieldRef.setStyleID(currentID);
      fieldRef.setProjectFieldTable(fieldTable);
      // add field ref to panel field table
      currentID = fieldRef.getID();
      fieldTable.addField(currentID, fieldRef);
      // add field ref to panel list
      nextPanel.addFieldRef(fieldRef);
    }
  }

 /**
   * removes the field on the panel with the id
   * @param refID the panel field id to remove
   */
  protected void removeField(String refID, ProjectFieldTable fieldTable)
  {
    Object field = fieldTable.get(refID);
    if (field != null)
    {
      if (field instanceof FieldRef)
      {
        fieldRefs.remove(field);
      }
      else
      {
        getPanelFields().remove(field);
      }
      fieldTable.removeItem(refID);
    }
  }

 /**
   * if field does not have a top superior field discard them
   *  from the panel list and the panel field table
   * @param fieldTable the project field table
   */
  protected void discardPanelFields(ProjectFieldTable fieldTable)
  {
    // check all AveryPanelFields
    List tempPanelFields = new LinkedList();
    tempPanelFields.addAll(getPanelFields());

    Iterator fields = tempPanelFields.iterator();
    while (fields.hasNext())
    {
      Object field = fields.next();
      String refID = ((AveryPanelField)field).getID();

      // findContentField removes project table entry if no top superior found
      Object tableField = fieldTable.findContentField(refID);
      if (tableField == null)
        getPanelFields().remove(field);
    }

    // check all FieldRefs
    tempPanelFields = new LinkedList();
    tempPanelFields.addAll(getFieldRefs());

    fields = tempPanelFields.iterator();
    while (fields.hasNext())
    {
      Object field = fields.next();
      String refID = ((FieldRef)field).getID();

      // findContentField removes project table entry if no top superior found
      Object tableField = fieldTable.findContentField(refID);
      if (tableField == null)
        getFieldRefs().remove(field);
    }
  }

  /**
   * creates a printable PDF representation of the project,
   * reporting any errors to stderr output
   * @param pdfPage - the pdf page of the document
 * @param dPageHeight - the height of the page in twips
 * @param master - AveryMasterpanel
 * @param imageCache - image cache hashtable
 * @param outlinePanel - true means draw outline around panels in pdf
 * @param outlineFields - true means draw outline around panel fields in pdf
   * @throws Exception on pdfDoc error
  */
  public void addToPDF(PDFPage pdfPage, Double dPageHeight,
    AveryMasterpanel master, Hashtable imageCache, boolean outlinePanel, boolean outlineFields)
    throws Exception
  {
    Point offset = new Point(position.x, position.y);
  	
    // save graphics before impending panel transform
  	pdfPage.save();

  	float yOffset = dPageHeight.floatValue() - offset.y;  	
  	double dRotation = getRotation().doubleValue();

  	// transform PDF coordinates to panel lower left corner
  	transformPDFPanel(pdfPage, offset.x / 20f, yOffset / 20f, dPageHeight, dRotation, master);
   	
  	// clip output to panel for common panel shapes
  	if (AveryPage.enablePanelClipping == true)
  		clipPanel(pdfPage, master);
	
  	boolean hasDrawings = false;
  	
  	// ask fields to render themselves, using DrawIterator from AverySuperPanel
    Iterator i = new DrawIterator(createDisplayList(master));
    while (i.hasNext())
    {
      AveryPanelField panelField = (AveryPanelField)i.next();
      
      // special handling will apply if panel has any Drawing objects
      if (panelField instanceof Drawing)
      	hasDrawings = true;
      
      // do not print if invisible or unprintable unless Averysoft global override is set
      if ((panelField.getVisible() && panelField.getPrint()) || Averysoft.includeAllPanelFieldsInPDF)
      {
      	Double panelHeight = master.getHeight();
        panelField.addToPDF(pdfPage, panelHeight, imageCache);
      }

	  // draws field's outline to the PDF
      if (outlineFields) 
      {
          panelField.addOutlineToPDF(pdfPage, master.getHeight());
      }
    }
    i = null;
    
    // adds transparent image to the panel to make sure transparent colors flatten correctly in PDF
    if (hasDrawings)
    {
    	addTransparentPanelImage(pdfPage, imageCache);
    }
    
    // can be used to draw outlines for debug purposes or for printable outlines
    // possibly as a reposition
    // option so users have visual calibration aid.
    if (master.isPrintOutline() || outlinePanel)
    {
      master.addOutlineToPDF(pdfPage, position.x, position.y, dPageHeight.doubleValue());
    }
    
    // restore page coordinates, also clears clipping area 
    pdfPage.restore();
  }
  
  // correct color mismatches when print transparent images over drawings
  private void addTransparentPanelImage(PDFPage pdfPage, Hashtable pdfImageCache)
  {
  	try
  	{
	    int height = (int)((0.5 + getHeight().floatValue()) / 20.0f);
	    int width = (int)((0.5 + getWidth().floatValue()) / 20.0f);
	    
	    float startX = 0f;
	    float startY = 0f;
	    
  		PDFImage pdfImage = (PDFImage) pdfImageCache.get(getMasterpanel().getID());
//	  	System.out.println("create transparent panel image");
	  	
  		if (pdfImage == null)
  		{
		    
		    // decide about bleed
		    if (getMasterpanel().isBleedable())
		    {
			    startX -= BLEED_DIMENSION;
			    startY -= BLEED_DIMENSION;
		    	width += 2 * BLEED_DIMENSION;    	
		    	height += 2 * BLEED_DIMENSION;    	
		    }
		    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	
		    Graphics2D graphics = image.createGraphics(); 
		    graphics.setPaint ( new Color ( 0, 0, 0, 0x0 ) );
		    graphics.fillRect ( 0, 0, image.getWidth(), image.getHeight() );
		    
		    ByteArrayOutputStream os = new ByteArrayOutputStream();
		    ImageIO.write(image,"png", os); 
		    InputStream is = new ByteArrayInputStream(os.toByteArray());
		    pdfImage = new PDFImage(is);
		  	// cache the transparent image stream by master panel id
		    pdfImageCache.put(getMasterpanel().getID(), pdfImage);
  		}
  		
	    PDFCanvas pdfCanvas = new PDFCanvas(width, height);
	    pdfCanvas.drawImage(pdfImage, 0f, height, width, 0f);    
	    pdfPage.drawCanvas(pdfCanvas, startX, startY + height, startX + width, startY);
  	}
  	catch (Exception e){}
  	
  }
  
  // clip common panel shapes
  private void clipPanel(PDFPage pdfPage, AveryMasterpanel master)
  	throws Exception
  {
  	// PDF coordinates are relative to the unrotated panel lower left corner  	
  	String shape = master.getShape().getShapeString();
  	float bleed = 0f;
  	if (master.isBleedable() == true)
  	{
  		bleed = BLEED_DIMENSION;
  	}
  	
		float x1 = -bleed;
		float y1 = -bleed;
		float x2 = x1 + master.getWidth().floatValue() / 20f + (2f * bleed);
		float y2 = y1 + master.getHeight().floatValue() / 20f + (2f * bleed);
				
  	String noBleedSides = master.getNoBleedSides();
  	
		if (noBleedSides != null)
		{
			
			if (noBleedSides.indexOf("left") >= 0)
			{
				x1 = 0;
			}
			
			if (noBleedSides.indexOf("right") >= 0)
			{
				x2 = x2 - bleed; 
			}
			
			if (noBleedSides.indexOf("top") >= 0)
			{
				y2 = y2 - bleed;
			}
			
			if (noBleedSides.indexOf("bottom") >= 0)
			{
				y1 = 0;
			}
		}
		
  	if ("rect".equals(shape) || "polygon".equals(shape))
  	{		
  		double radius = master.getShape().getCornerRadius();
  		
  		if (radius <= 0)
  		{
  			// perhaps only do this if it has bleed??
  			// if always clip to bleed even for non bleed may save ink because of additional clipping to panel bounding box
  			if ("polygon".equals(shape) && noBleedSides == null)  /*  && master.isBleedable()  */
  			{
					PolygonShape ps = (PolygonShape)master.getShape();
					
					clipBleedPolygon(pdfPage, ps);  					
  			}
  			// clip to panel bounding box, if bleed then bounding box increased by bleed
  			// applies to unrounded rectangular and polygon shaped panels
  			pdfPage.clipRectangle(x1, y1, x2, y2);
  		}
  		else
  		{
  			// clip rounded rectangular panel
  			pdfPage.clipRoundedRectangle(x1, y1, x2, y2, (float)(radius / 20f));
  		}

			// debug - use drawRectangle to visually inspect clip area
  		/*PDFStyle style = new PDFStyle();
      style.setFillColor(Color.red);
      style.setLineColor(Color.black);
  		pdfPage.setStyle(style);
  		
  		pdfPage.drawRectangle(-bleed, -bleed,
  		master.getWidth().floatValue() / 20f + 2f * bleed,
  		master.getHeight().floatValue() / 20f + 2f * bleed); */
	  		      
  	}
  	else if ("ellipse".equals(shape))
  	{
  		// clip elliptical panel
  		pdfPage.clipEllipse(x1, y1, x2, y2);
  		  		
  		// debug - use drawEllipse to visually inspect clip area
  		/*PDFStyle style = new PDFStyle();
      style.setFillColor(Color.red);
      style.setLineColor(Color.black);
  		pdfPage.setStyle(style);
  		
  		pdfPage.drawEllipse(-bleed, -bleed,
  			master.getWidth().floatValue() / 20f + 2f * bleed,
  			master.getHeight().floatValue() / 20f + 2f * bleed); */	
  	}
  }
  
  // polygons with bleed
  // walks bleed path segments, adds all segments to clipping path
  private void clipBleedPolygon(PDFPage pdfPage, PolygonShape ps)
  {
    GeneralPath masterBleedShape = ps.scaledBleedGraphicsPath(0.05);
    masterBleedShape.closePath();
    Area area1 = new Area(masterBleedShape);
    
    GeneralPath bleedPath = new GeneralPath(area1);
    
    double[] coords = new double[6];
		float height = masterpanel.getHeight().floatValue() / 20f;
		
		/* debug
	  PDFStyle linestyle = new PDFStyle();
	  linestyle.setLineColor(java.awt.Color.red);
	  linestyle.setLineWeighting(4f);
	  linestyle.setFillColor(java.awt.Color.cyan);
    pdfPage.setStyle(linestyle); */
		
		// NOTE FOR SOFTSERVE:
		// path iteration could be used in back end to parse and return clipping path for other platforms
		
    // get bleed polygon segment coordinates/control points and clip to the page 
    for (PathIterator pi = bleedPath.getPathIterator(null); ! pi.isDone(); pi.next())
    {
      switch (pi.currentSegment(coords))
      {
        case PathIterator.SEG_MOVETO:
         	//System.out.println("M: " + coords[0] + "," + coords[1]);
        	pdfPage.pathMove((float)coords[0], height - (float)coords[1]);
        	break;
        case PathIterator.SEG_LINETO:
         	//System.out.println("L: " + coords[0] + "," + coords[1]);
        	pdfPage.pathLine((float)coords[0], height - (float)coords[1]);
        	break;
        case PathIterator.SEG_QUADTO:
         	//System.out.println("Q: " + coords[0] + "," + coords[1]);
        	break;
        case PathIterator.SEG_CUBICTO:
         	//System.out.println("C: " + coords[0] + "," + coords[1] + " " + coords[2] + "," + coords[3] + " " + coords[4] + "," + coords[5]);
        	pdfPage.pathBezier((float)coords[0], height - (float)coords[1], (float)coords[2], height - (float)coords[3],
        			(float)coords[4], height - (float)coords[5]);
        	break;
        case PathIterator.SEG_CLOSE:
         	//System.out.println("Close");
        	break;
            
        //default:
            //throw new IllegalArgumentException("Path contains curves");
      }
    }
    //debug: pdfPage.pathPaint();
    
    // close the clipping path and perform the clipping
    pdfPage.pathClip();
	  
  }
  // clip common panel shapes
/*  private void clipPanel(PDFPage pdfPage, AveryMasterpanel master)
  	throws Exception
  {
  	// PDF coordinates are relative to the unrotated panel lower left corner  	
  	String shape = master.getShape().getShapeString();
  	float bleed = 0f;
  	if (master.isBleedable() == true)
  	{
  		bleed = BLEED_DIMENSION;
  	}
  	
  	
		float x1 = -bleed;
		float y1 = -bleed;
		float x2 = x1 + master.getWidth().floatValue() / 20f + (2f * bleed);
		float y2 = y1 + master.getHeight().floatValue() / 20f + (2f * bleed);
				
  	String noBleedSides = master.getNoBleedSides();
  	
		if (noBleedSides != null)
		{
			
			if (noBleedSides.indexOf("left") >= 0)
			{
				x1 = 0;
			}
			
			if (noBleedSides.indexOf("right") >= 0)
			{
				x2 = x2 - bleed; 
			}
			
			if (noBleedSides.indexOf("top") >= 0)
			{
				y2 = y2 - bleed;
			}
			
			if (noBleedSides.indexOf("bottom") >= 0)
			{
				y1 = 0;
			}
		}
		
  	if ("rect".equals(shape) || "polygon".equals(shape))
  	{		
  		double radius = master.getShape().getCornerRadius();
  		
  		if (radius <= 0)
  		{
  			if ("polygon".equals(shape) && noBleedSides == null)
  			{
  				if (master.isBleedable())
  				{
  					PolygonShape ps = (PolygonShape)master.getShape();
  					//if (!ps.hasCurves())
  					{
  						clipStraightLinePolygon(pdfPage, ps, bleed);
  						//return;
  					}
  				}
  			}
  			// clip rectangular or polygon shaped panel
  			pdfPage.clipRectangle(x1, y1, x2, y2);
  		}
  		else
  		{
  			// clip rounded rectangular panel
  			pdfPage.clipRoundedRectangle(x1, y1, x2, y2, (float)(radius / 20f));
  		}*/

			// debug - use drawRectangle to visually inspect clip area
  		/*PDFStyle style = new PDFStyle();
      style.setFillColor(Color.red);
      style.setLineColor(Color.black);
  		pdfPage.setStyle(style);
  		
  		pdfPage.drawRectangle(-bleed, -bleed,
  		master.getWidth().floatValue() / 20f + 2f * bleed,
  		master.getHeight().floatValue() / 20f + 2f * bleed); */
	  		      
/*  	}
  	else if ("ellipse".equals(shape))
  	{
  		// clip elliptical panel
  		pdfPage.clipEllipse(x1, y1, x2, y2);*/
  		  		
  		// debug - use drawEllipse to visually inspect clip area
  		/*PDFStyle style = new PDFStyle();
      style.setFillColor(Color.red);
      style.setLineColor(Color.black);
  		pdfPage.setStyle(style);
  		
  		pdfPage.drawEllipse(-bleed, -bleed,
  			master.getWidth().floatValue() / 20f + 2f * bleed,
  			master.getHeight().floatValue() / 20f + 2f * bleed); */	
 /* 	}
  }
  
  private void clipStraightLinePolygon(PDFPage pdfPage, PolygonShape ps, float bleed)
  {
		float height = masterpanel.getHeight().floatValue() / 20f;
    GeneralPath masterShape = ps.scaledBleedGraphicsPath(1, 1);       	
		GeneralPath panelShape = getStraightSegmentPolygonBleedPath(masterShape, 1.0, (int)(bleed * 20f), ps.getRotationSense());
		
		float coords[] = new float[6];
    
    List xvalues = new ArrayList();
    List yvalues = new ArrayList();
    
    // read original polygon coordinates into both original and rescaled lists
    for (PathIterator pi = panelShape.getPathIterator(null); ! pi.isDone(); pi.next())
    {
      switch (pi.currentSegment(coords))
      {
        case PathIterator.SEG_MOVETO:
        case PathIterator.SEG_LINETO:
        	xvalues.add(coords[0]);
        	yvalues.add(coords[1]);
        	break;
            
        //default:
            //throw new IllegalArgumentException("Path contains curves");
      }
    }
    // data structures required by clipPolygon
    float xarray[] = new float[xvalues.size()];
    float yarray[] = new float[yvalues.size()];
    
    // debug only
    PDFStyle style = new PDFStyle();
    style.setLineColor(java.awt.Color.red);
    style.setFillColor(java.awt.Color.red);
    pdfPage.setStyle(style);
    for (int i = 0; i < xvalues.size(); i++)
    {
    	xarray[i] = (float)xvalues.get(i) / 20f;
    	yarray[i] = height - (float)yvalues.get(i) / 20f;
    	pdfPage.drawCircle(xarray[i], yarray[i], 0.5f);
    }

    
		pdfPage.clipPolygon(xarray, yarray);
		return;
  	
  }*/
  

   /**
   * Transform PDF output to unrotated panel lower left corner
   *
   * @param pdfPage - target
   * @param panelx - panel x upper left corner position in points
   * @param panely - panel y upper left corner position in points
   * @param dPageHeight - page height in twips
   * @param panelRotation - panel rotation in degrees
   * @param master - AveryMasterpanel
   */
  private void transformPDFPanel(PDFPage pdfPage, float panelx, float panely, Double dPageHeight,
  		double panelRotation, AveryMasterpanel master)
  	throws Exception
  {
    double theta = 0.0;

    // compute position of panel lower left corner in points, before its potentially rotated
    float lowerLeftX = panelx;
    float lowerLeftY = panely - master.getHeight().floatValue() / 20f;
    
    // convert panel angle from degrees to radians
    theta = panelRotation * Math.PI / 180.0;
    float sintheta = (float)Math.sin(theta);
    float costheta = (float)Math.cos(theta);

    if (panelRotation != 0)
    {
    	// rotate lowerLeft point about upper left point
      float deltax = lowerLeftX - panelx;
      float deltay = lowerLeftY - panely;
      
      // rotate the field lower left corner around the panel corner
      float xprime = deltax * costheta - deltay * sintheta;
      float yprime = deltax * sintheta + deltay * costheta;
//      System.out.println("xprime= " + new Float(xprime).toString());
//      System.out.println("yprime= " + new Float(yprime).toString());
      if (xprime > -.01 && xprime < .01)
        xprime = 0.0f;
      if (yprime > -.01 && yprime < .01)
        yprime = 0.0f;
      
      lowerLeftX = panelx + xprime;
      lowerLeftY = panely + yprime;
    }
//      System.out.println("panelx= " + new Float(panelx).toString());
//     System.out.println("panely= " + new Float(panely).toString());

//      System.out.println("theta= " + new Double(theta).toString());

    // calculate the rotation matrix
    float a = costheta;
    float b = sintheta;
    float c = -sintheta;
    float d = costheta;

    // set up the transform matrix for subsequent rendering
    String concat = AveryPanelField.formatConcat(a, b, c, d, lowerLeftX, lowerLeftY);
    pdfPage.rawWrite(concat);
  }
  
  /**
   * Uses Java Graphics2D to draw the panel
   * @param graphics the drawing context
   * @param nTargetWidth width in pixels (used for scaling)
   * @param nTargetHeight height in pixels (used for scaling)
   * @param master masterpanel to draw fields from
   * @throws Exception when something goes horribly wrong
   */
  public void draw(Graphics2D graphics, int nTargetWidth, int nTargetHeight, AveryMasterpanel master)
    throws Exception
  {
    // mask the shape of the panel
    master.maskShape(graphics, nTargetWidth, nTargetHeight);

    // calculate the scaling factor
    double scalar = (double)nTargetWidth / master.getWidth().doubleValue();
    // note: multiplying the scalar by 1440 will give you the target DPI.

    master.drawGuides(graphics, scalar, new Color(0xff, 0, 0xff));

    if( drawHiddenInfo ) 
    {
    	AveryTextblock dummy = generateHiddenInfo(master);
    	dummy.draw(graphics, scalar);
    }
    else
    {
        // walk the fields in proper zOrder, Draw()ing each of them
        Iterator i = new DrawIterator(createDisplayList(master));
        while (i.hasNext())
        {
        	((AveryPanelField)i.next()).draw(graphics, scalar);
        }
    }

    // draw any cutouts that might exist
    master.drawCutouts(graphics, scalar);
    super.drawCutouts(graphics, scalar);
    
    // draw the shape outline on top of everything
    master.drawShape(graphics, scalar);
  }

  /**
   * Updates a field in this panel by replacing it with the given field
   * @param newField This field will replace the field that has the same z-order
   *        in the panel, if they don't match.  If no field with that z-order
   *        exists, newField is simply added to the panel.
   * @return the old field if a field was replaced (for Undo?), or null
   */
  public AveryPanelField updatePanelField(AveryPanelField newField)
  {
    Iterator i = getPanelFields().iterator();
    while (i.hasNext())
    {
      AveryPanelField currentField = (AveryPanelField)i.next();
      if (currentField.getZLevel() == newField.getZLevel())  // found z-order match?
      {
        if (currentField.matches(newField))   // do all attributes match?
        {
          return null;    // no need to replace it; our work here is done
        }
        getPanelFields().remove(currentField);
        getPanelFields().add(newField);
        return currentField;    // currentField was replaced by newField
      }
    }

    // field with this z-order not found here; let's see if it's in the masterpanel
    i = getMasterpanel().getPanelFields().iterator();
    while (i.hasNext())
    {
      AveryPanelField currentField = (AveryPanelField)i.next();
      if (currentField.getZLevel() == newField.getZLevel())  // found z-order match?
      {
        if (currentField.matches(newField)) // do all attributes match?
        {
          return null;    // no need to replace it; our work here is done
        }
        getPanelFields().add(newField);   // add field to this, NOT to the master
        return null;      // nothing was replaced
      }
    }

    // field with this z-order not found anywhere; just add the new field
    getPanelFields().add(newField);
    return null;          // nothing was replaced
  }

  /**
   * Creates a list of fields by combining
   * the fields from the masterpanel with the fields from this panel.  When
   * a z-order collision occurs, the field from the masterpanel is discarded
   * and the field from this panel is retained.
   * @param master our associated AveryMasterpanel
   * @return combined list of usable AveryPanelFields
   */
  private java.util.List createDisplayList(AveryMasterpanel master)
  {
    LinkedList displayList = new LinkedList();

    // first add panel fields
    Iterator i = getPanelFields().iterator();
    while (i.hasNext())
    {
      AveryPanelField field = (AveryPanelField)i.next();
      if (field.getID().equals(field.getContentID()) &&
         (field.getStyleID().equals("")|| field.getID().equals(field.getStyleID())))
      {
        // panel field is a full top superior for content and style if has style
        displayList.add(field);
      }
      else
      {
        // panel field is not a full top superior
        compositePanelField(field, displayList);
      }
    }

    // next add fieldRef fields
    i = getFieldRefs().iterator();
    while (i.hasNext())
    {
      FieldRef fieldRef = (FieldRef)i.next();
    	try
    	{
	      AveryPanelField contentField
	        = fieldRef.getProjectFieldTable().getContentField(fieldRef.getID());
	      if (!(fieldRef.getStyleID().equals("")))
	      {
	        // text field is not a full top superior
	        compositePanelFieldRef(fieldRef, contentField, displayList);
	      }
	      else if (contentField != null)
	      {
	        // non text field is a top superior for content, no style so not full top superior
	        displayList.add(contentField);
	      }
    	}
    	catch (NullPointerException nullex)	// something is very, very wrong here
    	{
    		System.err.println(nullex);
    		System.err.println("\t" + nullex.getStackTrace()[0]);
    		break;	// no point in continuing
    	}
    }

    // add any fields from the master that don't collide
    if (master != null)
    {
      i = master.getPanelFields().iterator();
      while (i.hasNext())
      {
        AveryPanelField masterField = (AveryPanelField)i.next();
        boolean collision = false;
        // test for collisions
        Iterator j = displayList.iterator();
        while (j.hasNext())
        {
          if (masterField.getZLevel() == ((AveryPanelField)j.next()).getZLevel())
          {
            collision = true;
            break;
          }
        }
        if (collision == false)
        {
          displayList.add(masterField);
        }
      }
    }

    return displayList;
  }

  private void compositePanelField(AveryPanelField field, LinkedList displayList)
  {
    AveryPanelField contentField
      = field.getProjectFieldTable().getContentField(field.getID());
    if (contentField != null)
    {
      AveryPanelField styleField
        = field.getProjectFieldTable().findStyleField(field.getID());

      if (styleField == null)  // it happens on images
      {
      	displayList.add(field);
      	return;
      }

      // copy content to style clone
      if (styleField instanceof AveryTextline)
        ((AveryTextline)styleField).setContent(((AveryTextline)contentField).getContent());
      else if (styleField instanceof AveryTextblock)
        ((AveryTextblock)styleField).setLines(((AveryTextblock)contentField).getLines());
      else if (styleField instanceof TextPath)
        ((TextPath)styleField).setContent(((TextPath)contentField).getContent());

      // content includes geometry
      styleField.setPosition(contentField.getPosition());
      styleField.setWidth(contentField.getWidth());
      styleField.setHeight(contentField.getHeight());
      styleField.setRotation(contentField.getRotation());
      // add the field composited from content and style
      displayList.add(styleField);
    }
  }

  private void compositePanelFieldRef(FieldRef fieldRef, AveryPanelField contentField,
    LinkedList displayList)
  {
    if (contentField != null)
    {
      AveryPanelField styleField
        = fieldRef.getProjectFieldTable().findStyleField(fieldRef.getID());

      if (styleField == null)	// happens with Barcodes
      {
      	displayList.add(contentField);
      	return;
      }

      // copy content to style clone
      if (styleField instanceof AveryTextline)
        ((AveryTextline)styleField).setContent(((AveryTextline)contentField).getContent());
      else if (styleField instanceof AveryTextblock)
        ((AveryTextblock)styleField).setLines(((AveryTextblock)contentField).getLines());
      else if (styleField instanceof TextPath)
      ((TextPath)styleField).setContent(((TextPath)contentField).getContent());

      if (styleField instanceof AveryTextfield && contentField instanceof AveryTextfield)
      {
      	// copy attributes from Flashprint namespace
      	((AveryTextfield)styleField).fromFlash = ((AveryTextfield)contentField).fromFlash;
      	((AveryTextfield)styleField).flashLineheight = ((AveryTextfield)contentField).flashLineheight;
      	((AveryTextfield)styleField).flashXscale = ((AveryTextfield)contentField).flashXscale;
      }

      // content includes geometry
      styleField.setPosition(contentField.getPosition());
      styleField.setWidth(contentField.getWidth());
      styleField.setHeight(contentField.getHeight());
      styleField.setRotation(contentField.getRotation());
      // add the field composited from content and style
      displayList.add(styleField);
    }
  }

  /**
   * From a list of masterpanels, get the one that has our ID.
   * Note that the result is stored in local member masterpanel,
   * so a subsequent call will not actually walk the list.
   * @param masterpanels - the list to search
   * @return the matching masterpanel
   * @throws Exception when masterpanel is not found (document is in error)
   */
  public AveryMasterpanel findMasterpanel(java.util.List masterpanels)
    throws Exception
  {
    // find the masterpanel
    Iterator iterator = masterpanels.iterator();
    while (iterator.hasNext())
    {
      AveryMasterpanel master = (AveryMasterpanel)iterator.next();
      if (master.getID().equals(getMasterID()))  // found masterpanel
      {
        setMasterpanel(master);
        return master;
      }
    }
    // if none was found, the document is in error!
    throw new Exception("No masterpanel found for page panel " + getDescription());
  }

  /**
   * Creates an unsorted List of the PanelFields associated with this panel,
   * including those from its masterpanel.  Fields from the masterpanel are
   * discarded when their z-order conflicts with local fields.
   * @param master passed to createDisplayList
   * @return list of AveryPanelFields used by the panel
   * @throws Exception if the associated masterpanel cannot be found
   */
  private java.util.List getCombinedFieldList(AveryMasterpanel master)
    throws Exception
  {
    return createDisplayList(master);
  }

  /**
   * This override of the abstract AverySuperPanel method returns the combined
   * list of all of the fields, sorted by their prompt order.  The list does not
   * include any fields that have a zero (0.0) prompt order.
   * @return a list of fields sorted by prompt order
   * @throws Exception if the associated masterpanel cannot be found
   */
  public java.util.List getPromptSortedPanelFields()
    throws Exception
  {
    java.util.List combinedList = getCombinedFieldList(getMasterpanel());
    Double zero = Double.valueOf("0.0");

    Iterator i = combinedList.iterator();
    while (i.hasNext())
    {
      AveryPanelField field = (AveryPanelField)(i.next());
      if (Double.valueOf(field.getPromptOrder()).equals(zero))
      {
        i.remove();
      }
    }

    Object[] objects = combinedList.toArray();
    Arrays.sort(objects, new PromptComparator());
    return Arrays.asList(objects);
  }

  /**
   * This method modifies the field, if it exists, to match recent changes
   * in another field.  It is used to update fields that have been made
   * obsolete by an updated Masterpanel (hence the name).
   * @param field - the field to pull changes from.  If the field does not
   * exist in this panel's master, the method returns quickly with 0.
   * @param flags - describes what attributes to update
   * @return the number of field modifications made
   */
  public int masterUberAlles(AveryPanelField field, BitSet flags)
  {
    if (getMasterpanel() != null)
    {
      if (getMasterpanel().getPanelFields().contains(field) == false)
      {
        return 0;
      }
    }

    Iterator i = getPanelFields().iterator();
    while (i.hasNext())
    {
      AveryPanelField localField = (AveryPanelField)i.next();
      if (localField.getClass().equals(field.getClass())
        && localField.getZLevel() == field.getZLevel())
      {
        return localField.masterUberAlles(field, flags);
      }
    }
    return 0;
  }

  /**
   * In this class, the width is obtained from the associated AveryMasterpanel
   * @return the width
   */
  public Double getWidth()
  {
    return getMasterpanel().getWidth();
  }

  /**
   * In this class, the height is obtained from the associated AveryMasterpanel
   * @return the height
   */
  public Double getHeight()
  {
    return getMasterpanel().getHeight();
  }

  /**
   * Generates an AveryPanel suitable for a .avery Zip file bundle.  Images
   * are given new, unique names starting with <code>prefix</code>, and are
   * copied to the HighRes diretory under <code>location<code>
   * @param prefix - typically the machine name
   * @param location - where the project XML will ultimately be created
   * @param images -
   * @return a panel, with image paths deleted, suitable for a .avery bundled project
   */
  AveryPanel getPagePanelForDotAveryBundle(String prefix, File location, Hashtable images)
  {
    // the superclass takes care of most stuff
    AveryPanel panel =
      (AveryPanel)super.getPanelForDotAveryBundle(prefix, location, images);

    if (panel != this)
    {
      panel.setMasterID(this.getMasterID());
      panel.setPosition(this.getPosition());
      panel.setRotation(this.getRotation());

      panel.altSortOrder = altSortOrder;
      panel.fieldRefs = fieldRefs;
      panel.projectFieldTable = projectFieldTable;
      panel.number = number;
      panel.ofHowMany = ofHowMany;
    }

    return panel;
  }

  /**
   * Creates an Avery.panel element to use in a JDOM Document
   * @return newly created Avery.panel Element
   */
  public Element getAveryElement()
  {
    Element element = new Element("Avery.panel");

    // add attributes
    element.setAttribute("masterID",
    		AveryMasterpanel.createLegacyID(getMasterID()).toString());
    element.setAttribute("position",
      Double.toString(getPosition().getX()) + ","
      + Double.toString(getPosition().getY()));
    if (getRotation().doubleValue() != 0.0)
    { // defaults to 0.0 in the DTD
      element.setAttribute("rotation", getRotation().toString());
    }
    element.setAttribute("description", getDescription());
    
    // add Avery.p.field Elements
    Iterator iterator = getPanelFields().iterator();
    while (iterator.hasNext())
    {
      AveryPanelField field = (AveryPanelField)iterator.next();
      field.setAveryElementName("Avery.p.field");
      element.addContent(field.getAveryElement());
    }

    // add any Avery.cutout Elements
    iterator = getCutouts().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((XMLElement)iterator.next()).getAveryElement());
    }

    return element;
  }

  void reorient90(boolean clockwise)
  {
    // note: width and height are from reoriented masterpanel
    double width = getWidth().doubleValue();
    double height = getHeight().doubleValue();
    // position and rotation will change
    double x = position.getX();
    double y = position.getY();
    double rotation = getRotation().doubleValue();

    if (clockwise)
    {
      if (rotation == 0)
      {
        x += width;
        rotation = 270;
      }
      else if (rotation == 90 || rotation == -270)
      {
        y -= width;    // tested
        rotation = 0;
      }
      else if (rotation == 180 || rotation == -180)
      {
        x -= width;
        rotation = 90;
      }
      else if (rotation == 270 || rotation == -90)
      {
        y += width;
        rotation = 180;
      }
    }
    else // counter-clockwise
    {
      if (rotation == 0)
      {
        y += height;   // tested
        rotation = 90;
      }
      else if (rotation == 90 || rotation == -270)
      {
        x += height;
        rotation = 180;
      }
      else if (rotation == 180 || rotation == -180)
      {
        y -= height;
        rotation = 270;
      }
      else if (rotation == 270 || rotation == -90)
      {
        x -= height;
        rotation = 0;
      }
    }

    // set new position and rotation
    position.setLocation(x, y);
    setRotation(new Double(rotation));
  }

  /**
   * helper method generates the masterpanel JDOM element, minus any content
   * @return the averysoft.xsd-compatible element
   */
  private Element makeAverysoftElement()
  {
    Element element = new Element("panel");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    // fixup for invalid master panel id
    // a source of these could be dtd based projects...
    if (!AveryUtils.isNCName(getMasterID()))
      setMasterID("MP" + getMasterID());

    element.setAttribute("master", getMasterID());

    if (getDescriptionObject() != null)
      element.addContent(getDescriptionObject().getAverysoftElement());

	  element.setAttribute("position",
	  		Double.toString(getPosition().getX()) + "," + Double.toString(getPosition().getY()));
	
    if (getRotation().doubleValue() != 0.0)
    { // defaults to 0 in averysoft.xsd
      element.setAttribute("rotation", new Integer(getRotation().intValue()).toString());
    }
    
    if (altSortOrder > 0)
    {
      element.setAttribute("altSortOrder", Integer.toString(altSortOrder));
    }
    
    // output the field refs
		Iterator iterator = getFieldRefs().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}

    return element;
  }

  private Element makeAverysoftIBElement()
  {
    Element element = new Element("pastePanel");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    // fixup for invalid master panel id
    // a source of these could be dtd based projects...
    if (!AveryUtils.isNCName(getMasterID()))
      setMasterID("MP" + getMasterID());

    /*element.setAttribute("master", getMasterID());

    if (getDescriptionObject() != null)
      element.addContent(getDescriptionObject().getAverysoftElement());*/

	  element.setAttribute("position",
	  		Double.toString(getPosition().getX()) + "," + Double.toString(getPosition().getY()));
	
    if (getRotation().doubleValue() != 0.0)
    { // defaults to 0 in averysoft.xsd
      element.setAttribute("rotation", new Integer(getRotation().intValue()).toString());
    }
    
    /*if (altSortOrder > 0)
    {
      element.setAttribute("altSortOrder", Integer.toString(altSortOrder));
    }
    
    // output the field refs
		Iterator iterator = getFieldRefs().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}*/

    return element;
  }

  public Element getAverysoftElement()
  {
    Element element = makeAverysoftElement();

		Iterator iterator = getPanelFields().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}
    return element;
	}

  public Element getAverysoftIBElement()
  {
    Element element = makeAverysoftIBElement();

		/*Iterator iterator = getPanelFields().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}*/
    return element;
	}


	/**
	 * Retrieves the description String
	 * @return the description, used for identification
	 * @see #setDescription
	 */
	public String getDescription()
	{
		String text = super.getDescription();
		if (text == null || text.length() == 0)
		{
			text = masterpanel.getDescription();
		}

		if (getNumber() > 0 && getOfHowMany() > 1)
		{
			text += " (" + getNumber() + "/" + getOfHowMany() + ")";
		}

		return text;
	}

	/**
	 * @return
	 */
	int getAltSortOrder()
	{
		return altSortOrder;
	}

	/**
	 * @return
	 */
	public int getNumber()
	{
		return number;
	}

	/**
	 * @return
	 */
	private int getOfHowMany()
	{
		return ofHowMany;
	}

	/**
	 * @param i
	 */
	public void setAltSortOrder(int i)
	{
		altSortOrder = i;
	}

	/**
	 * @param i
	 */
	void setNumber(int i)
	{
		number = i;
	}

	/**
	 * @param i
	 */
	void setOfHowMany(int i)
	{
		ofHowMany = i;
	}

  /**
   * This method clears all panelFields and fieldRefs from the panel.
   * It also removes them from the fieldTable, which can create instability
   * if fields in other panels are linked to these panelFields or fieldRefs.
   * Use with caution!
   * @param fieldTable - project-wide list of fields
   */
  void clearContent(ProjectFieldTable fieldTable)
  {
    if (fieldTable == null)
      fieldTable = new ProjectFieldTable();

    // remove all AveryPanelFields
    List list = new ArrayList();
    list.addAll(getPanelFields());

    Iterator fields = list.iterator();
    while (fields.hasNext())
    {
      AveryPanelField field = (AveryPanelField)fields.next();
      getPanelFields().remove(field);
      if (fieldTable.containsKey(field.getID()))
        fieldTable.remove(field.getID());
    }

    // remove all FieldRefs
    list = new ArrayList();
    list.addAll(getFieldRefs());

    fields = list.iterator();
    while (fields.hasNext())
    {
      FieldRef fieldref = (FieldRef)fields.next();
      getFieldRefs().remove(fieldref.getID());
      if (fieldTable.containsKey(fieldref.getID()))
        fieldTable.remove(fieldref.getID());
    }
  }

  /**
   *  The static flag signalize if draw() method generates
   * image with panel's hidden info instead of panel's fields
   */
  private static boolean drawHiddenInfo = false;

  /**
   *  Set the drawHiddenInfo flag. 
   * @param show
   * @return true if the value was changed
   */
  public static boolean showHiddenInfo(boolean show)
  {
	  if ( drawHiddenInfo != show ) 
	  {
		  drawHiddenInfo = show;
		  return true;
	  }
	  return false;
  }

  /**
   *  The measurement units to show panel info
   */
  private static int showInUnits = AveryUtils.TWIPS;

  public static void setShowUnits(int units)
  {
	  showInUnits = units;
  }
  
  /**
   *  The method returns dummy TextBlock object containing
   * hidden info for the panel.   
   * 
   * @param AveryMasterpanel master
   * @return AveryTextblock
   */
  private AveryTextblock generateHiddenInfo(AveryMasterpanel master)
  {
	  AveryTextblock infoField = master.createDefaultTextblock();
	  
	  infoField.setJustification("center");
	  infoField.setVerticalAlignment("top");
	  infoField.setPointsize(new Double(12.0));

	  infoField.getTextContent().clear();
	  infoField.setLines( infoField.getTextContent() );

	  infoField.addLine(super.getDescription() + ": " + this.number);
	  infoField.addLine("Pos x/y: " + 
			  				AveryUtils.twipsToUnitString(this.position.x, showInUnits) + 
			  			", " + 
			  				AveryUtils.twipsToUnitString(this.position.y, showInUnits));

	  return infoField;
  }
  
  // accurately bleed polygon shapes that only contain straight line segments
  // supports lines and cubic bezier curve segments - quads are converted to cubic in PolygonShapes class
  // the algorithm is based on determining the outward pointing unit normal vector to each line and quad segment
  // this vector is multiplied by bleed size to determine how to resize and position each polypoint segment
/*  static public GeneralPath getStraightSegmentPolygonBleedPath(GeneralPath path, double scalar, int bleedSizeTwips, boolean rotateSense)
  {
  	double scaledBleed = scalar * bleedSizeTwips;
  	boolean closed = false;
  	
  	// lists for original polygon outline and for recalculated bleed outline coordinates
    List<double[]> pointList = new ArrayList<double[]>();
    List<double[]> qointList = new ArrayList<double[]>();
    double pList[][] = null;
    double qList[][] = null;
    
    double[] coords = new double[6];
    
    // read original polygon coordinates into both original and rescaled lists
    for (PathIterator pi = path.getPathIterator(null); ! pi.isDone(); pi.next())
    {
      switch (pi.currentSegment(coords))
      {
        case PathIterator.SEG_MOVETO:
         	//System.out.println("M: " + coords[0] + "," + coords[1]);
        	pointList.add(Arrays.copyOf(coords, 2));
        	qointList.add(Arrays.copyOf(coords, 2));
        	break;
        case PathIterator.SEG_LINETO:
         	//System.out.println("L: " + coords[0] + "," + coords[1]);
        	pointList.add(Arrays.copyOf(coords, 2));
        	qointList.add(Arrays.copyOf(coords, 2));
        	break;
        //case PathIterator.SEG_QUADTO:
        	//System.out.println("Q");
        	//pointList.add(Arrays.copyOf(coords, 2));
        	//qointList.add(Arrays.copyOf(coords, 2));
        	//break;
        case PathIterator.SEG_CLOSE:
        	closed = true;
        	break;
            
        default:
            throw new IllegalArgumentException("Path contains curves");
      }
    }
		// convert lists to more easily usable arrays
    pList = pointList.toArray(new double[pointList.size()][]);
    qList = qointList.toArray(new double[qointList.size()][]);
    
    if (qList == null)
    	return path;
    
    calculateBleedIntersections(pList, scaledBleed, path.getBounds(), rotateSense, qList);
    
    // construct GeneralPath of the bleed outline
    GeneralPath scaledPath = new GeneralPath();
     
    // begin construction,  start with move to first point
  	scaledPath.moveTo((int)qList[0][0], (int)qList[0][1]);
  	//System.out.println("x0,y0= " + (int)qList[0][0] + "," + (int)qList[0][1]);
  	
    for (int k = 1; k < qList.length; k++)
    {
    	// line segment
    	scaledPath.lineTo((int)qList[k][0], (int)qList[k][1]);
    	//System.out.println("x,y= " + (int)qList[k][0] + "," + (int)qList[k][1]);
    }
     
    return scaledPath;
  }

  // calculate intersection points of all bleed lines
  //
  // lines are processed in adjacent pairs
  // unit vector calculated for each line is used to get parallel bleed line end points
  // the adjacent bleed lines are guaranteed to intersect, that intersection point is then calculated
  static public void calculateBleedIntersections(double[][] pList, double scaledBleed, Rectangle bounds, boolean rotateSense, double[][] qList)
  {
  	//System.out.println(pList.length);
  	double minx = - 2 * scaledBleed;
  	double miny = minx;
  	double maxx = (double)bounds.width + 2 * scaledBleed; 
  	double maxy = (double)bounds.height + 2 * scaledBleed; 
  	
  	// calculate and store slope and y intercept of each bleed line segment
  	double lineProperties[][] = new double [pList.length - 1][2];
  	
  	for (int i = 1; i < pList.length; i++)
  	{
    	double x1 = pList[i-1][0];
    	double y1 = pList[i-1][1];
    	double x2 = pList[i][0];
    	double y2 = pList[i][1];	
    	
    	//System.out.println("line segment " + (i-1) + ": " + x1 + "," + y1 + "," + x2 + "," + y2);
    	
    	// rotate line by 90 degrees, assume points follow the path in a given direction
    	double dx = x2 - x1;
    	double dy = y2 - y1;
    	double ndx = -dy;
    	double ndy = dx;
    	if (rotateSense)
    	{
    		// reverse rotation -90 degrees, points follow the opposite direction around the path
    		ndx = dy;
    		ndy = -dx;
    	}
    	// convert to unit vector
    	double magnitude = Math.sqrt(ndx * ndx + ndy * ndy);
    	if (magnitude < 0.00001 && magnitude > -.00001)
    			magnitude = 0.00001;
    	ndx = ndx / magnitude;
    	ndy = ndy / magnitude;
    	
    	// translate line end points to parallel bleed line end points 	
    	x1 = x1 + ndx * scaledBleed;
    	y1 = y1 + ndy * scaledBleed;
    	x2 = x2 + ndx * scaledBleed;
    	y2 = y2 + ndy * scaledBleed;
    	
    	double slope = slope(x1, y1, x2, y2);
    	if (slope == -0.0)
    		slope = 0.0;
    	
    	double yinter = y1 - slope * x1;
    	lineProperties[i-1][0] = slope;
    	lineProperties[i-1][1] = yinter;
    	//lineProperties[i-1][2] = x1;
    	//lineProperties[i-1][3] = y1;
  	}
  	
  	// now calculate and store intersections
  	for (int i = 0; i < pList.length - 1; i++)
  	{
  		int secondLineIndex = i - 1;
  		if (i == 0)
  			secondLineIndex = pList.length - 2;
  		
  		double slope1 = lineProperties[i][0];
  		double yinter1 = lineProperties[i][1];
  		double slope2 = lineProperties[secondLineIndex][0];
  		double yinter2 = lineProperties[secondLineIndex][1];
  		
    	//System.out.println("slope1, yinter1, slope2, yinter2=" + slope1 + "," + yinter1 + "," + slope2 + "," + yinter2);

  		if (secondLineIndex == pList.length - 2)
  			secondLineIndex = 0;
  		else
  			secondLineIndex++;
  		
    	double denom = slope1 - slope2;
    	double numer = yinter2 - yinter1;
    	//System.out.println("numer=" + numer);
    	//System.out.println("denom=" + denom);
    	
    	if (denom > -0.000000001 && denom < 0.0)
    	{
      	//System.out.println("slope1, slope2, denom=" + slope1 + "," + slope2 + "," + denom);
    		denom = -0.000000001;
    	}
    	else if (denom >= 0.0 && denom <= 0.000000001)
    	{
      	//System.out.println("slope1, slope2, denom=" + slope1 + "," + slope2 + "," + denom);
    		denom = 0.000000001;
     	}
    	
      double xint = numer / denom;
    	double yint = xint * slope1 + yinter1;
  		
    	// keep within reasonable distance of bounding box increased by bleed 
    	xint = Math.round(Math.min(Math.max(xint,  minx), maxx));
  		yint = Math.round(Math.min(Math.max(yint,  minx), maxy));		
  		//System.out.println("xint, yint= " + xint + "," + yint);
  		qList[i][0] = xint;
  		qList[i][1] = yint;

  	}
  	
  	// the last intersection is always equal to the first
  	qList[pList.length - 1][0] = qList[0][0];
  	qList[pList.length - 1][1] = qList[0][1];*/
  	


  		/*double xint1 = Math.min(Math.max(xint,  minx), maxx);
  		double yint1 = Math.min(Math.max(yint,  minx), maxy);
  		
  		if (xint != xint1 || yint != yint1)
  		{
  			System.out.println("OOB xint, yint= " + xint + "," + yint);
  			System.out.println("OOB x11, y11= " + x11 + "," + y11);
  		}*/



  /*}
  
  static public double slope(double x1, double y1, double x2, double y2)
  {
  	double diff = x2 - x1;
  	if (diff >= 0.0 && diff < 0.0001)
  		diff = 0.0001;
  	else if (diff > -0.0001 && diff < 0.0)
  		diff = -0.0001;
  	
  	return (y2 - y1) / diff;
  	
  }*/

}