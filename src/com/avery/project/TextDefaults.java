/* Project: Miwok
 * Filename: TextDefaults.java
 * Created on Jun 16, 2004 by Bob Lee
 * Copyright 2004 by Avery Dennison Corporation, all rights reserved
 */
package com.avery.project;

import java.util.Hashtable;
import java.util.Iterator;

import org.jdom.Element;

/**
 * @author b0b
 *
 * The TextDefaults object is owned by a masterpanel.  It defines the
 * attributes that an application should use when creating a default
 * text block.
 */
public class TextDefaults extends TextStyle
{
	private TwipsPosition blockPosition = new TwipsPosition(0.0, 0.0);
	private Double blockWidth = new Double(0.0);
	private Double blockHeight = new Double(0.0);
	private Integer blockRotation = new Integer(0);
  private Hashtable hints = null;

	/**
	 * Construct a textDefaults algorithmicly from a masterpanel
	 */
	TextDefaults(AveryMasterpanel master)
	{
		super();
    // get panel dimensions
    double width = master.getWidth().doubleValue();
    double height = master.getHeight().doubleValue();
    
    double fieldWidth = width * 9.0 / 10.0;
    double fieldHeight = height * 13.0 / 15.0;

    // center horizontally and vertically
    double x = (width - fieldWidth) / 2.0;
    double y = (height - fieldHeight) / 2.0;
    
    blockWidth = new Double(fieldWidth);
    blockHeight = new Double(fieldHeight);
    blockPosition = new TwipsPosition(x, y);

    double area = (fieldWidth / 1440.) * (fieldHeight / 1440.);
    if (area < 1.0)
    {
    	this.setPointSize(new Double(8.0));
    }
    else if (area < 3.0)
    {
    	this.setPointSize(new Double(10.0));
    }
    else if (area < 6.0)
    {
    	this.setPointSize(new Double(12.0));
    }
    else if (area < 8.0)
    {
    	this.setPointSize(new Double(14.0));
    }    
    else if (area < 10.0)
    {
    	this.setPointSize(new Double(16.0));
    }    
    else if (area < 20.0)
    {
    	this.setPointSize(new Double(18.0));
    }
    else if (area < 40.0)
    {
    	this.setPointSize(new Double(20.0));
    }
    else if (area < 80.0)
    {
    	this.setPointSize(new Double(22.0));
    }
    else
    {
    	this.setPointSize(new Double(24.0));
    }
	}

  /**
   * constructs a text defaults from an Avery.textDefaults XML Element
   * @param element - describes the text defaults
   */
  TextDefaults(Element element)
  throws Exception
  {
		if (element.getNamespace().equals(AveryProject.getAverysoftNamespace())
			&& element.getName().equals("textDefaults"))
		{
			setBlockPosition(new TwipsPosition(element.getAttributeValue("blockPosition")));
      setBlockWidth(Double.valueOf(element.getAttributeValue("blockWidth")));
      setBlockHeight(Double.valueOf(element.getAttributeValue("blockHeight")));
      setBlockRotation(Integer.valueOf(element.getAttributeValue("blockRotation","0")));

      Element child;		// reusable local item
      if ((child = element.getChild("textStyle", AveryProject.getAverysoftNamespace())) != null)
      {
        TextStyle textStyle = new TextStyle(child);
        this.setPointSize(textStyle.getPointSize());
        this.setTextColor(textStyle.getTextColor());

        if (textStyle.isLeftJustified())
          this.setLeftJustified();
        else if (textStyle.isRightJustified())
          this.setRightJustified();
        else if (textStyle.isCenterJustified())
          this.setCenterJustified();
        else if (textStyle.isFullJustified())
          this.setFullJustified();

        if (textStyle.isTopAligned())
          this.setTopAligned();
        else if (textStyle.isMiddleAligned())
          this.setMiddleAligned();
        else if (textStyle.isBottomAligned())
          this.setBottomAligned();

        if (textStyle.isShrinkWrap())
          this.setShrinkWrap();
        else if (textStyle.isWrapShrink())
          this.setWrapShrink();
        else if (textStyle.isFit())
          this.setFit();
        /*else if (textStyle.isShrink())
          this.setShrink();
        else if (textStyle.isWrap())
          this.setWrap();*/

        this.setBold(textStyle.isBold());
        this.setItalic(textStyle.isItalic());
        this.setUnderline(textStyle.isUnderline());

        this.setTypeface(textStyle.getTypeface());
      }
      
      Iterator iterator = element.getChildren("hint", AveryProject.getAverysoftNamespace()).iterator();
      while (iterator.hasNext())
      {
        child = (Element)iterator.next();
        addHint(child.getAttributeValue("name"), child.getAttributeValue("value"));
      }
    }
    else
    {
      throw new Exception("Not a Text Defaults element");
    }
  }

	/**
	 * @return
	 */
	public Double getBlockHeight()
	{
		return blockHeight;
	}

	/**
	 * @return
	 */
	public TwipsPosition getBlockPosition()
	{
		return blockPosition;
	}

	/**
	 * @return
	 */
	public Integer getBlockRotation()
	{
		return blockRotation;
	}

	/**
	 * @return
	 */
	public Double getBlockWidth()
	{
		return blockWidth;
	}

	/**
	 * @param double1
	 */
	public void setBlockHeight(Double double1)
	{
		blockHeight = double1;
	}

	/**
	 * @param point
	 */
	public void setBlockPosition(TwipsPosition point)
	{
		this.blockPosition = point;
	}

	/**
	 * @param i - degrees counter-clockwise
	 */
	public void setBlockRotation(Integer i)
	{
		blockRotation = i;
	}

	/**
	 * @param double1
	 */
	public void setBlockWidth(Double double1)
	{
		blockWidth = double1;
	}

	/**
	 * Get a JDOM representation of the object suitable for
	 * inclusion in an XML file based on averysoft.xsd
	 * @return an Averysoft-based JDOM Element
	 */
	public Element getAverysoftElement()
	{
		Element element = new Element("textDefaults");
    element.setNamespace(AveryProject.getAverysoftNamespace());

		element.setAttribute("blockPosition",
				Double.toString(getBlockPosition().getX()) + ","
				+ Double.toString(getBlockPosition().getY()));
		element.setAttribute("blockWidth", getBlockWidth().toString());
		element.setAttribute("blockHeight", getBlockHeight().toString());
		
		if (getBlockRotation().intValue() != 0)
		{
			element.setAttribute("blockRotation", getBlockRotation().toString());
		}
		
		// add the textStyle element created by the base class
		element.addContent(super.getAverysoftElement());
		
		// add any hints
    if (hints != null)
    {
      java.util.Enumeration e = hints.keys();
      while (e.hasMoreElements())
      {
        Averysoftable hint = (Hint)hints.get(e.nextElement());
        element.addContent(hint.getAverysoftElement());
      }
    }
    
		return element;
	}
	
	/**
	 * Get a JDOM representation of the object suitable for
	 * inclusion in an XML file based on averysoft.xsd
	 * @return an Averysoft-based JDOM Element
	 */
	public Element getAverysoftIBElement()
	{
		Element element = new Element("textDefaults");
    element.setNamespace(AveryProject.getAverysoftNamespace());

		element.setAttribute("blockPosition",
				AveryProject.twipsToMM(getBlockPosition().getX()) + ","
				+ AveryProject.twipsToMM(getBlockPosition().getY()));
		element.setAttribute("blockWidth", AveryProject.twipsToMM(getBlockWidth().doubleValue()));
		element.setAttribute("blockHeight", AveryProject.twipsToMM(getBlockHeight().doubleValue()));
		
		if (getBlockRotation().intValue() != 0)
		{
			element.setAttribute("blockRotation", getBlockRotation().toString());
		}
		
		// add the textStyle element created by the base class
		element.addContent(super.getAverysoftElement());
		
		// add any hints
    if (hints != null)
    {
      java.util.Enumeration e = hints.keys();
      while (e.hasMoreElements())
      {
        Averysoftable hint = (Hint)hints.get(e.nextElement());
        element.addContent(hint.getAverysoftElement());
      }
    }
    
		return element;
	}

	/**
	 * creates a new AveryTextblock and configures it from this object
	 * @return
	 */
	AveryTextblock createTextblock()
	{
		AveryTextblock textblock = new AveryTextblock();
		textblock.setPosition(this.getBlockPosition());
		textblock.setHeight(this.getBlockHeight());
		textblock.setWidth(this.getBlockWidth());
		textblock.setRotation(new Double(this.getBlockRotation().doubleValue()));
		
		textblock.setPointsize(this.getPointSize());
		textblock.setTextcolor(this.getTextColor());
		textblock.setTypeface(this.getTypeface());
		textblock.setBold(this.isBold());
		textblock.setItalic(this.isItalic());
		textblock.setUnderline(this.isUnderline());
		
		if (this.isTopAligned())
			textblock.setVerticalAlignment("top");
		else if (this.isMiddleAligned())
			textblock.setVerticalAlignment("middle");
		else if (this.isBottomAligned())
			textblock.setVerticalAlignment("bottom");
		
		if (this.isLeftJustified())
			textblock.setJustification("left");
		else if (this.isCenterJustified())
			textblock.setJustification("center");
		else if (this.isRightJustified())
			textblock.setJustification("right");
		else if (this.isFullJustified())
			textblock.setJustification("full");
		
		return textblock;
	}
	
  /**
   * Hints are stored as name/value pairs in averysoft XML,
   * and as a Hashtable in the AveryProject object.
   * @param name
   * @return the named Hint, or <code>null</code> if the Hint does not exist
   */
  public Hint getHint(String name)
  {
    if (hints != null)
    {
      return (Hint)hints.get(name);
    }
    return null;
  }

  /**
   * This sets a Hint in the project.  There is validation of neither
   * name nor value.  The Hint will be written into averysoft XML.
   * @param name - the name used for the Hint
   * @param value - the value of the Hint
   */
  private void addHint(String name, String value)
  {
    if (hints == null)
    {
      hints = new Hashtable();
    }
    else if (hints.contains(name))
    {
      hints.remove(name);
    }

    hints.put(name, new Hint(name, value));
  }
  
  /**
   * This reverses width and height, and adjusts position accordingly.
   * It does nothing if rotation is not zero.
   * @param newContainerWidth - the width in twips of the containing masterpanel, to calculate new X position
   */
  void reorient(double newContainerWidth)
  {
    if (blockRotation.intValue() == 0)
    {
      double newWidth = blockHeight.doubleValue();
      double newHeight = blockWidth.doubleValue();
      double newX = newContainerWidth - newWidth - blockPosition.y;
      double newY = blockPosition.x;
      
      blockWidth = new Double(newWidth);
      blockHeight = new Double(newHeight);
      blockPosition = new TwipsPosition(newX, newY);
    }
  }
}
