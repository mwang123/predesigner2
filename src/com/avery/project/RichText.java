package com.avery.project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.io.StringReader;

import org.jdom.Attribute;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.avery.project.richtext.RichParagraph;

public class RichText extends Text
{
  private Element originalContent = null;
  private Element currentRoot = null;
  private List paragraphs = null;

  public static final Namespace ADOBE_NAMESPACE = Namespace.getNamespace("http://ns.adobe.com/textLayout/2008");

  public static final String ELEMENT_TEXT_FLOW = "TextFlow";
  public static final String ELEMENT_PARAGRAPH = "p";
  
  private HashMap mergeFields = new HashMap();

  private String vAlign = "top";
  private String textAlign = "left";
  
  public RichText(Element element)
  {
    super(element);
    originalContent = super.getAverysoftElement();
    currentRoot = originalContent;
    paragraphs = new ArrayList();
    Iterator iter = element.getChildren().iterator();
    iterateTextElements(iter);
  }

  public RichText(String content)
  {
  	SAXBuilder builder = new SAXBuilder(false);
  	Element element = null;
  	try
  	{
  		element = builder.build(new StringReader(content)).getRootElement();
  	}
  	catch (Exception e)
  	{
  		System.err.println("Unable to parse RichText XML String in Constructor.");
  		return;
  	}
   	setContent(element.getText());
   	if (content == null || content.length() <= 0)
      content = " ";
    originalContent = super.getAverysoftElement();
    currentRoot = originalContent;
    paragraphs = new ArrayList();
    Iterator iter = element.getChildren().iterator();
    iterateTextElements(iter);
  }
  
  private void iterateTextElements(Iterator iter)
  {
    while (iter.hasNext())
    {
      Element child = (Element) iter.next();
      if (ELEMENT_PARAGRAPH.equals(child.getName()))
      {
        Element currentElement = new Element(ELEMENT_PARAGRAPH);
        currentElement.setNamespace(ADOBE_NAMESPACE);
        
        Iterator attributes = child.getAttributes().iterator();
        while (attributes.hasNext())
        {
          Attribute atr = (Attribute) attributes.next();
          if (atr.getName().equals("textAlign"))
          	textAlign = atr.getValue();
          currentElement.setAttribute(atr.getName(), atr.getValue());
        }
        
        paragraphs.add(new RichParagraph(child, currentElement, mergeFields));
        currentRoot.addContent(currentElement);
      }
      else if (ELEMENT_TEXT_FLOW.equalsIgnoreCase(child.getName()))
      {
        Element currentElement = new Element(ELEMENT_TEXT_FLOW);
        currentElement.setNamespace(ADOBE_NAMESPACE);

        Iterator attributes = child.getAttributes().iterator();
        while (attributes.hasNext())
        {
          Attribute atr = (Attribute) attributes.next();
          if (atr.getName().equals("verticalAlign"))
          	vAlign = atr.getValue();
          	
          currentElement.setAttribute(atr.getName(), atr.getValue());
        }

        currentRoot.addContent(currentElement);
        currentRoot = currentElement;
        iterateTextElements(child.getChildren().iterator());
      }
    }
  }

  public List getParagraphs()
  {
    return paragraphs;
  }

  public Element getAverysoftElement()
  {
    return originalContent;
  }

  public HashMap getMergeFields()
  {
    return mergeFields;
  }

  public String getVAlign()
  {
  	return vAlign;
  }
  
  public String getTextAlign()
  {
  	return textAlign;
  }
  
  public void setMergeFields(HashMap mergeFields)
  {
    this.mergeFields = mergeFields;
  }
  
  public void putMergeKey(String key, String value)
  {
    mergeFields.put(key, value);
  }
}
