/*
 * Descriptions.java Created on Apr 12, 2005 by Bob Lee
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.project;

import java.io.File;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.avery.Averysoft;
import com.avery.utils.AveryException;

/**
 * Holds localized description tables as static objects
 * @author Bob Lee
 */
public class Descriptions
{
	/**
	 * private - No external construction allowed.
	 * This constructor is called at the end of this file
	 * to generate a singleton at system startup
	 * so that the tables are never empty.
	 */
	private Descriptions()
	{
		initialize();		// default initialization
	}
	
	// static members have package scope for direct access by Description objects
	static Hashtable panelLists = new Hashtable();
	static Hashtable pageLists = new Hashtable();
	static Hashtable textFieldLists = new Hashtable();
	static Hashtable imageFieldLists = new Hashtable();
	static Hashtable backgroundFieldLists = new Hashtable();
	static Hashtable barcodeFieldLists = new Hashtable();
  static Hashtable drawingFieldLists = new Hashtable();
	
	// These indexes are used to create default Descriptions for the various objects
	static final int DEFAULT_PANEL_INDEX = 16;
	static final int DEFAULT_PAGE_INDEX = 16;
	static final int DEFAULT_TEXT_INDEX = 16;
	static final int DEFAULT_IMAGE_INDEX = 2;
	static final int DEFAULT_BACKGROUND_INDEX = 0;
	static final int DEFAULT_BARCODE_INDEX = 0;
  static final int DEFAULT_DRAWING_INDEX = 0;
	
	/**
	 * Language codes are used to select the table that an indexed description
	 * will draw its text from.
	 * @return an Enumeration of supported languages loaded in the Description system
	 */
	public static Enumeration getSupportedLanguages()
	{
		return panelLists.keys();
	}
	
	private static void initialize()
	{
		panelLists = new Hashtable();
		panelLists.put("en", Arrays.asList(defaultPanels));

		pageLists = new Hashtable();
		pageLists.put("en", Arrays.asList(defaultPages));

		textFieldLists = new Hashtable();
		textFieldLists.put("en", Arrays.asList(defaultTextFields));

		imageFieldLists = new Hashtable();
		imageFieldLists.put("en", Arrays.asList(defaultImageFields));

		backgroundFieldLists = new Hashtable();
		backgroundFieldLists.put("en", Arrays.asList(defaultBackgroundFields));

		barcodeFieldLists = new Hashtable();
		barcodeFieldLists.put("en", Arrays.asList(defaultBarcodeFields));
    
    drawingFieldLists = new Hashtable();   
    drawingFieldLists.put("en", Arrays.asList(defaultDrawingFields));
	}
	
	/**
	 * Initialize the static localization table from an averysoft descriptions
	 * XML file.  This method is public and should be called on startup of
	 * any application that expects to translate descriptions.
	 * @param file
	 * @throws Exception if anything goes wrong (a default initialization is performed)
	 */
	public static void initialize(File file)
	throws Exception
	{
		try
		{
			SAXBuilder builder = new SAXBuilder(true);
			builder.setFeature("http://apache.org/xml/features/validation/schema", true);
			builder.setProperty(
					"http://apache.org/xml/properties/schema/external-schemaLocation",
					Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 
			
			Element root = builder.build(file).getRootElement();
			if (root.getName().equals("descriptions") == false)
			{
				throw new AveryException(file.getAbsolutePath() 
						+ " is not a valid averysoft descriptions XML file");
			}
			
			Iterator iterator = root.getChildren().iterator();
			while (iterator.hasNext())
			{
				// create a new list
				Element listElement = (Element)iterator.next();
				List children = listElement.getChildren("description", AveryProject.getAverysoftNamespace());
				String list[] = new String[children.size()];				
				Iterator childIterator = children.iterator();
				while (childIterator.hasNext())
				{
					Element child = (Element)childIterator.next();
					int index = Integer.parseInt(child.getAttributeValue("index"));
					list[index] = child.getText();
				}
				
				// Add the localized list to the appropriate HashTable
				// See averysoft.xsd to fully understand this structure
				String language = listElement.getAttributeValue("language");
				if (listElement.getName().equals("pageDescriptions"))
				{
					pageLists.put(language, Arrays.asList(list));
				}
				else if (listElement.getName().equals("panelDescriptions"))
				{
					panelLists.put(language, Arrays.asList(list));
				}
				else if (listElement.getName().equals("fieldDescriptions"))
				{
					if (listElement.getAttributeValue("fieldType").equals("text"))
					{
						textFieldLists.put(language, Arrays.asList(list));
					}
					else if (listElement.getAttributeValue("fieldType").equals("image"))
					{
						imageFieldLists.put(language, Arrays.asList(list));
					}
					else if (listElement.getAttributeValue("fieldType").equals("background"))
					{
						backgroundFieldLists.put(language, Arrays.asList(list));
					}
					else if (listElement.getAttributeValue("fieldType").equals("barcode"))
					{
						barcodeFieldLists.put(language, Arrays.asList(list));
					}
          else if (listElement.getAttributeValue("fieldType").equals("drawing"))
          {
            drawingFieldLists.put(language, Arrays.asList(list));
          }
				}
			}	// end while loop
		}
		catch (Exception ex)
    {
			initialize();		// back to square one
			ex.printStackTrace();
			throw ex;
		}
	}
	
	private final static String[] defaultPanels =
	{
		/*0*/ "Back",
		/*1*/ "Badge",
		/*2*/ "Bookmark",
		/*3*/ "Business Card",
		/*4*/ "Card",
		/*5*/ "CD/DVD Label",
		/*6*/ "Cover",
		/*7*/ "Divider",
		/*8*/ "Envelope",
		/*9*/ "Front",
		/*10*/ "Index",
		/*11*/ "Insert",
		/*12*/ "Inside Left",
		/*13*/ "Inside Right",
		/*14*/ "Inside Spread",
		/*15*/ "Jewel Case Insert",
		/*16*/ "Label",
		/*17*/ "Magnet",
		/*18*/ "Full Page",
		/*19*/ "Photo",
		/*20*/ "Postcard Front",
		/*21*/ "Section",
		/*22*/ "Sign Sheet",
		/*23*/ "Sleeve",
		/*24*/ "Spine",
		/*25*/ "Sticker",
		/*26*/ "Tab",
		/*27*/ "Table of Contents",
		/*28*/ "Tag",
		/*29*/ "Tray Insert",
		/*30*/ "T-Shirt Transfer",
		/*31*/ "Postcard Back",
		/*32*/ "ID Card",
		/*33*/ "Profile",
		/*34*/ "Certificate",
		/*35*/ "Seal",
		/*36*/ "Bottom Flap",
		/*37*/ "Business Card Back",
		/*38*/ "Business Card Front",
		/*39*/ "Center",
		/*40*/ "Face",
		/*41*/ "Inside Bottom",
		/*42*/ "Inside Top",
		/*43*/ "Left Flap",
		/*44*/ "Right Flap",
		/*45*/ "Top Flap",
    /*46*/ "1/4 to 5/16 Inch Spine",
    /*47*/ "3/8 to 1/2 Inch Spine",
    /*48*/ "5/8 to 3/4 Inch Spine",
    /*49*/ "1 Inch or Larger Spine",
    /*50*/ "1 Inch Spine",
    /*51*/ "1 1/2 Inch Spine",
    /*52*/ "2 Inch Spine",
    /*53*/ "3 Inch or Larger Spine",
    /*54*/ "Compact Flash Label",
    /*55*/ "SD Card Label",
    /*56*/ "Memory Stick Label",
    /*57*/ "Smart Media Label",
    /*58*/ "Top",
    /*60*/ "Slim Case Spine",
    /*61*/ "Address",
    /*62*/ "Postage",
    /*63*/ "Small Face",
    /*64*/ "Small Case",
    /*65*/ "Wide Sticker",
    /*66*/ "Marble Label",
    /*67*/ "Block Label",
    /*68*/ "Stick Label",
    /*69*/ "Bottom",
    /*70*/ "Tag Left",
		/*71*/ "Tag Right",
		/*72*/ "Tag",
		/*73*/ "Rectangle Small",
		/*74*/ "Rectangle Large",
		/*75*/ "Header Small",
		/*76*/ "Header Large",
		/*77*/ "Callout",
		/*78*/ "Heart",
		/*79*/ "Round",
		/*80*/ "Ellipse",
		/*81*/ "Border 1 5/8",
		/*82*/ "Border 1 1/2",
		/*83*/ "Border 1",
		/*84*/ "Border 1 1/4",
		/*85*/ "Border 1 1/8",
		/*86*/ "Front and Back",
		/*87*/ "Front Left",
		/*88*/ "Front Center",
		/*89*/ "Front Right",
		/*90*/ "Back Left",
		/*91*/ "Back Center",
		/*92*/ "Back Right",
		/*93*/ "Front Top",
		/*94*/ "Front Bottom",
		/*95*/ "Back Top",
		/*96*/ "Back Bottom",
		/*97*/ "Left",
		/*98*/ "Right"
	};

	private final static String[] defaultPages =
	{
		/*0*/ "Back of Sheet",
		/*1*/ "Badge Sheet",
		/*2*/ "Bookmarks",
		/*3*/ "Card Sheet",
		/*4*/ "Cover Sheet",
		/*5*/ "Divider",
		/*6*/ "Envelopes",
		/*7*/ "Front of Sheet",
		/*8*/ "Front and Back",
		/*9*/ "Inserts",
		/*10*/ "Inside",
		/*11*/ "Label Sheet",
		/*12*/ "Magnet Sheet",
		/*13*/ "Outside",
		/*14*/ "Paper",
		/*15*/ "Report Cover",
		/*16*/ "Sheet",
		/*17*/ "Sign",
		/*18*/ "Sleeves",
		/*19*/ "Spines",
		/*20*/ "Sticker Sheet",
		/*21*/ "TOC Sheet",
		/*22*/ "Tab Sheet",
		/*23*/ "Tag Sheet",
		/*24*/ "T-Shirt Transfer Sheet",
		/*25*/ "Index Sheet",
		/*26*/ "Face",
		/*27*/ "Front of Card",
		/*28*/ "Back of Card",
		/*29*/ "Outside of Card",
		/*30*/ "Inside of Card"
	};

	private final static String[] defaultBackgroundFields =
	{
		/*0*/ "Background"
	};

	private final static String[] defaultBarcodeFields =
	{
		/*0*/ "Barcode",
		/*1*/ "Barcode 1",
		/*2*/ "Barcode 2",
		/*3*/ "Barcode 3",
		/*4*/ "Barcode 4"
	};

	private final static String[] defaultImageFields =
	{
		/*0*/ "Logo",
		/*1*/ "Photo",
		/*2*/ "Graphic",
		/*3*/ "Graphic 1",
		/*4*/ "Graphic 2",
		/*5*/ "Graphic 3",
		/*6*/ "Graphic 4",
		/*7*/ "Graphic 5",
		/*8*/ "Graphic 6",
		/*9*/ "Graphic 7",
		/*10*/ "Graphic 8",
		/*11*/ "Graphic 9",
		/*12*/ "Graphic 10",
		/*13*/ "Graphic 11",
		/*14*/ "Graphic 12",
		/*15*/ "Graphic 13",
		/*16*/ "Graphic 14",
		/*17*/ "Graphic 15",
		/*18*/ "Graphic 16",
		/*19*/ "Graphic 17",
		/*20*/ "Graphic 18",
		/*21*/ "Graphic 19",
		/*22*/ "Graphic 20",
		/*23*/ "BackImage1",
		/*24*/ "BackImage2",
		/*25*/ "BackImage3",
		/*26*/ "BackImage4",
		/*27*/ "BackImage5",
		/*28*/ "BackImage6",
		/*29*/ "BackImage7",
		/*30*/ "BackImage8",
		/*31*/ "BackImage9",
		/*32*/ "BackImage10",
		/*33*/ "FrontImage1",
		/*34*/ "FrontImage2",
		/*35*/ "FrontImage3",
		/*36*/ "FrontImage4",
		/*37*/ "FrontImage5",
		/*38*/ "FrontImage6",
		/*39*/ "FrontImage7",
		/*40*/ "FrontImage8",
		/*41*/ "FrontImage9",
		/*42*/ "FrontImage10",
	};

	private final static String[] defaultTextFields =
	{
		/*0*/ "Additional Information",
		/*1*/ "Address",
		/*2*/ "Address and Phone",
		/*3*/ "Artist",
		/*4*/ "Author",
		/*5*/ "Company",
		/*6*/ "Company Address",
		/*7*/ "Company Description",
		/*8*/ "Company Name",
		/*9*/ "Contact Information",
		/*10*/ "Contents",
		/*11*/ "Contents 1",
		/*12*/ "Contents 2",
		/*13*/ "Created By",
		/*14*/ "Date",
		/*15*/ "Date and Time",
		/*16*/ "Description",
		/*17*/ "Description 1",
		/*18*/ "Description 2",
		/*19*/ "Description 3",
		/*20*/ "Description 4",
		/*21*/ "E-mail",
		/*22*/ "Event",
		/*23*/ "Event Information",
		/*24*/ "Event Name",
		/*25*/ "Heading",
		/*26*/ "Heading 1",
		/*27*/ "Heading 2",
		/*28*/ "Information",
		/*29*/ "Information 1",
		/*30*/ "Information 2",
		/*31*/ "Instructions",
		/*32*/ "Invitation",
		/*33*/ "Message",
		/*34*/ "Message 1",
		/*35*/ "Message 2",
		/*36*/ "Message 3",
		/*37*/ "Message 4",
		/*38*/ "Name",
		/*39*/ "Name and Title",
		/*40*/ "Phone",
		/*41*/ "Phone 1",
		/*42*/ "Phone 2",
		/*43*/ "Phone and E-mail",
		/*44*/ "Recipient Address",
		/*45*/ "Recipient Name",
		/*46*/ "Sender Address",
		/*47*/ "Sender Name",
		/*48*/ "Ship To",
		/*49*/ "Ship To Address",
		/*50*/ "Ship To Name",
		/*51*/ "Songs",
		/*52*/ "Subtitle",
		/*53*/ "Subtitle 1",
		/*54*/ "Subtitle 2",
		/*55*/ "Tab Title",
		/*56*/ "Tab 1",
		/*57*/ "Tab 2",
		/*58*/ "Tab 3",
		/*59*/ "Tab 4",
		/*60*/ "Tab 5",
		/*61*/ "Tab 6",
		/*62*/ "Tab 7",
		/*63*/ "Tab 8",
		/*64*/ "Text",
		/*65*/ "Title",
		/*66*/ "Title and Contents",
		/*67*/ "Track list",
		/*68*/ "Tracks",
		/*69*/ "Website",
		/*70*/ "Year",
		/*71*/ "NoTextbox",
		/*72*/ "BackText1",
		/*73*/ "BackText2",
		/*74*/ "BackText3",
		/*75*/ "BackText4",
		/*76*/ "BackText5",
		/*77*/ "BackText6",
		/*78*/ "BackText7",
		/*79*/ "BackText8",
		/*80*/ "BackText9",
		/*81*/ "BackText10",
		/*82*/ "FrontText1",
		/*83*/ "FrontText2",
		/*84*/ "FrontText3",
		/*85*/ "FrontText4",
		/*86*/ "FrontText5",
		/*87*/ "FrontText6",
		/*88*/ "FrontText7",
		/*89*/ "FrontText8",
		/*90*/ "FrontText9",
		/*91*/ "FrontText10",
		/*92*/ "Merge",
	};
  
  private final static String[] defaultDrawingFields =
  {
    /*0*/ "Shape",
    /*1*/ "Shape 1",
    /*2*/ "Shape 2",
    /*3*/ "Shape 3",
    /*4*/ "Shape 4",
    /*5*/ "Shape 5",
    /*6*/ "Shape 6",
    /*7*/ "Shape 7",
    /*8*/ "Shape 8",
    /*9*/ "Shape 9",
    /*10*/ "Shape 10",
    /*11*/ "Shape 11",
    /*12*/ "Shape 12",
    /*13*/ "Shape 13",
    /*14*/ "Shape 14",
    /*15*/ "Shape 15",
    /*16*/ "Shape 16",
    /*17*/ "Shape 17",
    /*18*/ "Shape 18",
    /*19*/ "Shape 19",
    /*20*/ "Shape 20",
  };
	
	// force constructor once
	private static Descriptions singleton = new Descriptions(); 
}
