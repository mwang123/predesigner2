
/**
 * MergeData interface.
 *
 * @author Brad Nelson
 * @version 1.0
 */

package com.avery.project;

import java.util.ArrayList;

public interface MergeData
{
	public ArrayList getMergePanelData();
}

