package com.avery.project.richtext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.faceless.pdf2.PDFStyle;
import org.jdom.Attribute;
import org.jdom.Element;

import com.avery.project.RichText;

public class RichParagraph
{
  public static final String ALIGN_USE_PREVIOUS = "start";
  public static final String ALIGN_LEFT = "left";
  public static final String ALIGN_RIGHT = "right";
  public static final String ALIGN_CENTER = "center";
  public static final String ALIGN_JUSTIFY = "justify";

  // private HashMap mergeFields = null;

  public static final String ELEMENT_SPAN = "span";

  public static final String ATRIBUTE_TEXTALIGN = "textAlign";

  private List spans = null;

  private boolean usePreviousAlign = false;

  private int pdfAlign = PDFStyle.TEXTALIGN_LEFT;

  public RichParagraph(Element element, Element savingElement, HashMap mergeFields)
  {
    spans = new ArrayList();
    // this.mergeFields = mergeFields;

    Iterator attributes = element.getAttributes().iterator();
    while (attributes.hasNext())
    {
      Attribute atr = (Attribute) attributes.next();
      savingElement.setAttribute(atr.getName(), atr.getValue());
    }

    String textAlign = element.getAttributeValue(ATRIBUTE_TEXTALIGN);

    if (textAlign == null || ALIGN_USE_PREVIOUS.equals(textAlign))
    {
      usePreviousAlign = true;
    }
    else if (ALIGN_LEFT.equals(textAlign))
    {
      pdfAlign = PDFStyle.TEXTALIGN_LEFT;
    }
    else if (ALIGN_RIGHT.equals(textAlign))
    {
      pdfAlign = PDFStyle.TEXTALIGN_RIGHT;
    }
    else if (ALIGN_CENTER.equals(textAlign))
    {
      pdfAlign = PDFStyle.TEXTALIGN_CENTER;
    }
    else if (ALIGN_JUSTIFY.equals(textAlign))
    {
      pdfAlign = PDFStyle.TEXTALIGN_JUSTIFY;
    }

    Iterator iter = element.getChildren().iterator();
    while (iter.hasNext())
    {
      Element child = (Element) iter.next();
      if (ELEMENT_SPAN.equals(child.getName()))
      {
        Element spanElement = new Element(ELEMENT_SPAN);
        spanElement.setNamespace(RichText.ADOBE_NAMESPACE);
        spans.add(new RichSpan(child, spanElement, mergeFields));
        savingElement.addContent(spanElement);
      }
    }
  }

  public PDFStyle fillAlign(PDFStyle style)
  {
    if (!usePreviousAlign)
      style.setTextAlign(pdfAlign);
    return style;
  }

  public List getSpans()
  {
    return spans;
  }

  public boolean isUsePreviousAlign()
  {
    return usePreviousAlign;
  }

  public int getPdfAlign()
  {
    return pdfAlign;
  }
}
