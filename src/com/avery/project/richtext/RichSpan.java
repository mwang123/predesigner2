package com.avery.project.richtext;

import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;

import org.faceless.pdf2.OpenTypeFont;
import org.faceless.pdf2.PDFStyle;
import org.jdom.Attribute;
import org.jdom.Element;

import com.avery.project.AveryTextfield;

public class RichSpan
{
  public static final String ATRIBUTE_COLOR = "color";
  public static final String ATRIBUTE_FONTFAMILY = "fontFamily";
  public static final String ATRIBUTE_FONTSIZE = "fontSize";
  public static final String ATRIBUTE_FONTSTYLE = "fontStyle";
  public static final String ATRIBUTE_FONTWEIGHT = "fontWeight";
  public static final String ATRIBUTE_TEXTDECORATION = "textDecoration";
  public static final String ATRIBUTE_MAIL_MERGE_FIELD = "mailMergeField";

  public static final String FONTSTYLE_ITALIC = "italic";
  public static final String FONTWEIGHT_BOLD = "bold";
  public static final String TEXTDECORATION_UNDERLINE = "underline";

  private HashMap mergeFields = null;
  private String mFieldName = null;

  private Color fontColor = null;
  private boolean bold = false;
  private boolean italic = false;
  private boolean underline = false;
  private float size = 0;
  private String content = null;
  private String font = null;
  private String serialNumberField = null;

  public RichSpan(Element element, Element savingElement, HashMap mergeFields)
  {
    this.mergeFields = mergeFields;
    Iterator attributes = element.getAttributes().iterator();
    while (attributes.hasNext())
    {
      Attribute atr = (Attribute) attributes.next();
      savingElement.setAttribute(atr.getName(), atr.getValue());
    }

    String colorAtr = element.getAttributeValue(ATRIBUTE_COLOR);
    if (colorAtr != null)
    {
      fontColor = Color.decode(colorAtr);
    }

    String sizeAtr = element.getAttributeValue(ATRIBUTE_FONTSIZE);
    if (sizeAtr != null)
    {
      size = Float.parseFloat(sizeAtr);
    }

    italic = FONTSTYLE_ITALIC.equals(element.getAttributeValue(ATRIBUTE_FONTSTYLE));
    bold = FONTWEIGHT_BOLD.equals(element.getAttributeValue(ATRIBUTE_FONTWEIGHT));
    underline = TEXTDECORATION_UNDERLINE.equals(element.getAttributeValue(ATRIBUTE_TEXTDECORATION));

    font = element.getAttributeValue(ATRIBUTE_FONTFAMILY);
    if (font != null && font.length() > 0)
    {
      processFont();
    }

    serialNumberField = element.getAttributeValue("serialNumberField");
    if (serialNumberField != null && serialNumberField.length() <= 0)
    {
    	serialNumberField = null;
    }

    content = element.getText().replaceAll("[\r\n]", "");
    savingElement.setText(content);
    
    mFieldName = element.getAttributeValue(ATRIBUTE_MAIL_MERGE_FIELD);
    if (mFieldName != null && mFieldName.length() < 1)
    {
      mFieldName = null;
    }
    else
    {
      mergeFields.put(mFieldName, content);
    }
  }

  private void processFont()
  {
    font = font.replaceAll(" BoldItalic| Bold| Italic", "");
    if (bold && italic)
    {
      font += " BoldItalic";
    }
    else if (bold)
    {
      font += " Bold";
    }
    else if (italic)
    {
      font += " Italic";
    }
  }

  public PDFStyle fillStyle(PDFStyle style, AveryTextfield textField)
  {
    OpenTypeFont tmpFont = null;
    if (font != null && font.length() > 0)
    {
      try
      {
        tmpFont = textField.getFont(font);
      }
      catch (Exception e)
      {
        tmpFont = null;
        e.printStackTrace();
      }
    }
    if (tmpFont == null)
    {
      try
      {
        font = textField.getTypeface();
        processFont();
        tmpFont = textField.getFont(font);
      }
      catch (Exception e)
      {
        tmpFont = (OpenTypeFont) style.getFont();
        e.printStackTrace();
      }

    }

    float tmpFontSize = size;
    if (tmpFontSize == 0)
      tmpFontSize = style.getFontSize();

    style.setFont(tmpFont, tmpFontSize);

    if (fontColor != null)
    {
      style.setFillColor(fontColor);
    }

    style.setTextUnderline(underline);

    return style;
  }

  public String getContent()
  {
    return (String) (mFieldName == null ? content : mergeFields.get(mFieldName));
  }

  public Color getFontColor()
  {
    return fontColor;
  }

  public void setFontColor(Color fontColor)
  {
    this.fontColor = fontColor;
  }

  public boolean isBold()
  {
    return bold;
  }

  public void setBold(boolean bold)
  {
    this.bold = bold;
  }

  public boolean isItalic()
  {
    return italic;
  }

  public void setItalic(boolean italic)
  {
    this.italic = italic;
  }

  public boolean isUnderline()
  {
    return underline;
  }

  public void setUnderline(boolean underline)
  {
    this.underline = underline;
  }

  public float getSize()
  {
    return size;
  }

  public void setSize(float size)
  {
    this.size = size;
  }

  public String getFont()
  {
    return font;
  }

  public void setFont(String font)
  {
    this.font = font;
  }

  public String getSerialNumberField()
  {
    return serialNumberField;
  }

  public void setContent(String content)
  {
    this.content = content;
  }
}
