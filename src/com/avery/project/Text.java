/*
 * Text.java Created on Jun 24, 2005 by Bob Lee
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.project;

import java.util.Hashtable;

import org.jdom.Element;

/**
 * Java implementation of the Averysoft XML text element
 */
public class Text implements Averysoftable
{
	private String content;
	private String mergeKey;
	private boolean hardReturn = true;
	
	Text()
	{
		content = " ";
	}
	
  Text(Element element)
  {
   	setMergeKey(element.getAttributeValue("merge"));
   	setContent(element.getText());
   	if (content == null || content.length() <= 0)
      content = " ";
   	if ("false".equals(element.getAttributeValue("hardReturn")))
   		hardReturn = false;
  }
	
	public Text(String content)
	{
		this.content = content;
	}
	
	public Text(String content, String mergeKey)
	{
		this.content = content;
		this.mergeKey = mergeKey;		
	}
	
	Text(Text text)
	{
		this.content = text.content;
		this.mergeKey = text.mergeKey;
	}

	/* (non-Javadoc)
	 * @see com.avery.miwok.Averysoftable#getAverysoftElement()
	 */
	public Element getAverysoftElement()
	{
		//System.out.println(getContent());
		//System.out.println(hardReturn);
    Element textElement = new Element("text");
    textElement.setNamespace(AveryProject.getAverysoftNamespace());
    
    if (hasMergeKey())
    {
    	textElement.setAttribute("merge", getMergeKey());
    }
    
    if (!hardReturn)
    {
    	textElement.setAttribute("hardReturn", "false");
    }
   	
    
    // add the content
    textElement.setText(getContent());

		return textElement;
	}
	
	public Element getAverysoftIBElement()
	{
		//System.out.println(getContent());
		//System.out.println(hardReturn);
    Element textElement = new Element("text");
    textElement.setNamespace(AveryProject.getAverysoftNamespace());
    
    /*if (hasMergeKey())
    {
    	textElement.setAttribute("merge", getMergeKey());
    }*/
    
    if (!hardReturn)
    {
    	textElement.setAttribute("hardReturn", "false");
    }
   	
    
    // add the content
    textElement.setText(getContent());

		return textElement;
	}

	/**
	 * @return Returns the content.
	 */
	public String getContent()
	{
		return content;
	}
	/**
	 * @param content The content to set.
	 */
	public void setContent(String content)
	{
		this.content = (content == null) ? new String("") : content;
	}
	/**
	 * @return Returns the mergeKey or an empty string.
	 */
	public String getMergeKey()
	{
		return hasMergeKey() ? mergeKey : "";
	}
	
	/**
	 * @param mergeKey The mergeKey to set.
	 */
	public void setMergeKey(String mergeKey)
	{
		this.mergeKey = mergeKey;
	}
	
	/**
	 * @return <code>true</code> if this object has an associated mergeKey
	 */
	public boolean hasMergeKey()
	{
		if (this.mergeKey == null || this.mergeKey.length() < 1)	return false;
		else return true;
	}
	
	/**
	 * @param key
	 * @return <code>true</code> if the mergeKey equals <code>key</code> 
	 */
	boolean hasMergeKey(String key)
	{
		return getMergeKey().equals(key);
	}
	
	/**
	 * toString returns only the content, not the mergeKey.
	 * This is for compatibility with clients that aren't interested
	 * in merge functionality.
	 */
	public String toString()
	{
		return getContent();
	}
  
  /**
   * This is a line-by-line translation technique, used to do translations from the SampleText spreadsheet.
   * @param hash - keys are lowercase text to translate, values are the translations
   * @return <code>true</code> if translation failed (caller is counting errors)
   */
  boolean translateText(Hashtable hash)
  {
    if ( content == null ) 
    {
      return false;   // that's okay, don't count it as an error
    }

    // do not forget to exclude lines with blank spaces only.
    // Trim them before use as key.
    String key = this.content.trim().toLowerCase();
    //System.out.println("key=" + key);
    if ( key.length() == 0 )
    {
        return false;   // that's okay, don't count it as an error
    }
    
    if (hash.containsKey(key))
    {
      content = (String)hash.get(key);
      return false;
    }

    return true;  // true means that translation failed
  }
}
