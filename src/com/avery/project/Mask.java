/*
 * Mask.java Created on Jan 20, 2010 by leeb
 * Copyright 2010 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.project;

import java.util.ArrayList;
import java.util.Iterator;

import org.jdom.Element;

import com.avery.project.shapes.AveryShape;

/**
 * @author leeb
 * Jan 20, 2010
 * Mask
 */
class Mask implements Averysoftable
{
  private String id;
  private ArrayList geometries = new ArrayList();
  /**
   * Can only be constructed from an Avery.dtd or Averysoft XML Element
   * @param element
   * @throws Exception
   */
  Mask(Element element)
  throws Exception
  {
    id = element.getAttributeValue("id");
    
    Iterator iterator = element.getChildren("geometry", AveryProject.getAverysoftNamespace()).iterator();
    while (iterator.hasNext())
    {
      geometries.add(new Geometry((Element)iterator.next()));
    }
  }

  /**
   * Get a JDOM representation of the object suitable for
   * inclusion in an XML file based on averysoft.xsd
   * @return an Averysoft-based JDOM Element
   * @see com.avery.project.Averysoftable#getAverysoftElement()
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("mask");
    element.setNamespace(AveryProject.getAverysoftNamespace());
    element.setAttribute("id", id);
    Iterator iterator = geometries.iterator();
    while (iterator.hasNext())
    {
      element.addContent(((Geometry)iterator.next()).getAverysoftElement());
    }
    return element;
  }

	public Element getAverysoftIBElement()
	{
		return null;
	}

  /**
   * @return the id
   */
  String getId()
  {
    return id;
  }

  /**
   * @return the geometries
   */
  ArrayList getGeometries()
  {
    return geometries;
  }
  
  /**
   * @author leeb
   * Jan 20, 2010
   * Geometry
   */
  class Geometry implements Averysoftable
  {
    private AveryShape shape;
    private double x, y;
    /**
     * Can only be constructed from an Averysoft XML Element
     * @param element
     * @throws Exception
     */
    Geometry(Element element)
    throws Exception
    {
      shape = AveryShape.manufactureShape(element);
      
      x = Double.parseDouble(element.getAttributeValue("x"));
      y = Double.parseDouble(element.getAttributeValue("y"));
    }

    /**
     * Get a JDOM representation of the object suitable for
     * inclusion in an XML file based on averysoft.xsd
     * @return an Averysoft-based JDOM Element
     * @see com.avery.project.Averysoftable#getAverysoftElement()
     */
    public Element getAverysoftElement()
    {
      Element element = new Element("geometry");
      element.setNamespace(AveryProject.getAverysoftNamespace());
      element.setAttribute("x", Double.toString(x));
      element.setAttribute("y", Double.toString(y));
      element.setAttribute("width", Double.toString(shape.getWidth()));
      element.setAttribute("height", Double.toString(shape.getHeight()));
      element.setAttribute("shape", shape.getShapeString());
      Element polypointsElement = shape.getPolypointsElement();
      if (polypointsElement != null)
      {
        element.addContent(polypointsElement);
      }
      return element;
    }
    
  	public Element getAverysoftIBElement()
  	{
  		return null;
  	}

  }
}
