package com.avery.project;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;

import org.faceless.pdf2.PDFPage;
import org.jdom.Element;

import com.avery.project.shapes.AveryShape;

/**
 * Title:        AveryCutout<p>
 * Description:  <p>
 * Copyright:    Copyright (c)2000-2004 Avery Dennison Corp. All Rights Reserved<p>
 * Company:      Avery Dennison<p>
 * @author Bob Lee
 */
class AveryCutout implements XMLElement, Averysoftable
{
	private AveryShape shape;
	private Point position;

	/**
	 * Can only be constructed from an Avery.dtd or Averysoft XML Element
	 * @param element
	 * @throws Exception
	 */
  AveryCutout(Element element)
  throws Exception
  {
		shape = AveryShape.manufactureShape(element);
    position = AveryProject.buildPoint(element.getAttributeValue("position"));
  }

	String dump()
	 {
		 String str = "<P>AveryCutout";
		 str += "<br>width = " + getWidth().toString();
		 str += "<br>height = " + getHeight().toString();
		 str += "<br>position = " + getPosition().toString();
		 str += "<br>shape = " + getShapeString();
		 str += "</P>";
		 return str;
	 }

  private Double getWidth()
  {
    return new Double(shape.getWidth());
  }

  private Double getHeight()
  {
    return new Double(shape.getHeight());
  }

  private java.awt.Point getPosition()
  {
    return position;
  }

  private String getShapeString()
  {
    return shape.getShapeString();
  }

  private AveryShape getShape()
  {
  	return shape;
  }

  /**
   * Draws the cutout shape as a colored object with a 1-pixel black outline
   * @param graphics device context for drawing
   * @param scalar scaling multiplier (pixels/twip)
   * @param fillColor
   */
  void draw(Graphics2D graphics, double scalar, Color fillColor)
  {
    double x = getPosition().x * scalar;
    double y = getPosition().y * scalar;
    AffineTransform at = new AffineTransform();
    at.setToTranslation(x, y);
    Shape transformedShape = getShape().scaledGraphicsPath(scalar).createTransformedShape(at);

    graphics.setColor(fillColor);
    graphics.fill(transformedShape);
    graphics.setColor(Color.black);
    graphics.draw(transformedShape);
  }

  /**
   * Draws the cutout shape as a white object with a 1-pixel black outline
   * @param graphics device context for drawing
   * @param scalar scaling multiplier (pixels/twip)
   */
  void draw(Graphics2D graphics, double scalar)
  {
    draw(graphics, scalar, Color.white);
  }

  /**
   * Creates an Avery.cutout element to use in a JDOM Document
   * @return newly created Avery.cutout Element
   */
  public Element getAveryElement()
  {
    Element element = new Element("Avery.cutout");
    // add attributes
    element.setAttribute("width", getWidth().toString());
    element.setAttribute("height", getHeight().toString());
    element.setAttribute("position",
      Double.toString(getPosition().getX()) + ","
      + Double.toString(getPosition().getY()));
    element.setAttribute("shape", getShapeString());
		/* cornerRadius not supported in legacy Avery.dtd
		double cornerRadius = getShape().getCornerRadius();
		if (cornerRadius > 0)
		{
			element.setAttribute("cornerradius", Double.toString(cornerRadius));
		}
		*/
    return element;
  }

	/**
	 * Get a JDOM representation of the object suitable for
	 * inclusion in an XML file based on averysoft.xsd
	 * @return an Averysoft-based JDOM Element
	 */
	public Element getAverysoftElement()
	{
		Element element = new Element("cutout");
		element.setNamespace(AveryProject.getAverysoftNamespace());
		
		element.setAttribute("width", getWidth().toString());
		element.setAttribute("height", getHeight().toString());
		element.setAttribute("position",
				Double.toString(getPosition().getX()) + ","
				+ Double.toString(getPosition().getY()));
		
		element.setAttribute("shape", getShapeString());
		double cornerRadius = getShape().getCornerRadius();
		if (cornerRadius > 0)
		{
			element.setAttribute("cornerRadius", Double.toString(cornerRadius));
		}
		Element polypointsElement = getShape().getPolypointsElement();
		if (polypointsElement != null)
		{
			element.addContent(polypointsElement);
		}

		return element;
	}
  
	public Element getAverysoftIBElement()
	{
		Element element = new Element("cutout");
		element.setNamespace(AveryProject.getAverysoftNamespace());
		
		element.setAttribute("width", AveryProject.twipsToMM(getWidth().doubleValue()));
		element.setAttribute("height", AveryProject.twipsToMM(getHeight().doubleValue()));
		element.setAttribute("blockPosition",
				AveryProject.twipsToMM(getPosition().getX()) + ","
				+ AveryProject.twipsToMM(getPosition().getY()));
		
		element.setAttribute("shape", getShapeString());
		double cornerRadius = getShape().getCornerRadius();
		if (cornerRadius > 0)
		{
			element.setAttribute("cornerRadius", Double.toString(cornerRadius));
		}
		Element polypointsElement = getShape().getPolypointsElement();
		if (polypointsElement != null)
		{
			element.addContent(polypointsElement);
		}

		return element;
	}
	
  /**
   * draws the outline into a PDF file, using the current pdfStyle.
   * @param pdfPage
   * @param pdfX1 - left edge of container on page, in points
   * @param pdfY1 - bottom edge of container on page, in points
   * @param pdfX2 - right edge of container on page, in points
   * @param pdfY2 - top edge of container of page, in points
   */
  void addOutlineToPDF(PDFPage pdfPage, float pdfX1, float pdfY1, float pdfX2, float pdfY2)
  {
    float height = getHeight().floatValue() / 20;
    float x1 = pdfX1 + (position.x / 20);
    float y1 = pdfY2 - (position.y / 20) - height;
    float x2 = x1 + (getWidth().floatValue() / 20);
    float y2 = y1 + height;
    
    if ("ellipse".equals(shape.getShapeString()))
    {
      pdfPage.drawEllipse(x1, y1, x2, y2);
    }
    else if ("rect".equals(shape.getShapeString()))
    {
      pdfPage.drawRoundedRectangle(x1, y1, x2, y2, (float)(shape.getCornerRadius() / 20));
    }
    else // polygon shape
    {
    	GeneralPath path = shape.scaledGraphicsPath(.05);
    	PathIterator pathIter= path.getPathIterator( new AffineTransform(1, 0, 0, -1, x1, y2)); //height)); 
    	
    	float[] coords = new float[6];
    	while (!pathIter.isDone())
    	{
    		int pathSegment = pathIter.currentSegment(coords);
    		switch (pathSegment)
    		{
    			case PathIterator.SEG_MOVETO:
    				pdfPage.pathMove(coords[0], coords[1]);
    				break;
    				
    			case PathIterator.SEG_LINETO:
    				pdfPage.pathLine(coords[0], coords[1]);
    				break;
    				
    				// never used there is not an analog to quad in PDF
    			//case PathIterator.SEG_QUADTO:
    				//pdfPage.pathArc(coords[0], coords[1], coords[2], coords[3]);
    				//break;
    				
    			case PathIterator.SEG_CUBICTO:
    				pdfPage.pathBezier(coords[0], coords[1], coords[2], coords[3], 
    						coords[4], coords[5]);
    				break;
    				
    			default:
    				break;
    		}
    		pathIter.next();   		
    	}
    	pdfPage.pathPaint();
    }
  }
	
	// rotate by 90 or 270 degrees
	public void reorient(Double masterWidth, Double masterHeight, boolean clockwise)
	{
		double cutoutWidth = shape.getWidth();
		double cutoutHeight = shape.getHeight();
		
		double width = masterWidth.doubleValue();
		double height = masterHeight.doubleValue();
		
		if (clockwise)
		{
			int startX = position.x;
			position.x = (int)(height - position.y - cutoutHeight);				
			position.y = startX;
		}
		else
		{
			int startX = position.x;
			position.x = position.y;				
			position.y = (int)(width - startX - cutoutWidth);
		}
		
		shape.reorient(clockwise);
	}
	
}