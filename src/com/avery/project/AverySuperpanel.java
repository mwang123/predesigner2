/**
 * Title:       AverySuperpanel<p>
 * Description: base class for AveryPanel and AveryMasterpanel<p>
 * Copyright:   (c)Copyright 2000-2001 Avery Dennison Corp. All Rights Reserved.
 * @author      Bob Lee
 * @version     1.5
 */

package com.avery.project;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.jdom.Element;

import com.avery.utils.AveryUtils;

/**
 * AverySuperpanel is the common base class for {@link AveryPanel}, which
 * describes panels on a page, and {@link AveryMasterpanel}, which describes
 * panels that can be replicated in multiple positions on multiple pages.
 * Every AveryPanel references an AveryMasterpanel for completeness (the
 * master describes the overall shape of the panel).  The common elements
 * stored in the base class are a List of {@link AveryPanelField}s, a List of
 * {@link AveryCutout}s, and a description string.
 */
public abstract class AverySuperpanel implements XMLElement
{
  /**
   * The constructor makes a default description object for safety
   */
  public AverySuperpanel()
  {
  	description = Description.makePanelDescription();
  	usingDefaultDescription = true;
  }

  private Description description;
  protected boolean usingDefaultDescription;
  private List panelFields = new LinkedList();
  private List cutouts = new ArrayList();

  /**
   * Generates an HTML-formatted string that describes the panel.  This can be
   * useful for debugging.  {@link AveryPage#dump} calls this method.
   * @return HTML-formatted String
   */
  String dump()
  {
    String str;
    str = "<br>description = " + getDescription();

    Iterator i = panelFields.iterator();
    while (i.hasNext())
    {
      str += ((AveryPanelField)i.next()).dump();
    }

    i = cutouts.iterator();
    while (i.hasNext())
    {
      str += ((AveryCutout)i.next()).dump();
    }

    return str;
  }

  protected void setDescription(Element element)
  {
  	description = Description.makePanelDescription(element);
  }

  public void setDescription(String text)
  {
  	getDescriptionObject().setText(text);
  }

  /**
   * Retrieves the description String
   * @return the description, used for identification
   * @see #setDescription
   */
  public String getDescription()
  {
  	if (getDescriptionObject() != null)
  	{
  		return getDescriptionObject().getText();
  	}
    return null;
  }

  public java.util.List getLocalizableDescriptions(String language)
  {
  	return getDescriptionObject().getLocalizedStringList(language);
  }

	public Description getDescriptionObject()
	{
		return description;
	}

	protected void setDescriptionObject(Description d)
	{
		description = d;
	}

  /**
   * Retrieves the internal List of panelFields.
   * @return a List of {@link AveryPanelField} objects
   */
  public List getPanelFields()
  {
    return panelFields;
  }

  /**
   * Allows out-of-package callers to safely iterate the field list.
   * @return an iterator of the panelFields list
   */
  public Iterator getFieldIterator()
  {
    return panelFields.iterator();
  }

  /**
   * Adds a panelField to this panel
   * @param field - the field to be added
   */
  void addField(AveryPanelField field)
  {
    panelFields.add(field);
  }

  /**
   * Provides access to the list of cutouts
   * @return a List of {@link AveryCutout} objects
   */
  List getCutouts()
  {
    return cutouts;
  }

  void setCutouts(List list)
  {
    cutouts = list;
  }
  
  protected void clearCutouts()
  {
    cutouts.clear();
  }

  /**
   * Adds a cutout to the panel's cutout list
   * @param cutout - the {@link AveryCutout} to be added
   */
  public void addCutout(AveryCutout cutout)
  {
    cutouts.add(cutout);
  }

  /**
   * This is a "fixup" for projects that didn't have image galleries when they
   * were created, or have invalid galleries now.
   * All AveryImage fields in this panel that have bogus gallery
   * attributes are assigned to the new gallery.
   * @param newGallery path to a directory that has HighRes and LowRes subdirectories
   * @return the number of AveryImage fields couldn't be "fixed" (image file
   * doesn't exist in the original or new gallery).
   * @see AveryProject#fixEmptyGalleries
   */
  public int fixGalleries(String newGallery)
  {
    int counter = 0;
    Iterator j = getPanelFields().iterator();
    while (j.hasNext())
    {
      AveryPanelField field = (AveryPanelField)j.next();
      if (field instanceof AveryImage)
      {
        if (((AveryImage)field).fixGallery(newGallery) == false)
        {
          ++counter;  // counting failures
        }
      }
    }
    return counter;
  }

 /**
   * finds the super panel that the field with given field id belongs to
   * @param refID the panel field id
   * @return the AverySuperpanel object or null if not found
   */
  protected AverySuperpanel findPanel(String refID)
  {
    AverySuperpanel superPanel = null;

    // check fields for matching id
    Iterator iterator = getPanelFields().iterator();
    while (iterator.hasNext())
    {
      AveryPanelField field = (AveryPanelField)iterator.next();
      if (field.getID().equals(refID))
      {
        superPanel = this;
        break;
      }
    }
    return superPanel;
  }

  /**
   * This abstract AverySuperPanel method generates a list of all of the
   * the fields associated with this panel, sorted by their prompt order.
   * @return a list of fields sorted by prompt order
   * @throws Exception if anything goes wrong
   */
  abstract List getPromptSortedPanelFields()
    throws Exception;

  /**
   * Updates a field in this panel by replacing it with the given field
   * @param newField This field will replace the field that has the same z-order
   *        in the panel, if they don't match.  If no field with that z-order
   *        exists, newField is simply added to the panel.
   * @return the old field if a field was replaced (for Undo?), or
   *        <code>null</code> if no action was taken
   */
  AveryPanelField updatePanelField(AveryPanelField newField)
  {
    Iterator i = getPanelFields().iterator();
    while (i.hasNext())
    {
      AveryPanelField currentField = (AveryPanelField)i.next();
      if (currentField.getZLevel() == newField.getZLevel())  // found a match?
      {
        if (currentField.matches(newField))
        {
          return null;    // no need to replace it; our work here is done
        }
        getPanelFields().remove(currentField);
        getPanelFields().add(newField);
        return currentField;
      }
    }

    // field not found; just add the new field
    getPanelFields().add(newField);
    return null;
  }


  /**
   * Uses Java Graphics2D to draw the panel
   * @param graphics the drawing context
   * @param nTargetWidth width in pixels (used for scaling)
   * @param nTargetHeight height in pixels (used for scaling)
   * @param master masterpanel to draw fields from
   * @throws Exception when something goes horribly wrong
   */
  abstract void draw(Graphics2D graphics, int nTargetWidth, int nTargetHeight, AveryMasterpanel master)
    throws Exception;

  /**
   * Draws the cutout shape(s)
   * @param graphics - device context for drawing
   * @param scalar - scale factor for drawing
   */
  protected void drawCutouts(Graphics2D graphics, double scalar)
  {
    Iterator i = getCutouts().iterator();
    while (i.hasNext())
    {
      ((AveryCutout)i.next()).draw(graphics, scalar);
    }
  }

  /**
   * Helper class DrawIterator is constructed with a list of AveryPanelFields.
   * DrawIterator.next() will return the fields in the proper zOrder for
   * rendering.
   */
  protected class DrawIterator implements Iterator
  {
    private Object[] objects;
    private int drawn = 0;
    private int items;

    /**
     * constructor initializes by accepting a List of panelFields
     * @param list - a list of {@link AveryPanelField} objects that will be
     * returned in their proper drawing order.
     */
    public DrawIterator(List list)
    {
      items = list.size();
      objects = list.toArray();
      Arrays.sort(objects, new DrawComparator());
    }

    /**
     * standard Iterator override
     * @return <code>true</code> if there are more panelFields to iterate through
     */
    public boolean hasNext()
    {   return drawn < items;     }

    /**
     * standard Iterator override
     * @return the next {@link AveryPanelField} Object in the iterator
     */
    public Object next()
    {   return objects[drawn++];  }

    /**
     * For completeness; not supported.
     * @throws UnsupportedOperationException always as expected
     */
    public void remove()
    {   throw new UnsupportedOperationException();  }

    private class DrawComparator implements Comparator
    {
      public int compare(Object o1, Object o2)
      {
        Double z1 = new Double(((AveryPanelField)o1).getZLevel());
        Double z2 = new Double(((AveryPanelField)o2).getZLevel());
        return z1.compareTo(z2);
      }
    }
  }

  /**
   * used to sort {@link AveryPanelField}s by their prompt order
   */
  protected class PromptComparator implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      Double p1 = Double.valueOf(((AveryPanelField)o1).getPromptOrder());
      Double p2 = Double.valueOf(((AveryPanelField)o2).getPromptOrder());
      return p1.compareTo(p2);
    }
  }

  /**
   * Retrieves the width of the panel.
   * @return the width of the panel, in twips
   */
  abstract Double getWidth();

  /**
   * Retrieves the height of the panel
   * @return the height of the panel in twips
   */
  abstract Double getHeight();

  /**
   * Given a point in a representation of the panel, this function finds the
   * closest editable field and returns its index in the prompt-sorted list.
   * @param x - coordinate, units unknown
   * @param y - coordinate, units unknown
   * @param width - width of the representation of the panel, for scaling x
   * @param height - height of the representation of the panel, for scaling y
   * @return index of the closest field found in the prompt sorted field list
   * @see #getPromptSortedPanelFields
   */
  public int getClickedFieldPromptIndex(int x, int y, int width, int height)
  {
    int fieldIndex = 0;   // will be returned
    try
    { // scale x y point to twipspace
      Point point = new Point();
      point.setLocation((double)x * getWidth().doubleValue() / (double)width,
                        (double)y * getHeight().doubleValue() / (double)height);

      // iterate over the panel field list
      int fieldCounter = 0;
      double distance = 100000000;

      Iterator iterator = getPromptSortedPanelFields().iterator();
      while (iterator.hasNext() && (distance > 0.0))
      {
        Rectangle rect = ((AveryPanelField)iterator.next()).getBoundingRect();
        double distance1 = AveryUtils.findPointRectDistance(point, rect);

        if (distance1 < distance)
        {
          // set field index to closer field
          fieldIndex = fieldCounter;
          // make it tougher to be closer
          distance = distance1;
        }
        // set field index to next field
        fieldCounter++;
      }
    }
    catch (Exception e)
    {
      return 0;
    }
    return fieldIndex;
  }

  /**
   * This method generates a list of the names of all external files associated
   * with this panel.
   * @return list of file names as Strings, or <code>null</code>
   */
  List getFileList()
  {
    List list = null;

    Iterator i = getPanelFields().iterator();
    while (i.hasNext())
    {
      String filename = ((AveryPanelField)i.next()).getAssociatedFilename();
      if (filename != null)
      {
        if (list == null)
        {
          list = new ArrayList();
        }

        list.add(filename);
      }
    }

    return list;
  }

  /**
   * Creates a list containing just the AveryTextfields contained by this panel
   * @return the list
   */
  List getTextFields()
  {
    List list = new ArrayList();
    Iterator i = getPanelFields().iterator();
    while (i.hasNext())
    {
      AveryPanelField field = (AveryPanelField)i.next();

      if (field instanceof AveryTextfield)
      {
        list.add(field);
      }
    }
    return list;
  }

  /**
   * Creates a list containing just the AveryImages contained by this panel
   * @return the list
   */
  List getImageFields()
  {
    List list = new ArrayList();
    Iterator i = getPanelFields().iterator();
    while (i.hasNext())
    {
      AveryPanelField field = (AveryPanelField)i.next();

      if (field instanceof AveryImage)
      {
        list.add(field);
      }
    }
    return list;
  }

  /**
   * Generates an AveryPanel suitable for a .avery Zip file bundle.  Images
   * are given new, unique names starting with <code>prefix</code>, and are
   * copied to the HighRes diretory under <code>location<code>
   * @param prefix - typically the machine name
   * @param location - where the project XML will ultimately be created
   * @param images - tracks the images already processed
   * @return a panel, with image paths deleted, suitable for a .avery bundled project
   */
  AverySuperpanel getPanelForDotAveryBundle(String prefix, File location, Hashtable images)
  {
    List list = getFileList();
    AverySuperpanel panel;

    if (list == null)
    {
      panel = this;
    }
    else // create a new panel, because the Image objects will be different
    {
      if (this instanceof AveryMasterpanel)
      {
        panel = new AveryMasterpanel();
      }
      else // there are only two kinds of panels
      {
        panel = new AveryPanel();
      }

      // add the common attributes
      panel.setDescriptionObject(this.getDescriptionObject());
      panel.setCutouts(this.getCutouts());

      Iterator iterator = getPanelFields().iterator();
      while (iterator.hasNext())
      {
        AveryPanelField field = (AveryPanelField)iterator.next();
        if (field instanceof AveryImage)
        {
          try   // this is the important step
          {
            AveryImage imageField = ((AveryImage)field).getDotAveryImageField(prefix, location, images);
            if (imageField != null)
            {
              panel.addField(imageField);
            }
          }
          catch (IOException e)
          {
            System.err.println("AveryImage.getDotAveryImageField(" + prefix
                            + ", " + location.getAbsolutePath() + ") failed.");
            System.err.println(e.toString());
            System.err.println(field.dump());
          }
        }
        else  // just add the existing field to the new panel's list
        {
          panel.addField(field);
        }
      }
    }

    return panel;
  }

  /**
   * Gives all of the AveryImage fields access to a common Image cache
   * @param list assumed to be empty at this point
   */
  void initializeImageCache(ArrayList list)
  {
    Iterator iterator = getPanelFields().iterator();
    while (iterator.hasNext())
    {
      Object field = iterator.next();
      if (field instanceof AveryImage)
      {
        ((AveryImage)field).setCacheList(list);
      }
    }
  }

  /**
   * Remove a specific panelfield from the list
   * @param prompt - prompt of field to remove
   * @return the field that was removed from the panel's list
   */
  public AveryPanelField removePanelField(String prompt)
  {
    AveryPanelField found = null;

    Iterator iterator = panelFields.iterator();
    while (iterator.hasNext())
    {
      AveryPanelField test = (AveryPanelField)iterator.next();
      if (test.getPrompt().equals(prompt))
      {
        found = test;
        panelFields.remove(test);
        break;
      }
    }

    return found;
  }

  void removeAllPanelFields()
  {
    panelFields.clear();
  }

  public boolean isSquare()
  {
    return getWidth().equals(getHeight());
  }

  public boolean isPortrait()
  {
    return getWidth().compareTo(getHeight()) < 0;
  }

  public boolean isLandscape()
  {
    return getWidth().compareTo(getHeight()) > 0;
  }

  /**
   * This method returns true if the given field description is in use  
   * @param description - the string to match
   * @return <code>true</code> if a match was found
   */
  public boolean usesFieldDescription(String description)
  {
    Iterator i = this.getFieldIterator();
    while (i.hasNext())
    {
      if (((AveryPanelField)i.next()).getDescription().equals(description))
        return true;
    }
    return false; 
  }
}