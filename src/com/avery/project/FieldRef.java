package com.avery.project;

import org.jdom.Element;

import com.avery.utils.AveryException;

/**
 * <p>FieldRef</p>
 * <p>Description: Supports panel data linking by referencing AveryPanelFields</p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Brad Nelson
 * @version 1.0
 */

public class FieldRef implements Averysoftable
{
  // properties
  private String id = "";
  private String contentid = "";
  private String styleid = "";
  private String geometryid = "";
  private boolean visible = true;

  private ProjectFieldTable fieldTable = null;

  public FieldRef()
  {
  }

  /**
   * constructs a field ref from an Avery.fieldRef XML Element
   * @param element - describes the field ref
   * @throws Exception if element isn't up to snuff
   */
  FieldRef(Element element)
  throws Exception
  {
		if (element.getNamespace().equals(AveryProject.getAverysoftNamespace())
			&& element.getName().equals("fieldRef"))
		{
			setID(element.getAttributeValue("id"));
      final String trueString = "true";
      visible = trueString.equals(element.getAttributeValue("visible"));
			setContentID(element.getAttributeValue("contentId"));
      if (element.getAttributeValue("styleId") != null)
			  setStyleID(element.getAttributeValue("styleId"));
      if (element.getAttributeValue("geometryId") != null)
			  setGeometryID(element.getAttributeValue("geometryId"));
    }
		else
		{
			Exception ex = new AveryException("Not a field ref element");
			ex.printStackTrace();
			throw ex;
		}
  }

  /**
   * Sets the id of the field
   * @param newID - the desired id
   */
  public void setID(String newID)
  {
    id = newID;
  }

  /**
   * Retrieves the id of the field
   * @return the field's id
   */
  public String getID()
  {
    return id;
  }

  /**
   * Sets the content id of the field
   * @param newContentID - the desired id
   */
  public void setContentID(String newContentID)
  {
    contentid = newContentID;
  }

  /**
   * Retrieves the content id of the field
   * @return the field's content id
   */
  public String getContentID()
  {
    return contentid;
  }

  /**
   * Sets the style id of the field
   * @param newStyleID - the desired id
   */
  public void setStyleID(String newStyleID)
  {
    styleid = newStyleID;
  }

  /**
   * Retrieves the Style id of the field
   * @return the field's style id
   */
  public String getStyleID()
  {
    return styleid;
  }

  /**
   * Sets the Geometry id of the field
   * @param newGeometryID - the desired id
   */
  public void setGeometryID(String newGeometryID)
  {
    geometryid = newGeometryID;
  }

  /**
   * Retrieves the Geometry id of the field
   * @return the field's geometry id
   */
  public String getGeometryID()
  {
    return geometryid;
  }

  /**
   * Sets the visibility of the field
   * @param newVisible - the desired visibility
   */
  public void setVisible(boolean newVisible)
  {
    visible = newVisible;
  }

  /**
   * Retrieves the visibility of the field
   * @return the field's visibility
   */
  public boolean getVisible()
  {
    return visible;
  }

  public void setProjectFieldTable(ProjectFieldTable newFieldTable)
  {
    fieldTable = newFieldTable;
  }

  public ProjectFieldTable getProjectFieldTable()
  {
    return fieldTable;
  }

  public void createID()
  {
    setID(fieldTable.nextRefID());
  }

	/* (non-Javadoc)
	 * @see com.avery.miwok.Averysoft#getAverysoftElement()
	 */
	public Element getAverysoftElement()
	{
		Element element = new Element("fieldRef");
		element.setNamespace(AveryProject.getAverysoftNamespace());
		element.setAttribute("id", getID());
    if (!getVisible())
      element.setAttribute("visible", "false");
		element.setAttribute("contentId", getContentID());
    if (getStyleID().length() > 0)
		  element.setAttribute("styleId", getStyleID());
    if (getGeometryID().length() > 0)
		  element.setAttribute("geometryId", getGeometryID());

    return element;
  }
	
	public Element getAverysoftIBElement()
	{
		return null;
	}

}