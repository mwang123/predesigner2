/* Project: Miwok
 * Filename: TextStyle.java
 * Created on Jun 15, 2004 by Bob Lee
 * Copyright 2004 by Avery Dennison Corporation, all rights reserved
 */
package com.avery.project;

import java.awt.Color;
import org.jdom.Element;

/**
 * @author b0b
 *
 * Encapsulates the style features of text for the Averysoft XML format
 */
public class TextStyle implements Averysoftable
{
	// serializable attributes
	private String typeface = "Arial";
	private Double pointSize = new Double(12.0);
	private Color textColor = Color.black;
	private int justification = JUST_LEFT;
	private int valign = VERT_TOP;
	private String styles = "STYLE_NONE";
	private int overflow = SHRINKWRAP;

	// valid values for enumerations
	private static final int JUST_LEFT = 0;
	private static final int JUST_RIGHT = 1;
	private static final int JUST_CENTER = 2;
	private static final int JUST_FULL = 3;

	private static final int VERT_TOP = 0;
	private static final int VERT_MIDDLE = 1;
	private static final int VERT_BOTTOM = 2;

	private static final int SHRINKWRAP = 0;
	private static final int WRAPSHRINK = 1;
	private static final int FIT = 2;
	//private static final int WRAP = 2;
	//private static final int SHRINK = 3;

	private static final String STYLE_NONE = "none";
	private static final String STYLE_BOLD = "bold";
	private static final String STYLE_ITALIC = "italic";
	private static final String STYLE_UNDERLINE = "underline";

	/**
	 * makes a deep copy of an existing TextStyle
	 * @param original - a TextStyle to copy
	 */
	public TextStyle(TextStyle original)
	{
		typeface = original.typeface;
		pointSize = new Double(original.getPointSize().doubleValue());
		textColor = new Color(original.getTextColor().getRGB());
		justification = original.justification;
		valign = original.valign;
		styles = new String(original.styles);
		overflow = original.overflow;
	}

  TextStyle()
  {
  }

  TextStyle(Element element)
  throws Exception
  {
    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace())
    	&& element.getName().equals("textStyle"))
    {
      typeface = element.getAttributeValue("typeface");
      pointSize = Double.valueOf(element.getAttributeValue("pointSize"));
      if (element.getAttributeValue("textColor") != null)
      	setTextColor(Color.decode(element.getAttributeValue("textColor")));
      setJustification(element.getAttributeValue("justification"));
		  setVAlign(element.getAttributeValue("valign"));
		  styles = element.getAttributeValue("styles");
		  if (styles == null)
		  	styles = "none";
		  setOverflow(element.getAttributeValue("overflow"));
    }
    else
    {
      throw new Exception("Not a Text Style element");
    }
  }

	/**
	 * @return <code>true</code> if style includes bold
	 */
	public boolean isBold()
	{
		return styles.indexOf(STYLE_BOLD) != -1;
	}

	/**
	 * @return <code>true</code> if style includes italic
	 */
	public boolean isItalic()
	{
		return styles.indexOf(STYLE_ITALIC) != -1;
	}

	/**
	 * @return <code>true</code> if style includes both bold and italic
	 */
	public boolean isBoldItalic()
	{
		return isBold() && isItalic();
	}

	/**
	 * @return <code>true</code> if style includes underline
	 */
	public boolean isUnderline()
	{
		return styles.indexOf(STYLE_UNDERLINE) != -1;
	}

	public boolean isShrinkWrap()
	{
		return overflow == SHRINKWRAP;
	}

	/*public boolean isShrink()
	{
		return overflow == SHRINK;
	}

	public boolean isWrap()
	{
		return overflow == WRAP;
	}*/

	public boolean isWrapShrink()
	{
		return overflow == WRAPSHRINK;
	}

	public boolean isFit()
	{
		return overflow == FIT;
	}

  protected void setOverflow(String newOverflow)
  {
     if (newOverflow.equals("wrapshrink"))
      setWrapShrink();
     /*else if (newOverflow.equals("shrink"))
      setShrink();
     else if (newOverflow.equals("wrap"))
      setWrap();*/
     else if (newOverflow.equals("fit"))
       setFit();
     else
      setShrinkWrap();
  }

	public void setShrinkWrap()
	{
		overflow = SHRINKWRAP;
	}

	public void setWrapShrink()
	{
		overflow = WRAPSHRINK;
	}

	public void setFit()
	{
		overflow = FIT;		
	}

	/*public void setShrink()
	{
		overflow = SHRINK;
	}

	public void setWrap()
	{
		overflow = WRAP;
	}*/
	
	public Double getPointSize()
	{
		return pointSize;
	}

	public Color getTextColor()
	{
		return textColor;
	}

	public String getTypeface()
	{
		return typeface;
	}

	public boolean isTopAligned()
	{
		return valign == VERT_TOP;
	}

	public boolean isMiddleAligned()
	{
		return valign == VERT_MIDDLE;
	}

	public boolean isBottomAligned()
	{
		return valign == VERT_BOTTOM;
	}

  protected void setVAlign(String newVAlign)
  {
    if ("middle".equals(newVAlign))
      setMiddleAligned();
    else if ("bottom".equals(newVAlign))
      setBottomAligned();
    else
      setTopAligned();
  }

	public void setTopAligned()
	{
		valign = VERT_TOP;
	}

	/**
	 * Sets the vertical alignment to "middle"
	 */
	public void setMiddleAligned()
	{
		valign = VERT_MIDDLE;
	}

	public void setBottomAligned()
	{
		valign = VERT_BOTTOM;
	}

	/**
	 * adds a specific text style, e.g. STYLE_BOLD, STYLE_ITALIC, etc.
	 * @param newStyle
	 */
	private void addStyle(String newStyle)
	{
		if (styles.indexOf(newStyle) == -1)
		{
			if (styles.equals(STYLE_NONE))
			{
				styles = newStyle;
			}
			else
			{
				styles = styles + "," + newStyle;
			}
		}
	}

	/**
	 * removes a specific text style, e.g. STYLE_UNDERLINE
	 * @param theStyle
	 */
	private void removeStyle(String theStyle)
	{
		boolean b = isBold() && !theStyle.equals(STYLE_BOLD);
		boolean i = isItalic() && !theStyle.equals(STYLE_ITALIC);
		boolean u = isUnderline() && !theStyle.equals(STYLE_UNDERLINE);
		styles = b ? STYLE_BOLD : STYLE_NONE;
		if (i) setItalic(true);
		if (u) setUnderline(true);
	}

	/**
	 * @param b - <code>true</code> to add bold, <code>false</code> to remove bold
	 */
	public void setBold(boolean b)
	{
		if (b) addStyle(STYLE_BOLD);
		else removeStyle(STYLE_BOLD);
	}

	/**
	 * @param b - <code>true</code> to add italic, <code>false</code> to remove italic
	 */
	public void setItalic(boolean b)
	{
		if (b) addStyle(STYLE_ITALIC);
		else removeStyle(STYLE_ITALIC);
	}

	/**
	 * @param b - <code>true</code> to add underline, <code>false</code> to remove underline
	 */
	public void setUnderline(boolean b)
	{
		if (b) addStyle(STYLE_UNDERLINE);
		else removeStyle(STYLE_UNDERLINE);
	}

  protected void setJustification(String newJustification)
  {
     if (newJustification.equals("right"))
      setRightJustified();
     else if (newJustification.equals("center"))
      setCenterJustified();
     else if (newJustification.equals("full"))
      setFullJustified();
    else
      setLeftJustified();
  }

	public void setLeftJustified()
	{
		justification = JUST_LEFT;
	}

	public boolean isLeftJustified()
	{
		return justification == JUST_LEFT;
	}

	public void setRightJustified()
	{
		justification = JUST_RIGHT;
	}

	public boolean isRightJustified()
	{
		return justification == JUST_RIGHT;
	}

	public void setCenterJustified()
	{
		justification = JUST_CENTER;
	}

	public boolean isCenterJustified()
	{
		return justification == JUST_CENTER;
	}

	public void setFullJustified()
	{
		justification = JUST_FULL;
	}

	public boolean isFullJustified()
	{
		return justification == JUST_FULL;
	}

	/**
	 * @param d - the pointsize of the text
	 */
	public void setPointSize(Double d)
	{
		pointSize = d;
	}

	/**
	 * @param color - the color of the text
	 */
	public void setTextColor(Color color)
	{
		textColor = color;
	}

	/**
	 * @param string - the typeface name
	 */
	public void setTypeface(String string)
	{
		typeface = string;
	}

	/**
	 * Get a JDOM representation of the object suitable for
	 * inclusion in an XML file based on averysoft.xsd
	 * @return an Averysoft-based JDOM Element
	 */
	public Element getAverysoftElement()
	{
		Element element = new Element("textStyle");
    element.setNamespace(AveryProject.getAverysoftNamespace());

		element.setAttribute("typeface", getTypeface());
		element.setAttribute("pointSize", getPointSize().toString());
		element.setAttribute("textColor", formatColor0x(getTextColor()));

		final String JUSTIFICATION = "justification";
		switch (justification)
		{
			default:
			case JUST_LEFT:
				element.setAttribute(JUSTIFICATION, "left");
				break;
			case JUST_RIGHT:
				element.setAttribute(JUSTIFICATION, "right");
				break;
			case JUST_CENTER:
				element.setAttribute(JUSTIFICATION, "center");
				break;
			case JUST_FULL:
				element.setAttribute(JUSTIFICATION, "full");
				break;
		}

		final String VALIGN = "valign";
		switch (valign)
		{
			// optional attribute: default = "top"
			default:
			case VERT_TOP:
				break;
			case VERT_MIDDLE:
				element.setAttribute(VALIGN, "middle");
				break;
			case VERT_BOTTOM:
				element.setAttribute(VALIGN, "bottom");
				break;
		}

		element.setAttribute("styles", styles);

		final String OVERFLOW = "overflow";
		switch (overflow)
		{
			default:
			case SHRINKWRAP:
				element.setAttribute(OVERFLOW, "shrinkwrap");
				break;
			case WRAPSHRINK:
				element.setAttribute(OVERFLOW, "wrapshrink");
				break;
			case FIT:
				element.setAttribute(OVERFLOW, "fit");
				break;
			/*case WRAP:
				element.setAttribute(OVERFLOW, "wrap");
				break;
			case SHRINK:
				element.setAttribute(OVERFLOW, "shrink");
				break;*/
		}

		return element;
	}

	/**
	 * Get a JDOM representation of the object suitable for
	 * inclusion in an XML file based on averysoft.xsd
	 * @return an Averysoft-based JDOM Element
	 */
	public Element getAverysoftIBElement()
	{
		Element element = new Element("textStyle");
    element.setNamespace(AveryProject.getAverysoftNamespace());

		element.setAttribute("fontFamily", getTypeface());
		element.setAttribute("fontSize", getPointSize().toString());
		// sometimes ibright projects didn't define color hack this case
		Color textColor = getTextColor();
		if (textColor == null)
			textColor = Color.black;
		element.setAttribute("color", formatColor0x(textColor));

		final String JUSTIFICATION = "justification";
		switch (justification)
		{
			default:
			case JUST_LEFT:
				element.setAttribute(JUSTIFICATION, "left");
				break;
			case JUST_RIGHT:
				element.setAttribute(JUSTIFICATION, "right");
				break;
			case JUST_CENTER:
				element.setAttribute(JUSTIFICATION, "center");
				break;
			case JUST_FULL:
				element.setAttribute(JUSTIFICATION, "full");
				break;
		}

		final String VALIGN = "verticalAlignment";
		switch (valign)
		{
			// optional attribute: default = "top"
			default:
			case VERT_TOP:
				break;
			case VERT_MIDDLE:
				element.setAttribute(VALIGN, "middle");
				break;
			case VERT_BOTTOM:
				element.setAttribute(VALIGN, "bottom");
				break;
		}

		element.setAttribute("styles", styles);

		final String OVERFLOW = "overflow";
		switch (overflow)
		{
			default:
			case SHRINKWRAP:
				element.setAttribute(OVERFLOW, "shrinkwrap");
				break;
			case WRAPSHRINK:
				element.setAttribute(OVERFLOW, "wrapshrink");
				break;
			case FIT:
				element.setAttribute(OVERFLOW, "fit");
				break;
			/*case WRAP:
				element.setAttribute(OVERFLOW, "wrap");
				break;
			case SHRINK:
				element.setAttribute(OVERFLOW, "shrink");
				break;*/
		}

		return element;
	}
	/**
	 * produces a hex color string of the form 0xrrggbb
	 * @param c - the color to format
	 * @return formatted string
	 */
	public static String formatColor0x(Color c)
	{
		int r = c.getRed();
		int b = c.getBlue();
		int g = c.getGreen();
		return "0x"
			+ Integer.toHexString(r >> 4)	+ Integer.toHexString(r & 0xf)
			+ Integer.toHexString(g >> 4)	+ Integer.toHexString(g & 0xf)
			+ Integer.toHexString(b >> 4)	+ Integer.toHexString(b & 0xf);
	}
	
	/**
	 * produces an alpha hex color string of the form 0xrrggbbaa
	 * @param c - the color to format
	 * @return formatted string
	 */
/*	public static String formatAlphaColor0x(Color c)
	{
		int r = c.getRed();
		int b = c.getBlue();
		int g = c.getGreen();
		int a = c.getAlpha();
		return "0x"
			+ Integer.toHexString(r >> 4)	+ Integer.toHexString(r & 0xf)
			+ Integer.toHexString(g >> 4)	+ Integer.toHexString(g & 0xf)
			+ Integer.toHexString(b >> 4)	+ Integer.toHexString(b & 0xf)
		  + Integer.toHexString(a >> 4)	+ Integer.toHexString(a & 0xf);
	}
	
	public static Color decodeAlphaColor(String sColor)
	{
		int r = Integer.parseInt(sColor.substring(2, 4), 16);
		int g = Integer.parseInt(sColor.substring(4, 6), 16);
		int b = Integer.parseInt(sColor.substring(6, 8), 16);
		int a = Integer.parseInt(sColor.substring(8, 10), 16);
		
		return new Color(r, g, b, a);
	}*/
}