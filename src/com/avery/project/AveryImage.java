/**
 * Title: AveryImage
 * <p>
 * Description:
 * <p>
 * Copyright: Copyright (c)2000 Avery Dennison
 * <p>
 * Company: Avery Dennison
 * <p>
 * 
 * @author Bob Lee
 * @version 1.0
 */

package com.avery.project;

import javax.imageio.*;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Hashtable;
import java.util.Iterator;

import net.sf.javavp8decoder.imageio.WebPImageReader;
import net.sf.javavp8decoder.imageio.WebPImageReaderSpi;

import org.faceless.pdf2.PDF;
import org.faceless.pdf2.PDFCanvas;
import org.faceless.pdf2.PDFImage;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFParser;
import org.faceless.pdf2.PDFReader;
import org.faceless.pdf2.PagePainter;
import org.jdom.Element;

import com.avery.predesign.StyleSet;
import com.avery.project.RightRes.ImageFile;
import com.avery.utils.AveryUtils;
//import com.sun.image.codec.jpeg.JPEGCodec;
import com.twelvemonkeys.imageio.plugins.jpeg.JPEGColorSpace;
import com.twelvemonkeys.imageio.plugins.jpeg.JPEGImageReader;
import com.twelvemonkeys.imageio.plugins.tiff.TIFFImageReader;
import com.twelvemonkeys.imageio.plugins.tiff.TIFFImageReaderSpi;

public class AveryImage extends AveryPanelField implements Averysoftable
{
  // default constructor
  public AveryImage()
  {
  }

  // legacy constructor
  // the image cache is now passed in during the
  // addToPDF call
  public AveryImage(Hashtable pdfImageCache)
  {
    this.pdfImageCache = pdfImageCache;
  }

  protected AveryImage(Element element) throws Exception
  {
    pdfImageCache = new Hashtable();

    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace()) && element.getName().equals("image"))
    {
      setSourceAndGallery(element.getAttributeValue("source"), element.getAttributeValue("gallery"));
      readSuperAverysoftAttributes(element);

      stretch = "true".equals(element.getAttributeValue("stretch"));

      Element descriptionElement = element.getChild("description", AveryProject.getAverysoftNamespace());
      if (descriptionElement != null)
      {
        setDescriptionObject(Description.makeImageFieldDescription(descriptionElement));
      }
    }
    else if (!initFromAveryDtdElement(element))
    {
      throw new Exception("Not an Avery Image element");
    }
  }

  private boolean initFromAveryDtdElement(Element element) throws Exception
  {
    if (element.getName().equals("Avery.pf.image"))
    {
      setSourceAndGallery(element.getAttributeValue("src"), element.getAttributeValue("gallery"));
      readSuperAttributes(element.getParent());

      return true;
    }

    return false;
  }

  // types
  static final int TYPE_UNKNOWN = 0;
  static final int TYPE_JPG = 1;
  static final int TYPE_BMP = 2;
  static final int TYPE_WMF = 3;
  static final int TYPE_PNG = 4;
  static final int TYPE_GIF = 5;
  static final int TYPE_TIF = 6;
  static final int TYPE_PSD = 7;
  static final int TYPE_WEBP = 8;
  static final int TYPE_PDF = 9;
  //static final int TYPE_SVG = 10;

  static final int MIN_DPI = 72;
  static final double TWIPS_PER_PIXEL = 20;
  
  /**
   * IGNORE_PREFIX is a remnant of old AveryPrint code. Use
   * setVisible/getVisible instead for Averysoft comptibility.
   */
  public static final String IGNORE_PREFIX = "^";
  public static String INPUT_DOTAVERY_PATH = null;
  private static String ICCColorProfile = null;
  
  

  // properties
  protected String source = "";
  protected String gallery = "";
  
  protected int srcWidth = 0;
  protected int srcHeight = 0;

  // imageType is reliably set by setSource
  protected int imageType = TYPE_UNKNOWN;

  private boolean stretch = false;

  // run time data
  protected Hashtable pdfImageCache;

  private boolean hasOnlineGallery = false;  

  protected void setPdfImageCache(Hashtable cache)
  {
    pdfImageCache = cache;
  }

  /**
   * The matches() method returns <code>true</code> if the given field is an
   * AveryImage with the same source, gallery, and AveryPanelfield attributes.
   * 
   * @param field -
   *          the field to test against
   * @return <code>true</code> if the contents of the field match the contents
   *         of <code>this</code>, <code>false</code> otherwise
   */
  public boolean matches(AveryPanelField field)
  {
    if (field instanceof AveryImage)
    {
      AveryImage image = (AveryImage) field;
      if (getSource().equals(image.getSource()) && getGallery().equals(image.getGallery()))
      {
        return super.matches(field);
      }
    }
    return false;
  }

  /**
   * The differences() method compares this field with another AveryPanelField
   * and returns a BitSet describing how they differ. The bits of the BitSet are
   * defined in the AveryPanelField base class. If the field is an instance of a
   * different class, the APPLES_AND_ORANGES bit is set and the comparison goes
   * no further.
   * 
   * @param field -
   * @return BitSet describing the differences
   */
  public BitSet differences(AveryPanelField field)
  {
    BitSet flags = super.differences(field);

    if (flags.get(APPLES_AND_ORANGES))
    { // no sense in comparing apples and oranges
      return flags;
    }

    if (getSource().equals(((AveryImage) field).getSource()) == false)
    {
      flags.set(IMAGECONTENT_DIFFERS);
    }
    if (getGallery().equals(((AveryImage) field).getGallery()) == false)
    {
      flags.set(IMAGECONTENT_DIFFERS);
    }

    return flags;
  }

  /**
   * Creates a deep clone
   * 
   * @return deep clone
   */
  public AveryPanelField deepClone()
  {
    AveryImage im = new AveryImage();
    copyPrivateData(im);

    im.setSourceAndGallery(source, gallery);
    im.stretch = this.stretch;

    return im;
  }

  /**
   * This method modifies the field, if it exists, to match recent changes in
   * another field. It is used to update fields that have been made obsolete by
   * an updated Masterpanel (hence the name).
   * 
   * @param field -
   *          the field to pull changes from.
   * @param flags -
   *          describes what attributes to update
   * @return the number of modifications made
   */
  public int masterUberAlles(AveryPanelField field, BitSet flags)
  {
    int modified = super.masterUberAlles(field, flags);

    if (flags.get(IMAGECONTENT_DIFFERS))
    {
      AveryImage sourceField = (AveryImage) field;
      this.setSourceAndGallery(sourceField.getSource(), sourceField.getGallery());
      ++modified;
    }
    return modified;
  }

  // gets and sets
  public String getSource()
  {
    return source;
  }

  public void setSourceAndGallery(String newSource, String newGallery)
  {
    source = newSource;
    gallery = newGallery;
    if (gallery.startsWith("http://") || gallery.startsWith("https://") || gallery.startsWith("ftp://"))
    {
      hasOnlineGallery = true;
    }
    else
    {
      hasOnlineGallery = false;
    }
    discoverImageType();
  }

  public String getGallery()
  {
    return gallery;
  }

  public static void setICCColorProfile(String newProfile)
  {
    ICCColorProfile = newProfile;
  }

  public int getSrcWidth()
  {
    return srcWidth;
  }
  
  public void setSrcWidth(int w)
  {
    srcWidth = w;
  }

  public int getSrcHeight()
  {
    return srcHeight;
  }
  
  public void setSrcHeight(int h)
  {
  	srcHeight = h;
  }

  public String dump()
  {
    String str = "<P>AveryImage";
    str += super.dump();
    str += "<br>source = " + source;
    str += "<br>gallery = " + gallery;
    return str;
  }

  /**
   * If the currect gallery/HighRes/src file exists, no action is taken. No
   * action is taken if an image is hidden. Otherwise, newGallery is assigned
   * into the gallery attribute, and the test is repeated.
   * 
   * @param newGallery -
   *          a new gallery to use if the current one doesn't contain the image.
   * @return true if image file is located, false otherwise
   */
  boolean fixGallery(String newGallery)
  {
    if (getSource().startsWith(IGNORE_PREFIX))
    {
      // no action (AveryPrint 4) - ignore hidden image
      return true;
    }

    if (new File(getHighresFilespec()).exists())
    {
      return true;
    }

    gallery = newGallery;
    if (gallery.startsWith("http://") || gallery.startsWith("ftp://"))
    {
      hasOnlineGallery = true;
    }
    else
    {
      hasOnlineGallery = false;
    }

    return new File(getHighresFilespec()).exists();
  }

  // ////////////////////////////////////////////////////////////////////
  // implement draw() as declared in abstract superclass AveryPanelField
  public void draw(Graphics2D gr, double dScalar)
  {
    if (getSource().startsWith(IGNORE_PREFIX))
    {
      return; // Avery Print 4.0 ignore
    }
    if (!getVisible() || getOpacity().doubleValue() <= 0.0)
    {
      return; // Averysoft XML ignore
    }

    if (getMask() != null)
    {
      getMask().clipImageMask(gr, dScalar);
    }
    drawOutlineElement(gr, dScalar);
    switch (imageType)
    {
      case TYPE_JPG:
      case TYPE_PNG:
      case TYPE_TIF:
      case TYPE_PDF:
      case TYPE_BMP:
      case TYPE_WMF:
      case TYPE_WEBP:
        drawImage(gr, dScalar);
        break;

      case TYPE_GIF: // we can't render GIFs yet
      case TYPE_UNKNOWN:
      default:
        // draw a box with a red X in it
        int x = (int) (getPosition().getX() * dScalar);
        int y = (int) (getPosition().getY() * dScalar);
        int width = (int) (getWidth().doubleValue() * dScalar);
        int height = (int) (getHeight().doubleValue() * dScalar);

        gr.setColor(Color.white);
        gr.drawRect(x, y, width, height);
        gr.drawLine(x, y, x + width, y + height);
        gr.drawLine(x, y + height, x + width, y);
    }

    gr.setClip(null);
    if (getMask() != null && getMask().incrementIndex())
    {
      draw(gr, dScalar);
    }
  }

  // /////////////////////////////////////////////////////////////////////
  // draw() method for bitmapped images (JPEG, BMP etc.)
  protected void drawImage(Graphics2D gr, double dScalar)
  {
    if (source.length() < 1)
    {
      return; // nothing to draw
    }

    int x = (int) ((getPosition().getX() * dScalar) + 0.5);
    int y = (int) ((getPosition().getY() * dScalar) + 0.5);
    int width = (int) ((getWidth().doubleValue() * dScalar) + 0.5);
    int height = (int) ((getHeight().doubleValue() * dScalar) + 0.5);

    Image scaledImage = getScaledImage(width, height);

    if (scaledImage == null)
    {
      return;
    }

    // dimensions may be different due to aspect correction
    int scaledWidth = scaledImage.getWidth(null);
    int scaledHeight = scaledImage.getHeight(null);

    // reposition in center of changed dimension
    if (scaledWidth != width)
    {
      x += (width - scaledWidth) / 2;
    }
    else if (scaledHeight != height)
    {
      y += (height - scaledHeight) / 2;
    }

    // potentially rotate image then render it
    RotateandRender(gr, dScalar, scaledImage, x, y, scaledWidth, scaledHeight);

  } // end drawImage()

  // currently unused...
  protected void drawImageFull(Graphics2D gr, double dScalar)
  {
    if (source.length() < 1)
    {
      return; // nothing to draw
    }
    
    // get highres source + dimensions, always entry 0 in the list
    ArrayList allresList = new RightRes().getAllresList(this);
    ImageFile highresImageFile = (ImageFile)allresList.get(0);

    // dimensions may be different due to aspect correction
    int scaledWidth = (int)(highresImageFile.getWidth()); //scaledImage.getWidth(null);
    int scaledHeight = (int)(highresImageFile.getHeight()); //scaledImage.getHeight(null);

    int x = (int) ((getPosition().getX() * dScalar) + 0.5);
    int y = (int) ((getPosition().getY() * dScalar) + 0.5);
    int width = (int) ((getWidth().doubleValue() * dScalar) + 0.5);
    int height = (int) ((getHeight().doubleValue() * dScalar) + 0.5);
    
    // recalc target dimensions to match aspect of highresImageFile
    double imageAspect = highresImageFile.getAspect();
    double targetAspect = (double)width / (double)height;
    
    // reduce one dimension of target space to make it aspect correct
    if (targetAspect > imageAspect)
    {
      scaledWidth = width;
    	scaledHeight = (int)(((double)height) * (targetAspect / imageAspect));
    }
    else if (targetAspect < imageAspect)
    {
      scaledWidth = (int)(((double)width) * (imageAspect / targetAspect));
      scaledHeight = height;
    }

    Image scaledImage = getScaledImage(scaledWidth, scaledHeight); //getScaledImage(width, height);

    if (scaledImage == null)
    {
      return;
    }
    
    //scaledImage = getScaledImage(scaledWidth, scaledHeight);

    // reposition in center of changed dimension
    if (scaledWidth != width)
    {
      x += (width - scaledWidth) / 2;
    }
    else if (scaledHeight != height)
    {
      y += (height - scaledHeight) / 2;
    }

    // potentially rotate image then render it
    RotateandRender(gr, dScalar, scaledImage, x, y, scaledWidth, scaledHeight);

  } // end drawImage()

  protected void drawImageUnscaled(Graphics2D gr, double dScalar)
  {
    if (source.length() < 1)
    {
      return; // nothing to draw
    }
    
    // get highres source + dimensions, always entry 0 in the list
    ArrayList allresList = new RightRes().getAllresList(this);
    ImageFile highresImageFile = (ImageFile)allresList.get(0);

    // dimensions may be different due to aspect correction
    int scaledWidth = (int)(highresImageFile.getWidth());
    int scaledHeight = (int)(highresImageFile.getHeight());

    int x = (int) ((getPosition().getX() * dScalar) + 0.5);
    int y = (int) ((getPosition().getY() * dScalar) + 0.5);
    int width = (int) ((getWidth().doubleValue() * dScalar) + 0.5);
    int height = (int) ((getHeight().doubleValue() * dScalar) + 0.5);
    
    Image scaledImage = getScaledImage(width, height);

    if (scaledImage == null)
    {
      return;
    }
    
    // reposition in center of changed dimension
    if (scaledWidth != width)
    {
      x += (width - scaledWidth) / 2;
    }
    if (scaledHeight != height)
    {
      y += (height - scaledHeight) / 2;
    }

    // potentially rotate image then render it
    RotateandRender(gr, dScalar, scaledImage, x, y, scaledWidth, scaledHeight);
    
  } // end drawImage()

  /**
   * rotates fullImage if necessary then renders image to drawing surface
   * 
   * @param gr -
   *          Graphics2D
   * @param dScalar -
   *          scale factor to convert twips to pixels
   * @param fullImage -
   *          unrotated image
   * @param x -
   *          image left edge
   * @param y -
   *          image top edge
   * @param width -
   *          image width
   * @param height -
   *          image height
   */
  void RotateandRender(Graphics2D gr, double dScalar, Image fullImage, int x, int y, int width, int height)
  {
    if (gr == null || fullImage == null)
      return; // safety trap

    // store variables for rendering to surface in case of field rotation
    int targetx = x;
    int targety = y;
    int targetWidth = width;
    int targetHeight = height;
    // System.out.println("x, y= " + new Integer(targetx).toString() + "," + new
    // Integer(targety).toString());

    // field rotation
    double fieldRotation = getRotation().doubleValue() * Math.PI / 180.0;
    if (fieldRotation != 0.0)
    {
      // rotation involves rotating the graphic around the upper left hand
      // corner of the field. If the graphic is not square the corner of
      // the graphic is not at the same location as the field.

      // System.out.println("targetx, targety= " + new Float(targetx).toString()
      // + ", " + new Float(targety).toString());

      // calculate position of upper left hand corner of graphic relative to
      // upper left hand corner of field. the field corner is the origin.
      float deltax = targetx - (float) (getPosition().getX() * dScalar);
      float deltay = (float) (getPosition().getY() * dScalar) - targety;
      // System.out.println("deltax, deltay= " + new Float(deltax).toString() +
      // ", " + new Float(deltay).toString());
      // System.out.println("fieldRotation= " + new
      // Double(fieldRotation).toString());

      // rotate graphic corner around field corner
      float sintheta = (float) Math.sin(fieldRotation);
      float costheta = (float) Math.cos(fieldRotation);
      float xprime = deltax * costheta - deltay * sintheta;
      float yprime = deltax * sintheta + deltay * costheta;
      // System.out.println("xprime, yprime= " + new Float(xprime).toString() +
      // ", " + new Float(yprime).toString());

      // find the location of the rotated graphic corner relative to the origin
      xprime = xprime - deltax;
      yprime = yprime - deltay;
      // System.out.println("offset x, y= " + new Float(xprime).toString() + ",
      // " + new Float(yprime).toString());

      // make an image buffer the size of the graphic for measurements
      BufferedImage biDummy = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

      // find the rectangular coordinates of the rotated graphic assuming
      // that the origin is the upper left hand corner of the graphic
      AffineTransform at = new AffineTransform();
      at.rotate(-fieldRotation);
      AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
      Rectangle2D r2d = op.getBounds2D(biDummy);
      biDummy = null;
      targetWidth = (int) r2d.getWidth();
      targetHeight = (int) r2d.getHeight();
      // System.out.println(r2d.toString());

      // the image location is translated by:
      // the offset of the rotated graphic corner relative to the field corner
      // and ...
      targetx += xprime;
      targety -= yprime;
      // ... the offset of the rotated image rectangle upper left corner
      // relative
      // to the graphics upper left hand corner
      targetx += r2d.getMinX();
      targety += r2d.getMinY();

      // copy image to a buffered image
      BufferedImage bifullImage = new BufferedImage(fullImage.getWidth(null), fullImage.getHeight(null),
          BufferedImage.TYPE_INT_ARGB);
      Graphics2D g2D = bifullImage.createGraphics();
      g2D.drawImage(fullImage, null, null);
      

      g2D.dispose();
      
      // rotate buffered image using rotation filter
      ImageFilter filter = new RotationFilter(fieldRotation);
      ImageProducer producer = new FilteredImageSource(bifullImage.getSource(), filter);
      bifullImage = null;
      fullImage = null;
      // replace image with rotated image
      fullImage = Toolkit.getDefaultToolkit().createImage(producer);
    }

    // finally render image to target surface
    if (fullImage != null)
    {
      float opacity = getOpacity().floatValue();
    	Composite originalComposite = gr.getComposite();
      if (opacity >= 0f && opacity < 1f)
      {
      	gr.setComposite(makeComposite(opacity));
      }
      gr.drawImage(fullImage, targetx, targety, targetWidth, targetHeight, new Obs());
      if (opacity >= 0f && opacity < 1f)
      {
      	gr.setComposite(originalComposite);
      }
    }
  }
  
  /**
   * reads a JPG (JPEG)
   * 
   * @param sFilespec -
   *          the fully qualified path to the (local) JPG file
   * @returns a 96 dpi rendering of the JPG file, or null
   */
  BufferedImage readJPGImageFile(String sFilespec)
  {
    try
    {
      return readJPGImageFile(sFilespec, new FileInputStream(sFilespec));
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * reads a JPG (JPEG)
   * 
   * @param fis -
   *          the FileInputStream for the JPG file
   * @returns a 96 dpi rendering of the JPG file, or null
   */
  BufferedImage readJPGImageFile(String sFileSpec, InputStream fis)
  {
    try
    {    	
	  	Iterator readers = ImageIO.getImageReadersByFormatName("JPEG");
	  	ImageReader reader = (ImageReader)readers.next();
	  	ImageInputStream iis = ImageIO.createImageInputStream(fis);
	  	reader.setInput(iis);
	  	
	  	JPEGImageReader jir = (JPEGImageReader)reader;
	  	// BKN:  made changes to TwelveMonkeys JPEG plugin source to give public access to
	  	// all four of the methods invoked in the next line
	  	JPEGColorSpace jcsType = JPEGImageReader.getSourceCSType(jir.getJFIF(), jir.getAdobeDCT(), jir.getSOF());
	  	//System.out.println(jcsType);
		BufferedImage bufferedImage = reader.read(0);
		iis.close();
		
	  	if (jcsType.equals(JPEGColorSpace.YCbCr) || jcsType.equals(JPEGColorSpace.YCbCrA) ||
	  		jcsType.equals(JPEGColorSpace.RGB) || jcsType.equals(JPEGColorSpace.RGBA))
	  	{
	  	}
	  	else
	  	{
	  		// convert the image to RGB and overwrite it.
	  		File file = new File(sFileSpec);
	  		ColorSpace cs = bufferedImage.getColorModel().getColorSpace();
	  		int csType = cs.getType();
	      	BufferedImage dst = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(),BufferedImage.TYPE_INT_RGB);
	      	ColorSpace dcs = dst.getColorModel().getColorSpace();
	      	int dscType = dcs.getType();
	      	ColorConvertOp op = new ColorConvertOp(cs, dst.getColorModel().getColorSpace(), null);
	      	op.filter(bufferedImage,  dst);
	      	bufferedImage = null;
	      	bufferedImage = dst;
	      	fis.close();
	      	String jpg = file.getAbsolutePath().toLowerCase();
	      	//jpg = jpg.replace(".jpg", "_TMC.jpg");
	      	FileOutputStream fos = new FileOutputStream(jpg);
	      	ImageIO.write(dst, "jpg",  fos);
	      	fos.close();	
		}
	    return bufferedImage;	
	      //return JPEGCodec.createJPEGDecoder(fis).decodeAsBufferedImage();
    }
      
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * reads a PNG
   * 
   * @param sFilespec -
   *          the fully qualified path to the (local) PNG file
   * @returns a 96 dpi rendering of the PNG file, or null
   */
  BufferedImage readPNGImageFile(String sFilespec)
  {
    try
    {
      return readPNGImageFile(new FileInputStream(sFilespec));
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * reads a PNG
   * 
   * @param fis -
   *          the FileInputStream for the PNG file
   * @returns a 96 dpi rendering of the PNG file, or null
   */
  BufferedImage readPNGImageFile(InputStream fis)
  {
  	BufferedImage pngImage = null;
  	try
  	{
  		pngImage = ImageIO.read(fis);
  	}
  	catch (Exception e)
  	{
   	}
  	
  	return pngImage;
    //return (new LoadPNG()).getBufferedImage(fis);
  } // end readPNGImageFile

  /**
   * reads a TIF
   * 
   * @param sFilespec -
   *          the fully qualified path to the (local) TIF file
   * @returns a 96 dpi rendering of the TIF file, or null
   */
  /*BufferedImage readTIFImageFile(String sFilespec)
  {
    try
    {
      return readTIFImageFile(new FileInputStream(new File(sFilespec)));
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }
  }*/
  
  /**
   * reads a TIF
   * 
   * @param fis -
   *          the FileInputStream for the TIF file
   * @returns a 96 dpi rendering of the TIF file, or null
   * TIF requires JAI does not load using ImageIO no plugin?
   */
  BufferedImage readTIFImageFile(String sFileSpec)
  {
	BufferedImage tifImage = null;
	try
	{
      	TIFFImageReader tifir = new TIFFImageReader(new TIFFImageReaderSpi());
      	ImageInputStream iis = ImageIO.createImageInputStream(new File(sFileSpec));
      	tifir.setInput(iis);
      	int bits = tifir.getBitsPerSample();
      	if (bits < 1 || bits > 8)
      	{
      		throw new Exception();
      	}
      	
	  	tifImage = tifir.read(0);
      	iis.close();
	  	//fis.close();
	}
	catch (Exception e)
	{
		System.out.println("Unable to load TIFF");
	}
  	return tifImage;
    
  } // end readTIFImageFile
  
  BufferedImage readPDFImageFile(File file)
  {
		BufferedImage pdfImage = null;
		try
		{
			PDFReader reader = new PDFReader(file);
			PDF pdf = new PDF(reader);
			
			float dpi = 300;
			
			PDFParser parser = new PDFParser(pdf);
			PagePainter painter = parser.getPagePainter(0);
			pdfImage = painter.getImage(dpi);
			//ImageIO.write(pdfImage, "png", new File("c:/Users/Brad/temp.png"));
		}
		catch (Exception e)
		{
			System.out.println("Unable to load PDF");
		}
	  return pdfImage;
  }

  /**
   * reads a WEBP
   * 
   * @param sFilespec -
   *          the fully qualified path to the (local) WEBP file
   * @returns a 96 dpi rendering of the WEBP file, or null
   */
  BufferedImage readWEBPImageFile(String sFilespec)
  {
		BufferedImage webpImage = null;
		try
		{
	        WebPImageReader wpir = new WebPImageReader(new WebPImageReaderSpi());
	        ImageInputStream wpiis = ImageIO.createImageInputStream(new File(sFilespec));
	        wpir.setInput(wpiis);
	        webpImage = wpir.read(0);
	        wpiis.close();
		}
		catch (Exception e)
		{
			System.out.println("Unable to load WEBP");
		}
	  	return webpImage;
  }

  /**
   * reads a PNG
   * 
   * @param fis -
   *          the FileInputStream for the WEBP file
   * @returns a 96 dpi rendering of the WEBP file, or null
   */
  /*BufferedImage readWEBPImageFile(InputStream fis)
  {
  	BufferedImage wepbImage = null;
  	try
  	{
  		wepbImage = ImageIO.read(fis);
  	}
  	catch (Exception e)
  	{
   	}
  	
  	return wepbImage;
    //return (new LoadPNG()).getBufferedImage(fis);
  } // end readPNGImageFile*/


  // ////////////////////////////////////////////////////////////////////////
  // implement addtoPDF() as declared in abstract superclass AveryPanelField
  public void addToPDF(PDFPage pdfPage, Double dPanelHeight, Hashtable imageCache) throws Exception
  {
    addOutlineToPDF(pdfPage, dPanelHeight);
    int canvasStartX = 0;
    int canvasStartY = 0;
    double heightCalibration = 0;
    if (getPosition().x < 0)
    {
      canvasStartX = (int) (getPosition().x / 20f);
    }
    if (getPosition().y < 0)
    {
      canvasStartY = (int) (getPosition().y / 20f);
      heightCalibration = -getPosition().y;
    }

		int areaSize = getAreaSize(pdfPage, canvasStartX, canvasStartY);
    
    PDFCanvas contentContainer = null;
    float opacity = getOpacity().floatValue();
    if (opacity < 0f || opacity >= 1f)  	
    	contentContainer = new PDFCanvas(areaSize, areaSize);
    else
    	contentContainer = new PDFCanvas(areaSize, areaSize, opacity);

    if (getMask() != null)
    {
      getMask().clipMask(contentContainer, canvasStartX, canvasStartY);
    }
    setPdfImageCache(imageCache);

    if (getSource().startsWith(IGNORE_PREFIX))
    {
      return; // Avery Print 4.0 ignore
    }

    PDFImage pdfimage = null;

    switch (imageType)
    {
      case TYPE_JPG:
      case TYPE_PNG:
      case TYPE_TIF:
      case TYPE_WEBP:
      case TYPE_BMP:
      case TYPE_GIF:
     // case TYPE_PSD:
        pdfimage = makePdfImage(contentContainer);
        if (pdfimage == null)
        {
          return; // do nothing on failure
        }
        break;

      case TYPE_WMF:
        // not implemented
        // / playWmfIntoPDF(pdfPage, offset, dPageHeight);
        return;
      
      case TYPE_PDF:
        String address = getHighresFilespec();
      	PDF pdf = null;
  	    PDFCanvas cachedContentContainer = (PDFCanvas) pdfImageCache.get(address);
  	    
  	    if (cachedContentContainer == null)
  	    {
  	    	PDFReader reader = new PDFReader(new File(getHighresFilespec()));
  	    	pdf = new PDF(reader);
  	    	contentContainer = new PDFCanvas(pdf.getPage(0));
  	    	pdfImageCache.put(address, contentContainer);
  	    }
  	    else
  	    {
  	    	contentContainer = cachedContentContainer;
  	    }
  	    break;

      case TYPE_UNKNOWN:
      default:
        return;
    }

    // get everything into PostScript point space
    float fHeight = getHeight().floatValue() / 20.0f;
    float fWidth = getWidth().floatValue() / 20.0f;
    float fTop = (dPanelHeight.intValue() - getPosition().y) / 20.0f;
    float fLeft = getPosition().x / 20.0f;

    // calculate pixel-to-point scalar
    // float fImageDPI = pdfimage.getDPIX() < 1 ? 1.0f :
    // (float)pdfimage.getDPIX();
    // float fPixelScalar = 72.0f / fImageDPI;

    // fScalar is what the pdfPage will use for scaling
    // float fScalar = 1.0f;

    // fix aspect ratio
    float imageAspect;
    if (imageType != TYPE_PDF)
    	imageAspect = pdfimage.getWidth() / pdfimage.getHeight();  
    else
    	imageAspect = contentContainer.getWidth() / contentContainer.getHeight();  
    	
    float fieldAspect = getWidth().floatValue() / getHeight().floatValue();

    // reduce one dimension of target space to make it aspect correct
    if (maintainAspect())
    {
      if (fieldAspect > imageAspect)
      {
        float fFullWidth = fWidth;
        // adjust width
        fWidth *= imageAspect / fieldAspect;
        // adjust left edge to center image in target space
        fLeft += (fFullWidth - fWidth) / 2.0f;
        // use height to scale
        // fScalar = fHeight / (pdfimage.getHeight() * fPixelScalar);
      }
      else
      // if (fieldAspect < imageAspect) // or if they are equal
      {
        float fFullHeight = fHeight;
        // adjust height
        fHeight *= fieldAspect / imageAspect;
        // adjust top edge to center image in target space
        fTop -= (fFullHeight - fHeight) / 2.0f;
        // use width to scale
        // fScalar = fWidth / ((float)pdfimage.getWidth() * fPixelScalar);
      }
    }

    // field rotation
    double fieldRotation = getRotation().doubleValue();

    if (imageType != TYPE_PDF)
    {
	    // to rotate or not to rotate that is the question
	    if (fieldRotation != 0.0)
	    {
	      // save old graphics transform/color state
	      contentContainer.save();
	      // rotate the field
	      transformPDFField(contentContainer, new Double(dPanelHeight.doubleValue() + heightCalibration), fLeft - canvasStartX,
	          fTop - fHeight - canvasStartY, fieldRotation);
	      // all calculations are complete - add the image to the page in the PDF
	      // the lower left corner is at 0,0 because of the transform
	      contentContainer.drawImage(pdfimage, 0f, 0f, fWidth, fHeight);
	      // restore graphics state
	      contentContainer.restore();
	    }
	    else
	    {
	      // all calculations are complete - add the image to the page in the PDF
	    	contentContainer.drawImage(pdfimage, fLeft - canvasStartX, fTop - fHeight - canvasStartY,
	    			fLeft + fWidth - canvasStartX, fTop - canvasStartY);
	    }
	    pdfPage.drawCanvas(contentContainer, canvasStartX, canvasStartY, areaSize + canvasStartX, areaSize + canvasStartY);
    }
    else
    {
	    if (fieldRotation != 0.0)
	    {
	    }
	    else
	    {
	    	//contentContainer.transform(fWidth / contentContainer.getWidth(),0,0,fHeight / contentContainer.getHeight(),0,0);
	    	//contentContainer.transform(0.1,0,0,0.1,0,0);
	    }
	    pdfPage.drawCanvas(contentContainer, canvasStartX, fTop - canvasStartY, fWidth + canvasStartX, fTop - canvasStartY - fHeight);
    }
    
    ///pdfPage.drawImage(pdfimage, fLeft, fTop - fHeight, fLeft + fWidth, fTop);
    
    // not for dpo6 production only brand and print!
    /*if (AveryImage.ICCColorProfile != null)
    {
	    // attach a color profile to an image...
	    File file = new File(AveryImage.ICCColorProfile);
	    FileInputStream profileStream = new FileInputStream(file);
	    ICC_Profile profile = ICC_Profile.getInstance(profileStream);
	    ICC_ColorSpace colorSpace = new ICC_ColorSpace(profile);
	    pdfimage.setColorSpace(colorSpace);
	    profileStream.close();
    }*/
    
    
    if (getMask() != null && getMask().incrementIndex())
    {
      addToPDF(pdfPage, dPanelHeight, imageCache);
    }
    // for debugging, show outline of field
    // addOutlineToPDF(pdfdoc, offset, dPageHeight);
  } // end addToPDF()

  protected PDFImage makePdfImage(PDFCanvas pdfCanvas) throws Exception
  {
    PDFImage pdfimage = null;

    if (getSource().startsWith(IGNORE_PREFIX))
    {
      // no action (AveryPrint 4) - ignore this image
      return null;
    }

    if (!hasOnlineGallery && new File(getHighresFilespec()).exists() == false)
    {
      System.err.println(AveryUtils.timeStamp() + "AveryImage: What happened to " + getHighresFilespec()
          + "?  File not found!");
      return null;
    }

    if (hasOnlineGallery)
    {
      String address = getHighresFilespec();
      URL url = new URL(address);
      switch (imageType)
      {
        case TYPE_JPG:
        case TYPE_PNG:
        case TYPE_TIF:
        case TYPE_WEBP:
        case TYPE_BMP:
        case TYPE_GIF:
       // case TYPE_PSD:

	    pdfimage = (PDFImage) pdfImageCache.get(address);
	    if (pdfimage == null)
	    {
	    	try
	        {
	          if (imageType == TYPE_WEBP)
	          {
	        	  // make a PNG stream out of the WEBP image
	              WebPImageReader wpir = new WebPImageReader(new WebPImageReaderSpi());
	              ImageInputStream wpiis = ImageIO.createImageInputStream(new File(getHighresFilespec()));
	              wpir.setInput(wpiis);
	              BufferedImage bufferedImage = wpir.read(0);
	              wpiis.close();
	              ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
	              ImageIO.write(bufferedImage, "png", baos);
	              byte b[] = baos.toByteArray();
	              baos.close();
	              ByteArrayInputStream bais = new ByteArrayInputStream(b);
	              pdfimage = new PDFImage(bais); 
	              bais.close(); 
	          }
	          else
	          {
	        	  pdfimage = new PDFImage(url.openConnection().getInputStream());
	          }
        	  pdfImageCache.put(address, pdfimage);
	        }
	        catch (Exception e)
	        {
	          // e.printStackTrace();
	          System.err.println("AveryImage.makePdfImage failed on " + this.getHighresFilespec());
	          return null;
	        }
	      }
	      break;

        case TYPE_UNKNOWN:
        default:
          return null;
      }
    }
    else
    {
      File file;
      switch (imageType)
      {
        case TYPE_JPG:
        case TYPE_BMP:
          file = new RightRes().getPrintableJpegFile(this);
          pdfimage = (PDFImage) pdfImageCache.get(file);
          if (pdfimage == null)
          {
            pdfimage = new PDFImage(new FileInputStream(file.getAbsolutePath()));
            pdfImageCache.put(file, pdfimage);
          }
          break;

        case TYPE_PNG:
        case TYPE_TIF:
        case TYPE_WEBP:
        case TYPE_GIF:
        //case TYPE_PSD:
          file = new File(getHighresFilespec());
          pdfimage = (PDFImage) pdfImageCache.get(file);
          if (pdfimage == null)
          {
            try
            {
  	          if (imageType == TYPE_WEBP)
  	          {
  	        	  // make a PNG stream out of the WEBP image
  	              WebPImageReader wpir = new WebPImageReader(new WebPImageReaderSpi());
  	              ImageInputStream wpiis = ImageIO.createImageInputStream(file);
  	              wpir.setInput(wpiis);
  	              BufferedImage bufferedImage = wpir.read(0);
  	              wpiis.close();
  	              ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
  	              ImageIO.write(bufferedImage, "png", baos);
  	              byte b[] = baos.toByteArray();
  	              baos.close();
  	              ByteArrayInputStream bais = new ByteArrayInputStream(b);
  	              pdfimage = new PDFImage(bais); 
  	              bais.close(); 
  	          }
  	          else
  	          {
  	        	  pdfimage = new PDFImage(new FileInputStream(file.getAbsolutePath()));
  	          }
              pdfImageCache.put(file, pdfimage);
            }
            catch (Exception e)
            {
              // e.printStackTrace();
              System.err.println("AveryImage.makePdfImage failed on " + this.getHighresFilespec());
              return null;
            }
          }
          break;

        case TYPE_UNKNOWN:
        default:
          return null;
      }
    }
    return pdfimage;
  }

  // //////////////////////////////////////
  // helpers
  static class Obs implements ImageObserver
  {
    public Obs() // Orange Blossom Special?
    {
    }

    // our Obs imageUpdate returns "true" until image is fully loaded (ALLBITS)
    public boolean imageUpdate(Image img, int flags, int x, int y, int width, int height)
    {
      return ((flags & ALLBITS) == 0) && ((flags & ABORT) == 0);
    }
  }

  static Obs getObs()
  {
    return new Obs();
  }

  // ////////////////////////////////////////////////
  // discover imageType field according to extension of source filename string
  protected void discoverImageType()
  {
    imageType = TYPE_UNKNOWN;
    String str = new String(getSource()).toLowerCase();

    if (str.endsWith(".jpg") || str.endsWith(".jpeg"))
    {
      imageType = TYPE_JPG;
    }
    else if (str.endsWith(".png"))
    {
      imageType = TYPE_PNG;
    }
    else if (str.endsWith(".tif") || str.endsWith(".tiff"))
    {
      imageType = TYPE_TIF;
    }
    else if (str.endsWith(".webp"))
    {
      imageType = TYPE_WEBP;
    }
    else if (str.endsWith(".bmp"))
    {
      imageType = TYPE_BMP;
    }
    else if (str.endsWith(".wmf"))
    {
      imageType = TYPE_WMF;
    }
    else if (str.endsWith(".gif"))
    {
      imageType = TYPE_GIF;
    }
    else if (str.endsWith(".psd"))
    {
      imageType = TYPE_PSD;
    }
    else if (str.endsWith(".pdf") || str.endsWith(".ai"))
    {
      imageType = TYPE_PDF;
    }
    /*else if (str.endsWith(".svg"))
    {
      imageType = TYPE_SVG;
    }*/
  }

  /**
   * This simply returns a constructed string. It doesn't guarantee that a file
   * exists.
   * 
   * @return a String containing a filespec.
   */
  protected String getHighresFilespec()
  {
    String lcGallery = gallery.toLowerCase();
    if (lcGallery.endsWith("highres") || lcGallery.endsWith("user"))
    {
      return gallery + "/" + source;
    }
    else if (lcGallery.endsWith("highres/") || lcGallery.endsWith("user/"))
    {
      return gallery + source;
    }
    else
    {
      return gallery + "/HighRes/" + source;
    }
  }

  /**
   * Overridden from the base class
   * 
   * @return fully qualified filename, or <code>null</code>
   */
  public String getAssociatedFilename()
  {
    if (getGallery() != null)
    {
      return getHighresFilespec();
    }

    return getSource();
  }

  /**
   * Determine whether this object may be transparent
   * 
   * @return <code>true</code> if the image type supports transparency
   */
  boolean mayBeTransparent()
  {
    if (imageType == TYPE_UNKNOWN)
    {
      discoverImageType();
    }
    switch (imageType)
    {
      case TYPE_WMF:
      case TYPE_PNG:
      case TYPE_TIF:
      case TYPE_WEBP:
      case TYPE_PSD:
      case TYPE_PDF:
      //case TYPE_SVG:
        return true;
      default:
        return false;
    }
  }

  /**
   * Determine whether this object should maintain its aspect ratio when it is
   * resized.
   * 
   * @return <code>true</code> always for the AveryImage class
   */
  boolean maintainAspect()
  {
    return (stretch == false);
  }

  /**
   * The AveryImage is aware of the source filetype.
   * 
   * @return TYPE_JPG, TYPE_BMP, TYPE_WMF, TYPE_PNG or TYPE_UNKNOWN
   */
  public int getImageType()
  {
    return imageType;
  }

  public void setImageType(int newImageType)
  {
    imageType = newImageType;
  }

  /**
   * Creates an AveryImage (or AveryBackground), and writes the appropriate
   * printer-res image file to a gallery at <code>location</code> on disk.
   * 
   * @param prefix -
   *          used to generate a unique filename for the image
   * @param location -
   *          a directory with a HighRes directory under it
   * @param images -
   *          tracks images already processed
   * @return a new AveryImage object, or null if IGNORE_PREFIX is set
   * @throws IOException
   *           if the image copy operation failed
   */
  AveryImage getDotAveryImageField(String prefix, File location, Hashtable images) throws IOException
  {
    AveryImage imageField;
    
    if (this instanceof AveryBackground)
    {
      imageField = new AveryBackground(pdfImageCache);
      ((AveryBackground) imageField).setBackgroundcolor(((AveryBackground) this).getBackgroundcolor());
    }
    else
    // we only support two types of image fields
    {
      imageField = new AveryImage(pdfImageCache);
    }

    // copy base class attributes
    super.copyAttributesForDotAveryBundle(imageField);
    imageField.stretch = this.stretch;
    imageField.setVisible(this.getVisible());
    imageField.setPrint(this.getPrint());

    // generate a unique filename
    String newFileName = AveryUtils.generateFileName(location.getAbsolutePath() + "/HighRes", prefix);

    // figure out which source file to use (try not to exceed printer res)
    File rightFile;
    switch (getImageType())
    {
      case TYPE_JPG:
      case TYPE_BMP:
        rightFile = new RightRes().getPrintableJpegFile(this);
        newFileName += ".jpg";
        break;

      case TYPE_PNG:
        rightFile = new File(this.getHighresFilespec());
        newFileName += ".png";
        break;

      case TYPE_TIF:
        rightFile = new File(this.getHighresFilespec());
        newFileName += ".tif";
        break;

      case TYPE_WEBP:
	      rightFile = new File(this.getHighresFilespec());
	      newFileName += ".webp";
	      break;

     case TYPE_WMF:
        rightFile = new File(this.getHighresFilespec());
        newFileName += ".wmf";
        break;

      case TYPE_GIF:
        rightFile = new File(this.getHighresFilespec());
        newFileName += ".gif";
        break;

      case TYPE_PSD:
      	rightFile = new File(this.getHighresFilespec());
      	newFileName += ".psd";
      	break;

      case TYPE_PDF:
			  rightFile = new File(this.getHighresFilespec());
			  if (this.getHighresFilespec().endsWith(".ai"))
			  	newFileName += ".ai";
			  else
			  	newFileName += ".pdf";			
			  break;
      
      /*case TYPE_SVG:
	      rightFile = new File(this.getHighresFilespec());
	      newFileName += ".svg";
	      break;*/
          

      default:
        rightFile = new File(this.getHighresFilespec());
        break;
    }

    if (rightFile == null)
    {
      return null; // well, we tried...
    }

    File newFile;

    if (images.containsKey(rightFile))
    {
      // copy has already been created
      newFile = (File) images.get(rightFile);
    }
    else
    // new to us
    {
      newFile = new File(newFileName);

      // copy rightFile to the uniquely named newFile
      //System.out.println(rightFile.getAbsolutePath());
      String fullPath = rightFile.getAbsolutePath();
      
      if (!rightFile.exists() || rightFile.isDirectory())
      {
      	String path = newFile.getParent();
      	if (INPUT_DOTAVERY_PATH != null)
      		path = INPUT_DOTAVERY_PATH;
      	String name = rightFile.getName();
      	if (!rightFile.isDirectory())
      		rightFile = new File(path + "\\" + name);
      	else
      	{
      		rightFile = new File("");
      		newFile = new File("");
      	}
        //System.out.println(rightFile.getAbsolutePath());
      }
      
      if (rightFile.exists())
      {
	      FileInputStream inStream = new FileInputStream(rightFile);
	      FileOutputStream outStream = new FileOutputStream(newFile);
	
	      byte contents[] = new byte[inStream.available()];
	      inStream.read(contents);
	      outStream.write(contents);
	      inStream.close();
	      outStream.close();
	
	      // stash so that we don't end up processing the same image multiple times
	      images.put(rightFile, newFile);
      }
    }

    // .Avery bundles are saved without any gallery path information
    imageField.setSourceAndGallery(newFile.getName(), "");
    return imageField;
  }

  // caching for quick repeated access in full page preview
  private ArrayList cacheList = null;
  private Image cacheImage = null;
  private int cacheWidth = 0;
  private int cacheHeight = 0;

  public void clearCachedImage()
  {
  	cacheImage = null;
  }
  
  /**
   * Searches for an image in the cache
   * 
   * @param width -
   * @param height -
   * @return an Image, or <code>null</code>
   */
  private Image getCachedImage(int width, int height)
  {
    if (cacheImage != null)
    {
      if (cacheWidth == width && cacheHeight == height)
      {
        return cacheImage;
      }
    }

    if (cacheList != null)
    {
      String filespec = getHighresFilespec();
      Iterator iterator = cacheList.iterator();
      while (iterator.hasNext())
      {
        AveryImage aImage = (AveryImage) iterator.next();
        if (aImage.getHighresFilespec().equals(filespec) && aImage.cacheWidth == width && aImage.cacheHeight == height)
        {
          return aImage.cacheImage;
        }
      }
    }

    // no cached image found
    return null;
  }

  /**
   * Retrieves a scaled copy of the image
   * 
   * @param width -
   * @param height -
   * @return image sized appropriately
   */
  protected Image getScaledImage(int width, int height)
  {
    Image scaledImage = getCachedImage(width, height);

    if (scaledImage == null)
    {
      scaledImage = new RightRes().getImage(this, width, height);

      if (scaledImage != null)
      {
        cacheImage = scaledImage;
        cacheWidth = width;
        cacheHeight = height;
        if (cacheList != null)
        {
          cacheList.add(this);
        }
      }
    }
    return scaledImage;
  }

  void setCacheList(ArrayList list)
  {
    cacheList = list;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAveryElement()
  {
    Element element = super.getParentAveryElement();
    Element content = new Element("Avery.pf.image");
    content.setAttribute("src", getSource());
    content.setAttribute("gallery", getGallery());

    element.addContent(content);
    return element;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("image");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    makeAverysoftElement(element);

    return element;
  }

	public Element getAverysoftIBElement()
	{
    Element element = new Element("image");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    makeAverysoftIBElement(element);

    return element;
 	}

  public void makeAverysoftElement(Element element)
  {
    element.setAttribute("source", getSource());
    element.setAttribute("gallery", getGallery());
    if (stretch)
    {
      element.setAttribute("stretch", "true");
    }

    applyParentAverysoftAttributes(element);

    if (getDescriptionObject() == null)
    {
      if (this instanceof AveryBackground)
        setDescriptionObject(Description.makeBackgroundFieldDescription(getID()));
      else
        setDescriptionObject(Description.makeImageFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
	  position.setAttribute("x", Double.toString(getPosition().getX()));
	  position.setAttribute("y", Double.toString(getPosition().getY()));
    element.addContent(position);
  }

  public void makeAverysoftIBElement(Element element)
  {
    element.setAttribute("source", getSource());
    //element.setAttribute("gallery", getGallery());
    if (stretch)
    {
      element.setAttribute("stretch", "true");
    }

    applyParentAverysoftIBAttributes(element);

/*    if (getDescriptionObject() == null)
    {
      if (this instanceof AveryBackground)
        setDescriptionObject(Description.makeBackgroundFieldDescription(getID()));
      else
        setDescriptionObject(Description.makeImageFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());*/

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
		position.setAttribute("x", AveryProject.twipsToMM(getPosition().getX()));
		position.setAttribute("y", AveryProject.twipsToMM(getPosition().getY()));
    element.addContent(position);
  }

  protected void addDefaultDescriptionObject()
  {
    setDescriptionObject(Description.makeImageFieldDescription());
  }

  void replaceStyle(StyleSet original, StyleSet replacement)
  {
    if (original.graphic01.equals(source))
    {
      source = replacement.graphic01;
    }
    else if (original.graphic02.equals(source))
    {
      source = replacement.graphic02;
    }
    else if (original.graphic03.equals(source))
    {
      source = replacement.graphic03;
    }
    else if (original.graphic04.equals(source))
    {
      source = replacement.graphic04;
    }
    else if (original.graphic05.equals(source))
    {
      source = replacement.graphic05;
    }
    else if (original.graphic06.equals(source))
    {
      source = replacement.graphic06;
    }
    else if (original.graphic07.equals(source))
    {
      source = replacement.graphic07;
    }
    else if (original.graphic08.equals(source))
    {
      source = replacement.graphic08;
    }
    else if (original.graphic09.equals(source))
    {
      source = replacement.graphic09;
    }
    else if (original.graphic10.equals(source))
    {
      source = replacement.graphic10;
    }
    else if (original.graphic11.equals(source))
    {
      source = replacement.graphic11;
    }
    else if (original.graphic12.equals(source))
    {
      source = replacement.graphic12;
    }

    discoverImageType(); // in case it changed
  }

  boolean matchesStyle(StyleSet ss)
  {
    if (ss.graphic01.equals(source))
    {
      return true;
    }
    if (ss.graphic02.equals(source))
    {
      return true;
    }
    if (ss.graphic03.equals(source))
    {
      return true;
    }
    if (ss.graphic04.equals(source))
    {
      return true;
    }
    if (ss.graphic05.equals(source))
    {
      return true;
    }
    if (ss.graphic06.equals(source))
    {
      return true;
    }
    if (ss.graphic07.equals(source))
    {
      return true;
    }
    if (ss.graphic08.equals(source))
    {
      return true;
    }
    if (ss.graphic09.equals(source))
    {
      return true;
    }
    if (ss.graphic10.equals(source))
    {
      return true;
    }
    if (ss.graphic11.equals(source))
    {
      return true;
    }
    if (ss.graphic12.equals(source))
    {
      return true;
    }
    return false;
  }

  public boolean hasOnlineGallery()
  {
    return hasOnlineGallery;
  }
  
  public boolean isDPITooSmall()
  {
  	if (srcWidth <= 0 || srcHeight <= 0)
  		return false;
  	if (getWidth().intValue() <= 0 || getHeight().intValue() <= 0)
  		return false;
  	
  	int dpiWidth  = MIN_DPI * (int)((double)srcWidth / (getWidth().doubleValue() / TWIPS_PER_PIXEL));
  	int dpiHeight = MIN_DPI * (int)((double)srcHeight / (getHeight().doubleValue() / TWIPS_PER_PIXEL));
  	
  	System.out.println("src, true width=" + srcWidth + "," + getWidth().intValue());
  	System.out.println("src, true width=" + srcHeight + "," + getHeight().intValue());
  	
  	if (dpiWidth < MIN_DPI || dpiHeight < MIN_DPI)
  		return true;
  	
  	return false;
  }
  

}
