package com.avery.project;

import java.awt.*;
import java.awt.image.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.avery.utils.*;
import org.faceless.pdf2.*;
import org.jdom.Element;

/**
 * Title: TextPath
 * <p>
 * Description: TextPath describes a textfield that is known to contain just one
 * line of text, and is rendered on a path shape
 * <p>
 * Copyright: Copyright (c)2002 Avery Dennison
 * <p>
 * Company: Avery Dennison
 * <p>
 * 
 * @author Brad Nelson
 * @version 1.0
 */

public class TextPath extends AveryTextfield implements Averysoftable
{
  public TextPath()
  {
  }

  protected TextPath(Element element) throws Exception
  {
    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace()) && element.getName().equals("textPath"))
    {
      readTextfieldAverysoftAttributes(element);

      setStartPosition(AveryProject.buildPoint(element.getAttributeValue("startPosition")));
      rotatesense = element.getAttributeValue("rotateSense");
      shape = element.getAttributeValue("shape");

      Element descriptionElement = element.getChild("description", AveryProject.getAverysoftNamespace());
      if (descriptionElement != null)
      {
        setDescriptionObject(Description.makeTextFieldDescription(descriptionElement));
      }

      Element textElement = element.getChild("text", AveryProject.getAverysoftNamespace());
      if (textElement != null)
      {
        setContent(textElement.getText());
      }
    }
    else if (!initFromAveryDtdElement(element))
    {
      throw new Exception("Not a TextPath element");
    }
  }

  private boolean initFromAveryDtdElement(Element element) throws Exception
  {
    if (element.getName().equals("Avery.pf.textpath"))
    {
      setTypeface(element.getAttributeValue("typeface"));
      setPointsize(Double.valueOf(element.getAttributeValue("pointsize")));
      setTextcolor(element.getAttributeValue("textcolor"));
      setJustification(element.getAttributeValue("justification"));
      setStyles(element.getAttributeValue("styles"));

      setContent(element.getText());
      setShape(element.getAttributeValue("shape"));
      setStartPosition(AveryProject.buildPoint(element.getAttributeValue("startposition")));
      setRotateSense(element.getAttributeValue("rotatesense"));

      readSuperAttributes(element.getParent());

      return true;
    }

    return false;
  }

  private String shape = "ellipse";
  private Point startposition = new Point(0, 0);
  private String rotatesense = "cw";
  private String content = "";

  // this is the point size that is calculated by the preview
  // the (int) value of this variable is the actual point size used for the
  // preview and the pdf
  static double ddScalar = 0.0;

  /**
   * The matches() method returns <code>true</code> if the given field is a
   * TextPath with the same content and AveryTextfield attributes.
   * 
   * @param field -
   *          the field to test against
   * @return <code>true</code> if the contents of the field match the contents
   *         of <code>this</code>, <code>false</code> otherwise
   */
  public boolean matches(AveryPanelField field)
  {
    if (field instanceof TextPath)
    {
      TextPath textpath = (TextPath) field;
      if (getContent().equals(textpath.getContent()))
      {
        return super.matches(field);
      }
    }
    return false;
  }

  /**
   * The differences() method compares this field with another AveryPanelField
   * and returns a BitSet describing how they differ. The bits of the BitSet are
   * defined in the AveryPanelField base class. If the field is an instance of a
   * different class, the APPLES_AND_ORANGES bit is set and the comparison goes
   * no further.
   * 
   * @param field -
   *          the field to test against
   * @return BitSet describing the difference
   */
  public BitSet differences(AveryPanelField field)
  {
    BitSet flags = super.differences(field);

    if (flags.get(APPLES_AND_ORANGES))
    { // no sense in comparing apples and oranges
      return flags;
    }

    if (getContent().equals(((TextPath) field).getContent()) == false)
    {
      flags.set(TEXTCONTENT_DIFFERS);
    }

    return flags;
  }

  /**
   * Creates a deep clone
   * 
   * @return deep clone
   */
  public AveryPanelField deepClone()
  {
    TextPath tp = new TextPath();
    copyPrivateData(tp);

    tp.setTypeface(getTypeface());
    tp.setPointsize(getPointsize());
    tp.setTextcolor(getTextcolor());
    tp.setStyles(getStyles());
    tp.setJustification(getJustification());
    tp.setVerticalAlignment(getVerticalAlignment());
    tp.setOverflow(getOverflow());
    tp.setTextStyle(getTextStyle());

    tp.setShape(shape);
    tp.setStartPosition(startposition);
    tp.setRotateSense(rotatesense);
    tp.setContent(getContent());

    return (AveryPanelField) tp;
  }

  /**
   * This method modifies the field, if it exists, to match recent changes in
   * another field. It is used to update fields that have been made obsolete by
   * an updated Masterpanel (hence the name).
   * 
   * @param field -
   *          the field to pull changes from.
   * @param flags -
   *          describes what attributes to update
   * @return the number of modifications made
   */
  public int masterUberAlles(AveryPanelField field, BitSet flags)
  {
    int modified = super.masterUberAlles(field, flags);

    if (flags.get(TEXTCONTENT_DIFFERS))
    {
      setContent(((TextPath) field).getContent());
      ++modified;
    }

    return modified;
  }

  /**
   * Sets the field's shape, ellipse is default.
   * 
   * @param newShape -
   *          the shape
   */
  public void setShape(String newShape)
  {
    shape = newShape;
  }

  /**
   * Gets the field's shape.
   * 
   * @return the field's shape.
   */
  public String getShape()
  {
    return shape;
  }

  /**
   * Sets the field's text content.
   * 
   * @param newContent -
   *          the text
   */
  public void setContent(String newContent)
  {
    content = newContent;
  }

  /**
   * Gets the field's text content.
   * 
   * @return the field's text content.
   */
  public String getContent()
  {
    return content;
  }

  /**
   * Sets the field's start position, relative to the containing panel. The
   * position is a point on the text path, measured in twips from relative to
   * the center of this field.
   * 
   * @param newStartPosition -
   *          the desired position
   */
  public void setStartPosition(java.awt.Point newStartPosition)
  {
    startposition = newStartPosition;
  }

  /**
   * retrieves the start position
   * 
   * @return the field's start position relative to the center of this field, in
   *         twips
   * @see #setStartPosition
   */
  public java.awt.Point getStartPosition()
  {
    return startposition;
  }

  /**
   * Sets the field's rotation sense.
   * 
   * @param newRotateSense -
   *          the rotation sense (cw or ccw)
   */
  public void setRotateSense(String newRotateSense)
  {
    rotatesense = newRotateSense;
  }

  /**
   * Gets the field's rotation sense.
   * 
   * @return the field's rotation sense.
   */
  public String getRotateSense()
  {
    return rotatesense;
  }

  public String dump()
  {
    String str = new String("");
    str += "<P>TextPath";
    str += super.dump();
    str += "<br>shape = " + shape;
    str += "<br>content = " + content;
    str += "<br>startposition = " + startposition.toString();
    str += "<br>rotatesense = " + rotatesense;
    return str;
  }

  // implement Draw() as declared in abstract superclass AveryPanelField
  public void draw(Graphics2D gr, double dScalar)
  {
    if (getMask() != null)
    {
      getMask().clipImageMask(gr, dScalar);
    }
    drawOutlineElement(gr, dScalar);

    // NOTE: currently no support for rotated field for textpath
    // may not need to rotate this object on the panel, no spec.?

    // get position in twips
    Point positionOrig = super.getPosition();
    Point position = new Point(positionOrig);
    //System.out.println("twips x, y= " + position.toString());

    // convert position to pixels
    position.x = (int) ((double) position.x * dScalar);
    position.y = (int) ((double) position.y * dScalar);
    //System.out.println("pixels field x, y= " + position.toString());

    //System.out.println("twips w,h= " + super.getWidth().doubleValue() + "," + super.getHeight().doubleValue());
    double pixelwidth = super.getWidth().doubleValue() * dScalar;
    double pixelheight = super.getHeight().doubleValue() * dScalar;
    //System.out.println("pixels w,h= " + pixelwidth + "," + pixelheight);

    String encodedContent = content;
    if (getEncoding().equals("macroman"))
    {
      // convert xml UTF8 to MacRoman encoding because displaying a type1
      // macroman encoded font
      encodedContent = AveryUtils.convertUTF8StringToMacRoman(content);
    }
    else if (getEncoding().equals("UTF8"))
    {
      // convert XML generated UTF8 decimal to Unicode
      encodedContent = AveryUtils.convertUTF8DecimalToUnicode(content);
    }

    // System.out.println("draw content encodedContent= " + content + " " +
    // encodedContent);

    gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    gr.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    gr.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    FontRenderContext frc = gr.getFontRenderContext();

    // always make a new font
    Double ps = new Double(getPointsize().doubleValue());
    setPointsize(ps);
    ps = null;
    TextPath.ddScalar = dScalar;
    int previewFontsize = (int) (dScalar * 20.0 * getPointsize().doubleValue());

    makeFont(getPreviewTypeface(), previewFontsize);

    // need ascent to calculate text baseline which is a smaller concentric
    // ellipse
    // than the ellipse defined by the field bounding box
    GlyphVector gv1 = getFont().createGlyphVector(frc, "M");
    GlyphMetrics gm1 = gv1.getGlyphMetrics(0);
    Rectangle2D r2d = gm1.getBounds2D();
    double ascent = r2d.getHeight();
    //System.out.println("ascent= " + ascent);

    String justify = getJustification();

    double startangle = startPositionToAngle(startposition);

    /*
     * double startx = startposition.getX(); double starty =
     * startposition.getY(); if (startx >= -.0001 && startx < 0.0) startx =
     * -.0001; if (startx >= 0.0 && startx < 0.0001) startx = .0001; double
     * startangle = Math.atan(startposition.getY() / startx); if (startangle <
     * 0.0) startangle += 2.0 * Math.PI;
     */
    // System.out.println(startangle);
    // make sure the shrinking ellipse hasn't dwindled into virtual space
    int xcorrection = (int) (2. * ascent);
    int ycorrection = (int) (2. * ascent);
    if (pixelwidth - xcorrection <= 0)
      xcorrection = 0;
    if (pixelwidth - ycorrection <= 0)
      ycorrection = 0;

    // create the elliptical text render object
    EllipseText ellipsetext = new EllipseText(encodedContent, pixelwidth - xcorrection, pixelheight - ycorrection);

    // apply rotation sense
    if (rotatesense.equals("ccw"))
      ellipsetext.setDirection(false);

    // start angle depends on justification
    if (!justify.equals("left"))
    {
      double totalAngle = ellipsetext.getStringAngle(gr, 0.0, getFont());
      // System.out.println(totalAngle);
      double deltajustify = 0.0;

      if (justify.equals("center"))
      {
        deltajustify = totalAngle / 2.0;
      }
      else
      {
        deltajustify = totalAngle;
      }

      if (rotatesense.equals("cw"))
      {
        startangle -= deltajustify;
      }
      else
      {
        // appear to need a partial character width correction on PDF of start
        // angle
        if (justify.equals("center"))
          deltajustify -= 0.025 * totalAngle / (double) content.length();
        startangle += deltajustify;
      }
    }
    // System.out.println(startangle);
    ellipsetext.setStartAngle(startangle);

    // center point is middle of the field
    ellipsetext.setCenterPoint((int) (position.x + (pixelwidth / 2.0)), (int) (position.y + (pixelheight / 2.0)));

    // check field rotation
    double fieldRotationAngle = getRotation().doubleValue() * Math.PI / 180.0;
    if (fieldRotationAngle != 0.0)
    {
      ellipsetext.setFieldRotationAngle(fieldRotationAngle);
    }

    // some other cool effects unused in this UI
    // limits text between two angles
    // ellipsetext.setArc(185, 350);
    // shrinks to fit
    // int nFontSize = ellipsetext.autoArcFit("Verdana");
    // Font fitFont = new Font("Verdana", Font.PLAIN, nFontSize);

    // set font
    ellipsetext.setTextFont(getFont());
    ellipsetext.setTextColor(getTextcolor());
    ellipsetext.setTextOutlineColor(getTextcolor());

    // some other cool effects with no UI to turn them on
    // ellipsetext.setTextAlpha(.6f);
    // ellipsetext.setTextShadowAlpha(.6f);
    // ellipsetext.setTextShadowAngle(225);
    // ellipsetext.setTextShadowLength(22);
    // ellipsetext.setTextOutlineWidth(4);
 
    // paint the thing
    ellipsetext.paint(gr, getOpacity().floatValue());

    gr.setClip(null);
    if (getMask() != null && getMask().incrementIndex())
    {
      draw(gr, dScalar);
    }
  }

  public void addToPDF(PDFPage pdfPage, Double dPanelHeight, Hashtable imageCache) throws Exception
  {
    addOutlineToPDF(pdfPage, dPanelHeight);
    int canvasStartX = 0;
    int canvasStartY = 0;
    double heightCalibration = 0;
    if (getPosition().x < 0)
    {
      canvasStartX = (int) (getPosition().x / 20f);
    }
    if (getPosition().y < 0)
    {
      canvasStartY = (int) (getPosition().y / 20f);
      heightCalibration = - getPosition().y;
    }

		int areaSize = getAreaSize(pdfPage, canvasStartX, canvasStartY);
		
    PDFCanvas contentContainer = new PDFCanvas(areaSize, areaSize);

    if (getMask() != null)
    {
      getMask().clipMask(contentContainer, canvasStartX, canvasStartY);
    }

    if (content.length() < 1)
      return;

    // the font size used depends on the font size of the preview.
    // if the preview font size was (int) truncated, must decrease the
    // pdf font size by one, or no adjustment if the preview size happened to
    // be an integral value before conversion to an int.
    double dPreviewFontsize = TextPath.ddScalar * 20.0 * getPointsize().doubleValue();
    int previewFontsize = (int) dPreviewFontsize;

    int fontSizeDelta = -1;
    if ((double) previewFontsize == dPreviewFontsize)
      fontSizeDelta = 0;

    // Set up font
    PDFStyle style = new PDFStyle();
    try
    {
      // use font cache
      Hashtable fontCache = AveryProject.fontCache;
      if (fontCache.containsKey(getTypeface()))
        setOpenFont((OpenTypeFont) fontCache.get(getTypeface()));
      else
      {
        String fullFontName = AveryProject.fontDir + "/" + getFontFileName();

        // at least for now UTF8 fonts are assumed 2 byte unicode
        // to support the eastern asian fonts that work with BFO.
        int bytesPerChar = 1;
        if (getEncoding().equals("UTF8"))
          bytesPerChar = 2;

        setOpenFont(new OpenTypeFont(new FileInputStream(fullFontName), bytesPerChar));
        fontCache.put(getTypeface(), getOpenFont());
      }

      // experiments show some asian fonts do not subset.
      // recommend only using fonts of any kind that will subset
      // this flag forces the entire font to be embedded.
      // this makes the pdf large but this needed explanation here.
      // if you want all chars of a font use this line of code at
      // the peril of large files:
      // openFont.setSubset(false);

      style.setFont(getOpenFont(), getPointsize().floatValue() + (float) fontSizeDelta);
      style.setFillColor(getTextcolor());

      contentContainer.setStyle(style);

    }
    catch (Exception e)
    {
      System.out.println("TextPath Font Exception!");
      System.out.println(e.toString());
      setTypeface("times");
      style.setFont(new StandardFont(StandardFont.TIMES), getPointsize().floatValue() + (float) fontSizeDelta);
    }

    style.setFontFeature("latinligatures", false);
    
    // get twip position of field
    double twippositionx = getPosition().x;
    double twippositiony = dPanelHeight.intValue() - getPosition().y;

    // get point position of field
    double pointpositionx = twippositionx / 20.0 - canvasStartX;
    double pointpositiony = twippositiony / 20.0 - canvasStartY;

    // get point size of field
    double pointwidth = super.getWidth().doubleValue() / 20.0 - canvasStartX;
    double pointheight = super.getHeight().doubleValue() / 20.0 - canvasStartY;

    // create a java font and Graphics2D to position elliptical text
    BufferedImage dummy = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);

    Graphics2D gr = dummy.createGraphics();

    gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    gr.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    gr.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    FontRenderContext frc = gr.getFontRenderContext();

    // always make a new font
    Double ps = new Double(getPointsize().doubleValue());
    setPointsize(ps);
    ps = null;
    int pdfFontsize = getPointsize().intValue() + fontSizeDelta;
    makeFont(getPreviewTypeface(), pdfFontsize);
    GlyphVector gv1 = getFont().createGlyphVector(frc, "M");
    GlyphMetrics gm1 = gv1.getGlyphMetrics(0);
    Rectangle2D r2d = gm1.getBounds2D();
    int ascent = (int) (r2d.getHeight());
    //System.out.println("ascent= " + ascent);

    // calculate baseline ellipse from bounding box and current font size
    String justify = getJustification();
    double startx = startposition.getX();
    // double starty = startposition.getY();
    if (startx >= -.0001 && startx < 0.0)
      startx = -.0001;
    if (startx >= 0.0 && startx < 0.0001)
      startx = .0001;
    double startangle = Math.atan(startposition.getY() / startx);
    if (startangle < 0.0)
      startangle += 2.0 * Math.PI;

    // make sure the shrinking ellipse hasn't dwindled into virtual space
    // something happened to make the ellipse too small??
    // even though this looks like inscribing inside the bounding box something else
    // seems to have happened??? for now set corrections to zero to make box larger.
    
    double xcorrection = 0; //(double) (2 * ascent);
    double ycorrection = 0; //(double) (2 * ascent);
    if (pointwidth - xcorrection <= 0.0)
      xcorrection = 0.0;
    if (pointheight - ycorrection <= 0.0)
      ycorrection = 0.0;

    // get glyph positions from the elliptical text render object
    EllipseText ellipsetext = new EllipseText(content, pointwidth - xcorrection, pointheight - ycorrection);

    // apply rotation sense
    if (rotatesense.equals("ccw"))
      ellipsetext.setDirection(false);

    // start angle depends on justification
    if (!justify.equals("left"))
    {
      double totalAngle = ellipsetext.getStringAngle(gr, 0.0, getFont());
      double deltajustify = 0.0;

      if (justify.equals("center"))
      {
        deltajustify = totalAngle / 2.0;
      }
      else
      {
        deltajustify = totalAngle;
      }

      if (rotatesense.equals("cw"))
      {
        startangle -= deltajustify;
      }
      else
      {
        // appear to need a partial character width correction on PDF of start
        // angle
        if (justify.equals("center"))
          deltajustify -= 0.025 * totalAngle / (double) content.length();
        startangle += deltajustify;
      }
    }
    ellipsetext.setStartAngle(startangle);

    // center point is middle of the field
    double centerx = pointpositionx + pointwidth / 2.;
    double centery = pointpositiony - pointheight / 2.;
    ellipsetext.setCenterPoint((int) centerx, (int) centery);
    // System.out.println("center x,y");
    // System.out.println(centerx);
    // System.out.println(centery);

    // set font
    ellipsetext.setTextFont(getFont());

    // get the glyph coordinats and rotation angle
    double coordinates[] = ellipsetext.getGlyphCoordinates(gr);

    gr.dispose();
    gr = null;

    // Write glyphs
    int count = 0;
    for (int i = 0; i < content.length(); i++)
    {
      // char c = content.charAt(i);
      String textOut = content.substring(i, i + 1);

      // field rotation
      double fieldRotation = getRotation().doubleValue();

      // rotate?
      if (fieldRotation != 0.0)
      {
        // access coordinate and rotation angle of each glyph
        float positionx = (float) (centerx + coordinates[count++]);
        float positiony = (float) (centery - coordinates[count++]);
        float angle = (float) coordinates[count++];

        // don't let concats concat inside the rotate
        contentContainer.save();
        // get rotation matrix values
        transformPDFField(contentContainer, new Double(dPanelHeight.doubleValue() + heightCalibration), positionx, positiony,
            fieldRotation);
        // transform glyph
        float sinangle = (float) Math.sin(angle);
        float cosangle = (float) Math.cos(angle);

        String concat = AveryPanelField.formatConcat(cosangle, -sinangle, sinangle, cosangle, 0.0f, 0.0f);
        contentContainer.rawWrite(concat);

        // render text
        LayoutBox tmpBox = new LayoutBox(getWidth().floatValue() / 20.0f);
        tmpBox.addTextNoBreak(textOut, style, null);
        contentContainer.drawLayoutBox(tmpBox, 0.0f, 0.0f);
        // contentContainer.drawText(textOut, 0.0f, 0.0f);

        // forget the rotate operations
        contentContainer.restore();
      }
      else
      {
        // access coordinate and rotation angle of each glyph
        float positionx = (float) (centerx + coordinates[count++]);
        float positiony = (float) (centery - coordinates[count++]);
        float angle = (float) coordinates[count++];

        // don't let concats concat inside the rotate
        contentContainer.save();
        // transform glyph
        float sinangle = (float) Math.sin(angle);
        float cosangle = (float) Math.cos(angle);

        String concat = AveryPanelField.formatConcat(cosangle, -sinangle, sinangle, cosangle, positionx, positiony);
        contentContainer.rawWrite(concat);

        // render text
        // contentContainer.drawText(textOut, 0.0f, 0.0f);
        LayoutBox tmpBox = new LayoutBox(getWidth().floatValue() / 20.0f);
        tmpBox.addTextNoBreak(textOut, style, null);
        contentContainer.drawLayoutBox(tmpBox, 0.0f, 0.0f);
        // if (isUnderline())
        // {
        // pdfdoc.setlinewidth(underlineThickness);
        // pdfdoc.moveto(positionx, positiony - underlineOffset);
        // pdfdoc.lineto(positionx + pdfdoc.stringwidth(textOut), positiony -
        // underlineOffset);
        // pdfdoc.stroke();
        // }

        // forget the rotate operations
        contentContainer.restore();
      }
    }
    // release coordinates array
    coordinates = null;
    pdfPage.drawCanvas(contentContainer, canvasStartX, canvasStartY, areaSize + canvasStartX, areaSize + canvasStartY);
    if (getMask() != null && getMask().incrementIndex())
    {
      addToPDF(pdfPage, dPanelHeight, imageCache);
    }
  } // end addToPDF()

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAveryElement()
  {
    Element element = getParentAveryElement();
    Element content = new Element("Avery.pf.textpath");

    // add attributes
    content.setAttribute("typeface", getTypeface());
    content.setAttribute("pointsize", getPointsize().toString());
    content.setAttribute("textcolor", "0x" + Integer.toHexString(getTextcolor().getRGB()).substring(2));
    content.setAttribute("justification", getJustification());
    content.setAttribute("styles", getStyles());
    content.setAttribute("startposition", Double.toString(getStartPosition().getX()) + ","
        + Double.toString(getStartPosition().getY()));
    content.setAttribute("rotatesense", (getRotateSense() == null) ? "cw" : getRotateSense());
    if (getShape() != null)
    { // defaults to "ellipse" in the DTD
      content.setAttribute("shape", getShape());
    }

    // add the content itself
    if (getContent() != null)
    {
      content.setText(getContent());
    }

    // push content child element into the parent element
    element.addContent(content);
    return element;
  }

  /**
   * Converts starting point to angle in radians
   */
  protected double startPositionToAngle(Point startposition)
  {
    double startx = startposition.getX();
    double starty = startposition.getY();

    // locate quadrant
    int quadrant = 0;
    if (startx > 0.0 && starty >= 0.0)
    {
      quadrant = 0;
    }
    else if (startx <= 0.0 && starty > 0.0)
    {
      quadrant = 1;
      startx = -startx;
    }
    else if (startx < 0.0 && starty <= 0.0)
    {
      quadrant = 2;
      startx = -startx;
      starty = -starty;
    }
    else if (startx >= 0.0 && starty < 0.0)
    {
      quadrant = 3;
      starty = -starty;
    }

    if (startx >= 0.0 && startx < 0.00001)
      startx = .00001;

    double startangle = Math.atan(starty / startx);

    // account for quadrant
    if (quadrant == 1)
      startangle = 3.14159 - startangle;
    else if (quadrant == 2)
      startangle = 3.14159 + startangle;
    else if (quadrant == 3)
      startangle = 6.28318 - startangle;

    return startangle;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("textPath");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    super.applyTextfieldAverysoftAttributes(element);

    element.setAttribute("startPosition", Double.toString(getStartPosition().getX()) + ","
        + Double.toString(getStartPosition().getY()));
		
    element.setAttribute("rotateSense", getRotateSense());
    element.setAttribute("shape", getShape());

    if (getDescriptionObject() == null)
    {
      setDescriptionObject(Description.makeTextFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
	  position.setAttribute("x", Double.toString(getPosition().getX()));
	  position.setAttribute("y", Double.toString(getPosition().getY()));
	  element.addContent(position);

    if (getID().equals(getStyleID()))
    {
      copyStyleAttributes();
      element.addContent(getTextStyle().getAverysoftElement());
    }

    if (getID().equals(getContentID()))
    {
      Element text = new Element("text");
      text.setNamespace(AveryProject.getAverysoftNamespace());

      // add the content
      text.setText(getContent());
      element.addContent(text);
    }

    return element;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAverysoftIBElement()
  {
    Element element = new Element("textPath");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    super.applyTextfieldAverysoftAttributes(element);

		element.setAttribute("startPosition", AveryProject.twipsToMM(getStartPosition().getX()) + ","
				+ AveryProject.twipsToMM(getStartPosition().getY()));

    element.setAttribute("rotateSense", getRotateSense());
    element.setAttribute("shape", getShape());

    /*if (getDescriptionObject() == null)
    {
      setDescriptionObject(Description.makeTextFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());*/

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
		position.setAttribute("x", AveryProject.twipsToMM(getPosition().getX()));
		position.setAttribute("y", AveryProject.twipsToMM(getPosition().getY()));
    element.addContent(position);

    if (getID().equals(getStyleID()))
    {
      copyStyleAttributes();
      element.addContent(getTextStyle().getAverysoftElement());
    }

    if (getID().equals(getContentID()))
    {
      Element text = new Element("text");
      text.setNamespace(AveryProject.getAverysoftNamespace());

      // add the content
      text.setText(getContent());
      element.addContent(text);
    }

    return element;
  }
  /**
   * This SHOULD calculates the maximum pointsize that will render correctly. In
   * reality, it returns the current pointsize * 1.5, on the theory that the
   * user will increase in small increments and back off if it turns ugly. Used
   * by Avery Print 5.0.
   * 
   * @return the maximum size in points.
   */
  public double maxPointSize()
  {
    return this.getPointsize().doubleValue() * 1.5;
  }

  /**
   * @see AveryTextfield.translateText
   */
  int translateText(Hashtable hash)
  {
    if (hash.containsKey(content))
    {
      content = (String) hash.get(content);
      return 1;
    }
    else
      return 0;
  }
}
