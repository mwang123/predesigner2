/**
 * Title:        EllipseTextath<p>
 * Description:  EllipseText renders text on an ellipse
 * Copyright:    Copyright (c)2003 Avery Dennison<p>
 * Company:      Avery Dennison<p>
 * @author       Brad Nelson
 * @version      2.0
 */

package com.avery.project;

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.image.*;

public class EllipseText
{
  private final int MIN_FONT_SIZE = 8;
  private final int MAX_FONT_SIZE = 216;

  // geometry variables
  private Point ptCenter = new Point(74, 49);
  private double  aActual = 125;
  private double  bActual = 100;

  private double  startAngle = 1.5 * Math.PI;
  private double  endAngle = -1;

  private double  fieldRotationAngle = 0.0;

  private boolean bDirection = true;  //clockwise

  // text variables
  private String  sText = "Insert text here";

  private Font textFont = new Font("Serif", Font.PLAIN, 36);
  private Color textColor = Color.black;
  private Color textOutlineColor = Color.black;

  private int textOutlineWidth = 1;

  private float fTextShadowAlpha = 1.f;

  private double textShadowAngle = Math.PI / 4.;
  private int textShadowLength = 0;

  private int xshadow = 0;
  private int yshadow = 0;

  private int autoFontSize = MAX_FONT_SIZE;

  // one of me to make it possible to save two of me in a jpeg....
  EllipseText  ellipseText = null;

  // default to radians
  private boolean bDegrees = false;

  public EllipseText()
  {
  }

  public EllipseText(String sText, double xBound, double yBound)
  {
    setText(sText);
    setBoundingRect(xBound, yBound);
  }

  public void setText(String sText)
  {
    this.sText = sText;
  }

  public void setTextFont(Font textFont)
  {
    this.textFont = textFont;
  }

  public void setTextColor(Color textColor)
  {
    this.textColor = textColor;
  }

  public void setTextOutlineColor(Color textOutlineColor)
  {
    this.textOutlineColor = textOutlineColor;
  }

  public void setTextOutlineWidth(int textOutlineWidth)
  {
    if (textOutlineWidth > 0 && textOutlineWidth < 10)
        this.textOutlineWidth = textOutlineWidth;
  }

  public void setCenterPoint(int x, int y)
  {
    ptCenter.x = x;
    ptCenter.y = y;
//    System.out.println("center= " + ptCenter.toString());
  }

  public void setCenterPoint(Point center)
  {
    ptCenter = center;
//    System.out.println("center= " + center.toString());
  }

  public void setBoundingRect(double xBound, double yBound)
  {
    aActual = xBound / 2.;
    bActual = yBound / 2.;
//    System.out.println(aActual);
//    System.out.println(bActual);
  }

  public void setStartAngle(double startAngle)
  {
    double radians = startAngle;
    if (bDegrees == true)
    {
      radians = startAngle * Math.PI / 180.0;
    }

    this.startAngle = radians;
  }

  public void setEndAngle(double endAngle)
  {
    double radians = endAngle;
    if (bDegrees == true)
    {
      radians = endAngle * Math.PI / 180.0;
    }

    this.endAngle = radians;
  }

  public void setFieldRotationAngle(double fieldRotationAngle)
  {
    double radians = fieldRotationAngle;
    if (bDegrees == true)
    {
      radians = fieldRotationAngle * Math.PI / 180.0;
    }

    this.fieldRotationAngle = radians;
  }

  public void setArc(double startAngle, double endAngle)
  {
    setStartAngle(startAngle);
    setEndAngle(endAngle);
  }

  public void setDirection(boolean bDirection)
  {
    this.bDirection = bDirection;
  }

  public void setDegrees(boolean bDegrees)
  {
    this.bDegrees = bDegrees;
  }

  public void setTextShadowAlpha(float fTextShadowAlpha)
  {
    this.fTextShadowAlpha = fTextShadowAlpha;
  }

  public void setTextShadowAngle(double textShadowAngle)
  {
    double radians = textShadowAngle;
    if (bDegrees == true)
    {
        radians = textShadowAngle * Math.PI / 180.0;
    }

    this.textShadowAngle = radians;
    if (textShadowLength > 0)
        setShadowVector();
  }

  public void setTextShadowLength(int textShadowLength)
  {
    this.textShadowLength = textShadowLength;
    if (textShadowLength > 0)
        setShadowVector();
  }

  private void setShadowVector()
  {
    xshadow = (int)((double)textShadowLength * Math.cos(textShadowAngle));
    yshadow = (int)((double)textShadowLength * Math.sin(textShadowAngle));
  }

  // returns the size of the font that fits the text in the arc
  public int autoArcFit(String fontName)
  {
    boolean bDone = false;

    BufferedImage mImage = new BufferedImage((int)(2.5 * aActual), (int)(2.5 * bActual), BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = mImage.createGraphics();

    int nFontSize = MIN_FONT_SIZE - 1;
    double arcAngle = 0;
    if (bDirection == true)
    {
      if (startAngle >= endAngle)
        arcAngle = 2. * Math.PI - startAngle + endAngle;
      else
        arcAngle = Math.abs(endAngle - startAngle);
    }
    else
    {
      if (endAngle >= startAngle)
        arcAngle = 2. * Math.PI - endAngle + startAngle;
      else
        arcAngle = Math.abs(startAngle - endAngle);
    }
    while (bDone == false && nFontSize < MAX_FONT_SIZE)
    {
      nFontSize++;
      Font testingFont = new Font(fontName, Font.PLAIN, nFontSize);
      double total_angle =  getStringAngle(g2, startAngle, testingFont);

      if (total_angle >= arcAngle)
        bDone = true;
      else if (total_angle < (arcAngle / 2.25))
        nFontSize += nFontSize - 2;
    }

    autoFontSize = nFontSize;

    return (nFontSize);
  }

  // locate glyph coordinates and rotation angles for PDF
  public double[] getGlyphCoordinates(Graphics2D g2)
  {
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
      RenderingHints.VALUE_ANTIALIAS_ON);

    FontRenderContext frc = g2.getFontRenderContext();

    GlyphVector gv1 = textFont.createGlyphVector(frc, "M");
    GlyphMetrics gm1 = gv1.getGlyphMetrics(0);
    Rectangle2D r2d= gm1.getBounds2D();
    double ascent = r2d.getHeight();

    GlyphVector gv = textFont.createGlyphVector(frc, sText);
    int length = gv.getNumGlyphs();
    // this must be released by the consumer!
    double coordinates[] = new double[length * 3];

    double a = aActual;
    double b = bActual;

    double r1_angle = startAngle;


            // draw test axis
//                Line2D xline = new Line2D.Double(0., 399., 799., 399.);
//                g2.draw(xline);
//                Line2D yline = new Line2D.Double(399., 0., 399., 799.);
//                g2.draw(yline);
//               Line2D xyline = new Line2D.Double(0., 0., 799., 799.);
//                g2.draw(xyline);
//               Ellipse2D ellipse = new Ellipse2D.Double(99., 149., 2 * aActual, 2 * bActual);
//               g2.draw(ellipse);

    int count = 0;
    for (int i = 0; i < length; i++)
    {
      GlyphMetrics gm = gv.getGlyphMetrics(i);
      double advance = gm.getAdvance();
//      System.out.println(advance);

      double r1_x = a * Math.cos(r1_angle);
      double r1_y = b * Math.sin(r1_angle);
      double phi = getCharacterAngle(advance, r1_angle);
      double rho = (phi + r1_angle) / 2.;
      double r2_x = a * Math.cos(rho);
      double r2_y = b * Math.sin(rho);

      if (bDirection == false)
      {
        double cwidth = advance - gm.getLSB();
        double omega = getCharacterAngle(cwidth, r1_angle);
        omega = (omega + r1_angle) / 2.;
        double dx = 0.96 * ascent * Math.cos(omega);
        double dy = 0.96 * ascent * Math.sin(omega);
        r1_x += dx;
        r1_y += dy;
      }

      double r2_x_approx = r2_x;
      if (r2_x_approx == 0.0)
        r2_x_approx = .000001;
      double slope = (a * a * r2_y) / (b * b * r2_x_approx);

      double rotation_angle = Math.atan(slope); //+ Math.PI / 2.;
      if (r2_x < -0.00001)
        rotation_angle += Math.PI;
//                    double epsilon = 3. * Math.PI / 180.;

//                Double ra = new Double(rotation_angle);
//                Double r1x = new Double(r1_x);
//                Double r1y = new Double(r1_y);

//                System.out.println(r1x.toString() + "," + r1y.toString() + "," +  ra.toString());

      if (bDirection == true)
      {
        rotation_angle += Math.PI / 2.;
      }
      else
      {
        rotation_angle -= Math.PI / 2.;
      }

      r1_angle = phi;

      if (r1_angle > (2.0 * Math.PI))
        r1_angle -= (2.0 * Math.PI);

      // store the results
      coordinates[count++] = r1_x;
      coordinates[count++] = r1_y;
      coordinates[count++] = rotation_angle;
    }
    return coordinates;
  }

  public void paint(Graphics2D g2, float opacity)
  {
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
      RenderingHints.VALUE_ANTIALIAS_ON);

    // variables to restore to context
    Composite cold = g2.getComposite();
    Color colorold = g2.getColor();

    float fTextAlpha = 1.0f;
    if (opacity >= 0f && opacity < 1.0f)
    	fTextAlpha = opacity;
    
    Composite c = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, fTextAlpha);
    g2.setComposite(c);

    FontRenderContext frc = g2.getFontRenderContext();

    GlyphVector gv1 = textFont.createGlyphVector(frc, "M");
    GlyphMetrics gm1 = gv1.getGlyphMetrics(0);
    Rectangle2D r2d= gm1.getBounds2D();
    double ascent = r2d.getHeight();

    GlyphVector gv = textFont.createGlyphVector(frc, sText);
    int length = gv.getNumGlyphs();

    double a = aActual;
    double b = bActual;

    double r1_angle = startAngle;


                // debug - draw test axis
                // the center and length depends on the object, so the debug code is not
                // generalized, coordinates have to be manually adjusted if this is used.
/*               Line2D xline = new Line2D.Double(222 - aActual, 222., 222 + aActual, 222.);
                g2.draw(xline);
                Line2D yline = new Line2D.Double(224., 224. - aActual, 224., 224 + aActual);
                g2.draw(yline);
               Line2D xyline = new Line2D.Double(0., 0., 2 * aActual, 2 * bActual);
                g2.draw(xyline);
               Ellipse2D ellipse = new Ellipse2D.Double(55., 55., 2 * aActual, 2 * bActual);
               g2.draw(ellipse); */

              // debug - variables to draw line connecting lower left corner of each glyph
/*              double startx = 0.0;
              double starty = 0.0;
              double endx = 0.0;
              double endy = 0.0; */

    Point2D p2d = new Point2D.Double(0.0, 0.0);

    for (int i = 0; i < length; i++)
    {
      // coordinates of each glyph must be set to zero as of JRE 1.4.2
      // this was not necessary in earlier releases of the JRE
      gv.setGlyphPosition(i, p2d);

      GlyphMetrics gm = gv.getGlyphMetrics(i);
      double advance = gm.getAdvance();
      double r1_x = a * Math.cos(r1_angle);
      double r1_y = b * Math.sin(r1_angle);
      double phi = getCharacterAngle(advance, r1_angle);
      double rho = (phi + r1_angle) / 2.;
      double r2_x = a * Math.cos(rho);
      double r2_y = b * Math.sin(rho);

      if (bDirection == false)
      {
        double cwidth = advance - gm.getLSB();
        double omega = getCharacterAngle(cwidth, r1_angle);
        omega = (omega + r1_angle) / 2.;
        double dx = 0.96 * ascent * Math.cos(omega);
        double dy = 0.96 * ascent * Math.sin(omega);
        r1_x += dx;
        r1_y += dy;
      }

      double r2_x_approx = r2_x;
      if (r2_x_approx == 0.0)
        r2_x_approx = .000001;
      double slope = (a * a * r2_y) / (b * b * r2_x_approx);

      double rotation_angle = Math.atan(slope); //+ Math.PI / 2.;
      if (r2_x < -0.00001)
        rotation_angle += Math.PI;

//                    double epsilon = 3. * Math.PI / 180.;

//                Double ra = new Double(rotation_angle);
//                Double r1x = new Double(r1_x);
//                Double r1y = new Double(r1_y);

//                System.out.println(r1x.toString() + "," + r1y.toString() + "," +  ra.toString());

      double deltax = 0.0;
      double deltay = 0.0;

      if (fieldRotationAngle != 0.0)
      {
        // translate glyph position to field coordinates
        // the origin is now the field upper left corner
        // positive y axis points down in this coordinate system
        deltax = r1_x + aActual + ascent;
        deltay = -(r1_y + bActual + ascent);

//      System.out.println("deltax= " + new Float(deltax).toString());
//      System.out.println("deltay= " + new Float(deltay).toString());

        double sintheta = Math.sin(fieldRotationAngle);
        double costheta = Math.cos(fieldRotationAngle);
//      System.out.println("sintheta= " + new Float(sintheta).toString());
//      System.out.println("costheta= " + new Float(costheta).toString());

        // rotate the field around the field upper left corner
        double xprime = deltax * costheta - deltay * sintheta;
        double yprime = deltax * sintheta + deltay * costheta;
//      System.out.println("xprime= " + new Float(xprime).toString());
//      System.out.println("yprime= " + new Float(yprime).toString());
        if (xprime > -.001 && xprime < .001)
          xprime = 0.0f;
        if (yprime > -.001 && yprime < .001)
          yprime = 0.0f;

        // get deltas from original glyph position to rotated field glyph position
        deltax = xprime - deltax;
        deltay = -(yprime - deltay);
//      System.out.println("new deltax= " + new Float(deltax).toString());
//      System.out.println("new deltay= " + new Float(deltay).toString());
      }

      AffineTransform at = AffineTransform.getTranslateInstance((double)ptCenter.x + r1_x + deltax, (double)ptCenter.y + r1_y + deltay);
      AffineTransform atshadow = null;

              // debug - draws line connecting lower left corner of each glyph
/*              if (i == 0)
              {
                startx = endx = (double)ptCenter.x + r1_x + deltax;
                starty = endy = (double)ptCenter.y + r1_y + deltay;
              }
              else
              {
                endx = (double)ptCenter.x + r1_x + deltax;
                endy = (double)ptCenter.y + r1_y + deltay;
                Line2D glyphline = new Line2D.Double(startx, starty, endx, endy);
                g2.draw(glyphline);
                startx = endx;
                starty = endy;
              } */

      if (textShadowLength > 0)
      {
        atshadow = AffineTransform.getTranslateInstance((double)ptCenter.x + r1_x + deltax + xshadow, (double)ptCenter.y + r1_y + deltay + yshadow);
      }

      if (bDirection == true)
      {
        at.rotate(rotation_angle - fieldRotationAngle + (Math.PI / 2.));
      }
      else
      {
        at.rotate(rotation_angle - fieldRotationAngle - (Math.PI / 2.));
      }

      Shape glyph = gv.getGlyphOutline(i);

      if (atshadow != null)
      {
        c = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, fTextShadowAlpha);
        g2.setComposite(c);
        if (bDirection == true)
        {
          atshadow.rotate(rotation_angle - fieldRotationAngle + (Math.PI / 2.));
        }
        else
        {
          atshadow.rotate(rotation_angle - fieldRotationAngle - (Math.PI / 2.));
        }
        Shape transformedShadowGlyph = atshadow.createTransformedShape(glyph);
        g2.setColor(Color.black);
        g2.draw(transformedShadowGlyph);
        g2.setColor(Color.black);
        g2.fill(transformedShadowGlyph);
        c = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, fTextAlpha);
        g2.setComposite(c);
      }

      BasicStroke bs = null;
      BasicStroke bsold = null;

      if (textOutlineWidth > 1)
      {
        bsold = (BasicStroke)(g2.getStroke());
        bs = new BasicStroke(textOutlineWidth);
        g2.setStroke(bs);
      }

      Shape transformedGlyph = at.createTransformedShape(glyph);
      g2.setColor(textOutlineColor);
      g2.draw(transformedGlyph);

      // do not fill stroked fonts - presently only CAMPBELL
      if (!textFont.getName().startsWith("CAMPBELL"))
      {
        g2.setColor(textColor);
        g2.fill(transformedGlyph);
      }

      if (bsold != null)
        g2.setStroke(bsold);

      r1_angle = phi;

      if (r1_angle > (2.0 * Math.PI))
        r1_angle -= (2.0 * Math.PI);
    }
    g2.setComposite(cold);
    g2.setColor(colorold);
  }

  public double getCharacterAngle(double w, double theta)
  {
    double twopi = 2. * Math.PI;
    double sintheta = Math.sin(theta);
    double costheta = Math.cos(theta);
    double wsquared = w * w;
    double deltaphi = twopi / 3600.;
    double deltaphidir = deltaphi;
    double phi = theta;
    double angle = 0.;

    boolean bDone = false;
    double old_result = 100000000000000.0;

    if (bDirection == false)
    {
      deltaphidir = -deltaphidir;
 //           wsquared = wsquared * 1.1;
    }

    while (bDone == false && angle < twopi)
    {
      phi += deltaphidir;
      angle += deltaphi;
      double cosdiff = Math.cos(phi) - costheta;
      double sindiff = Math.sin(phi) - sintheta;
      double result = aActual * aActual * cosdiff * cosdiff + bActual * bActual * sindiff * sindiff;
      double diff = result - wsquared;
      if (diff < 0.)
        diff = -diff;
//      System.out.println(diff);
      if (old_result > diff)
      {
        old_result = diff;
      }
      else
        bDone = true;
    }
    return phi;
  }

  public double getStringAngle(Graphics2D g2, double startingAngle, Font testingFont)
  {
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
      RenderingHints.VALUE_ANTIALIAS_ON);

    FontRenderContext frc = g2.getFontRenderContext();

    // GlyphVector gv1 = testingFont.createGlyphVector(frc, "M");
    // GlyphMetrics gm1 = gv1.getGlyphMetrics(0);
    // Rectangle2D r2d= gm1.getBounds2D();
    // double ascent = r2d.getHeight();

    GlyphVector gv = testingFont.createGlyphVector(frc, sText);
    int length = gv.getNumGlyphs();

    double r1_angle = startingAngle;
    double totalAngle = 0.;

    for (int i = 0; i < length; i++)
    {
      GlyphMetrics gm = gv.getGlyphMetrics(i);
      double advance = gm.getAdvance();

      // double r1_x = aActual * Math.cos(r1_angle);
      // double r1_y = bActual * Math.sin(r1_angle);
      double phi = getCharacterAngle(advance, r1_angle);

      totalAngle += Math.abs(phi - r1_angle);

      r1_angle = phi;

      if (r1_angle > (2.0 * Math.PI))
        r1_angle -= (2.0 * Math.PI);
    }
//		Double ta = new Double(totalAngle);
//		System.out.println(ta.toString());
    return totalAngle;
  }

  // SaveJPEG - renders text to a white background and saves image as a JPEG
  // can save this text ellipse AND optionally another text ellipse if the additional
  // one is associated to this one....for example to save a 'top' and 'bottom' section of text

  public void SaveJPEG(String fileName)
  {
    try
    {
      // estimate the width and the height of the image...based on font size, shadow length, and baseline interior rectangle
      int imageWidth = 2 * (int)((double)autoFontSize + (double)textShadowLength + aActual);
      int imageHeight = 2 * (int)((double)autoFontSize + (double)textShadowLength + bActual);
      int imageXCenter = imageWidth / 2;
      int imageYCenter = imageHeight / 2;
      Point ptOldCenter = new Point(ptCenter.x, ptCenter.y);
      Point ptNewCenter = new Point(imageXCenter, imageYCenter);
      setCenterPoint(ptNewCenter);

      java.io.FileOutputStream out = new java.io.FileOutputStream(fileName);
      java.awt.image.BufferedImage bi = new  java.awt.image.BufferedImage(imageWidth,imageHeight,java.awt.image.BufferedImage.TYPE_INT_RGB);  //null;
      // HARDWIRED!! this should be configured based on the ellipse being rendered to....
      java.awt.Graphics2D g2d = (Graphics2D)bi.getGraphics();
      g2d.setBackground(Color.white);
      g2d.clearRect(0,0,imageWidth,imageHeight);

      paint(g2d, 1.0f);
      if (ellipseText != null)
      {
        ellipseText.setCenterPoint(ptNewCenter);
        ellipseText.paint(g2d, 1.0f);
        ellipseText.setCenterPoint(ptOldCenter);
      }

      setCenterPoint(ptOldCenter);

      com.sun.image.codec.jpeg.JPEGImageEncoder encoder = com.sun.image.codec.jpeg.JPEGCodec.createJPEGEncoder(out);
      encoder.encode(bi);
      out.flush();
      out.close();
    }
    catch (Exception e) {}
  }
}
