package com.avery.project;

import java.awt.Color;

public class Outline
{
  private float width;
  private Color color;
  private float opacity;
  public float getWidth()
  {
    return width;
  }
  public void setWidth(float width)
  {
    this.width = width;
  }
  public Color getColor()
  {
    return color;
  }
  public void setColor(Color color)
  {
    this.color = color;
  }
  public float getOpacity()
  {
    return opacity;
  }
  public void setOpacity(float opacity)
  {
    this.opacity = opacity;
  }
}
