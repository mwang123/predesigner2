/* Project: Ape 
 * Filename: Averysoft.java
 * Created on Jun 9, 2004 by Bob Lee
 * Copyright 2004 by Avery Dennison Corporation, all rights reserved
 */
package com.avery.project;

import org.jdom.Element;

/**
 * @author b0b
 *
 * Use this interface for all objects that can be serialized into
 * the format defined by averysoft.xsd
 */
interface Averysoftable
{
	/**
	 * Get a JDOM representation of the object suitable for
	 * inclusion in an XML file based on averysoft.xsd
	 * @return an Averysoft-based JDOM Element
	 */
	Element getAverysoftElement();
	
	/**
	 * Get a JDOM representation of the object suitable for
	 * inclusion in an XML file based on averysoft.xsd
	 * @return an Averysoft-based JDOM Element
	 */
	Element getAverysoftIBElement();
	
/**
	 * Get a JDOM representation of the object suitable for
	 * inclusion in a Blank Avery Template (BAT) file
	 * @return an Averysoft-compatible JDOM Element
	 */
	// Element getBlankAveryTemplateElement();
}
