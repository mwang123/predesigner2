/*
 * AveryGridLayout.java Created on Jul 30, 2007 by Bob Lee
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.project;

import org.jdom.Element;

/**
 * @author Bob Lee
 * Jul 30, 2007
 * AveryGridLayout - Java class reflects the Averysoft page.gridLayout XML element
 */
public class AveryGridLayout implements Averysoftable
{
  // all fields final except for master this object is not editable
  public String master;
  public double x;
  public final double y;
  public final double hpitch;
  public final double vpitch;
  private final int numberAcross;
  private final int numberDown;
  private boolean reorient;
  
  /**
   * create from Averysoft XML Element
   * @param element
   */
  AveryGridLayout(Element element)
  {
    master = element.getAttributeValue("master");
    x = Double.parseDouble(element.getAttributeValue("x"));
    y = Double.parseDouble(element.getAttributeValue("y"));
    hpitch = Double.parseDouble(element.getAttributeValue("hpitch"));
    vpitch = Double.parseDouble(element.getAttributeValue("vpitch")); 
    numberAcross = Integer.parseInt(element.getAttributeValue("numberAcross"));
    numberDown = Integer.parseInt(element.getAttributeValue("numberDown"));
    reorient = "true".equals(element.getAttributeValue("reorient"));
  }
  
  /**
   * construct from known values
   * @param master ID of masterpanel
   * @param x coordinate
   * @param y coordinate
   * @param hpitch horizontal pitch
   * @param vpitch vertical pitch
   * @param across number of panels across
   * @param down number of panels down
   * @param reorient if true, use landscape orientation
   */
  public AveryGridLayout(String master, double x, double y, double hpitch, double vpitch, int across, int down, boolean reorient)
  {
    this.master = master;
    this.x = x;
    this.y = y;
    this.hpitch = hpitch;
    this.vpitch = vpitch;
    this.numberAcross = across;
    this.numberDown = down;
    this.reorient = reorient;
  }

  /* (non-Javadoc)
   * @see com.avery.project.Averysoftable#getAverysoftElement()
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("gridLayout");
    element.setNamespace(AveryProject.getAverysoftNamespace());
    element.setAttribute("master", master);
    element.setAttribute("x", Double.toString(x));
    element.setAttribute("y", Double.toString(y));
    element.setAttribute("hpitch", Double.toString(hpitch));
    element.setAttribute("vpitch", Double.toString(vpitch));
    element.setAttribute("numberAcross", Integer.toString(numberAcross));
    element.setAttribute("numberDown", Integer.toString(numberDown));
    element.setAttribute("reorient", reorient? "true" : "false");
    return element;
  }
  
	public Element getAverysoftIBElement()
	{
    Element element = new Element("gridLayout");
    element.setNamespace(AveryProject.getAverysoftNamespace());
    element.setAttribute("master", master);	
		element.setAttribute("x", AveryProject.twipsToMM(x));
		element.setAttribute("y", AveryProject.twipsToMM(y));
		element.setAttribute("hpitch", AveryProject.twipsToMM(hpitch));
		element.setAttribute("vpitch", AveryProject.twipsToMM(vpitch));
    element.setAttribute("numberAcross", Integer.toString(numberAcross));
    element.setAttribute("numberDown", Integer.toString(numberDown));
    element.setAttribute("reorient", reorient? "true" : "false");
    return element;
	}

  public String getMaster()
  {
  	return master;
  }
  
  public void setMaster(String masterID)
  {
  	 master = masterID;
  }
  
  public double getX()
  {
  	return x;
  }
  
  public double getY()
  {
  	return y;
  }

  public double getHPitch()
  {
  	return hpitch;
  }
  
  public double getVPitch()
  {
  	return vpitch;
  }

  public int getNumberAcross()
  {
	  return numberAcross;
  }

  public int getNumberDown()
  {
	  return numberDown;
  }
  
	public void setReorient(boolean val)
  {
	  this.reorient = val;
  }

  public boolean getReorient()
  {
	  return reorient;
  }

  
}
