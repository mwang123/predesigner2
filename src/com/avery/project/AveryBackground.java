/**
 * Title: AveryImage
 * <p>
 * Description:
 * <p>
 * Copyright: Copyright (c)2000 Avery Dennison
 * <p>
 * Company: Avery Dennison
 * <p>
 * 
 * @author Bob Lee
 * @version 1.0
 */

package com.avery.project;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Hashtable;

import org.faceless.pdf2.PDFCanvas;
import org.faceless.pdf2.PDFImage;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFStyle;
import org.jdom.Element;

import com.avery.predesign.StyleSet;
import com.avery.project.RightRes.ImageFile;
import com.avery.project.shapes.AveryShape;

public class AveryBackground extends AveryImage implements Averysoftable
{
	private boolean bUnscaled = false;
	
  // default constructor
  public AveryBackground()
  {
  }

  // legacy constructor
  public AveryBackground(Hashtable pdfImageCache)
  {
    super(pdfImageCache);
  }

  protected AveryBackground(Element element) throws Exception
  {
    super(new Hashtable());

    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace()) && element.getName().equals("background"))
    {
      setSourceAndGallery(element.getAttributeValue("source"), element.getAttributeValue("gallery"));
      setBackgroundcolor(element.getAttributeValue("color"));
      readSuperAverysoftAttributes(element);

      Element descriptionElement = element.getChild("description", AveryProject.getAverysoftNamespace());
      if (descriptionElement != null)
      {
        setDescriptionObject(Description.makeBackgroundFieldDescription(descriptionElement));
      }
    }
    else if (!initFromAveryDtdElement(element))
    {
      throw new Exception("Not an Avery Background element");
    }
  }

  private boolean initFromAveryDtdElement(Element element) throws Exception
  {
    if (element.getName().equals("Avery.pf.background"))
    {
      setSourceAndGallery(element.getAttributeValue("src"), element.getAttributeValue("gallery"));
      setBackgroundcolor(element.getAttributeValue("backgroundcolor"));

      readSuperAttributes(element.getParent());

      return true;
    }

    return false;
  }

  // optional background color
  private Color backgroundcolor = Color.white; // new Color(0,0,0);

  /**
   * The matches() method returns <code>true</code> if the given field is an
   * AveryBackground with the same backgroundcolor and AveryImage attributes.
   * 
   * @param field
   *          the field to test against
   * @return <code>true</code> if the contents of the field match the contents
   *         of <code>this</code>, <code>false</code> otherwise
   */
  public boolean matches(AveryPanelField field)
  {
    if (field instanceof AveryBackground)
    {
      AveryBackground background = (AveryBackground) field;
      if (getBackgroundcolor().equals(background.getBackgroundcolor()))
      {
        return super.matches(field);
      }
    }
    return false;
  }

  /**
   * Creates a deep clone
   * 
   * @return deep clone
   */
  public AveryPanelField deepClone()
  {
    AveryBackground bim = new AveryBackground();
    copyPrivateData(bim);

    bim.setSourceAndGallery(source, gallery);
    bim.setBackgroundcolor(backgroundcolor);

    return bim;
  }

  // gets and sets
  public void setBackgroundcolor(java.awt.Color newBackgroundcolor)
  {
    backgroundcolor = newBackgroundcolor;
  }

  public void setUnscaled(boolean unscaled)
  {
  	unscaleImage(0.05);

  	bUnscaled = unscaled;
  }
  
  public void setBackgroundcolor(String str)
  {
    try
    {
      backgroundcolor = Color.decode(str);
    }
    catch (Exception e)
    {
      // on failure, color is set to the default black
      backgroundcolor = Color.white; // new Color(0,0,0);
      return;
    }
  }

  public java.awt.Color getBackgroundcolor()
  {
    return backgroundcolor;
  }

  public String dump()
  {
    String str = "<P>AveryBackground";
    str += super.dump();
    str += "<br>source = " + source;
    str += "<br>gallery = " + gallery;
    str += "<br>backgroundcolor = " + backgroundcolor.toString();
    return str;
  }

  protected void unscaleImage(double dScalar)
  {
    if (source.length() < 1 || bUnscaled)
    {
      return; // nothing to draw
    }
    
    // get highres source + dimensions, always entry 0 in the list
    ArrayList allresList = new RightRes().getAllresList(this);
    ImageFile highresImageFile = (ImageFile)allresList.get(0);

    // dimensions may be different due to aspect correction
    int scaledWidth = (int)(highresImageFile.getWidth()); //scaledImage.getWidth(null);
    int scaledHeight = (int)(highresImageFile.getHeight()); //scaledImage.getHeight(null);

    int x = (int) ((getPosition().getX() * dScalar) + 0.5);
    int y = (int) ((getPosition().getY() * dScalar) + 0.5);
    int width = (int) ((getWidth().doubleValue() * dScalar) + 0.5);
    int height = (int) ((getHeight().doubleValue() * dScalar) + 0.5);
    
    // reposition in center of changed dimension
    if (scaledWidth != width)
    {
      x += (width - scaledWidth) / 2;
    }
    if (scaledHeight != height)
    {
      y += (height - scaledHeight) / 2;
    }

    setPosition(new Point((int)((double)x / dScalar), (int)((double)y / dScalar)));
    setWidth(new Double(scaledWidth / dScalar));
    setHeight(new Double(scaledHeight / dScalar));

  } // end unscaleImage()
  
  // ////////////////////////////////////////////////////////////////////
  // implement draw() as declared in abstract superclass AveryPanelField
  public void draw(Graphics2D gr, double dScalar)
  {
    if (getSource().startsWith(IGNORE_PREFIX))
    {
      // no action (AveryPrint 4) - ignore this image
      return;
    }
    if (!getVisible())
    {
      return; // Averysoft XML ignore
    }

    if (getMask() != null)
    {
      getMask().clipImageMask(gr, dScalar);
    }
    drawOutlineElement(gr, dScalar);
    switch (imageType)
    {
      case TYPE_JPG:
      case TYPE_PNG:
      //case TYPE_TIF:
      case TYPE_BMP:
        drawImage(gr, dScalar);
        break;

      // should we fill here or should we call this TYPE_FILL
      // if we do solid color fill in the future, and make
      // TYPE_UNKNOWN do nothing like the PDF draw now is...
      case TYPE_GIF:
      	break;
      case TYPE_UNKNOWN:
        // fill solid color
        int x = (int) (getPosition().getX() * dScalar);
        int y = (int) (getPosition().getY() * dScalar);
        int width = (int) (getWidth().doubleValue() * dScalar);
        int height = (int) (getHeight().doubleValue() * dScalar);
        gr.setColor(getBackgroundcolor());
        gr.fillRect(x, y, width, height);
        //System.out.println("x, y, width, height=" + x + "," + y + "," + width + "," + height);
    }

    gr.setClip(null);
    if (getMask() != null && getMask().incrementIndex())
    {
      draw(gr, dScalar);
    }
  }

  // /////////////////////////////////////////////////////////////////////
  // draw() method for bitmapped images (JPEG, BMP etc.)
  protected void drawImage(Graphics2D gr, double dScalar)
  {
    if (source.length() < 1)
    {
      return; // nothing to draw
    }

    if (maintainAspect())
    {    
    	super.drawImageUnscaled(gr, dScalar);
 	
    	return;
    }
    
    int x = (int) (getPosition().getX() * dScalar + 0.5);
    int y = (int) (getPosition().getY() * dScalar + 0.5);
    int width = (int) (getWidth().doubleValue() * dScalar + 0.5);
    int height = (int) (getHeight().doubleValue() * dScalar + 0.5);

    Image scaledImage = getScaledImage(width, height);

    // potentially rotate image then render it
    RotateandRender(gr, dScalar, scaledImage, x, y, width, height);

  } // end drawImage()

  // ////////////////////////////////////////////////////////////////////////
  // implement addtoPDF() as declared in abstract superclass AveryPanelField
  public void addToPDF(PDFPage pdfPage, Double dPanelHeight, Hashtable imageCache) throws Exception
  {
    addOutlineToPDF(pdfPage, dPanelHeight);
    int canvasStartX = 0;
    int canvasStartY = 0;
    double heightCalibration = 0;
    if (getPosition().x < 0)
    {
      canvasStartX = (int) (getPosition().x / 20f);
    }
    
    if (getPosition().y < 0)
    {
      canvasStartY = (int) (getPosition().y / 20f);
      heightCalibration = - getPosition().y;
    }
    
    if (getPosition().x > 0)
    {
      canvasStartX = (int) (getPosition().x / 20f);
    }
    
    if (getPosition().y > 0)
    {
      canvasStartY = (int) (getPosition().y / 20f);
      heightCalibration = getPosition().y;
    }
   
		int areaSize = getAreaSize(pdfPage, canvasStartX, canvasStartY);
        
    PDFCanvas contentContainer = null;
    float opacity = getOpacity().floatValue();
    if (opacity < 0f || opacity >= 1f)  	
    	contentContainer = new PDFCanvas(areaSize, areaSize);
    else
    	contentContainer = new PDFCanvas(areaSize, areaSize, opacity);

    if (getMask() != null)
    {
      getMask().clipMask(contentContainer, canvasStartX, canvasStartY);
    }
    setPdfImageCache(imageCache);

    if (getSource().startsWith(IGNORE_PREFIX))
    {
      // no action (AveryPrint 4) - ignore this image
      return;
    }

    PDFImage pdfimage = null;

    switch (imageType)
    {
      case TYPE_JPG:
      case TYPE_PNG:
      //case TYPE_TIF:
      case TYPE_BMP:
      case TYPE_GIF:
        pdfimage = makePdfImage(contentContainer);
        if (pdfimage == null)
        {
          return;
        }
        break;

      case TYPE_WMF:
        // not implemented
        // / playWmfIntoPDF(pdfPage, offset, dPageHeight);
        return;

        // if we ever get around to solid color fills this will do it but
        // rotation is NOT being handled. TYPE_FILL is presently not defined
        // and this will need to happen in draw as well.
        // case TYPE_FILL:
        // fill solid color
        // addFillToPDF(pdfdoc, offset, dPageHeight);
        // return;

        // in these cases DO NOTHING
      case TYPE_UNKNOWN:
      default:
      	if (backgroundcolor != Color.WHITE && source.length() < 1)
      		break;
        return;
    }

    
    // get everything into PostScript point space
    float fHeight = getHeight().floatValue() / 20.0f;
    float fWidth = getWidth().floatValue() / 20.0f;
    float fTop = (dPanelHeight.intValue() - getPosition().y) / 20.0f;
    float fLeft = getPosition().x / 20.0f;

    // background color fill
    if (source.length() < 1)
    {
    	PDFStyle style = new PDFStyle();
      style.setFillColor(backgroundcolor);

    	contentContainer.setStyle(style);
    	contentContainer.drawRectangle(fLeft - canvasStartX, fTop - fHeight - canvasStartY, (int)fWidth, (int)fHeight);
      pdfPage.drawCanvas(contentContainer, canvasStartX, canvasStartY, areaSize + canvasStartX, areaSize + canvasStartY);
    	return;
    }
    // calculate pixel-to-point scalar
    // float fImageDPI = pdfimage.getDPIX() < 1 ? 1.0f :
    // (float)pdfimage.getDPIX();
    // float fPixelScalar = 72.0f / fImageDPI;

    // calculate width and height scalars used by pdfDoc
    // float fWidthScalar = fWidth / ((float)(pdfimage.getWidth()) *
    // fPixelScalar);
    // float fHeightScalar = fHeight / ((float)(pdfimage.getHeight()) *
    // fPixelScalar);

    // field rotation
    double fieldRotation = getRotation().doubleValue();

    // to rotate or not to rotate that is the question
    if (fieldRotation != 0.0)
    {
      // save old graphics transform/color state
      contentContainer.save();
      // rotate the field
      transformPDFField(contentContainer, new Double(dPanelHeight.doubleValue() + heightCalibration), fLeft - canvasStartX, fTop
          - fHeight - canvasStartY, fieldRotation);
      // all calculations are complete - add the image to the page in the PDF
      // the lower left corner is at 0,0 because of the transform
      contentContainer.drawImage(pdfimage, 0f, 0f, fWidth, fHeight);
      // restore graphics state
      contentContainer.restore();
    }
    else
    {
      // all calculations are complete - add the image to the page in the PDF
      contentContainer.drawImage(pdfimage, fLeft - canvasStartX, fTop - fHeight - canvasStartY,
          fLeft + fWidth - canvasStartX, fTop - canvasStartY);
    }
    pdfPage.drawCanvas(contentContainer, canvasStartX, canvasStartY, areaSize + canvasStartX, areaSize + canvasStartY);
    if (getMask() != null && getMask().incrementIndex())
    {
      addToPDF(pdfPage, dPanelHeight, imageCache);
    }
    // for debugging, show outline of field
    // addOutlineToPDF(pdfdoc, offset, dPageHeight);
  } // end addToPDF()

  // ////////////////////////////////////////////////////////////////////////
  // implement addFillToPDF()
  /*
   * void addFillToPDF(pdfDoc pdfdoc, Point offset, Double dPageHeight) throws
   * Exception { // get everything into PostScript point space float fHeight =
   * getHeight().floatValue() / 20.0f; float fWidth = getWidth().floatValue() /
   * 20.0f; float fTop = (float)(dPageHeight.intValue() - (offset.y +
   * getPosition().y)) / 20.0f; float fLeft = (float)(offset.x +
   * getPosition().x) / 20.0f; // set fill color after scale components between
   * 0.0 and 1.0 float red = (float)backgroundcolor.getRed() / 255.f; float
   * green = (float)backgroundcolor.getGreen() / 255.f; float blue =
   * (float)backgroundcolor.getBlue() / 255.f; if (red > 1.f) red = 1.f; if
   * (green > 1.f) green = 1.f; if (blue > 1.f) blue = 1.f;
   * pdfdoc.setrgbcolor(red, green, blue); // trace outline path then close and
   * fill it pdfdoc.setlinewidth(1.0f); pdfdoc.moveto(fLeft, fTop);
   * pdfdoc.lineto(fLeft + fWidth - 1.0f, fTop); pdfdoc.lineto(fLeft + fWidth -
   * 1.0f, fTop - fHeight + 1.0f); pdfdoc.lineto(fLeft, fTop - fHeight + 1.0f);
   * pdfdoc.lineto(fLeft, fTop); pdfdoc.closepath(); pdfdoc.fill(); } // end
   * addFillToPDF()
   */

  /**
   * Determine whether this object should maintain its aspect ratio when it is
   * resized.
   * 
   * @return <code>false</code> always for the AveryBackground class
   */
  boolean maintainAspect()
  {
    return bUnscaled; //false;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAveryElement()
  {
    Element element = super.getParentAveryElement();
    Element content = new Element("Avery.pf.background");
    content.setAttribute("src", getSource());
    content.setAttribute("gallery", getGallery());

    element.addContent(content);
    return element;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("background");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    element.setAttribute("color", "0x" + Integer.toHexString(getBackgroundcolor().getRGB()).substring(2));

    super.makeAverysoftElement(element);

    return element;
  }

  public Element getAverysoftIBElement()
  {
    Element element = new Element("background");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    element.setAttribute("color", "0x" + Integer.toHexString(getBackgroundcolor().getRGB()).substring(2));

    super.makeAverysoftIBElement(element);

    return element;
  }

  /**
   * A background swaps its width and height when the panel goes from portrait
   * to landscape
   */
  void reorient()
  {
    Double w = getWidth();
    Double h = getHeight();
    setWidth(h);
    setHeight(w);
  }

  protected void addDefaultDescriptionObject()
  {
    setDescriptionObject(Description.makeBackgroundFieldDescription());
  }

  void replaceStyle(StyleSet original, StyleSet replacement)
  {
    super.replaceStyle(original, replacement);

    if (original.color1.equals(backgroundcolor))
    {
      backgroundcolor = replacement.color1;
    }
    else if (original.color2.equals(backgroundcolor))
    {
      backgroundcolor = replacement.color2;
    }
    else if (original.color3.equals(backgroundcolor))
    {
      backgroundcolor = replacement.color3;
    }
  }
}
