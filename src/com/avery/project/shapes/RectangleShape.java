package com.avery.project.shapes;

import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

/*
 * File: RectangleShape.java
 * Created: Nov 11, 2004 by Bob Lee
 * Copyright (c) Avery Dennison Corporation, All Rights Reserved
 */

/**
 * com.avery.project.shapes
 * RectangleShape 
 */
public class RectangleShape extends AveryShape
{
	private double cornerRadius;
	
	public RectangleShape(double width, double height, double radius)
	{
		super(width, height);
		cornerRadius = radius;
	}

	/**
	 * @see com.avery.project.shapes.AveryShape#scaledGraphicsPath(double)
	 */
	public GeneralPath scaledGraphicsPath(double scale)
	{
		Rectangle2D scaledRect = scaledBounds(scale);
		double scaledRadius = getCornerRadius() * scale;

		if (scaledRadius < 1)
		{
			return new GeneralPath(scaledRect);
		}
		else // the scaled rect has rounded corners
		{
			RoundRectangle2D roundrect = new RoundRectangle2D.Double(
				scaledRect.getX(), scaledRect.getY(),
				scaledRect.getWidth(), scaledRect.getHeight(), 
				scaledRadius, scaledRadius);
				
			return new GeneralPath(roundrect);
		}
	}

	/**
	 * @see com.avery.project.shapes.AveryShape#scaledGraphicsPath(double, double)
	 */
	public GeneralPath scaledGraphicsPath(double xScale, double yScale)
	{
		Rectangle2D scaledRect = scaledBounds(xScale, yScale);
		double scaledXRadius = getCornerRadius() * xScale;
		double scaledYRadius = getCornerRadius() * yScale;

		if (scaledXRadius < 1 && scaledYRadius < 1)
		{
			return new GeneralPath(scaledRect);
		}
		else // the scaled rect has rounded corners
		{
			RoundRectangle2D roundrect = new RoundRectangle2D.Double(
				scaledRect.getX(), scaledRect.getY(),
				scaledRect.getWidth(), scaledRect.getHeight(), 
				scaledXRadius, scaledYRadius);
				
			return new GeneralPath(roundrect);
		}
	}

	/**
	 * @see com.avery.project.shapes.AveryShape#getCornerRadius()
	 */
	public double getCornerRadius()
	{
		return cornerRadius;
	}
	
	/* (non-Javadoc)
	 * @see com.avery.miwok.shapes.AveryShape#getShapeString()
	 */
	public String getShapeString()
	{
		return "rect";
	}
	

	/* (non-Javadoc)
	 * @see com.avery.miwok.shapes.AveryShape#getPolypointsElement()
	 */
	public org.jdom.Element getPolypointsElement()
	{
		return null;
	}
  
  /**
   * resizes the shape independently in both directions
   * @param scalex
   * @param scaley
   */
  public void scale(double scalex, double scaley)
  {
    super.scale(scalex, scaley);
    cornerRadius *= (scalex + scaley) / 2.0;
  }
}
