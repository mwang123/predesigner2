package com.avery.project.shapes;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

import org.jdom.Element;

import com.avery.Averysoft;
import com.avery.project.AveryProject;
import com.avery.utils.AveryException;

/*
 * File: AveryShape.java
 * Copyright:   Copyright (c) 2013-2015 Avery Products Corp. All Rights Reserved.
 * @author      Bob Lee, Brad Nelson
 */

/**
 * com.avery.miwok.shapes.AveryShape
 * A public abstract superclass for the several derived shapes.
 * Derived shapes have package scope. This abstract class is the
 * only public entry point for accessing AveryShape objects.
 * A factory method is provided to construct the derived objects.
 * <p>It's worth noting that the position of an AveryShape is always
 * set internally to 0,0.  By design, positioning on a target drawing
 * surface is performed by the client, and is beyond the scope of the
 * AveryShape object.</p>
 */
abstract public class AveryShape
{
	private Rectangle2D bounds;

	public AveryShape()
	{
		
	}
	
	/**
	 * protected constructor sets internal rect at 0,0 offset
	 * @param width - width of bounds in twips
	 * @param height - height of bounds in twips
	 */
	protected AveryShape(double width, double height)
	{
		bounds = new Rectangle2D.Double(0, 0, width, height);
	}

	/**
	 * generates a scaled bounding rect of the shape
	 * @param scale - multiplier from internal units of 1440 dpi
	 * @return a rectangle
	 */
	protected Rectangle2D scaledBounds(double scale)
	{
		double x = bounds.getX() * scale;
		double y = bounds.getY() * scale;

		// Subtract half a pixel so that it doesn't round UP when drawing
		// and go beyond the bounds of the context.
		double w = Math.floor(bounds.getWidth() * scale - 0.5);
		// Subtract one and a half pixels so that it doesn't round UP when drawing
		// and go beyond the bounds of the context.
		double h = Math.floor(bounds.getHeight() * scale - 1.5);

		return new Rectangle2D.Double(x, y, w, h);
	}

	/**
	 * generates a scaled bounding rect of the shape
	 * @param scale - multiplier from internal units of 1440 dpi
	 * @return a rectangle
	 */
	protected Rectangle2D scaledBounds(double xScale, double yScale)
	{
		double x = bounds.getX() * xScale;
		double y = bounds.getY() * yScale;

		// Subtract half a pixel so that it doesn't round UP when drawing
		// and go beyond the bounds of the context.
		double w = Math.floor(bounds.getWidth() * xScale - 0.5);
		// Subtract one and a half pixels so that it doesn't round UP when drawing
		// and go beyond the bounds of the context.
		double h = Math.floor(bounds.getHeight() * yScale - 1.5);

		return new Rectangle2D.Double(x, y, w, h);
	}
	
	public double getWidth()
	{
		return bounds.getWidth();
	}

	public double getHeight()
	{
		return bounds.getHeight();
	}
	
	public void setBounds(double width, double height)
	{
		bounds = new Rectangle2D.Double(0, 0, width, height);
	}

	/**
	 * abstract method generates a scaled path for drawing
	 * @param scale - scaling factor for resulting path, from twips (1440 dpi)
	 * @return a path that can be drawn
	 */
	abstract public GeneralPath scaledGraphicsPath(double scale);
	
	/**
	 * abstract method generates a scaled path for drawing
	 * @param scale - scaling factor for resulting path, from twips (1440 dpi)
	 * @return a path that can be drawn
	 */
	abstract public GeneralPath scaledGraphicsPath(double xScale, double yScale);

	/**
	 * If the derived shape is a rounded rectangle, this method returns the corner radius.
	 * Otherwise it returns 0.
	 * @return the cornerRadius in twips, or 0.0
	 */
	abstract public double getCornerRadius();

	/**
	 * Each derived shape type is represented by a short string in our XML files
	 * @return "rect", "ellipse", or "polygon"
	 */
	abstract public String getShapeString();

	/**
	 * an Averysoft XML output aide
	 * @return if the shape is a polygon, this returns a detached averysoft.xsd
	 * compatible polypoints Element.  Otherwise, it returns <code>null</code>.
	 */
	abstract public Element getPolypointsElement();
	
	public boolean allowReorientation()
	{
		boolean allow = ("polygon".equals(getShapeString()));
		if (allow)
			return allow;

		return getWidth() != getHeight();
	}
	
	public void reorient()
	{
		if (allowReorientation())
		{
			bounds = new Rectangle2D.Double(0, 0, bounds.getHeight(), bounds.getWidth());
		}
	}

	public void reorient(boolean clockwise)
	{
		if (getShapeString().equals("polygon"))
		{
			((PolygonShape)this).orient(clockwise);
		}
		bounds = new Rectangle2D.Double(0, 0, bounds.getHeight(), bounds.getWidth());
	}

	/**
	 * given an Avery XML element that includes shape data, this static factory method
	 * constructs the appropriate AveryShape type
	 * @param element - typically a masterpanel or cutout element
	 * @return a shape
	 * @throws AveryException
	 */
	static public AveryShape manufactureShape(Element element)
	throws AveryException
	{
		String shapeString = element.getAttributeValue("shape", "rect");
		double width = java.lang.Double.parseDouble(element.getAttributeValue("width"));
		double height = java.lang.Double.parseDouble(element.getAttributeValue("height"));

		if (shapeString.equals("rect"))
		{
			// check for cornerRadius
			double cornerRadius = 0;
			String str = (element.getNamespaceURI().equals(Averysoft.getAverysoftURI()))
					? element.getAttributeValue("cornerRadius")		// averysoft.xsd form
					: element.getAttributeValue("cornerradius");	// Avery.dtd form

			if (str != null && str.length() > 0)
			{
				cornerRadius = java.lang.Double.parseDouble(str);
			}

			return new RectangleShape(width, height, cornerRadius);
		}
		else if (shapeString.equals("ellipse"))
		{
			return new EllipseShape(width, height);
		}
		else if (shapeString.equals("polygon"))
		{
			Element polypointsElement = element.getChild("polypoints", AveryProject.getAverysoftNamespace());
			if (polypointsElement != null)
			{
				return new PolygonShape(width, height, polypointsElement);
			}
		}

		// something is drastically wrong with this element
		throw new AveryException("Element " + element.getName() + " did not contain valid shape information");
	}
  
  /**
   * makes a new AveryShape
   * @param type - "rect", "ellipse" or "polygon"
   * @param width - in TWIPs
   * @param height - in TWIPs
   * @param cornerRadius - for rounded rects, in TWIPs
   * @param polypoints - used to construct polygons
   * @return a newly constructed AveryShape
   * @throws AveryException if something went wrong
   */
  static public AveryShape manufactureShape(String type, double width, double height, double cornerRadius, Element polypoints)
  throws AveryException
  {
    if ("rect".equals(type))
    {
      return new RectangleShape(width, height, cornerRadius);
    }
    else if ("ellipse".equals(type))
    {
      return new EllipseShape(width, height);
    }
    else if ("polygon".equals(type))
    {
      return new PolygonShape(width, height, polypoints);
    }
    
    // something is drastically wrong
    throw new AveryException("call to AveryShape.manufactureShape failed");
  }
  
  /**
   * resizes the shape independently in both directions
   * @param scalex
   * @param scaley
   */
  public void scale(double scalex, double scaley)
  {
    double x = bounds.getX() * scalex;
    double y = bounds.getY() * scaley;
    double w = bounds.getWidth() * scalex;
    double h = bounds.getHeight() * scaley;
    bounds = new Rectangle2D.Double(x, y, w, h);
  }
}
