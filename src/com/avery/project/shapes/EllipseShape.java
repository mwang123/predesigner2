package com.avery.project.shapes;

import java.awt.geom.*;

/*
 * File: EllipseShape.java
 * Created: Nov 11, 2004 by Bob Lee
 * Copyright (c) Avery Dennison Corporation, All Rights Reserved
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * com.avery.miwok.shapes
 * EllipseShape 
 */
class EllipseShape extends AveryShape
{
	/**
	 * @param width
	 * @param height
	 */
	EllipseShape(double width, double height)
	{
		super(width, height);
	}

	/* (non-Javadoc)
	 * @see com.avery.miwok.shapes.AveryShape#scaledGraphicsPath(double)
	 */
	public GeneralPath scaledGraphicsPath(double scale)
	{
		Rectangle2D scaledRect = scaledBounds(scale);

		Ellipse2D ellipse = new Ellipse2D.Double(
				scaledRect.getX(), scaledRect.getY(),
				scaledRect.getWidth(), scaledRect.getHeight());
				
		return new GeneralPath(ellipse);
	}

	/* (non-Javadoc)
	 * @see com.avery.miwok.shapes.AveryShape#scaledGraphicsPath(double, double)
	 */
	public GeneralPath scaledGraphicsPath(double xScale, double yScale)
	{
		Rectangle2D scaledRect = this.scaledBounds(xScale, yScale);

		Ellipse2D ellipse = new Ellipse2D.Double(
				scaledRect.getX(), scaledRect.getY(),
				scaledRect.getWidth(), scaledRect.getHeight());
				
		return new GeneralPath(ellipse);
	}

	/* (non-Javadoc)
	 * @see com.avery.miwok.shapes.AveryShape#getCornerRadius()
	 */
	public double getCornerRadius()
	{
		return 0.0;
	}
	
	/* (non-Javadoc)
	 * @see com.avery.miwok.shapes.AveryShape#getShapeString()
	 */
	public String getShapeString()
	{
		return "ellipse";
	}
	
	/* (non-Javadoc)
	 * @see com.avery.miwok.shapes.AveryShape#getPolypointsElement()
	 */
	public org.jdom.Element getPolypointsElement()
	{
		return null;
	}
}
