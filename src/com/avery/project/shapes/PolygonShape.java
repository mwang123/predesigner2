package com.avery.project.shapes;

import java.awt.BasicStroke;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;
import java.io.BufferedWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.jdom.Attribute;
import org.jdom.Element;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanel;

/*
 * File: PolygonShape.java
 * Copyright:   Copyright (c) 2013-2015 Avery Products Corp. All Rights Reserved.
 * @author      Bob Lee, Brad Nelson
 */

/**
 * com.avery.miwok.shapes PolygonShape
 */
public class PolygonShape extends AveryShape
{
  private Element polypointsElement;
  private GeneralPath path;
  private GeneralPath bleedPath;
  private List elements = null;
  
  private Point2D minPosition = new Point2D.Double(999999999, 999999999);
  private Point2D maxPosition = new Point2D.Double(0, 0);

  //private boolean pointCWOrder = true;
  private boolean hasCurves = false;
  
  private float qxold = -10000.0f;
  private float qyold = -10000.0f;
  private float cxold = -10000.0f;
  private float cyold = -10000.0f;

  public class PolyPoint
  {
    public float x = 0;
    public float y = 0;
    public boolean move = false;

    public PolyPoint(float x, float y, boolean move)
    {
      this.x = x;
      this.y = y;
      this.move = move;
    }
  }
  
  public class PolyQuad
  {
    public float x1 = 0;
    public float y1 = 0;
    public float x2 = 0;
    public float y2 = 0;
    public float x3 = 0;
    public float y3 = 0;
    public boolean bleed = true;

    public PolyQuad(float x1, float y1, float x2, float y2, float x3, float y3)
    {
      this.x1 = x1;
      this.y1 = y1;
      this.x2 = x2;
      this.y2 = y2;
      this.x3 = x3;
      this.y3 = y3;
    }
  }
  
  public class PolyCurve
  {
    public float x1 = 0;
    public float y1 = 0;
    public float x2 = 0;
    public float y2 = 0;
    public float x3 = 0;
    public float y3 = 0;
    public float x4 = 0;
    public float y4 = 0;

    public PolyCurve(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
    {
      this.x1 = x1;
      this.y1 = y1;
      this.x2 = x2;
      this.y2 = y2;
      this.x3 = x3;
      this.y3 = y3;
      this.x4 = x4;
      this.y4 = y4;
    }
  }
  

  public class PolyArc
  {
    public float x1 = 0;
    public float y1 = 0;
    public float x2 = 0;
    public float y2 = 0;
    public float start = 0;
    public float extent = 0;
 
    public PolyArc(float x1, float y1, float x2, float y2, float start, float extent)
	  {
	    this.x1 = x1;
	    this.y1 = y1;
	    this.x2 = x2;
	    this.y2 = y2;
	    this.start = start;
	    this.extent = extent;	
	  }
  }

  public PolygonShape()
  {
  }
  
  /**
   * @param width
   * @param height
   */
  public PolygonShape(double width, double height, Element element)
  {
    super(width, height);
    elements = new ArrayList();
    polypointsElement = (Element) element.clone(); // someone might want it
    // back someday

    ArrayList points = new ArrayList();
    
    // read the children again to construct the path
    Iterator i = element.getChildren().iterator();
    while (i.hasNext())
    {
      Element child = (Element) i.next();
      if (child.getName().equals("point"))
      {
        points.add(addPoint(child));
      }
      else if (child.getName().equals("bezier"))
      {
      	hasCurves = true;
        points.add(addBezier(child));
      }
      else if (child.getName().equals("arc"))
      {
      	hasCurves = true;
        points.add(addArc(child));
      }
    }

    if (!hasCurves)
    {
    	double trueWidth = maxPosition.getX() - minPosition.getX();
    	double trueHeight = maxPosition.getY() - minPosition.getY();
    	setBounds(trueWidth, trueHeight);
    }
    
    // calculate bleed path using areas
    constructBleedPath();

    //calculatePointOrder(points, (maxPosition.getX() + minPosition.getX()) / 2, (maxPosition.getY() + minPosition.getY()) / 2);
  }
  
  // creates bleed path using internal units of twips
  private void constructBleedPath()
  {
  	// start with polygon panel outline
    Area area51 = new Area(path);
    
    // create a stroke twice the bleed dimension in twips
    float strokeWidth = 2 * AveryPanel.BLEED_DIMENSION * 20f;
    BasicStroke stroke = new BasicStroke(strokeWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    //masterShape.closePath();
    // this is the magic - creates a path of the panel outline sized to twice the bleed because stroke
    // is half inside the panel boundary and half outside thus outside part is equal to the bleed
    Shape strokedShape = stroke.createStrokedShape(path);
    GeneralPath strokedPath = new GeneralPath(strokedShape);
  	Area area2 = new Area(strokedPath);
  	// area51 becomes polygon outline area plus the stroked area
  	area51.add(area2);
  	// bleed path is constructed from the combined areas
  	bleedPath = new GeneralPath(area51);
  }

  /*private void calculatePointOrder(ArrayList points, double midX, double midY)
  {
  	// sum angles of each point around center
  	double angleIncreaseCount = 0;
  	double angleDecreaseCount = 0;
  	
  	double previousAngle = 0.0;  // this value is never used but it has to be initialized
  	
  	for (int i = 0; i < points.size(); i++)
  	{
  		Point point = (Point)points.get(i);
  		float angle = (float) Math.toDegrees(Math.atan2(point.y - midY, point.x - midX));
  		if (angle < 0)
  			angle += 360;
  		if (i != 0)
  		{
  			if (angle > previousAngle)
  				angleIncreaseCount++;
  			if (previousAngle > angle)
  				angleDecreaseCount++;
  		}
  		//System.out.println(angleDecreaseCount + "," + angleIncreaseCount);
  		previousAngle = angle;
  	}
  	//System.out.println(angleSum);
  	if (angleDecreaseCount > angleIncreaseCount)
  		pointCWOrder = false;
  }
  
  public boolean hasCurves()
  {
  	return hasCurves;
  }*/
  
  public PolygonShape clonePolygonShape()
  {
  	PolygonShape shape = new PolygonShape();
  	shape.setBounds(getWidth(), getHeight());
  	shape.setPolypointsElement(getPolypointsElementClone());
  	shape.setPath(path);
  	shape.setMinPosition(minPosition);
  	shape.setMaxPosition(maxPosition);
  	shape.constructBleedPath();

  	return shape;
  }

  public void setPath(GeneralPath p)
  {
  	path = p;
  }
  
  // true = CW, false = CCW
  /*public boolean getRotationSense()
  {
  	return pointCWOrder;
  }*/
  
  /**
   * adds a point extracted from an averysoft point XML element to the path
   * 
   * @param element -
   *          the element to read for the point coordinates
   */
  private Point addPoint(Element element)
  {
    float x = java.lang.Float.parseFloat(element.getAttributeValue("x"));
    float y = java.lang.Float.parseFloat(element.getAttributeValue("y"));
    boolean move = false;
    //boolean bleed = true;
    String sMove = element.getAttributeValue("move");
    if (sMove != null && sMove.equals("true"))
    	move = true;
    /*String sBleed = element.getAttributeValue("bleed");
    if (sBleed != null && sBleed.equals("false"))
    	bleed = false;*/

    elements.add(new PolyPoint(x, y, move));
    
    if (path == null)
    {
      path = new GeneralPath();
      path.moveTo(x, y);
    }
    else if (move == true)
    {
      path.moveTo(x, y);
    }
    else
    {
      path.lineTo(x, y);
    }

    double newMinX = minPosition.getX();
    double newMinY = minPosition.getY();
    if (newMinX > x || newMinY > y)
    {
    	if (newMinX > x)
    		newMinX = x;
    	if (newMinY > y)
    		newMinY = y;
    	
    	minPosition = new Point2D.Double(newMinX, newMinY);
    }
    
    double newMaxX = maxPosition.getX();
    double newMaxY = maxPosition.getY();
    if (newMaxX < x || newMaxY < y)
    {
    	if (newMaxX < x)
    		newMaxX = x;
    	if (newMaxY < y)
    		newMaxY = y;
    	
    	maxPosition = new Point2D.Double(newMaxX, newMaxY);
    }

    Point xyPoint = new Point((int)x, (int)y);
    return xyPoint;
  }

  /**
   * adds a quadratic bezier extracted from an averysoft bezier XML element to
   * the path
   * 
   * @param element -
   *          the element to read for the point coordinates
   */
  private Point addBezier(Element element)
  {
    float x1 = java.lang.Float.parseFloat(element.getAttributeValue("x1"));
    float y1 = java.lang.Float.parseFloat(element.getAttributeValue("y1"));
    float x2 = java.lang.Float.parseFloat(element.getAttributeValue("x2"));
    float y2 = java.lang.Float.parseFloat(element.getAttributeValue("y2"));
    float x3 = java.lang.Float.parseFloat(element.getAttributeValue("x3"));
    float y3 = java.lang.Float.parseFloat(element.getAttributeValue("y3"));
    /*boolean bleed = true;
    String sBleed = element.getAttributeValue("bleed");
    if (sBleed != null && sBleed.equals("false"))
    	bleed = false;*/

    if (path == null)
    {
      path = new GeneralPath();
      path.moveTo(x1, y1);
    }
    else
    {
      path.lineTo(x1, y1);
    }
    
    double newMinX = minPosition.getX();
    double newMinY = minPosition.getY();
    if (newMinX > x1 || newMinY > y1)
    {
    	if (newMinX > x1)
    		newMinX = x1;
    	if (newMinY > y1)
    		newMinY = y1;
    	
    	minPosition = new Point2D.Double(newMinX, newMinY);
    }
    double newMaxX = maxPosition.getX();
    double newMaxY = maxPosition.getY();
    if (newMaxX < x1 || newMaxY < y1)
    {
    	if (newMaxX < x1)
    		newMaxX = x1;
    	if (newMaxY < y1)
    		newMaxY = y1;
    	
    	maxPosition = new Point2D.Double(newMaxX, newMaxY);
    }

    if (element.getAttributeValue("x4") != null)
    {
      float x4 = java.lang.Float.parseFloat(element.getAttributeValue("x4"));
      float y4 = java.lang.Float.parseFloat(element.getAttributeValue("y4"));

      elements.add(new PolyCurve(x1, y1, x2, y2, x3, y3, x4, y4));
      
      path.curveTo(x2, y2, x3, y3, x4, y4);
           
      newMinX = minPosition.getX();
      newMinY = minPosition.getY();
      if (newMinX > x4 || newMinY > y4)
      {
      	if (newMinX > x4)
      		newMinX = x4;
      	if (newMinY > y4)
      		newMinY = y4;
      	
      	minPosition = new Point2D.Double(newMinX, newMinY);
      }
      newMaxX = maxPosition.getX();
      newMaxY = maxPosition.getY();
      if (newMaxX < x4 || newMaxY < y4)
      {
      	if (newMaxX < x4)
      		newMaxX = x4;
      	if (newMaxY < y4)
      		newMaxY = y4;
      	
      	maxPosition = new Point2D.Double(newMaxX, newMaxY);
      }
    }
    else
    {
    	// convert quad to cubic because PDF does not render quadratic bezier
    	float[] cubic = QuadToCubic(x1, y1, x2, y2, x3, y3);
    	
    	elements.add(new PolyQuad(x1, y1, x2, y2, x3, y3));
    
    	// using quadTo for a test of rendering bleed polygons.
    	// restore to curveTo when making PDF files
      // for the bleed outline code disabled currently (written 7/2015) need to use quadTo not curveTo
    	// if turn it on make another method or set a flag on this one to decide
    	//path.quadTo(x2, y2, x3, y3);
      
    	path.curveTo(cubic[2], cubic[3], cubic[4], cubic[5], cubic[6], cubic[7]);
      
      newMinX = minPosition.getX();
      newMinY = minPosition.getY();
      if (newMinX > x3 || newMinY > y3)
      {
      	if (newMinX > x3)
      		newMinX = x3;
      	if (newMinY > y3)
      		newMinY = y3;
      	
      	minPosition = new Point2D.Double(newMinX, newMinY);
      }
      newMaxX = maxPosition.getX();
      newMaxY = maxPosition.getY();
      if (newMaxX < x3 || newMaxY < y3)
      {
      	if (newMaxX < x3)
      		newMaxX = x3;
      	if (newMaxY < y3)
      		newMaxY = y3;
      	
      	maxPosition = new Point2D.Double(newMaxX, newMaxY);
      }
    }
    Point xyPoint = new Point((int)x2, (int)y2);
    return xyPoint;
  }

  private Point addArc(Element element)
  {
    float x1 = java.lang.Float.parseFloat(element.getAttributeValue("x1"));
    float y1 = java.lang.Float.parseFloat(element.getAttributeValue("y1"));
    float x2 = java.lang.Float.parseFloat(element.getAttributeValue("x2"));
    float y2 = java.lang.Float.parseFloat(element.getAttributeValue("y2"));
    float start = java.lang.Float.parseFloat(element.getAttributeValue("start"));
    float extent = java.lang.Float.parseFloat(element.getAttributeValue("extent"));
      
    elements.add(new PolyArc(x1, y1, x2, y2, start, extent));
    
    if (path == null)
    {
      path = new GeneralPath();
      path.moveTo(x1, y1);
    }
    else
    {
    	Rectangle2D rect = new Rectangle2D.Double(x1, y1, x2 - x1, y2 - y1);
    	GeneralPath arcPath = new GeneralPath(new Arc2D.Double(rect, start, extent, Arc2D.OPEN));
    	PathIterator pi = arcPath.getPathIterator(new AffineTransform());
      path.append(pi,  true);
    }

    /*if (bleedPath == null)
    {
    	bleedPath = new GeneralPath();
    	bleedPath.moveTo(x, y);
  		//System.out.println("M: " + x + "," + y);
    }
    else if (move == true)
    {
    	bleedPath.moveTo(x, y);
  		//System.out.println("M: " + x + "," + y);
    }
    else //if (bleed == true)
    {
    	bleedPath.lineTo(x, y);
  		//System.out.println("L: " + x + "," + y);
    }*/

    double newMinX = minPosition.getX();
    double newMinY = minPosition.getY();
    if (newMinX > x1 || newMinY > y1)
    {
    	if (newMinX > x1)
    		newMinX = x1;
    	if (newMinY > y1)
    		newMinY = y1;
    	
    	minPosition = new Point2D.Double(newMinX, newMinY);
    }
    
    double newMaxX = maxPosition.getX();
    double newMaxY = maxPosition.getY();
    if (newMaxX < x2 || newMaxY < y2)
    {
    	if (newMaxX < x2)
    		newMaxX = x2;
    	if (newMaxY < y2)
    		newMaxY = y2;
    	
    	maxPosition = new Point2D.Double(newMaxX, newMaxY);
    }

    Point xyPoint = new Point((int)x2, (int)y2);
    return xyPoint;
  }

/*  private void discretizeQuadraticBleed(float x1, float y1, float x2, float y2, float x3, float y3)
  {
  	for (float t = 0.1f; t < 1.0f; t += 0.1f)
  	{
  		float k = 1 - t;
  		float ks = k * k;
  		float twok = 2 * k;
  		float ts = t * t;
  		float qx = ks * x1 + twok * t * x2 + ts * x3;
  		float qy = ks * y1 + twok * t * y2 + ts * y3;
  		  		
  		if (Math.abs(qx - qxold) > 20.0f || Math.abs(qy - qyold) > 20.0f)
  		{
  			bleedPath.lineTo(qx, qy);
  			System.out.println("Q: " + qx + "," + qy);
  		}
  		qxold = qx;
  		qyold = qy;
  	}
  }
  
  private void discretizeCubicBleed(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
  {
  	float start = 0.06f;
  	float increment = 0.08f;
  	float dx = Math.abs(x4-x1);
  	float dy = Math.abs(y4-y1);
  	if (dx < 100f && dy < 100f)
  	{
  		start = 0.1f;
  		increment = 0.1f;
  	}
  	if (dx < 50f && dy < 50f)
  	{
  		start = 0.2f;
  		increment = 0.2f;
  	}
  	if (dx < 25f && dy < 25f)
  	{
  		cxold = x4;
  		cyold = y4;
  		return;
  	}
  	for (float t = start; t < 1.0f; t += increment)
  	{
  		float k = 1 - t;
  		float ks = k * k;
  		float kc = k * ks;
  		float threek = 3 * k;
  		float threeks = 3 * ks;
  		float ts = t * t;
  		float tc = t * ts;
  		float cx = kc * x1 + threeks * t * x2 + threek * ts * x3 + tc * x4;
  		float cy = kc * y1 + threeks * t * y2 + threek * ts * y3 + tc * y4;
  		  		
			//System.out.println("dx, dy= " + Math.abs(x4-x1) + "," + Math.abs(y4-y1));
  		if (Math.abs(cx - cxold) > 20.0f || Math.abs(cy - cyold) > 20.0f)
  		{
  			bleedPath.lineTo(cx, cy);
  			//System.out.println("C: " + cx + "," + cy);
  		}
  		cxold = cx;
  		cyold = cy;
  	}
  }*/
  
  /*
   * (non-Javadoc)
   * 
   * @see com.avery.miwok.shapes.AveryShape#scaledGraphicsPath(double)
   */
  public GeneralPath scaledGraphicsPath(double scale)
  {
  	GeneralPath gp = null;
  	try
  	{
  		AffineTransform at = new AffineTransform();
  		at.setToScale(scale, scale);
  		Shape s = path.createTransformedShape(at);
  		gp = new GeneralPath(s);
  	}
  	catch(Exception ex)
  	{
  		System.err.print(ex.getStackTrace());
  		System.err.print(ex.getMessage());	
  	}
    return gp;
  }
  
  /*
   * (non-Javadoc)
   * 
   * @see com.avery.miwok.shapes.AveryShape#scaledGraphicsPath(double, double)
   */
  public GeneralPath scaledGraphicsPath(double xScale, double yScale)
  {
  	GeneralPath gp = null;
  	try
  	{
  		AffineTransform at = new AffineTransform();
  		at.setToScale(xScale, yScale);
  		Shape s = path.createTransformedShape(at);
  		gp = new GeneralPath(s);
  	}
  	catch(Exception ex)
  	{
  		System.err.print(ex.getStackTrace());
  		System.err.print(ex.getMessage());	
  	}
    return gp;
  }

  // returns scaled bleed path 
  public GeneralPath scaledBleedGraphicsPath(double scale)
  {
  	GeneralPath scaledBleedPath = null;
  	
  	try
  	{
  		AffineTransform at = new AffineTransform();
  		at.setToScale(scale, scale);
  		// scale bleed path
  		Shape s = bleedPath.createTransformedShape(at);
  		scaledBleedPath = new GeneralPath(s);
  	}
  	catch(Exception ex)
  	{
  		System.err.print(ex.getStackTrace());
  		System.err.print(ex.getMessage());	
  	}
    return scaledBleedPath;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.avery.miwok.shapes.AveryShape#getCornerRadius()
   */
  public double getCornerRadius()
  {
    return 0.0;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.avery.miwok.shapes.AveryShape#getShapeString()
   */
  public String getShapeString()
  {
    return "polygon";
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.avery.miwok.shapes.AveryShape#getPolypointsElement()
   */
  public Element getPolypointsElementClone()
  {
  	Element cloneElement = (Element)polypointsElement.clone();
  	return cloneElement;
  }
  
  public Element getPolypointsElement()
  {
  	return polypointsElement.detach();
  }

  public void setPolypointsElement(Element element)
  {
    polypointsElement = element;
  }

  public boolean allowReorientation()
  {
    return true;
  }

  public Point2D getMinPosition()
  {
  	return minPosition;
  }
  
  public void setMinPosition(Point2D point)
  {
  	minPosition = point;
  }
  
  public Point2D getMaxPosition()
  {
  	return maxPosition;
  }
  
  public void setMaxPosition(Point2D point)
  {
  	maxPosition = point;
  }
  

  public double getPolyWidth()
  {
  	return getMaxPosition().getX() - getMinPosition().getX();
  }
  
  public double getPolyHeight()
  {
  	return getMaxPosition().getY() - getMinPosition().getY();
  }
  
  /**
   * resizes the polygon independently in both directions
   * 
   * @param scalex
   * @param scaley
   */
  public void scale(double scalex, double scaley)
  {
    // scale the bounding rect
    super.scale(scalex, scaley);

    // scale the path
    AffineTransform at = new AffineTransform();
    at.setToScale(scalex, scaley);
    path = new GeneralPath(path.createTransformedShape(at));

    // the redundant elements array needs to be reformulated because
    // unfortunately it is used to render the PDF of the polygon
    elements = new ArrayList();
    boolean bFirstElement = true;
    
    // scale the Averysoft XML element
    Iterator i = polypointsElement.getChildren().iterator();
    while (i.hasNext())
    {
      Element child = (Element) i.next();
      if (child.getName().equals("point"))
      {
        Attribute attx = child.getAttribute("x");
        Attribute atty = child.getAttribute("y");

        double x = Double.parseDouble(attx.getValue()) * scalex;
        double y = Double.parseDouble(atty.getValue()) * scaley;

        attx.setValue(Double.toString(x));
        atty.setValue(Double.toString(y));
        elements.add(new PolyPoint((int)x, (int)y, bFirstElement));
      }
      else if (child.getName().equals("bezier"))
      {
        Attribute attx1 = child.getAttribute("x1");
        Attribute atty1 = child.getAttribute("y1");
        Attribute attx2 = child.getAttribute("x2");
        Attribute atty2 = child.getAttribute("y2");
        Attribute attx3 = child.getAttribute("x3");
        Attribute atty3 = child.getAttribute("y3");

        double x1 = Double.parseDouble(attx1.getValue()) * scalex;
        double y1 = Double.parseDouble(atty1.getValue()) * scaley;
        double x2 = Double.parseDouble(attx2.getValue()) * scalex;
        double y2 = Double.parseDouble(atty2.getValue()) * scaley;
        double x3 = Double.parseDouble(attx3.getValue()) * scalex;
        double y3 = Double.parseDouble(atty3.getValue()) * scaley;

        attx1.setValue(Double.toString(x1));
        atty1.setValue(Double.toString(y1));
        attx2.setValue(Double.toString(x2));
        atty2.setValue(Double.toString(y2));
        attx3.setValue(Double.toString(x3));
        atty3.setValue(Double.toString(y3));

        if (bFirstElement)
          elements.add(new PolyPoint((int)x1, (int)y1, true));
        // cubic bezier has one more point than quadratic
        //System.out.println(child.getAttribute("x4"));
        if (child.getAttribute("x4") != null)
        {
          //System.out.println("cubic bezier case");
          Attribute attx4 = child.getAttribute("x4");
          Attribute atty4 = child.getAttribute("y4");
          double x4 = Double.parseDouble(attx4.getValue()) * scalex;
          double y4 = Double.parseDouble(atty4.getValue()) * scaley;
          attx4.setValue(Double.toString(x4));
          atty4.setValue(Double.toString(y4));
        	elements.add(new PolyCurve((int)x1, (int)y1, (int)x2, (int)y2, (int)x3, (int)y3, (int)x4, (int)y4));       	
        }
        else
        {
        	elements.add(new PolyQuad((int)x1, (int)y1, (int)x2, (int)y2, (int)x3, (int)y3));       	
        }
      }
      bFirstElement = false;
    }
  }

  /**
   * reorients the polygon by 90 or 270 degrees
   * 
   * @param clockwise
   */
  public void orient(boolean clockwise)
  {
  	double width = getWidth();
  	double height = getHeight();
  	
    // reorient each polypoint XML element
    Iterator i = polypointsElement.getChildren().iterator();
    path = null;
    
    if (clockwise)
    {
    	// clockwise
	    while (i.hasNext())
	    {
		      Element child = (Element) i.next();
		      if (child.getName().equals("point"))
		      {
		        Attribute attx = child.getAttribute("x");
		        Attribute atty = child.getAttribute("y");
		
		        double x = Double.parseDouble(attx.getValue());
		        double y = Double.parseDouble(atty.getValue());
		        double startX = x;
		        x = height - y;
		        y = startX;
		
		        attx.setValue(Double.toString(x));
		        atty.setValue(Double.toString(y));
		        
		        if (path == null)
		        {
		        	path = new GeneralPath();
		        	path.moveTo(x, y);
		        }
		        else
		        	path.lineTo(x, y);
		      }
		      else if (child.getName().equals("bezier"))
		      {
		        Attribute attx1 = child.getAttribute("x1");
		        Attribute atty1 = child.getAttribute("y1");
		        Attribute attx2 = child.getAttribute("x2");
		        Attribute atty2 = child.getAttribute("y2");
		        Attribute attx3 = child.getAttribute("x3");
		        Attribute atty3 = child.getAttribute("y3");
		
		        double x1 = Double.parseDouble(attx1.getValue());
		        double y1 = Double.parseDouble(atty1.getValue());
		        double x2 = Double.parseDouble(attx2.getValue());
		        double y2 = Double.parseDouble(atty2.getValue());
		        double x3 = Double.parseDouble(attx3.getValue());
		        double y3 = Double.parseDouble(atty3.getValue());
		        
		        double startX = x1;
		        x1 = height - y1;
		        y1 = startX;
		        startX = x2;
		        x2 = height - y2;
		        y2 = startX;
		        startX = x3;
		        x3 = height - y3;
		        y3 = startX;
		
		        attx1.setValue(Double.toString(x1));
		        atty1.setValue(Double.toString(y1));
		        attx2.setValue(Double.toString(x2));
		        atty2.setValue(Double.toString(y2));
		        attx3.setValue(Double.toString(x3));
		        atty3.setValue(Double.toString(y3));
		        
		        if (path == null)
		        {
		          path = new GeneralPath();
		          path.moveTo(x1, y1);
		        }
		        else
		        {
		          path.lineTo(x1, y1);
		        }
		
		        // cubic bezier has one more point than quadratic
		        //System.out.println(child.getAttribute("x4"));
		        if (child.getAttribute("x4") != null)
		        {
		          //System.out.println("cubic bezier case");
		          Attribute attx4 = child.getAttribute("x4");
		          Attribute atty4 = child.getAttribute("y4");
		          double x4 = Double.parseDouble(attx4.getValue());
		          double y4 = Double.parseDouble(atty4.getValue());
			        startX = x4;
			        x4 = height - y4;
			        y4 = startX;
		          attx4.setValue(Double.toString(x4));
		          atty4.setValue(Double.toString(y4));
		          
		          path.curveTo(x2, y2, x3, y3, x4, y4);
		        }
		        else
		        {
			        path.quadTo(x2, y2, x3, y3);		        	
		        }
		      }
		   }
    }
    else
    {
    	// ccw
	    while (i.hasNext())
	    {
		      Element child = (Element) i.next();
		      if (child.getName().equals("point"))
		      {
		        Attribute attx = child.getAttribute("x");
		        Attribute atty = child.getAttribute("y");
		
		        double x = Double.parseDouble(attx.getValue());
		        double y = Double.parseDouble(atty.getValue());
		        double startX = x;
		        x = y;
		        y = width - startX;
		
		        attx.setValue(Double.toString(x));
		        atty.setValue(Double.toString(y));
		        
		        if (path == null)
		        {
		        	path = new GeneralPath();
		        	path.moveTo(x, y);
		        }
		        else
		        	path.lineTo(x, y);
		      }
		      else if (child.getName().equals("bezier"))
		      {
		        Attribute attx1 = child.getAttribute("x1");
		        Attribute atty1 = child.getAttribute("y1");
		        Attribute attx2 = child.getAttribute("x2");
		        Attribute atty2 = child.getAttribute("y2");
		        Attribute attx3 = child.getAttribute("x3");
		        Attribute atty3 = child.getAttribute("y3");
		
		        double x1 = Double.parseDouble(attx1.getValue());
		        double y1 = Double.parseDouble(atty1.getValue());
		        double x2 = Double.parseDouble(attx2.getValue());
		        double y2 = Double.parseDouble(atty2.getValue());
		        double x3 = Double.parseDouble(attx3.getValue());
		        double y3 = Double.parseDouble(atty3.getValue());
		        
		        double startX = x1;
		        x1 = y1;
		        y1 = width - startX;
		        startX = x2;
		        x2 = y2;
		        y2 = width - startX;
		        startX = x3;
		        x3 = y3;
		        y3 = width - startX;
		
		        attx1.setValue(Double.toString(x1));
		        atty1.setValue(Double.toString(y1));
		        attx2.setValue(Double.toString(x2));
		        atty2.setValue(Double.toString(y2));
		        attx3.setValue(Double.toString(x3));
		        atty3.setValue(Double.toString(y3));
		        
		        if (path == null)
		        {
		          path = new GeneralPath();
		          path.moveTo(x1, y1);
		        }
		        else
		        {
		          path.lineTo(x1, y1);
		        }
		
		        // cubic bezier has one more point than quadratic
		        //System.out.println(child.getAttribute("x4"));
		        if (child.getAttribute("x4") != null)
		        {
		          //System.out.println("cubic bezier case");
		          Attribute attx4 = child.getAttribute("x4");
		          Attribute atty4 = child.getAttribute("y4");
		          double x4 = Double.parseDouble(attx4.getValue());
		          double y4 = Double.parseDouble(atty4.getValue());
			        startX = x4;
			        x4 = y4;
			        y4 = width - startX;
		          attx4.setValue(Double.toString(x4));
		          atty4.setValue(Double.toString(y4));
		          
		          path.curveTo(x2, y2, x3, y3, x4, y4);
		        }
		        else
		        {
			        path.quadTo(x2, y2, x3, y3);		        			        	
		        }
		      }
		   }
    }
  }
  
  
  public Iterator getElementsIterator()
  {
    return elements.iterator();
  }
  
	float[] QuadToCubic(float x1, float y1, float x2, float y2, float x3, float y3)
	{
		float cubic[] = new float[8];

		// points[0] is the start point
		cubic[0] = x1;
		cubic[1] = y1;

		// points[3] is the end point
		cubic[6] = x3;
		cubic[7] = y3;

		// calculate control points of cubic bezier
		// see http://fontforge.sourceforge.net/bezier.html
		cubic[2] = x1 + (2.0f * (x2 - x1) / 3.0f);
		cubic[3] = y1 + (2.0f * (y2 - y1) / 3.0f);

		cubic[4] = x3 + (2.0f * (x2 - x3) / 3.0f);
		cubic[5] = y3 + (2.0f * (y2 - y3) / 3.0f);

		return cubic;
	}
	
  // flips the outline about the x axis at mid panel height
  public void flipX(String productGroup, double height, Element element)
  {
  	Path pathx = Paths.get(productGroup + "_flip_x.xml");
  	float h = (float)height;
  	
  	try (BufferedWriter writer = Files.newBufferedWriter(pathx, StandardCharsets.UTF_8))
  	{
  		writer.write("    <avery:polypoints>");
  		writer.newLine();

  	
	    Iterator i = element.getChildren().iterator();
	    while (i.hasNext())
	    {
	      Element child = (Element) i.next();
	      if (child.getName().equals("point"))
	      {
	        float x = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x")));
	        float y = java.lang.Float.parseFloat(child.getAttributeValue("y"));
	        float yflip = positiveZero(-(y - h));
	        writer.write("      <avery:point x=\"" + x + "\" y=\"" + yflip + "\"/>");
	        writer.newLine();
	      }
	      else if (child.getName().equals("bezier"))
	      {
	        float x1 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x1")));
	        float y1 = java.lang.Float.parseFloat(child.getAttributeValue("y1"));
	        float x2 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x2")));
	        float y2 = java.lang.Float.parseFloat(child.getAttributeValue("y2"));
	        float x3 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x3")));
	        float y3 = java.lang.Float.parseFloat(child.getAttributeValue("y3"));
	        float y1flip = positiveZero(-(y1 - h));
	        float y2flip = positiveZero(-(y2 - h));
	        float y3flip = positiveZero(-(y3 - h));
	        if (child.getAttributeValue("x4") != null)
	        {
	          float x4 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x4")));
	          float y4 = java.lang.Float.parseFloat(child.getAttributeValue("y4"));
		        float y4flip = positiveZero(-(y4 - h));
		        writer.write("      <avery:bezier x1=\"" + x1 + "\" y1=\"" + y1flip +
		        "\" x2=\"" + x2 + "\" y2=\"" + y2flip +
		        "\" x3=\"" + x3 + "\" y3=\"" + y3flip +
		        "\" x4=\"" + x4 + "\" y4=\"" + y4flip + "\"/>");
		        writer.newLine();
	        }
	        else
	        {
	        	writer.write("      <avery:bezier x1=\"" + x1 + "\" y1=\"" + y1flip +
		        "\" x2=\"" + x2 + "\" y2=\"" + y2flip +
		        "\" x3=\"" + x3 + "\" y3=\"" + y3flip + "\"/>");
	        	writer.newLine();
	        	
	        }
	      }
	    }
	    writer.write("    </avery:polypoints>");
	    writer.newLine();
	    writer.close();
    }
  	catch (Exception e)
  	{
  		
  	}
 	
  }

  // flips the outline about the y axis at mid panel height
  public void flipY(String productGroup, double width, Element element)
  {
  	Path pathy = Paths.get(productGroup + "_flip_y.xml");
  	float w = (float)width;
  	
  	try (BufferedWriter writer = Files.newBufferedWriter(pathy, StandardCharsets.UTF_8))
  	{
  		writer.write("    <avery:polypoints>");
  		writer.newLine();
	
	    Iterator i = element.getChildren().iterator();
	    while (i.hasNext())
	    {
	      Element child = (Element) i.next();
	      if (child.getName().equals("point"))
	      {
	        float x = java.lang.Float.parseFloat(child.getAttributeValue("x"));
	        float y = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y")));
	        float xflip = positiveZero(-(x - w));
	        writer.write("      <avery:point x=\"" + xflip + "\" y=\"" + y + "\"/>");
	        writer.newLine();
	      }
	      else if (child.getName().equals("bezier"))
	      {
	        float x1 = java.lang.Float.parseFloat(child.getAttributeValue("x1"));
	        float y1 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y1")));
	        float x2 = java.lang.Float.parseFloat(child.getAttributeValue("x2"));
	        float y2 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y2")));
	        float x3 = java.lang.Float.parseFloat(child.getAttributeValue("x3"));
	        float y3 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y3")));
	        float x1flip = positiveZero(-(x1 - w));
	        float x2flip = positiveZero(-(x2 - w));
	        float x3flip = positiveZero(-(x3 - w));
	        if (child.getAttributeValue("x4") != null)
	        {
	          float x4 = java.lang.Float.parseFloat(child.getAttributeValue("x4"));
	          float y4 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y4")));
		        float x4flip = positiveZero(-(x4 - w));
		        writer.write("      <avery:bezier x1=\"" + x1flip + "\" y1=\"" + y1 +
		        "\" x2=\"" + x2flip + "\" y2=\"" + y2 +
		        "\" x3=\"" + x3flip + "\" y3=\"" + y3 +
		        "\" x4=\"" + x4flip + "\" y4=\"" + y4 + "\"/>");
		        writer.newLine();
	        }
	        else
	        {
	        	writer.write("      <avery:bezier x1=\"" + x1flip + "\" y1=\"" + y1 +
		        "\" x2=\"" + x2flip + "\" y2=\"" + y2 +
		        "\" x3=\"" + x3flip + "\" y3=\"" + y3 + "\"/>");
	        	writer.newLine();	        	
	        }
	      }
	    }
	    writer.write("    </avery:polypoints>");
	    writer.newLine();
	    writer.close();
     }
  	catch (Exception e)
  	{
  		
  	}
 	
  }

  public void rotateLeft90(String productGroup, Element element)
  {
  	Path path = Paths.get(productGroup + "_rotate_left.xml");
  
  	try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8))
  	{
      writer.write("    <avery:polypoints>");
      writer.newLine();
 	
	    Iterator i = element.getChildren().iterator();
	    while (i.hasNext())
	    {
	      Element child = (Element) i.next();
	      if (child.getName().equals("point"))
	      {
	        float x = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x")));
	        float y = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y")));
	        writer.write("      <avery:point x=\"" + y + "\" y=\"" + x + "\"/>");
	        writer.newLine();
	      }
	      else if (child.getName().equals("bezier"))
	      {
	        float x1 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x1")));
	        float y1 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y1")));
	        float x2 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x2")));
	        float y2 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y2")));
	        float x3 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x3")));
	        float y3 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y3")));
	        if (child.getAttributeValue("x4") != null)
	        {
	          float x4 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x4")));
	          float y4 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("y4")));
		        writer.write("      <avery:bezier x1=\"" + y1 + "\" y1=\"" + x1 +
		        "\" x2=\"" + y2 + "\" y2=\"" + x2 +
		        "\" x3=\"" + y3 + "\" y3=\"" + x3 +
		        "\" x4=\"" + y4 + "\" y4=\"" + x4 + "\"/>");
		        writer.newLine();
	        }
	        else
	        {
		        writer.write("      <avery:bezier x1=\"" + y1 + "\" y1=\"" + x1 +
		        "\" x2=\"" + y2 + "\" y2=\"" + x2 +
		        "\" x3=\"" + y3 + "\" y3=\"" + x3 + "\"/>");
		        writer.newLine();
	        	
	        }
	      }
	    }
      writer.write("    </avery:polypoints>");
      writer.newLine();
	    writer.close();
    }
  	catch (Exception e)
  	{
  		
  	}
 	
  }
  
  public void rotateRight90(String productGroup, double height, Element element)
  {
  	float h = (float)height;
  	Path path = Paths.get(productGroup + "_rotate_right.xml");
  
  	try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8))
  	{
      writer.write("    <avery:polypoints>");
      writer.newLine();

  	
	    Iterator i = element.getChildren().iterator();
	    while (i.hasNext())
	    {
	      Element child = (Element) i.next();
	      if (child.getName().equals("point"))
	      {
	        float x = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x")));
	        float y = java.lang.Float.parseFloat(child.getAttributeValue("y"));
	        float xNew = positiveZero(h - y);
	        
	        writer.write("      <avery:point x=\"" + xNew + "\" y=\"" + x + "\"/>");
	        writer.newLine();
	      }
	      else if (child.getName().equals("bezier"))
	      {
	        float x1 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x1")));
	        float y1 = java.lang.Float.parseFloat(child.getAttributeValue("y1"));
	        float x2 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x2")));
	        float y2 = java.lang.Float.parseFloat(child.getAttributeValue("y2"));
	        float x3 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x3")));
	        float y3 = java.lang.Float.parseFloat(child.getAttributeValue("y3"));
	        float x1New = positiveZero(h - y1);
	        float x2New = positiveZero(h - y2);
	        float x3New = positiveZero(h - y3);
	          
	        if (child.getAttributeValue("x4") != null)
	        {
	          float x4 = positiveZero(java.lang.Float.parseFloat(child.getAttributeValue("x4")));
	          float y4 = java.lang.Float.parseFloat(child.getAttributeValue("y4"));
		        float x4New = positiveZero(h - y4);
		        writer.write("      <avery:bezier x1=\"" + x1New + "\" y1=\"" + x1 +
		        "\" x2=\"" + x2New + "\" y2=\"" + x2 +
		        "\" x3=\"" + x3New + "\" y3=\"" + x3 +
		        "\" x4=\"" + x4New + "\" y4=\"" + x4 + "\"/>");
		        writer.newLine();
	        }
	        else
	        {
		        writer.write("      <avery:bezier x1=\"" + x1New + "\" y1=\"" + x1 +
		        "\" x2=\"" + x2New + "\" y2=\"" + x2 +
		        "\" x3=\"" + x3New + "\" y3=\"" + x3 + "\"/>");
		        writer.newLine();
	        	
	        }
	      }
	    }
      writer.write("    </avery:polypoints>");
      writer.newLine();
	    writer.close();
    }
  	catch (Exception e)
  	{
  		
  	}
 	
  }
  
  private float positiveZero(float input)
  {
  	if (input == -0.0)
  		input = 0.0f;
  	return input;
  }

  
}
