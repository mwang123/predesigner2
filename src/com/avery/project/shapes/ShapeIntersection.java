package com.avery.project.shapes;

import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Area;
import java.awt.geom.AffineTransform;
/**
 * <p>Copyright: Copyright 2013 Avery Products Corporation, all rights reserved</p>
*/

/**
 * Title:       ShapeIntersection<p>
 * Description: Combines two transformed areas into one <p>
 * Copyright:   (c)Copyright 2013 Avery Products Corp. All Rights Reserved.
 * <p>Company: Avery Products Corporation</p>
 * @author Brad Nelson
 * @version 1
  */
public class ShapeIntersection
{
	
	public ShapeIntersection()
	{
		
	}
	
	public Area createArea(double x, double y, double width, double height, double angle)
	{
		//System.out.println("createArea");
		//System.out.println("x, y, width, height, angle=" + x + "," + y + "," + width + "," + height + "," + angle);
		Rectangle2D.Double rect = new Rectangle2D.Double(0, 0, width, height);
		Area area = new Area(rect);
		AffineTransform t = new AffineTransform();
		t.rotate(degreesToRadians(angle));
		area = area.createTransformedArea(t);
		t = new AffineTransform();
		t.translate(x, y);
		area = area.createTransformedArea(t);
		
		return area;
	}
	
	public void union(Area area1, Area area2)
	{
		area1.add(area2);
		//Rectangle2D rect = area1.getBounds2D();
		//System.out.println("union");
		//System.out.println(rect.getX() + "," + rect.getY());
		//System.out.println(rect.getWidth() + "," + rect.getHeight());
		//System.out.println(rect.getMinX() + "," + rect.getMinY());
		//System.out.println(rect.getMaxX() + "," + rect.getMaxY());
	}
	
	public Rectangle2D unrotate(Area area, double angle)
	{
		//System.out.println("unrotate");
		AffineTransform t = new AffineTransform();
		t.rotate(degreesToRadians(-angle));
		area = area.createTransformedArea(t);
		t = new AffineTransform();
		Rectangle2D rect = area.getBounds2D();
		//System.out.println(rect.getX() + "," + rect.getY());
		//System.out.println(rect.getWidth() + "," + rect.getHeight());
		//System.out.println(rect.getMinX() + "," + rect.getMinY());
		//System.out.println(rect.getMaxX() + "," + rect.getMaxY());
		
		return rect;
	}

	private double degreesToRadians(double angle)
	{
		return angle * Math.PI / 180.0;
	}
	
}
