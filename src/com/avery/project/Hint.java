/* Project: Ape
 * Filename: Hint.java
 * Created on Jun 9, 2004 by Bob Lee
 * Copyright 2004 by Avery Dennison Corporation, all rights reserved
 */
package com.avery.project;

import org.jdom.Element;

/**
 * Describes a name/value pair carried by various elements to describe
 * application-specific behavior
 * @author b0b
 */
public class Hint implements Averysoftable
{
	private String name;
	private String value;

	/**
	 * Warning: No validation is performed.
	 */
	public Hint(String name, String value)
	{
		setName(name);
		setValue(value);
	}

	/**
	 * @return the name of the Hint
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the value of the Hint
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param string
	 */
	public void setValue(String string)
	{
		value = string;
	}

	/**
	 * Get a JDOM representation of the object suitable for
	 * inclusion in an XML file based on averysoft.xsd
	 * @return an Averysoft-based JDOM Element
	 */
	public Element getAverysoftElement()
	{
		Element hintElement = new Element("hint");
    hintElement.setNamespace(AveryProject.getAverysoftNamespace());

		hintElement.setAttribute("name", getName());
		hintElement.setAttribute("value", getValue());
		return hintElement;
	}
	
	public Element getAverysoftIBElement()
	{
		return null;
	}

}
