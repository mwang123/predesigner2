package com.avery.project;

import org.jdom.Element;

public class SerialNumber implements Averysoftable
{
  /**
   * Creates a SerialNumber object from a JDOM Element representing an Averysoft XML
   * serial number object
   */
	private String startValue;
	private String endValue;
	private String serialNumberType;
	private String step;
	private String prefix;
	private String suffix;
	private String id;
	
	SerialNumber(Element element) throws Exception
  {
    startValue = element.getAttributeValue("startValue");
    endValue = element.getAttributeValue("endValue");
    serialNumberType = element.getAttributeValue("serialNumberType");
    step = element.getAttributeValue("step");
    prefix = element.getAttributeValue("prefix");
    suffix = element.getAttributeValue("suffix");
    id = element.getAttributeValue("id");
  }

  /**
   * @return a JDOM Element representing this object, consistent with the
   *         averysoft.xsd schema
   * @see com.avery.project.Averysoftable#getAverysoftElement()
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("serialNumber");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    element.setAttribute("startValue", startValue);
    element.setAttribute("endValue", endValue);
    element.setAttribute("serialNumberType", serialNumberType);
    element.setAttribute("step", step);
    element.setAttribute("prefix", prefix);
    element.setAttribute("suffix", suffix);
    element.setAttribute("id", id);
    
    return element;
  }
  
  public Element getAverysoftIBElement()
  {
  	return getAverysoftElement();
  }

}
