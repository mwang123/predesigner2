/*
 * Mergeable.java Created on Jul 13, 2005 by leeb
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.project;

import java.util.List;

/**
 * @author leeb
 *
 * Interface for AveryPanelfield classes that support "merge" operations
 */
public interface Mergeable
{
	/**
	 * 
	 * @return a List of com.avery.miwok.Text objects.  In everything but an
	 * AveryTextblock, this will be a List of just one object.
	 */
	public List getTextContent();
	
	/**
	 * @param list - of com.avery.miwok.Text objects.  In everything but an
	 * AveryTextblock, this should be a List of just one object.
	 */
	public void setTextContent(List list);
}
