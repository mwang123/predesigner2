package com.avery.project.masks.geometry;

import java.awt.Graphics2D;

import org.faceless.pdf2.PDFCanvas;

public interface MaskGeometry
{
  public void clipMask(PDFCanvas cnvs, float canvasStartX, float canvasStartY);
  public void clipImageMask(Graphics2D graphics, double scalar);
}
