package com.avery.project.masks.geometry;

import java.awt.Graphics2D;

import org.faceless.pdf2.PDFCanvas;
import org.jdom.DataConversionException;
import org.jdom.Element;

public class RectangeGeometry implements MaskGeometry
{

  private double width;
  private double height;
  private double x;
  private double y;

  public RectangeGeometry(Element geometryElement) throws DataConversionException
  {
    x = geometryElement.getAttribute("x").getDoubleValue();
    y = geometryElement.getAttribute("y").getDoubleValue();
    width = geometryElement.getAttribute("width").getDoubleValue();
    height = geometryElement.getAttribute("height").getDoubleValue();
  }

  public void clipMask(PDFCanvas cnvs, float canvasStartX, float canvasStartY)
  {
    cnvs.clipRectangle((float) x / 20.0f - canvasStartX, (float) y / 20.0f - canvasStartY, (float) (x + width) / 20.0f
        - canvasStartX, (float) (y + height) / 20.0f - canvasStartY);
  }

  public void clipImageMask(Graphics2D graphics, double scalar)
  {
    graphics.clipRect((int) (x * scalar), (int) (y * scalar), (int) (width * scalar), (int) (height * scalar));
  }

}
