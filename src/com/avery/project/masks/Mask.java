package com.avery.project.masks;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.faceless.pdf2.PDFCanvas;
import org.jdom.Element;

import com.avery.project.AveryProject;
import com.avery.project.masks.geometry.MaskGeometry;
import com.avery.project.masks.geometry.RectangeGeometry;

public class Mask
{
  private ArrayList geometriesContainer = new ArrayList();
  private int geometryIndex = 0;

  public boolean incrementIndex()
  {
    geometryIndex++;
    if (geometryIndex >= geometriesContainer.size())
    {
      geometryIndex = 0;
      return false;
    }
    return true;
  }

  public void resetIndex()
  {
    geometryIndex = 0;
  }

  public Mask(Element element)
  {
    List geometries = element.getChildren("geometry", AveryProject.getAverysoftNamespace());
    Iterator iter = geometries.iterator();
    while (iter.hasNext())
    {
      Element geometry = (Element) iter.next();
      if (geometry.getAttributeValue("shape").equals("rect"))
      {
        try
        {
          geometriesContainer.add(new RectangeGeometry(geometry));
        }
        catch (Exception e)
        {
          System.err.println("Wrong mask parameters");
          e.printStackTrace();
        }
      }
    }
  }

  public void clipMask(PDFCanvas cnvs, float canvasStartX, float canvasStartY)
  {
    if (geometriesContainer.isEmpty())
    {
      return;
    }
    ((MaskGeometry) geometriesContainer.get(geometryIndex)).clipMask(cnvs, canvasStartX, canvasStartY);
  }

  public void clipImageMask(Graphics2D graphics, double scalar)
  {
    if (geometriesContainer.isEmpty())
    {
      return;
    }
    ((MaskGeometry) geometriesContainer.get(geometryIndex)).clipImageMask(graphics, scalar);
  }
}
