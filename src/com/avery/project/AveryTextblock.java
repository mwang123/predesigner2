/**
 * Title: AveryTextblock
 * <p>
 * Description: AveryTextblock describes a textfield that may contain multiple
 * lines of text
 * <p>
 * Copyright: Copyright (c)2000 Avery Dennison
 * <p>
 * Company: Avery Dennison
 * <p>
 * 
 * @author Bob Lee
 * @version 1.0
 */

package com.avery.project;

import java.util.Hashtable;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.awt.*;
import java.util.*;

import org.faceless.pdf2.PDFPage;
import org.jdom.Element;

import com.avery.project.richtext.RichParagraph;
import com.avery.project.richtext.RichSpan;

public class AveryTextblock extends AveryTextfield implements Averysoftable, Mergeable
{

  public AveryTextblock()
  {
    setOverflow("wrapshrink");
  }

  protected AveryTextblock(Element element) throws Exception
  {
    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace()) && element.getName().equals("textBlock"))
    {
    	if (element.getAttribute("lineSpacing") != null)
    	{
    		lineSpacing = Double.valueOf(element.getAttributeValue("lineSpacing"));  		
//    		System.out.println(lineSpacing.doubleValue());
    	}
    	
    	if (element.getAttribute("maxLines") != null)
    	{
    		maxLines = Integer.valueOf(element.getAttributeValue("maxLines"));  		
//    		System.out.println(maxLines.intValue());
    	}
      readTextfieldAverysoftAttributes(element);
      
      setAllDifferent("true".equals(element.getAttributeValue("allDifferent")));
      
      Element descriptionElement = element.getChild("description", AveryProject.getAverysoftNamespace());
      if (descriptionElement != null)
      {
        setDescriptionObject(Description.makeTextFieldDescription(descriptionElement));
      }

      Iterator iterator = element.getChildren("text", AveryProject.getAverysoftNamespace()).iterator();

      // super.hasNativeContent = iterator.hasNext();
      // Element textElement;
      while (iterator.hasNext())
      {
        Element line = (Element) iterator.next();
        if (line.getChildren().isEmpty())
          addLine(new Text(line));
        else
        {
        	RichText richText = new RichText(line);
        	TextStyle ts = getTextStyle();
        	ts.setVAlign(richText.getVAlign());
        	ts.setJustification(richText.getTextAlign());
        	List paragraphs = richText.getParagraphs();
        	RichParagraph p = (RichParagraph)paragraphs.get(0);
        	RichSpan span = (RichSpan)p.getSpans().get(0);
        	ts.setPointSize(new Double(span.getSize()));
        	ts.setTypeface(span.getFont());
        	ts.setTextColor(span.getFontColor());
        	ts.setBold(span.isBold());
        	ts.setItalic(span.isItalic());
        	copyTextStyleAttributes();
        	
        	Iterator piter = paragraphs.iterator();
         	while (piter.hasNext())
        	{
         		RichParagraph rp = (RichParagraph)piter.next();
          	List spans = rp.getSpans();
       	
	        	Iterator siter = spans.iterator();
	        	while (siter.hasNext())
	        	{
	        		RichSpan spani = (RichSpan)siter.next();
	        		addLine(spani.getContent());
	        		if (spani.getSerialNumberField() != null)
	        		{
	        			serialNumberField = spani.getSerialNumberField();
	        		}
	        	}
        	}
          ///addLine(richText);
        }
      }
    }
    else if (!initFromAveryDtdElement(element))
    {
      throw new Exception("Not an Avery Textblock element");
    }
  }

  private boolean initFromAveryDtdElement(Element element) throws Exception
  {
    if (element.getName().equals("Avery.pf.textblock"))
    {
      setTypeface(element.getAttributeValue("typeface"));
      setPointsize(Double.valueOf(element.getAttributeValue("pointsize")));
      setTextcolor(element.getAttributeValue("textcolor"));
      setJustification(element.getAttributeValue("justification"));
      setVerticalAlignment(element.getAttributeValue("valign"));
      setStyles(element.getAttributeValue("styles"));
      setOverflow(element.getAttributeValue("overflow"));

      // read the lines of text
      List lines = element.getChildren("P");
      Iterator iterator = lines.iterator();
      while (iterator.hasNext())
      {
        //addLine(new Text(((Element) iterator.next()).getText()));
        Element line = (Element) iterator.next();
        if (line.getChildren().isEmpty())
          addLine(new Text(line));
        else
        {
          addLine(new RichText(line));
        }
      }

      readSuperAttributes(element.getParent());

      return true;
    }

    return false;
  }

  // this is an array of com.avery.miwok.Text objects
  private List lines = new ArrayList();
  
  private Double lineSpacing = new Double(1.0);

  private Integer maxLines = new Integer(-1);
  
  private String serialNumberField = null;

  /**
   * @return the array of com.avery.miwok.Text objects
   */
  public List getLines()
  {
    return lines;
  }

  /**
   * @param newLines
   *          a List of com.avery.miwok.Text objects
   */
  public void setLines(List newLines)
  {
    lines = newLines;
  }

  void addLine(Text text)
  {
    lines.add(text);
  }

  /**
   * for compatibility with AveryPrint 4.5
   * 
   * @param string -
   *          a line of text
   */
  public void addLine(String string)
  {
    Text text = new Text();
    text.setContent(string);
    addLine(text);
  }

  public Double getLineSpacing()
  {
  	return lineSpacing;
  }
  
  public void setLineSpacing(Double newLineSpacing)
  {
  	lineSpacing = newLineSpacing;
  }

  public Integer getMaxLines()
  {
  	return maxLines;
  }
  
  public void setMaxLines(Integer newMaxLines)
  {
  	maxLines = newMaxLines;
  }

 
  /**
   * AveryPanelField.clone doesn't clone this class properly, so we override it
   * here. Textblock content is stored in a List of Strings, and that List is
   * not Cloneable by default.
   * 
   * @return a clone of this object
   */
  public Object clone()
  {
    AveryTextblock newBlock = (AveryTextblock) super.clone();
    
    newBlock.lineSpacing = lineSpacing;
    newBlock.lines = new ArrayList();
    Iterator i = getLines().iterator();
    while (i.hasNext())
    {
      Text tmp = (Text) i.next();
      if (tmp instanceof RichText)
      {
        newBlock.addLine(new RichText(tmp.getAverysoftElement()));
      }
      else
      {
        newBlock.addLine(new Text(tmp));
      }
    }
    return newBlock;
  }

  /**
   * Creates a deep clone
   * 
   * @return deep clone
   */
  public AveryPanelField deepClone()
  {
    AveryTextblock tb = new AveryTextblock();
    copyPrivateData(tb);

    tb.setTypeface(getTypeface());
    tb.setPointsize(getPointsize());
    tb.setTextcolor(getTextcolor());
    tb.setStyles(getStyles());
    tb.setJustification(getJustification());
    tb.setVerticalAlignment(getVerticalAlignment());
    tb.setOverflow(getOverflow());
    tb.setTextStyle(getTextStyle());
    tb.setAllDifferent(isAllDifferent());

    tb.fromFlash = fromFlash;
    tb.flashLineheight = flashLineheight;
    tb.flashXscale = flashXscale;
    tb.outline = outline;

    tb.lineSpacing = lineSpacing;
    tb.maxLines = maxLines;
    tb.lines = new ArrayList();
    Iterator i = getLines().iterator();
    while (i.hasNext())
    {
      Text tmp = (Text) i.next();
      if (tmp instanceof RichText)
      {
        tb.addLine(new RichText(tmp.getAverysoftElement()));
      }
      else
      {
        tb.addLine(new Text(tmp));
      }
    }

    return (AveryPanelField) tb;
  }

  /**
   * The matches() method returns <code>true</code> if the given field is an
   * AveryTextblock with the same content and AveryTextfield attributes.
   * 
   * @param field
   *          the field to test against
   * @return <code>true</code> if the contents of the field match the contents
   *         of <code>this</code>, <code>false</code> otherwise
   */
  public boolean matches(AveryPanelField field)
  {
    if (field instanceof AveryTextblock)
    {
      AveryTextblock textblock = (AveryTextblock) field;
      if (getLines().size() == textblock.getLines().size())
      {
        Iterator i1 = getLines().iterator();
        Iterator i2 = textblock.getLines().iterator();
        while (i1.hasNext())
        {
          String s1 = i1.next().toString();
          String s2 = i2.next().toString();
          if (!s1.equals(s2))
          {
            return false; // some string didn't match
          }
        }
        return super.matches(field);
      }
    }
    return false;
  }

  /**
   * The differences() method compares this field with another AveryPanelField
   * and returns a BitSet describing how they differ. The bits of the BitSet are
   * defined in the AveryPanelField base class. If the field is an instance of a
   * different class, the APPLES_AND_ORANGES bit is set and the comparison goes
   * no further.
   */
  public BitSet differences(AveryPanelField field)
  {
    BitSet flags = super.differences(field);

    if (flags.get(APPLES_AND_ORANGES))
    { // no sense in comparing apples and oranges
      return flags;
    }

    AveryTextblock textblock = (AveryTextblock) field;
    if (getLines().size() == textblock.getLines().size())
    {
      Iterator i1 = getLines().iterator();
      Iterator i2 = textblock.getLines().iterator();
      while (i1.hasNext())
      {
        if (!((String) i1.next()).equals((String) i2.next()))
        {
          flags.set(TEXTCONTENT_DIFFERS);
          break;
        }
      }
    }
    else
    // differing number of lines
    {
      flags.set(TEXTCONTENT_DIFFERS);
    }

    return flags;
  }

  /**
   * This method modifies the field, if it exists, to match recent changes in
   * another field. It is used to update fields that have been made obsolete by
   * an updated Masterpanel (hence the name).
   * 
   * @param field -
   *          the field to pull changes from.
   * @param flags -
   *          describes what attributes to update
   * @return the number of modifications made
   */
  public int masterUberAlles(AveryPanelField field, BitSet flags)
  {
    int modified = super.masterUberAlles(field, flags);

    if (flags.get(TEXTCONTENT_DIFFERS))
    {
      setLines(((AveryTextblock) field).getLines());
      ++modified;
    }

    return modified;
  }

  private String buildTextBlock()
  {
    String lineSeparator = "\b\b";

    List blockLines = this.getLines();
    if (blockLines.size() == 0)
    {
      return "Bogus Textblock";
    }
    // Get the text
    String sTextToShow = "";
    Iterator i = blockLines.iterator();
    while (i.hasNext())
    {
      String sLine = i.next().toString();
      if (sLine == null || sLine.equalsIgnoreCase("null"))
      {
        // Put in the blank line
        sTextToShow += lineSeparator;
      }
      else
      {
        sLine.trim();
        sTextToShow += sLine + lineSeparator; // ding ding (double bells)
      }
    }
    return sTextToShow;
  }

  public String dump()
  {
    String str = new String("");
    str = "<P>AveryTextblock";
    str += super.dump();
    str += "<br>lines = ";
    Iterator i = lines.iterator();
    while (i.hasNext())
    {
      str += "<br> &nbsp; " + i.next().toString();
    }
    return str;
  }

  public void setTextContent(String textContent)
  {
    lines.clear();

    StringTokenizer st = new StringTokenizer(textContent, "\r\n");

    if (!st.hasMoreElements())
    {
      st = new StringTokenizer(textContent, "\n");
      // String delimiter = "\n";
      if (!st.hasMoreElements())
      {
        st = new StringTokenizer(textContent, "\r");
        // delimiter = "\r";
      }
    }

    while (st.hasMoreElements())
    {
      String line = st.nextToken();
      // trace ("line=<" + line + ">");
      // trace ("line=<" + line + ">");
      addLine(new Text(line));
    }
  }

  // implement draw() as declared in abstract superclass AveryPanelField
  public void draw(Graphics2D gr, double dScalar)
  {
    // Prepare the text lines of the block into a specially coded string
    String content = buildTextBlock();

    // draw the text
    drawText(gr, content, dScalar);

    Iterator iter = getLines().iterator();
    while (iter.hasNext())
    {
      Object line = iter.next();
      if (line instanceof RichText)
      {
        drawRichContent(gr, dScalar, (RichText) line);
      }
    }
  }

  public void addToPDF(PDFPage pdfPage, Double dPanelHeight, Hashtable imageCache) throws Exception
  {
    // for debugging, show outline of field
    // / addOutlineToPDF(pdfdoc, offset, dPageHeight);
  	//lines.clear();         test unicode chinese
  	//addLine("\u89E3\u997F");

    // Prepare the text lines of the block into a specially coded string
    String content = buildTextBlock();

    Iterator iter = this.getLines().iterator();
    while (iter.hasNext())
    {
      Object tmp = iter.next();
      if (tmp instanceof RichText)
      {
        super.addToPDF(pdfPage, dPanelHeight, (RichText) tmp);
        return;
      }
    }

    // render text to PDF
    super.addToPDF(pdfPage, dPanelHeight, content);

  } // end addToPDF()

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAveryElement()
  {
    Element element = getParentAveryElement();
    Element content = new Element("Avery.pf.textblock");

    // add attributes
    content.setAttribute("typeface", getTypeface());
    content.setAttribute("pointsize", getPointsize().toString());
    content.setAttribute("textcolor", "0x" + Integer.toHexString(getTextcolor().getRGB()).substring(2));
    content.setAttribute("justification", getJustification());
    content.setAttribute("valign", getVerticalAlignment());
    content.setAttribute("styles", getStyles());
    content.setAttribute("overflow", getOverflow());

    // add the content itself, a bunch of <P> elements
    Iterator iterator = getLines().iterator();
    while (iterator.hasNext())
    {
      Element p = new Element("P");
      p.setText(iterator.next().toString());
      content.addContent(p);
    }

    // push content into the parent element
    element.addContent(content);
    return element;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("textBlock");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    super.applyTextfieldAverysoftAttributes(element);
    if (lineSpacing.doubleValue() != 1.0)
    	element.setAttribute("lineSpacing", String.valueOf(lineSpacing.doubleValue()));
    if (maxLines.intValue() > 0)
    	element.setAttribute("maxLines", String.valueOf(maxLines.intValue()));
    
    if (isAllDifferent())
    {
    	element.setAttribute("allDifferent", "true");
    }

    if (getDescriptionObject() == null)
    {
      setDescriptionObject(Description.makeTextFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
	  position.setAttribute("x", Double.toString(getPosition().getX()));
	  position.setAttribute("y", Double.toString(getPosition().getY()));
    
    element.addContent(position);

    if (getID().equals(getStyleID()))
    {
      copyStyleAttributes();
      element.addContent(getTextStyle().getAverysoftElement());
    }

    if (getID().equals(getContentID()))
    {
      // add the content
      Iterator iterator = getLines().iterator();
      while (iterator.hasNext())
      {
      	Element textElement = ((Text)iterator.next()).getAverysoftElement();
      	textElement.detach();
      	element.addContent(textElement);
      }
    }

    return element;
  }

  /**
   * Creates a JDOM Element for this object
   * 
   * @return Element suitable for addition to an Avery.Project XML document
   */
  public Element getAverysoftIBElement()
  {
    Element element = new Element("textBlock");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    super.applyTextfieldAverysoftIBAttributes(element);
    if (lineSpacing.doubleValue() != 1.0)
    	element.setAttribute("lineSpacing", String.valueOf(lineSpacing.doubleValue()));
    if (maxLines.intValue() > 0)
    	element.setAttribute("maxLines", String.valueOf(maxLines.intValue()));
    
    /*if (isAllDifferent())
    {
    	element.setAttribute("allDifferent", "true");
    }

    if (getDescriptionObject() == null)
    {
      setDescriptionObject(Description.makeTextFieldDescription(getID()));
    }
    element.addContent(getDescriptionObject().getAverysoftElement());*/

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
		position.setAttribute("x", AveryProject.twipsToMM(getPosition().getX()));
		position.setAttribute("y", AveryProject.twipsToMM(getPosition().getY()));
   
    element.addContent(position);

    if (getID().equals(getStyleID()))
    {
      copyStyleAttributes();
      element.addContent(getTextStyle().getAverysoftIBElement());
    }

    if (serialNumberField != null)
    	element.setAttribute("serialNumberField", serialNumberField);
    
    if (getID().equals(getContentID()))
    {
      // add the content
      Iterator iterator = getLines().iterator();
      while (iterator.hasNext())
      {
      	Element textElement = ((Text)iterator.next()).getAverysoftIBElement();
      	textElement.detach();
      	element.addContent(textElement);
      }
    }

    return element;
  }

  /**
   * Implementation of Mergeable interface
   * 
   * @return a List of com.avery.miwok.Text objects.
   */
  public List getTextContent()
  {
    return getLines();
  }

  /**
   * Implementation of Mergeable interface
   * 
   * @param list -
   *          of com.avery.miwok.Text objects.
   */
  public void setTextContent(List list)
  {
    setLines(list);
  }

  /**
   * @param key
   * @return <code>true</code> if the field contains a mergemap line matching
   *         the specified mergemap key ("AF0", "AF1", etc.)
   */
  boolean hasMergeKey(String key)
  {
    Iterator it = getLines().iterator();
    while (it.hasNext())
    {
      if (((Text) it.next()).hasMergeKey(key))
      {
        // found specified key
        return true;
      }
    }

    // couldn't find key
    return false;
  }

  /**
   * This calculates the maximum pointsize that will render correctly. Used by
   * Avery Print 5.0.
   * 
   * @return the maximum size in points.
   */
  public double maxPointSize()
  {
    double lineHeight = this.getHeight().doubleValue() / lines.size();
    return lineHeight / 20.0;
  }

  /**
   * @see AveryTextfield.translateText
   */
  int translateText(Hashtable hash)
  {
    int sum = 0;
    Iterator i = lines.iterator();
    while (i.hasNext())
    {
      if (((Text) i.next()).translateText(hash))
      {
        ++sum;
      }
    }
    return sum;
  }
}
