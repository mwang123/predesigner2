package com.avery.project;

/**
 * <p>Title: XMLElement interface</p>
 * <p>Description: Describes a common interface for getting JDOM Elements
 * that represent the implementation class</p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

interface XMLElement
{
  /**
   * Get a representation of the object in the Avery-specific form
   * @return an Avery-specific Element
   */
  org.jdom.Element getAveryElement();
}