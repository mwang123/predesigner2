package com.avery.project;

import java.awt.Color;
import java.awt.Graphics2D;

import org.jdom.Element;

/*
 * File: Guide.java
 * Created: Nov 19, 2004 by Bob Lee
 * Copyright (c) Avery Dennison Corporation, All Rights Reserved
 */

/**
 * com.avery.miwok
 * Guide is a read-only class, constructed from an averysoft XML guide element.
 */
class Guide
{
	private final String usage;
	private final TwipsPosition startPosition;
	private final TwipsPosition endPosition;
	private final String shapeString;
	private final double cornerRadius;

	/**
	 * Constructor fills the private elements, which are all declared as final.
	 * Note that there is no checking on the Element's name or namespace -
	 * it is <i>assumed</i> to be a correctly formed Averysoft.xsd guide element.
	 * @param element - the unit to parse and clone
	 */	
	Guide(Element element)
	{
		// assumes we are passed the correct Element!
		usage = element.getAttributeValue("usage");
		startPosition = new TwipsPosition(element.getAttributeValue("startPosition"));
		endPosition = new TwipsPosition(element.getAttributeValue("endPosition"));
		shapeString = element.getAttributeValue("shape", "line");
		cornerRadius = Double.parseDouble(element.getAttributeValue("cornerRadius", "0.0"));
	}
	
	/**
	 * for use by XML writers
	 * @return a guide element in the averysoft namespace 
	 */
	Element getXmlElement()
	{
    Element newElement = new Element("guide");
    newElement.setNamespace(AveryProject.getAverysoftNamespace());
    newElement.setAttribute("usage", usage);
	  newElement.setAttribute("startPosition", startPosition.toString());
	  newElement.setAttribute("endPosition", endPosition.toString());
	  
    if (!shapeString.equals("line"))
    {
      newElement.setAttribute("shape", shapeString);
    }
    if (cornerRadius > 0.0)
    {
      newElement.setAttribute("cornerRadius", Double.toString(cornerRadius));
    }
    return newElement;
	}
	
	/**
	 * for use by XML writers
	 * @return a guide element in the averysoft namespace 
	 */
	Element getXmlIBElement()
	{
    Element newElement = new Element("guide");
    newElement.setNamespace(AveryProject.getAverysoftNamespace());
    newElement.setAttribute("usage", usage);
		
    newElement.setAttribute("startPosition", startPosition.toMMString());
		newElement.setAttribute("endPosition", endPosition.toMMString());
		
    if (!shapeString.equals("line"))
    {
      newElement.setAttribute("shape", shapeString);
    }
    if (cornerRadius > 0.0)
    {
      newElement.setAttribute("cornerRadius", Double.toString(cornerRadius));
    }
    return newElement;
	}
	public String getUsage()
	{
		return usage;
	}
	
  /**
   * draws the guide
   * @param g - the Graphics context
   * @param scalar - scalar for twips to device conversion
   * @param color - the color of the drawing line
   */
	void draw(Graphics2D g, double scalar, Color color, Color fillColor)
  {
    if ("line".equals(shapeString))
    {
      g.setColor(color);
      int x1 = (int)(startPosition.x * scalar);
      int y1 = (int)(startPosition.y * scalar);
      int x2 = (int)(endPosition.x * scalar);
      int y2 = (int)(endPosition.y * scalar);      
      g.drawLine(x1,y1,x2,y2);
    }
    else if ("rect".equals(shapeString))
    {
      g.setClip(null);
      g.setColor(color);
      int x = (int)(startPosition.x * scalar);
      int y = (int)(startPosition.y * scalar);
      int w = (int)((endPosition.x - startPosition.x) * scalar);
      int h = (int)((endPosition.y - startPosition.y) * scalar);
      g.drawRect(x, y, w, h);
    	if ("usableArea".equals(usage))
    	{
    		// testing only fillColor = Color.red;
    		g.setPaint(fillColor);
    		g.fillRect(x, y, w, h);
    	}
    }
    else if ("ellipse".equals(shapeString))
    {
      g.setClip(null);
      g.setColor(color);
      int x = (int)(startPosition.x * scalar);
      int y = (int)(startPosition.y * scalar);
      int w = (int)((endPosition.x - startPosition.x) * scalar);
      int h = (int)((endPosition.y - startPosition.y) * scalar);
      g.drawOval(x, y, w, h);
    	if ("usableArea".equals(usage))
    	{
    		g.setPaint(fillColor);
    		g.fillOval(x, y, w, h);
    	}
    }
  }
  
	public boolean allowReorientation()
	{
		return (!("polygon".equals(shapeString)));
	}
	
	// rotate by 90 or 270 degrees
	public void reorient(Double masterWidth, Double masterHeight, boolean clockwise)
	{
		double guideWidth = Math.abs(endPosition.x - startPosition.x);
		double guideHeight = Math.abs(endPosition.y - startPosition.y);
		
		double width = masterWidth.doubleValue();
		double height = masterHeight.doubleValue();
		
		if (clockwise)
		{
			double startX = startPosition.x;
			startPosition.x = height - startPosition.y - guideHeight;				
			startPosition.y = startX;
		}
		else
		{
			double startX = startPosition.x;
			startPosition.x = startPosition.y;				
			startPosition.y = width - startX - guideWidth;
		}
		endPosition.x = startPosition.x + guideHeight;
		endPosition.y = startPosition.y + guideWidth;				
	}
	
}
