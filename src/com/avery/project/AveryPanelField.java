/**
 * Title: AveryPanelField
 * <p>
 * Description: This is the superclass from which all field classes are derived
 * <p>
 * Copyright: Copyright (c)2000-2007 Avery Dennison
 * <p>
 * Company: Avery Dennison
 * <p>
 * 
 * @author Bob Lee
 */

package com.avery.project;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;

import org.faceless.pdf2.PDFCanvas;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFStyle;
import org.jdom.Attribute;
import org.jdom.Element;
import org.jdom.Namespace;

import com.avery.predesign.StyleSet;
import com.avery.project.masks.Mask;

/**
 * This is the superclass from which all fields that rest on
 * {@link AverySuperpanel}s are derived. Most of the public operations for
 * panel fields can be performed through this class.
 */
public abstract class AveryPanelField implements Cloneable, XMLElement
{
  // properties
  private String id = "";
  private String contentid = "";
  private String styleid = "";
  private String geometryId = "";
  private Double width = new Double(0.0);
  private Double height = new Double(0.0);
  private Double rotation = new Double(0.0);
  private TwipsPosition position = new TwipsPosition(0.0, 0.0);
  private String promptOrder = "0.0";
  private double zLevel = 0.0;
  private boolean zLocked = false;
  private boolean visible = true;
  private Description description;
  private boolean editable = true;
  private boolean movable = true;
  private boolean styleable = true;
  private boolean previewable = true;
  private boolean print = true;
  private Mask mask = null;
  private Double opacity = new Double(1.0);

  private ProjectFieldTable fieldTable = null;

  protected List alienAttributes = new ArrayList();

  /**
   * The constructor ensures that the Description is not null
   */
  public AveryPanelField()
  {
    this.addDefaultDescriptionObject();
  }

  public String toString()
  {
    return getPrompt();
  }

  /**
   * This is used by AveryImage when assembling a new AveryImage object for a
   * "dotAvery" bundle
   * 
   * @param target -
   *          the new AveryImage or AveryBackground field
   */
  protected void copyAttributesForDotAveryBundle(AveryPanelField target)
  {
    target.setHeight(getHeight());
    target.setPosition(getPosition());
    target.setDescriptionObject(getDescriptionObject());
    target.setPromptOrder(getPromptOrder());
    target.setRotation(getRotation());
    target.setWidth(getWidth());
    target.zLevel = this.zLevel;
    target.zLocked = this.zLocked;
    target.setID(getID());
    target.setGeometryId(getGeometryId());
    target.setContentID(getContentID());

    target.editable = editable;
    target.movable = movable;
    target.styleable = styleable;
    target.previewable = previewable;
    target.print = print;
    target.styleid = styleid; // included for completeness
    target.opacity = opacity;
    target.alienAttributes = alienAttributes;
  }

  static AveryPanelField getPanelField(Element element) throws Exception
  {
    AveryPanelField field = null;
    if (element.getName().equals("Avery.mp.field") || element.getName().equals("Avery.p.field"))
    {
      Element child = null;
      if ((child = element.getChild("Avery.pf.textblock")) != null)
      {
        field = new AveryTextblock(child);
      }
      else if ((child = element.getChild("Avery.pf.textline")) != null)
      {
        field = new AveryTextline(child);
      }
      else if ((child = element.getChild("Avery.pf.image")) != null)
      {
        field = new AveryImage(child);
      }
      else if ((child = element.getChild("Avery.pf.background")) != null)
      {
        field = new AveryBackground(child);
      }
      else if ((child = element.getChild("Avery.pf.textpath")) != null)
      {
        field = new TextPath(child);
      }
      else if ((child = element.getChild("Avery.pf.barcode")) != null)
      {
        field = new Barcode(child);
      }
    }
    return field;
  }

  private String convertId(String oldId)
  {
    if (oldId.equals("0"))
    {
      oldId = fieldTable.lastFullId;
    }

    if (oldId.length() > 7)
    {
      fieldTable.lastFullId = oldId;

      // its a flash one that must be converted
      if (fieldTable.flashHash.containsKey(oldId))
      {
        return (String) (fieldTable.flashHash.get(oldId));
      }
      else
      {
        String newId = "PF" + Integer.toString(fieldTable.flashId++);
        fieldTable.flashHash.put(oldId, newId);
        return newId;
      }
    }
    else
    {
      return oldId;
    }
  }

  protected void readOutLine(Element element)
  {
    Element outlineElement = element.getChild("outline", AveryProject.getAverysoftNamespace());
    if (outlineElement != null)
    {
      Outline result = new Outline();
      result.setWidth(Float.parseFloat(outlineElement.getAttributeValue("width")));
      if (element.getAttributeValue("opacity") != null)
      	result.setOpacity(Float.parseFloat(element.getAttributeValue("opacity")));
      else
      	result.setOpacity(1.0f);
      
      Color outlineColor;
      outlineColor = Color.decode(outlineElement.getAttributeValue("color"));
      result.setColor(outlineColor);
      this.outline = result;
    }
  }

  protected void readSuperAverysoftAttributes(Element element)
  {
    setID(element.getAttributeValue("id"));

    readOutLine(element);

    visible = !"false".equals(element.getAttributeValue("visible"));
    setWidth(Double.valueOf(element.getAttributeValue("width")));
    setHeight(Double.valueOf(element.getAttributeValue("height")));

    Element positionElement = element.getChild("position", AveryProject.getAverysoftNamespace());
    double x = 0;
    double y = 0;
    if (positionElement != null)
    {
      x = Double.parseDouble(positionElement.getAttributeValue("x"));
      y = Double.parseDouble(positionElement.getAttributeValue("y"));
    }
    setPosition(new TwipsPosition(x, y));

    setRotation(element.getAttributeValue("rotation"));
    setPromptOrder(element.getAttributeValue("promptOrder"));
    setZOrder(element.getAttributeValue("zOrder"));
    if (element.getAttributeValue("geometryId") != null)
      setGeometryId(element.getAttributeValue("geometryId"));

    setContentID(element.getAttributeValue("contentId"));

    if (element.getAttributeValue("editable") != null)
    {
      setEditable(element.getAttributeValue("editable").equals("true"));
    }

    if (element.getAttributeValue("movable") != null)
    {
      setMovable(element.getAttributeValue("movable").equals("true"));
    }

    if (element.getAttributeValue("styleable") != null)
    {
      setStyleable(element.getAttributeValue("styleable").equals("true"));
    }

    if (element.getAttributeValue("previewable") != null)
    {
      setPreviewable(element.getAttributeValue("previewable").equals("true"));
    }

    if (element.getAttributeValue("print") != null)
    {
      setPrint(element.getAttributeValue("print").equals("true"));
    }
    
    if (element.getAttributeValue("opacity") != null)
    {
      setOpacity(Double.valueOf(element.getAttributeValue("opacity")));
    }

    /** this is code to handle attributes from other namespaces. * */
    // List additionalNamespaces = element.getAdditionalNamespaces();
    // if (additionalNamespaces.size() > 0)
    // {
    // System.err.println("additional namespace found: " +
    // additionalNamespaces.get(0).toString());
    Iterator allAtts = element.getAttributes().iterator();
    while (allAtts.hasNext())
    {
      org.jdom.Attribute att = (Attribute) allAtts.next();
      if (!att.getNamespace().equals(Namespace.NO_NAMESPACE))
      {
        if (!att.getNamespace().equals(AveryProject.getAverysoftNamespace()))
        {
          // System.err.println("Alien attribute " + att.toString() + " " +
          // att.getNamespace().toString());
          alienAttributes.add(att.clone());
        }
      }
    }
    // }
  }

  protected void readSuperAttributes(Element element)
  {
    readOutLine(element);
    setWidth(Double.valueOf(element.getAttributeValue("width")));
    setHeight(Double.valueOf(element.getAttributeValue("height")));
    setPosition(new TwipsPosition(element.getAttributeValue("position")));
    setRotation(element.getAttributeValue("rotation"));
    setDescription(element.getAttributeValue("prompt"));
    setPromptOrder(element.getAttributeValue("promptOrder"));
    setZOrder(element.getAttributeValue("zOrder"));
  }

  void setProjectFieldTable(ProjectFieldTable newFieldTable)
  {
    fieldTable = newFieldTable;

    // fixup FlashPrint ids
    /*
     * id = convertId(id); contentid = convertId(contentid); if (styleid !=
     * null) { styleid = convertId(styleid); }
     */
  }

  public ProjectFieldTable getProjectFieldTable()
  {
    return fieldTable;
  }

  public void createID()
  {
    setID(fieldTable.nextRefID());
  }

  public List getAlienAttributes()
  {
  	return alienAttributes;
  }
  
  // draw() is implemented in the subclasses
  /**
   * Scales the field's content by the given scalar and draws it onto the given
   * Graphics2D surface.
   * 
   * @param gr -
   *          the Graphics2D to draw on
   * @param dScalar -
   *          the scaling factor. Multiply native units (twips) by
   *          <code>dScalar</code> before drawing.
   */
  abstract void draw(Graphics2D gr, double dScalar);

  /**
   * Adds the field's content to the given PDF document
   * 
   * @param pdfPage -
   *          the pdf page under construction
   * @param offset -
   *          the position in twips of the containing {@link AveryPanel},
   *          relative to the upper left corner of the page
   * @param dPageHeight -
   *          the page height in twips, necessary for coordinate reversal
   * @param pdfImageCache -
   *          prevents redundant image data in PDF file
   * @throws Exception
   *           if something goes horrible wrong
   */
  public abstract void addToPDF(PDFPage pdfPage, Double dPageHeight, java.util.Hashtable pdfImageCache) throws Exception;

  /**
   * The matches() method returns <code>true</code> if another field is a
   * clone of this one or if it references the same object. It's similar to
   * String.equals() but it avoids any Object.hashCode() implications. This
   * method is overridden in the derived classes, which ultimately call their
   * superclass as well.
   * 
   * @param field
   *          the field to test against
   * @return <code>true</code> if the contents of the field match the contents
   *         of <code>this</code>, <code>false</code> otherwise
   */
  boolean matches(AveryPanelField field)
  {
    if (field.equals(this)) // is it the same Object?
      return true;

    // all of the properties must match to return true
    if (field.getWidth().equals(getWidth()))
      if (field.getHeight().equals(getHeight()))
        if (field.getRotation().equals(getRotation()))
          if (field.getPrompt().equals(getPrompt()))
            if (field.getPosition().equals(getPosition()))
              if (field.getPromptOrder().equals(getPromptOrder()))
                if (field.zLevel == this.zLevel && field.zLocked == this.zLocked)
                  if (field.getVisible() == getVisible())
                    if (field.getPrint() == getPrint())
                      if (field.isEditable() == isEditable())
                        if (field.isMovable() == isMovable())
                          if (field.isStyleable() == isStyleable())
                            if (field.isPreviewable() == isPreviewable())
                            	return true;

    // not a perfect match
    return false;
  }

  // flags to determine what changed when an AveryPanelField is edited.
  /** Bitmask for {@link #differences} method */
  static final int WIDTH_DIFFERS = 1;
  /** Bitmask for {@link #differences} method */
  static final int HEIGHT_DIFFERS = 2;
  /** Bitmask for {@link #differences} method */
  static final int PROMPT_DIFFERS = 4;
  /** Bitmask for {@link #differences} method */
  static final int POSITION_DIFFERS = 5;
  /** Bitmask for {@link #differences} method */
  static final int PROMPT_ORDER_DIFFERS = 6;
  /** Bitmask for {@link #differences} method */
  static final int Z_ORDER_DIFFERS = 7;
  /** Bitmask for {@link #differences} method */
  static final int TEXTCONTENT_DIFFERS = 10;
  /** Bitmask for {@link #differences} method */
  static final int TYPEFACE_DIFFERS = 11;
  /** Bitmask for {@link #differences} method */
  static final int TEXTCOLOR_DIFFERS = 12;
  /** Bitmask for {@link #differences} method */
  static final int POINTSIZE_DIFFERS = 13;
  /** Bitmask for {@link #differences} method */
  static final int JUSTIFICATION_DIFFERS = 14;
  /** Bitmask for {@link #differences} method */
  static final int STYLE_DIFFERS = 15;
  /** Bitmask for {@link #differences} method */
  static final int VALIGN_DIFFERS = 14;
  /** Bitmask for {@link #differences} method */
  static final int IMAGECONTENT_DIFFERS = 20;
  /** Bitmask for {@link #differences} method */
  static final int APPLES_AND_ORANGES = 63;

  /**
   * Compares this field with another AveryPanelField and returns a BitSet
   * describing how they differ. The bits of the BitSet are defined in the
   * AveryPanelField base class. If the field is an instance of a different
   * class, the APPLES_AND_ORANGES bit is set and the comparison goes no
   * further.
   * 
   * @param field -
   *          the field being tested against
   * @return BitSet describing the difference
   */
  BitSet differences(AveryPanelField field)
  {
    BitSet flags = new BitSet();

    if (false == getClass().isInstance(field))
    {
      flags.set(APPLES_AND_ORANGES);
    }
    else
    // set individual flags for this base class
    {
    }

    return flags;
  }

  /**
   * This method modifies the field, if it exists, to match recent changes in
   * another field. It is used to update fields that have been made obsolete by
   * an updated Masterpanel (hence the name).
   * 
   * @param field -
   *          the field to pull changes from.
   * @param flags -
   *          describes what attributes to update
   * @return the number of modifications made
   */
  int masterUberAlles(AveryPanelField field, BitSet flags)
  {
    // update this base class as required
    return 0;
  }

  /**
   * Sets the id of the field
   * 
   * @param newID -
   *          the desired id
   */
  public void setID(String newID)
  {
  	//System.out.println("apf setID: oldID, newID=" + getID() + "," + newID);
    id = newID;
  }

  /**
   * Retrieves the id of the field
   * 
   * @return the field's id
   */
  public String getID()
  {
    return id;
  }

  /**
   * Sets the content id of the field
   * 
   * @param newContentID -
   *          the desired id
   */
  public void setContentID(String newContentID)
  {
    contentid = newContentID;
  }

  /**
   * Retrieves the content id of the field
   * 
   * @return the field's content id
   */
  public String getContentID()
  {
    return contentid;
  }

  /**
   * Sets the style id of the field
   * 
   * @param newStyleID -
   *          the desired id
   */
  public void setStyleID(String newStyleID)
  {
    styleid = newStyleID;
  }

  /**
   * Retrieves the Style id of the field
   * 
   * @return the field's style id
   */
  public String getStyleID()
  {
    return styleid;
  }

  /**
   * Sets the width of the field in twips
   * 
   * @param newWidth -
   *          the desired width
   */
  public void setWidth(Double newWidth)
  {
    width = newWidth;
  }

  /**
   * Retrieves the width of the field in twips
   * 
   * @return the field's width
   */
  public Double getWidth()
  {
    return width;
  }

  /**
   * Sets the height of the field in twips
   * 
   * @param newHeight -
   *          the desired height
   */
  public void setHeight(Double newHeight)
  {
  	//System.out.println("height=" + newHeight.intValue());
    height = newHeight;
  }

  /**
   * Retrieves the height of the field in twips
   * 
   * @return the field's height
   */
  public Double getHeight()
  {
    return height;
  }

  /**
   * Sets the rotation of the field about its position
   * 
   * @param newRotation
   *          the desired rotation in degrees
   */
  public void setRotation(Double newRotation)
  {
    rotation = newRotation;
  }

  /**
   * Sets the rotation of the field about its position.
   * 
   * @param str -
   *          describes the rotation in degrees. If the string cannot be parsed,
   *          rotation is set to zero degrees.
   */
  public void setRotation(String str)
  {
    try
    {
      rotation = Double.valueOf(str);
    }
    catch (Exception e)
    {
      // on failure, rotation is set to the default 0.0
      rotation = new Double(0.0);
      return;
    }
  }

  /**
   * Retrieves the rotation of the field about its position point
   * 
   * @return the amount of rotation in degrees
   */
  public Double getRotation()
  {
    return rotation;
  }

  /**
   * Sets the prompt string for this field. The prompt string is typically
   * presented by the user interface of an application to identify the field for
   * editing. Note that the prompt as a string has been replaced internally by
   * the localizable Description object.
   * 
   * @param newPrompt -
   *          the desired prompt string
   * @deprecated use #setDescription(int) or #setDescription(String) instead
   */
  public void setPrompt(String newPrompt)
  {
    setDescription(newPrompt);
  }

  /**
   * Retrieves the prompt string
   * 
   * @return the field's prompt string
   * @see #setPrompt
   */
  public String getPrompt()
  {
    return description.getText();
  }

  /**
   * Sets the field's position, relative to the containing panel. The position
   * is defined as the upper left corner of the field, measures in twips from
   * the upper left corner of the panel.
   * 
   * @param newPosition -
   *          the desired position
   */
  public void setPosition(java.awt.Point newPosition)
  {
    this.position = new TwipsPosition(newPosition.getX(), newPosition.getY());

  }

  void setPosition(TwipsPosition newPosition)
  {
    this.position = newPosition;
  }

  /**
   * retrieves the position
   * 
   * @return the field's position on the panel, in twips
   * @see #setPosition
   */
  public java.awt.Point getPosition()
  {
    return this.position.scaledPoint(1.0);
  }

  /**
   * Sets the visibility of the field
   * 
   * @param newVisible -
   *          the desired visibility
   */
  public void setVisible(boolean newVisible)
  {
    visible = newVisible;
  }

  /**
   * Retrieves the visibility of the field
   * 
   * @return the field's visibility
   */
  public boolean getVisible()
  {
    return visible;
  }

  /**
   * @return Returns the editable.
   */
  public boolean isEditable()
  {
    return editable;
  }

  /**
   * @param editable
   *          The editable to set.
   */
  public void setEditable(boolean editable)
  {
    this.editable = editable;
  }

  /**
   * Sets the printing state of the field
   * 
   * @param newPrint -
   *          the desired printing state
   */
  public void setPrint(boolean newPrint)
  {
    print = newPrint;
  }

  /**
   * Retrieves the printing state of the field
   * 
   * @return the field's printing state
   */
  public boolean getPrint()
  {
    return print;
  }

  protected Description getDescriptionObject()
  {
    return description;
  }

  /**
   * Sets the opacity of the field
   * 
   * @param newOpacity -
   *          the desired opacity
   */
  public void setOpacity(Double newOpacity)
  {
  	opacity = newOpacity;
  }

  /**
   * Retrieves the opacity of the field
   * 
   * @return the field's opacity
   */
  public Double getOpacity()
  {
    return opacity;
  }
  
 protected void setDescriptionObject(Description newDescription)
  {
    description = newDescription;
  }

  public void translateDescription(String language)
  {
    description.setLanguage(language);
  }

  /**
   * Creates a copy of this AveryPanelField
   * 
   * @return the copy
   */
  public Object clone()
  {
    try
    {
      return super.clone();
    }
    catch (CloneNotSupportedException e)
    {
      // This should never happen
      throw new InternalError(e.toString());
    }
  }

  /**
   * Creates a copy of the private data for this AveryPanelField. Called by
   * derived class's deepClone() method.
   * 
   * @param newField -
   *          the field to copy this field's data to
   */
  protected void copyPrivateData(AveryPanelField newField)
  {
    // fill in the private data from this base class
  	
    newField.setWidth(getWidth());
    newField.setHeight(getHeight());
    newField.setRotation(getRotation());
    newField.setPosition(getPosition());
    newField.setDescriptionObject(new Description(description));
    newField.setPromptOrder(promptOrder);
    newField.zLevel = this.zLevel;
    newField.zLocked = this.zLocked;
    newField.setVisible(visible);
    newField.setEditable(editable);
    newField.setMovable(true);
    newField.setStyleable(true);
    newField.setPreviewable(true);
    newField.setPrint(print);
    newField.setOpacity(opacity);
    newField.setAveryElementName(averyElementName);
    newField.setProjectFieldTable(fieldTable);
    newField.setID(id);
    newField.setStyleID(styleid);
    newField.setGeometryId(geometryId);
    newField.setContentID(contentid);
  }

  /**
   * Generates an HTML-formatted string that describes the field. This can be
   * useful for debugging.
   * 
   * @return HTML-formatted String
   */
  String dump()
  {
    String str;
    str = "<br>width = " + width.toString();
    str += "<br>height = " + height.toString();
    str += "<br>position = " + position.toString();
    str += "<br>rotation = " + rotation.toString();
    str += "<br>description = " + getDescription();
    return str;
  }

  /**
   * For debugging, this draws the outline of the field into the PDF document.
   * 
   * @param pdfPage -
   *          the pdf page under construction
   * @param offset -
   *          the position in twips of the containing {@link AveryPanel},
   *          relative to the upper left corner of the page
   * @param dPageHeight -
   *          the page height in twips, necessary for coordinate reversal
   * @throws Exception
   *           if the pdfDoc methods fail
   */
  void addOutlineToPDF(PDFPage pdfPage, Point offset, Double dPageHeight) throws Exception
  {
    // test by drawing a rectangle
    PDFStyle style = new PDFStyle();
    style.setLineColor(Color.red);
    pdfPage.setStyle(style);

    // calc coordinates in pointspace, not twipspace (20 twips = 1 point)
    float w = getWidth().floatValue() / 20.f;
    float h = getHeight().floatValue() / 20.f;
    float x = (float) (offset.x + getPosition().x) / 20.f;
    float y = (float) (offset.y + getPosition().y) / 20.f;
    // y starts at bottom of page, not top
    y = (dPageHeight.floatValue() / 20.f) - y;
    // box drawing starts at bottom left
    y -= h;

    pdfPage.pathMove(x, y);
    pdfPage.pathLine(x + w - 1.f, y);
    pdfPage.pathLine(x + w - 1.f, y + h - 1.f);
    pdfPage.pathLine(x, y + h - 1.f);
    pdfPage.pathClose();
    pdfPage.pathPaint();

  }

  /**
   * The promptOrder String describes a floating point number that represents
   * the position of the field's prompt in a list of all of the fields in the
   * current panel. This is useful when an application is presenting a list of
   * fields to be edited. Note that a promptOrder of "0" (or an unparsable
   * String) may be used by an application to prevent field editing.
   * 
   * @param newPromptOrder -
   *          the desired prompt order
   */
  public void setPromptOrder(String newPromptOrder)
  {
    promptOrder = newPromptOrder;
  }

  /**
   * Retrieves the promptOrder String
   * 
   * @return the promptOrder String
   * @see #setPromptOrder
   */
  public String getPromptOrder()
  {
    return promptOrder;
  }

  /**
   * The zOrder String describes a floating point number that represents both
   * the position of a field in the drawing order and its locked state. Fields
   * on a panel are sorted by the absolute value of zOrder (the zLevel), and the
   * fields are then drawn in ascending order. The zLocked attribute is set if
   * the zOrder is a negative number
   * 
   * @param newZOrder -
   *          the desired z order
   */
  private void setZOrder(String newZOrder)
  {
    double z = Double.parseDouble(newZOrder);
    zLevel = Math.abs(z);
    zLocked = z < 0;
  }

  /**
   * sets the zLevel component of zOrder without affecting the zLocked flag
   * 
   * @param z
   */
  public void setZLevel(double z)
  {
    zLevel = Math.abs(z);
  }

  /**
   * Retrieves the serializable zOrder String
   * 
   * @return the numeric zOrder String, with a minus sign if zLocked is set
   */
  private String getZOrderString()
  {
    return Double.toString(zLocked ? -zLevel : zLevel);
  }

  /**
   * @return the drawing level for this field (always positive)
   */
  public double getZLevel()
  {
    return zLevel;
  }

  public boolean isZLocked()
  {
    return zLocked;
  }

  public void setZLocked(boolean newLock)
  {
    zLocked = newLock;
  }

  /**
   * Rotates field and places it in pdf document
   * 
   * @param pdfCanvas -
   *          target
   * @param offset -
   *          panel position in twips
   * @param dPageHeight -
   *          page height in twips
   * @param positionx -
   *          field x position on panel in twips
   * @param positiony -
   *          field y position on panel in twips
   * @param panelRotation -
   *          panel rotation in degrees
   * @param fieldRotation -
   *          field rotation in degrees
   * @throws Exception
   *           if a pdfPage method fails
   */
  void transformPDFField(PDFCanvas pdfCanvas, Double dPanelHeight, float positionx, float positiony, double fieldRotation)
      throws Exception
  {
    double theta = 0.0;

    // compute field upper left corner in points, relative to panel lower left
    // corner
    float fieldpositionx = getPosition().x / 20.0f;
    float fieldpositiony = (dPanelHeight.intValue() - getPosition().y) / 20.0f;

    // translate field lower left corner to field coordinates
    // the origin is now the field upper left corner
    // positive y axis points up in this coordinate system
    float deltax = positionx - fieldpositionx;
    float deltay = positiony - fieldpositiony;

    // System.out.println("fieldpositionx= " + new
    // Float(fieldpositionx).toString());
    // System.out.println("fieldpositiony= " + new
    // Float(fieldpositiony).toString());
    // System.out.println("positionxold= " + new Float(positionx).toString());
    // System.out.println("positionyold= " + new Float(positiony).toString());
    // System.out.println("deltax= " + new Float(deltax).toString());
    // System.out.println("deltay= " + new Float(deltay).toString());

    // convert field angle from degrees to radians
    theta = fieldRotation * Math.PI / 180.0;
    // System.out.println("theta= " + new Double(theta).toString());
    float sintheta = (float) Math.sin(theta);
    float costheta = (float) Math.cos(theta);

    // rotate the field around the field upper left corner
    float xprime = deltax * costheta - deltay * sintheta;
    float yprime = deltax * sintheta + deltay * costheta;
    // System.out.println("xprime= " + new Float(xprime).toString());
    // System.out.println("yprime= " + new Float(yprime).toString());
    if (xprime > -.01 && xprime < .01)
      xprime = 0.0f;
    if (yprime > -.01 && yprime < .01)
      yprime = 0.0f;

    // translate field lower left corner back to page coordinates
    positionx = fieldpositionx + xprime;
    positiony = fieldpositiony + yprime;

    // calculate the rotation matrix
    float a = costheta;
    float b = sintheta;
    float c = -sintheta;
    float d = costheta;

    // set up the transform matrix for subsequent rendering

    String concat = formatConcat(a, b, c, d, positionx, positiony);
    pdfCanvas.rawWrite(concat);
  }

  /**
   * Calculates the position and size of rotated field <br>
   * NOTE: Passes Point and Dimension objects that are modified on return
   * 
   * @param angle -
   * @param position
   *          upper left corner of unrotated field in twips
   * @param size
   *          Dimension in twips of unrotated field
   */
  void rotateFieldRect(double angle, java.awt.Point position, java.awt.Dimension size)
  {
    // System.out.println("old position= " + position.toString());
    double width = (double) size.width;
    double height = (double) size.height;
    // System.out.println("old width height= " + new Double(width).toString() +
    // " " + new Double(height).toString());
    BufferedImage image = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_RGB);

    // find the rectangular coordinates of the rotated field,
    // rotating about upper left hand corner
    AffineTransform at1 = new AffineTransform();
    // create rectangle size metrics of rotated field
    at1.rotate(-angle);
    AffineTransformOp op1 = new AffineTransformOp(at1, AffineTransformOp.TYPE_BILINEAR);
    Rectangle2D r2d = op1.getBounds2D(image);
    image = null;
    size.width = (int) r2d.getWidth();
    size.height = (int) r2d.getHeight();
    // System.out.println("new width height= " + new
    // Double(size.width).toString() + " " + new
    // Double(size.height).toString());
    // calculate the starting point from the rotated rectangle
    position.x += r2d.getMinX();
    position.y += r2d.getMinY();
    // System.out.println("new position= " + position.toString());
  }

  /**
   * Get the bounding rect of this field
   * 
   * @return a bounding rectangle
   */
  public Rectangle getBoundingRect()
  {
    Rectangle rect = new Rectangle(getPosition().x, getPosition().y, getWidth().intValue(), getHeight().intValue());

    if (getRotation().doubleValue() != 0.0)
    {
      // find the rectangular coordinates of the rotated field,
      // rotating about upper left hand corner
      AffineTransform at1 = new AffineTransform();
      double angle = getRotation().doubleValue() * Math.PI / 180.0;
      at1.rotate(-angle);
      // Create a rotated rectangle shape
      Shape shape = at1.createTransformedShape(new Rectangle2D.Double(0.0, 0.0, rect.getWidth(), rect.getHeight()));
      // get its bounding rect
      Rectangle2D r2d = shape.getBounds2D();

      // assign results into original rect
      rect.width = (int) r2d.getWidth();
      rect.height = (int) r2d.getHeight();
      // System.out.println("new width height= " + rect.getSize().toString()));
      // calculate the starting point from the rotated rectangle
      rect.x += r2d.getMinX();
      rect.y += r2d.getMinY();
      // System.out.println("new position= " + rect.getLocation().toString());
    }

    return rect;
  }

  // area aize for PDFCanvas
  public int getAreaSize(PDFPage pdfPage, int canvasStartX, int canvasStartY)
  {
    int areaSize = (int) (pdfPage.getWidth() > pdfPage.getHeight() ? pdfPage.getWidth() - canvasStartX : pdfPage.getHeight()
        - canvasStartY);
  	
    // it may be a tiled project - use panel field bounds to decide
    Rectangle rect = getBoundingRect();
    int maxPos = Math.max((int)(rect.getMaxX() + 1), (int)(rect.getMaxY() + 1));
    int tiledAreaSize = maxPos / 20;
    if (tiledAreaSize > areaSize)
    {
    	// is tiled, area size is multiplied by max tile the field occupies
    	tiledAreaSize = tiledAreaSize / areaSize + 1;
    	tiledAreaSize = tiledAreaSize * areaSize;
    	areaSize = tiledAreaSize * areaSize;
    }
    
    return areaSize;
  }
  

  /**
   * Some fields (like AveryImage) require the presence of an external file.
   * 
   * @return fully qualified filename, or <code>null</code>
   */
  public String getAssociatedFilename()
  {
    return null;
  }

  /**
   * used by derived class implementations of getAveryElement
   */
  private String averyElementName = "Avery.p.field";

  private String getAveryElementName()
  {
    return averyElementName;
  }

  /**
   * Set the prefix to be used as part of the Element name in a subsequent call
   * to getAveryElement
   * 
   * @param name -
   *          prefix, typically "Avery.p.field" or "Avery.mp.field"
   */
  void setAveryElementName(String name)
  {
    averyElementName = name;
  }

  /**
   * Called by derived class getAveryElement, to get the parent Element that
   * will hold class-specific content.
   * 
   * @return an Avery.p.field or Avery.pf.field Element with Attributes but no
   *         content
   */
  protected Element getParentAveryElement()
  {
    Element element = new Element(getAveryElementName());
    // add attributes
    element.setAttribute("width", getWidth().toString());
    element.setAttribute("height", getHeight().toString());

    element.setAttribute("position", Double.toString(getPosition().getX()) + "," + Double.toString(getPosition().getY()));

    element.setAttribute("rotation", getRotation().toString());
    element.setAttribute("prompt", getPrompt());
    element.setAttribute("promptOrder", getPromptOrder());
    element.setAttribute("zOrder", getZOrderString());

    // caller will add child element content
    return element;
  }

  protected void applyParentAverysoftAttributes(Element element)
  {
    readOutLine(element);
    // add attributes
    // String myID = getID();

    element.setAttribute("id", getID());

		element.setAttribute("width", getWidth().toString());
		element.setAttribute("height", getHeight().toString());

    String.valueOf(getRotation().intValue());

    element.setAttribute("rotation", String.valueOf(getRotation().intValue()));
    element.setAttribute("promptOrder", getPromptOrder());
    element.setAttribute("zOrder", getZOrderString());
    if (getGeometryId().length() > 0)
    	element.setAttribute("geometryId", getGeometryId());

    if (!getVisible())
    {
      element.setAttribute("visible", "false");
    }

    element.setAttribute("contentId", getContentID());

    if (!isEditable())
      element.setAttribute("editable", "false");

    if (!isMovable())
      element.setAttribute("movable", "false");

    if (!isStyleable())
      element.setAttribute("styleable", "false");

    if (!isPreviewable())
      element.setAttribute("previewable", "false");

     if (!getPrint())
      element.setAttribute("print", "false");
    
    if (opacity.doubleValue() >= 0 && opacity.doubleValue() < 1.0)
    	element.setAttribute("opacity", getOpacity().toString());

    // restore any alien attributes (see readSuperAverysoftAttributes())
    if (alienAttributes.size() > 0)
    {
      Iterator atts = alienAttributes.iterator();
      while (atts.hasNext())
      {
        element.setAttribute(((Attribute) atts.next()).detach());
      }
    }
  }

  protected void applyParentAverysoftIBAttributes(Element element)
  {
    readOutLine(element);
    // add attributes
    // String myID = getID();

    //element.setAttribute("id", getID());

		element.setAttribute("width", AveryProject.twipsToMM(getWidth().doubleValue()));
		element.setAttribute("height", AveryProject.twipsToMM(getHeight().doubleValue()));


    String.valueOf(getRotation().intValue());

    element.setAttribute("rotation", String.valueOf(getRotation().intValue()));
    //element.setAttribute("promptOrder", getPromptOrder());
    element.setAttribute("zOrder", getZOrderString());
    //if (getGeometryId().length() > 0)
    	//element.setAttribute("geometryId", getGeometryId());

    if (!getVisible())
    {
      element.setAttribute("visible", "false");
    }

    //element.setAttribute("contentId", getContentID());

    if (!isEditable())
      element.setAttribute("editable", "false");

    if (!isMovable())
      element.setAttribute("movable", "false");

    if (!isStyleable())
      element.setAttribute("styleable", "false");

    if (!isPreviewable())
      element.setAttribute("previewable", "false");

     if (!getPrint())
      element.setAttribute("print", "false");
    
    if (opacity.doubleValue() >= 0 && opacity.doubleValue() < 1.0)
    	element.setAttribute("opacity", getOpacity().toString());

    // restore any alien attributes (see readSuperAverysoftAttributes())
    if (alienAttributes.size() > 0)
    {
      Iterator atts = alienAttributes.iterator();
      while (atts.hasNext())
      {
        element.setAttribute(((Attribute) atts.next()).detach());
      }
    }
  }
  /**
   * This is called when panel orientation has been changed. In most derived
   * classes it does nothing, but in AveryBackground the width and height are
   * swapped.
   */
  void reorient()
  {
  }

  // BFOPDF forces us to format our own concat matrix and output it
  // using a writeRaw method...
  static String formatConcat(float a, float b, float c, float d, float e, float f)
  {
    a = validateFloatRange(a);
    b = validateFloatRange(b);
    c = validateFloatRange(c);
    d = validateFloatRange(d);
    e = validateFloatRange(e);
    f = validateFloatRange(f);
    
    Float A = new Float(a);
    Float B = new Float(b);
    Float C = new Float(c);
    Float D = new Float(d);
    Float E = new Float(e);
    Float F = new Float(f);

    String formatString = "%.4f %.4f %4.4f %.4f %.4f %.4f cm";
    Object[] transArgs = new Object[] {A, B, C, D, E, F};
    
    return String.format(formatString, transArgs);
    
    //String result = String.format(formatString, transArgs);
    //System.out.println(result);
    //return result;
    
    // Roman found this could produce scientific notation!!
    //return (String.valueOf(a) + " " + String.valueOf(b) + " " + String.valueOf(c) + " " + String.valueOf(d) + " "
      //  + String.valueOf(e) + " " + String.valueOf(f) + " cm");
  }

  static float validateFloatRange(float afloat)
  {
    float bfloat = afloat;
    if (afloat > -.0001f && afloat <= .0001f)
      bfloat = 0f;
    else if (afloat < -1000000000f)
      bfloat = -1000000000f;
    else if (bfloat > 1000000000f)
      bfloat = 1000000000f;

    return bfloat;
  }

  public java.util.List getLocalizableDescriptions(String language)
  {
    return getDescriptionObject().getLocalizedStringList(language);
  }

  /**
   * Note: in averysoft XML, the field prompt is encased in a localizable
   * description element. The prompt as a simple string is being phased out.
   * This method is provided for backwards compatibility. It creates a new
   * Description object with custom text and applies it to the field.
   * 
   * @param newPrompt
   *          the field description string
   */
  public void setDescription(String newPrompt)
  {
    getDescriptionObject().setText(newPrompt);
  }

  public void setDescription(int index)
  {
    getDescriptionObject().setIndex(index);
  }

  public String getDescription()
  {
    return getDescriptionObject().getText();
  }

  /**
   * Every panelField must have a Description object with the appropriate
   * translation tables. This method, called from the constructor, should do
   * that. </b>@see AveryImage#addDefaultDescriptionObject for a sample
   * implementation using a Description factory method.
   */
  abstract protected void addDefaultDescriptionObject();

  abstract public AveryPanelField deepClone();

  /**
   * Always returns <code>false</code> in the base class. Override in fields
   * that support mergeMaps
   * 
   * @param key
   * @return <code>true</code> if the field contains a mergemap line matching
   *         the specified mergemap key ("AF0", "AF1", etc.)
   */
  boolean hasMergeKey(String key)
  {
    return false;
  }

  abstract void replaceStyle(StyleSet original, StyleSet replacement);

  abstract boolean matchesStyle(StyleSet style);

  public boolean hasCustomDescription()
  {
    return description.isCustom();
  }

  public Mask getMask()
  {
    return mask;
  }

  public void setMask(Mask mask)
  {
    this.mask = mask;
  }

  public String getGeometryId()
  {
    return geometryId;
  }

  public void setGeometryId(String geometryId)
  {
    this.geometryId = geometryId;
  }

  public boolean isMovable()
  {
    return movable;
  }

  public void setMovable(boolean movable)
  {
    this.movable = movable;
  }

  public boolean isStyleable()
  {
    return styleable;
  }

  public void setStyleable(boolean styleable)
  {
    this.styleable = styleable;
  }

  public boolean isPreviewable()
  {
    return previewable;
  }

  public void setPreviewable(boolean previewable)
  {
    this.previewable = previewable;
  }

  protected Outline outline = null;

  /**
   * Prints field outline to pdf
   * 
   * @param pdfPage
   * @param dPanelHeight
   * @throws Exception
   */
  public void addOutlineToPDF(PDFPage pdfPage, Double dPanelHeight) throws Exception
  {
    if (outline == null)
      return;

    float pixelOutlinewWidth = outline.getWidth() / 20.0f;

    int canvasStartX = 0;
    int canvasStartY = 0;
    double heightCalibration = 0;
    if (getPosition().x < 0)
    {
      canvasStartX = (int) ((getPosition().x - outline.getWidth()) / 20f);
    }
    if (getPosition().y < 0)
    {
      canvasStartY = (int) ((getPosition().y - outline.getWidth()) / 20f);
      heightCalibration = -getPosition().y;
    }

    int areaSize = (int) (pdfPage.getWidth() > pdfPage.getHeight() ? pdfPage.getWidth() - canvasStartX : pdfPage.getHeight()
        - canvasStartY);
    areaSize += pixelOutlinewWidth * 2f;
    PDFCanvas contentContainer = new PDFCanvas(areaSize, areaSize, outline.getOpacity());

    if (getMask() != null)
    {
      getMask().clipMask(contentContainer, canvasStartX, canvasStartY);
    }

    float height = getHeight().floatValue() / 20.0f;
    float width = getWidth().floatValue() / 20.0f;
    float top = (dPanelHeight.intValue() - getPosition().y) / 20.0f - canvasStartY;
    float left = getPosition().x / 20.0f - canvasStartX;

    // save old graphics transform/color state
    contentContainer.save();

    // translate and rotate the target space
    transformPDFField(contentContainer, new Double(dPanelHeight.doubleValue() + heightCalibration), left, top - height,
        getRotation().doubleValue());

    PDFStyle style = new PDFStyle();
    style.setLineWeighting(pixelOutlinewWidth);
    style.setLineColor(outline.getColor());

    contentContainer.setStyle(style);

    //contentContainer.drawRectangle(0, 0, width + pixelOutlinewWidth, height + pixelOutlinewWidth);

    contentContainer.restore();
    pdfPage.drawCanvas(contentContainer, canvasStartX - pixelOutlinewWidth / 2, canvasStartY - pixelOutlinewWidth / 2,
        areaSize + canvasStartX, areaSize + canvasStartY);
  }

  AlphaComposite makeComposite(float alpha)
  {
    int type = AlphaComposite.SRC_OVER;
    return(AlphaComposite.getInstance(type, alpha));
  }


  /**
   * Prints field outline to preview
   * 
   * @param gr
   * @param dScalar
   */
  void drawOutlineElement(Graphics2D gr, double dScalar)
  {
    if (outline == null)
      return;

    Point positionOrig = getPosition();
    Point position = new Point(positionOrig);

    // System.out.println("twips x, y= " + position.toString());
    // convert position to pixels
    position.x = (int) ((((double) position.x) - outline.getWidth() / 2) * dScalar);
    position.y = (int) ((((double) position.y) - outline.getWidth() / 2) * dScalar);
    // System.out.println("pixels x, y= " + position.toString());

    //int iWidth = (int) ((getWidth().floatValue() + outline.getWidth()) * dScalar);
    //int iHeight = (int) ((getHeight().floatValue() + outline.getWidth()) * dScalar);
    // position.y -= iHeight;

    Composite originalComposite = gr.getComposite();
    Stroke oldStroke = gr.getStroke();

    gr.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, outline.getOpacity()));
    gr.setStroke(new BasicStroke((float) (outline.getWidth() * dScalar)));
    gr.setColor(outline.getColor());
    //gr.drawRect(position.x, position.y, iWidth, iHeight);

    gr.setStroke(oldStroke);
    gr.setComposite(originalComposite);
  }
}
