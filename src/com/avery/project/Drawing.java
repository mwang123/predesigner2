/*
 * Drawing.java Created on Nov 14, 2007 by leeb Copyright 2007 Avery Dennison
 * Corp., All Rights reserved.
 */
package com.avery.project;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import java.util.Iterator;
import java.io.File;
import java.io.StringReader;

import javax.imageio.ImageIO;
//import java.util.Exception;

import org.faceless.pdf2.CMYKColorSpace;
import org.faceless.pdf2.PDFCanvas;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFStyle;
import org.faceless.pdf2.SpotColorSpace;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.avery.Averysoft;
import com.avery.predesign.StyleSet;
import com.avery.project.shapes.AveryShape;
import com.avery.project.shapes.PolygonShape;
import com.avery.project.shapes.PolygonShape.PolyCurve;
import com.avery.project.shapes.PolygonShape.PolyPoint;
import com.avery.project.shapes.PolygonShape.PolyQuad;
import com.avery.utils.AveryException;

/**
 * @author leeb Nov 14, 2007 Drawing
 */
public class Drawing extends AveryPanelField implements Averysoftable
{
  private AveryShape shape;

  private Color color;
  private boolean maintainAspect = false;
  private double outlineWidth = 0.0;
  private Color outlineColor = null;
  private float outlineOpacity;
  private String spotColor = null;

  private double lineThickness = -1.0f;			// for rect shapes in predesigner only, in points
  
  private boolean bFirstTimeOnly = true;
  
  //private static int CONVERT_TO_IMAGE_THRESHOLD = 32;
  //private String imageName = null;
  
  /**
   * Creates a Drawing object from a JDOM Element representing an Averysoft XML
   * drawing object
   */
  Drawing(Element element) throws Exception
  {
    shape = AveryShape.manufactureShape(element);
    
    String sColor = element.getAttributeValue("color");
    color = Color.decode(sColor);

    maintainAspect = "true".equals(element.getAttributeValue("maintainAspect"));

    readSuperAverysoftAttributes(element);

    Element outlineElement = element.getChild("outline", AveryProject.getAverysoftNamespace());
    if (outlineElement != null)
    {
      outlineWidth = Double.parseDouble(outlineElement.getAttributeValue("width"));
      sColor = outlineElement.getAttributeValue("color");
      outlineColor = Color.decode(sColor);
      if (element.getAttributeValue("opacity") != null)
      	outlineOpacity = Float.valueOf(element.getAttributeValue("opacity")).floatValue();
      else
      	outlineOpacity = 1.0f;
    }
    spotColor = element.getAttributeValue("spotColor");
    
    Element child;		// reusable local item

    Iterator iterator = element.getChildren("hint", AveryProject.getAverysoftNamespace()).iterator();
    while (iterator.hasNext())
    {
      child = (Element)iterator.next();
      if (child.getAttributeValue("name").equals("lineThickness"))
      	lineThickness = Double.parseDouble(child.getAttributeValue("value"));
    }

    setDescriptionObject(Description.makeDrawingFieldDescription(element.getChild("description", AveryProject
        .getAverysoftNamespace())));
  }

  public Drawing(AveryShape shape, double x, double y)
  {
    this.shape = shape;

    this.setPosition(new TwipsPosition(x, y));
    setDescriptionObject(Description.makeDrawingFieldDescription());
  }

  public boolean hasValidOutline()
  {
    return (outlineWidth > 0.0 && outlineColor != null);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.avery.project.AveryPanelField#draw(java.awt.Graphics2D, double)
   */
  void draw(Graphics2D graphics, double dScalar)
  {
    if (getMask() != null)
    {
      getMask().clipImageMask(graphics, dScalar);
    }

    drawOutlineElement(graphics, dScalar);
    
    double x = getPosition().x * dScalar;
    double y = getPosition().y * dScalar;

    GeneralPath path = shape.scaledGraphicsPath(dScalar);

    if (getRotation().doubleValue() != 0.0)
    {
      AffineTransform at = new AffineTransform();
      at.setToRotation(-(getRotation().doubleValue() * Math.PI / 180.0));
      path.transform(at);
    }

    AffineTransform at = new AffineTransform();
    at.setToTranslation(x, y);
    Shape transformedShape = path.createTransformedShape(at);

    int alpha = (int)(this.getOpacity().doubleValue() * 255);
    
    Color oldColor = graphics.getColor();
    graphics.setColor(new Color(color.getRed(),
				color.getGreen(),
				color.getBlue(),
				alpha));
    
    graphics.fill(transformedShape);
    if (hasValidOutline())
    {
      Stroke oldStroke = graphics.getStroke();
      graphics.setStroke(new BasicStroke((float) (outlineWidth * dScalar)));
      if (alpha == 0)
      	alpha = 255;
      graphics.setColor(new Color(outlineColor.getRed(),
      		outlineColor.getGreen(),
      		outlineColor.getBlue(),
  				alpha));
      graphics.draw(transformedShape);
      graphics.setStroke(oldStroke);
    }
    graphics.setColor(oldColor);

    graphics.setClip(null);
    if (getMask() != null && getMask().incrementIndex())
    {
      draw(graphics, dScalar);
    }
  }
  
  /*void drawOutline(Graphics2D gr, double dScalar)
  {
    Point positionOrig = getPosition();
    Point position = new Point(positionOrig);

    // System.out.println("twips x, y= " + position.toString());
    // convert position to pixels
    position.x = (int) ((((double) position.x) - outlineWidth / 2) * dScalar);
    position.y = (int) ((((double) position.y) - outlineWidth / 2) * dScalar);
    // System.out.println("pixels x, y= " + position.toString());

   // int iWidth = (int) ((getWidth().floatValue() + outline.getWidth()) * dScalar);
   // int iHeight = (int) ((getHeight().floatValue() + outline.getWidth()) * dScalar);
    // position.y -= iHeight;

    Composite originalComposite = gr.getComposite();
    Stroke oldStroke = gr.getStroke();

    gr.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, this.getOpacity().floatValue()));
    gr.setStroke(new BasicStroke((float) (outlineWidth * dScalar)));
    gr.setColor(outlineColor);
    //gr.drawRect(position.x, position.y, iWidth, iHeight);

    gr.setStroke(oldStroke);
    gr.setComposite(originalComposite);
  }*/

  /*
   * (non-Javadoc)
   * 
   * @see com.avery.project.AveryPanelField#addToPDF(org.faceless.pdf2.PDFPage,
   *      java.awt.Point, java.lang.Double, java.lang.Double,
   *      java.util.Hashtable)
   */
  /*public void addToPDF(PDFPage pdfPage, Double dPanelHeight, Hashtable pdfImageCache) throws Exception
  {
    addOutlineToPDF(pdfPage, dPanelHeight);
    int canvasStartX = 0;
    int canvasStartY = 0;
    double heightCalibration = 0;
    if (getPosition().x < 0)
    {
      canvasStartX = (int) (getPosition().x / 20f);
    }
    if (getPosition().y < 0)
    {
      canvasStartY = (int) (getPosition().y / 20f);
      heightCalibration = -getPosition().y;
    }

    int areaSize = (int) (pdfPage.getWidth() > pdfPage.getHeight() ? pdfPage.getWidth() - canvasStartX : pdfPage.getHeight()
        - canvasStartY);
    
    float fillOpacity = this.getOpacity().floatValue();
    float shapeOpacity = fillOpacity;
    if (fillOpacity == 0)
    	shapeOpacity = 1.0f;
    	
    PDFCanvas contentContainer = new PDFCanvas(areaSize, areaSize, shapeOpacity);

    if (getMask() != null)
    {
      getMask().clipMask(contentContainer, canvasStartX, canvasStartY);
    }
    // get everything into PostScript point space
    float height = getHeight().floatValue() / 20.0f;
    float width = getWidth().floatValue() / 20.0f;
    float top = (dPanelHeight.intValue() - getPosition().y) / 20.0f - canvasStartY;
    float left = getPosition().x / 20.0f - canvasStartX;

    // save old graphics transform/color state
    contentContainer.save();

    // translate and rotate the target space
    transformPDFField(contentContainer, new Double(dPanelHeight.doubleValue() + heightCalibration), left, top - height,
        getRotation().doubleValue());

    // don't fill if fillOpacity is 0
    PDFStyle style = new PDFStyle();
    if (fillOpacity > 0.f)	
    	style.setFillColor(color);
    else
    	style.setFillColor(null);
    
    style.setLineWeighting((float) outlineWidth / 20.0f);
    style.setLineColor(outlineWidth > 0 ? outlineColor : color);

    contentContainer.setStyle(style);

    if (isRectangle())
    {
      contentContainer.drawRectangle(0f, 0f, width, height);
    }
    else if (isEllipse())
    {
      contentContainer.drawEllipse(0f, 0f, width, height);
    }
    else if (isPolygon())
    {
      Iterator iter = ((PolygonShape) shape).getElementsIterator();
      boolean firstElemnt = true;
      while (iter.hasNext())
      {
        Object tmp = iter.next();
        if (tmp instanceof PolyPoint)
        {
          PolyPoint p = (PolyPoint) tmp;
          if (firstElemnt)
          {
            firstElemnt = false;
            contentContainer.pathMove(p.x / 20f, p.y / 20f);
          }
          else
          {
            contentContainer.pathLine(p.x / 20f, p.y / 20f);
          }
        }
        else if (tmp instanceof PolyQuad)
        {
          PolyQuad p = (PolyQuad) tmp;
          if (firstElemnt)
          {
            firstElemnt = false;
            contentContainer.pathMove(p.x1 / 20f, p.y1 / 20f);
          }
          else
          {
            contentContainer.pathLine(p.x1 / 20f, p.y1 / 20f);
            contentContainer.pathArc(p.x2 / 20f, p.y2 / 20f, p.x3 / 20f, p.y3 / 20f);
          }
        }
        else if (tmp instanceof PolyQuad)
        {
          PolyCurve p = (PolyCurve) tmp;
          if (firstElemnt)
          {
            firstElemnt = false;
            contentContainer.pathMove(p.x1 / 20f, p.y1 / 20f);
          }
          else
          {
            contentContainer.pathLine(p.x1 / 20f, p.y1 / 20f);
            contentContainer.pathBezier(p.x2 / 20f, p.y2 / 20f, p.x3 / 20f, p.y3 / 20f, p.x4 / 20f, p.y4 / 20f);
          }
        }
      }
      contentContainer.pathClipAndPaint();
    }

    // restore pdfPage state
    contentContainer.restore();
    pdfPage.drawCanvas(contentContainer, canvasStartX, canvasStartY, areaSize + canvasStartX, areaSize + canvasStartY);
    if (getMask() != null && getMask().incrementIndex())
    {
      addToPDF(pdfPage, dPanelHeight, pdfImageCache);
    }
  }*/
  
  public void convertShapeToImage(String path, String name, boolean bUseHints)
  {
  	double scaleImageFactor = .05;        // unscaled size .05;
  	
  	try
  	{
	  	File outputFile = new File(path + File.separatorChar + name);
	  	if (outputFile.exists())
	  		return;
  		
	  	System.out.println("create shape image=" + name);
	  	
  		// TODO if the image already exists don't recreate it
  		// TODO after PDF is generated delete all shape images
	    // create the BufferedImage
  		
  		double outlineW = 0;
  		int outlineHW = 0;
  		
  		if (outlineWidth > 0)
  		{
  			outlineW = (outlineWidth / 20f / 14f);	
  			outlineHW = (int)(outlineWidth / 2);	
  		}
	    int height = (int)((0.5 + getHeight().floatValue()) / 20.0f);
	    int width = (int)((0.5 + getWidth().floatValue()) / 20.0f);
	    	 
	    /*BufferedImage image = new BufferedImage(width + outlineW, height + outlineW, BufferedImage.TYPE_INT_ARGB);

	    Graphics2D graphics = image.createGraphics(); 
	    if (bUseHints)
	    {
	    	graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	    	graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    	graphics.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
	    	graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
	    	graphics.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
	    }
	   
	    graphics.setPaint ( new Color ( 0, 0, 0, 0) );
	    graphics.fillRect ( 0, 0, image.getWidth(), image.getHeight() );
	    
      Point positionOrig = getPosition();
      Double rotatationOrig = getRotation();
      Point position = new Point(outlineHW, outlineHW);
      setPosition(position);
      setRotation(0.0);
	  	draw(graphics, scaleImageFactor);
      setPosition(positionOrig);
      setRotation(rotatationOrig);
	  	ImageIO.write(image, "png", outputFile);
      
      */
      
      
      int redColor = color.getRed();
      int greenColor = color.getGreen();
      int blueColor = color.getBlue();
      int alphaColor = color.getAlpha();
      
	  	//String parms = "s,Heart,1440,1440,12,12,120,255,2,240,24,24,255," + outputFile;
	  	String parms = "s,";
	  	String description = getDescription();
	  	if (description.indexOf("_") > 0)
	  		description = description.substring(0, description.lastIndexOf("_"));
	  	
	  	parms += description + "," + width + "," + height + "," + redColor + "," + greenColor + "," + blueColor + "," + alphaColor + ",";
	  	if (outlineW > 0)
	  	{
	      int outlineRedColor = outlineColor.getRed();
	      int outlineGreenColor  = outlineColor.getGreen();
	      int outlineBlueColor  = outlineColor.getBlue();
	      int outlineAlphaColor  = outlineColor.getAlpha();
		  	parms += outlineW + "," + outlineRedColor + "," + outlineGreenColor + "," + outlineBlueColor + "," + outlineAlphaColor + ",";
	  	}
	  	else
	  	{
		  	parms += "0,0,0,0,0,";
	  	}
  		parms += outputFile;
	  
      ProcessBuilder pb=new ProcessBuilder("java", "-jar", "RenderShapes.jar", parms);
      //pb.redirectOutput(Redirect.appendTo(new File("c:/JavaImaging/JavaFX/output.txt")));
      //pb.redirectError(Redirect.appendTo(new File("c:/JavaImaging/JavaFX/output.txt")));
      String userDir = System.getProperty("user.dir");
      pb.directory(new File(userDir));
      Process p = pb.start();
      p.waitFor();
	  	
  	}
  	catch (Exception e){}
  }
  
  /*
   * (non-Javadoc)
   * 
   * @see com.avery.project.AveryPanelField#addToPDF(org.faceless.pdf2.PDFPage,
   *      java.awt.Point, java.lang.Double, java.lang.Double,
   *      java.util.Hashtable)
   */
  public void addToPDF(PDFPage pdfPage, Double dPanelHeight, Hashtable pdfImageCache) throws Exception
  {
		Point topLeft = new Point();
		Point bottomRight = new Point();
		
		int canvasStartX = 0;
		int canvasStartY = 0;
		if (getPosition().x < 0)
		{
		  canvasStartX = (int) (getPosition().x / 20f);
		}
		if (getPosition().y < 0)
		{
		  canvasStartY = (int) (getPosition().y / 20f);
		}
		
		int areaSize = getAreaSize(pdfPage, canvasStartX, canvasStartY);

    float fillOpacity = this.getOpacity().floatValue();
    float shapeOpacity = fillOpacity;
    if (fillOpacity == 0)
    	shapeOpacity = 1.0f;
    	    
    float height = getHeight().floatValue() / 20.0f;
    float width = getWidth().floatValue() / 20.0f;
    // the 'top' is actually the distance from the bottom of the panel to the bottom of the drawing object
    float top = (dPanelHeight.intValue() - getPosition().y) / 20.0f - height - canvasStartY;
    
    // the 'top' must be >= 0 to fit drawing completely on the PDFCanvas
    if (top < 0)
    {
    	canvasStartY = canvasStartY + (int)top;
    	top = 0;
    }
    float left = getPosition().x / 20.0f - canvasStartX;
    topLeft.x = (int)left;
    topLeft.y = (int)top;
    bottomRight.x = topLeft.x + (int)width;
    bottomRight.y = topLeft.y + (int)height;
     
   	PDFCanvas contentContainer = new PDFCanvas(areaSize, areaSize, shapeOpacity);
   
    // don't fill if fillOpacity is 0
    PDFStyle style = new PDFStyle();
    //float fillOpacity = this.getOpacity().floatValue();
    if (fillOpacity > 0.f)
    {
        style.setFillColor(color);
    }
    else
    {
        style.setFillColor(null);
    }
    
    style.setLineWeighting((float) outlineWidth / 20.0f); 
    
		if (spotColor != null && outlineWidth > 0)
		{
	    // this is for custom shape proof of concept ONLY
	    Color fallback = CMYKColorSpace.getColor(
	    		AveryMasterpanel.SPOT_COLOR_PANEL[0],
	    		AveryMasterpanel.SPOT_COLOR_PANEL[1],
	    		AveryMasterpanel.SPOT_COLOR_PANEL[2],
	    		AveryMasterpanel.SPOT_COLOR_PANEL[3]);
	    SpotColorSpace spotInk = new SpotColorSpace(AveryMasterpanel.SPOT_COLOR_NAME, fallback);
	    Color spotColor = spotInk.getColor(1);
	    style.setLineColor(spotColor);
		}
		else
		{
			style.setLineColor(outlineWidth > 0 ? outlineColor : color);
		}
    //style.setLineCap(PDFStyle.LINEJOIN_ROUND);
    contentContainer.setStyle(style);

    // field rotation
    double fieldRotation = getRotation().doubleValue();
    // to rotate or not to rotate that is the question
    if (fieldRotation != 0.0)
    {
			// save old graphics transform/color state
			contentContainer.save();
			// rotate container
			contentContainer.rotate(topLeft.x, bottomRight.y, -fieldRotation);
			// Draw field. The lower left corner is at 0,0 because of the transform
			drawPdfField(contentContainer, topLeft, bottomRight);
			// restore graphics state
			contentContainer.restore();
    }
    else
    {
      // all calculations are complete - add the image to the page in the
      // PDF
      drawPdfField(contentContainer, topLeft, bottomRight);
    }
    pdfPage.drawCanvas(contentContainer, canvasStartX, canvasStartY, areaSize + canvasStartX, areaSize + canvasStartY);
  }

  // from SoftServe 11/4/11
  private void drawPdfField(PDFCanvas contentContainer, Point topLeft, Point bottomRight)
  {
    if (isRectangle())
    {
    	contentContainer.drawRectangle(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }
    else if (isEllipse())
    {
    	contentContainer.drawEllipse(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }
    else if (isPolygon())
    {
      /*GeneralPath path = shape.scaledGraphicsPath(.05);
      contentContainer.pathShape(path);
      
      Iterator iter = ((PolygonShape) shape).getElementsIterator();
    	
      Object tmp = iter.next();
      if (tmp instanceof PolyPoint)
      {
      	PolyPoint p = (PolyPoint) tmp;
        contentContainer.pathMove(0,0);  	  
      }
    	else if (tmp instanceof PolyQuad)
    	{
				PolyQuad p = (PolyQuad) tmp;
		    contentContainer.pathMove(0,0);
    	}
    	else if (tmp instanceof PolyCurve)
    	{
        PolyCurve p = (PolyCurve) tmp;
        contentContainer.pathMove(0,0);
    	}
      
      contentContainer.pathClipAndPaint();
      contentContainer.save();
      contentContainer.rotate((topLeft.x + bottomRight.x) / 2, topLeft.y, 180.);
			contentContainer.restore();*/
             
      Iterator iter = ((PolygonShape) shape).getElementsIterator();
      boolean firstElemnt = true;
      while (iter.hasNext())
      {
      	Object tmp = iter.next();
      	if (tmp instanceof PolyPoint)
      	{
          PolyPoint p = (PolyPoint) tmp;
          if (firstElemnt || p.move)
          {
            firstElemnt = false;
            contentContainer.pathMove(topLeft.x + p.x / 20f, bottomRight.y - p.y / 20f);
          }
          else
          {
          	contentContainer.pathLine(topLeft.x + p.x / 20f, bottomRight.y - p.y / 20f);
          }
      	}
      	else if (tmp instanceof PolyQuad)
      	{
					PolyQuad p = (PolyQuad) tmp;
					if (firstElemnt)
					{
				    firstElemnt = false;
				    contentContainer.pathMove(topLeft.x + p.x1 / 20f, bottomRight.y - p.y1 / 20f);
					}
					else
					{
            contentContainer.pathBezier(
                    topLeft.x + p.x1 / 20f,
                    bottomRight.y - p.y1 / 20f,
                    topLeft.x + p.x2 / 20f,
                    bottomRight.y - p.y2 / 20f,
                    topLeft.x + p.x3 / 20f,
                    bottomRight.y - p.y3 / 20f);
					}
      	}
      	else if (tmp instanceof PolyCurve)
      	{
	        PolyCurve p = (PolyCurve) tmp;
	        if (firstElemnt)
	        {
            firstElemnt = false;
            contentContainer.pathMove(
                    topLeft.x + p.x1 / 20f,
                    bottomRight.y - p.y1 / 20f);
	        }
	        else
	        {
            contentContainer.pathLine(
                    topLeft.x + p.x1 / 20f,
                    bottomRight.y - p.y1 / 20f);
            contentContainer.pathBezier(
                    topLeft.x + p.x2 / 20f,
                    bottomRight.y - p.y2 / 20f,
                    topLeft.x + p.x3 / 20f,
                    bottomRight.y - p.y3 / 20f,
                    topLeft.x + p.x4 / 20f,
                    bottomRight.y - p.y4 / 20f);
           }
      	}
      }
      contentContainer.pathClose();
      contentContainer.pathClipAndPaint();
      //contentContainer.pathPaint();
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.avery.project.AveryPanelField#addDefaultDescriptionObject()
   */
  protected void addDefaultDescriptionObject()
  {
    setDescriptionObject(Description.makeImageFieldDescription());
  }

  /**
   * @return a JDOM Element representing this object, consistent with the
   *         averysoft.xsd schema
   * @see com.avery.project.Averysoftable#getAverysoftElement()
   */
  public Element getAverysoftElement()
  {
    Element element = new Element("drawing");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    applyParentAverysoftAttributes(element);

    element.setAttribute("shape", shape.getShapeString());
    element.setAttribute("color", TextStyle.formatColor0x(color));
    if (maintainAspect)
    {
      element.setAttribute("maintainAspect", "true");
    }
    
    if (spotColor != null)
    element.setAttribute("spotColor", spotColor);
    
    element.addContent(getDescriptionObject().getAverysoftElement());
    
    if (lineThickness > 0)
    {
    	Hint hint = new Hint("lineThickness", String.valueOf(lineThickness));
      element.addContent(hint.getAverysoftElement());
    }

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
    position.setAttribute("x", Double.toString(getPosition().getX()));
    position.setAttribute("y", Double.toString(getPosition().getY()));
    element.addContent(position);

    if (hasValidOutline())
    {
      Element outline = new Element("outline");
      outline.setNamespace(AveryProject.getAverysoftNamespace());
      outline.setAttribute("width", Double.toString(outlineWidth));
      outline.setAttribute("color", TextStyle.formatColor0x(outlineColor));
      element.addContent(outline);
    }

    Element polypointsElement = shape.getPolypointsElement();
    if (polypointsElement != null)
    {
      element.addContent(polypointsElement);
    	//Element polyElement = getPolygonXML();
    	//polyElement.detach();
    	//element.addContent(polyElement);
    }
    
    
    return element;
  }

  /**
   * @return a JDOM Element representing this object, consistent with the
   *         averysoft.xsd schema
   * @see com.avery.project.Averysoftable#getAverysoftElement()
   */
  public Element getAverysoftIBElement()
  {
    Element element = new Element("drawing");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    applyParentAverysoftIBAttributes(element);

    element.setAttribute("shape", shape.getShapeString());
    element.setAttribute("color", TextStyle.formatColor0x(color));
    if (maintainAspect)
    {
      element.setAttribute("maintainAspect", "true");
    }
    
    if (spotColor != null)
    	element.setAttribute("spotColor", spotColor);
    
    /*element.addContent(getDescriptionObject().getAverysoftElement());
    
    if (lineThickness > 0)
    {
    	Hint hint = new Hint("lineThickness", String.valueOf(lineThickness));
      element.addContent(hint.getAverysoftElement());
    }*/

    Element position = new Element("position");
    position.setNamespace(AveryProject.getAverysoftNamespace());
    position.setAttribute("x", AveryProject.twipsToMM(getPosition().getX()));
    position.setAttribute("y", AveryProject.twipsToMM(getPosition().getY()));
    element.addContent(position);

    if (hasValidOutline())
    {
      Element outline = new Element("outline");
      outline.setNamespace(AveryProject.getAverysoftNamespace());
  		outline.setAttribute("width", AveryProject.twipsToMM(outlineWidth));
  		
  		outline.setAttribute("color", TextStyle.formatColor0x(outlineColor));
      element.addContent(outline);
    }

    if (shape.getShapeString().equals("polygon"))
    {
    	try
    	{
	    	Element polypointsElement = getShape().getPolypointsElement();
	    	XMLOutputter xmlOutput = new XMLOutputter();
	    	String strElement = xmlOutput.outputString(polypointsElement);
	    	if (strElement.indexOf("NaN") > 0)
	    	{
		    	strElement = strElement.replaceAll("NaN", "0.0");
		    	SAXBuilder builder = new SAXBuilder(false);
		    	Document doc = builder.build(new StringReader(strElement));
		    	Element ppElement = doc.getRootElement();
		    	if (polypointsElement != null)
		    	{
		    		element.addContent(ppElement.detach());
		    	}
	    	}
	    	else
	    	{
	    		element.addContent(polypointsElement);
	    	}
    	}    	
	   	catch (Exception e){}
    }
    
    
    return element;
  }

  /**
   * @return <code>null</code> because this object was not supported in
   *         Avery.dtd
   */
  public Element getAveryElement()
  {
    return null;
  }

  public static Element getPolygonXML(String shapesDir, String polyShape)
  {
  	Element polyElement = null;
  	try
  	{
	    SAXBuilder builder = new SAXBuilder(false);
			builder.setFeature("http://apache.org/xml/features/validation/schema", false);
			builder.setProperty(
					"http://apache.org/xml/properties/schema/external-schemaLocation",
					Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 
			
			File xmlfile = new File(shapesDir, polyShape + ".xml");
			polyElement = builder.build(xmlfile).getRootElement();
			polyElement.detach();
  	}
  	catch (Exception e)
  	{
  	}
  	return polyElement;
  }
  /**
   * Sets the width of the field in twips
   * 
   * @param newWidth -
   *          the desired width
   */
  public void setWidth(Double newWidth)
  {
  	if (AveryProject.outputMM)
  	{
  		if (bFirstTimeOnly)
  		{
  			double scalex = newWidth / 56.6929138 / shape.getWidth();
    		shape.scale(scalex, maintainAspect ? scalex : 1.0);
  		}
    }
  	else
  	{
  		double scalex = newWidth.doubleValue() / shape.getWidth();
  		shape.scale(scalex, maintainAspect ? scalex : 1.0);
  	}
  }

  /**
   * Retrieves the width of the field in twips
   * 
   * @return the field's width
   */
  public Double getWidth()
  {
  	if (AveryProject.outputMM)
  		return new Double(shape.getWidth() * 56.6929138);
    return new Double(shape.getWidth());
  }

  /**
   * Sets the height of the field in twips
   * 
   * @param newHeight -
   *          the desired height
   */
  public void setHeight(Double newHeight)
  {
  	if (AveryProject.outputMM)
  	{
  		if (bFirstTimeOnly)
  		{
  			double scaley = newHeight / 56.6929138 / shape.getHeight();
    		shape.scale(maintainAspect ? scaley : 1.0, scaley);
    		bFirstTimeOnly = false;
  		}
    }
  	else
  	{
  		double scaley = newHeight.doubleValue() / shape.getHeight();
  		shape.scale(maintainAspect ? scaley : 1.0, scaley);
  	}
  	
  }

  /**
   * Retrieves the height of the field in twips
   * 
   * @return the field's height
   */
  public Double getHeight()
  {
  	if (AveryProject.outputMM)
  		return new Double(shape.getHeight() * 56.6929138);
		return new Double(shape.getHeight());
 	
  }
  
  public double getLineThickness()
  {
  	return lineThickness;
  }
  
  public void setLineThickness(double newLineThickness)
  {
  	lineThickness = newLineThickness;
  }

  public double getOutlineWidth()
  {
  	return outlineWidth;
  }
  
  public void setOutlineWidth(double newOutlineWidth)
  {
  	outlineWidth = newOutlineWidth;
  }
  
  public boolean isRectangle()
  {
    return "rect".equals(shape.getShapeString());
  }

  public boolean isEllipse()
  {
    return "ellipse".equals(shape.getShapeString());
  }

  public boolean isPolygon()
  {
    return "polygon".equals(shape.getShapeString());
  }
  
  public Color getColor()
  {
  	return color;
  }
  
  public void setColor(Color newColor)
  {
  	color = newColor;
  }
  
  public Color getOutlineColor()
  {
  	return outlineColor;
  }
  
  public void setOutlineColor(Color newColor)
  {
  	outlineColor = newColor;
  }
  
  public boolean isMaintainAspect()
  {
  	return maintainAspect;
  }
  
  public void setMaintainAspect(boolean newMaintainAspect)
  {
  	maintainAspect = newMaintainAspect;
  }
  
  public float getOutlineOpacity()
  {
  	return outlineOpacity;
  }
  
  public void setOutlineOpacity(float newOutlineOpacity)
  {
  	outlineOpacity = newOutlineOpacity;
  }

  public String getSpotColor()
  {
  	return spotColor;
  }

  public void setSpotColor(String newSpotColor)
  {
  	spotColor = newSpotColor;
  }

  public AveryShape getShape()
  {
  	return shape;
  }
    
  /**
   * Creates a deep clone
   * 
   * @return deep clone
   */
  public AveryPanelField deepClone()
  {
    try
    {
      AveryShape newShape = AveryShape.manufactureShape(shape.getShapeString(), shape.getWidth(), shape.getHeight(), shape
          .getCornerRadius(), shape.getPolypointsElement());

      Drawing drawing = new Drawing(newShape, getPosition().getX(), getPosition().getY());

      copyPrivateData(drawing);

      drawing.color = color;
      drawing.maintainAspect = maintainAspect;
      drawing.outlineWidth = outlineWidth;
      drawing.outlineColor = outlineColor;
      drawing.outlineOpacity = outlineOpacity;
      drawing.lineThickness = lineThickness;
      drawing.spotColor = spotColor;

      return drawing;
    }
    catch (AveryException ex)
    {
      ex.printStackTrace();
      return null;
    }
  }

  void replaceStyle(StyleSet original, StyleSet replacement)
  {
    if (original.color1.equals(color))
    {
      color = replacement.color1;
    }
    else if (original.color2.equals(color))
    {
      color = replacement.color2;
    }
    else if (original.color3.equals(color))
    {
      color = replacement.color3;
    }

    if (original.color1.equals(outlineColor))
    {
      outlineColor = replacement.color1;
    }
    else if (original.color2.equals(outlineColor))
    {
      outlineColor = replacement.color2;
    }
    else if (original.color3.equals(outlineColor))
    {
      outlineColor = replacement.color3;
    }
  }

  boolean matchesStyle(StyleSet ss)
  {
    if (ss.color1.equals(color) || ss.color2.equals(color) || ss.color3.equals(color))
    {
      if (outlineColor == null)
      {
        return true; // we have a match
      }

      if (ss.color1.equals(outlineColor) || ss.color2.equals(outlineColor) || ss.color3.equals(outlineColor))
      {
        return true; // double match!
      }
    }

    return false;
  }
}
