/**
 * Title:       AveryMasterpanel<p>
 * Description: A masterpanel is used to store AveryPanelField elements that
 *              can be replicated on multiple AveryPanels<p>
 * Copyright:   (c)Copyright 2013-2015 Avery Products Corp. All Rights Reserved.
 * @author      Bob Lee, Brad Nelson
 * @version     2.0
 */

package com.avery.project;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.faceless.pdf2.CMYKColorSpace;
import org.faceless.pdf2.SpotColorSpace;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFStyle;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.output.XMLOutputter;

import com.avery.project.shapes.AveryShape;
import com.avery.project.shapes.PolygonShape;
import com.avery.project.shapes.PolygonShape.PolyPoint;
import com.avery.utils.AveryException;
import com.avery.utils.AveryUtils;
import com.sun.image.codec.jpeg.JPEGCodec;

public class AveryMasterpanel extends AverySuperpanel implements Averysoftable
{
	private static double SAFE_DISTANCE_PERCENT = .03;	// percent of smallest dimension to use for safe line inset
	private static int MIN_SAFE_DISTANCE = 57;       		// 1mm
	private static int MAX_SAFE_DISTANCE = 170;			 		// 3mm
	
  // all measurements are real world, in twips (1/1440 inch)
  private String ID;
  private String paperimage;
  private Color paperColor = null;
  private AveryShape shape;
  private int ordinal;
  private boolean bleedable;
  private String noBleedSides = null;
  private String printOutline = "false";
  private String outlineColor = "null";
  private String mergeMapName = new String("");
  private List guides = new ArrayList();
  private List masks;
  private Hashtable hints;
  private List textDefaults = new ArrayList();
  private boolean noMerge = false;
  private boolean editable;
  private boolean previewable;
  
  private boolean noDrawSafeBleed = false;
  private boolean safeOn = true;
  
  // obtained outside of averysoft
  private String customProof = null;
  
  // Spot color for panel
  public static float[] SPOT_COLOR_PANEL = {0.572556f, 0.941177f, 0.956863f, 0.0f};
  public static String SPOT_COLOR_NAME = "CutContour_kiss";
  //public static float[] SPOT_COLOR_PANEL = {0.0f, 0.0f, 0.0f, 0.0f};
  //public static String SPOT_COLOR_NAME = "HPI-White";
  
  public static float panelStrokeWidth = 1;
  
  private boolean displayPromptOrder = false;
  
  protected AveryMasterpanel()
  { }

  public AveryMasterpanel(Element element)
  throws Exception
  {
    if (element.getNamespace().equals(AveryProject.getAverysoftNamespace())
      && element.getName().equals("masterpanel"))
    {
      ID = element.getAttributeValue("id");
  		shape = AveryShape.manufactureShape(element);
			bleedable = "true".equals(element.getAttributeValue("bleed"));
			noBleedSides = element.getAttributeValue("noBleedSides");			
			noMerge = "true".equals(element.getAttributeValue("noMerge"));
			editable = !"false".equals(element.getAttributeValue("editable"));
			previewable = !"false".equals(element.getAttributeValue("previewable"));
			String str = element.getAttributeValue("printOutline");
			
			if (str != null)
			{
				printOutline = str;
			}
			String outlineColor = element.getAttributeValue("outlineColor");
			if (outlineColor != null)
			{
				outlineColor = str;
			}
			
			setMergeMapName(element.getAttributeValue("mergeMap"));

			super.setDescription(element.getChild("description", AveryProject.getAverysoftNamespace()));

			Iterator iterator = element.getChildren("hint", AveryProject.getAverysoftNamespace()).iterator();
			while (iterator.hasNext())
			{
				Element child = (Element)iterator.next();
				addHint(child.getAttributeValue("name"), child.getAttributeValue("value"));
			}

			iterator = element.getChildren("textDefaults", AveryProject.getAverysoftNamespace()).iterator();
			while (iterator.hasNext())
			{
				addTextDefaults(new TextDefaults((Element)iterator.next()));
			}

			iterator = element.getChildren("cutout", AveryProject.getAverysoftNamespace()).iterator();
			while (iterator.hasNext())
			{
				addCutout(new AveryCutout((Element)iterator.next()));
			}

			List guideElements = element.getChildren("guide", AveryProject.getAverysoftNamespace());
			if (guideElements.size() > 0)
			{
				guides = new ArrayList(guideElements.size());
				iterator = guideElements.iterator();
				while (iterator.hasNext())
				{
					guides.add(new Guide((Element)iterator.next()));
				}
			}
			
	    List maskElements = element.getChildren("mask", AveryProject.getAverysoftNamespace());
	    if (maskElements.size() > 0)
	    {
	      masks = new ArrayList(maskElements.size());
	      iterator = maskElements.iterator();
	      while (iterator.hasNext())
	      {
	        masks.add(new Mask((Element)iterator.next()));
	      }
	    }

      // read fields
      iterator = element.getChildren().iterator();
      while (iterator.hasNext())
      {
        Element child = (Element)iterator.next();

        AveryPanelField field = null;
        if (child.getName().equals("textBlock"))
        {
          field = new AveryTextblock(child);
        }
        else if (child.getName().equals("textLine"))
        {
          field = new AveryTextline(child);
        }
        else if (child.getName().equals("image"))
        {
          field = new AveryImage(child);
        }
        else if (child.getName().equals("background"))
        {
          field = new AveryBackground(child);
        }
        else if (child.getName().equals("textPath"))
        {
          field = new TextPath(child);
        }
        else if (child.getName().equals("barcode"))
        {
          field = new Barcode(child);
        }
        else if (child.getName().equals("drawing"))
        {
          field = new Drawing(child);
        }
        
        if (field != null)
        {
          addField(field);
        }
      }
    }
    else if (!initFromAveryDtdElement(element))
    {
      Exception ex = new AveryException("Not an Avery masterpanel element");
      ex.printStackTrace();
      throw ex;
    }
  }

  private boolean initFromAveryDtdElement(Element element)
  throws Exception
  {
    if (element.getName().equals("Avery.masterpanel"))
    {
      ID = convertNumericID(element.getAttributeValue("id"));

      setDescriptionObject(
      		Description.makePanelDescription(element.getAttributeValue("description")));
      shape = AveryShape.manufactureShape(element);

      Element paperElement = element.getChild("Avery.paperimage");
      if (paperElement != null)
      {
        setPaperimage(paperElement.getAttributeValue("src"));
      }

      // read fields
      Iterator i = element.getChildren("Avery.mp.field").iterator();
      while (i.hasNext())
      {
        AveryPanelField field = AveryPanelField.getPanelField((Element)i.next());
        if (field != null)
        {
          addField(field);
        }
      }

      // read cutouts
      Iterator j = element.getChildren("Avery.cutout").iterator();
      while (j.hasNext())
      {
        this.addCutout(new AveryCutout((Element)j.next()));
      }
      return true;
    }
    else return false;
  }

  static String convertNumericID(String input)
  {
  	try
  	{
  		Integer number = Integer.decode(input);
  		return "MP" + number;
  	}
  	catch (NumberFormatException ex)
  	{
  		return input;
  	}
  }

  public String dump()
  {
    String str = "<P>AveryMasterpanel";
    str += "<br>width = " + getWidth().toString();
    str += "<br>height = " + getHeight().toString();
    str += "<br>ID = " + ID.toString();
    str += "<br>shape = " + getShape().getShapeString();
    str += "<br>cornerradius = " + Double.toString(getShape().getCornerRadius());
    str += super.dump();
    str += "<br>end AveryMasterpanel ID = " + ID.toString() + "</P>\n";

    return str;
  }

	public boolean isBleedable()
	{
		return bleedable;
	}

	public void setBleed(boolean bleed)
	{
		bleedable = bleed;
	}

	public String getNoBleedSides()
	{
		return noBleedSides;
	}

	public void setNoBleedSides(String noBleed)
	{
		noBleedSides = noBleed;
	}

	public void setSafeOn(boolean safeOn)
	{
		this.safeOn = safeOn;
	}
	
  void setOrdinal(int n)
  {
    ordinal = n;
  }

  public int getOrdinal()
  {
    return ordinal;
  }

  public Double getWidth()
  {
    return new Double(getShape().getWidth());
  }

  public Double getHeight()
  {
    return new Double(getShape().getHeight());
  }

  public String getID()
  {
    return ID;
  }

  public AveryShape getShape()
  {
  	return shape;
  }

  public void setShape(AveryShape shape)
  {
    this.shape = shape;
  }

  public void setID(String id)
  {
  	this.ID = id;
  }

  public void setPrintOutline(String str)
  {
  	printOutline = str;
  }
  
  public boolean isPrintOutline()
  {
  	return printOutline.equals("true");
  }
  
  public String getOutlineColor()
  {
  	return outlineColor;
  }
  
  public void setOutlineColor(String str)
  {
  	outlineColor = str;
  }
  
  public boolean isNoMerge()
  {
  	return noMerge;
  }
  
  public void setNoMerge(boolean nyetMerge)
  {
  	noMerge = nyetMerge;
  }
 
  public boolean isEditable()
  {
    return editable;
  }

  public void setEditable(boolean editable)
  {
    this.editable = editable;
  }

  /**
   * Provides access to the list of text defaults
   * @return a List of {@link TextDefaults} objects
   */
  public List getTextDefaults()
  {
    return textDefaults;
  }
  
  public boolean isPreviewable()
  {
    return previewable;
  }

  public void setPreviewable(boolean previewable)
  {
    this.previewable = previewable;
  }
  
  public void setNoDrawSafeBleed(boolean bDraw)
  {
  	noDrawSafeBleed = bDraw;
  }

  /**
   * Adds a text defaults to the masterpanel's textDefaults list
   * @param textDefault - the {@link TextDefaults} to be added
   */
  private void addTextDefaults(TextDefaults textDefault)
  {
    textDefaults.add(textDefault);
  }
  
  public void setCustomProof(String newCustomProof)
  {
  	customProof = newCustomProof;
  }
  
  public AveryMasterpanel cloneMaster()
  {
  	AveryMasterpanel master = new AveryMasterpanel();
  	master.bleedable = bleedable;
  	master.noBleedSides = noBleedSides;
  	master.displayPromptOrder = displayPromptOrder;
  	master.guides = guides;
  	master.hints = hints;
  	master.ID = ID;
  	master.masks = masks;
  	master.mergeMapName = mergeMapName;
  	master.ordinal = ordinal;
  	master.paperimage = paperimage;
  	master.printOutline = printOutline;
  	master.outlineColor = outlineColor;
  	master.noMerge = noMerge;
  	if (shape instanceof PolygonShape)
  	{
  		PolygonShape ps = (PolygonShape)shape;
  		master.shape = ps.clonePolygonShape();
  	}
  	else
  	{
    	master.shape = shape;  		
  	}
  	master.textDefaults = textDefaults;
  	master.usingDefaultDescription = usingDefaultDescription;
  	return master;
  }

  /**
   * Uses Java Graphics2D to draw the panel
   * @param graphics the drawing context
   * @param targetWidth width in pixels (used for scaling)
   * @param targetHeight height in pixels (used for scaling)
   * @param master not used in this implementation
   * @throws Exception when something goes horribly wrong
   */
  public void draw(Graphics2D graphics, int targetWidth, int targetHeight, AveryMasterpanel master)
    throws Exception
  {
  	
  	//final Graphics2D graphics = (Graphics2D)g2.create();
  	//try
  	//{
	    // set the correct clip
	    ///maskShape(graphics, targetWidth, targetHeight);
	
	    // calculate the scaling factor
	    double scalar = (double)targetWidth / getWidth().doubleValue();
	
	    if (getPaperimage() != null)
	    {
	      try
	      {
	        BufferedImage background =
	          JPEGCodec.createJPEGDecoder(new FileInputStream(paperimage)).decodeAsBufferedImage();
	
	        graphics.drawImage(background, 0, 0, targetWidth, targetHeight, null);
	      }
	      catch (Exception e)   // report error and move on
	      {
	        System.err.println(AveryUtils.timeStamp() + e.toString());
	        System.err.println("  AveryMasterpanel.draw couldn't render paperimage " + getPaperimage());
	      }
	    }
     
	    if (isBleedable())
	    {
	    	panelStrokeWidth = 1;
	    	int bleedSizeTwips = 20 * (int)AveryPanel.BLEED_DIMENSION;
	      drawShape(graphics, targetWidth, targetHeight, bleedSizeTwips);
	    }
	    
	    // walk the fields in proper zOrder, Draw()ing each of them
	    Iterator i = new DrawIterator(getPanelFields());
	    while (i.hasNext())
	    {
	      ((AveryPanelField)i.next()).draw(graphics, scalar);
	
	      // NOTE: The following debug code is useful for sending a report into the out log
	      // flagging projects that have backgrounds that occupy less than half of the
	      // area of a panel. In Scout the Project class contains a debug statement
	      // that displays the names of projects that are generating a preview.
	      // That statement in Scout contains text "...Scount is generating..."
	      // Enable that debug statement at the same time as enabling the block below,
	      // and be sure to erase all preview jpgs in the preview folder
	      // also comment out the above line in this method:
	      // ((AveryPanelField)i.next()).draw(graphics, scalar);
	
	      /* AveryPanelField apf = (AveryPanelField)i.next();
	      apf.draw(graphics, scalar);
	      if (apf instanceof AveryBackground)
	      {
	        double panelArea = getWidth().doubleValue() * getHeight().doubleValue();
	        double fieldArea = apf.getWidth().doubleValue() * apf.getHeight().doubleValue();
	        if (fieldArea < panelArea / 2.0)
	          System.out.println(
	            "WARNING: Preceding project's background area less than half it's containing panel area!");
	      }
	      */
	      // end of debug statements
	    }

	    // draw any cutouts that might exist
	    super.drawCutouts(graphics, scalar);
	
	    // draw the panel shape
	    drawShape(graphics, targetWidth, targetHeight, 0);
	    
	    if (displayPromptOrder)
	    {
	    	drawPromptOrder(graphics, scalar);
	    }
  	//}
  	//finally
  	//{
      //graphics.dispose();
  	//}
  }
  
  public void drawPromptOrder(Graphics g, double scalar)
  {
  	Iterator fieldIterator = this.getFieldIterator();
  	while(fieldIterator.hasNext())
  	{
      AveryPanelField field = (AveryPanelField)fieldIterator.next();
      int x = (int)(field.getPosition().x * scalar);
      int y = (int)(field.getPosition().y * scalar);
      g.setColor(Color.WHITE);
      g.fillRect(x, y, 24, 14);
      g.setColor(Color.RED);
      Font promptFont = new Font("Arial", 0, 12);
      g.setFont(promptFont);
      g.drawString(field.getPromptOrder(), x + 2, y + 12);
  	}	
  }
  
  public void drawTextDefaults(Graphics g, double scalar, Color color)
  {
    g.setClip(null);
    g.setColor(color);
    
    Iterator iterator = textDefaults.iterator();
    while (iterator.hasNext())
    {
      TextDefaults td = (TextDefaults)iterator.next();
      if (td.getBlockRotation().intValue() == 0)
      {
        double x = td.getBlockPosition().x * scalar;
        double y = td.getBlockPosition().y * scalar;
        double w = td.getBlockWidth().doubleValue() * scalar;
        double h = td.getBlockHeight().doubleValue() * scalar;
        g.drawRect((int)x, (int)y, (int)w, (int)h);
      }
      // TODO: handle rotated TextDefaults
    }
  }
  
  public void drawGuides(Graphics2D g, double scalar, Color color)
  {
    if (guides != null)
    {
      Iterator iterator = guides.iterator();
      boolean isUsageUsableArea = false;
      
      while (iterator.hasNext())
      {
      	Guide guide = (Guide)iterator.next();
      	if ("usableArea".equals(guide.getUsage()))
      	{
      		if (!isUsageUsableArea)
      			drawFilledShape(g, scalar, new Color(0xee, 0xee, 0xee, 0x7f));
      		Color fillColor = getPaperColor() == null ? Color.white : getPaperColor();
      		guide.draw(g, scalar, Color.white, fillColor);
      		isUsageUsableArea = true;
      	}
      	else
      	{
      		guide.draw(g, scalar, color, null);     		
      	}
      }
    }
  }

  /**
   * This override of the abstract AverySuperPanel method returns all of the
   * fields, sorted by their prompt order.  The list does not include any fields
   * that have a zero prompt order.
   * @throws Exception to satisfy prototype
   * @return a List of AveryPanelField objects
   */
  public java.util.List getPromptSortedPanelFields()
    throws Exception
  {
    ArrayList list = new ArrayList(getPanelFields().size());
    Double zero = Double.valueOf("0.0");
    Iterator i = getPanelFields().iterator();
    while (i.hasNext())
    {
      AveryPanelField field = (AveryPanelField)(i.next());
      if (Double.valueOf(field.getPromptOrder()).equals(zero))
      {
        continue;   // ignore fields with promptOrder of zero
      }
      list.add(field);
    }

    Object[] objects = list.toArray();
    Arrays.sort(objects, new PromptComparator());
    return Arrays.asList(objects);
  }

  public java.util.Iterator getZLevelIterator()
  {
    return new DrawIterator(getPanelFields());
  }

 /**
   * updates the field with the content id
   * @param contentID the panel field content id to return
   * @param field the panel field
   * @param fieldTable the ProjectFieldTable
   */
  protected void updateContentField(String contentID, AveryPanelField field,
    ProjectFieldTable fieldTable)
  {
    fieldTable.updateContentField(contentID, field);
  }

 /**
   * updates the field with the style id
   * @param styleID the panel field style id to return
   * @param field the panel field
   * @param fieldTable the ProjectFieldTable
   */
  protected void updateStyleField(String styleID, AveryPanelField field,
    ProjectFieldTable fieldTable)
  {
    fieldTable.updateStyleField(styleID, field);
  }


  /**
   * @param graphics device context for drawing
   * @param width of the Graphics2D target space
   * @param height of the Graphics2D target space
   */
  public void drawShape(Graphics2D graphics, int width, int height, int bleedSizeTwips)
  {
		double scaleW = (((double)width)) / getWidth().doubleValue();
		double scaleH = (((double)height)) / getHeight().doubleValue();
		double scale = scaleW < scaleH ? scaleW : scaleH;
			
		if (bleedSizeTwips > 0 && !noDrawSafeBleed)
		{
			drawBleedShape(graphics, scale, bleedSizeTwips);
		}
		
		drawShape(graphics, scale);
		
		if ((customProof != null && customProof.length() > 0 && !noDrawSafeBleed) || safeOn)
			drawSafeShape(graphics, scale);	
    graphics.setStroke(new BasicStroke(1));
  }

  /**
   * Draws the shape of the panel as a black 1-pixel line.  Note that this
   * also removes the <code>clip</code> from the <code>graphics</code>
   * object because it interferes with the outline drawing.
   * @param graphics context for drawing
   * @param scale the scaling factor of the drawing
   */
  void drawShape(Graphics2D graphics, double scale)
  {  	
    graphics.setClip(null);
    graphics.setColor(new Color(0, 0, 0));
    float strokeSize = panelStrokeWidth;
    if (scale < .035)
    	strokeSize = panelStrokeWidth + 1;
    graphics.setStroke(new BasicStroke(strokeSize));
    graphics.draw(getShape().scaledGraphicsPath(scale));
  }

  void drawBleedShape(Graphics2D graphics, double scale, int bleedSizeTwips)
  {
    graphics.setClip(null);
    graphics.setColor(new Color(0xee, 0, 0, 30));
    float strokeSize = 2 *(float)bleedSizeTwips * (float)scale;
    
    graphics.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
    graphics.draw(getShape().scaledGraphicsPath(scale));
    graphics.setColor(getPaperColor() == null ? Color.white : getPaperColor());
    graphics.fill(getShape().scaledGraphicsPath(scale));
    
   // strokeSize = 4;
  	//if (this.getShape().getShapeString().equals("polygon"))
  	//	strokeSize = 6;
    
    //float[] dashPattern = {10, 5};
   // graphics.setColor(new Color(0xff, 0, 0, 90));
   // graphics.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 0f, dashPattern, 0f));
   // graphics.draw(getShape().scaledGraphicsPath(scale));
   // graphics.setColor(getPaperColor() == null ? Color.white : getPaperColor());
    
    //graphics.fill(getShape().scaledGraphicsPath(scale));
  }

  void drawSafeShape(Graphics2D graphics, double scale)
  {
  	float baseStrokeSize = 2;
  	
  	float minSafeSize = MIN_SAFE_DISTANCE;
  	float maxSafeSize = MAX_SAFE_DISTANCE;
  	double safeSizeTwips = SAFE_DISTANCE_PERCENT * Math.min(getWidth(), getHeight());
  	safeSizeTwips = Math.min(safeSizeTwips, maxSafeSize);
  	safeSizeTwips = Math.max(safeSizeTwips, minSafeSize);
  	
  	// calculate reduced scale in x and y
  	double dxScale = 2 * scale * safeSizeTwips / getWidth();
  	double newXScale = scale - dxScale;
  	double dyScale = 2 * scale * safeSizeTwips / getHeight();
  	double newYScale = scale - dyScale;
    graphics.setClip(null);
    float strokeSize = baseStrokeSize;
    if (scale < .035)
    	strokeSize = baseStrokeSize + 1;
    //float strokeSize = 2 *(float)safeSizeTwips * (float)newScale;
    
    double dOffset = safeSizeTwips * scale;
    graphics.translate(dOffset, dOffset);
    
    float[] dashPattern = {5, 5}; // 10, 10 };
    graphics.setColor(new Color(0x7f, 0x7f, 0x7f, 60));
    graphics.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 0f, dashPattern, 0f));
    graphics.draw(getShape().scaledGraphicsPath(newXScale, newYScale));

    // 10, 10 };    graphics.setColor(getPaperColor() == null ? Color.white : getPaperColor());
    //graphics.fill(getShape().scaledGraphicsPath(newScale));
    graphics.translate(-dOffset, -dOffset);

  }
  
    
    //double dOffset = safeSizeTwips * scale;
/*    graphics.translate(dOffsetX, dOffsetY);
    
    float[] dashPattern = {5, 5}; // 10, 10 };
    graphics.setColor(new Color(0x7f, 0x7f, 0x7f, 60));
    graphics.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 0f, dashPattern, 0f));
    graphics.draw(getShape().scaledGraphicsPath(newScale, newScale));

    // 10, 10 };    graphics.setColor(getPaperColor() == null ? Color.white : getPaperColor());
    //graphics.fill(getShape().scaledGraphicsPath(newScale));
    graphics.translate(-dOffsetX, -dOffsetY);

  }*/

  void addBleedShapeToPDF(PDFPage pdfPage, double scale)
  {
    float width = getWidth().floatValue() / 20;
    float height = getHeight().floatValue() / 20;
    // already transformed
    float pdfX1 = 0f;			// left
    float pdfY1 = 0f;			// bottom
    
    float pdfX2 = width;	// right
    float pdfY2 = height;	// top
    
    PDFStyle style = new PDFStyle();
    style.setLineColor(new Color(0xff, 0, 0));
    style.setLineWeighting(1.0f);
    style.setLineDash(10, 5, 0);
    pdfPage.setStyle(style);

    if ("ellipse".equals(shape.getShapeString()))
    {
      pdfPage.drawEllipse(pdfX1, pdfY1, pdfX2, pdfY2);
    }
    else if ("rect".equals(shape.getShapeString()))
    {
      pdfPage.drawRoundedRectangle(pdfX1, pdfY1, pdfX2, pdfY2, (float)(shape.getCornerRadius() / 20));
    }
    else // polygon shape
    {
    	GeneralPath path = shape.scaledGraphicsPath(scale);
    	PathIterator pathIter= path.getPathIterator( new AffineTransform(1, 0, 0, -1, 0, height)); 
    	
    	float[] coords = new float[6];
    	while (!pathIter.isDone())
    	{
    		int pathSegment = pathIter.currentSegment(coords);
    		switch (pathSegment)
    		{
    			case PathIterator.SEG_MOVETO:
    				pdfPage.pathMove(coords[0], coords[1]);
    				break;
    				
    			case PathIterator.SEG_LINETO:
    				pdfPage.pathLine(coords[0], coords[1]);
    				break;
    				
    				// never used...all quad paths are converted to cubics
 //   			case PathIterator.SEG_QUADTO:
 //   				pdfPage.pathArc(coords[0], coords[1], coords[2], coords[3]);
 //   				break;
    				
    			case PathIterator.SEG_CUBICTO:
    				pdfPage.pathBezier(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
    				break;
    				
    			default:
    				break;
    		}
    		pathIter.next();   		
    	}
    	pdfPage.pathPaint();
    }

  }
  
  void addSafeShapeToPDF(PDFPage pdfPage, double scale)
  {
  	float minSafeSize = MIN_SAFE_DISTANCE;
  	float maxSafeSize = MAX_SAFE_DISTANCE;
  	
  	double safeSizeTwips = SAFE_DISTANCE_PERCENT * Math.min(getWidth(), getHeight());
  	safeSizeTwips = Math.min(safeSizeTwips, maxSafeSize);
  	safeSizeTwips = Math.max(safeSizeTwips, minSafeSize);
  	
  	// calculate reduced scale in x and y
  	double dxScale = 2 * scale * safeSizeTwips / getWidth();
  	double newXScale = scale - dxScale;
  	double dyScale = 2 * scale * safeSizeTwips / getHeight();
  	double newYScale = scale - dyScale;

    double dOffset = safeSizeTwips * scale;
    float dPointOffset = (float)safeSizeTwips / 20f;

    float width = getWidth().floatValue() / 20;
    float height = getHeight().floatValue() / 20;
    
    // already transformed
    float pdfX1 = 0f;			// left
    float pdfY1 = 0f;			// bottom
    
    float pdfX2 = width;	// right
    float pdfY2 = height;	// top
    
    PDFStyle style = new PDFStyle();
    style.setLineColor(new Color(0x7f, 0x7f, 0x7f));
    style.setLineWeighting(1.0f);
    style.setLineDash(5, 5, 0);
    pdfPage.setStyle(style);

    if ("ellipse".equals(shape.getShapeString()))
    {
      pdfPage.drawEllipse(pdfX1 + dPointOffset, pdfY1 + dPointOffset, pdfX2 - dPointOffset, pdfY2 - dPointOffset);
    }
    else if ("rect".equals(shape.getShapeString()))
    {
      pdfPage.drawRoundedRectangle(pdfX1 + dPointOffset, pdfY1 + dPointOffset, pdfX2 - dPointOffset, pdfY2 - dPointOffset, (float)(shape.getCornerRadius() / 20));
    }
    else // polygon shape
    {
    	GeneralPath path = shape.scaledGraphicsPath(newXScale, newYScale);
    	PathIterator pathIter= path.getPathIterator( new AffineTransform(1, 0, 0, -1, dOffset, height - dOffset)); 
    	
    	float[] coords = new float[6];
    	while (!pathIter.isDone())
    	{
    		int pathSegment = pathIter.currentSegment(coords);
    		switch (pathSegment)
    		{
    			case PathIterator.SEG_MOVETO:
    				pdfPage.pathMove(coords[0], coords[1]);
    				break;
    				
    			case PathIterator.SEG_LINETO:
    				pdfPage.pathLine(coords[0], coords[1]);
    				break;
    				
    				// never used...all quad paths are converted to cubics
 //   			case PathIterator.SEG_QUADTO:
 //   				pdfPage.pathArc(coords[0], coords[1], coords[2], coords[3]);
 //   				break;
    				
    			case PathIterator.SEG_CUBICTO:
    				pdfPage.pathBezier(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
    				break;
    				
    			default:
    				break;
    		}
    		pathIter.next();   		
    	}
    	pdfPage.pathPaint();
    }

  }

  /**
   * Draws the shape of the panel as a filled rectangle with a black 1-pixel line.
   * Note that this also removes the <code>clip</code> from the <code>graphics</code>
   * object because it interferes with the outline drawing.
   * @param graphics context for drawing
   * @param scale the scaling factor of the drawing
   */
  public void drawFilledShape(Graphics2D graphics, double scale, Color fillColor)
  {
    graphics.setClip(null);
    graphics.setColor(Color.black);
    graphics.setPaint(fillColor);
    graphics.draw(getShape().scaledGraphicsPath(scale));
    graphics.fill(getShape().scaledGraphicsPath(scale));
  }
 /**
   * Sets the clipping area of the Graphics2D to match the shape of this
   * AveryMasterpanel.  Polygons are not yet supported.
   * @param graphics the device context for this operation
   * @param width pixel width of target Graphics2D space
   * @param height pixel height of target Graphics2D space
   */
  public void maskShape(Graphics2D graphics, int width, int height)
  {
		double scale = ((double)width) / getWidth().doubleValue();

		graphics.clip(getShape().scaledGraphicsPath(scale));
  }

  public void setPaperimage(String newPaperimage)
  {
    paperimage = newPaperimage;
  }

  public String getPaperimage()
  {
    return paperimage;
  }
  
  public void setPaperColor(Color newPaperColor)
  {
  	paperColor = newPaperColor;
  }
  
  public Color getPaperColor()
  {
  	return paperColor;
  }

  /**
   * creates an outline image, devoid of content
   * @param maxSize assumes that the image must fit in a square
   * @param background color for active panel area
   * @return the newly created image
   */
  public BufferedImage makeWireframe(int maxSize, Color background)
  {
    double scale = AveryProject.calcRectScalar(
      new java.awt.Rectangle(getWidth().intValue(), getHeight().intValue()),
      (double)maxSize, (double)maxSize);

    int width = (int)(getWidth().doubleValue() * scale);
    int height = (int)(getHeight().doubleValue() * scale);

    // create the BufferedImage
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = image.createGraphics();

    // paint it the background color
    graphics.setBackground(background);
    graphics.clearRect(0, 0, width, height);

    // create the mask, and repaint entire rect with white
    maskShape(graphics, width, height);
    graphics.setColor(Color.white);
    graphics.fillRect(0, 0, width, height);

    // draw the panel outline
  	panelStrokeWidth = 2;
    drawShape(graphics, width, height, 0);

    // draw any cutouts
    Iterator i = getCutouts().iterator();
    while (i.hasNext())
    {
      ((AveryCutout)i.next()).draw(graphics, scale, background);
    }

    // cleanup
    graphics.dispose();
    return image;
  }

  /**
   * Generates an AveryMasterpanel suitable for a .avery Zip file bundle.  Images
   * are given new, unique names starting with <code>prefix</code>, and are
   * copied to the HighRes diretory under <code>location<code>
   * @param prefix - typically the machine name
   * @param location - where the project XML will ultimately be created
   * @param images - tracks the images already processed
   * @return a masterpanel, with image paths deleted, suitable for a .avery bundled project
   */
  AveryMasterpanel getMasterpanelForDotAveryBundle(String prefix, File location, Hashtable images)
  {
    // the superclass takes care of most stuff
    AveryMasterpanel panel =
      (AveryMasterpanel)super.getPanelForDotAveryBundle(prefix, location, images);

    if (panel != this)
    {
      // add masterpanel-specific attributes
      panel.setShape(this.getShape());
      panel.setID(this.getID());
      panel.setPaperimage(this.getPaperimage());

      panel.hints = hints;
      panel.textDefaults = textDefaults;
      panel.guides = guides;
      panel.masks = masks;
      panel.bleedable = bleedable;
      panel.mergeMapName = mergeMapName;
      panel.printOutline = printOutline;
      panel.outlineColor = outlineColor;
    }

    return panel;
  }

  /**
   * Check to see if there's anything that the ChangeStyle servlet in AP4
   * can work with (a background or some text).
   * @return true if this has something to offer ChangeStyle
   */
  public boolean isStylableInAveryPrint4()
  {
    Iterator iterator = getPanelFields().iterator();
    while (iterator.hasNext())
    {
      AveryPanelField field = (AveryPanelField)iterator.next();
      if (field instanceof AveryBackground)
      {
        return true;
      }
      if (field instanceof AveryTextfield)
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Creates an Avery.masterpanel element to use in a JDOM Document
   * @return newly created Avery.masterpanel Element
   */
  public Element getAveryElement()
  {
    Element element = new Element("Avery.masterpanel");

    // add attributes
    element.setAttribute("width", getWidth().toString());
    element.setAttribute("height", getHeight().toString());
    element.setAttribute("id", createLegacyID(getID()).toString());
    if (getDescription() != null)
    {
      element.setAttribute("description", getDescription());
    }

    if (getShape().getShapeString().equals("rect"))
    {
    	if (getShape().getCornerRadius() > 0)
    	{
    		element.setAttribute("cornerradius", Double.toString(getShape().getCornerRadius()));
    	}
    }
    else
    {
      element.setAttribute("shape", getShape().getShapeString());
    }

    if (printOutline.equals("true"))
    {
    	element.setAttribute("printOutline", printOutline);
    }
    
    if (!outlineColor.equals("null"))
    {
    	element.setAttribute("outlineColor", outlineColor);
    }
    
    // add content
    if (getPaperimage() != null)
    {
      Element paperElement  = new Element("Avery.paperimage");
      paperElement.setAttribute("src", getPaperimage());
      element.addContent(paperElement);
    }

    // add Avery.mp.field Elements
    Iterator iterator = getPanelFields().iterator();
    while (iterator.hasNext())
    {
      AveryPanelField field = (AveryPanelField)iterator.next();
      field.setAveryElementName("Avery.mp.field");
      element.addContent(field.getAveryElement());
    }

    // add any Avery.cutouts Elements
    iterator = getCutouts().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((XMLElement)iterator.next()).getAveryElement());
    }

    return element;
  }

  /**
   * Outputs this masterpanel as an XML file
   * @param file - the file to be created
   * @throws IOException
   * @throws JDOMException
   */
  public void writeMasterpanelFile(File file)
  throws IOException, JDOMException
  {
    Document document = new Document(getAverysoftElement());
    FileOutputStream out = new FileOutputStream(file);
    new XMLOutputter(" ", true).output(document, out);
    out.flush();
    out.close();
  }

  /**
   * An application won't want to allow two backgrounds
   * @return true if the panel has an AveryBackground field
   */
  public boolean hasBackground()
  {
    Iterator iterator = getPanelFields().iterator();
    while (iterator.hasNext())
    {
      AveryPanelField field = (AveryPanelField)iterator.next();
      if (field instanceof AveryBackground)
      {
        return true;
      }
    }

    return false;
  }

  public boolean allowReorientation()
  {
  	// don't allow some types of masterpanel shapes
  	boolean allow = getShape().allowReorientation();
  	
  	if (allow)
  	{
  		// don't allow polygon guides shape - not supported as an AveryShape yet
      Iterator guideIter = guides.iterator();
      while (guideIter.hasNext())
      {
      	Guide guide = (Guide)guideIter.next();
      	if (!guide.allowReorientation())
      		allow = false;
      }
  	}

  	return allow;
  }

  void reorient(boolean clockwise)
  {
  	getShape().reorient(clockwise);
    
    // reorient the textDefaults too
    double newWidth = this.getWidth().doubleValue();
    Iterator tds = textDefaults.iterator();
    while (tds.hasNext())
    {
      ((TextDefaults)tds.next()).reorient(newWidth);
    }
    
    Iterator cutoutIter = getCutouts().iterator();
    while (cutoutIter.hasNext())
    {
    	AveryCutout cutout = (AveryCutout)cutoutIter.next();
    	cutout.reorient(getHeight(), getWidth(), clockwise);
    }
    
    Iterator guideIter = guides.iterator();
    while (guideIter.hasNext())
    {
    	Guide guide = (Guide)guideIter.next();
    	guide.reorient(getHeight(), getWidth(), clockwise);
    }
  }

  /**
   * helper method generates the masterpanel JDOM element, minus any content
   * @return the averysoft.xsd-compatible element
   */
  private Element makeAverysoftElement()
  {
    Element element = new Element("masterpanel");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    // fixup for invalid master panel id
    // a source of these could be dtd based projects...
    if (!AveryUtils.isNCName(getID()))
      setID("MP" + getID());

    element.setAttribute("id", getID());
		
		element.setAttribute("width", new Double(getWidth()).toString());
		element.setAttribute("height", new Double(getHeight()).toString());
		//}
		if (getShape().getShapeString().equals("rect"))
		{
			if (getShape().getCornerRadius() > 0)
			{
				element.setAttribute("cornerRadius", Double.toString(getShape().getCornerRadius()));
			}
		}
		else
		{
			element.setAttribute("shape", getShape().getShapeString());
		}

		if (isBleedable())
		{
			element.setAttribute("bleed", "true");
		}
    
		if (noBleedSides != null)
		{
			element.setAttribute("noBleedSides", noBleedSides);
		}
    
		if (printOutline.equals("true"))
    {
    	element.setAttribute("printOutline", printOutline);
    }

    if (isNoMerge())
    {
    	element.setAttribute("noMerge", "true");
    }
    
    if (!isEditable())
    {
    	element.setAttribute("editable", "false");
    }

    if (!isPreviewable())
    {
    	element.setAttribute("previewable", "false");
    }


    return element;
  }

  /**
   * helper method generates the masterpanel JDOM element, minus any content
   * @return the averysoft.xsd-compatible element
   */
  private Element makeAverysoftIBElement()
  {
    Element element = new Element("panel");
    element.setNamespace(AveryProject.getAverysoftNamespace());

    // fixup for invalid master panel id
    // a source of these could be dtd based projects...
    /*if (!AveryUtils.isNCName(getID()))
      setID("MP" + getID());

    element.setAttribute("id", getID());*/
    double scalex = getWidth().doubleValue() / 56.6929138 / shape.getWidth();
    double scaley = getHeight().doubleValue() / 56.6929138 / shape.getHeight();
    shape.scale(scalex, scaley);

		
		double dWidth = getWidth();
		double dHeight = getHeight();
		dWidth = (double)Math.round(100 * dWidth) / 100;
		dHeight = (double)Math.round(100 * dHeight) / 100;

		element.setAttribute("width", new Double(dWidth).toString());
		element.setAttribute("height", new Double(dHeight).toString());
		//}
		if (getShape().getShapeString().equals("rect"))
		{
			if (getShape().getCornerRadius() > 0)
			{
				element.setAttribute("cornerRadius", AveryProject.twipsToMM(getShape().getCornerRadius()));
			}
		}
//		else
//		{
			element.setAttribute("shape", getShape().getShapeString());
//		}

		if (isBleedable())
		{
			element.setAttribute("bleed", "true");
		}
    
		/*if (noBleedSides != null)
		{
			element.setAttribute("noBleedSides", noBleedSides);
		}
    
		if (printOutline.equals("true"))
    {
    	element.setAttribute("printOutline", printOutline);
    }

    if (isNoMerge())
    {
    	element.setAttribute("noMerge", "true");
    }*/
    
    if (!isEditable())
    {
    	element.setAttribute("editable", "false");
    }

    if (!isPreviewable())
    {
    	element.setAttribute("previewable", "false");
    }


    return element;
  }


  /**
   * Get a JDOM representation of the object suitable for
   * inclusion in an XML file based on averysoft.xsd
   * @return an Averysoft-based JDOM Element
   */
  public Element getAverysoftElement()
	{
    Element element = makeAverysoftElement();

    if (getMergeMapName().length() > 0 && !getMergeMapName().equals("none"))
    {
    	element.setAttribute("mergeMap", getMergeMapName());
    }
    element.addContent(getDescriptionObject().getAverysoftElement());

    // hints
		if (hints != null && !hints.isEmpty())
		{
			java.util.Enumeration e = hints.keys();
			while (e.hasMoreElements())
			{
				Hint hint = (Hint)hints.get(e.nextElement());
				element.addContent(hint.getAverysoftElement());
			}
		}

    // textDefaults
		Iterator iterator = getTextDefaults().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}

		// polypoints
		Element polyPoints = getShape().getPolypointsElement();
		if (polyPoints != null)
		{
			element.addContent(polyPoints);
		}

    // cutouts
		iterator = getCutouts().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}

    // guides
		if (guides != null)
		{
			iterator = guides.iterator();
			while (iterator.hasNext())
			{
				element.addContent(((Guide)iterator.next()).getXmlElement());
			}
		}
		
    // masks
    if (masks != null)
    {
      iterator = masks.iterator();
      while (iterator.hasNext())
      {
        element.addContent(((Mask)iterator.next()).getAverysoftElement());
      }
    }

		// fields
    iterator = getPanelFields().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
    }

    return element;
  }

  /**
   * Get a JDOM representation of the object suitable for
   * inclusion in an XML file based on averysoft.xsd
   * @return an Averysoft-based JDOM Element
   */
  public Element getAverysoftIBElement()
	{
    Element element = makeAverysoftIBElement();

    /*if (getMergeMapName().length() > 0 && !getMergeMapName().equals("none"))
    {
    	element.setAttribute("mergeMap", getMergeMapName());
    }
    element.addContent(getDescriptionObject().getAverysoftElement());

    // hints
		if (hints != null && !hints.isEmpty())
		{
			java.util.Enumeration e = hints.keys();
			while (e.hasMoreElements())
			{
				Hint hint = (Hint)hints.get(e.nextElement());
				element.addContent(hint.getAverysoftElement());
			}
		}

    // textDefaults
		Iterator iterator = getTextDefaults().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}*/

		// polypoints
		Element polyPoints = getShape().getPolypointsElement();
		if (polyPoints != null)
		{
			element.addContent(polyPoints);
		}

    // cutouts
		Iterator iterator = getCutouts().iterator();
		while (iterator.hasNext())
		{
			element.addContent(((Averysoftable)iterator.next()).getAverysoftElement());
		}

    // guides
		if (guides != null)
		{
			iterator = guides.iterator();
			while (iterator.hasNext())
			{
				element.addContent(((Guide)iterator.next()).getXmlIBElement());
			}
		}
		
    // masks
    /*if (masks != null)
    {
      iterator = masks.iterator();
      while (iterator.hasNext())
      {
        element.addContent(((Mask)iterator.next()).getAverysoftElement());
      }
    }*/

		// fields
    iterator = getPanelFields().iterator();
    while (iterator.hasNext())
    {
      element.addContent(((Averysoftable)iterator.next()).getAverysoftIBElement());
    }

    return element;
  }
  
	/**
	 * Hints are stored as name/value pairs in averysoft XML,
	 * and as a Hashtable in the AveryProject object.
	 * @param name
	 * @return the named Hint, or <code>null</code> if the Hint does not exist
	 */
	public Hint getHint(String name)
	{
		if (hints != null)
		{
			return (Hint)hints.get(name);
		}
		return null;
	}

	/**
	 * This sets a Hint in the project.  There is validation of neither
	 * name nor value.  The Hint will be written into averysoft XML.
	 * @param name - the name used for the Hint
	 * @param value - the value of the Hint
	 */
	public void addHint(String name, String value)
	{
		if (hints == null)
		{
			hints = new Hashtable();
		}
		else if (hints.contains(name))
		{
			hints.remove(name);
		}

		hints.put(name, new Hint(name, value));
	}

	/**
	 * Use this method to remove a specific Hint.
	 * @param name - the name of the Hint
	 * @return the Hint that was removed,
	 * or <code>null</code> if the Hint didn't exist
	 */
	public Hint removeHint(String name)
	{
		if (hints == null || hints.containsKey(name) == false)
		{
			return null;
		}
		Hint hint = (Hint)hints.get(name);
		hints.remove(name);
		return hint;
	}

	public void setDisplayPromptOrder(boolean displayOrder)
	{
		displayPromptOrder = displayOrder;
	}
	
	public AveryTextblock createDefaultTextblock()
	{
		if (getTextDefaults().isEmpty())
		{
			// create a new default TextDefaults object.
			getTextDefaults().add(new TextDefaults(this));
		}

		return ((TextDefaults)getTextDefaults().get(0)).createTextblock();
	}

	static Integer createLegacyID(String str)
	{
		try
		{
			int i = 0;
			while (i < str.length())
			{
				char c = str.charAt(i);
				if (c <= '9' && c >= '0')
					break;
				++i;
			}

			return Integer.decode(str.substring(i));
		}
		catch (Exception e)
		{
			int sum = 0;
			for (int i=0; i < str.length(); i++)
			{
				sum += (int)str.charAt(i);
			}

			return new Integer(sum);
		}
	}

	/**
	 * @return Returns the mergeMapName.
	 */
	public String getMergeMapName()
	{
		return mergeMapName;
	}

	/**
	 * @param mergeMapName The mergeMapName to set.
	 */
	public void setMergeMapName(String mergeMapName)
	{
		if (mergeMapName == null)
		{
			mergeMapName = new String("");
		}

		this.mergeMapName = mergeMapName;
	}

	/**
	 * This method determines if all of the keys listed in <code>mergeKeys</code>
	 * are accounted for in the masterpanel's fields.  As it discovers keys in the
	 * fields, they are removed from the Hashtable.  On complete success (all keys
	 * accounted for) <code>mergeKeys</code> will be empty on return.
	 * @param mergeKeys
	 * @return <code>true</code> if all keys are accounted for.  <code>false</code>
	 * if any keys could not be found.  Only the unfound keys will remain in the
	 * Hashtable <code>mergeKeys</code> on return.
	 */
	public boolean supportsMergeKeys(Hashtable mergeKeys)
	{
		Iterator it = getFieldIterator();
		while (it.hasNext() && !mergeKeys.isEmpty())
		{
			AveryPanelField field = (AveryPanelField)it.next();
			Enumeration keys = mergeKeys.keys();
			while (keys.hasMoreElements())
			{
				String key = (String)keys.nextElement();
				if (field.hasMergeKey(key))
				{
					mergeKeys.remove(key);
				}
			}
		}

		return mergeKeys.isEmpty();
	}
  
  /**
   * Adds a panelField to this panel
   * @param field - the field to be added
   */
  public void addField(AveryPanelField field)
  {
    try
    {
      // guarantee unique promptOrder
      double pNew = Double.parseDouble(field.getPromptOrder());
      Iterator fields = getPromptSortedPanelFields().iterator();
      while (fields.hasNext())
      {
        if (pNew == Double.parseDouble(((AveryPanelField)fields.next()).getPromptOrder()))
        {
          pNew += 1.0;          
        }
      }
      field.setPromptOrder(Double.toString(pNew));
      
      // guarantee unique zOrder
      double zNew = field.getZLevel();    
      fields = new DrawIterator(getPanelFields());
      while (fields.hasNext())
      {
        if (zNew == ((AveryPanelField)fields.next()).getZLevel())
        {
          zNew += 1.0;          
        }
      }  
      field.setZLevel(zNew);
    } 
    catch (Exception e) 
    { 
      e.printStackTrace();
    }    

    this.getPanelFields().add(field);
  }
  
  /**
   * Adds an outline of this masterpanel's shape to the PDF document.
   * @param pdfPage - the pdf page of the document
   * @param x - upper left x in twip space
   * @param y - upper left y in twip space
   * @param pageHeight - height of PDFPage in twips
   * @throws Exception on pdfDoc error
  */
  void addOutlineToPDF(PDFPage pdfPage, double x, double y, double pageHeight)
  throws Exception
  {
    float width = getWidth().floatValue() / 20;
    float height = getHeight().floatValue() / 20;
    
    // already transformed
    float pdfX1 = 0f;			// left
    float pdfY1 = 0f;			// bottom
    
    float pdfX2 = width;	// right
    float pdfY2 = height;	// top
    
    PDFStyle style = new PDFStyle();

    if (isPrintOutline())
    {
	    // Draw panel outline using SPOT_COLOR
	    Color fallback = CMYKColorSpace.getColor(
	    		SPOT_COLOR_PANEL[0],
	    		SPOT_COLOR_PANEL[1],
	    		SPOT_COLOR_PANEL[2],
	    		SPOT_COLOR_PANEL[3]);
	    SpotColorSpace spotInk = new SpotColorSpace(AveryMasterpanel.SPOT_COLOR_NAME, fallback);
	    Color spotColor = spotInk.getColor(1);
	    style.setLineColor(spotColor);
    }
    else
    {
    	style.setLineColor(Color.black);
    }
    style.setLineWeighting(1.0f);
    pdfPage.setStyle(style);
    
    if ("ellipse".equals(shape.getShapeString()))
    {
      pdfPage.drawEllipse(pdfX1, pdfY1, pdfX2, pdfY2);
    }
    //else 
    //if ("rect".equals(shape.getShapeString()))
    else if ("rect".equals(shape.getShapeString()))
    {
      pdfPage.drawRoundedRectangle(pdfX1, pdfY1, pdfX2, pdfY2, (float)(shape.getCornerRadius() / 20));
    }
    //else // polygon shape
    else
    {
    	GeneralPath path = shape.scaledGraphicsPath(.05);
    	PathIterator pathIter= path.getPathIterator( new AffineTransform(1, 0, 0, -1, 0, height)); 
    	
    	float[] coords = new float[6];
    	while (!pathIter.isDone())
    	{
    		int pathSegment = pathIter.currentSegment(coords);
    		switch (pathSegment)
    		{
    			case PathIterator.SEG_MOVETO:
    				pdfPage.pathMove(coords[0], coords[1]);
    				break;
    				
    			case PathIterator.SEG_LINETO:
    				pdfPage.pathLine(coords[0], coords[1]);
    				break;
    				
    				// never used...all quad paths are converted to cubics
 //   			case PathIterator.SEG_QUADTO:
 //   				pdfPage.pathArc(coords[0], coords[1], coords[2], coords[3]);
 //   				break;
    				
    			case PathIterator.SEG_CUBICTO:
    				pdfPage.pathBezier(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
    				break;
    				
    			default:
    				break;
    		}
    		pathIter.next();   		
    	}
    	pdfPage.pathPaint();
    }
    
    // if customProof is defined draw safe line
		if (customProof != null && customProof.length() > 0)
		{
			addBleedShapeToPDF(pdfPage, .05);
			addSafeShapeToPDF(pdfPage, .05);
		}

    Iterator cutouts = getCutouts().iterator();
    while (cutouts.hasNext())
    {
      ((AveryCutout)cutouts.next()).addOutlineToPDF(pdfPage, 0, 0, width, height);
    }
  }
  
  public void removeCombinedMasterElements()
  {
  	guides = new ArrayList();
  	clearCutouts();
  	masks = new ArrayList(); 	
  	hints = null;
  }
  
  /*static public double slope(double x1, double y1, double x2, double y2)
  {
  	double diff = x2 - x1;
  	if (diff >= 0.0 && diff < 0.0001)
  		diff = 0.0001;
  	else if (diff > -0.0001 && diff < 0.0)
  		diff = -0.0001;
  	
  	return (y2 - y1) / diff;
  }*/

}