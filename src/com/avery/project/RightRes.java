/**
 * <p>Title: RightRes</p>
 * <p>Description: Generates and maintains images in requested resolutions</p>
 * <p>Copyright: Copyright 2002 Avery Dennison Corp.</p>
 * <p>Author: Bob Lee
 */

package com.avery.project;

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.*;

import com.avery.predesigner.Predesigner;
import com.avery.utils.*;
import com.sun.image.codec.jpeg.*;
import com.sun.media.imageio.plugins.tiff.TIFFImageWriteParam;
import com.sun.media.imageioimpl.plugins.tiff.TIFFImageWriter;
import com.sun.media.imageioimpl.plugins.tiff.TIFFImageWriterSpi;
import com.twelvemonkeys.imageio.plugins.psd.PSDImageReader;
import com.twelvemonkeys.imageio.plugins.psd.PSDImageReaderSpi;





import java.awt.Toolkit;
import java.awt.image.ImageObserver;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.JOptionPane;

import org.faceless.pdf2.PDF;
import org.faceless.pdf2.PDFParser;
import org.faceless.pdf2.PDFReader;
import org.faceless.pdf2.PagePainter;

//import javax.imageio.IIOImage;
//import javax.imageio.ImageIO;
//import javax.imageio.ImageTypeSpecifier;
//import javax.imageio.ImageWriteParam;
//import javax.imageio.ImageWriter;
//import javax.imageio.stream.FileImageInputStream;
//import javax.imageio.stream.ImageOutputStream;

//import org.apache.batik.ext.awt.image.spi.ImageWriter;

public class RightRes
{
  /**
   * This static hashtable maintains highres image File objects as keys
   * corresponding to lists of associated lowres ImageFile objects.
   */
  static private Hashtable imageFiles;

  /**
   * This is used to hold the high resolution version of the current image if
   * we have to decode it.
   */
  // private Image highresImage = null;

  /**
   * The constructor creates the internal static Hashtable if it didn't already
   * exist.
   */
  public RightRes()
  {
    if (imageFiles == null)
    {
      imageFiles = new Hashtable();
    }
  }
  
  /**
   * This static, application-level switch controls the behavior of
   * getPrintableJpegFile. If <code>true</code>, calls to getPrintableJpegFile
   * return the original highRes image.  The default value is 
   * <code>false</code>. Programs that require this functionality should set it
   * to <code>true</code> during initialization.
   * @see #getPrintableJpegFile(AveryImage) 
   */
  static public boolean highResIsPrintable = false;

  /**
   * Creates an Image of the correct resolution, stores a copy of it to disk
   * as a JPEG, and stores a reference to that file in the imageFiles Hashtable
   * for future reference.
   * @param aImage contains a reference to the highres image file and gallery
   * @param targetWidth preferred output width in pixels
   * @param targetHeight preferred output height in pixels
   * @return a ready-to-use Image, or a blank white image on failure
   */
  public Image getImage(AveryImage aImage, int targetWidth, int targetHeight)
  {
    ArrayList allresList = getAllresList(aImage);
    
    if (allresList == null)
    {
      BufferedImage blankImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
      Graphics gr = blankImage.getGraphics();
      gr.setColor(Color.white);
      gr.fillRect(0, 0, targetWidth-1, targetHeight-1);
      gr.dispose();  gr = null;
      return blankImage;
    }

    // get highres source + dimensions, always entry 0 in the list
    ImageFile highresImageFile = (ImageFile)allresList.get(0);

    if (aImage.maintainAspect())  // target size might need to be adjusted
    {
      // recalc target dimensions to match aspect of highresImageFile
      double imageAspect = highresImageFile.getAspect();
      double targetAspect = (double)targetWidth / (double)targetHeight;

      // reduce one dimension of target space to make it aspect correct
      if (targetAspect > imageAspect)
      {
        targetWidth = (int)((double)targetWidth * (imageAspect / targetAspect));
      }
      else if (targetAspect < imageAspect)
      {
        targetHeight = (int)((double)targetHeight * (targetAspect / imageAspect));
      }
    }

    // see if there is a matching lowres in the list
    ImageFile lowresImageFile = findRightRes(allresList, targetWidth, targetHeight);

    // make the image to send back to the caller
    Image lowresImage = null;
    try
    {
      if (lowresImageFile == null)
      { // create new Image at target resolution
      	Image highresImage = null;
      	
        if (aImage.getImageType() == AveryImage.TYPE_JPG)
        {
        	highresImage = aImage.readJPGImageFile( highresImageFile.getFile().getAbsolutePath());
        	aImage.setSrcWidth(highresImage.getWidth(null));
        	aImage.setSrcHeight(highresImage.getHeight(null));
       }
        else if (aImage.getImageType() == AveryImage.TYPE_PNG)
        {
        	highresImage = aImage.readPNGImageFile( new FileInputStream(highresImageFile.getFile()));
        	aImage.setSrcWidth(highresImage.getWidth(null));
        	aImage.setSrcHeight(highresImage.getHeight(null));
        }
        else if (aImage.getImageType() == AveryImage.TYPE_TIF)
        {
        	highresImage = aImage.readTIFImageFile(highresImageFile.getFile().getAbsolutePath());
        	aImage.setSrcWidth(highresImage.getWidth(null));
        	aImage.setSrcHeight(highresImage.getHeight(null));
        }
        else if (aImage.getImageType() == AveryImage.TYPE_WEBP)
        {
        	highresImage = aImage.readWEBPImageFile( highresImageFile.getFile().getAbsolutePath());
        	aImage.setSrcWidth(highresImage.getWidth(null));
        	aImage.setSrcHeight(highresImage.getHeight(null));
        }
       // else if (aImage.getImageType() == AveryImage.TYPE_PSD)
        //{
        	//highresImage = aImage.readTIFImageFile( new FileInputStream(highresImageFile.getFile()));
        	//aImage.setSrcWidth(highresImage.getWidth(null));
        	//aImage.setSrcHeight(highresImage.getHeight(null));
        //}
        else if (aImage.getImageType() == AveryImage.TYPE_BMP)
        {
        	highresImage = readBmpImageFile(highresImageFile.getFile());
        }
        else if (aImage.getImageType() == AveryImage.TYPE_PDF)
        {
        	highresImage = aImage.readPDFImageFile(highresImageFile.getFile());
        }
        else  // this method can't handle anything else
        {
          BufferedImage blankImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
          Graphics gr = blankImage.getGraphics();
          gr.setColor(Color.white);
          gr.fillRect(0, 0, targetWidth-1, targetHeight-1);
          gr.dispose();		gr = null;
          return blankImage;
        }

        // scale the image down
        BufferedImage scaledImage = new BufferedImage(targetWidth, targetHeight,
          aImage.mayBeTransparent() ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB);

        Graphics scaledGr = scaledImage.getGraphics();
        scaledGr.drawImage(highresImage, 0, 0, targetWidth, targetHeight, AveryImage.getObs());
        try {
	        if (!waitForImageBits(Toolkit.getDefaultToolkit(), scaledImage))
	        {
	          throw(new Exception("couldn't scale image"));
	        }
        }
        finally {
        	scaledGr.dispose();   // done with this Graphics object
        	scaledGr = null;
        	highresImage = null;
        }

        lowresImage = scaledImage;

        // save the new image as a JPEG
        if (aImage.mayBeTransparent() == false)
        {
          // save scaledImage for next time
          String lowresName = aImage.getSource().substring(0, aImage.getSource().lastIndexOf('.') + 1)
            + targetWidth + "." + targetHeight + ".jpg";
          File file = new File(lowresPath(aImage.getGallery()), lowresName);

          FileOutputStream out = new FileOutputStream(file);

          // lowres files are always jpegs
          JPEGImageEncoder jpeg = JPEGCodec.createJPEGEncoder(out);
          JPEGEncodeParam eparam;
          eparam = JPEGCodec.getDefaultJPEGEncodeParam(scaledImage);
          eparam.setQuality( 0.75f, false );
          jpeg.setJPEGEncodeParam( eparam );
          jpeg.encode(scaledImage);
          out.flush();
          out.close();

          // add this new lowres file reference to the allresList
          allresList.add(new ImageFile(file, targetWidth, targetHeight));
        }
      }
      else
      { // create an Image from the existing lowres file
        lowresImage = JPEGCodec.createJPEGDecoder(
          new FileInputStream(lowresImageFile.getFile())).decodeAsBufferedImage();
      }
    }
    catch (Exception e)
    {
      System.err.println(AveryUtils.timeStamp() + e.toString());
      return lowresImage;
    }

    return lowresImage;
  }

  /**
   * Gets an image file suitable for printing, re-rendering at a lower
   * resolution if {@link #highResIsPrintable} is false.
   * @param aImage
   * @return JPEG file guaranteed to exist, or <code>null</code> on failure
   */
  File getPrintableJpegFile(AveryImage aImage)
  {
    if (highResIsPrintable)
    {
      File file = new File(aImage.getHighresFilespec());
      return file.exists() ? file : null;
    }
    
    double MAX_DPI = 300;
    double MIN_DPI = 200;
    double OVERSIZED_DPI = 200;

    ArrayList allresList = getAllresList(aImage);
    
    if (allresList == null)
    {
    	File file = new File(aImage.getHighresFilespec());
    	return file.exists() ? file : null;
    }

    // get highres source + dimensions, always entry 0 in the list
    ImageFile highresImageFile = (ImageFile)allresList.get(0);

    // we'll return the highres file if it looks right
    File rightresFile = null;
    if (aImage.getImageType() == AveryImage.TYPE_JPG)
    {
      rightresFile = highresImageFile.getFile();
    }

    // get AveryImage size (starts in twips)
    int targetWidth = aImage.getWidth().intValue();
    int targetHeight = aImage.getHeight().intValue();

    if (aImage.maintainAspect())  // target aspect might need to be adjusted
    {
      // recalc target dimensions to match aspect of highresImageFile
      double imageAspect = highresImageFile.getAspect();
      double targetAspect = (double)targetWidth / (double)targetHeight;

      // reduce one dimension of target space to make it aspect correct
      if (targetAspect > imageAspect)
      {
        targetWidth = (int)(((double)targetWidth) * (imageAspect / targetAspect));
      }
      else if (targetAspect < imageAspect)
      {
        targetHeight = (int)(((double)targetHeight) * (targetAspect / imageAspect));
      }
    }

    double inchesWide = ((double)targetWidth) / 1440;
    double inchesHigh = ((double)targetHeight) / 1440;
    double highresDpiWide = highresImageFile.getWidth() / inchesWide;
    double highresDpiHigh = highresImageFile.getHeight() / inchesHigh;
    boolean oversized = (inchesWide > 18) || (inchesHigh > 18);

    if ((highresDpiWide > MAX_DPI && highresDpiHigh > MAX_DPI) 
        || (aImage.getImageType() == AveryImage.TYPE_BMP)
        || (oversized && highresDpiWide > OVERSIZED_DPI && highresDpiHigh > OVERSIZED_DPI))
    {   // we need a suitable lowres file
      targetWidth = (int)(inchesWide * (oversized ? OVERSIZED_DPI : MIN_DPI));
      targetHeight = (int)(inchesHigh * (oversized ? OVERSIZED_DPI : MIN_DPI));

      ImageFile rightresImageFile = findRightRes(allresList, targetWidth, targetHeight);
      if (rightresImageFile != null && rightresImageFile.getFile().exists()
        && rightresImageFile.getFile().getName().toLowerCase().endsWith(".jpg"))
      {
        rightresFile = rightresImageFile.getFile();
      }
      else try // doesn't exist yet
      {     // create new Image at target resolution
      	Image highresImage = null;
        if (aImage.getImageType() == AveryImage.TYPE_JPG)
        {
          highresImage = aImage.readJPGImageFile(highresImageFile.getFile().getAbsolutePath());
        }
        else if (aImage.getImageType() == AveryImage.TYPE_PNG)
        {
          highresImage = aImage.readPNGImageFile(new FileInputStream(highresImageFile.getFile()));
        }
        else if (aImage.getImageType() == AveryImage.TYPE_TIF)
        {
          highresImage = aImage.readTIFImageFile(highresImageFile.getFile().getAbsolutePath());
        }
        else if (aImage.getImageType() == AveryImage.TYPE_WEBP)
        {
          highresImage = aImage.readWEBPImageFile(highresImageFile.getFile().getAbsolutePath());
        }
        //else if (aImage.getImageType() == AveryImage.TYPE_PSD)
        //{
          //highresImage = aImage.readTIFImageFile( new FileImageInputStream(highresImageFile.getFile()));
        //}
        else if (aImage.getImageType() == AveryImage.TYPE_PDF)
        {
        	highresImage = aImage.readPDFImageFile(highresImageFile.getFile());
        }
        else if (aImage.getImageType() == AveryImage.TYPE_BMP)
        {
          highresImage = readBmpImageFile(highresImageFile.getFile());
        }
        else  // this method can't handle anything else
        {
          System.err.println(AveryUtils.timeStamp()
            + highresImageFile.getFile().getAbsolutePath() + " not JPG, PNG, TIF, WEBP, or BMP!?");
          return null;
        }

        // scale the image down
        BufferedImage scaledImage = new BufferedImage(targetWidth, targetHeight,
          aImage.mayBeTransparent() ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB);

        Graphics scaledGr = scaledImage.getGraphics();
        scaledGr.drawImage(highresImage, 0, 0, targetWidth, targetHeight, AveryImage.getObs());
        try {
	        if (!waitForImageBits(Toolkit.getDefaultToolkit(), scaledImage))
	        {
	          throw(new Exception("couldn't scale image"));
	        }
          
          reduceColorSpace(scaledImage);
        }
        finally {
        	scaledGr.dispose();   // done with this Graphics object
        	scaledGr = null;
        	highresImage = null;
        }
        

        // save the new image as a JPEG
        if (aImage.mayBeTransparent() == false)
        {
          // save scaledImage for next time
          String lowresName = aImage.getSource().substring(0, aImage.getSource().lastIndexOf('.') + 1)
            + targetWidth + "." + targetHeight + ".jpg";
          File file = new File(lowresPath(aImage.getGallery()), lowresName);

          FileOutputStream out = new FileOutputStream(file);

          // lowres files are always jpegs
          JPEGImageEncoder jpeg = JPEGCodec.createJPEGEncoder(out);
          JPEGEncodeParam eparam;
          eparam = JPEGCodec.getDefaultJPEGEncodeParam(scaledImage);
          eparam.setQuality(1.00f, false );
          jpeg.setJPEGEncodeParam( eparam );
          jpeg.encode(scaledImage);
          out.flush();
          out.close();

          // add this new lowres file reference to the allresList
          allresList.add(new ImageFile(file, targetWidth, targetHeight));

          rightresFile = file;
        }
      }
      catch (Exception e)
      {
        System.err.println(AveryUtils.timeStamp() + e.toString());
      }
    }

    return rightresFile;
  }


  ImageFile findRightRes(ArrayList allresList, int targetWidth, int targetHeight)
  {
    Iterator iterator = allresList.iterator();
    while (iterator.hasNext())
    {
      ImageFile imageFile = (ImageFile)iterator.next();
      if (imageFile.sizeMatches(targetWidth, targetHeight))
      {
        if (imageFile.getFile().exists())
        {
          return imageFile;
        }
      }
    }
    return null;
  }
  

  /**
   * Fetches the allres list for this AveryImage, creating it if necessary.
   * If the highres image had to be decoded, it is returned in the highresImage
   * class member for efficiency.
   * @param aImage - the AveryImage of interest
   * @return an array of ImageFile objects, or <code>null</code>
   */
  public ArrayList getAllresList(AveryImage aImage)
  {
    ArrayList allresList;   // will be returned

    File sourceFile = new File(aImage.getHighresFilespec());
    if (sourceFile.exists() == false)
    {
      System.err.println(AveryUtils.timeStamp() + " File not found: " + sourceFile.getAbsolutePath());
      return null;  // no tickee, no washee
    }

    if (imageFiles.containsKey(sourceFile))
    { // we've seen this file before
      allresList = (ArrayList)imageFiles.get(sourceFile);
      return allresList;
    }
    else  // first time for this sourceFile
    {
      allresList = new ArrayList();
      BufferedImage highresImage = null;
      // open the highres file to determine its dimensions
      try
      {
        if (aImage.getImageType() == AveryImage.TYPE_JPG)
        {
          highresImage = aImage.readJPGImageFile(sourceFile.getAbsolutePath());
        }
        else if (aImage.getImageType() == AveryImage.TYPE_PNG)
        {
          highresImage = aImage.readPNGImageFile(aImage.getHighresFilespec());
        }
        else if (aImage.getImageType() == AveryImage.TYPE_TIF)
        {
          highresImage = aImage.readTIFImageFile(aImage.getHighresFilespec());
        }
        else if (aImage.getImageType() == AveryImage.TYPE_PDF)
        {
        	highresImage = aImage.readPDFImageFile(new File(aImage.getHighresFilespec()));
        }
        else if (aImage.getImageType() == AveryImage.TYPE_WEBP)
        {
          highresImage = aImage.readWEBPImageFile(aImage.getHighresFilespec());
        }
        else if (aImage.getImageType() == AveryImage.TYPE_PSD)
        {
        	//Iterator readers = ImageIO.getImageReadersByFormatName("psd");
        	//PSDImageReader psdir = (PSDImageReader)readers.next();
        	PSDImageReader psdir = new PSDImageReader(new PSDImageReaderSpi());
          	ImageInputStream iis = ImageIO.createImageInputStream(new File(aImage.getHighresFilespec()));
          	psdir.setInput(iis);
          	
          	short bitDepth = psdir.getBits();
          	short mode = psdir.getMode();

          	if (bitDepth > 8)
          		return null;
          	
        	//System.out.println(aImage.getHighresFilespec());
        	// convert PSD to TIF then import the TIF
        	//FileInputStream fis = new FileInputStream(sourceFile);
    	    BufferedImage bufferedImage = psdir.read(0); //ImageIO.read(fis);
    	    iis.close();
    	    
          	// if CMYK convert to RGB...
          	if (mode == 4)
          	{
          		ColorSpace cs = bufferedImage.getColorModel().getColorSpace();
          		//int csType = cs.getType();
    	      	BufferedImage dst = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(),BufferedImage.TYPE_INT_RGB);
    	      	//ColorSpace dcs = dst.getColorModel().getColorSpace();
    	      	//int dscType = dcs.getType();
    	      	ColorConvertOp op = new ColorConvertOp(cs, dst.getColorModel().getColorSpace(), null);
    	      	op.filter(bufferedImage,  dst);
    	      	bufferedImage = null;
    	      	bufferedImage = dst;  		
          	}

        	//int numComponents = bufferedImage.getColorModel().getNumComponents();
        	
    	    String tif = aImage.getHighresFilespec();
    	    tif = tif.replace(".psd",  ".tif");
    	    sourceFile = new File(tif);
    	       	    
          	TIFFImageWriter tifiw = new TIFFImageWriter(new TIFFImageWriterSpi());
          	
          	if (tifiw != null)
          	{
    		    File tifFile = new File(tif);
    		    try
    		    {
    		    	ImageOutputStream ios = ImageIO.createImageOutputStream(tifFile);
    		    	tifiw.setOutput(ios);
    		    	IIOImage outImage = new IIOImage(bufferedImage, null, null);
    		    	
    		    	// see AddImage TIFF writer, same story about compression does not work for all image types
    	    		TIFFImageWriteParam tiwp = (TIFFImageWriteParam)(tifiw.getDefaultWriteParam());
    	    		String[] cts = tiwp.getCompressionTypes();
    	    		tiwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
    	    		tiwp.setCompressionType("LZW");    		
    	    		tifiw.write(null, outImage, tiwp);
    		    	
    	    		//tifiw.write(null, outImage, null);  // no compression
    	    		
    	    		/*if (numComponents < 4)
    		    	{
    		    		TIFFImageWriteParam tiwp = (TIFFImageWriteParam)(tifiw.getDefaultWriteParam());
    		    		String[] cts = tiwp.getCompressionTypes();
    		    		tiwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
    		    		tiwp.setCompressionType("JPEG");
    		    		tifiw.write(null, outImage, tiwp);
    		    	}
    		    	else
    		    	{
    		    		tifiw.write(null, outImage, null);    		    		
    		    	}*/
    	    		
    		    	ios.close();
    		    }
    		    catch (Exception e1)
    		    {
    		    	return null;
    		    }
    		    
          	}
    	    
    	    //FileOutputStream fos = new FileOutputStream(sourceFile);
    	    //boolean result = ImageIO.write(bufferedImage, "tif", fos);
    	    //fos.close();
    	    //if (!result)
    	    	//return null;
    	    aImage.setSourceAndGallery(sourceFile.getName(), sourceFile.getParentFile().getAbsolutePath());
        	System.out.println(aImage.getHighresFilespec());
    	    highresImage = aImage.readPNGImageFile(aImage.getHighresFilespec());
        }
/*        else if (aImage.getImageType() == AveryImage.TYPE_PDF)
        {
        	try
        	{
        		File file = new File(aImage.getHighresFilespec());
  	  			PDFReader reader = new PDFReader(file);
  	  			PDF pdf = new PDF(reader);
  	  			
  	  			float dpi = 300;
  	  			
  	  			PDFParser parser = new PDFParser(pdf);
  	  			PagePainter painter = parser.getPagePainter(0);
  	  			BufferedImage bi = painter.getImage(dpi);
  	  			String png = file.getAbsolutePath().toLowerCase();
      	    png = png.replace(".pdf",  ".png");
      	    png = png.replace(".ai",  ".png");
  			    FileOutputStream fos = new FileOutputStream(png);
  	  			ImageIO.write(bi, "png", fos);
  	  			fos.close();
  	  			
  	  			sourceFile = new File(png);
      	    aImage.setSourceAndGallery(sourceFile.getName(), sourceFile.getParentFile().getAbsolutePath());
          	//System.out.println(aImage.getHighresFilespec());
      	    highresImage = aImage.readPNGImageFile(aImage.getHighresFilespec());
        	}
  		    catch (Exception e1)
  		    {
  		        return null;
  		    }
       	
        }
        else if (aImage.getImageType() == AveryImage.TYPE_SVG)
        {
            SVGImageReader svgir = new SVGImageReader(new SVGImageReaderSpi());
            ImageInputStream svgiis = ImageIO.createImageInputStream(new File(aImage.getHighresFilespec()));
		    		svgir.setInput(svgiis);
		    		
		    		BufferedImage bufferedImage = svgir.read(0); //, param);
		    		svgiis.close();
    	    
          	if (svgir != null)
          	{
        	    String png = aImage.getHighresFilespec();
        	    png = png.replace(".svg", ".png");
        	    sourceFile = new File(png);
        	    
    		    try
    		    {
    			    FileOutputStream fos = new FileOutputStream(png);
    			    boolean bSuccess = ImageIO.write(bufferedImage, "png", fos);
    			    fos.close();
    		    }
    		    catch (Exception e1)
    		    {
    		    	return null;
    		    }    		    
          }
    	    aImage.setSourceAndGallery(sourceFile.getName(), sourceFile.getParentFile().getAbsolutePath());
        	//System.out.println(aImage.getHighresFilespec());
    	    highresImage = aImage.readPNGImageFile(aImage.getHighresFilespec());
        }*/
        else if (aImage.getImageType() == AveryImage.TYPE_BMP)
        {
          highresImage = readBmpImageFile(sourceFile);
        }
        else  // this method can't handle anything else
        {
          System.err.println(AveryUtils.timeStamp() + sourceFile.getAbsolutePath() + " not JPG, PNG, TIF, WEBP, or BMP?");
          return null;
        }
      }
      catch (Exception e)
      {
        System.err.println(AveryUtils.timeStamp() + sourceFile.getAbsolutePath() + ": " + e.toString());
        return null;
      }

      // create the highres ImageFile object and add it to the list
      ImageFile highresImageFile = new ImageFile(sourceFile, highresImage.getWidth(null), highresImage.getHeight(null));
      allresList.add(0, highresImageFile);
      highresImage = null;

      // discover all existing lowres versions and add them to the list
      allresList.addAll(1, discoverLowresFiles(aImage));

      // add the new list to the hashtable
      imageFiles.put(sourceFile, allresList);
    }
/*  // debugging
System.err.println(AveryUtils.timeStamp() + ": allresList contains " + allresList.size() + " items");
System.err.println("  highresImage is " + highresImage.toString());
System.err.println("  sourceFile is " + sourceFile.toString());
System.err.println("  aImage.getImageType is " + aImage.getImageType());
System.err.println("  allresList[0] is " + ((ImageFile)(allresList.get(0))).dump());
*/
    return allresList;
  }

  /**
   * The ImageFile class is used to maintain the dimensions (in pixels)
   * of images in the system, so that the files don't have to be opened.
   */
  class ImageFile
  {
    // attributes
    private File file;
    private int width;
    private int height;

    /**
     * Constructor simply packs the data attributes of the class
     * @param file is assumed to exist
     * @param width pixel width of image
     * @param height pixel height of image
     */
    ImageFile(File file, int width, int height)
    {
      this.file = file;
      this.width = width;
      this.height = height;
    }

    // attribute access
    File getFile()  {   return file;    }
    int getWidth()  {   return width;   }
    int getHeight() {   return height;  }

    /**
     * Tests whether this image file has the pixel dimensions specified in the
     * parameter list.
     * @param width
     * @param height
     * @return true if the dimensions match
     */
    boolean sizeMatches(int width, int height)
    {
        return width == getWidth() && height == getHeight();
    }

    /**
     * get the aspect ratio of the image
     * @return width divided by height
     */
    double getAspect()
    {
      return ((double)getWidth()) / ((double)getHeight());
    }

    String dump()
    {
      return getFile().toString() + ", width=" + getWidth() + ", height=" + getHeight();
    }
  } // end of internal ImageFile class

  /**
   * This method searches the lowres branch of an AveryImage's gallery
   * for files that have names matching the basename.width.height.jpg
   * pattern.  It returns the ones it found.
   * @param aImage specifies the basename and gallery
   * @return a list containing lowres ImageFile objects (may be empty)
   */
  ArrayList discoverLowresFiles(AveryImage aImage)
  {
    class LowresFilter implements FileFilter
    {
      ArrayList foundImageFiles;
      private String basename;

      LowresFilter(String highresName)
      {
        basename = highresName.substring(0, highresName.lastIndexOf('.'));
        foundImageFiles = new ArrayList();
      }

      /**
       * Test whether the filename adheres to the magic syntax of
       * basename.width.height.jpg
       * @param file is the file to test
       * @return true only if the file is acceptable
       */
      public boolean accept(File file)
      {
        String s = file.getName();

        if (s.endsWith(".jpg"))
        {
          // toss suffix
          s = s.substring(0, s.indexOf(".jpg"));
          try
          { // extract width and height fields
            String height = s.substring(s.lastIndexOf('.') + 1);
            s = s.substring(0, s.lastIndexOf('.'));
            String width = s.substring(s.lastIndexOf('.') + 1);
            s = s.substring(0, s.lastIndexOf('.'));
            // does remainder match basename?
            if (s.equals(basename))
            { // decoding will throw an exception if number format is wrong
              int h = Integer.decode(height).intValue();
              int w = Integer.decode(width).intValue();
              if (w > 0 && h > 0)
              { // an exception wasn't thrown, and we've passed all tests
                // add this one to the list, and return true
                foundImageFiles.add(new ImageFile(file, w, h));
                return true;
              }
            }
          }
          catch (Exception e) {   return false;   }
        }
        return false;
      }
    }   // end LowresFilter class

    LowresFilter filter = new LowresFilter(aImage.getSource());
    File lowresDir = new File(lowresPath(aImage.getGallery()));
    lowresDir.listFiles(filter);

    return filter.foundImageFiles;

  } // end of discoverLowresFiles method

  /**
   * decodes the named BMP file
   * @param file - the fully qualified path to the (local) image file
   * @return an Image containing all of the pixel bits of the named BMP fil
   */
  BufferedImage readBmpImageFile(File file)
  {
    BufferedImage image = null;
    ImageProducer producer = null;
    try
    {
      producer = WinBmp.getImageProducer(file.getAbsolutePath());
    }
    catch (Exception e)
    {
      System.err.println(AveryUtils.timeStamp() + e.toString());
      return null;
    }

    Toolkit toolkit = Toolkit.getDefaultToolkit();
    image = (BufferedImage)toolkit.createImage(producer);
    toolkit.prepareImage(image, -1, -1, AveryImage.getObs());
    if (!waitForImageBits(toolkit, image))
    {
      return null;
    }

    return image;
  }

  /**
   * Polls until the image bits appear
   * @param toolkit - the toolkit fetching the image
   * @param image - the image to be filled with bits
   * @return <code>false</code> on ImageObserver.ABORT, <code>true</code> otherwise
   */
  static private boolean waitForImageBits(Toolkit toolkit, Image image)
  {
    while(true)
    {
      int status = toolkit.checkImage(image, -1, -1, null);
      if ((status & ImageObserver.ALLBITS) == ImageObserver.ALLBITS)
      {
        break;
      }
      if ((status & ImageObserver.ABORT) == ImageObserver.ABORT)
      {
        return false;
      }
    }
    return true;
  }

  /**
   * Gets a lowres JPG image file suitable for previewing, creating it
   * at the appropriate resolution if necessary.
   * @param aImage - the AveryImage to preview
   * @param directory - the directory to create the target image in
   * @param targetWidth - maximum width of resulting image
   * @param targetHeight - maximum height of resulting image
   * @return JPEG file guaranteed to exist, or <code>null</code> on failure.
   *         Also, targetWidth and targetHeight may be adjusted to reflect
   *         proper aspect ratio.
   * @throws Exception if file operations fail
   */
  public File getPreviewJpegFile(AveryImage aImage, String directory, int targetWidth, int targetHeight)
  throws Exception
  {
    // get the list of all resolutions that exist for this image
    ArrayList allresList = getAllresList(aImage);
    
    if (allresList == null)		// early exit on rightRes error
    {
    	File file = new File(aImage.getHighresFilespec());
    	return file.exists() ? file : null;
    }

    // get highres source + dimensions, always entry 0 in the list
    ImageFile highresImageFile = (ImageFile)allresList.get(0);

    if (aImage.maintainAspect())
    {
      // recalc target dimensions to match aspect of image
      double imageAspect = ((double)highresImageFile.getWidth())
                         / ((double)highresImageFile.getHeight());
      // reduce one dimension of target space to make it aspect correct
      if (imageAspect < 1.0)
      {
        targetWidth = (int)(((double)targetWidth) * imageAspect);
      }
      else if (imageAspect > 1.0)
      {
        targetHeight = (int)(((double)targetHeight) / imageAspect);
      }
    }

    // the preview filename must follow our convention of name.w.h.jpg
    File previewFile = new File(directory,
          aImage.getSource().substring(0, aImage.getSource().lastIndexOf('.') + 1)
            + targetWidth + "." + targetHeight + ".jpg");

    if (previewFile.exists())
    {
      // nothing else to do!
      return previewFile;
    }

    // we might already have a rightres image in another location
    ImageFile rightresImageFile = findRightRes(allresList, targetWidth, targetHeight);

    if (rightresImageFile != null
      && rightresImageFile.getFile().getName().toLowerCase().endsWith(".jpg"))
    {
      // copy file to target preview directory (in web space, probably)
      FileInputStream in = new FileInputStream(rightresImageFile.getFile());
      FileOutputStream out = new FileOutputStream(previewFile);
      AveryUtils.streamCopy(in, out);
      in.close();
      out.flush();
      out.close();
    }
    else // doesn't exist yet
    {
    	Image highresImage = null;
      // create new Image at target resolution
      if (aImage.getImageType() == AveryImage.TYPE_JPG)
      {
        highresImage = aImage.readJPGImageFile( highresImageFile.getFile().getAbsolutePath()) ;
      }
      else if (aImage.getImageType() == AveryImage.TYPE_PNG)
      {
        highresImage = aImage.readPNGImageFile(new FileInputStream(highresImageFile.getFile()));
      }
      else if (aImage.getImageType() == AveryImage.TYPE_TIF)
      {
        highresImage = aImage.readTIFImageFile(highresImageFile.getFile().getAbsolutePath());
      }
      //else if (aImage.getImageType() == AveryImage.TYPE_PDF)
      //{
      	//highresImage = aImage.readPDFImageFile(highresImageFile.getFile());
      //}
      else if (aImage.getImageType() == AveryImage.TYPE_WEBP)
      {
        highresImage = aImage.readWEBPImageFile(highresImageFile.getFile().getAbsolutePath());
      }
      else if (aImage.getImageType() == AveryImage.TYPE_BMP)
      {
        highresImage = readBmpImageFile(highresImageFile.getFile());
      }
      else  // this method can't handle anything else
      {
        System.err.println(AveryUtils.timeStamp()
          + highresImageFile.getFile().getAbsolutePath() + " not JPG, PNG, TIF, WEBP, or BMP!?");
        return null;
      }

      // scale the image down
      BufferedImage scaledImage = new BufferedImage(targetWidth, targetHeight,
        aImage.mayBeTransparent() ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB);

      Graphics scaledGr = scaledImage.getGraphics();
      scaledGr.drawImage(highresImage, 0, 0, targetWidth, targetHeight, AveryImage.getObs());
      try {
        if (!waitForImageBits(Toolkit.getDefaultToolkit(), scaledImage))
        {
          throw(new Exception("couldn't scale image"));
        }
      }
      finally {
      	scaledGr.dispose();   // done with this Graphics object
      	scaledGr = null;
      	highresImage = null;
      }

      if (aImage.mayBeTransparent())
      {
        // copy from scaled ARGB to new RGB, setting alpha-masked pixels to white
        BufferedImage rgbImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
        ColorModel cm = scaledImage.getColorModel();
        for (int y = 0; y < targetHeight; y++)
        {
          for (int x = 0; x < targetWidth; x++)
          {
            int rgb = scaledImage.getRGB(x, y);
            if ((cm.getAlpha(rgb)) == 0)
            {
              rgbImage.setRGB(x, y, java.awt.Color.white.getRGB());
            }
            else
            {
              rgbImage.setRGB(x, y, rgb);
            }
          }
        }
        // use the new Image
        scaledImage = rgbImage;
      }

      // save the new image as a JPEG
      FileOutputStream out = new FileOutputStream(previewFile);

      // lowres files are always jpegs
      JPEGImageEncoder jpeg = JPEGCodec.createJPEGEncoder(out);
      JPEGEncodeParam eparam;
      eparam = JPEGCodec.getDefaultJPEGEncodeParam(scaledImage);
      eparam.setQuality( 0.75f, false );
      jpeg.setJPEGEncodeParam( eparam );
      jpeg.encode(scaledImage);
      out.flush();
      out.close();

      // add this new lowres file reference to the allresList
      if (previewFile.getParentFile().getAbsolutePath().toLowerCase().endsWith("lowres"))
      {
        allresList.add(new ImageFile(previewFile, targetWidth, targetHeight));
      }
    }

    return previewFile;
  }
  
  /**
   * @param gallery - an AveryImage gallery string
   * @return a path guaranteed to end in "/LowRes"
   */
  private String lowresPath(String gallery)
  {
  	String lowres = new String(gallery);
  	int highresIndex = lowres.toLowerCase().indexOf("/highres");
  	if (highresIndex > -1)
  	{
  		return lowres.substring(0, highresIndex) + "/LowRes";
  	}
  	
  	if (lowres.endsWith("/"))
  	{
  		return lowres + "LowRes";

  	}
  	else return lowres + "/LowRes";
  }
  
  /**
   * Reduces the color space of a 24 bit image to 18 bits (262,144 colors)
   * @param source
   * @return the source, modified
   */
  public static void reduceColorSpace(BufferedImage source)
  {
    class ReductionFilter extends RGBImageFilter
    {
      ReductionFilter() { canFilterIndexColorModel = true; }
      
      public int filterRGB(int x, int y, int argb)
      {
        return (argb & 0xfcfcfcfc) | ((argb & 0x0c0c0c0c) >> 2);
      }
    };
    
    ImageProducer producer = new FilteredImageSource(source.getSource(), new ReductionFilter());
    
    Graphics gr = source.getGraphics();
    gr.drawImage(Toolkit.getDefaultToolkit().createImage(producer), 0, 0, AveryImage.getObs());
    
    if (!waitForImageBits(Toolkit.getDefaultToolkit(), source))
    {
      System.err.println("RightRes couldn't reduceColorSpace of image");
    }
  }
  
}