package com.avery.project;

import java.awt.Point;
import java.awt.geom.Point2D;

/*
 * File: TwipsPosition.java
 * Created: Nov 19, 2004 by Bob Lee
 * Copyright (c) Avery Dennison Corporation, All Rights Reserved
 */

/**
 * com.avery.miwok
 * TwipsPosition 
 */
public class TwipsPosition extends Point2D.Double
{
	/**
	 * default constructor 
	 */
	TwipsPosition()
	{
		super();
	}

	/**
	 * @param x - the x coordinate in twips
	 * @param y - the y coordinate in twips
	 */
	public TwipsPosition(double x, double y)
	{
		super(x, y);
	}

	/**
	 * Construct from a text string of comma separate x,y values
	 * @param str x,y e.g. "100.5,75"
	 */	
	TwipsPosition(String str)
	{
		int commaIndex = str.indexOf(',');
		double x = java.lang.Double.parseDouble(str.substring(0, commaIndex));
		double y = java.lang.Double.parseDouble(str.substring(commaIndex + 1));
		this.setLocation(x, y);
	}
	
	public String toString()
	{
		return new String(this.x + "," + this.y);
	}
	
	public String toMMString()
	{
		return new String(AveryProject.twipsToMM(this.x) + "," + (AveryProject.twipsToMM(this.y)));
	}
	
	Point2D scaledPoint2D(double scale)
	{
		return new Point2D.Double(x * scale, y * scale);
	}
	
	Point scaledPoint(double scale)
	{
		return new Point((int)(x * scale), (int)(y * scale));
	}
}
