/*
 * DesignUseCategories.java Created on Dec 3, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesign;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

public class DesignUseCategories
{
  /**
   * if <code>true</code>, this object has been modified since construction or last <code>write()</code>
   */
  public boolean modified = false;
  
  private Hashtable hashTable;
  private String copyright;  
  private static final Namespace namespace = Namespace.getNamespace("http://print.avery.com");

  /**
   * constructor reads a simple but specific xml format:
   * <code>
   * <productUseCategories xmlns="http://print.avery.com"> 
   *  <productType name="uniqueName">
   *   <use>categoryName</use> 1..n
   *  </productType> 1..n
   * </docRootName>
   * </code>
   * @param file
   * @throws IOException
   */
  public DesignUseCategories(File file)
  throws IOException
  {
    try
    {
      Element root = new SAXBuilder().build(file).getRootElement();
      List productTypeElements = root.getChildren("productType", namespace);
      hashTable = new Hashtable(productTypeElements.size());
      copyright = root.getAttributeValue("copyright");
      
      Iterator productTypeIterator = productTypeElements.iterator();
      while (productTypeIterator.hasNext())
      {
        Element productType = (Element)productTypeIterator.next();
        List useElements = productType.getChildren("use", namespace);
        
        ArrayList uses = new ArrayList(useElements.size());
        
        Iterator useIterator = useElements.iterator();
        while (useIterator.hasNext())
        {
          uses.add(((Element)useIterator.next()).getText());
        }
        
        hashTable.put(productType.getAttributeValue("name"), uses);        
      }
    }
    catch (JDOMException jdomx)
    {
      IOException iox = new IOException(jdomx.getMessage());
      iox.setStackTrace(jdomx.getStackTrace());
      throw iox;
    }
  }
  
  public List getUses(String productType)
  {
    return (List)hashTable.get(productType);
  }
  
  /**
   * adds a new use to the specified productType's list.
   * @param productType
   * @param use
   * @return the productType's updated List
   */
  public List addUse(String productType, String use)
  {
    List list = (List)hashTable.get(productType);
    if (list == null)
    {
      list = new ArrayList();
    }
    
    if (!list.contains(use))
    {    
      list.add(use);
      hashTable.put(productType, list);
      modified = true;
    }
    return list;
  }
  
  public String toString()
  {
    return hashTable.toString();
  }
  
  /**
   * Writes an XML file containing the current mappings
   * @param file for output
   * @throws IOException
   */
  public void write(File file)
  throws IOException
  {
    Document document = new Document();
    document.setRootElement(getProductUseCategoriesElement());

    FileOutputStream out = new FileOutputStream(file);
    new XMLOutputter(" ", true).output(document, out);
    out.flush();
    out.close();
    modified = false;
  }
  
  private Element getProductUseCategoriesElement()
  {
    Element root = new Element("productUseCategories", namespace);
    root.setAttribute("createdBy", DesignUseCategories.class.getName());
    root.setAttribute("creationDate", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
    root.setAttribute("copyright", copyright);
    Enumeration keys = hashTable.keys();
    while (keys.hasMoreElements())
    {
      String key = (String)keys.nextElement();
      Element productType = new Element("productType", namespace);
      productType.setAttribute("name", key);
      Iterator iterator = ((ArrayList)hashTable.get(key)).iterator();
      while (iterator.hasNext())
      {
        Element use = new Element("use", namespace);
        use.setText((String)iterator.next());
        productType.addContent(use);
      }
      root.addContent(productType);
    }
    
    return root;
  }
}
