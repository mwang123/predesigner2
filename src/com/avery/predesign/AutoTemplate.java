/*
 * AutoTemplate.java Created on Nov 6, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesign;

import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.jdom.Element;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;
import com.avery.project.AveryProject;
import com.avery.project.AveryTextfield;
import com.avery.project.Drawing;
import com.avery.project.TextStyle;

/**
 * @author leeb
 * Nov 6, 2007
 * AutoTemplate
 */
public class AutoTemplate extends AveryMasterpanel
{ 
  /**
   * construct from JDOM element
   * @param element - masterpanel XML
   * @throws Exception
   */
  public AutoTemplate(Element element)
  throws Exception
  {
    super(element);
    
    if (!getDescription().equals("AutoTemplate"))
    {
      specialize();
    }
  }
  
  /**
   * construct from the serializable elements of an existing masterpanel
   * @param master
   * @param productType
   * @throws Exception
   */  
  public AutoTemplate(AveryMasterpanel master)
  throws Exception
  {
    // construct via serializable element of master
    super(master.getAverysoftElement());
    
    // add the special AutoTemplate stuff
    specialize();
  }
  
  /**
   * massage this virgin masterpanel into an AutoTemplate
   */
  private void specialize()
  {
    setDescription("AutoTemplate");
    
    // remove cutouts - AutoTemplates don't need them
    clearCutouts();
    
    // clear known inappropriate hints
    removeHint("asSetsOf");
    removeHint("copyContentAndStyleTo");
    
    // add our own hints
    addHint("legalNotice",
        "Copyright " + new GregorianCalendar().get(Calendar.YEAR) + " Avery Products Corp., all rights reserved");
    
    if (!getTextDefaults().isEmpty())
    {
      // extract default pointSize and stash as a hint
      TextStyle textStyle = (TextStyle)getTextDefaults().get(0);
      addHint("defaultPointSize", textStyle.getPointSize().toString());
      
      // remove TextDefaults objects - AutoTemplates don't use them
      getTextDefaults().clear();
    }
  }
  
  public void applyTo(AveryProject project, AveryMasterpanel master)
  {
    double scaleX = master.getWidth().doubleValue() / getWidth().doubleValue();
    double scaleY = master.getHeight().doubleValue() / getHeight().doubleValue();
    double textdelta;
    try {
      textdelta = master.createDefaultTextblock().getPointsize().doubleValue()
                  - Double.parseDouble(getHint("defaultPointSize").getValue());
    }
    catch (Exception ex) { 
      textdelta = 0;
    }
    
    java.util.Iterator iterator = getFieldIterator();
    while (iterator.hasNext())
    {
      AveryPanelField field = (AveryPanelField)iterator.next();
      
      Point oldPosition = field.getPosition();
      Point newPosition = new Point();
      newPosition.setLocation(oldPosition.x * scaleX, oldPosition.y * scaleY);

      // [ Point(width, 0), Point(0, height) ]
      double[] points = { field.getWidth().doubleValue(), 0, 0, field.getHeight().doubleValue() };      
      
      double theta = field.getRotation().doubleValue() * Math.PI / 180.0;
      AffineTransform.getRotateInstance(-theta, 0, 0).transform(points, 0, points, 0, 2);
      AffineTransform.getScaleInstance(scaleX, scaleY).transform(points, 0, points, 0, 2);

      // calculate new width and height
      double newWidth = Point.distance(0, 0, points[0], points[1]);
      double newHeight = Point.distance(0, 0, points[2], points[3]);
      
      if (field instanceof Drawing)
      {
      	Drawing df = (Drawing)field;
      	if (df.isMaintainAspect())
	      {
	        double min = Math.min(newWidth, newHeight);
	        newWidth = min;
	        newHeight = min;
	      }
      }
      
      field.setPosition(newPosition);
      field.setWidth(new Double(newWidth));
      field.setHeight(new Double(newHeight));
      
      if (field instanceof AveryTextfield)
      {
        if (textdelta != 0)
        {
          int pointsize = (int)(((AveryTextfield)field).getPointsize().doubleValue() + textdelta);
          ((AveryTextfield)field).setPointsize(new Double(pointsize));
        }
        
        double lineHeight = ((AveryTextfield)field).getPointsize().doubleValue() * 20.0;
        if (newHeight < lineHeight)
        {
          field.setHeight(new Double(lineHeight));
        }
      }

      project.addFieldToMasterpanel(field, master);
    }
    
    master.setMergeMapName(getMergeMapName());
    
    // add design theme if appropriate
    if (getHint("designTheme") != null)
    {
      project.setDesignTheme(getHint("designTheme").getValue());
    }
  }
}