/*
 * StyleSet.java Created on Nov 6, 2007 by leeb
 * Copyright 2007-2008 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesign;

import java.awt.Color;

public class StyleSet implements java.io.Serializable
{
  static final long serialVersionUID = 2008643838522255553L;
  public String name;
  public String font;
  public String placeholderFont;
  public String description;
  public Color color1;
  public Color color2;
  public Color color3;
  public String graphic01;
  public String graphic02;
  public String graphic03;
  public String graphic04;
  public String graphic05;
  public String graphic06;
  public String graphic07;
  public String graphic08;
  public String graphic09;
  public String graphic10;
  public String graphic11;
  public String graphic12;
  
  private StyleSet() { }  // create through newStyleSet or serialization
  
  public static StyleSet newStyleSet()
  {
    StyleSet ss = new StyleSet();
    
    ss.name = "placeholder";
    ss.font = "Arial";
    ss.placeholderFont = "Arial";
    ss.description = "original placeholder style";
    ss.color1 = Color.BLACK;
    ss.color2 = Color.GRAY;
    ss.color3 = Color.WHITE;
    ss.graphic01 = "placeholder.png";
    ss.graphic02 = "placeholderBack.png";
    ss.graphic03 = "placeholderBW.png";
    ss.graphic04 = "placeholderCardTall.png";
    ss.graphic05 = "placeholderCardWide.png";
    ss.graphic06 = "placeholderCD.png";
    ss.graphic07 = "placeholderCDInsert.png";
    ss.graphic08 = "placeholderCDLeft.png";
    ss.graphic09 = "placeholderCDRight.png";
    ss.graphic10 = "placeholderLogo.png";
    ss.graphic11 = "placeholderTall.png";
    ss.graphic12 = "placeholderWide.png";
    
    return ss;
  }
  
  /**
   * only alphanumeric chars are allowed in filenames (no chars, punctuation, etc.)
   * @param ssName - typically from the designTheme of an AveryProject
   * @return ssName minus non-alphanumeric chars plus ".ss" extension
   */
  public static String constructFilename(String ssName)
  {
    // only alphanumeric chars allowed in filename
    return (ssName == null) ? ".ss" : (ssName.replaceAll("[\\W]", "") + ".ss");
  }
  
  /**
   * only alphanumeric chars are allowed in filenames (no chars, punctuation, etc.)
   * @return the StyleSet's name minus non-alphanumeric chars plus ".ss" extension
   */
  public String constructFilename()
  {
    return constructFilename(name);
  }
}
