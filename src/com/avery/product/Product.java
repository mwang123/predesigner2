/*
 * Product.java Created on Jan 18, 2006 by leeb
 * Copyright 2006 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.product;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.jdom.Element;

/**
 * @author Bob Lee
 *
 * an Avery printable media product as defined by Averysoft
 */
public class Product
{
	private String skuName;
	private String batName;
	private String productGroupName;
	private String creator;
	private Integer number;
	private String sizeDescription;
	private Boolean isManualFeed;
	private Boolean isMergable;
  private String productType;
  private String printHelp;
	private Hashtable descriptions = new Hashtable();
	private Hashtable appearances = new Hashtable();
	private Hashtable hints = new Hashtable();
	private ArrayList categories = new ArrayList();
	private ArrayList categoryRefs = new ArrayList();
	private ArrayList subProducts;

	Product(Element element)
	{
		skuName = element.getAttributeValue("SKUname");
		batName = element.getAttributeValue("BATname");
		productGroupName = element.getAttributeValue("productGroupName");
		creator = element.getAttributeValue("creator");
		number = Integer.decode(element.getAttributeValue("number"));
		sizeDescription = element.getAttributeValue("sizeDescription");
		isManualFeed = Boolean.valueOf(element.getAttributeValue("isManualFeed"));
		isMergable = Boolean.valueOf(element.getAttributeValue("isMergable"));
    productType = element.getAttributeValue("productType");
    printHelp = element.getAttributeValue("printHelp");
		
    Iterator iterator = element.getChildren().iterator();
    while (iterator.hasNext())
    {
      Element child = (Element)iterator.next();
      if (child.getName().equals("productDescription"))
      {
      	Description d = new Description(child);
      	descriptions.put(d.language, d);
      }
      else if (child.getName().equals("appearance"))
      {
      	Appearance a = new Appearance(child);
      	appearances.put(new Integer(a.getPageNumber()), a);
      }
      else if (child.getName().equals("hint"))
      {
      	hints.put(child.getAttributeValue("name"), child.getAttributeValue("value"));
      }
      else if (child.getName().equals("category"))
      {
      	categories.add(new Category(child));
      }
      else if (child.getName().equals("marketingCategoryRef"))
      {
        categoryRefs.add(child.getText());
      }
      else if (child.getName().equals("subProductComponent"))
      {
        if (subProducts == null)
        {
          subProducts = new ArrayList();
        }
        subProducts.add(new Product(child));
      }
    }
	}
	
	Element getAveryElement()
	{
    Element element = new Element("productComponent");
    
    element.setAttribute("SKUname", skuName);
    element.setAttribute("BATname", batName);
    element.setAttribute("productGroupName", productGroupName);
    element.setAttribute("creator", creator);
    element.setAttribute("number", number.toString());
    if (sizeDescription != null)
    	element.setAttribute("sizeDescription", sizeDescription);
    if (isManualFeed.booleanValue())
    {
      element.setAttribute("isManualFeed", "true");
    }
    if (!isMergable.booleanValue())
    {
      element.setAttribute("isMergable", "false");
    }
    element.setAttribute("productType", productType);
    if (printHelp != null)
    {
      element.setAttribute("printHelp", printHelp);   	
    }
    
    Enumeration enumerator = descriptions.keys();
    while (enumerator.hasMoreElements())
    {
    	String key = (String)enumerator.nextElement();
    	Description d = (Description)descriptions.get(key);
    	element.addContent(d.getAveryElement());
    }
    
    Iterator iterator = categoryRefs.iterator();
    while (iterator.hasNext())
    {
    	String categoryRef = (String)iterator.next();
    	Element marketingCategoryRefElement = new Element("marketingCategoryRef");
    	marketingCategoryRefElement.setText(categoryRef);
    	element.addContent(marketingCategoryRefElement);
   }
		
    return element;
	}
	
	/**
	 * uses marketingCategoryIds to build the Product's category list
	 * @param marketingCategories contains the document-wide category list
	 */
	void buildCategoryList(Hashtable marketingCategories)
	{
	  Iterator it = categoryRefs.iterator();
	  while (it.hasNext())
	  {
	    Object cat = marketingCategories.get(it.next());
	    if (cat != null && !categories.contains(cat))
	    {
	      categories.add(cat);
	    }
	  }
	  
	  // subProductComponents have categories too
	  if (subProducts != null)
	  {
	    it = subProducts.iterator();
	    while (it.hasNext())
	    {
	      ((Product)it.next()).buildCategoryList(marketingCategories);
	    }
	  }
	}
	
	/**
	 * Java representation of the Averysoft XML 
	 * productList/productComponent/productDescription element  
	 */
	private class Description
	{
		String language;
		String longDescription;
		String shortDescription;
		String colorDescription;
		String useDescription;
		
		Description(Element element)
		{
			language = element.getAttributeValue("language");
			longDescription = element.getAttributeValue("longDescription");
			shortDescription = element.getAttributeValue("shortDescription");
			colorDescription = element.getAttributeValue("colorDescription");
			useDescription = element.getAttributeValue("colorDescription");
		}
		
		Element getAveryElement()
		{
			Element descriptionElement = new Element("productDescription");
			descriptionElement.setAttribute("language", language);
			descriptionElement.setAttribute("longDescription", longDescription);
			descriptionElement.setAttribute("shortDescription", shortDescription);
			descriptionElement.setAttribute("colorDescription", colorDescription);
			descriptionElement.setAttribute("useDescription", useDescription);
			
			return descriptionElement;
		}
		
	}
	
	/**
	 * Java representation of the Averysoft XML 
	 * productList/productComponent/category element  
	 */
	public static class Category
	{
		String language;
		int level;
		String sort;
		String content;
		
		Category()  // used by derived class ProductList.MarketingCategory
		{
		  sort = "1";
		  level = 1;
		}
		
		Category(Element element)
		{
      language = element.getAttributeValue("language");
      try
      {
        level = Integer.decode(element.getAttributeValue("level")).intValue();
      }
      catch (Exception ex) 
      {
        level = 1; 
      }
      
      content = element.getText();
		}
    
    public String toString()
    {
      return content;
    }
	}

	/**
	 * @return Returns the batName.
	 */
	public String getBatName()
	{
		return batName;
	}
	/**
	 * @return Returns the creator.
	 */
	public String getCreator()
	{
		return creator;
	}
	/**
	 * @return Returns the isManualFeed.
	 */
	public boolean isManualFeed()
	{
		return isManualFeed.booleanValue();
	}
	/**
	 * @return Returns the isMergable.
	 */
	public boolean isMergable()
	{
		return isMergable.booleanValue();
	}
	/**
	 * @return Returns the number.
	 */
	public Integer getNumber()
	{
		return number;
	}
	/**
	 * @return Returns the productGroupName.
	 */
	public String getProductGroupName()
	{
		return productGroupName;
	}
	/**
	 * @return Returns the sizeDescription.
	 */
	public String getSizeDescription()
	{
		return sizeDescription;
	}

	public void setSizeDescription(String sizeDescription)
	{
		this.sizeDescription = sizeDescription;
	}

	/**
	 * @return Returns the skuName.
	 */
	public String getSkuName()
	{
		return skuName;
	}
  
  public String getLongDescription(String language)
  {
    return ((Description)descriptions.get(language)).longDescription;
  }
  
  public void setLongDescription(String language, String longDescription)
  {
  	Description description = (Description)descriptions.get(language);
  	description.longDescription = longDescription;
  }
  
  public String getShortDescription(String language)
  {
    return ((Description)descriptions.get(language)).shortDescription;
  }
  
  public void setShortDescription(String language, String shortDescription)
  {
  	Description description = (Description)descriptions.get(language);
  	description.shortDescription = shortDescription;
  }
  
  public String getColorDescription(String language)
  {
    return ((Description)descriptions.get(language)).colorDescription;
  }
  
  public void setColorDescription(String language, String colorDescription)
  {
  	Description description = (Description)descriptions.get(language);
  	description.colorDescription = colorDescription;
  }
  
  public String getUseDescription(String language)
  {
    return ((Description)descriptions.get(language)).useDescription;    
  }
	
  public void setUseDescription(String language, String useDescription)
  {
  	Description description = (Description)descriptions.get(language);
  	description.useDescription = useDescription;
  }
  
	/**
	 * retrieve the Appearance object associated with the specified pageNumber
	 * @param pageNumber - the page of interest
	 * @return the matching Appearance, or <code>null</code>
	 */
	public Appearance getAppearance(int pageNumber)
	{
		return (Appearance)appearances.get(new Integer(pageNumber));
	}
  
  /**
   * @param language - an xs:language type compliant string
   * @return a List of Product.Category objects associated with this Product, 
   * in the language specified (may be an empty List!)
   */
  public List getCategories(String language)
  {
    List list = new ArrayList();
    Iterator i = categories.iterator();
    while (i.hasNext())
    {
      Category category = (Category)i.next();
      if (category.language.equals(language))
      {
        list.add(category);
      }
    }
    return list;
  }
  
  /**
   * @return Returns the printHelp token.
   */
  public String getPrintHelp()
  {
    return printHelp;
  }
  
  /**
   * @return Returns the productType string.
   */
  public String getProductType()
  {
    return productType;
  }
  
  ArrayList productgroups = new ArrayList();
  
  /**
   * SubProductComponents may have different productGroupNames.
   * @return List containing all of the productGroupName Strings from this
   * product and all of its subProducts.  List is guaranteed to contain
   * at least one item.
   */
  public List getAllProductGroupNames()
  {
    if (productgroups.size() == 0)
    {
      productgroups.add(productGroupName);
      if (subProducts != null)
      {
        Iterator it = subProducts.iterator();
        while (it.hasNext())
        {
          String name = ((Product)it.next()).productGroupName;
          if (!productgroups.contains(name))
          {
            productgroups.add(name);
          }
        }
      }
    }
    return productgroups;
  }
  
  /**
   * @return a list containing this Product plus all of its subProducts
   */
  public List getAllProducts()
  {
    ArrayList list = new ArrayList();
    list.add(this);
    if (subProducts != null)
    {
      list.addAll(subProducts);
    }
    return list;
  }
  
  //public static String removeEscapes(String str)
 // {
  	//String unescape = str.replaceAll("&amp;", "&");
  	//return unescape;
  //}
}
