/*
 * ProductList.java Created on Jan 18, 2006 by leeb
 * Copyright 2006 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.product;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
//import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
//import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.avery.Averysoft;

/**
 * @author Bob Lee
 * This Java object holds the contents of an Averysoft productList XML file
 */
public class ProductList extends ArrayList
{
  private static final long serialVersionUID = -5340456677542394722L;
	String copyright;
	Integer revision;
	String targetName;
	String mainProductRange;
	private Hashtable marketingCategories = new Hashtable();
	
	private static ProductList productList = null;
	
	private Element filter = null;
	
	public ProductList()
	{
		super();
		setCopyright("Copyright 2006 Avery Dennison Corp., All Rights reserved.");
	}
	
	/**
	 * Construct from a file
	 * @param file - an Averysoft productList XML file 
	 * @throws JDOMException - if JDOM can't parse the file
	 * @throws IOException - if the file can't be accessed
	 */
	public ProductList(File file)
	throws JDOMException, IOException
	{
		super();
		SAXBuilder builder = new SAXBuilder(true);
		builder.setFeature("http://apache.org/xml/features/validation/schema", true);
		builder.setProperty(
				"http://apache.org/xml/properties/schema/external-schemaLocation",
				Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 

		Element element = builder.build(file).getRootElement();

		if (element.getName().equals("productList"))
		{
			setCopyright(element.getAttributeValue("copyright"));
			setRevision(Integer.decode(element.getAttributeValue("revision")));
			setTargetName(element.getAttributeValue("targetName"));
			setMainProductRange(element.getAttributeValue("mainProductRange"));
			
	    Iterator iterator = element.getChildren().iterator();
	    while (iterator.hasNext())
	    {
	      Element child = (Element)iterator.next();
	      if (child.getName().equals("productComponent"))
	      {
	      	this.add(new Product(child));
	      }
	      else if (child.getName().equals("marketingCategory"))
	      {
	        MarketingCategory cat = new MarketingCategory(child);
	        marketingCategories.put(cat.id, cat);
	      }
	      else if (child.getName().equals("filter"))
	      {
	      	// not parsing it at thist time - keep it to rewrite
	        filter = child;
	      }
	    }
	    
	    if (marketingCategories.size() > 0)
	    {
	      Iterator it = this.iterator();
	      while (it.hasNext())
	      {
	        ((Product)it.next()).buildCategoryList(marketingCategories);
	      }
	    }
		}
		productList = this;
	}
	
	Element getAveryElement()
	{
		// JDOM causes all child elements that do not set a namespace to write -> xmlns="" if set namespace only on parent
		Element element = new Element("productList", Averysoft.getAverysoftURI()); 
		
		element.setAttribute("copyright", copyright);
		if (revision.intValue() > 0)
			element.setAttribute("revision", revision.toString());
		element.setAttribute("targetName", targetName);
		element.setAttribute("mainProductRange", mainProductRange);
		
		if (filter != null)
		{
			filter.detach();
			element.addContent(filter);
		}
   
    Enumeration enumerator = marketingCategories.keys();
    while (enumerator.hasMoreElements())
    {
    	String key = (String)enumerator.nextElement();
    	MarketingCategory cat = (MarketingCategory)marketingCategories.get(key);
    	element.addContent(cat.getAveryElement());
    }
    
    Iterator iterator = this.iterator();
    while (iterator.hasNext())
    {
    	Product product = (Product)iterator.next();
    	element.addContent(product.getAveryElement());
   }
    
		return element;
	}
	
  void write(File file)
  throws Exception
  {
    org.jdom.Element productListElement = getAveryElement();
    
    Document document = new Document();
		document.setRootElement(productListElement);

		// remove blank xmlns on all elements requires converting output first to string then to file
		StringWriter stringWriter = new StringWriter();
    new XMLOutputter(" ", true).output(document, stringWriter); 
    String text = stringWriter.toString();
    text = text.replace(" xmlns=\"\"", "");
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF8"));
    out.write(text);

    //FileOutputStream out = new FileOutputStream(file);  
    //new XMLOutputter(" ", true).output(document, out);
    out.flush();
    out.close();
  }
 
  public class MarketingCategory extends Product.Category
  {
    String id;
    String marketingCategory;
    String language;
    
    MarketingCategory(Element element)
    {
      super(element);
      marketingCategory = element.getText();
      id = element.getAttributeValue("id");
      language = element.getAttributeValue("language");
    }
    
    Element getAveryElement()
    {
			Element marketingCategoryElement = new Element("marketingCategory");
			marketingCategoryElement.setNamespace(null);
			marketingCategoryElement.setText(marketingCategory);
			marketingCategoryElement.setAttribute("id", id);
			marketingCategoryElement.setAttribute("language", language);
			
			return marketingCategoryElement;	
    }
    
    public String getMarketingCategory()
    {
    	return marketingCategory;
    }
    
    public String getid()
    {
    	return id;
    }
  }
	
  public Appearance getAppearanceBySku(String sku, int pageNumber)
  {
    Iterator products = productList.iterator();
    Appearance appearance = null;
    
    while (products.hasNext())
    {
      Product product = (Product)products.next();
      // filter products that match current project
      if (product.getSkuName().equals(sku))
      {
        appearance = product.getAppearance(pageNumber);
        break;
      }
    }
    return appearance;
	}
  
  public static ProductList getProductList()
  {
  	return productList;
  }
  
	public String getCopyright()
	{
		return copyright;
	}
	
	private void setCopyright(String copyright)
	{
		this.copyright = copyright;
	}
	
	public String getMainProductRange()
	{
		return mainProductRange;
	}
	
	private void setMainProductRange(String mainProductRange)
	{
		this.mainProductRange = mainProductRange;
	}
	
	public Integer getRevision()
	{
		return revision;
	}
	
	private void setRevision(Integer revision)
	{
		this.revision = revision;
	}
	
	public String getTargetName()
	{
		return targetName;
	}
	
	private void setTargetName(String targetName)
	{
		this.targetName = targetName;
	}
	
	public Hashtable getMarketingCategories()
	{
		return marketingCategories;
	}
	
}
