/*
 * ProductGroupList.java Created on Mar 14, 2005 by leeb
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.product;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Element;

/**
 * representation of the Averysoft.xsd productGroupList document type
 */
public class ProductGroupList extends ArrayList
{
	String copyright;
	Integer revision;
	String mainProductRange;
	
	/**
	 * default constructor
	 */
	public ProductGroupList()
	{
		super();
	}
	
	/**
	 * Construct from a JDom element
	 */
	public ProductGroupList(Element element)
	{
		super();
		if (element.getName().equals("productGroupList"))
		{
			setCopyright(element.getAttributeValue("copyright"));
			setRevision(Integer.decode(element.getAttributeValue("revision")));
			setMainProductRange(element.getAttributeValue("mainProductRange"));
			
	    Iterator iterator = element.getChildren().iterator();
	    while (iterator.hasNext())
	    {
	      Element child = (Element)iterator.next();
	      if (child.getName().equals("productGroup"))
	      {
          ProductGroup pg = new ProductGroup(child);
          this.add(pg);
	      }
	    }
		}
	}
	
	public String getCopyright()
	{
		return copyright;
	}
	public String getMainProductRange()
	{
		return mainProductRange;
	}
	public Integer getRevision()
	{
		return revision;
	}
	
	private void setCopyright(String copyright)
	{
		this.copyright = copyright;
	}
	private void setMainProductRange(String mainProductRange)
	{
		this.mainProductRange = mainProductRange;
	}
	private void setRevision(Integer revision)
	{
		this.revision = revision;
	}
  
  /**
   * @param productGroupName
   * @return a productGroup with a matching name, or null
   */
  public ProductGroup getProductGroup(String productGroupName)
  {
    Iterator groups = iterator();
    while (groups.hasNext())
    {
      ProductGroup productGroup = (ProductGroup)groups.next();
      if (productGroup.getName().equals(productGroupName))
      {
        return productGroup;
      }
    }
    return null;
  }

  /**
   * @param productGroupName
   * @return the productType string, or null
   */
  public String getProductType(String productGroupName)
  {
    ProductGroup pg = getProductGroup(productGroupName);
    return (pg == null) ? null : pg.getProductType();
  }
  
  /**
   * A productTypePrefix is the opening substring of a productType,
   * up to the first '_' character
   * @return a list if the productTypePrefixes used in this ProductGroupList
   */
  public List getProductTypePrefixes()
  {
  	List list = new ArrayList();
    Iterator groups = iterator();
    while (groups.hasNext())
    {
      String productType = ((ProductGroup)groups.next()).getProductType();
      String prefix = productType.substring(0, productType.indexOf('_'));
      if (!list.contains(prefix))
      {
      	list.add(prefix);
      }
    }
  	
  	return list;
  }
}
