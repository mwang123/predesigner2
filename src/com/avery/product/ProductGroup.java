/*
 * ProductGroup.java Created on Mar 14, 2005 by Bob Lee
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.product;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;

import org.jdom.Element;

/**
 * Encapsulates a productGroup element as expressed in Averysoft XML
 */
public class ProductGroup
{
	private String name;
	private String description;
	private String batName;
	private String baseSku;
	private Color defaultPaperColor = Color.white;
	private String productType;
	private String templateFamily;
	private boolean isComplex = false;
	private ArrayList appearances = new ArrayList();
	private String model3d;
	private String customProof;
	
	/**
	 * constructs a ProductGroup object from an Averysoft.xsd 
	 * @param element JDom Element named productGroup.
	 */
	ProductGroup(Element element)
	{
		name = element.getAttributeValue("name");
		description = element.getAttributeValue("description");
		batName = element.getAttributeValue("BATname");
		baseSku = element.getAttributeValue("baseDesignSKU");
		try
		{
			defaultPaperColor = Color.decode(element.getAttributeValue("defaultPaperColor"));
		}
		catch (Exception e)
		{
			defaultPaperColor = Color.white;
		}
		productType = element.getAttributeValue("productType");
		if (element.getAttributeValue("templateFamily") != null)
		{
			templateFamily = element.getAttributeValue("templateFamily");
		}
		isComplex = "true".equals(element.getAttributeValue("isComplex"));
		model3d = element.getAttributeValue("model3d");
		customProof = element.getAttributeValue("customProof");
		
    Iterator iterator = element.getChildren().iterator();
    while (iterator.hasNext())
    {
      Element child = (Element)iterator.next();
      if (child.getName().equals("appearance"))
      {
      	this.appearances.add(new Appearance(child));
      }
    }
	}
	
	public ArrayList getAppearances()
	{
		return appearances;
	}
	
	/**
	 * This method will synthesize a new Appearance object from the defaultPaperColor
	 * if none exists for the supplied pageNumber
	 * @param pageNumber is 1-based
	 * @return
	 */
	public Appearance getAppearance(int pageNumber)
	{
		Iterator iterator = appearances.iterator();
		while (iterator.hasNext())
		{
			Appearance appearance = (Appearance)iterator.next();
			if (appearance.getPageNumber() == pageNumber)
			{
				return appearance;
			}
		}
		
		return new Appearance(pageNumber, getDefaultPaperColor());
	}
	
	public String getBaseSku()
	{
		return baseSku;
	}
	
	public String getBatName()
	{
		return batName;
	}
	
	public Color getDefaultPaperColor()
	{
		return defaultPaperColor;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String toString()
	{
		String str = getName();
		str += " \"" + getDescription() + "\" ";
		str += getBatName() + " " + getBaseSku();		
		return str;
	}
  
  public boolean isComplex()
  {
    return isComplex;
  }
  
  /**
   * Warning: can return <code>null</code>
   * @return the productType string, or <code>null</code> if it doesn't exist 
   */
  public String getProductType()
  {
    return productType;
  }
 
	public String getTemplateFamily()
	{
		return templateFamily;
	}

  /**
   * Warning: can return <code>null</code>
   * @return the name of an associated 3D model file, or <code>null</code>
   */
  public String getModel3d()
  {
    return model3d;
  }
  
  /**
   * Warning: can return <code>null</code>
   * @return the custom proof string, or <code>null</code>
   */
  public String getCustomProof()
  {
    return customProof;
  }
  
}
