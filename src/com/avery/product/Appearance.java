/*
 * Apearance.java - Created on Mar 14, 2005
 * Copyright 2005 Avery Dennison Corp., All Rights reserved
 */
package com.avery.product;

import java.awt.Color;
import org.jdom.Element;

/**
 * encapsulates the Averysoft.xsd appearance element
 */
public class Appearance
{
	private int pageNumber;
	private Color color;
	private String image;

	/**
	 * construct from an Averysoft.xsd appearance element
	 * 
	 * @param element the element to read
	 */
	public Appearance(Element element)
	{
		try
		{
			setPageNumber(Integer.decode(element.getAttributeValue("pageNumber")).intValue());
		}
		catch (Exception e)
		{
			setPageNumber(1);
		}

		try
		{
			setColor(Color.decode(element.getAttributeValue("color")));
		}
		catch (Exception e)
		{
			setColor(Color.white);
		}

		setImage(element.getAttributeValue("image"));
	}
	
	/**
	 * constructor used within the package to synthesize Appearance objects
	 * where none exist in the ProductGroup
	 * @param pageNumber
	 * @param paperColor
	 */
	public Appearance(int pageNumber, Color paperColor)
	{
		this.pageNumber = pageNumber;
		this.color = paperColor;
	}

	public Color getColor()
	{
		return color;
	}

	public String getImage()
	{
		return image;
	}

	public int getPageNumber()
	{
		return pageNumber;
	}

	private void setColor(Color color)
	{
		this.color = color;
	}

	private void setImage(String image)
	{
		this.image = image;
	}

	private void setPageNumber(int pageNumber)
	{
		this.pageNumber = pageNumber;
	}
}