package com.avery.product;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import java.util.Iterator;
import java.util.List;

import com.avery.office.XlWorkbook;
import com.avery.office.XlWorksheet;
import com.avery.product.Product;
import com.avery.product.ProductList;

public class ProductListEditTool
{
	public static int SHORT_DESCRIPTION = 0;
	public static int LONG_DESCRIPTION = 1;
	public static int COLOR_DESCRIPTION = 2;
	public static int USE_DESCRIPTION = 3;
	public static int SIZE_DESCRIPTION = 4;

	public static ProductListEditTool plst;
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		plst = new ProductListEditTool();
		
		if (args.length == 1)
		{
			plst.createSpreadsheetCSV(args);
			return;
		}
		
		if (args.length < 5)
		{
			System.out.println("Requires 5 command line parameters: [Excel xlsx filename][Product List xml filename][Worksheet Name][key column name][value column name]{language code}");
			return;
		}
		
		if (args[4].toLowerCase().equals("short description"))
			plst.parseDescription(args, SHORT_DESCRIPTION);		
		else if (args[4].toLowerCase().equals("long description"))
			plst.parseDescription(args, LONG_DESCRIPTION);		
		else if (args[4].toLowerCase().equals("color description"))
			plst.parseDescription(args, COLOR_DESCRIPTION);		
		else if (args[4].toLowerCase().equals("use description"))
			plst.parseDescription(args, USE_DESCRIPTION);		
		else if (args[4].toLowerCase().equals("size description"))
			plst.parseDescription(args, SIZE_DESCRIPTION);		
	}

	private void parseDescription(String[] args, int descriptionType)
	{
		try
		{
			XlWorkbook workBook = new XlWorkbook(new File(args[0])); //"MicrosoftAgave_ShortProductDesc.xlsx"));	
			XlWorksheet workSheet = workBook.getWorksheet(args[2]); //"Products");
			List keyColumn = workSheet.getColumn(args[3]); //"Prod No");
			List valueColumn = workSheet.getColumn(args[4]); //"Short Description");
			// if present specifies language for each product
			List languageColumn = workSheet.getColumn("Language");
			
			// open the product list
			ProductList productList = new ProductList(new File(args[1])); //new File("US_en_Avery_Microsoft-Agave-US_2012-04-09.xml"));
			
			if (keyColumn.size() > 1)
			{				
				int i = 1;
				// language defaults to en, default can be overriden by optional last command line parameter
				String lang = "en";
				if (args.length > 5)
					lang = args[5];
				
				while (i < keyColumn.size())
				{
					// the key column is a number
					String key = (String)keyColumn.get(i);
					if (key != null && key.length() > 0)
					{
						//int keyInt = Integer.parseInt(key);
						String value = (String)valueColumn.get(i);
						String language = (String)languageColumn.get(i);
						if (language.length() == 2)
							lang = language;
					
						Iterator prodIterator = productList.iterator();
						while (prodIterator.hasNext())
						{
							Product product = (Product)prodIterator.next();
							if (product.getNumber().toString().equals(key))
							{
								if (descriptionType == SHORT_DESCRIPTION)
									product.setShortDescription(lang, value);
								else if (descriptionType == LONG_DESCRIPTION)
									product.setLongDescription(lang, value);
								else if (descriptionType == COLOR_DESCRIPTION)
									product.setColorDescription(lang, value);
								else if (descriptionType == USE_DESCRIPTION)
									product.setUseDescription(lang, value);
								else if (descriptionType == SIZE_DESCRIPTION)
									product.setSizeDescription(value);
								System.out.println(key + "=" + value);
								break;
							}
						}
					}
					i++;
				}
			}
			productList.write(new File("New_" + args[1]));
		}
		catch (Exception e)
		{
			e.printStackTrace();			
		}
	}
	
	private void createSpreadsheetCSV(String[] args)
	{
		try
		{
			String lang = "en";
			// open the product list
			ProductList productList = new ProductList(new File(args[0])); //new File("US_en_Avery_Microsoft-Agave-US_2012-04-09.xml"));
			
			// open the output file
			BufferedWriter out = new BufferedWriter(new FileWriter(new File(args[0] + ".txt")));
			String header = "Prod No\tSKUName\tLong Description\tShort Description\tColor Description\tUse Description\tSize Description\tLanguage\r\n";
			out.write(header);
			
			Iterator prodIterator = productList.iterator();
			while (prodIterator.hasNext())
			{
				Product product = (Product)prodIterator.next();
				out.write(validateString(product.getNumber().toString()) + "\t");
				out.write(validateString(product.getSkuName()) + "\t");
				out.write(validateString(product.getLongDescription(lang)) + "\t");
				out.write(validateString(product.getShortDescription(lang)) + "\t");
				out.write(validateString(product.getColorDescription(lang)) + "\t");
				out.write(validateString(product.getUseDescription(lang)) + "\t");
				out.write(validateString(product.getSizeDescription()) + "\t");
				out.write(lang);
				
				out.write("\r\n");
			}
			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();			
		}
	}
	
	String validateString(String in)
	{
		if (in == null || in.length() < 1)
			return "";
			return in;
	}
}
