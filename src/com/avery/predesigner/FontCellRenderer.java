package com.avery.predesigner;

import java.awt.Component;
import java.awt.Font;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class FontCellRenderer extends JLabel implements ListCellRenderer
{
	protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
	//private final ListCellRenderer delegate;

	//public FontCellRenderer(ListCellRenderer delegate)
	//{
    //this.delegate = delegate;
	//}
	 
	@Override
  public Component getListCellRendererComponent(JList list, Object value,
      int index, boolean isSelected, boolean cellHasFocus)
	{
  	JLabel label = (JLabel)defaultRenderer.getListCellRendererComponent(list, value, index,
  			isSelected,cellHasFocus);
  	Font font = new Font((String)value, Font.PLAIN, 20);
  	//System.out.println(value);
  	label.setFont(font);
  	label.setText((String)value);
  	return label;
  }
}
