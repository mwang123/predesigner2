package com.avery.predesigner;

import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: panel containing gadgets for the Select Direction of Rotation dialog</p>
 * <p>Copyright: Copyright 2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author 
 * @version 1.0
 */

public class SelectDirectionPanel extends JPanel 
{
	private static final long serialVersionUID = 5716051525638915488L;
	private ButtonGroup direction = new ButtonGroup();
	private JRadioButton cw = new JRadioButton("Clockwise");
	private JRadioButton ccw = new JRadioButton("Counter-Clockwise");
	  
	SelectDirectionPanel(Project project)
	{
	  setLayout(new GridLayout(0,1));
	  direction.add(cw);
	  direction.add(ccw);
	  add(new BiPane(cw, ccw));
	  
	  // by default
	  cw.setSelected(true);
	  
	  // 
	  if (RotationManager.getViewOrientationFor(project).indexOf("ccw") > -1 ) {
		  ccw.setSelected(true);
	  }
	}
	  
	public boolean isClockwise()
	{
	  return cw.isSelected();
	}
  
	private class BiPane extends JPanel
	{
	  private static final long serialVersionUID = -1099138316436419987L;
	
	  	BiPane(JComponent one, JComponent two)
		{
	      setLayout(new GridLayout(1,2,5,5));
	      add(one);
		  add(two);
		}
	}
}
