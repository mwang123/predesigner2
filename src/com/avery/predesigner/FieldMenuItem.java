package com.avery.predesigner;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.Font;

import com.avery.predesigner.editors.FieldEditorPanel;
import com.avery.project.*;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: a MenuItem for editing an AveryPanelField</p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 1.7
 */

class FieldMenuItem extends JMenuItem
{
	private static final long serialVersionUID = -4377207118248806846L;
	private AveryPanelField field;
  private Frame1 frame;

  FieldMenuItem(AveryPanelField field, Frame1 frame)
  {
    this.field = field;
    this.frame = frame;

    if (field instanceof AveryTextfield)
    {
      // consider setting font of menu item to field font
      setMenuItemFont();
    }

    this.setText(field.getPrompt());
    this.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent event)
      {
        onClick(event);
      }
    });
  }

  private Frame1 getFrame()
  {
    return this.frame;
  }

  private void onClick(ActionEvent event)
  {
    FieldEditorPanel editor = FieldEditorPanel.getEditorPanel(field, false);

    // loop until they get it right or cancel
    while (JOptionPane.showConfirmDialog(frame, editor, field.getClass().getName(),
      JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
      == JOptionPane.OK_OPTION)
    {
      String errorMessage = editor.updateField();
      if (field instanceof AveryTextblock)
      {
      	getFrame().getMasterpanelToolbar().getTextEditPanel().setTextLines((AveryTextblock)field);
      }

      if (errorMessage == null)   // this is good!
      {
        getFrame().onFieldChanged();
        break;
      }
      // show the error message and try again
      JOptionPane.showMessageDialog(getFrame(), errorMessage, "Field Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  private void setMenuItemFont()
  {
    // if font is UTF8 display prompt menu item using field font
    AveryTextfield atf = (AveryTextfield)field;
    String encoding = atf.getEncoding();
    if (encoding.equals("UTF8"))
    {
      Font menuItemFont = new Font(atf.getTypeface(), Font.PLAIN, 14);
      this.setFont(menuItemFont);
      menuItemFont = null;
    }
  }

}