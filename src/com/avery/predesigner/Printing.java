package com.avery.predesigner;

import java.util.ArrayList;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PDFRenderer;

public class Printing
{
	
	public ArrayList enumeratePrinters(String defaultPrinterName)
	{
		ArrayList printerNames = new ArrayList();
    PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
    for (int i = 0; i < printServices.length; i++)
    {
    	PrintService printer = printServices[i];
    	String thisPrinterName = printer.getName();
    	if (thisPrinterName.equals(defaultPrinterName))
    		printerNames.add(0, thisPrinterName);
    	else
    		printerNames.add(thisPrinterName);
    }
    return printerNames;
	}

	public String getDefaultPrinterName()
	{
		return PrintServiceLookup.lookupDefaultPrintService().getName();
	}
	
  public void printPDF(String filePath, String jobName, int numCopies, String printerName)
	throws IOException, PrinterException
	{
		FileInputStream fileInputStream = new FileInputStream(filePath);
		byte[] pdfContent = new byte[fileInputStream.available()];
		fileInputStream.read(pdfContent, 0, fileInputStream.available());
		ByteBuffer buffer = ByteBuffer.wrap(pdfContent);
	
		final PDFFile pdfFile = new PDFFile(buffer);
		Printable printable = new Printable()
		{
			public int print(Graphics graphics, PageFormat pageFormat,
					int pageIndex) throws PrinterException
			{
				int pagenum = pageIndex + 1;
	
	      if ((pagenum >= 1) && (pagenum <= pdfFile.getNumPages()))
	      {
					Graphics2D graphics2D = (Graphics2D) graphics;
					PDFPage page = pdfFile.getPage(pagenum);
					
					Rectangle imageArea = new Rectangle((int) pageFormat.getImageableX(),
						(int) pageFormat.getImageableY(),
						(int) pageFormat.getImageableWidth(),
						(int) pageFormat.getImageableHeight());
	
	        graphics2D.translate(0, 0);
	
	        PDFRenderer pdfRenderer = new PDFRenderer(page, graphics2D, imageArea,
	        	null, null);
	
	        try
	        {
						page.waitForFinish();							
						pdfRenderer.run();
	        }
	        catch (InterruptedException exception)
	        {
	        	exception.printStackTrace();
	        }
	        
	        return PAGE_EXISTS;
	      }
	      else
	      {
	      	return NO_SUCH_PAGE;
	      }
			}
		};
	
	  PrinterJob printJob = PrinterJob.getPrinterJob();
	  PageFormat pageFormat = PrinterJob.getPrinterJob().defaultPage();
	  printJob.setJobName(jobName);
	  Book book = new Book();
	  book.append(printable, pageFormat, pdfFile.getNumPages());
	  printJob.setPageable(book);
	
		Paper paper = new Paper();
		paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());
		pageFormat.setPaper(paper);
		
		if (printerName != null)
		{
			PrintService[] printServices = PrinterJob.lookupPrintServices();
			
			for (int count = 0; count < printServices.length; ++count)
			{
				if (printerName.equalsIgnoreCase(printServices[count].getName()))
				{	
		      printJob.setPrintService(printServices[count]);	
		      break;
				}
			}			
		}
		
		if (numCopies > 1)
			printJob.setCopies(numCopies);
		
		printJob.print();
	}
}
