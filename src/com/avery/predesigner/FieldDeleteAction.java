package com.avery.predesigner;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Undoable Action to delete a field from a masterpanel</p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

class FieldDeleteAction extends UndoableAction
{
  private AveryMasterpanel panel;
  private AveryPanelField field;

  FieldDeleteAction(AveryMasterpanel panel, AveryPanelField field)
  {
    super("Undo Delete");
    this.panel = panel;
    this.field = field;
  }
  
  void undo()
  {
  	Predesigner.getProject().addFieldToMasterpanel(field, panel);
    Predesigner.getFrame().updateView();
  }
  
  void execute()
  {
    panel.removePanelField(field.getPrompt());
    Predesigner.getFrame().updateView();
  }
}