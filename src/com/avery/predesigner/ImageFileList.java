/*
 * ImageListPanel.java Created on Jan 9, 2008 by leeb
 * Copyright 2008 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;

import com.avery.project.AveryImage;
import com.avery.project.RightRes;

/**
 * @author leeb
 * Jan 9, 2008
 * ImageListPanel
 */
public class ImageFileList extends JList
{
  private int imageSize = 72;
  private Hashtable icons;
  
  public ImageFileList(List filenames, int iconSize)
  {
    super(filenames.toArray());
    this.icons = new Hashtable(filenames.size());
    this.imageSize = iconSize;
    this.setCellRenderer(new ImageRenderer());
    this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    this.setBackground(new Color(0,0,0,0));
  }
  
  private class ImageRenderer extends DefaultListCellRenderer
  {
    public Component getListCellRendererComponent(JList list,
        Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
      Thumb thumb = new Thumb((String)value, isSelected);
      return thumb;
    }
  }
  
  private class Thumb extends JLabel
  { 
    Thumb(String filename, boolean isSelected)
    {
      if (filename != null)
      {
      	//String fullFilename = "textures/" + filename;
      	//System.out.println("filename=" + filename);
        ImageIcon icon = (ImageIcon)icons.get(filename);
        if (icon == null)
        {
          AveryImage aImage = new AveryImage();
          aImage.setSourceAndGallery(filename, Config.getDefaultImageGallery());
          Image image = new RightRes().getImage(aImage, imageSize, imageSize);
    
          BufferedImage centeredImage = new BufferedImage(imageSize+2, imageSize+2, BufferedImage.TYPE_INT_RGB);
          Graphics gr = centeredImage.getGraphics();
          gr.setColor(Color.LIGHT_GRAY); 
          gr.fillRect(0, 0, imageSize+1, imageSize+1);
          int x = 1 + ((imageSize - image.getWidth(null)) / 2);
          int y = 1 + ((imageSize - image.getHeight(null)) / 2);
          gr.drawImage(image, x, y, null);
          gr.dispose();
          icon = new ImageIcon(centeredImage);
          icons.put(filename, icon);
        }
        
        setIcon(icon);
      }

      setBorder(BorderFactory.createBevelBorder(
            isSelected ? BevelBorder.LOWERED : BevelBorder.RAISED));
    }
  }
}
