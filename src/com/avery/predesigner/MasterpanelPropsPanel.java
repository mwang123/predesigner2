package com.avery.predesigner;

import java.awt.GridLayout;
import java.util.Iterator;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.avery.project.AveryMasterpanel;
import com.avery.utils.MergeMap;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: panel containing gadgets for the Masterpanel Properties dialog</p>
 * <p>Copyright: Copyright 2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

class MasterpanelPropsPanel extends JPanel
{
	private static final long serialVersionUID = 3631198611332381301L;
	private boolean wasTall;
    private String oldMergeMapName;
	private JComboBox descriptionComboBox;
	private JComboBox mergemapComboBox;
	private JCheckBox editableGadget = new JCheckBox("Editable");
	private JCheckBox previewableGadget = new JCheckBox("Previewable");
	private boolean oldEditable;
	private boolean oldPreviewable;
	
  private ButtonGroup orientation = new ButtonGroup();
  private JRadioButton tall = new JRadioButton("Tall");
  private JRadioButton wide = new JRadioButton("Wide");
  
  MasterpanelPropsPanel(AveryMasterpanel masterpanel)
  {
    setLayout(new GridLayout(0,1));
    add(new TriPane(
    		new JLabel("Width:"),
				new JLabel(Predesigner.twipsToUnitString(masterpanel.getWidth().doubleValue())),
				new JLabel()));
    
    add(new TriPane(
    		new JLabel("Height:"),
				new JLabel(Predesigner.twipsToUnitString(masterpanel.getHeight().doubleValue())),
				new JLabel()));
    
    if (initMergemapGadget(masterpanel))
    {
    	add(new JLabel("mergeMap Name:"));
    	add(mergemapComboBox);
    }
    
    descriptionComboBox = new JComboBox(
    		masterpanel.getLocalizableDescriptions(Predesigner.getProject().getLanguage()).toArray());
    
	descriptionComboBox.setSelectedItem(masterpanel.getDescription());
	if (!descriptionComboBox.getSelectedItem().equals(masterpanel.getDescription()))
	{
		descriptionComboBox.addItem(masterpanel.getDescription());
		descriptionComboBox.setSelectedItem(masterpanel.getDescription());
	}
	descriptionComboBox.setEditable(true);

	add(new JLabel("Masterpanel Description:"));
	add(descriptionComboBox);

    wasTall = masterpanel.isPortrait();
    tall.setSelected(wasTall);
    
    if (!masterpanel.isSquare())
    {
      wide.setSelected(masterpanel.isLandscape());
      orientation.add(tall);
      orientation.add(wide);
      add(new JLabel("Masterpanel Orientation:")); 	
      add(new TriPane(new JLabel(), tall, wide));
    }
    
    boolean isRotationAllowed = RotationManager.isRotationAllowed(masterpanel, Predesigner.getProject());
    if (!isRotationAllowed)
    {
	    tall.setEnabled(tall.isSelected());
	    wide.setEnabled(wide.isSelected());
    }
    
    // add the "editable" checkbox
    oldEditable = masterpanel.isEditable();
    oldPreviewable = masterpanel.isPreviewable();
    editableGadget.setSelected(oldEditable); 
    add(editableGadget);
    // add the "previewable" checkbox
    previewableGadget.setSelected(oldPreviewable); 
    add(previewableGadget);
  }
  
  String getDescription()
  {
    return (String)descriptionComboBox.getSelectedItem();
  }
  
  boolean orientationChanged()
  {
    return tall.isSelected() != wasTall;
  }
  
  boolean mergeMapChanged()
  {
  	return getMergeMapName().equals(oldMergeMapName) != true;
  }
  
  boolean editableChanged()
  {
  	return oldEditable != editableGadget.isSelected();
  }
  
  boolean previewableChanged()
  {
  	return oldPreviewable != previewableGadget.isSelected();
  }
  
  boolean isEditable()
  {
  	return editableGadget.isSelected();
  }
  
  boolean isPreviewable()
  {
  	return previewableGadget.isSelected();
  }
  /**
   * Note: in cases where the MergeMap file could not be read, 
   * this will return "none".  It's best to ask mergeMapChanged before
   * bothering to call this method.
   * @return the selection from the mergemap combo box
   */
  String getMergeMapName()
  {
  	if (mergemapComboBox != null)
  	{
  		Object o = mergemapComboBox.getSelectedItem();
  		if (o instanceof MergeMap)
  		{
  			return ((MergeMap)o).getName();
  		}
  		else if (o instanceof String)
  		{
  			return (String)o;
  		}
  	}
  	
  	return "none";
  }
  
  /**
   * Checks available mappings for the current project language.
   * If there are any, the mergemapComboBox is initialized.
   * @return <code>true</code> if mappings exist and the mergemapComboBox
   * is initialized
   */
  boolean initMergemapGadget(AveryMasterpanel masterpanel)
  {
    oldMergeMapName = masterpanel.getMergeMapName();
    if (oldMergeMapName == null || oldMergeMapName.length() == 0)
    {
    	oldMergeMapName = "none";
    }

 		try
		{
  		MergeMaps maps = new MergeMaps(Config.mergeMapFile);
  		List list = maps.getMaps(Predesigner.getProject().getLanguage());
  		if (list.size() > 0)
  		{
  			mergemapComboBox = new JComboBox();
  			mergemapComboBox.addItem("none");
  			Iterator iterator = list.iterator();
  			while (iterator.hasNext())
  			{
  				MergeMap map = (MergeMap)iterator.next();
  				mergemapComboBox.addItem(map);
  				if (map.getName().equals(oldMergeMapName))
  				{
  	   			mergemapComboBox.setSelectedItem(map);
  				}
  			}
  			
   			if (mergemapComboBox.getSelectedIndex() < 0)
   			{
   				mergemapComboBox.addItem(oldMergeMapName);
     			mergemapComboBox.setSelectedItem(oldMergeMapName);
   			}
  			
  			return true;	// total success
  		}
  		else throw new Exception("no mergeMaps for language: " + Predesigner.getProject().getLanguage());
		}
  	catch (Exception e)
		{
  		System.err.println(e.toString());
      JOptionPane.showMessageDialog(this, e.getMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
		}
  	 		
  	return false;
  }
  
  private class TriPane extends JPanel
	{
		private static final long serialVersionUID = 2343803705562773494L;

		TriPane(JComponent one, JComponent two, JComponent three)
		{
      setLayout(new GridLayout(1,3,5,5));
      add(one);
  		add(two);
  		add(three);
		}
	}
}