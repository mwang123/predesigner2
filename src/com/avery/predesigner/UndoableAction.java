package com.avery.predesigner;

import java.util.ArrayList;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: base class for actions that can be undone</p>
 * <p>Copyright: Copyright 2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

abstract class UndoableAction
{
  private String undoString;
  
  UndoableAction(String undoString)
  {
    this.undoString = undoString;
  }
  
  String getUndoString()
  {
    return undoString;
  }
  
  abstract void execute();
  abstract void undo();
  
}