package com.avery.predesigner;

import java.awt.Point;
import java.util.ArrayList;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;
import com.avery.project.Drawing;

public class FieldAlignDimensionAction extends UndoableAction
{
  private static int ALIGN_WIDTH = 0;
  private static int ALIGN_HEIGHT = 1;

  protected ArrayList currentSelections = new ArrayList();
  protected AveryMasterpanel panel = null;
  protected ArrayList originalSizes = new ArrayList();
  protected int currentSelection;
  protected int alignStyle = 0;
  
  public FieldAlignDimensionAction(AveryMasterpanel panel, ArrayList currentSelections, int currentSelection, int alignStyle)
  {
    super("Undo Align W or H");
    this.currentSelections.addAll(currentSelections);
    this.panel = panel;
    this.currentSelection = currentSelection;
    this.alignStyle = alignStyle;
  }
  
  void execute()
  {
  	AveryPanelField field = (AveryPanelField)(panel.getPanelFields()).get(currentSelection);
  	int width = field.getWidth().intValue();
  	int height = field.getHeight().intValue();
  	
		for (int i = 0; i < currentSelections.size(); i++)
		{
			int selection = (int)currentSelections.get(i);
			if (selection == currentSelection)
				break;
			
			field = (AveryPanelField)(panel.getPanelFields()).get(selection);
			boolean bMaintain = false;
	  	if (field instanceof Drawing)
	  	{
	  		Drawing drawing = (Drawing)field;
	  		bMaintain = drawing.isMaintainAspect();
	  	}
	    
			originalSizes.add(new Point(field.getWidth().intValue(), field.getHeight().intValue()));
	    Point newSize = new Point();
	    if (alignStyle == ALIGN_WIDTH)
	    {
	    	newSize.x = width;
	    	newSize.y = field.getHeight().intValue();
	    	if (bMaintain)
	    		newSize.y = width;
	    }
	    else
	    {
	    	newSize.x = field.getWidth().intValue();
	    	newSize.y = height;
	    	if (bMaintain)
	    		newSize.x = height;
	    }
	    
			field.setWidth(new Double(newSize.x));
			field.setHeight(new Double(newSize.y));
		} 
  }
  
	void undo()
  {
		for (int i = 0; i < currentSelections.size(); i++)
		{
			int selection = (int)currentSelections.get(i);

			if (selection != currentSelection)
			{
				AveryPanelField field = (AveryPanelField)panel.getPanelFields().get(selection);
				Point originalSize = (Point)originalSizes.get(i);
				field.setWidth(new Double(originalSize.x));
				field.setHeight(new Double(originalSize.y));
			}
		}
    Predesigner.getFrame().updateView();
  }

}
