package com.avery.predesigner;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
//import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
//import java.util.Locale;





import javax.imageio.*;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.faceless.pdf2.PDF;
import org.faceless.pdf2.PDFReader;
import org.faceless.pdf2.PagePainter;
import org.faceless.pdf2.PDFParser;

import net.sf.javavp8decoder.imageio.WebPImageReader;
import net.sf.javavp8decoder.imageio.WebPImageReaderSpi;

import com.avery.predesigner.editors.FieldEditorPanel;
import com.avery.project.AveryBackground;
import com.avery.project.AveryImage;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;
import com.avery.project.AveryTextblock;
import com.avery.project.AveryTextline;
import com.avery.project.Barcode;
import com.avery.project.Description;
import com.avery.project.Drawing;
import com.avery.project.TextPath;
import com.avery.project.shapes.AveryShape;
import com.avery.project.shapes.PolygonShape;
import com.avery.utils.AveryException;
import com.sun.media.imageio.plugins.tiff.TIFFImageWriteParam;
import com.sun.media.imageioimpl.plugins.tiff.TIFFImageWriter;
import com.sun.media.imageioimpl.plugins.tiff.TIFFImageWriterSpi;
import com.twelvemonkeys.imageio.plugins.jpeg.JPEGColorSpace;
import com.twelvemonkeys.imageio.plugins.jpeg.JPEGImageReader;
import com.twelvemonkeys.imageio.plugins.psd.PSDImageReader;
import com.twelvemonkeys.imageio.plugins.psd.PSDImageReaderSpi;
import com.twelvemonkeys.imageio.plugins.tiff.TIFFImageReader;
import com.twelvemonkeys.imageio.plugins.tiff.TIFFImageReaderSpi;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Predesigner's Add menu is used to add fields to a masterpanel</p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2
 */

class AddMenu extends JMenu
{
  private static final long serialVersionUID = -624315801421899649L;
  //public JMenuItem addTextLineMenuItem = new JMenuItem("Add Text Line...");
  public JMenuItem addTextBlockMenuItem = new JMenuItem("Add Text Block...");
  //private JMenuItem addHTMLBlockMenuItem = new JMenuItem("Add HTML Block...");
  public JMenuItem addImageMenuItem = new JMenuItem("Add Image...");
  public JMenuItem addBackgroundMenuItem = new JMenuItem("Add Background...");
  public JMenuItem addCurvedTextMenuItem = new JMenuItem("Add Curved Text...");
  public JMenuItem addBarcodeMenuItem = new JMenuItem("Add Barcode...");
  public JMenuItem addShapeMenuItem = new JMenuItem("Add Shape...");
  private JMenuItem addMasterpanelMenuItem = new JMenuItem("Add Combined Masterpanel...");
  private JMenuItem deleteMasterpanelMenuItem = new JMenuItem("Delete Masterpanel...");

  /**
   * Constructor handles all initialization
   */
  AddMenu()
  {
    setText("Add");
    
    // this is CTRL on PCs.
    int shortcutMask = java.awt.Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    //addTextLineMenuItem.setMnemonic('T');
    //addTextLineMenuItem.setAccelerator(KeyStroke.getKeyStroke('1', shortcutMask, false));
    //addTextLineMenuItem.addActionListener(new ActionListener() {
      //public void actionPerformed(ActionEvent e) {
        //onAddTextLine();
      //}
    //});
    //add(addTextLineMenuItem);

    addTextBlockMenuItem.setMnemonic('B');
    addTextBlockMenuItem.setAccelerator(KeyStroke.getKeyStroke('2', shortcutMask, false));
    addTextBlockMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onAddTextBlock();
      }
    });
    add(addTextBlockMenuItem);
   
    /*addHTMLBlockMenuItem.setMnemonic('H');
    addHTMLBlockMenuItem.setAccelerator(KeyStroke.getKeyStroke('3', shortcutMask, false));
    addHTMLBlockMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onAddHTMLTextBlock();
      }
    });
    add(addHTMLBlockMenuItem);*/

    addImageMenuItem.setMnemonic('I');
    addImageMenuItem.setAccelerator(KeyStroke.getKeyStroke('4', shortcutMask, false));
    addImageMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onAddImage();
      }
    });
    add(addImageMenuItem);

    addBackgroundMenuItem.setMnemonic('B');
    addBackgroundMenuItem.setAccelerator(KeyStroke.getKeyStroke('5', shortcutMask, false));
    addBackgroundMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onAddBackground();
      }
    });
    add(addBackgroundMenuItem);

    addCurvedTextMenuItem.setMnemonic('C');
    addCurvedTextMenuItem.setAccelerator(KeyStroke.getKeyStroke('6', shortcutMask, false));
    addCurvedTextMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onAddCurvedText();
      }
    });
    add(addCurvedTextMenuItem);
    
		addBarcodeMenuItem.setMnemonic('A');
		addBarcodeMenuItem.setAccelerator(KeyStroke.getKeyStroke('7', shortcutMask, false));
		addBarcodeMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onAddBarcode();
			}
		});
		add(addBarcodeMenuItem);
    
    addShapeMenuItem.setMnemonic('S');
    addShapeMenuItem.setAccelerator(KeyStroke.getKeyStroke('8', shortcutMask, false));
    addShapeMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onAddShape();
      }
    });
    add(addShapeMenuItem);

    
    addSeparator();
    addSeparator();
    addSeparator();

    addMasterpanelMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
      	onAddMasterpanelMenuItem();
      }
    });
   add(addMasterpanelMenuItem);

    deleteMasterpanelMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
      	onDeleteMasterpanelMenuItem();
      }
    });
   add(deleteMasterpanelMenuItem);

    
    addMenuListener(new AddMenuListener());
  }
  

  /**
   * The MenuListener grays out commands that are inappropriate for the
   * current state of the program.
   */
  class AddMenuListener implements MenuListener
  {
    public void menuCanceled(MenuEvent event)
    {   }
    public void menuDeselected(MenuEvent event)
    {   }
    public void menuSelected(MenuEvent event)
    {
      boolean allow = Predesigner.getFrame().allowMasterpanelEditing();

      //addTextLineMenuItem.setEnabled(allow);
      addTextBlockMenuItem.setEnabled(allow);
      //addHTMLBlockMenuItem.setEnabled(allow);
      addImageMenuItem.setEnabled(allow);
      addBackgroundMenuItem.setEnabled(allow && !Predesigner.getFrame().getCurrentMasterpanel().hasBackground());
      addCurvedTextMenuItem.setEnabled(allow);
      addBarcodeMenuItem.setEnabled(allow);
      addShapeMenuItem.setEnabled(allow);
    }
  }

  /**
   * Called when Add Text Line.. is selected
   * Creates a new AveryTextline, presents it for editing, then adds it
   * to the current Masterpanel.
   */
  void onAddTextLine()
  {
    AveryTextline field = new AveryTextline();
    field.setTypeface(Predesigner.styleSet.font);
    field.translateDescription(Predesigner.getProject().getLanguage());
		
    // get panel dimensions
    AveryMasterpanel masterpanel = Predesigner.getFrame().getCurrentMasterpanel();
    int width = masterpanel.getWidth().intValue();
    int height = masterpanel.getHeight().intValue();
    Point2D polyOffset = getPolyOffset(masterpanel);

    // scaling of field size depends on how big the panel is
    int fieldWidth  = (width - (int)polyOffset.getX()) / 2;
    if (fieldWidth < 360)
      fieldWidth = 360;
    
    int trueHeight = (height - (int)polyOffset.getY());
    int fieldHeightFactor = 5 + trueHeight / 1440;
    int fieldHeight  = trueHeight / fieldHeightFactor;
    if (fieldHeight < 120)
      fieldHeight = 120;

    // center horizontally, place at top
    // top instead of center because its easier to move it
    // in relation to existing stuff
    int x = ((int)polyOffset.getX() + width - fieldWidth) / 2;
    field.setWidth(new Double(fieldWidth));
    field.setHeight(new Double(fieldHeight));
    field.setPosition(new Point(x, 0));

    // point size depends on height
    int pointSize = fieldHeight / 22;
    field.setPointsize(new Double(pointSize < 72 ? pointSize : 72));

    if (editField(field, false))
    {
      // set promptOrder, ZOrder
      endOfPromptOrder(field);
      endOfZOrder(field);

      Predesigner.getProject().addFieldToMasterpanel(field, Predesigner.getFrame().getCurrentMasterpanel());
      Predesigner.getFrame().onFieldChanged();
    }
  }

  /**
   * Called when Add Text Block... is selected
   * Creates a new AveryTextblock, presents it for editing, then adds it
   * to the current Masterpanel.
   */
  void onAddTextBlock()
  {
    AveryTextblock field = Predesigner.getFrame().getCurrentMasterpanel().createDefaultTextblock();
    field.setTypeface(Predesigner.styleSet.font);
    field.translateDescription(Predesigner.getProject().getLanguage());
    
    if (editField(field, false))
    {
      // set promptOrder, ZOrder
      endOfPromptOrder(field);
      endOfZOrder(field);

      Predesigner.getProject().addFieldToMasterpanel(field, Predesigner.getFrame().getCurrentMasterpanel());
      Predesigner.getFrame().onFieldChanged();
    }
  }

  /**
   * Called when Add HTML Block... is selected
   * Creates a new AveryTextblock, presents it for editing, then adds it
   * to the current Masterpanel, using HTML editor.
   */
  void onAddHTMLTextBlock()
  {
    AveryTextblock field = Predesigner.getFrame().getCurrentMasterpanel().createDefaultTextblock();
    field.setTypeface(Predesigner.styleSet.font);
    field.translateDescription(Predesigner.getProject().getLanguage());
    
    if (editField(field, true))
    {
      // set promptOrder, ZOrder
      endOfPromptOrder(field);
      endOfZOrder(field);

      Predesigner.getProject().addFieldToMasterpanel(field, Predesigner.getFrame().getCurrentMasterpanel());
      Predesigner.getFrame().onFieldChanged();
    }
  }

  /**
   * Called when Add Image... is selected
   * Creates a new AveryImage, presents it for editing, then adds it
   * to the current Masterpanel.
   */
  void onAddImage()
  {
    AveryImage field = new AveryImage();
    field.translateDescription(Predesigner.getProject().getLanguage());
    
    File file = new File(Config.getDefaultImageGallery() + "/HighRes", Predesigner.styleSet.graphic01);

    if (!file.exists()) // styleset image doesn't exist
    {
      // pick an image from the file system
      JFileChooser dlg = new JFileChooser(Config.getDefaultImageGallery() + "/HighRes");
      dlg.setFileFilter(new ImageFileFilter());
      dlg.setDialogTitle("Select an image file");
      dlg.setAcceptAllFileFilterUsed(false);
      
      ImageAccessory thumb = new ImageAccessory(dlg);
      dlg.setAccessory(thumb);
      dlg.addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, thumb);
  
      if (dlg.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
      {
        file = dlg.getSelectedFile();
      }
      else return;  // user can't decide on an image
    }
    
    java.awt.image.BufferedImage bufferedImage = null;
    try
    {
      if (file.getName().toLowerCase().endsWith(".png"))
      {
        java.io.FileInputStream fis = new java.io.FileInputStream(file);
	    bufferedImage = ImageIO.read(fis);
	    fis.close();
      }
      else if (file.getName().toLowerCase().endsWith(".tif") || file.getName().toLowerCase().endsWith(".tiff"))
      {
        //java.io.FileInputStream fis = new java.io.FileInputStream(file);
      	TIFFImageReader tifir = new TIFFImageReader(new TIFFImageReaderSpi());
      	ImageInputStream iis = ImageIO.createImageInputStream(file);
      	tifir.setInput(iis);
      	int bits = tifir.getBitsPerSample();
      	if (bits < 1 || bits > 8)
      	{
      		throw new Exception();
      	}     	
      	bufferedImage = tifir.read(0);
      	iis.close();
      	//fis.close();
      }
      else if (file.getName().toLowerCase().endsWith(".webp"))
      {
        WebPImageReader wpir = new WebPImageReader(new WebPImageReaderSpi());
        ImageInputStream wpiis = ImageIO.createImageInputStream(file);
        wpir.setInput(wpiis);
        bufferedImage = wpir.read(0);
        wpiis.close();
        //File outfile = new File("c:/Users/Brad/webp.png");
        //ImageIO.write(bufferedImage, "png", new FileOutputStream(outfile));
      }
      else if (file.getName().toLowerCase().endsWith(".psd"))
      {
    	java.io.FileInputStream fis = new java.io.FileInputStream(file);
    	//Iterator readers = ImageIO.getImageReadersByFormatName("psd");
    	//PSDImageReader psdir = (PSDImageReader)readers.next();
    	PSDImageReader psdir = new PSDImageReader(new PSDImageReaderSpi());
      	ImageInputStream iis = ImageIO.createImageInputStream(file);
      	psdir.setInput(iis);
      	
      	short bitDepth = psdir.getBits();
      	short mode = psdir.getMode();
      	
      	if (bitDepth < 1 || bitDepth > 8)
      	{
      		throw new Exception("PSD bits per pixel not supported!");     		
      	}
      	
    	/*String names[] = ImageIO.getReaderFormatNames();
      	for (int i = 0; i < names.length; i++)
      		System.out.println("reader " + names[i]);
    	names = ImageIO.getWriterFormatNames();
    	for (int i = 0; i < names.length; i++)
    		System.out.println("writer " + names[i]);*/
        	
	    bufferedImage = psdir.read(0); //ImageIO.read(fis);
      	iis.close(); 
      	
      	// if CMYK convert to RGB...
      	if (mode == 4)
      	{
      		ColorSpace cs = bufferedImage.getColorModel().getColorSpace();
      		//int csType = cs.getType();
	      	BufferedImage dst = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(),BufferedImage.TYPE_INT_RGB);
	      	//ColorSpace dcs = dst.getColorModel().getColorSpace();
	      	//int dscType = dcs.getType();
	      	ColorConvertOp op = new ColorConvertOp(cs, dst.getColorModel().getColorSpace(), null);
	      	op.filter(bufferedImage,  dst);
	      	bufferedImage = null;
	      	bufferedImage = dst;  		
      	}
      	
      	
    	//int numComponents = bufferedImage.getColorModel().getNumComponents();
      	
	    String tif = file.getAbsolutePath().toLowerCase();
	    tif = tif.replace(".psd",  ".tif");
	        
      	TIFFImageWriter tifiw = new TIFFImageWriter(new TIFFImageWriterSpi());
      	
      	/*while (writers.hasNext() && tifiw == null)
      	{
      		ImageWriter writer = (ImageWriter)writers.next();
      		if (writer instanceof TIFFImageWriter)
      			tifiw = (TIFFImageWriter)writer;
      	}*/
      	
      	if (tifiw != null)
      	{
		    File tifFile = new File(tif);
		    try
		    {
		    	ImageOutputStream ios = ImageIO.createImageOutputStream(tifFile);
		    	tifiw.setOutput(ios);
		    	IIOImage outImage = new IIOImage(bufferedImage, null, null);
		    	
	    		TIFFImageWriteParam tiwp = (TIFFImageWriteParam)(tifiw.getDefaultWriteParam());
	    		String[] cts = tiwp.getCompressionTypes();
	    		tiwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
	    		//if (numComponents < 4)
	    		//{
	    			tiwp.setCompressionType("LZW"); 
	    		//}
	    		//else
	    		//{
	    			//tiwp.setCompressionType("PackBits");  // ZLib fail, JPEG fail, PackBits fails with CMYK images
	    		//}    		
	    		tifiw.write(null, outImage, tiwp);
		    	
	    		// use this for no compression 
		    	//tifiw.write(null, outImage, null);
	    		
		    	
		    	// todo - Convert TIFF CMYK to RGB

		    	
		    	/*if (numComponents < 4)
		    	{
		    		TIFFImageWriteParam tiwp = (TIFFImageWriteParam)(tifiw.getDefaultWriteParam());
		    		String[] cts = tiwp.getCompressionTypes();
		    		tiwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		    		//tiwp.setCompressionType("EXIF JPEG");
		    		tiwp.setCompressionType("JPEG");                 // 'new style' JPEG?
		    		//tiwp.setCompressionType("CCITT T.4");
		    		//tiwp.setCompressionType("PackBits");
		    		//tiwp.setCompressionType("LZW");
		    		//tiwp.setCompressionType("ZLib");
		    		tifiw.write(null, outImage, tiwp);
		    	}
		    	else
		    	{
		    		tifiw.write(null, outImage, null);
		    	}*/
		    	ios.close();
		    }
		    catch (Exception e1)
		    {
		        JOptionPane.showMessageDialog(Predesigner.getFrame(), e1.getMessage(), "Error PSD", JOptionPane.ERROR_MESSAGE);
		    	return;
		    }
		    
		    file = new File(tif);
      	}
      }
      else if (file.getName().toLowerCase().endsWith(".pdf") || file.getName().toLowerCase().endsWith(".ai"))
      {
      	try
      	{
	  			PDFReader reader = new PDFReader(file);
	  			PDF pdf = new PDF(reader);
	  			
	  			float dpi = 300;
	  			
	  			PDFParser parser = new PDFParser(pdf);
	  			PagePainter painter = parser.getPagePainter(0);
	  			bufferedImage = painter.getImage(dpi);
	  			//BufferedImage bi = painter.getImage(dpi);
	  			//String png = file.getAbsolutePath().toLowerCase();
    	    //png = png.replace(".pdf",  ".png");
    	    //png = png.replace(".ai",  ".png");
			    //FileOutputStream fos = new FileOutputStream(png);
	  			//ImageIO.write(bi, "png", fos);
	  			//fos.close();
	  			
			    //file = new File(png);
	  			//bufferedImage = ImageIO.read(file);  			
	  			//bufferedImage = ImageIO.read(new File(png));  			
      	}
		    catch (Exception e1)
		    {
		      JOptionPane.showMessageDialog(Predesigner.getFrame(), e1.getMessage(), "Error PDF", JOptionPane.ERROR_MESSAGE);
		    	return;
		    }
      }
      /*else if (file.getName().toLowerCase().endsWith(".svg"))
      {
        SVGImageReader svgir = new SVGImageReader(new SVGImageReaderSpi());
        ImageInputStream svgiis = ImageIO.createImageInputStream(file);
				svgir.setInput(svgiis);
				
				int width = svgir.getWidth(0);
				int height = svgir.getHeight(0);
				
				if (width > 0 && height > 0)
				{
					svgiis.close();
					svgiis = ImageIO.createImageInputStream(file);
					SVGReadParam param = new SVGReadParam();
					param.setSourceRenderSize(new Dimension(4 * width, 4 * height));
					bufferedImage = svgir.read(0, param);					
				}
				else
				{
					bufferedImage = svgir.read(0);									
				}
				
				// do NOT query dimensions this causes batik to run code that does not let it resize!!
				//int oWidth = svgir.getWidth(0);
				//int oHeight = svgir.getHeight(0);
				// this hint rewritten by brad to force it to repaint at desired size.
		      	
				svgiis.close();
        	      	
      	if (svgir != null)
      	{
    	    String png = file.getAbsolutePath().toLowerCase();
    	    png = png.replace(".svg",  ".png");
			    File pngFile = new File(png);
			    try
			    {
				    FileOutputStream fos = new FileOutputStream(png);
				    boolean bSuccess = ImageIO.write(bufferedImage, "png", fos);
				    fos.close();
			    }
			    catch (Exception e1)
			    {
			        JOptionPane.showMessageDialog(Predesigner.getFrame(), e1.getMessage(), "Error SVG", JOptionPane.ERROR_MESSAGE);
			    	return;
			    }
			    
			    file = new File(png);
      	}
      }*/
      else
      {
      	Iterator readers = ImageIO.getImageReadersByFormatName("JPEG");
      	ImageReader reader = (ImageReader)readers.next();
      	ImageInputStream iis = ImageIO.createImageInputStream(file);
      	reader.setInput(iis);
      	
      	JPEGImageReader jir = (JPEGImageReader)reader;
      	// BKN:  made changes to TwelveMonkeys JPEG plugin source to give public access to
      	// all four of the methods invoked in the next line
      	JPEGColorSpace jcsType = JPEGImageReader.getSourceCSType(jir.getJFIF(), jir.getAdobeDCT(), jir.getSOF());
      	//System.out.println(jcsType);
	  		bufferedImage = reader.read(0);
	  		iis.close();
  		
      	/*Iterator<ImageTypeSpecifier> its = reader.getImageTypes(0);
      	while (its.hasNext())
      	{
      		System.out.println("ITS=" + its.next());  		
      	}*/
      	if (jcsType.equals(JPEGColorSpace.YCbCr) || jcsType.equals(JPEGColorSpace.YCbCrA) ||
      		jcsType.equals(JPEGColorSpace.RGB) || jcsType.equals(JPEGColorSpace.RGBA))
      	{
      	}
      	else
      	{
      		// convert the image to RGB and overwrite it.
      		//bufferedImage = ImageIO.read(fis);
      		File file1 = new File(file.getAbsolutePath());
      		ColorSpace cs = bufferedImage.getColorModel().getColorSpace();
      		int csType = cs.getType();
	      	BufferedImage dst = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(),BufferedImage.TYPE_INT_RGB);
	      	ColorSpace dcs = dst.getColorModel().getColorSpace();
	      	int dscType = dcs.getType();
	      	ColorConvertOp op = new ColorConvertOp(cs, dst.getColorModel().getColorSpace(), null);
	      	op.filter(bufferedImage,  dst);
	      	bufferedImage = null;
	      	bufferedImage = dst;
	      	String jpg = file.getAbsolutePath().toLowerCase();
	      	//jpg = jpg.replace(".jpg", "_TMC.jpg");
	      	FileOutputStream fos = new FileOutputStream(jpg);
	      	ImageIO.write(dst, "jpg",  fos);
	      	fos.close();
	      }
      	
      	  // the simple old way - does not handle CMYK and some other JPEG formats I bet
	     //bufferedImage = com.sun.image.codec.jpeg.JPEGCodec.createJPEGDecoder(fis).decodeAsBufferedImage();
    	}
    }
    catch (Exception e)
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    field.setSourceAndGallery(file.getName(), file.getParentFile().getParent());

    AveryMasterpanel masterpanel = Predesigner.getFrame().getCurrentMasterpanel();
    double masterWidth = masterpanel.getWidth().doubleValue();
    double masterHeight = masterpanel.getHeight().doubleValue();

    // calculate an appropriate size
    double imageAspect = (double)bufferedImage.getWidth() / (double)bufferedImage.getHeight();
    double height = masterHeight / 2.0;
    double width = height * imageAspect;
    if (width > masterWidth)
    {
      width = masterWidth / 2.0;
      height = width / imageAspect;
    }

    // set position to center of masterpanel
    int x = (int)(((masterWidth - width) / 2));
    int y = (int)(((masterHeight - height) / 2));
    
    if (masterpanel.getShape() instanceof PolygonShape)
    {
    	Rectangle2D rect = getPolyRect(masterpanel);
      height = rect.getHeight() / 2.0;
      width = height * imageAspect;
      if (width > rect.getWidth())
      {
        width = rect.getWidth() / 2.0;
        height = width / imageAspect;
      }
    	x = (int)(rect.getMinX() + (rect.getWidth() - width) / 2);
    	y = (int)(rect.getMinY() + (rect.getHeight() - height) / 2);
    }
    
    field.setWidth(new Double(width));
    field.setHeight(new Double(height));
    field.setPosition(new java.awt.Point(x, y));

    if (editField(field, false))
    {
      // set promptOrder, ZOrder
      endOfPromptOrder(field);
      endOfZOrder(field);

      Predesigner.getProject().addFieldToMasterpanel(field, masterpanel);
      Predesigner.getFrame().onFieldChanged();
    }
  }

  /**
   * Called when Add Background.. is selected
   * Creates a new AveryBackground, presents it for editing, then adds it
   * to the current Masterpanel.
   */
  void onAddBackground()
  {
    AveryBackground field = new AveryBackground();
    field.translateDescription(Predesigner.getProject().getLanguage());
    
    File file = new File(Config.getDefaultImageGallery() + "/HighRes", Predesigner.styleSet.graphic02);
		
    /*if (!file.exists()) // pick an image from the file system
    {
      JFileChooser dlg = new JFileChooser(Config.getDefaultImageGallery() + "/HighRes");
      dlg.setFileFilter(new ImageFileFilter());
      dlg.setDialogTitle("Select a background image file");
      dlg.setAcceptAllFileFilterUsed(false);
      
      ImageAccessory thumb = new ImageAccessory(dlg);
      dlg.setAccessory(thumb);
      dlg.addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, thumb);
  
      if (dlg.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
      {
        file = dlg.getSelectedFile();
      }
      else return;  // user can't decide on an image
    }
    
    field.setSourceAndGallery(file.getName(), file.getParentFile().getParent());*/

    // set to full size of panel
    AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
  	AveryShape shape = master.getShape();  
    Point2D polyOffset = getPolyOffset(master);
  	Point pos = new Point();
    
    if (master.isBleedable())
    {
    	double bleed = Config.getDefaultBleed();
    	pos.setLocation(-bleed, -bleed);
    	
    	Double fieldWidth = new Double(master.getWidth().doubleValue() + bleed + bleed);
    	Double fieldHeight = new Double(master.getHeight().doubleValue() + bleed + bleed);
    	
    	if (shape instanceof PolygonShape)
    	{
    		PolygonShape polyShape = (PolygonShape)shape;
      	pos.setLocation(-bleed + polyOffset.getX(), -bleed + polyOffset.getY());
      	fieldWidth = new Double(polyShape.getPolyWidth() + bleed + bleed);
      	fieldHeight =  new Double(polyShape.getPolyHeight() + bleed + bleed);
    	}
    	field.setPosition(pos);
    	field.setWidth(fieldWidth);
    	field.setHeight(fieldHeight);

    }
    else
    {
    	double panelWidth = master.getWidth().doubleValue();
    	double panelHeight = master.getHeight().doubleValue();
    	if (shape instanceof PolygonShape)
    	{
    		PolygonShape polyShape = (PolygonShape)shape;
    		panelWidth = polyShape.getPolyWidth();
    		panelHeight = polyShape.getPolyHeight();
      	pos.setLocation(polyOffset.getX(), polyOffset.getY());
    	}

    	field.setPosition(pos);
    	field.setWidth(new Double(panelWidth));
    	field.setHeight(new Double(panelHeight));
    }

    // edit and add the field to the masterpanel
    if (editField(field, false))
    {
      // set to beginning of promptOrder
      field.setPromptOrder("1.0");
      try
      { // move all others down a notch
        Iterator iterator = master.getPromptSortedPanelFields().iterator();
        double nextP = 2.0;
        while (iterator.hasNext())
        {
          ((AveryPanelField)iterator.next()).setPromptOrder(Double.toString(nextP));
          nextP += 1.0;
        }
      }
      catch (Exception e)    {    }

      // set to beginning of ZOrder
      field.setZLevel(1.0);
      // move all others down a notch
      Iterator iterator = master.getZLevelIterator();
      double nextZ = 2.0;
      while (iterator.hasNext())
      {
        ((AveryPanelField)iterator.next()).setZLevel(nextZ);
        nextZ += 1.0;
      }

      Predesigner.getProject().addFieldToMasterpanel(field, master);
      Predesigner.getFrame().onFieldChanged();
    }
  }

  /**
   * Called when Add Curved Text... is selected
   * Creates a new TextPath, presents it for editing, then adds it
   * to the current Masterpanel.
   */
  void onAddCurvedText()
  {
    TextPath field = new TextPath();
    field.setTypeface(Predesigner.styleSet.font);
    field.translateDescription(Predesigner.getProject().getLanguage());
		
    // default values generalized while inclined towards cd panels...
    // get panel dimensions
    AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
    
    int width = master.getWidth().intValue();
    int height = master.getHeight().intValue();
    int dimension = width > height ? height : width;
    dimension = (dimension * 9) / 10;
    int x = (width - dimension) / 2;
    int y = (height - dimension) / 2;

    
    AveryShape shape = master.getShape();
  	if (shape instanceof PolygonShape)
  	{
  		PolygonShape polyShape = (PolygonShape)shape;
  		width = (int)polyShape.getPolyWidth();
  		height = (int)polyShape.getPolyHeight();
      dimension = width > height ? height : width;
      dimension = (dimension * 9) / 10;
      Point2D polyOffset = getPolyOffset(master);
      x = (int)polyOffset.getX() + (width - dimension) / 2;
      y = (int)polyOffset.getY() + (height - dimension) / 2;
  	}

    // make largest square field that fits in the panel, scaled down 10%
    
    field.setWidth(new Double(dimension));
    field.setHeight(new Double(dimension));
    field.setPosition(new Point(x, y));

    // point size depends on dimension
    int pointSize = dimension / 180;
    if (pointSize < 8)
      pointSize = 8;
    if (pointSize > 72)
      pointSize = 72;
    field.setPointsize(new Double(pointSize));

    // other expected defaults
    field.setJustification("center");
    field.setStartPosition(new Point(0,-1));

    if (editField(field, false))
    {
      // set promptOrder, ZOrder
      endOfPromptOrder(field);
      endOfZOrder(field);

      Predesigner.getProject().addFieldToMasterpanel(field, Predesigner.getFrame().getCurrentMasterpanel());
      Predesigner.getFrame().onFieldChanged();
    }
  }
  
  void onAddBarcode()
  {
  	Barcode field = new Barcode("EAN 13", "1234567890128", new Point(0,0));
    field.translateDescription(Predesigner.getProject().getLanguage());
  	
  	// calculate center position based on size of newly constructed Barcode field
    AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
		double panelWidth = master.getWidth().doubleValue();
		double panelHeight = master.getHeight().doubleValue();
		int x = (int)((panelWidth - field.getWidth().doubleValue()) / 2);
		int y = (int)((panelHeight - field.getHeight().doubleValue()) / 2);
    
		AveryShape shape = master.getShape();
  	if (shape instanceof PolygonShape)
  	{
  		PolygonShape polyShape = (PolygonShape)shape;
  		panelWidth = polyShape.getPolyWidth();
  		panelHeight = polyShape.getPolyHeight();
      Point2D polyOffset = getPolyOffset(master);
  		x = (int)(polyOffset.getX() + (panelWidth - field.getWidth().doubleValue()) / 2);
  		y = (int)(polyOffset.getY() + (panelHeight - field.getHeight().doubleValue()) / 2);
  	}
		
		field.setPosition(new Point(x,y));
		
		if (editField(field, false))
		{
			// set promptOrder, ZOrder
			endOfPromptOrder(field);
			endOfZOrder(field);

      Predesigner.getProject().addFieldToMasterpanel(field, Predesigner.getFrame().getCurrentMasterpanel());
			Predesigner.getFrame().onFieldChanged();			
		}
  }
  
  void onAddShape()
  {
    Object[] options = { "Rectangle", "Square", "Ellipse", "Circle", "Polygon",  "Cancel"};
    int option = JOptionPane.showOptionDialog(Predesigner.getFrame(),
      "What kind of shape do you want to add?",
      "Create Shape",
      JOptionPane.YES_NO_CANCEL_OPTION,
      JOptionPane.QUESTION_MESSAGE,
      null, options, null);
    
    if (option == 5 || option == JOptionPane.CLOSED_OPTION)
      return;

    AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
    double width = master.getWidth().doubleValue() / 2.0;
    double height = master.getHeight().doubleValue() / 2.0;
    double x = width / 2.0;
    double y = height / 2.0;
    
    AveryShape shape = master.getShape();
    if (shape instanceof PolygonShape)
    {
    	PolygonShape polyShape = (PolygonShape)shape;
    	width = polyShape.getPolyWidth() / 2.0;
    	height = polyShape.getPolyHeight() / 2.0;
      Point2D polyOffset = getPolyOffset(master);
  		x = polyOffset.getX() + (width / 2);
  		y = polyOffset.getY() + (height / 2);
    }
    
    Drawing field;
    try
    {
    	if (option == 4)  // polygon shapes located in shapes folder
    	{	
    		Object[] possibilities = {
    				"Thin_Line",
    				"Thick_Line",
    				"Rounded_Rect",
    				"Arrow_Small",
    				"Arrow_Large", 
    				"Heart",
    				"Diamond",
    				"Star_5_Points_Flat",
    				"Star_5_Points",
    				"Star_12_Points",
    				"Triangle_Equilateral",     				
    				"Triangle_Right",     				
    				"Pentagon",
    				"Hexagon",
    				"Octagon",
    				"Scallop",
    				"Cruciform"
    		};
    		String s = (String)JOptionPane.showInputDialog(
    				Predesigner.getFrame(),
    		                    "Select a Gallery Shape:",
    		                    "Gallery Shape Selection Dialog",
    		                    JOptionPane.PLAIN_MESSAGE,
    		                    null,
    		                    possibilities,
    		                    "Thin_Line");

    		//If a string was returned, say so.
    		if ((s != null) && (s.length() > 0))
    		{    			
          String type = "polygon";
          // MAGIC NUMBER comes from the size of shapes in the shape gallery.
          // shapes xml were obtained by opening a 5160 in DPO 7, adding a shape to the label
          // and saving the project without resizing the shape. Then extracting the polypoints element
          // of the shape from the saved project xml. All shapes are saved with the same square dimensions
          width = height = 379;
          if (s.equals("Rounded_Rect"))
          {
          	width = 1728;
          	height = 797.54;
          }
          else if (s.equals("Heart"))
          {
          	width = 930.46;
          	height = 864;
          }
          else if (s.equals("Thin_Line"))
          {
          	width = 1440;
          	height = 80;
          }
          else if (s.equals("Thick_Line"))
          {
          	width = 1440;
          	height = 160;
          }
          else if (s.equals("Scallop"))
          {
          	width = 5300;
          	height = 5400;
          }
          
          String userDir = System.getProperty("user.dir");
          String shapesDir = userDir + File.separatorChar + "shapes";
          
          field = new Drawing(
          	AveryShape.manufactureShape(type, width, height, 0, Drawing.getPolygonXML(shapesDir, s)),
          	x, y);
    			//field.setDrawingName(s);
    			field.setDescription(s + "_");

          if (!(s.indexOf("_Line") > 0))
          {
            double d = Math.min(width, height);
            field.setWidth(new Double(d));
          	field.setHeight(new Double(d));
          }
          else
          {
            field.setWidth(new Double(width));
          	field.setHeight(new Double(height));    	
          }
          
          //field.setMaintainAspect(true);
          field.setColor(Color.gray);
          
          if (editField(field, false))
          {
            // set promptOrder, ZOrder
            endOfPromptOrder(field);
            endOfZOrder(field);

            Predesigner.getProject().addFieldToMasterpanel(field, Predesigner.getFrame().getCurrentMasterpanel());
            Predesigner.getFrame().onFieldChanged();      
          }
          return;
     		}
    		else
    		{
    			return;
    		}
     		
    	}
      String type = (option < 2) ? "rect" : "ellipse";
      field = new Drawing(AveryShape.manufactureShape(type, width, height, 0, null), x, y);
      //if (type.equals("rect"))
				//field.setDescription("Rectangle_");
      //else
  			//field.setDescription("Ellipse_");

      if (option % 2 == 1)  // square or circle
      {
        double d = Math.min(width, height);
        field.setWidth(new Double(d));
        field.setHeight(new Double(d));
        field.setMaintainAspect(true);
      }
      field.setColor(Color.gray);
    }
    catch (AveryException ax) 
    { 
      ax.printStackTrace();
      return; 
    }
    
    if (editField(field, false))
    {
      // set promptOrder, ZOrder
      endOfPromptOrder(field);
      endOfZOrder(field);

      Predesigner.getProject().addFieldToMasterpanel(field, Predesigner.getFrame().getCurrentMasterpanel());
      Predesigner.getFrame().onFieldChanged();      
    }
  }
  
  void onAddMasterpanelMenuItem()
  {
  	Project project = Predesigner.getFrame().getProject();
  	if (project.getAllMasterpanelFields().size() > 0)
  		return;
  	
  	PageView view = Predesigner.getFrame().getPageView();
  	Description description = Predesigner.getFrame().getCurrentMasterpanel().getDescriptionObject();
  	
  	project.addMasterpanel(view.getPageNumber(), description.getIndex());
  	
    ArrayList masterList = project.getMasterpanels();
      
  	if (masterList.size() > 0)
  			Predesigner.getFrame().setCurrentMasterpanel(0);
		Predesigner.getFrame().rebuildMasterpanelPopupMenu(project);
		Predesigner.getFrame().rebuildPagePopupMenu(project);
		Predesigner.frame.updateView();
  }

void onDeleteMasterpanelMenuItem()
{
	Project project = Predesigner.getFrame().getProject();
	if (project.getAllMasterpanelFields().size() > 0)
		return;
	
  AveryMasterpanel deleteMaster = Predesigner.getFrame().getCurrentMasterpanel();
  
  if (!deleteMaster.getFieldIterator().hasNext())
  {
    project.removeMasterpanel(deleteMaster);
    ArrayList masterList = project.getMasterpanels();
    
		if (masterList.size() > 0)
			Predesigner.getFrame().setCurrentMasterpanel(0);
		Predesigner.getFrame().rebuildMasterpanelPopupMenu(project);
		Predesigner.getFrame().rebuildPagePopupMenu(project);
		Predesigner.frame.updateView();
  } 
}

  /**
   * Handles presentation of a field's editor.  Loops on error until the user
   * either gets it right or cancels the edit process.
   * @param field - the field to edit
   * @return <code>true</code> on successful validation of the field,
   * <code>false</code> if the user gave up in frustration.
   */
  boolean editField(AveryPanelField field, boolean isHTML)
  {
  	
    FieldEditorPanel editor = FieldEditorPanel.getEditorPanel(field, isHTML);
   
    // loop until they get it right or cancel
    while (JOptionPane.showConfirmDialog(Predesigner.getFrame(), editor, field.getClass().getName(),
      JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
      == JOptionPane.OK_OPTION)
    {
      String errorMessage = editor.updateField();
      if (field instanceof AveryTextblock)
      {
      	Predesigner.getFrame().getMasterpanelToolbar().getTextEditPanel().setTextLines((AveryTextblock)field);
      }
     if (errorMessage == null)   // this is good!
      {
        return true;
      }
      // show the error message and try again
      JOptionPane.showMessageDialog(Predesigner.getFrame(), errorMessage, "Field Error", JOptionPane.ERROR_MESSAGE);
    }
    return false;
  }

  void endOfZOrder(AveryPanelField field)
  {
    java.util.Iterator zIterator = Predesigner.getFrame().getCurrentMasterpanel().getZLevelIterator();
    double top = 1.0;
    while (zIterator.hasNext())
    {
      double z = ((AveryPanelField)zIterator.next()).getZLevel();
      if (z != OrderFieldZ.FOREGROUND_Z)
      {
        top = z;
      }
    }

    field.setZLevel(top + 1.0);
  }

  void endOfPromptOrder(AveryPanelField field)
  {
    try
    {
      java.util.Iterator iterator = Predesigner.getFrame().getCurrentMasterpanel().getPromptSortedPanelFields().iterator();
      String end = "1.0";
      while (iterator.hasNext())
      {
        end = ((AveryPanelField)iterator.next()).getPromptOrder();
      }

      field.setPromptOrder(Double.toString((Double.parseDouble(end) + 1.0)));
    }
    catch (Exception e)
    {    }
  }
  
  Point2D getPolyOffset(AveryMasterpanel masterpanel)
  {
  	Point2D offset = new Point2D.Double(0,0);
  	AveryShape shape = masterpanel.getShape();
  
  	if (shape instanceof PolygonShape)
  	{
  		PolygonShape polyShape = (PolygonShape)shape;
  		offset = polyShape.getMinPosition();
  	}
  	return offset;
  }
  
  Rectangle2D getPolyRect(AveryMasterpanel masterpanel)
  {
  	Rectangle2D size = new Rectangle2D.Double(0,0, 1, 1);
  	AveryShape shape = masterpanel.getShape();
  
  	if (shape instanceof PolygonShape)
  	{
  		PolygonShape polyShape = (PolygonShape)shape;
  		size = new Rectangle2D.Double(polyShape.getMinPosition().getX(), polyShape.getMinPosition().getY(),
  				polyShape.getPolyWidth(), polyShape.getPolyHeight());
  	
  	}
  	return size;
  }

}