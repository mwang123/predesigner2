package com.avery.predesigner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import com.avery.product.ProductGroup;
import com.avery.product.ProductGroupList;
import com.avery.project.AveryGridLayout;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPage;
import com.avery.project.AveryPanel;

public class RotationManager 
{
	static final int DIRECTION_NONE = 0;
	static final int DIRECTION_CW = 1;
	static final int DIRECTION_CCW = 2;
	
	static int suggestDirection(Project project)
	{
		int direction = DIRECTION_NONE;
		
		ProductGroup group = getProductGroupFor(project);
		if ( null != group && !group.isComplex()) 
		{ 
			// use pre-defined direction
			String pageOrientation = RotationManager.getViewOrientationFor(project);
			if ( pageOrientation.indexOf("preferPortrait") > -1 ) {
		    	direction = DIRECTION_CW;
			}
			else if ( pageOrientation.indexOf("preferLandscape-cw") > -1 ) {
		    	direction = DIRECTION_CW;
			}
			else if ( pageOrientation.indexOf("preferLandscape-ccw") > -1 ) {
		    	direction = DIRECTION_CCW;
			}
		}
		else
		{
			// let an actor to select direction
		    SelectDirectionPanel directionUI = new SelectDirectionPanel(project);
		    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), directionUI, "Direction of Content Rotation",
		    		JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
		    {
		    	direction = directionUI.isClockwise() ? DIRECTION_CW : DIRECTION_CCW;
	        }
		}

		return direction;
	}
	
	static boolean isRotationAllowed(AveryMasterpanel master, Project project)
	{
    	// master panels with polygon shapes do not allow re-orientation
	    if (master == null || master.allowReorientation() == false) {
	      return false;
	    }

		ProductGroup group = getProductGroupFor(project);
		if ( null != group && !group.isComplex()) 
		{
			String pageOrientation = RotationManager.getViewOrientationFor(project);
			if ( pageOrientation.indexOf("preferPortrait") > -1 ) {
				return true;
			}
			else if ( pageOrientation.indexOf("preferLandscape-cw") > -1 ) {
				return true;
			}
			else if ( pageOrientation.indexOf("preferLandscape-ccw") > -1 ) {
				return true;
			}
			
			return false;
		}
		
		return true;
	}

	static boolean rotateMasterpanel(Project project, String panelId, boolean clockwise)
	{
		boolean result = project.reorientMasterpanel(panelId, clockwise);

		ProductGroup group = getProductGroupFor(project);
		if ( null != group && !group.isComplex()) { 
		    validateSortOrder(project, panelId, clockwise);
		}
		
		return result;
	}

	static String getViewOrientationFor(Project project)
	{
		AveryPage page = project.getPage(1);
		if (null != page) {
			return page.getViewOrientation(); 
		}
		
		return "";
	}

	/**
	 * The method corrects panels sort order (it should be moving across 
	 * from top-left, and then down). Typically, new sort order is represented 
	 * by AltSortOrder value of certain panel.
	 * 
	 * @param project
	 * @param panelId
	 * @param clockwiseRotation 
	 */
	private static void validateSortOrder(Project project, String panelId, boolean clockwiseRotation) 
	{
		// walk through all pages
		Iterator itPages = project.getPages().iterator();
		while (itPages.hasNext())
		{
			AveryPage page = (AveryPage)itPages.next();
			
			AveryGridLayout gridLayout = page.getGridLayout(panelId);
			final boolean reorient = gridLayout.getReorient();
			final int rows = reorient ? gridLayout.getNumberAcross() : gridLayout.getNumberDown();
			final int cols = reorient ? gridLayout.getNumberDown() : gridLayout.getNumberAcross();

			// get cloned panels with alternative sort order
			List srcPanels = clonePanels( page.getAltSortedPanels( clockwiseRotation ) );
			Iterator itSrc = srcPanels.iterator();

			// get panels with natural sort order
			List dstPanels = page.getPanels();
			Iterator itDst = dstPanels.iterator();
		    while (itDst.hasNext())
		    {
		    	AveryPanel dstPanel = (AveryPanel)itDst.next();
		    	AveryPanel srcPanel = (AveryPanel)itSrc.next();
		    	
		    	dstPanel.setPosition(srcPanel.getPosition());
		    	dstPanel.setAltSortOrder( generateAltSortOrder( dstPanel.getNumber(), rows, cols ) );
		    }
		}
	}
	
	private static List clonePanels(List src)
	{
		List dst = new ArrayList();
		Iterator itSrc = src.iterator();
	    while (itSrc.hasNext())
	    {
	    	dst.add(((AveryPanel)itSrc.next()).positionClone());	    	
	    }

		return dst;
	}
	
	private static int generateAltSortOrder(int position, int rows, int cols) 
	{
		// validate position: [1 : rows * cols]
		if (position < 1 || position > (rows * cols)) {
			return 0;
		}
		
		position--;
		return (rows * cols) - (((position % cols) + 1) * rows) + (position / cols) + 1;
	}

	private static ProductGroup getProductGroupFor(Project project)
	{
		try
		{
			ProductGroupList list = Predesigner.getProductGroupList();
			return list.getProductGroup(project.getProductGroup());
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(
				Predesigner.getFrame(), e.getStackTrace(), "Rotation Manager", JOptionPane.PLAIN_MESSAGE);
		}

		return null;
	}
}
