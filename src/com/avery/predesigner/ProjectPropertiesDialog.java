package com.avery.predesigner;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import com.avery.predesign.DesignUseCategories;
import com.avery.product.ProductGroupList;
import com.avery.project.AveryGridLayout;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPage;
import com.avery.project.AveryProject;
import com.borland.jbcl.layout.PaneConstraints;
import com.borland.jbcl.layout.PaneLayout;
import com.borland.jbcl.layout.VerticalFlowLayout;

/**
 * Title:        Predesigner - Avery Project Editor
 * Description:  Editor dialog for Project Properties
 * Copyright:    Copyright (c)2001-2004 Avery Dennison Corp. All Rights Reserved
 * Company:      Avery Dennison
 * @author       Bob Lee; Ver 1.5 John Angermeyer
 * @version      2.0
 */

class ProjectPropertiesDialog extends JDialog
{
	private static final long serialVersionUID = 6475187746453546979L;

	/**
   * the project being edited
   */
  private AveryProject theProject;
  
  private DesignUseCategories designUseCategories;
  private String productType;
  private List altUses;

  // the things that we're editing
  private JTextField copyrightField = new JTextField();
  private JTextField descriptionField = new JTextField();
  private JTextField themeField = new JTextField();
  private JTextField productGroupField = new JTextField();
  private JTextField skuField = new JTextField();
  private JTextField skuDescriptionField = new JTextField();
  private JComboBox altUsesComboBox = new JComboBox();
  private JButton altUseAddButton = new JButton("Add...");
  private JComboBox editorComboBox = new JComboBox();
  private JCheckBox allowAddCheckBox = new JCheckBox("Allow Adding Fields");
  private JComboBox reorientComboBox = new JComboBox();
  
  static final String ALLOW_ADD_HINT_NAME = "allowAddPanelField"; 

  // becomes true when user pushes Okay button
  boolean okay = false;

  ProjectPropertiesDialog(Frame1 frame, AveryProject project)
  {
    super(frame, "Project Properties", true);
    try
    {
      theProject = project;
      initData(project);
      initUI();
      pack();

      Dimension dlgSize = this.getPreferredSize();
      Dimension frmSize = frame.getSize();
      Point loc = frame.getLocation();
      this.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  private void initUI() throws Exception
  {
    JPanel mainPanel = new JPanel(new VerticalFlowLayout());
    Border border1 = BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140));
    mainPanel.setBorder(border1);
    getContentPane().add(mainPanel);
    
    JPanel copyrightPanel = new JPanel(new PaneLayout());
    JLabel copyrightLabel = new JLabel("Copyright");
    copyrightPanel.add(copyrightLabel, new PaneConstraints("copyrightLabel", "copyrightLabel", PaneConstraints.ROOT, 0.25f));
    copyrightPanel.add(copyrightField, new PaneConstraints("copyrightField", "copyrightLabel", PaneConstraints.RIGHT, 0.75f));
    mainPanel.add(copyrightPanel, null);    
    
    JPanel descriptionPanel = new JPanel(new PaneLayout());
    JLabel descriptionLabel = new JLabel("Description"); 
    descriptionPanel.add(descriptionLabel, new PaneConstraints("descriptionLabel", "descriptionLabel", PaneConstraints.ROOT, 0.25f));
    descriptionPanel.add(descriptionField, new PaneConstraints("descriptionField", "descriptionLabel", PaneConstraints.RIGHT, 0.75f));
    mainPanel.add(descriptionPanel, null);
    
    JPanel themePanel = new JPanel(new PaneLayout());
    JLabel themeLabel = new JLabel("Style Set");
    themePanel.add(themeLabel, new PaneConstraints("themeLabel", "themeLabel", PaneConstraints.ROOT, 0.25f));
    themePanel.add(themeField, new PaneConstraints("themeField", "themeLabel", PaneConstraints.RIGHT, 0.75f));
    themeField.setEnabled(false);     // user can't change this, except via changing Style Set
    mainPanel.add(themePanel, null);
    
    JPanel altUsePanel = new JPanel(new PaneLayout());
    JLabel altUseLabel = new JLabel("Alternate Use");
    altUsePanel.add(altUseLabel, new PaneConstraints("altUseLabel", "altUseLabel", PaneConstraints.ROOT, 0.25f));
    altUsePanel.add(altUsesComboBox, new PaneConstraints("altUsesComboBox", "altUseLabel", PaneConstraints.RIGHT, 0.75f));
    altUsePanel.add(altUseAddButton, new PaneConstraints("altUsesAddButton", "altUsesComboBox", PaneConstraints.RIGHT, 0.35f));
    altUseAddButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e)  {
        onAddAlternateUse();
      }
    });
    mainPanel.add(altUsePanel, null);
    
    JPanel editorPanel = new JPanel(new PaneLayout());
    JLabel editorLabel = new JLabel("Editor Hint");
    editorPanel.add(editorLabel, new PaneConstraints("editorLabel", "editorLabel", PaneConstraints.ROOT, 0.25f));
    editorPanel.add(editorComboBox, new PaneConstraints("editorField", "editorLabel", PaneConstraints.RIGHT, 0.75f));
    allowAddCheckBox.setHorizontalAlignment(SwingConstants.RIGHT);
    editorPanel.add(allowAddCheckBox, new PaneConstraints("allowAddCheckBox", "allowAddCheckBox", PaneConstraints.RIGHT, 0.35f));    
    mainPanel.add(editorPanel, null);
    
    if (reorientComboBox.getItemCount() > 1)
    {
      JPanel reorientPanel = new JPanel(new PaneLayout());
      JLabel reorientLabel = new JLabel("Page View Orientation");
      reorientPanel.add(reorientLabel, new PaneConstraints(null, null, PaneConstraints.ROOT, 0.25f));    
      reorientPanel.add(reorientComboBox, new PaneConstraints("reorientField", "reorientLabel", PaneConstraints.RIGHT, 0.75f));
      reorientPanel.add(new JPanel(), new PaneConstraints(null, null, PaneConstraints.RIGHT, 0.35f));
      mainPanel.add(reorientPanel, null);
    }

    JPanel productGroupPanel = new JPanel(new GridLayout(1, 6));   
    JLabel productGroupLabel = new JLabel("Product Group ");
    productGroupPanel.add(productGroupLabel); 
    productGroupPanel.add(productGroupField); 
    
    JLabel skuLabel = new JLabel("  Design SKU ");
    skuLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    productGroupPanel.add(skuLabel); 
    productGroupPanel.add(skuField); 

    JLabel skuDescriptionLabel = new JLabel("  SKU Desc ");
    skuDescriptionLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    productGroupPanel.add(skuDescriptionLabel); 
    productGroupPanel.add(skuDescriptionField); 
    
    mainPanel.add(productGroupPanel, null);
    
    JButton okayButton = new JButton("Okay");
    okayButton.setEnabled(true);
    okayButton.setSelected(true);
    okayButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // save changes back to project
        okay = saveFieldText(theProject);
        dispose();
      }
    });    
    
    JButton cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });
    
    JPanel buttonPanel = new JPanel();
    buttonPanel.add(okayButton, null);
    buttonPanel.add(cancelButton, null);
    mainPanel.add(buttonPanel, null);
  }

  /**
   * initialize the UI elements with data from the project
   * @param project - the AveryProject being edited
   */
  private void initData(AveryProject project)
  {
    // Update all editable fields with project settings
    copyrightField.setText        ( project.getCopyright() );
    descriptionField.setText      ( project.getDescription() );
    themeField.setText      			( project.getDesignTheme() );
    productGroupField.setText     ( project.getProductGroup() );
    skuField.setText              ( project.getSku() );
    skuDescriptionField.setText	  ( project.getSkuDescription() );
    
    ProductGroupList pgList = null;
    try { pgList = Predesigner.getProductGroupList(); }
    catch (Exception ex) { ex.printStackTrace(); }
    
    try   // set up the design categories list
    {
      productType = Predesigner.getProductGroupList().getProductType(project.getProductGroup());
      designUseCategories = new DesignUseCategories(Config.designCategoriesFile);
      altUses = designUseCategories.getUses(productType);
      Object designCategory = project.getDesignCategory();
      if (designCategory == null)
      {
        designCategory = altUses.get(0);
      }
      else if (!altUses.contains(designCategory))
      {
        altUses.add(designCategory);
      }
      altUsesComboBox.setModel(new DefaultComboBoxModel(altUses.toArray()));
      altUsesComboBox.setSelectedItem(designCategory);
    }
    catch (Exception ex)
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(),
          "productType = " + productType + "\n" + ex, 
          "Design Use Categories Error", JOptionPane.ERROR_MESSAGE);
      ex.printStackTrace();
      
      if (altUsesComboBox.getItemCount() == 0)
      {
        altUsesComboBox.addItem(project.getDesignCategory());
      }
    }

    editorComboBox.addItem("");  // blank is first
    editorComboBox.addItem("Designer");
    editorComboBox.addItem("EditContent");
    editorComboBox.addItem("UploadMergeData");
    editorComboBox.addItem("CreateSets");
    editorComboBox.addItem("OpenPath");
    
    String editor = project.getHintValue("predesignEditor");
    if (editor == null)
    {
      editorComboBox.setSelectedIndex(0);
    }
    else
    {
      editorComboBox.setSelectedItem(editor);
    }
    
    allowAddCheckBox.setSelected(!"false".equals(theProject.getHintValue(ALLOW_ADD_HINT_NAME)));
    
    try
    {
      if (project.getPages().size() == 1)  // only applies to single page projects
      {
        String pgName = project.getProductGroup();
        if (Predesigner.getProductGroupList().getProductGroup(pgName).isComplex())
        {          
          AveryPage page = (AveryPage)project.getPages().get(0);
          String orientation = page.getViewOrientation();

          reorientComboBox.addItem("portrait");
          reorientComboBox.addItem("landscape-cw");
          reorientComboBox.addItem("landscape-ccw");
          reorientComboBox.setSelectedItem(orientation);
          if (!reorientComboBox.getSelectedItem().equals(orientation))
          {
            // handle values like "preferPortrait"
            reorientComboBox.addItem(orientation);
            reorientComboBox.setSelectedItem(orientation);
          }
        }
      }
    }
    catch (Exception ex) { ex.printStackTrace(); }
  }

  /**
   * Updates project settings with text from all editable fields
   * @return should return <code>true</code> only if a field has changed,
   * but currently <b>always</b> returns <code>true</code>
   */
  private boolean saveFieldText(AveryProject project)
  {
    project.setCopyright       ( copyrightField.getText() );
    project.setDescription     ( descriptionField.getText() );
    // project.setDesignTheme     ( themeField.getText() );   // no longer editable
    project.setDesignCategory 	( (String)altUsesComboBox.getSelectedItem() );
    if (editorComboBox.getSelectedIndex() == 0)
    {
      project.removeHint("predesignEditor");
    }
    else
    {
      project.addHint("predesignEditor", (String)editorComboBox.getSelectedItem());
    }
    
    if (allowAddCheckBox.isSelected())
    {
      // no hint means "true" by default
      project.removeHint(ALLOW_ADD_HINT_NAME);
    }
    else
    {
      project.addHint(ALLOW_ADD_HINT_NAME, "false");
    }
    
    if (reorientComboBox.getItemCount() > 0)
    {
      AveryPage page = (AveryPage)project.getPages().get(0);
      String oldOrientation = page.getViewOrientation();
      String newOrientation = (String)reorientComboBox.getSelectedItem();
      if (!oldOrientation.equals(newOrientation))
      {
        page.setViewOrientation(newOrientation);
        
        // handle gridLayout reorientation
        if (((oldOrientation.indexOf("trait") > 0) && (newOrientation.indexOf("scape") > 0))
         || ((oldOrientation.indexOf("scape") > 0) && (newOrientation.indexOf("trait") > 0)))
        {
          flipGridLayoutReorientFlags(project, page);
        }
        
        Predesigner.getFrame().updateView();
      }
    }
    
    project.setProductGroup    ( productGroupField.getText() );
    project.setSku             ( skuField.getText() );
    project.setSkuDescription  ( skuDescriptionField.getText() );
    
    if (designUseCategories.modified) try 
    {
      designUseCategories.write(Config.designCategoriesFile);
    } 
    catch(IOException iox) 
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(),
          "There was a problem updating " + Config.designCategoriesFile + "\n" + iox.getMessage(),
          "Add Alternate Use", JOptionPane.ERROR_MESSAGE);
    }
    return true;
  }
  
  private void flipGridLayoutReorientFlags(AveryProject project, AveryPage page)
  {
    Iterator iterator = project.getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      String masterID = ((AveryMasterpanel)iterator.next()).getID();
      AveryGridLayout gridLayout = page.getGridLayout(masterID);
      if (gridLayout != null)
      {
        gridLayout.setReorient(!gridLayout.getReorient());
      }
    }
  }

  /* Overridden so we can exit when window is closed */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      dispose();
    }
    super.processWindowEvent(e);
  }
  
  private void onAddAlternateUse()
  {
    JTextField textField = new JTextField();
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), 
        textField, "Add Alternate Use", 
        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
    {
      String newUse = textField.getText();
      if (newUse == null || newUse.length() == 0)
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), "Nothing to add!",
            "Add Alternate Use", JOptionPane.ERROR_MESSAGE);
      }
      else if (altUses.contains(newUse))
      {
        altUsesComboBox.getModel().setSelectedItem(newUse);
      }
      else
      {
        // add it to the big list
        designUseCategories.addUse(productType, newUse);
        
        // add new item to the combobox and select it 
        altUses = designUseCategories.getUses(productType);
        altUsesComboBox.setModel(new DefaultComboBoxModel(altUses.toArray()));
        altUsesComboBox.getModel().setSelectedItem(newUse);
      }
    }
  }
}