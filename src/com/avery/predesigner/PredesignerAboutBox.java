/**
 * Title:        Predesigner - the AveryProject Editor
 * Copyright:    Copyright (c)2001-2005 Avery Dennison Corp. All Rights Reserved
 * Company:      Avery Dennison
 * @author       Bob Lee
 * @version      3.0
 */

package com.avery.predesigner;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

class PredesignerAboutBox extends JDialog implements ActionListener
{
	private static final long serialVersionUID = 2741400691277021984L;
	private String product		= "AveryDoc Predesign Creator / Editor / Multiplier";
  private String version		= "Version 2.0 - Apr 27, 2010";
  private String copyright	= "Copyright (c) 2000-2010 Avery Dennison Corp.";

  PredesignerAboutBox(JFrame frame1)
  {
    //super(frame1);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);

	  setTitle("About Averysoft Predesigner2");
    setResizable(false);
    
    JLabel imageLabel = new JLabel();
		imageLabel.setIcon(new ImageIcon(PredesignerAboutBox.class.getResource("OPNAICON40.gif")));
		imageLabel.setAlignmentX(0.5f);
		imageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		imageLabel.setHorizontalTextPosition(SwingConstants.CENTER);

    Box box = Box.createVerticalBox();
    box.add(Box.createVerticalStrut(10));
    box.add(new JLabel(product));
    box.add(Box.createVerticalStrut(10));
    box.add(new JLabel(version));
    box.add(Box.createVerticalStrut(2));
    box.add(new JLabel("Built with beta BFO BarCode image generation"));
    box.add(Box.createVerticalStrut(10));
    box.add(new JLabel(copyright));
    box.add(Box.createVerticalStrut(10));
     
    JPanel panel = new JPanel();
    panel.setBorder(
    		new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(148, 145, 140)));
    panel.add(imageLabel);
    panel.add(box);
    
    getContentPane().add(panel);
    pack();
  }

  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      dispose();
    }
    super.processWindowEvent(e);
  }

  public void actionPerformed(ActionEvent e)
  {  }
}