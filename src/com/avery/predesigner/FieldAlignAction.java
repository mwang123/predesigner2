package com.avery.predesigner;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

public class FieldAlignAction extends UndoableAction
{
  private static int ALIGN_LEFT = 0;
  private static int ALIGN_RIGHT = 1;
  private static int ALIGN_TOP = 2;
  private static int ALIGN_BOTTOM = 3;

  protected ArrayList currentSelections = new ArrayList();
  protected AveryMasterpanel panel = null;
  protected ArrayList originalPositions = new ArrayList();
  protected int currentSelection;
  protected int alignStyle = 0;
  
  public FieldAlignAction(AveryMasterpanel panel, ArrayList currentSelections, int currentSelection, int alignStyle)
  {
    super("Undo Align Edge");
    this.currentSelections.addAll(currentSelections);
    this.panel = panel;
    this.currentSelection = currentSelection;
    this.alignStyle = alignStyle;
  }
  
  void execute()
  {
  	AveryPanelField field = (AveryPanelField)(panel.getPanelFields()).get(currentSelection);
  	int x = field.getPosition().x;
  	int xwidth = field.getPosition().x + field.getWidth().intValue();
  	int y = field.getPosition().y;
  	int yheight = field.getPosition().y + field.getHeight().intValue();
  	
		for (int i = 0; i < currentSelections.size(); i++)
		{
			int selection = (int)currentSelections.get(i);
			if (selection == currentSelection)
				break;
			
			field = (AveryPanelField)(panel.getPanelFields()).get(selection);
	    
			originalPositions.add(field.getPosition());
	    Point newPosition = new Point();
	    if (alignStyle == ALIGN_LEFT)
	    {
	    	newPosition.x = x;
	    	newPosition.y = field.getPosition().y;
	    }
	    else if (alignStyle == ALIGN_RIGHT)
	    {
	    	newPosition.x = xwidth - field.getWidth().intValue();
	    	newPosition.y = field.getPosition().y;
	    }
	    else if (alignStyle == ALIGN_TOP)
	    {
	    	newPosition.x = field.getPosition().x;
	    	newPosition.y = y;
	    }
	    else // ALIGN_BOTTOM
	    {
	    	newPosition.x = field.getPosition().x;
	    	newPosition.y = yheight - field.getHeight().intValue();
	    }
	    
			field.setPosition(newPosition);
		} 
  }
  
	void undo()
  {
		for (int i = 0; i < currentSelections.size(); i++)
		{
			int selection = (int)currentSelections.get(i);

			if (selection != currentSelection)
			{
				AveryPanelField field = (AveryPanelField)panel.getPanelFields().get(selection);
				Point originalPosition = (Point)originalPositions.get(i);
				field.setPosition(originalPosition);
			}
		}
    Predesigner.getFrame().updateView();
  }



}
