/*
 * ProductMultiplier.java Created on Jan 14, 2010 by leeb
 * Copyright 2010 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.avery.Averysoft;
import com.avery.predesign.AutoTemplate;
import com.avery.product.ProductGroup;
import com.avery.product.ProductGroupList;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPage;
import com.avery.project.AveryProject;

/**
 * @author leeb
 * Jan 14, 2010
 * ProductMultiplier
 */
class ProductsMultiplier
{
  // hold these variables for duration of session
  static private String inputFolder;
  static private String outputFolder;
  
  static void multiply()
  throws IOException, JDOMException
  {
    SelectorPanel selector = new ProductsMultiplier.SelectorPanel(
      inputFolder == null ? Config.getProjectHome() : inputFolder, 
      outputFolder == null ? Config.getProjectHome() : outputFolder ); 
    
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), selector, 
        "Select Family Group Folders", JOptionPane.OK_CANCEL_OPTION, 
        JOptionPane.PLAIN_MESSAGE) != JOptionPane.OK_OPTION)
    {
      return;
    }
    
  	boolean bUseDesignTheme = false;
    HashMap groupsMap = parseFamilyGroups();
    ///logBaseFamilyGroups();
    
    //if (true == true)
    	//return;
    
    if (groupsMap.containsKey("designTheme"))
    	bUseDesignTheme = true;
    
    ProductGroupList pgl = Predesigner.getProductGroupList();
    
    
    //groupsMap.put("U-0087-01", 1);
    //groupsMap.put("U-0089-01", 1);
    //groupsMap.put("U-0093-01", 1);
    
    //ProductGroup inputGroup = selector.getSourceProductGroup();    
    inputFolder = selector.inputFolderPanel.getDirectory();
    
    outputFolder = selector.outputFolderPanel.getDirectory();
    
    HashMap filterFamilies = new HashMap();
    MultiplierDialog dlg = new MultiplierDialog("Product Family Predesign Generator");    
    dlg.logComment("Started " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
    dlg.setVisible(true);
    //dlg.makeBusy(true);
    
    List<File> inputFiles = getInputFiles(inputFolder);
    //System.out.println("number input files=" + inputFiles.size());
    for (int j = 0; j < inputFiles.size(); j++)
    {
      //System.out.println("j=" + j);
    	File nextFile = inputFiles.get(j);
    	if (nextFile.isDirectory())
    		continue;
    	
      Project inProject = new Project();
      try
      {
      	inProject.read(nextFile);
      }
      catch (Exception e)
      {
      	System.out.println("unable to read project name=" + nextFile.getAbsolutePath());
        dlg.logError(e.toString(), nextFile);
      	continue;
      }
    	String inputProductGroupName = inProject.getProductGroup();
    	if (filterFamilies.containsKey(inputProductGroupName))
    		continue;
    	filterFamilies.put(inputProductGroupName, "y");
    	//System.out.println("inputProductGroupName=" + inputProductGroupName);
    
    	// TODO: validate user inputs
    
    	// make an arraylist of all products to output to
    	ArrayList matchers = new ArrayList();
    	String match = inputProductGroupName;
    	if (bUseDesignTheme)
    	{
    		match = match + inProject.getDesignTheme();
    		//System.out.println("match=" + match);
    	}
    	String matchValue = (String)groupsMap.get(match);
    	if (matchValue == null)
    		continue;
    	
      Set<String> keys = groupsMap.keySet();
      for (String key: keys)
      {
      	//System.out.println("key=" + key);
      	String keyValue = (String)groupsMap.get(key);
      	//System.out.println("keyValue=" + keyValue);
      	if (matchValue.equals(keyValue))
      	{
      		String addKey = key;
      		if (bUseDesignTheme)
      		{
      			int index = addKey.indexOf(inProject.getDesignTheme());
      			if (index > -1)
      			{
      				addKey = addKey.substring(0, index);
        			matchers.add(addKey);
        			//System.out.println("addKey=" + addKey);
      			}      				
      		}
      		else
      		{
      			matchers.add(addKey);
      			//System.out.println("addKey=" + addKey);
      		}
      	}  		
      }

	    Iterator iterator = matchers.iterator();
	    //Iterator iterator = Predesigner.getProductGroupList().iterator();
	    dlg.logComment("Using ProductGroup " + inputProductGroupName + " files from " + inputFolder);
	    
	    //String inputGroupsKey = inputGroup.getName();
	    //if (bUseDesignTheme)
	    	//inputGroupsKey += 
	    //String inputGroupsName = (String)groupsMap.get(inputGroup.getName());
	        
	    // LATER ON CHECK TO MAKE SURE THIS KEY EXISTS
	    String ign = (String)groupsMap.get(inputProductGroupName);
	    //System.out.println("ign=" + ign);
    
	    while (iterator.hasNext())
	    {
	    	//System.out.println("hasNext");
	    	String opgn = (String)iterator.next();
	    	
	      ProductGroup outputGroup = pgl.getProductGroup(opgn);
	      if (outputGroup == null)
	      {
	  	    dlg.logComment("Error: ProductGroup " + opgn + " not found");
	      	continue;
	      }
	      
	      String outputGroupName = outputGroup.getName();
	      
	      if (outputGroup.getName().equals(inputProductGroupName))
	      	continue;
	      
	      if (!bUseDesignTheme)
	      {
	        //System.out.println("search output ProductGroup=" + outputGroup.getName());
	        if (!groupsMap.containsKey(outputGroup.getName()))
	        	continue;
	        //System.out.println("a");
	      
	        String ogn = (String)groupsMap.get(outputGroup.getName());
	        //System.out.println("ogn=" + ogn);
	        if (!ign.equals(ogn))
	        	continue;      	
	        //System.out.println("b");
	      }
	     
	      boolean generatePreviews = true;
	    
	/*	    int yesNo = JOptionPane.showConfirmDialog(
		        null,
		        "Would you like to generate page previews?",
		        "Generate Page Previews",
		        JOptionPane.YES_NO_OPTION);
		
		    if (yesNo == JOptionPane.YES_OPTION)
		    {
		    }*/
	      
		    File batFile = new File(Config.getBatDirectory(), outputGroup.getBatName());
		    
		    
		    File[] files = new File(inputFolder).listFiles();
	    
	    
	      dlg.makeBusy(true);   // guarantee busy cursor
		    for (int i = 0; i < files.length; ++i)
		    {
		      //System.out.println("i=" + i);
		      File file = files[i];
		      String filename = file.getName();
		      if (filename.endsWith(".xml") && filename.indexOf(inputProductGroupName) >= 0)
		      {
		        try
		        {
		          Project source = new Project();
		          source.read(file);
		          if (!inputProductGroupName.equals(source.getProductGroup()))
		          {
		            dlg.logError("misleading ProductGroup in filename", file);
		            continue;
		          }
		           
		          if (bUseDesignTheme)
		          {
			          String inputGroupsKey = inputProductGroupName;
			          String outputGroupsKey = outputGroup.getName();
			          inputGroupsKey += source.getDesignTheme();
			          outputGroupsKey += source.getDesignTheme();
			          
			          String inputGroupsName = (String)groupsMap.get(inputGroupsKey);
			          if (!groupsMap.containsKey(outputGroupsKey))
			          	continue;
			          
			          String outputGroupsName = (String)groupsMap.get(outputGroupsKey);
			          if (!inputGroupsName.equals(outputGroupsName))
			          	continue;
		          }
	
		          if (i == 0)
		          	dlg.logComment("Making ProductGroup " + outputGroup.getName() + " files in " + outputFolder);
	
		          Project target = new Project();
		          target.read(batFile);
		          
		          // upgrade BAT to template
		          target.setSku(outputGroup.getBaseSku());
		          target.setProductGroup(outputGroup.getName());
		          // apply paperImage, color, etc. from ProductGroup
		          target.applyAppearances(outputGroup, new File(Config.getBatDirectory() + "/papers").getAbsolutePath(), false);
		          
		          // copy project-level attributes from source
		          target.setLanguage(source.getLanguage());
		          target.setCopyright(source.getCopyright());
		          target.setDescription(source.getDescription());
		          target.setDesignTheme(source.getDesignTheme());
		          target.setDesignCategory(source.getDesignCategory());
		
		          // do the Autotemplate magic
		          int masters = Math.min(source.getMasterpanels().size(), target.getMasterpanels().size());
		          for (int n=0; n < masters; ++n)
		          {
		            AutoTemplate autoTemplate = new AutoTemplate((AveryMasterpanel)source.getMasterpanels().get(n));
		            autoTemplate.applyTo(target, (AveryMasterpanel)target.getMasterpanels().get(n));
		          }
	          
		          target.fixGalleries(inputFolder + "/HighRes");
	
		          File output = ToolsMenu.autoFile(target, outputGroup.getDescription(), outputFolder);
		
		          // always make page previews to ensure text font size fits box
		        	int k = 1;
		        	Iterator pageIterator = target.getPages().iterator();
		        	while (pageIterator.hasNext())
		        	{
		      			AveryPage page = (AveryPage)pageIterator.next();
		      			int pageWidth = (int)(page.getWidth().doubleValue() * .05);
		      			BufferedImage image = target.makePagePreview(k++, pageWidth);
		        		if (generatePreviews)
		        		{
		        			String outfilename = output.getName();
		        			String pgNumberExtension = ".pg" + String.valueOf(k - 1) + ".png";
		        			String pngFileName = outfilename.replace(".xml", pgNumberExtension);
		        			ImageIO.write((RenderedImage)image,"png", new File(outputFolder + "/" + pngFileName));
		        		}
		        	}
	        	
		          target.write(output);
		          
		          // log results
		          dlg.logProject(output);
		        }
		        catch (Exception ex)
		        {
		          // log error or any I/O exception
		          dlg.logError(ex.toString(), file);
		          ex.printStackTrace();
		          dlg.makeBusy(false);
		        }
		      }
	    	}
	      //System.out.println("DONE Files Loop");
    
	    }
      //System.out.println("DONE NEXT");
	    
    } // end files[] loop
    //System.out.println("DONE INPUT FILES");
    dlg.logComment("Finished " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
    dlg.makeBusy(false);
    
  }
  
	static List<File> getInputFiles(String inputFolder)
	{  	  
		String files;
		File folder = new File(inputFolder);
		File[] listOfFiles = folder.listFiles();
		List<File> xmlListOfFiles = new ArrayList<>();
		
		for (int i = 0; i < listOfFiles.length; i++) 
		{
			if (listOfFiles[i].isFile()) 
			{
				files = listOfFiles[i].getName();
				if (files.endsWith(".xml") || files.endsWith(".XML"))
				{
					//System.out.println(files);
					xmlListOfFiles.add(listOfFiles[i]);
				}
			}
		}
		return xmlListOfFiles;
  }

  static HashMap parseFamilyGroups()
  {
  	HashMap familyGroups = new HashMap();
  	try
  	{
  		File fgf = Config.familyGroupFile;
  		SAXBuilder builder = null;
  		builder = new SAXBuilder(false);
  		boolean bFirstDesignTheme = false;
		
  		Element root = builder.build(fgf).getRootElement();  
    	String rootDesignTheme = root.getAttributeValue("designTheme");
    	
    	if (rootDesignTheme != null && rootDesignTheme.length() > 0 && !bFirstDesignTheme)
    	{
  			bFirstDesignTheme = true;
      	familyGroups.put("designTheme", "true");     			
    	}
      Iterator fgiterator = (root.getChildren("familyGroup").iterator());
      while (fgiterator.hasNext())
      {
        Element fgchild = (Element)fgiterator.next();
      	String name = fgchild.getAttributeValue("name");
      	String designTheme = fgchild.getAttributeValue("designTheme");
      	if (rootDesignTheme != null)
      		designTheme = rootDesignTheme;
      	
      	String[] dtList = null;
      	if (designTheme != null && designTheme.length() > 0)
      	{
        	dtList = designTheme.split(" ");
        	if (!bFirstDesignTheme)
        	{
        		bFirstDesignTheme = true;
        		familyGroups.put("designTheme", "true");
        	}
      	}
        Iterator pgiterator = (fgchild.getChildren("productGroup").iterator());

        while (pgiterator.hasNext())
        {
          Element pgchild = (Element)pgiterator.next();
          String productGroupOnly =  pgchild.getTextTrim();
        	String productGroup = productGroupOnly;
        	if (dtList != null && dtList.length > 0)
        	{
        		for (int j = 0; j < dtList.length; j++)
        		{
        			productGroup = productGroupOnly + dtList[j];
          		familyGroups.put(productGroup, name);
        			//System.out.println("key=" + productGroup + " name=" + name);
        		}
        	}
        	else
        	{
        		familyGroups.put(productGroup, name);
          	//System.out.println("key=" + productGroup + " name=" + name);
        	}
        }
      }
  	}
  	catch (Exception e)
  	{}
  	
  	return familyGroups; 	
  }
  
  static void logBaseFamilyGroups()
  {
  	try
  	{
    	String path = new File(".").getCanonicalPath();
      File folder = new File(path + "/FamilyGroups");
      File logFile = new File(folder, "FamilyGroupsBaseProducts.log");
      
  		File fgf = Config.familyGroupFile;
  		SAXBuilder builder = null;
  		builder = new SAXBuilder(false);
  		
      PrintWriter log = new PrintWriter(new FileOutputStream(logFile));
  		log.println("Base Product Group\tDesign Themes (optional)");
		
  		Element root = builder.build(fgf).getRootElement();  
    	String rootDesignTheme = root.getAttributeValue("designTheme");
    	
      Iterator fgiterator = (root.getChildren("familyGroup").iterator());
      while (fgiterator.hasNext())
      {
        Element fgchild = (Element)fgiterator.next();
      	String name = fgchild.getAttributeValue("name");
      	String designTheme = fgchild.getAttributeValue("designTheme");
      	if (rootDesignTheme != null)
      	{
      		designTheme = rootDesignTheme;
      	}
      	else
      	{
      		if (designTheme == null)
      			designTheme = "";
      	}
      	
        Iterator pgiterator = (fgchild.getChildren("productGroup").iterator());

        int count = 0;
        while (pgiterator.hasNext())
        {
          Element pgchild = (Element)pgiterator.next();
          String productGroupOnly =  pgchild.getTextTrim();
        	String productGroup = productGroupOnly;
        	
    			if (count == 0)
    			{
    				System.out.println("key=" + productGroupOnly + " designThemes=" + designTheme);
    	  		log.println(productGroupOnly + "\t\t" + designTheme);
    			}
        	
        	count++;
        }
      }
      // close log
      log.flush();
      log.close();
  	}
  	catch (Exception e)
  	{}
  }
  
  // JPanel for user input dialog
  static private class SelectorPanel extends JPanel
  {   
    //JComboBox sourceProductGroupComboBox = new JComboBox();
    DirectoryPanel inputFolderPanel;
    //JComboBox targetProductGroupComboBox = new JComboBox();
    DirectoryPanel outputFolderPanel;
    
    SelectorPanel(String inputs, String outputs)
    throws JDOMException, IOException
    {        
      inputFolderPanel = new DirectoryPanel("Input Folder:", inputs);
      outputFolderPanel = new DirectoryPanel("Output Folder:", outputs);
      
      /*Iterator iterator = Predesigner.getProductGroupList().iterator();
      while (iterator.hasNext())
      {
        ProductGroup productGroup = (ProductGroup)iterator.next();
        sourceProductGroupComboBox.addItem(productGroup);
        //targetProductGroupComboBox.addItem(productGroup);
      }*/
     
      setLayout(new GridBagLayout());
      GridBagConstraints constraints = new GridBagConstraints();
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      add(new JLabel("Use the design content from one product's predesigns" +
                     " to generate new predesigns for a different product."), constraints);
      constraints.fill = GridBagConstraints.HORIZONTAL;
      constraints.insets = new Insets(10, 10, 0, 10);
      constraints.gridwidth = 1;     
      add(new JLabel("Use all predesigns from :"), constraints);
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      //add(sourceProductGroupComboBox, constraints);
      add(inputFolderPanel, constraints);
      constraints.gridwidth = 1;
      add(new JLabel("Generate predesigns for :"), constraints);
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      //add(targetProductGroupComboBox, constraints);
      add(outputFolderPanel, constraints);
    }
    
    //ProductGroup getSourceProductGroup()  {
      //return (ProductGroup)sourceProductGroupComboBox.getSelectedItem();
    //}
    
    //ProductGroup getTargetProductGroup() {
      //return (ProductGroup)targetProductGroupComboBox.getSelectedItem();
    //}
  }
}
  


