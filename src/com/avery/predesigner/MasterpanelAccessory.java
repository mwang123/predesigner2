/*
 * ImageAccessory.java Created on Mar 18, 2005 by leeb Copyright 2005 Avery
 * Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

import org.jdom.input.SAXBuilder;

import com.avery.Averysoft;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryProject;

/**
 * @author http://java.sun.com/developer/JDCTechTips/2004/tt0316.html
 */
public class MasterpanelAccessory extends JLabel implements PropertyChangeListener
{
	private static final long serialVersionUID = -1929483482112615518L;
	private static final int PREFERRED_WIDTH = 150;
	private static final int PREFERRED_HEIGHT = 150;

	public MasterpanelAccessory(JFileChooser chooser)
	{
		setVerticalAlignment(JLabel.CENTER);
		setHorizontalAlignment(JLabel.CENTER);
		chooser.addPropertyChangeListener(this);
		setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
	}
	
	/**
	 * @param changeEvent (looking for JFileChooser.SELECTED_FILE_CHANGED_PROPERTY)
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent changeEvent)
	{
		String changeName = changeEvent.getPropertyName();
		if (changeName.equals(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY))
		{
			File file = (File) changeEvent.getNewValue();
			if (file != null)
			{
				Image image = getMasterpanelImage(file);
				setIcon(image == null ? null : new ImageIcon(image));
			}
		}
	}
	
	private Image getMasterpanelImage(File file)
	{
    try
    {
      SAXBuilder builder = new SAXBuilder(true);
  		builder.setFeature("http://apache.org/xml/features/validation/schema", true);
  		builder.setProperty(
  				"http://apache.org/xml/properties/schema/external-schemaLocation",
  				Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 

      org.jdom.Document document = builder.build(file);
      org.jdom.Element element =  document.getRootElement();

      // if it's not a masterpanel file, use the first masterpanel
      if (element.getName().equals("Avery.project"))
      {
        element = element.getChild("Avery.masterpanel");
      }
      else if (element.getName().equals("project"))
      {
        element = element.getChild("masterpanel", AveryProject.getAverysoftNamespace());
      }
      
      if (element == null)
      {
      	throw new Exception("no masterpanel found in " + file.getAbsolutePath());
      }

      AveryMasterpanel master = new AveryMasterpanel(element);
      int errors = master.fixGalleries(Config.getDefaultImageGallery());
      if (errors != 0)
      {
      	errors = master.fixGalleries(Config.getProjectHome());
      	if (errors != 0)
      	{
      		errors = master.fixGalleries(file.getParent());
      		if (errors != 0)
      		{
      			throw new Exception("Couldn't find images for " + file.getAbsolutePath());
      		}
      	}
      }
      
      // calculate scaling factor for aspect-correct size
      double Xscale = (double)PREFERRED_WIDTH / master.getWidth().doubleValue();
      double Yscale = (double)PREFERRED_HEIGHT / master.getHeight().doubleValue();
      double scale = (Xscale < Yscale) ? Xscale : Yscale;
      int width = (int)((scale * master.getWidth().doubleValue()) + 0.5);
      int height = (int)((scale * master.getHeight().doubleValue()) + 0.5);

      // create the BufferedImage
      BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D graphics = image.createGraphics();
      graphics.setBackground(Color.white);
      graphics.clearRect(0, 0, width, height);
      master.draw(graphics, width, height, master);
      return image;
    }
    catch (Exception ex)
    {
    	System.err.println(ex.getMessage());
    	return null;
    }		
	}
}