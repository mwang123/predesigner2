package com.avery.predesigner;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

// this one supports multiply selected fields
public class FieldCenterAllAction extends UndoableAction
{
  private static int CENTER_BOTH = 0;
  private static int CENTER_HORZ = 1;
  private static int CENTER_VERT = 2;

  protected ArrayList currentSelections = new ArrayList();
  protected AveryMasterpanel panel = null;
  protected ArrayList originalPositions = new ArrayList();
  protected int centerStyle = 0;
  
  public FieldCenterAllAction(AveryMasterpanel panel, ArrayList currentSelections, int centerStyle)
  {
    super("Undo Center All");
    this.currentSelections.addAll(currentSelections);
    this.panel = panel;
    this.centerStyle = centerStyle;
  }

  void execute()
  {
		for (int i = 0; i < currentSelections.size(); i++)
		{
			int selection = (int)currentSelections.get(i);
			AveryPanelField field = (AveryPanelField)(panel.getPanelFields()).get(selection);
	    Rectangle bounds = field.getBoundingRect();
	    double centerX = ((double)bounds.x) + (((double)bounds.width) / 2.0);
	    double centerY = ((double)bounds.y) + (((double)bounds.height) / 2.0);
	    
	    double panelCenterX = panel.getWidth().doubleValue() / 2.0;
	    double panelCenterY = panel.getHeight().doubleValue() / 2.0;
	    
			originalPositions.add(field.getPosition());
	    Point newPosition = new Point();
	    AffineTransform aft = AffineTransform.getTranslateInstance(panelCenterX - centerX, panelCenterY - centerY);
	    if (centerStyle == CENTER_HORZ)
	    	aft = AffineTransform.getTranslateInstance(panelCenterX - centerX, 0);
	    else if (centerStyle == CENTER_VERT)
	    	aft = AffineTransform.getTranslateInstance(0, panelCenterY - centerY);
	    aft.transform(field.getPosition(), newPosition);
			field.setPosition(newPosition);
			
		}
  }

  void undo()
  {
		for (int i = 0; i < currentSelections.size(); i++)
		{
			int selection = (int)currentSelections.get(i);
			AveryPanelField field = (AveryPanelField)panel.getPanelFields().get(selection);
			Point originalPosition = (Point)originalPositions.get(i);
			field.setPosition(originalPosition);
		}
    Predesigner.getFrame().updateView();
  }
}
