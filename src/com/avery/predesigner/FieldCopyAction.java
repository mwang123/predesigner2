/*
 * FieldCopyAction.java Created on May 16, 2005 by Bob Lee
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import com.avery.project.AveryPanelField;

/**
 * @author Bob lee
 */
class FieldCopyAction extends UndoableAction
{
  private AveryPanelField field;	
  private Object oldHold;
  
	public FieldCopyAction(AveryPanelField field)
	{
		super("Undo Copy");
    this.field = (AveryPanelField)field.clone();
    oldHold = Predesigner.getFrame().getHeldObject();
	}

	/* (non-Javadoc)
	 * @see com.avery.predesigner.UndoableAction#execute()
	 */
	void execute()
	{
    Predesigner.getFrame().setHeldObject(field);
    Predesigner.getFrame().updateView();
	}

	/* (non-Javadoc)
	 * @see com.avery.predesigner.UndoableAction#undo()
	 */
	void undo()
	{
    Predesigner.getFrame().setHeldObject(oldHold);
    Predesigner.getFrame().updateView();
	}
}
