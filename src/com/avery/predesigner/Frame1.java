/**
 * Title:         Frame - Avery Project Editor Frame
 * Copyright:     Copyright (c)2001-2003 Avery Dennison Corp. All Rights Reserved
 * Company:       Avery Dennison
 * @author        Bob Lee
 * @version       1.8
 */

package com.avery.predesigner;

import java.awt.AWTEvent;
//import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.io.File;
import java.net.URL;
import java.util.Iterator;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
//import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPage;
import com.avery.project.AveryPanelField;
import com.avery.project.AveryProject;


public class Frame1 extends JFrame
{
	private static final long serialVersionUID = -7337974299020102655L;
	private JMenuBar menuBar = new JMenuBar();
  private FileMenu fileMenu = new FileMenu();
  private JMenu editMenu = new EditMenu();
  private JMenu viewMenu = new JMenu("View");
  private AddMenu addMenu = new AddMenu();
  private FieldsMenu fieldsMenu = new FieldsMenu();
  private JMenu toolsMenu = new ToolsMenu();
  private JMenu helpMenu = new JMenu("Help");
  private JMenu viewMasterpanelPopup = new JMenu("Masterpanel");
  private JMenu viewPagePopup = new JMenu("Page");
  private JCheckBoxMenuItem viewPanelInfoMenuItem = new JCheckBoxMenuItem("Panel Info", false);
  private JCheckBoxMenuItem viewOutlinesMenuItem = new JCheckBoxMenuItem("Outlines", true);
  private JCheckBoxMenuItem viewTextDefaultsMenuItem = new JCheckBoxMenuItem("TextDefault Outlines", false);
  private JCheckBoxMenuItem viewSafeMenuItem = new JCheckBoxMenuItem("Show Safe Lines", true);
  private JCheckBoxMenuItem viewQuickTextItem = new JCheckBoxMenuItem("Show Quick Text", true);
  private JCheckBoxMenuItem viewPromptOrderMenuItem = new JCheckBoxMenuItem("Prompt Order", false);
  private JMenuItem zoomInMenuItem = new JMenuItem("Zoom In");
  private JMenuItem zoomOutMenuItem = new JMenuItem("Zoom Out");
  private JMenuItem helpDocHelpMenuItem = new JMenuItem("Predesigner Help...");
  private JMenuItem helpAboutMenuItem = new JMenuItem("About...");
  private JMenuItem status = new JMenuItem("");
  
  // group of objects designed to work inside the scrollpane
  // scrollPane is attached to frame
  private JScrollPane scrollPane;
  // contentPane is the view of the scrollPane
  private ContentPanel contentPane;
  // add menu items toolbar
  private AddToolBar addToolBar = new AddToolBar(JToolBar.VERTICAL);
  // cards panel will contain cardLayout
  private JPanel cards;
  // individual card panels
  private JPanel card1;
  private JPanel card2;
  private CardLayout cardLayout = new CardLayout();
  // views placed in their own card
  private MasterpanelView masterpanelView = new MasterpanelView(null);
  private PageView pageView = new PageView(null);
  
  private MasterpanelToolbar masterpanelToolbar = new MasterpanelToolbar();

  
  // a fake local clipboard
  private Object heldObject;

  /**Construct the frame*/
  public Frame1(String frameName) 
  {
	  super(frameName);
  }
  
  public void createFrame()
  {
  	enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
      URL iconURL = getClass().getResource("OPNAICON32.gif");
      //setIconImage(Toolkit.getDefaultToolkit().getImage(iconURL));
  	}
    catch(Exception e)
    {
      displayErrorMessage(e.toString());
      e.printStackTrace();
    }
	  
  }

 /**
   * Component initialization
   * @throws Exception
   */
  private void jbInit() throws Exception
  {
    // this is CTRL on PCs.
    int shortcutMask = java.awt.Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    //setIconImage(Toolkit.getDefaultToolkit().createImage(Frame1.class.getResource("[Your Icon]")));
    this.setSize(new Dimension(640, 480));
    this.setTitle("Averysoft Predesigner2");
    
    viewMasterpanelPopup.setMnemonic('M');
    viewPagePopup.setMnemonic('P');

    viewPanelInfoMenuItem.setMnemonic('I');
    viewPanelInfoMenuItem.setAccelerator(KeyStroke.getKeyStroke('I', shortcutMask, false));
    viewPanelInfoMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onViewPanelInfo();
      }
    });
    
    viewOutlinesMenuItem.setMnemonic('O');
    viewOutlinesMenuItem.setAccelerator(KeyStroke.getKeyStroke('L', shortcutMask, false));
    viewOutlinesMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onViewOutlines();
      }
    });
    
    viewTextDefaultsMenuItem.setMnemonic('T');
    viewTextDefaultsMenuItem.setAccelerator(KeyStroke.getKeyStroke('T', shortcutMask, false));
    viewTextDefaultsMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onViewTextDefaultOutlines();
      }
    });
    
    //viewBleedSafeMenuItem.setMnemonic('T');
    //viewBleedSafeMenuItem.setAccelerator(KeyStroke.getKeyStroke('T', shortcutMask, false));
    viewSafeMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onViewSafe();
      }
    });
    
    //viewQuickTextMenuItem.setMnemonic('T');
    //viewQuickTextItem.setAccelerator(KeyStroke.getKeyStroke('T', shortcutMask, false));
    viewQuickTextItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onViewQuickText();
      }
    });
   viewPromptOrderMenuItem.setMnemonic('P');
    viewPromptOrderMenuItem.setAccelerator(KeyStroke.getKeyStroke('P', shortcutMask, false));
    viewPromptOrderMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onViewPromptOrder();
      }
    });
    
		zoomInMenuItem.setMnemonic('=');
		zoomInMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_EQUALS, shortcutMask, false));
		//zoomInMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ADD, 0, false));
    zoomInMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onZoomIn();
      }
    });

		zoomOutMenuItem.setMnemonic('-');
		zoomOutMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_MINUS, shortcutMask, false));
//		zoomOutMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_SUBTRACT, 0, false));
    zoomOutMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onZoomOut();
      }
    });

    helpMenu.setActionCommand("About...");
    helpMenu.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onHelpAbout();
      }
    });

    helpDocHelpMenuItem.setMnemonic('H');
    helpDocHelpMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onHelpDocHelp();
      }
    });
    
    helpAboutMenuItem.setMnemonic('A');
    helpAboutMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onHelpAbout();
      }
    });

    fileMenu.setMnemonic('F');
    menuBar.add(fileMenu);
    
    editMenu.setMnemonic('E');
    menuBar.add(editMenu);
    
    viewMenu.setMnemonic('V');
    menuBar.add(viewMenu);
    
    addMenu.setMnemonic('A');
    menuBar.add(addMenu);
    
    fieldsMenu.setMnemonic('I');
    menuBar.add(fieldsMenu);
    
    toolsMenu.setMnemonic('T');
    menuBar.add(toolsMenu);
    
    helpMenu.setMnemonic('H');
    menuBar.add(helpMenu);
    
    menuBar.add(status);
    
    this.setJMenuBar(menuBar);

    viewMenu.add(viewMasterpanelPopup);
    viewMenu.add(viewPagePopup);
    viewMenu.addSeparator();
    viewMenu.add(viewOutlinesMenuItem);
    viewMenu.add(viewTextDefaultsMenuItem);
    viewMenu.add(viewSafeMenuItem);
    viewMenu.add(viewQuickTextItem);
    viewMenu.add(viewPanelInfoMenuItem);
    viewMenu.add(viewPromptOrderMenuItem);
    viewMenu.addSeparator();
    viewMenu.add(zoomInMenuItem);
    viewMenu.add(zoomOutMenuItem);
    viewMenu.addMenuListener(new ViewMenuListener());

    helpMenu.add(helpDocHelpMenuItem);
    helpMenu.add(helpAboutMenuItem);


    // add views inside a scroll pane
    addScrollableViews();

    this.add(masterpanelToolbar, BorderLayout.SOUTH);
    
    // handle resizing of the frame
    this.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentResized(ComponentEvent e)
      {
        if (pageView.isVisible() == true)
        {
          pageView.setVisible(false);
          viewPage(pageView.getPageNumber());
        }
      }
    });
    
    this.addKeyListener(masterpanelView);
  }

  /**
   * MenuListener.menuSelected disables commands that are inappropriate for the
   * current state of the program.
   */
  class ViewMenuListener implements MenuListener
  {
    public void menuCanceled(MenuEvent event)
    {   }
    public void menuDeselected(MenuEvent event)
    {   }
    public void menuSelected(MenuEvent event)
    {
      boolean hasProject = (Predesigner.getProject() != null);

      viewPanelInfoMenuItem.setEnabled( hasProject );
      viewPromptOrderMenuItem.setEnabled( hasProject );
    }
  }
  
  private void addScrollableViews()
  {
    // add the views
    contentPane = new ContentPanel();
    contentPane.setLayout(new BorderLayout());

    // create the panel containing the card views inside cardLayout
    cards = new JPanel(cardLayout);

    // masterpanelView card
    card1 = new JPanel();
    card1.setLayout(new GridBagLayout());
    //card1.add(masterpanelView);
    //card1.setBackground(Color.blue);

    // constraints provide size of the cards
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.gridx = 0;
    constraints.gridy = 0;
    constraints.gridwidth = 0;
    constraints.gridheight = 0;
    constraints.ipadx = 0;
    constraints.ipady = 0;

    // initially the cards are zero size
    card1.add(masterpanelView, constraints);

    // pageView card
    card2 = new JPanel();
    card2.setLayout(new GridBagLayout());
    card2.add(pageView, constraints);
    
    //JScrollPane scrollPane = new JScrollPane(pageView);
    //setPreferredSize(new Dimension(450, 110));
    //card2.add(scrollPane, BorderLayout.CENTER);
    //this.add(scrollPane, BorderLayout.CENTER)

    // add each card to the cards panel
    // note that this JPanel.add syntax notifies the cardLayout as well
    cards.add(card1, "masterpanelView");
    cards.add(card2, "pageView");

    // visibility state will determine view to hide and show later
    masterpanelView.setVisible(true);
    // pageView.setVisible(false);

    // show your cards
    cardLayout.show(cards, "masterpanelView");

    // display the views centered at the top of the scroll pane
    contentPane.add(cards, BorderLayout.NORTH);

    addToolBar.setMaximumSize(addToolBar.getSize());
    getContentPane().add(addToolBar, BorderLayout.WEST);

    // attach scrollPane to frame
    scrollPane = new JScrollPane(contentPane);
    getContentPane().add(scrollPane, BorderLayout.CENTER);

    // disable arrow keys or they will scroll the scrollpane!
    scrollPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
      put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "none");
    scrollPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
      put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "none");
    scrollPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
      put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "none");
    scrollPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
      put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "none");

    // display a reasonable blank scrollPane until a project is loaded
    resizeCard1(480, 360);
    resizeCard2(480, 360);
    

  }

  // sets size of the masterpanelView and redraws the scroll pane
  void resizeCard1(int width, int height)
  {
    // get the existing constraints
    GridBagLayout gbl = (GridBagLayout)card1.getLayout();
    GridBagConstraints constraints = gbl.getConstraints(masterpanelView);

    // update the dimension constraints
    constraints.ipadx = width;
    constraints.ipady = height;
    gbl.setConstraints(masterpanelView, constraints);

    card1.doLayout();

    // dirty the scrollPane and then force a paint
    scrollPane.invalidate();
    scrollPane.validate();
  }

  // sets size of the masterpanelView and redraws the scroll pane
  void resizeCard2(int width, int height)
  {
  	//width = card2.getWidth() * 2;
  	//height = card2.getHeight() * 2;
    // get the existing constraints
    GridBagLayout gbl = (GridBagLayout)card2.getLayout();
    GridBagConstraints constraints = gbl.getConstraints(pageView);

    // update the dimension constraints
    constraints.ipadx = width;
    constraints.ipady = height;
    gbl.setConstraints(pageView, constraints);

    card2.doLayout();

    // dirty the scrollPane and then force a paint
    scrollPane.invalidate();
    scrollPane.validate();
  }
  
  /**
   * the project being edited
   */
  static private Project project = null;

  /**
   * Sets the current project to the specified project.  This includes a
   * reset of the ApeViews and a rebuild of the dynamic menu items.
   * @param project - the project
   */
  void setProject(Project project)
  {
    Frame1.project = project;

    // assign new project to the views
    masterpanelView.setProject(getProject());
    pageView.setProject(getProject());

    // rebuild the menus
    rebuildMasterpanelPopupMenu(project);
    rebuildPagePopupMenu(project);
    
    reconstructTitle();
    
    // refresh the view
    contentPane.invalidate();
    contentPane.repaint();
    
    viewMasterpanel(0);

  }

  void setProjectInvisibly(Project project)
  {
    Frame1.project = project;

    // assign new project to the views
    masterpanelView.setProject(getProject());
    masterpanelView.setVisible(false);
    //pageView.setProject(getProject());

    // rebuild the menus
    //rebuildMasterpanelPopupMenu(project);
    //rebuildPagePopupMenu(project);
    
    //reconstructTitle();
    
    // refresh the view
    //contentPane.invalidate();
   // contentPane.repaint();
    
    //viewMasterpanel(0);
  }

  /**
   * @return the current AveryProject
   */
  static Project getProject()
  {
    return project;
  }
  
  public AddMenu getAddMenu()
  {
  	return addMenu;
  }
  
  public FieldsMenu getFieldsMenu()
  {
  	return fieldsMenu;
  }
  
  public MasterpanelToolbar getMasterpanelToolbar()
  {
  	return masterpanelToolbar;
  }
  
  void reconstructTitle()
  {
    String title = "Averysoft Predesigner2 - " + getProject().getName();
    if (getProject().isModified())
    {
      title += "*";
    }
    setTitle(title);
  }

  /**
   * Help | About action performed
   */
  private void onHelpAbout()
  {
    PredesignerAboutBox dlg = new PredesignerAboutBox(this);
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
    dlg.setModal(true);
    dlg.setVisible(true);
  }

  /**
   * Overridden so we can exit when window is closed
   * @param e -
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      exitApe(0);
    }
    
    try
    {
      super.processWindowEvent(e);
    }
    catch (Exception ex)
    {
      displayErrorMessage(ex.toString());
    }
  }


  /**
   * Error message dialog
   * @param errorText - the text to display
   */
  void displayErrorMessage(String errorText)
  {
    JOptionPane.showMessageDialog(this, errorText, "Error", JOptionPane.ERROR_MESSAGE);
  }

  /**
   * Called when the project changes, this populates the View/Masterpanel
   * popup menu with the masterpanels of the new project
   * @param project - the current project
   */
  void rebuildMasterpanelPopupMenu(AveryProject project)
  {
    viewMasterpanelPopup.removeAll();

    masterpanelToolbar.clear();

    int i = 0;
    Iterator iterator = project.getMasterpanels().iterator();
    while (iterator.hasNext())
    {
      AveryMasterpanel mp = (AveryMasterpanel)iterator.next();
      JMenuItem item = new JMenuItem(mp.getDescription());
      final int mpIndex = i++;
      masterpanelToolbar.addMasterPanelButton(mp.getDescription(), mpIndex);
      item.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          viewMasterpanel(mpIndex);
        }
      });

      viewMasterpanelPopup.add(item);
    }

    // masterpanel is 0th element in viewMenu
  	viewMenu.remove(0);
  	 
    if (i == 1)	// only one masterpanel found
    {
    	viewMenu.insert(viewMasterpanelPopup.getItem(0), 0);
    }
    else
    {
    	viewMenu.insert(viewMasterpanelPopup, 0);
    }
    
    masterpanelToolbar.addTextEditPanel();
    masterpanelToolbar.repaint();
    
  }

  /**
   * Called when the project changes, this populates the View/Page
   * popup menu with an entry for each page of the project.
   * @param project - the current project
   */
  void rebuildPagePopupMenu(Project project)
  {
    viewPagePopup.removeAll();
    if (project.isTiledProject())
    {
    	// no page views of tiled projects
    	JMenuItem noPage = new JMenuItem("Page view");
    	noPage.setEnabled(false);
      // pageView is 1th element in viewMenu
    	viewMenu.remove(1);
    	viewMenu.insert(noPage, 1);
    	return;
    }

    for (int i = 1; i <= project.getPages().size(); ++i)
    {
    	String name = "Page " + i;
    	String description = ((AveryPage)project.getPages().get(i - 1)).getDescription();
    	
      JMenuItem item = new JMenuItem(name + " (" + description + ")");
      
      final int pageNumber = i;
      item.addActionListener(new java.awt.event.ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            viewPage(pageNumber);
          }
        });

      viewPagePopup.add(item);
    }
    
    // pageView is 1th element in viewMenu
  	viewMenu.remove(1);
  	 
    if (project.getPages().size() == 1)	// only one page in project
    {
    	viewMenu.insert(viewPagePopup.getItem(0), 1);
    }
    else
    {
    	viewMenu.insert(viewPagePopup, 1);
    }
  }

  void onFieldChanged()
  {
    project.setModified(true);
    masterpanelView.buildFieldSelectionTools();
    contentPane.invalidate();
    contentPane.repaint();
  }
  
  void updateView(boolean rebuildSelectionTools)
  {
    if (rebuildSelectionTools)
    {
      masterpanelView.buildFieldSelectionTools();
    }
    contentPane.invalidate();
    contentPane.repaint();
  }
  
  void updateView()
  {
    updateView(masterpanelView.isVisible());
  }
  
  public void viewMasterpanel(int index)
  {
    masterpanelToolbar.clearText();
    masterpanelView.setCurrentMasterpanelIndex(index);

    scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    cardLayout.show(cards, "masterpanelView");
    pageView.setVisible(false);
    masterpanelView.setVisible(true);
    masterpanelView.invalidate();
    masterpanelView.updateStatusDisplay();
    onViewPromptOrder();
    masterpanelView.invalidate();
  }

  private void viewPage(int pageNumber)
  {
    pageView.setPageNumber(pageNumber);

    // get the existing constraints
    GridBagLayout gbl = (GridBagLayout)card2.getLayout();
    GridBagConstraints constraints = gbl.getConstraints(pageView);
    
    // update the dimension constraints
    Container clientArea = this.getContentPane();
    constraints.ipadx = clientArea.getWidth();
    // magic number 14 required to adjust client size down for client borders?
    constraints.ipady = clientArea.getHeight() - 14;
    gbl.setConstraints(pageView, constraints);
    card2.doLayout();

    scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    cardLayout.show(cards, "pageView");
    masterpanelView.setVisible(false);
    pageView.setVisible(true);
    pageView.invalidate();
    pageView.updateStatusDisplay();
  }

  void exitApe(int errorcode)
  {
    fileMenu.promptSaveModified();
    Config.write();
    System.exit(errorcode);
  }

  File getProjectFile()
  {
    return project.getFile();
  }

  void onViewPanelInfo()
  {
	  updatePanelInfo();
  }
    
  void updatePanelInfo()
  {
	  showPanelInfo( viewPanelInfoMenuItem.getState() );
  }
  
  void showPanelInfo(boolean show)
  {
    pageView.showPanelInfo( show );
    
    // ensure if appropriate menu item has correct state
    viewPanelInfoMenuItem.setState( show );
  }

  void onViewOutlines()
  {
    masterpanelView.enableOutlines(viewOutlinesMenuItem.getState());
  }
  
  void onViewTextDefaultOutlines()
  {
    masterpanelView.enableTextDefaultOutlines(viewTextDefaultsMenuItem.getState());
  }
  
  void onViewSafe()
  {
    masterpanelView.enableSafe(viewSafeMenuItem.getState());  	
  }
  
  void onViewQuickText()
  {
  	masterpanelView.enableQuickText(viewQuickTextItem.getState());
  }
  
  void onViewPromptOrder()
  {
    masterpanelView.displayPromptOrder(viewPromptOrderMenuItem.getState());
  }

  boolean allowMasterpanelEditing()
  {
    return (project != null) && masterpanelView.isVisible();
  }

  public AveryMasterpanel getCurrentMasterpanel()
  {
    return masterpanelView.getCurrentMasterpanel();
  }
  
  public MasterpanelView getMasterpanelView()
  {
  	return masterpanelView;
  }
  
  void setCurrentMasterpanel(int index)
  {
  	masterpanelView.setCurrentMasterpanelIndex(index);
  }
  
  PageView getPageView()
  {
  	return pageView;
  }
  
  public JPanel getContentPanel()
  {
  	return this.contentPane;
  }
  
  boolean allowDelete()
  {
    return (project != null) && masterpanelView.isVisible() && masterpanelView.hasSelection();
  }
  
  void onDelete()
  {
    if (allowDelete())
    {
      masterpanelView.deleteSelection();
    }
  }
  
  boolean allowCenter()
  {
    return (project != null) && masterpanelView.isVisible() && masterpanelView.hasSelection();
  }
  
  void onCenter()
  {
    if (allowCenter())
    {
      masterpanelView.centerSelection();
    }
  }
  
  void onCenterHorz()
  {
    if (allowCenter())
    {
      masterpanelView.centerSelectionHorz();
    }
  }
  
  void onCenterVert()
  {
    if (allowCenter())
    {
      masterpanelView.centerSelectionVert();
    }
  }

  void onAlignLeft()
  {
    //if (allowCenter())
    {
      masterpanelView.alignLeft();
    }
  }
  
  void onAlignRight()
  {
    //if (allowCenter())
    {
    	masterpanelView.alignRight();
    }
  }

  void onAlignTop()
  {
    //if (allowCenter())
    {
    	masterpanelView.alignTop();
    }
  }

  void onAlignBottom()
  {
    //if (allowCenter())
    {
    	masterpanelView.alignBottom();
    }
  }

  void onAlignWidth()
  {
    //if (allowCenter())
    {
    	masterpanelView.alignWidth();
    }
  }

  void onAlignHeight()
  {
    //if (allowCenter())
    {
    	masterpanelView.alignHeight();
    }
  }

  /**
   * @return <code>true</code> if the view is in a state that's compatible with
   * the clipboard Cut and Copy operations.
   */
  boolean allowCutCopy()
  {
    return (project != null) && masterpanelView.isVisible() && masterpanelView.hasSelection();
  }
  
  /**
   * @return <code>true</code> if the view is in a state where a clipboard Paste
   * would be possible.
   */
  boolean allowPaste()
  {
  	boolean b = (getHeldObject() != null)
			&& (getHeldObject() instanceof AveryPanelField);
  		
    return b && (project != null) && masterpanelView.isVisible();
  }
  
  void onZoomIn()
  {
    if (project != null)
    {
    	if (masterpanelView.isVisible())
    	{
    		masterpanelView.zoomIn();
    		contentPane.invalidate();
    		contentPane.repaint();
    	}
    	else if (pageView.isVisible())
    	{
    		pageView.zoomIn();
    		contentPane.invalidate();
    		contentPane.repaint();
    	}
    }
  }
  
  void onZoomOut()
  {
    if (project != null)
    {
    	if (masterpanelView.isVisible())
    	{
    		masterpanelView.zoomOut();
    		contentPane.invalidate();
    		contentPane.repaint();
    	}
    	else if (pageView.isVisible())
    	{
    		pageView.zoomOut();
    		contentPane.invalidate();
    		contentPane.repaint();
    	}
    }
  }
  
  void updateStatus(String status)
  {
  	this.status.setText(status);
  }
  
  
  void onCut()
  {
    if (allowCutCopy())
    {
      masterpanelView.cutSelection();
    }
  }
  
  void onCopy()
  {
    if (allowCutCopy())
    {
      masterpanelView.copySelection();
    }
  }
  
  void onPaste()
  {
  	if (allowPaste())
  	{
      masterpanelView.paste();
  	}
  }
  
  void onCopyAll(JMenuItem pasteAllMenuItem)
  {
  	masterpanelView.copyAll(pasteAllMenuItem);
  }
  
  void onPasteAll()
  {
  	masterpanelView.pasteAll();
  }
  

  // a fake clipboard
  Object getHeldObject()
  {
  	return heldObject;
  }
  
  void setHeldObject(Object o)
  {
  	heldObject = o;
  }
  
  void clearHeldObject()
  {
  	heldObject = null;
  }
  
  public void clearSelection()
  {
  	masterpanelView.clearSelection();
  }
  
  private JEditorPane helpPane;
  
  /**
   * Help | Online Help... action performed
   */
  private void onHelpDocHelp()
  {
    try
    {
    	if (Desktop.isDesktopSupported())
      {
      	String curPath = new File(".").getAbsolutePath();
      	curPath = curPath.substring(0, curPath.length()-1);

        Desktop.getDesktop().open(new File(curPath + "pd2.pdf"));  //"pd2.docx"
        
      }/*      helpPane = new JEditorPane("http://software.averytest.com/Predesigner4/Help/index.html");
      helpPane.setEditable(false);
      helpPane.addHyperlinkListener(new HyperlinkListener() {
        public void hyperlinkUpdate(HyperlinkEvent event) 
        { 
          try  
          { 
            if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
            {
              helpPane.setPage(event.getURL());
            }
          }
          catch(IOException iox) 
          {
            displayErrorMessage(iox.toString());
          }
        }
      });

      JDialog dlg = new JDialog(this, "Predesigner2 Help", false);
      dlg.setBounds(getLocation().x, getLocation().y, 660, 500);
      dlg.getContentPane().add(new JScrollPane(helpPane));
      dlg.setVisible(true);*/
    }
    catch (Exception e)
    { 
      displayErrorMessage(e.toString());
    }
  }
  
  class ContentPanel extends JPanel
  {
    public void paint(Graphics g)
    {
      super.paint(g);
      clipContent(g);
    }
    
    void clipContent(Graphics g)
    {
    	if (masterpanelView.isVisible())
    	{
    		Rectangle rectContent = this.getVisibleRect();
    		Rectangle rectMasterView = masterpanelView.getVisibleRect();
    		
        GeneralPath viewPath = new GeneralPath(rectContent);
        Area area1 = new Area(viewPath);
        Area area2 = new Area(rectMasterView);
        // translate masterShape
        int area2x = (rectContent.width - rectMasterView.width) / 2;
        area2.transform(new AffineTransform(1, 0, 0, 1, area2x, 0));
        area1.subtract(area2);
        GeneralPath gp = new GeneralPath(area1);
        g.setClip(gp);
        Color clipColor = new Color(0xef, 0xef, 0xef, 0x7f);
        g.setColor(clipColor);
        Graphics2D g2d = (Graphics2D)g;        
        g2d.fill(viewPath);     		
    	}
    }

  }

}