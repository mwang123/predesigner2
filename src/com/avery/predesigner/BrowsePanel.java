/*
 * BrowsePanel.java Created on Dec 5, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author leeb
 * Dec 5, 2007
 * BrowsePanel
 */
abstract class BrowsePanel extends JPanel
{
  protected JLabel label = new JLabel();
  protected JTextField textField = new JTextField();
  protected JButton button = new JButton("Browse...");
  
  BrowsePanel(String description, String path)
  {
    label.setText(description);
    textField.setText(path);
    
    button.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent e)
      {
        browse();
      }
    });
    
    java.awt.BorderLayout layout = new java.awt.BorderLayout();
    layout.setHgap(6);
    this.setLayout(layout);
    this.add(label, "West");
    this.add(button, "East");    
    this.add(textField, "South");
  }
  
  /**
   * This is the action that takes place when the user clicks 
   * the "Browse..." button.  On success, it should call textField.setText
   * with the user-selected path.
   */
  abstract protected void browse();
}
