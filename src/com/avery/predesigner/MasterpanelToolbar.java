package com.avery.predesigner;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JToolBar;

import com.avery.predesigner.editors.TextEditPanel;

public class MasterpanelToolbar extends JToolBar
{
	private TextEditPanel textEditPanel = new TextEditPanel();
	
	public MasterpanelToolbar()
	{
		setFloatable(false);
		setRollover(true);
	}
	
	public void clear()
	{
		removeAll();
		addSeparator(new Dimension(50,0));
		this.repaint();
	}
	
	public void clearText()
	{
		textEditPanel.clearText();
	}
	
	public void addTextEditPanel()
	{
		add(textEditPanel);
	}
	
	public TextEditPanel getTextEditPanel()
	{
		return textEditPanel;
	}

	public void addMasterPanelButton(String text, int index)
	{
		final int mpIndex = index;
		JButton button = new JButton(text);
		button.setBorder(BorderFactory.createLineBorder(Color.black));
    add(button);
    addSeparator();
    
    button.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        Predesigner.getFrame().viewMasterpanel(mpIndex);
      }
    });

	}

}
