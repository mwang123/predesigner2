package com.avery.predesigner;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: UndoableAction centers a field horizontally within a masterpanel</p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

class FieldCenterHorzAction extends FieldCenterAction
{
  public FieldCenterHorzAction(AveryMasterpanel panel, AveryPanelField field)
  {
    super(panel, field);
    
    // newPosition has been centered in both X and Y.  We have to reset the Y.
    newPosition.setLocation(newPosition.getX(), originalPosition.getY());
  }
}