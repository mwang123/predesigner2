package com.avery.predesigner;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JOptionPane;

import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.avery.Averysoft;
import com.avery.predesign.StyleSet;
import com.avery.product.Appearance;
import com.avery.product.ProductGroup;
import com.avery.product.ProductList;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;
import com.avery.project.AveryProject;
import com.avery.product.ProductGroupList;
import com.avery.utils.MergeMap;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Predesigner's API to access the AveryProject class</p>
 * <p>Copyright: Copyright 2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

public class Project extends AveryProject
{
  private File file = null;
  private boolean modified = false;
  
  Project()
  { }

  void initFrom(File file) throws Exception
  {
  	read(file);
    this.file = file;
  }
  
  String getHomeDirectory()
  {
	  return file.getParent();
  }
  
  String getFileDirectory()
  {
	  return getHomeDirectory();
  }
  
  void read(File xmlfile)
  throws Exception
  {
  	SAXBuilder builder = null;
		if (AveryProject.outputMM)
		{
			builder = new SAXBuilder(false);
		}
		else
		{
			builder = new SAXBuilder(true);
			builder.setFeature("http://apache.org/xml/features/validation/schema", true);
			builder.setProperty(
				"http://apache.org/xml/properties/schema/external-schemaLocation",
				Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL());
		}
		
    super.initFromElement(builder.build(xmlfile).getRootElement());  
    
    // remove Averysoft fieldRefs - they will be regenerated at save time 
    if (this.hasMasterpanelFields()  || this.getAllPanelFields().size() == 0)
    	clearProjectFieldTable();
    
		try
		{
			ProductGroupList list = Predesigner.getProductGroupList();
			ProductGroup product = list.getProductGroup(getProductGroup());
			String customProof = product.getCustomProof();
			if (customProof != null && customProof.length() > 0)
			{
		    Iterator iMaster = this.getMasterpanels().iterator();
		    while (iMaster.hasNext())
		    {
		    	AveryMasterpanel master = (AveryMasterpanel)iMaster.next();
		    	master.setCustomProof(customProof);
		    }				
			}
		}
		catch (Exception e)
		{}
  }

  void setModified(boolean flag)
  {
    boolean wasModified = modified;
    modified = flag;
    if (wasModified != modified && this.equals(Predesigner.getProject()))
    {
      Predesigner.getFrame().reconstructTitle();
    }
  }

  boolean isModified()
  {
    return modified;
  }

  /**
   * Provides a name for the project, typically for a title bar.
   * @return the filename or, lacking a file, the ProductGroup string
   */
  String getName()
  {
    if (file != null)
    {
      return file.getName();
    }
    else if (getProductGroup() != null)
    {
      return getProductGroup();
    }
    else return new String();
  }

  /**
   * @return the File associated with this Project
   */
  File getFile()
  {
    return file;
  }
  
  void saveLegacy(File file)
  throws Exception
  {
    org.jdom.Element projectElement = getAveryElement();
    stripImagePaths(projectElement);

    Document document = new Document(projectElement,
      new DocType("Avery.project", Frame1.getProject().getSystemID()));

    FileOutputStream out = new FileOutputStream(file);
    new XMLOutputter(" ", true).output(document, out);
    out.flush();
    out.close();
    if (!file.equals(this.file))
    {
      this.file = file;
      Predesigner.getFrame().reconstructTitle();
    }
    setModified(false);
    setLegacyProject(true);
  }

  void write(File file)
  throws Exception
  {
    clearProjectFieldTable();
    initializePanelDataLinking();
   
    // do not remove panel field refs in DPO 7
 /*   if (hasPanelSetOptions())
    {
      // the arrangement of data in panel sets will be done in end user applications
      removePanelSets();
    }*/

    org.jdom.Element projectElement = getAverysoftElement();
    
    // set galleries to null for distribution in masterpanels
    Iterator i = projectElement.getChildren("masterpanel", Project.getAverysoftNamespace()).iterator();
    while (i.hasNext())
    {
      Element mpElement = (Element)i.next();
      Iterator j = mpElement.getChildren("image", Project.getAverysoftNamespace()).iterator();
      while (j.hasNext())
      {
      	((Element)j.next()).getAttribute("gallery").setValue("");
      }
      
      j = mpElement.getChildren("background", Project.getAverysoftNamespace()).iterator();
      while (j.hasNext())
      {
      	((Element)j.next()).getAttribute("gallery").setValue("");
      }
    }

    // set galleries to null for distribution in panels
    Iterator k = projectElement.getChildren("page", Project.getAverysoftNamespace()).iterator();
    while (k.hasNext())
    {
      Element pgElement = (Element)k.next();
    	i = pgElement.getChildren("panel", Project.getAverysoftNamespace()).iterator();
	    while (i.hasNext())
	    {
	      Element pElement = (Element)i.next();
	      Iterator j = pElement.getChildren("image", Project.getAverysoftNamespace()).iterator();
	      while (j.hasNext())
	      {
	      	Element iElement = (Element)j.next();
	      	if (Config.getProjectHome().equals(iElement.getAttribute("gallery").getValue()))
	      		iElement.getAttribute("gallery").setValue("");
	      }
	      
	      j = pElement.getChildren("background", Project.getAverysoftNamespace()).iterator();
	      while (j.hasNext())
	      {
	      	Element bElement = (Element)j.next();
	      	if (Config.getProjectHome().equals(bElement.getAttribute("gallery").getValue()))
	      		bElement.getAttribute("gallery").setValue("");
	      }
	    }
    }

    Document document = new Document();
		document.setRootElement(projectElement);

    FileOutputStream out = new FileOutputStream(file);
    new XMLOutputter(" ", true).output(document, out);
    out.flush();
    out.close();
  }

  void writeiBright(File file)
  throws Exception
  {
    clearProjectFieldTable();
    initializePanelDataLinking();
   
    // do not remove panel field refs in DPO 7
 /*   if (hasPanelSetOptions())
    {
      // the arrangement of data in panel sets will be done in end user applications
      removePanelSets();
    }*/

    
    org.jdom.Element projectElement = getAverysoftIBElement();
    
    // set galleries to null for distribution in masterpanels
    Iterator i = projectElement.getChildren("masterpanel", Project.getAverysoftNamespace()).iterator();
    while (i.hasNext())
    {
      Element mpElement = (Element)i.next();
      Iterator j = mpElement.getChildren("image", Project.getAverysoftNamespace()).iterator();
      while (j.hasNext())
      {
      	((Element)j.next()).getAttribute("gallery").setValue("");
      }
      
      j = mpElement.getChildren("background", Project.getAverysoftNamespace()).iterator();
      while (j.hasNext())
      {
      	((Element)j.next()).getAttribute("gallery").setValue("");
      }
    }

    // set galleries to null for distribution in panels
    Iterator k = projectElement.getChildren("page", Project.getAverysoftNamespace()).iterator();
    while (k.hasNext())
    {
      Element pgElement = (Element)k.next();
    	i = pgElement.getChildren("panel", Project.getAverysoftNamespace()).iterator();
	    while (i.hasNext())
	    {
	      Element pElement = (Element)i.next();
	      Iterator j = pElement.getChildren("image", Project.getAverysoftNamespace()).iterator();
	      while (j.hasNext())
	      {
	      	Element iElement = (Element)j.next();
	      	if (Config.getProjectHome().equals(iElement.getAttribute("gallery").getValue()))
	      		iElement.getAttribute("gallery").setValue("");
	      }
	      
	      j = pElement.getChildren("background", Project.getAverysoftNamespace()).iterator();
	      while (j.hasNext())
	      {
	      	Element bElement = (Element)j.next();
	      	if (Config.getProjectHome().equals(bElement.getAttribute("gallery").getValue()))
	      		bElement.getAttribute("gallery").setValue("");
	      }
	    }
    }

    Document document = new Document();
		document.setRootElement(projectElement);

    FileOutputStream out = new FileOutputStream(file);
    new XMLOutputter(" ", true).output(document, out);
    out.flush();
    out.close();
  }

  public boolean loadAveryProject(File file)
  {
    boolean success = false;
    try
    {
        Project project = Predesigner.openProjectInvisibly(file);
        if (project != null)
        {
          // Set the projectHome directory to whatever the user selected in file open dialog
        	//if (bOpenedProject)
        	Config.setProjectHome(project.getHomeDirectory());

          // fixup image galleries
          int orphans = project.fixGalleries(Config.getDefaultImageGallery());

          if (orphans != 0 && new File(Config.getProjectHome(), "HighRes").exists())
          {
            orphans = project.fixGalleries(Config.getProjectHome());
          }

          if (orphans /* still */ != 0)
          {
            // warn about unfound images
            String text = "There are " + orphans + " unresolved image references in this project.";
						JOptionPane.showMessageDialog(Predesigner.getFrame(), text, "Error", JOptionPane.ERROR_MESSAGE);
          }

          // fixup paperimage references
          boolean foundImage = project.updatePapers(Config.getProjectHome() + "/papers");
          if (!foundImage)
          {
          	ProductGroup group = Predesigner.getProductGroupList().getProductGroup(project.getProductGroup());
  		      project.applyAppearances(group,	new File(Config.getBatDirectory() + "/papers").getAbsolutePath(), false);
          }
                  
          // open the designated style set
          ///Predesigner.styleSet = project.getStyleSet();
          success = true;
        }
        else // Error in loading project
        {
          System.err.println("Error in loading project: " + file.getAbsolutePath());
          //Predesigner.getFrame().displayErrorMessage( sErrLoadingProject );
        }
    }
    catch (Exception ex)
    {
      Predesigner.getFrame().displayErrorMessage(ex.toString() + "\nFile: " + file.getAbsolutePath());
      ex.printStackTrace();
    }

    return success;
  }
  
  void saveAverysoft(File file)
  throws Exception
  {
  	if (Config.getFileSaveType().equals("ibright"))
    	writeiBright(file);
  	else
  		write(file);
  	
    if (!file.equals(this.file))
    {
      this.file = file;
      Predesigner.getFrame().reconstructTitle();
    }
    setModified(false);
    setLegacyProject(false);
  }

  /*void saveAverysoftIB(File file)
  throws Exception
  {
  	writeiBright(file);
  	
    if (!file.equals(this.file))
    {
      this.file = file;
      Predesigner.getFrame().reconstructTitle();
    }
    setModified(false);
    setLegacyProject(false);
  }*/

  void save()
  throws Exception
  {
  	if (isLegacyProject())
  	{
  		saveLegacy(getFile());
  	}
  	else
  	{
  		saveAverysoft(getFile());
  	}
  }

  void saveAverysoft()
  throws Exception
  {
    saveAverysoft(getFile());
  }

  void templatize()
  {
    super.removeAllPanelFields();
    file = null;
    modified = false;
    Predesigner.getFrame().updateView();
    Predesigner.getFrame().reconstructTitle();
  }

  private UndoableAction undoableAction;

  private void setUndo(UndoableAction action)
  {
    this.undoableAction = action;
  }

  private UndoableAction getUndo()
  {
    return this.undoableAction;
  }

  private void clearUndo()
  {
    undoableAction = null;
  }

  boolean undoable()
  {
    return undoableAction != null;
  }

  void executeAction(UndoableAction action)
  {
    action.execute();
    setModified(true);
    setUndo(action);
  }

  void undoLastAction()
  {
    if (undoable())
    {
      getUndo().undo();
      clearUndo();
    }
  }

  String getUndoString()
  {
    if (undoable())
    {
      return getUndo().getUndoString();
    }
    else
    {
      return "Undo";
    }
  }

  /**
   * Appearances are objects from the ProductList that specify paperimage scan file names
   * or page colors but not both.  This method applies them to the pages
   * of the AveryProject.
   * @param productGroup an object from the ProductGroupList
   * @param directory where paperImage files can be found
   */
  void applyAppearances(ProductGroup productGroup, String directory, boolean regenerate)
  {
    try
    {
      File productsFile = Config.productListFile;
      if (ProductList.getProductList() == null)
      	new ProductList(productsFile);
      
      if (productGroup != null)
      {
				for (int pageNumber = 1; pageNumber <= getPages().size(); pageNumber++)
				{
					//Appearance appearance = productGroup.getAppearance(pageNumber);
					
		      String sku = productGroup.getBaseSku();
		      Appearance appearance = ProductList.getProductList().getAppearanceBySku(sku, pageNumber);  
		      if (appearance != null)
		      {
						getPage(pageNumber).setPaperColor(appearance.getColor());
						if (appearance.getImage() != null)
						{
							getPage(pageNumber).setPaperimage(directory + "/" + appearance.getImage());
						}
		      }
				}
		
				generateMasterBackdrops(directory, regenerate);
      }
    }
    catch (Exception ex)
    {
    	System.err.println(ex.getMessage());
    }		
  }
  

  int applyAppearances(Appearance appearance, String directory, boolean regenerate)
  {
  	int generated = 0;
  	
  	int pageNumber = Predesigner.frame.getPageView().getPageNumber();
  	
		if (appearance.getImage() != null)
		{
			getPage(pageNumber).setPaperimage(directory + "/" + appearance.getImage());
		}
		else if (appearance.getColor() != null) // && appearance.getColor() != Color.white)
  	{
  		getPage(pageNumber).setPaperColor(appearance.getColor());
  	}
		generated = generateMasterBackdrops(directory, regenerate);

		return generated;
  }
  
  /**
   * Constructs a Hashtable describing the problems in any masterpanel that
   * failed to implement all required elements of its mergeMap.
   * @return Hashtable. Keys are masterpanel names, objects are Hashtables
   * with keys of map line names (eg. "AF1") and objects the display text
   * (eg. "Name and Address").  
   */
  Hashtable getIncompleteMergemapWarnings()
  {
  	Hashtable result = new Hashtable();
 	 	Iterator it = this.getMasterpanels().iterator();
 	 	while (it.hasNext())
 	 	{
 	 		AveryMasterpanel master = (AveryMasterpanel)it.next();
 	 		MergeMap map = MergeMaps.getMap(Config.mergeMapFile, master.getMergeMapName(), getLanguage());
 	 		if (map != null)
 	 		{
 	 			Hashtable lines = map.getRequiredLines(getLanguage());
 	 			if (master.supportsMergeKeys(lines) == false)
 	 			{
 	 				result.put(master.getDescription(), lines);
 	 			}
 	 		}
 	 	}
  	
  	return result;
  }
  
  public void saveDotAvery(File dotAveryFile)
  throws Exception
  {
    // safer to use a new Project instead of this
    Project tempProject = new Project();
    tempProject.initFromElement(this.getAverysoftElement());
    // remove Averysoft fieldRefs - they will be regenerated at save time
    if (this.hasMasterpanelFields()  || this.getAllPanelFields().size() == 0)
    	tempProject.clearProjectFieldTable();
    
    File tempFile = File.createTempFile("avery", ".xml", dotAveryFile.getParentFile());
    String prefix = tempFile.getName();
    prefix = prefix.substring(0, prefix.indexOf(".xml"));
    
    File dir = dotAveryFile.getParentFile();
    File highResDir = new File(dir, "HighRes");
    if (!highResDir.exists())
    {
      highResDir.mkdir();
    }

    Project newProject = new Project();
     
    if (this.hasMasterpanelFields() || this.getAllPanelFields().size() == 0)
    {
      tempProject.fillProjectForDotAveryBundle(newProject, prefix, dotAveryFile.getParentFile());
    	newProject.write(tempFile);
    }
    else
    {
     	this.write(tempFile);					// has page panel fields - write original
  	}
    
    // will contain a list of all project files
    ArrayList projectList = new ArrayList();
    projectList.add(tempFile.getPath());

    // list of project resource file names
    java.util.List resourceList = null;
    if (this.hasMasterpanelFields() || this.getAllPanelFields().size() == 0)
    	resourceList = newProject.getFileList();
    else
    	resourceList = this.getFileList();
    
    // images in newProject are returned as "/HighRes/newFilename.ext"
    
    // loop through the file list
    Iterator i = resourceList.iterator();
    while (i.hasNext())
    {
      String filename = null;
      if (this.hasMasterpanelFields() || this.getAllPanelFields().size() == 0)
      	filename = dir + (String)(i.next());
      else
      	filename = (String)(i.next());

      //System.out.println("resource filename entry=" + filename);
      if (!projectList.contains(filename))
      {
        // add unique resource name to project file list
        projectList.add(filename);
      }
    }

    // prep a project file list walker
    Iterator iterator = projectList.iterator();
    ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(dotAveryFile));

    // add each project file to the zip file
    while (iterator.hasNext())
    {
      String filepath = (String)(iterator.next());
      String nameOnly = new File(filepath).getName();
      File testDir = new File(filepath);
      if (!testDir.isDirectory())
      {
	      FileInputStream fin = new FileInputStream(filepath);
	      byte [] buf = new byte[1024];
	      // no path information in file name
	      ZipEntry ze = new ZipEntry(nameOnly); //sfile);
	      zos.putNextEntry(ze);
	      int bytesRead = fin.read(buf, 0, 1024);
	      while (bytesRead != -1)
	      {
	        zos.write(buf, 0, bytesRead);
	        bytesRead = fin.read(buf, 0, 1024);
	      }
	      zos.closeEntry();
	      fin.close();
	      
	      if (nameOnly.indexOf(prefix) == 0)
	      {
	        new File(filepath).delete();
	      }
      }
    }
    zos.close();
    
    tempFile.delete();
  }
  
  void saveMasterpanelThumbnail(File jpgFile)
  throws IOException, Exception
  {
  	//AveryMasterpanel.panelStrokeWidth = 2;
    BufferedImage image = super.makePanelImage(1, (String)getMasterPanelDescriptions().get(0), 
        Config.panelThumbnailSize, Config.panelThumbnailSize);
    
    saveImageAsJpgFile(image, jpgFile, 0.85f);
  }
  
  void savePageThumbnail(File jpgFile)
  throws IOException, Exception
  {
    BufferedImage image = super.makePagePreview(1, Config.pageThumbnailSize); 
    saveImageAsJpgFile(image, jpgFile, 0.85f);
  }
  
  static void saveImageAsJpgFile(BufferedImage image, File jpgFile, float quality)
  throws IOException, Exception
  {
    FileOutputStream out = new FileOutputStream(jpgFile);
    JPEGImageEncoder jpeg = JPEGCodec.createJPEGEncoder(out);
    JPEGEncodeParam eparam;

    eparam = JPEGCodec.getDefaultJPEGEncodeParam(image);
    eparam.setQuality(quality, false);
    jpeg.setJPEGEncodeParam(eparam);

    jpeg.encode(image);
    out.flush();
    out.close();
    out = null;
  }
  
  public void setDesignTheme(String newTheme)
  {
    if (newTheme == null)
    {
      if (getDesignTheme() != null)
      {
        super.setDesignTheme(null);
        setModified(true);
      }
    }
    else if (!newTheme.equals(getDesignTheme()))
    {
      super.setDesignTheme(newTheme);
      setModified(true);
    }
  }
  
  public void replaceStyle(StyleSet original, StyleSet replacement)
  {
    super.replaceStyle(original, replacement);
    resetProject();
  }
  
  public void resetProject()
  {
    modified = true;
        file = null;    // force new name on save
    Predesigner.getFrame().reconstructTitle();	
  }
  
  StyleSet getStyleSet()
  {
    StyleSet styleSet = StyleSet.newStyleSet();

    if (getDesignTheme() != null && !getDesignTheme().equals("placeholder"))
    {
      String ssFilename = StyleSet.constructFilename(Predesigner.getProject().getDesignTheme());
      File ssFile = new File(Config.getStyleSetsDir(), ssFilename);
      if (ssFile.exists())
      {
        try
        {
          ObjectInputStream ois = new ObjectInputStream(new FileInputStream(ssFile));
          Object ss = ois.readObject();
          ois.close();
          styleSet = (StyleSet)ss;    // successful load
        }
        catch (Exception ex)
        {
          JOptionPane.showMessageDialog(Predesigner.frame, 
              "Couldn't read Style Set from " + ssFile + "\n" +  ex.getMessage(), 
              "Style Set Failure", JOptionPane.ERROR_MESSAGE);
        }
      }
    }
    return styleSet;
  }
  
  /**
   * checks for non-localizable descriptions in masterpanel fields
   * @return the number of custom descriptions found
   */
  int numberOfCustomFieldDescriptions()
  {
    int num = 0;
    
    Iterator fit = super.getAllMasterpanelFields().iterator();
    while (fit.hasNext())
    {
      if (((AveryPanelField)fit.next()).hasCustomDescription())
        ++num;
    }
    
    return num;
  }
}