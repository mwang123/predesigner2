/*
 * UndoableActionTest.java Created on May 2, 2006 by leeb
 * Copyright 2006 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import junit.framework.TestCase;

/**
 * @author leeb
 * May 2, 2006
 * UndoableActionTest
 */
public class UndoableActionTest extends TestCase
{

	/**
	 * Constructor for UndoableActionTest.
	 * @param arg0
	 */
	public UndoableActionTest(String arg0)
	{
		super(arg0);
	}
	
	// need a concrete extension to test
	class TestAction extends UndoableAction
	{
		TestAction() { super(null); }
		TestAction(String str) { super(str); }
		void execute() {}
		void undo() {}
	}

	public final void testUndoableAction()
	{
		UndoableAction action = new TestAction();
		assertNotNull(action);
		
		UndoableAction action2 = new TestAction(null);
		assertNotNull(action2);

		UndoableAction action3 = new TestAction("foo");
		assertNotNull(action3);
	}

	public final void testGetUndoString()
	{
		UndoableAction action = new TestAction();
		assertNull(action.getUndoString());
		
		UndoableAction action2 = new TestAction(null);
		assertNull(action2.getUndoString());

		UndoableAction action3 = new TestAction("foo");
		assertEquals(action3.getUndoString(), "foo");
	}
}
