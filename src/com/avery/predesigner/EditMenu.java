package com.avery.predesigner;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.zip.ZipException;

import javax.swing.JComboBox;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.jdom.JDOMException;

import com.avery.project.AveryMasterpanel;
import com.avery.project.Descriptions;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: EditMenu, attaches to Frame</p>
 * <p>Copyright: Copyright 2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

class EditMenu extends JMenu
{
	private static final long serialVersionUID = -2921670044273495124L;
	private JMenuItem undoMenuItem = new JMenuItem("Undo");
  private JMenuItem cutMenuItem = new JMenuItem("Cut");
  private JMenuItem copyMenuItem = new JMenuItem("Copy");
  private JMenuItem pasteMenuItem = new JMenuItem("Paste");
  private JMenuItem copyAllMenuItem = new JMenuItem("Copy All");
  private JMenuItem pasteAllMenuItem = new JMenuItem("Paste All");
  private JMenuItem centerHorzMenuItem = new JMenuItem("Center Horizontal");
  private JMenuItem centerVertMenuItem = new JMenuItem("Center Vertical");
  private JMenuItem centerMenuItem = new JMenuItem("Center Both");
  private JMenuItem alignLeftMenuItem = new JMenuItem("Align Left");
  private JMenuItem alignRightMenuItem = new JMenuItem("Align Right");
  private JMenuItem alignTopMenuItem = new JMenuItem("Align Top");
  private JMenuItem alignBottomMenuItem = new JMenuItem("Align Bottom");
  private JMenuItem alignWidthMenuItem = new JMenuItem("Align Width");
  private JMenuItem alignHeightMenuItem = new JMenuItem("Align Height");
  private JMenuItem deleteMenuItem = new JMenuItem("Delete");
  private JMenuItem translateMenuItem = new JMenuItem("Translate Project...");
  private JMenuItem projectPropsMenuItem = new JMenuItem("Project Properties...");
  private JMenuItem masterPropsMenuItem = new JMenuItem("Masterpanel Properties...");
  private JMenuItem pagePropsMenuItem = new JMenuItem("Page Properties...");
  private JMenuItem paperImageMenuItem = new JMenuItem("Paper {Image or Color}...");
  
  public EditMenu()
  {
    // this is CTRL on PCs.
    int shortcutMask = java.awt.Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    undoMenuItem.setMnemonic('U');
    undoMenuItem.setEnabled(true);
    undoMenuItem.setAccelerator(KeyStroke.getKeyStroke('Z', shortcutMask, false));
    undoMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (Predesigner.getProject() != null)
        {
          Predesigner.getProject().undoLastAction();
        }
      }
    });    

    cutMenuItem.setMnemonic('t');
    cutMenuItem.setEnabled(true);
    cutMenuItem.setAccelerator(KeyStroke.getKeyStroke('X', shortcutMask, false));
    cutMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
      	Predesigner.getFrame().onCut();
      }
    });
    
    copyMenuItem.setMnemonic('C');
    copyMenuItem.setEnabled(true);
    copyMenuItem.setAccelerator(KeyStroke.getKeyStroke('C', shortcutMask, false));
    copyMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
      	Predesigner.getFrame().onCopy();
      }
    });
    
    pasteMenuItem.setMnemonic('P');
    pasteMenuItem.setEnabled(true);
    pasteMenuItem.setAccelerator(KeyStroke.getKeyStroke('V', shortcutMask, false));
    pasteMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
      	Predesigner.getFrame().onPaste();
      }
    });
    
    copyAllMenuItem.setEnabled(true);
    copyAllMenuItem.setAccelerator(KeyStroke.getKeyStroke('Y', shortcutMask, false));
    copyAllMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
      	Predesigner.getFrame().onCopyAll(pasteAllMenuItem);
      }
    });
    
    pasteAllMenuItem.setEnabled(false);
    pasteAllMenuItem.setAccelerator(KeyStroke.getKeyStroke('A', shortcutMask, false));
    pasteAllMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
      	Predesigner.getFrame().onPasteAll();
      }
    });
    
    centerHorzMenuItem.setMnemonic('[');
    centerHorzMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_OPEN_BRACKET, shortcutMask, false));
    centerHorzMenuItem.setEnabled(true);
    centerHorzMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onCenterHorz();
      }
    });    

    centerVertMenuItem.setMnemonic(']');
    centerVertMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_CLOSE_BRACKET, shortcutMask, false));
    centerVertMenuItem.setEnabled(true);
    centerVertMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onCenterVert();
      }
    });

    centerMenuItem.setMnemonic('\\');
    centerMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SLASH, shortcutMask, false));
    centerMenuItem.setEnabled(true);
    centerMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onCenter();
      }
    });
    
    //alignLeftMenuItem.setMnemonic('\\');
    //alignLeftMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SLASH, shortcutMask, false));
    alignLeftMenuItem.setEnabled(true);
    alignLeftMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onAlignLeft();
      }
    });
    
    //alignRightMenuItem.setMnemonic('\\');
    //alignRightMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SLASH, shortcutMask, false));
    alignRightMenuItem.setEnabled(true);
    alignRightMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onAlignRight();
      }
    });
    
    //alignTopMenuItem.setMnemonic('\\');
    //alignTopMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SLASH, shortcutMask, false));
    alignTopMenuItem.setEnabled(true);
    alignTopMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onAlignTop();
      }
    });
    
    //alignBottomMenuItem.setMnemonic('\\');
    //alignBottomMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SLASH, shortcutMask, false));
    alignBottomMenuItem.setEnabled(true);
    alignBottomMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onAlignBottom();
      }
    });
    
    //alignWidthMenuItem.setMnemonic('\\');
    //alignWidthMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SLASH, shortcutMask, false));
    alignWidthMenuItem.setEnabled(true);
    alignWidthMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onAlignWidth();
      }
    });
    
    //alignHeightMenuItem.setMnemonic('\\');
    //alignHeightMenuItem.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SLASH, shortcutMask, false));
    alignHeightMenuItem.setEnabled(true);
    alignHeightMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onAlignHeight();
      }
    });
    
    //deleteMenuItem.setMnemonic("D");
    deleteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
    deleteMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Predesigner.getFrame().onDelete();
      }
    });
    deleteMenuItem.setEnabled(true);
    
    translateMenuItem.setMnemonic('R');
    translateMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { onTranslate(); }
        catch (Exception ex) { 
          JOptionPane.showMessageDialog(Predesigner.frame, 
          Config.sampleTextFile.getAbsolutePath() + "\n" + ex.toString(), 
          "SampleText error", JOptionPane.ERROR_MESSAGE);
        }
      }
    });    
    
    projectPropsMenuItem.setMnemonic('P');
    projectPropsMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onProjectProperties();
      }
    });
    
    masterPropsMenuItem.setMnemonic('M');
    masterPropsMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onMasterpanelProperties();
      }
    });
    
    paperImageMenuItem.setMnemonic('I');
    paperImageMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onPaperImage();
      }
    });
    
    pagePropsMenuItem.setMnemonic('A');
    
    setText("Edit");
    add(undoMenuItem);
    addSeparator();
    add(cutMenuItem);
    add(copyMenuItem);
    add(pasteMenuItem);
    add(copyAllMenuItem);
    add(pasteAllMenuItem);
    addSeparator();
    add(deleteMenuItem);
    addSeparator();
    add(centerHorzMenuItem);
    add(centerVertMenuItem);
    add(centerMenuItem);
    addSeparator();
    add(alignLeftMenuItem);
    add(alignRightMenuItem);
    add(alignTopMenuItem);
    add(alignBottomMenuItem);
    add(alignWidthMenuItem);
    add(alignHeightMenuItem);
    addSeparator();
    add(translateMenuItem);
    add(projectPropsMenuItem);
    add(masterPropsMenuItem);
    add(paperImageMenuItem);
   // add(pagePropsMenuItem);
    
    addMenuListener(new EditMenuListener());
  }
  
  /**
   * MenuListener.menuSelected disables commands that are inappropriate for the
   * current state of the program.
   */
  class EditMenuListener implements MenuListener
  {
    public void menuCanceled(MenuEvent event)
    {
      // enable the commands that have accelerators once they are out of view
      undoMenuItem.setEnabled(true);
      cutMenuItem.setEnabled(true);
      copyMenuItem.setEnabled(true);
      pasteMenuItem.setEnabled(true);
      deleteMenuItem.setEnabled(true);    
    }
    
    public void menuDeselected(MenuEvent event)
    {
      // enable the commands that have accelerators once they are out of view
      undoMenuItem.setEnabled(true);
      cutMenuItem.setEnabled(true);
      copyMenuItem.setEnabled(true);
      pasteMenuItem.setEnabled(true);
      deleteMenuItem.setEnabled(true);
    }
    
    public void menuSelected(MenuEvent event)
    {
      boolean hasProject = (Predesigner.getProject() != null);
      
      if (hasProject)
      {
        undoMenuItem.setText(Predesigner.getProject().getUndoString());
        undoMenuItem.setEnabled(Predesigner.getProject().undoable());
      }
      else
      {
        undoMenuItem.setEnabled(false);
      }
      
      cutMenuItem.setEnabled(Predesigner.getFrame().allowCutCopy());
      copyMenuItem.setEnabled(Predesigner.getFrame().allowCutCopy());
      pasteMenuItem.setEnabled(Predesigner.getFrame().allowPaste());
      
      deleteMenuItem.setEnabled(Predesigner.getFrame().allowDelete());
      
      centerMenuItem.setEnabled(Predesigner.getFrame().allowCenter());
      centerHorzMenuItem.setEnabled(Predesigner.getFrame().allowCenter());
      centerVertMenuItem.setEnabled(Predesigner.getFrame().allowCenter());
      
      translateMenuItem.setEnabled(hasProject);
      projectPropsMenuItem.setEnabled(hasProject);
      masterPropsMenuItem.setEnabled(hasProject);
      paperImageMenuItem.setEnabled(PaperImagePanel.getColorsImages(Predesigner.getProject()) > 1);
      // pagePropsMenuItem.setEnabled(false);  // not implemented yet
    }
  }
  
  /**
   * Edit | Project Properties... menu item
   */
  void onProjectProperties()
  {
    ProjectPropertiesDialog dlg = new ProjectPropertiesDialog(Predesigner.getFrame(), Predesigner.getProject());
    dlg.setVisible(true);
    if (dlg.okay)
    {
      Predesigner.getProject().setModified(true);
    }
  }
  
  /**
   * Edit | Masterpanel Properties... menu item
   */
  void onMasterpanelProperties()
  {
    AveryMasterpanel masterpanel = Predesigner.getFrame().getCurrentMasterpanel(); 
    MasterpanelPropsPanel ui = new MasterpanelPropsPanel(masterpanel);
    
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), ui, "Edit Masterpanel Properties",
      JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
      == JOptionPane.OK_OPTION)
    {
      boolean changed = false;  // until we know something's changed
      boolean clockwise = false;
      
      if (!masterpanel.getDescription().equals(ui.getDescription()))
      {
        changed = true;
      }
      
      if (ui.orientationChanged())
      {
    	  int direction = RotationManager.suggestDirection(Predesigner.getProject());
    	  if (direction != RotationManager.DIRECTION_NONE)
    	  {
              clockwise = (direction == RotationManager.DIRECTION_CW);
              changed = true;
    	  }
      }
      
      if (ui.mergeMapChanged())
      {
      	changed = true;
      }
      
      if (ui.editableChanged())
      {
      	changed = true;
      }
     	
      if (ui.previewableChanged())
      {
      	changed = true;
      }
      	
      if (changed)
      {
        UndoableAction action = new MasterpanelPropsAction(
            Predesigner.getProject(), masterpanel, ui.getDescription(), 
						ui.orientationChanged(), clockwise, ui.mergeMapChanged(),
						ui.getMergeMapName(), ui.isEditable(), ui.isPreviewable());
        
        Predesigner.getProject().executeAction(action);
        Predesigner.getFrame().updatePanelInfo();
      }
    }
  }
  
  /**
   * called when user selects "Translate Project..."
   */
  void onTranslate()
  throws IOException, JDOMException, ZipException
  {
    SampleText sampleText = new SampleText();   // can throw exceptions
    
  	JComboBox comboBox = new JComboBox();
  	String currentLanguage = Predesigner.getProject().getLanguage();
  	
  	if (currentLanguage != null)
  	{
  		comboBox.addItem(currentLanguage);
  	}
  	
    List sampleTextLanguages = sampleText.getSupportedLanguages();
		Enumeration languages = Descriptions.getSupportedLanguages();
		while (languages.hasMoreElements())
		{
			String language = (String)languages.nextElement();
			if (!language.equals(currentLanguage)
        && sampleTextLanguages.contains(language))
			{
				comboBox.addItem(language);
			}
		}
		
		comboBox.setSelectedItem(currentLanguage == null ? "en" : currentLanguage);
  	
		if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), comboBox, 
				"Select Language Code", JOptionPane.OK_CANCEL_OPTION, 
				JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
		{
			String newLanguage = (String)comboBox.getSelectedItem();
			
			if (!newLanguage.equals(currentLanguage))
			{
				Predesigner.getProject().setLanguage((String)comboBox.getSelectedItem());
				Predesigner.getFrame().rebuildMasterpanelPopupMenu(Predesigner.getProject());
				Predesigner.getFrame().rebuildPagePopupMenu(Predesigner.getProject());
        
        Hashtable translations = sampleText.translationTable(currentLanguage, newLanguage);
        Predesigner.getProject().translateText(translations);
        Predesigner.getProject().setModified(true);
        Predesigner.getFrame().updateView();
      }
		}
  }
  
  void onPaperImage()
  {
  	PaperImagePanel paperImagePanel = new PaperImagePanel(Predesigner.getProject());
  	if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), paperImagePanel, "Edit Paper Image",
        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)== JOptionPane.OK_OPTION)
  	{
  		paperImagePanel.updateAppearance();
  	}
  }
}