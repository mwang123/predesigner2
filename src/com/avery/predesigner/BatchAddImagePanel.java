package com.avery.predesigner;

import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class BatchAddImagePanel extends JPanel
{
	private static final long serialVersionUID = 3631198611332381301L;
	private JComboBox pageNumberComboBox;
	private JComboBox horizontalComboBox;
	private JComboBox verticalComboBox;
	
	String[] pageStrings = { "1", "2", "Front", "Back", "Inside Top", "Inside Bottom", "Inside Left", "Inside Right"};
	String[] horStrings = { "left", "center", "right"};
	String[] verStrings = { "top", "middle", "bottom"};
  
	BatchAddImagePanel()
  {
    setLayout(new GridLayout(0,1));
    
    pageNumberComboBox = new JComboBox(pageStrings); 
    pageNumberComboBox.setSelectedIndex(0);
    
    add(new JLabel("Page # to add image OR Masterpanel Description:"));
    add(pageNumberComboBox);

    horizontalComboBox = new JComboBox(horStrings); 
    horizontalComboBox.setSelectedIndex(0);
    
    add(new JLabel("Horizontal Position:"));
    add(horizontalComboBox);

    verticalComboBox = new JComboBox(verStrings); 
    verticalComboBox.setSelectedIndex(0);
    
    add(new JLabel("Vertical Position:"));
    add(verticalComboBox);
  }
  
  String getPageOrDescription()
  {
    return (String)pageNumberComboBox.getSelectedItem();
  }
  
  String getHorizontalPosition()
  {
    return (String)horizontalComboBox.getSelectedItem();
  }

  String getVerticalPosition()
  {
    return (String)verticalComboBox.getSelectedItem();
  }
 
}
