/*
 * SampleTextPanel.java Created on Dec 13, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import com.borland.jbcl.layout.VerticalFlowLayout;

/**
 * @author leeb
 * Dec 13, 2007
 * SampleTextPanel
 */
public class SampleTextPanel extends JPanel
{
  private SampleText sampleText = null;
  
  private JComboBox comboBox;
  
  public JList list;
  public JButton button = new JButton("< Add Sample Text Line");

  public SampleTextPanel()
  {
    try
    {
      sampleText = new SampleText();
    }
    catch (Exception ex)
    {
      JOptionPane.showMessageDialog(Predesigner.frame, 
          Config.sampleTextFile.getAbsolutePath() + "\n" + ex.toString(), 
          "SampleText error", JOptionPane.ERROR_MESSAGE);
    }
    
    JLabel comboLabel = new JLabel("Sample Text Category");
    comboLabel.setHorizontalAlignment(JLabel.CENTER);

    initComboBox();
    comboBox.setBorder(BorderFactory.createEtchedBorder());
    
    JLabel listLabel = new JLabel("Sample Text");
    listLabel.setHorizontalAlignment(JLabel.CENTER);
    
    initList();
    list.setBorder(BorderFactory.createLineBorder(Color.black));

    if (sampleText != null)
    {
      // BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
      setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP));
  
      add(comboLabel);
      add(comboBox);
      add(listLabel);
      add(new JScrollPane(list));
      add(button);
    }
  }
  
  private void initComboBox()
  {
    if (sampleText == null)
    {
      comboBox = new JComboBox();
      return;
    }
    List sheetnames = sampleText.getCategories();
    comboBox = new JComboBox(sheetnames.toArray());
    try 
    {
      Project project = Predesigner.getProject();
      String productType = Predesigner.getProductGroupList().getProductType(project.getProductGroup());
      if (productType != null)
      {
      	String defaultCategory = sampleText.getDefaultCategory(productType);
      	if (defaultCategory != null)
      		comboBox.setSelectedItem(defaultCategory);
      }
    }
    catch (Exception x)
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), x.toString(), "error", JOptionPane.ERROR_MESSAGE);
      x.printStackTrace();
    }
    
    comboBox.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        updateList((String)comboBox.getSelectedItem());
      }
    });
  }
  
  private void initList()
  {
    if (sampleText == null)
    {
      list = new JList();
      return;
    }
    String category = (String)comboBox.getSelectedItem();
    List strings = sampleText.getTextStrings(category, Predesigner.getProject().getLanguage());
    if (strings == null)
    {
      list = new JList();   // empty list!!
    }
    else  // normal case
    {
      list = new JList(strings.toArray());
      list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
  }
  
  private void updateList(String category)
  {
    List strings = sampleText.getTextStrings(category, Predesigner.getProject().getLanguage());
    if (strings != null)
    {
      list.setListData(strings.toArray());
    }    
  }
}
