/*
 * Config.java Created on Jun 7, 2006 by leeb
 * Copyright 2006 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import javax.swing.JOptionPane;

import com.avery.project.AveryProject;
import com.avery.utils.AveryUtils;

public final class Config
{
	// Predesigner configuration globals
	private static String batDirectory        = new File("BATs").getAbsolutePath();
	private static String stencilsDirectory   = new File("stencils").getAbsolutePath();
	private static String projectHome         = new File("projects").getAbsolutePath();
	private static String defaultImageGallery = new File("projects").getAbsolutePath();
  private static String autoTemplatesDir    = new File("AutoTemplates").getAbsolutePath();
  private static String styleSetsDir        = new File("StyleSets").getAbsolutePath();
  private static String masterpanelsDir     = new File("Masterpanels").getAbsolutePath();
  private static String averyBundleDir      = new File("Bundles").getAbsolutePath();
	private static double defaultBleed        = 170.0;
  
  private static String fontDirectory;
  
  //private static String familyGroup;
  
  static 
  { // setup fontDirectory for Mac vs Windows
    if (System.getProperty("os.name").toLowerCase().startsWith("mac os x"))
    {
      fontDirectory = "/Library/Fonts";
    }
    else
    {
      fontDirectory = "C:\\Windows\\Fonts";
    }
  }
	
  public static int units = AveryUtils.INCHES;
  
  // files used for global environment
  static File productGroupListFile         = new File("ProductGroupList.xml");
  static File productListFile         		 = new File("ProductList.xml");
  static File designCategoriesFile         = new File("designUseCategory.xml");
  static File sampleTextFile               = new File("SampleText_master_U.xlsx");
  public static File mergeMapFile          = new File("mergeMap.xml");
  static File translationsFile             = new File("Translations.xml");
  static File fontEncodingsFile            = new File("FontEncodings.txt");
  static File familyGroupFile							 = new File("FamilyGroup.xml");
  
  // these values are used to create JPG thumbnails of DotAvery bundles:
  static int panelThumbnailSize = 0;
  static int pageThumbnailSize = 0;
  
  private static String fileSaveType			 = "averysoft";  // or ibright
  
  // where the config is stored
	private final static File outputFile = new File("Predesigner2.config");
	
	/**
	 * Reads the configuration variables from the file Predesigner2.config or,
	 * in a legacy update, from the file Ape.properties
	 */
	static void read()
  {
		File inputFile = outputFile;

		if (!inputFile.exists())
		{
      if (new File("./Designer.config").exists())
      {
        inputFile = new File("./Designer.config");  // legacy configuration
      }
      else if (new File("./Ape.properties").exists())
			{
				inputFile = new File("./Ape.properties");		// older legacy configuration
			}
			else	// new configuration 
			{
				return;
			}
		}

    try
    {
      Properties props = new Properties();
      props.load(new FileInputStream(inputFile));

      batDirectory        = props.getProperty("BatDirectory", batDirectory);
      stencilsDirectory   = props.getProperty("StencilsDirectory", stencilsDirectory);
      projectHome         = props.getProperty("ProjectHome", projectHome);
      defaultImageGallery = props.getProperty("DefaultImageGallery", projectHome);
      autoTemplatesDir    = props.getProperty("AutoTemplatesDir", autoTemplatesDir);
      styleSetsDir        = props.getProperty("StyleSetsDir", styleSetsDir);
      masterpanelsDir     = props.getProperty("MasterpanelsDir", masterpanelsDir);
      averyBundleDir      = props.getProperty("AveryBundleDir", averyBundleDir);
      fontDirectory       = props.getProperty("FontDirectory", fontDirectory);

      String measurementUnits = props.getProperty("MeasurementUnits", "twips").toLowerCase();
      if (measurementUnits.startsWith("t"))
      {
        Config.units = AveryUtils.TWIPS;
      }
      else if (measurementUnits.startsWith("i"))
      {
        Config.units = AveryUtils.INCHES;
      }
      else if (measurementUnits.startsWith("c"))
      {
        Config.units = AveryUtils.CENTIMETERS;
      }
      
      defaultBleed = Double.parseDouble(props.getProperty("DefaultBleed", "170.0"));
      
      // runtime environment files
      productGroupListFile = new File(props.getProperty("ProductGroupList", "ProductGroupList.xml"));
      productListFile 		 = new File(props.getProperty("ProductList", "ProductList.xml"));
      designCategoriesFile = new File(props.getProperty("DesignCategories", "designUseCategory.xml"));
      sampleTextFile       = new File(props.getProperty("SampleText", "SampleText_master_U.xlsx"));
      mergeMapFile         = new File(props.getProperty("mergeMaps", "mergeMap.xml"));
      translationsFile     = new File(props.getProperty("translations", "Translations.xml"));
      fontEncodingsFile    = new File(props.getProperty("fontEncodings", "FontEncodings.txt"));
      familyGroupFile			 = new File(props.getProperty("familyGroup", "/FamilyGroups/familyGroups.xml"));
      
      panelThumbnailSize	 = Integer.parseInt(props.getProperty("PanelThumbnailSize", "0"));
      pageThumbnailSize		 = Integer.parseInt(props.getProperty("PageThumbnailSize", "0"));
      
      fileSaveType				 = props.getProperty("fileSaveType", fileSaveType);
      if (fileSaveType.equals("ibright"))
      	AveryProject.outputMM = true;

      // read the recent files list to the FileMenu
      java.util.ArrayList fileList = new java.util.ArrayList();
      int i = 0;
      while (i++ < FileMenu.MAXRECENT)
      {
      	String filename = props.getProperty("recent" + i);
      	if (filename != null)
      	{
      		File file = new File(filename);
      		if (file.exists())
      		{
      			fileList.add(file);
      		}
      	}
      }
      
      // restore recent files to the FileMenu
      FileMenu.setRecentFileList(fileList);
    }
    catch (Exception e)
    {
    }
  }

	/**
	 * writes the configuration variables to the Predesigner.config properties file
	 *
	 */
  static void write()
  {
    try
    {
      Properties props = new Properties();
      props.setProperty("BatDirectory", batDirectory);
      props.setProperty("StencilsDirectory", stencilsDirectory);
      props.setProperty("ProjectHome", projectHome);
      props.setProperty("DefaultImageGallery", defaultImageGallery);
      props.setProperty("AutoTemplatesDir", autoTemplatesDir);
      props.setProperty("StyleSetsDir", styleSetsDir);
      props.setProperty("MasterpanelsDir", masterpanelsDir);
      props.setProperty("averyBundleDir", averyBundleDir);
      props.setProperty("FontDirectory", fontDirectory);
      
      String units;
      switch (Config.units)
      {
      case AveryUtils.INCHES:
        units = "inches";
        break;
      case AveryUtils.CENTIMETERS:
        units = "centimeters";
        break;
      default:
        units = "twips";
        break;
      }
      props.setProperty("MeasurementUnits", units);
      props.setProperty("DefaultBleed", Double.toString(Config.getDefaultBleed()));
      
      props.setProperty("PanelThumbnailSize", Integer.toString(panelThumbnailSize));
      props.setProperty("PageThumbnailSize", Integer.toString(pageThumbnailSize));
      
      props.setProperty("ProductGroupList", productGroupListFile.getAbsolutePath());
      props.setProperty("ProductList", productListFile.getAbsolutePath());
      props.setProperty("DesignCategories", designCategoriesFile.getAbsolutePath());
      props.setProperty("SampleText", sampleTextFile.getAbsolutePath());
      props.setProperty("mergeMaps", mergeMapFile.getAbsolutePath());
      props.setProperty("translations", translationsFile.getAbsolutePath());
      props.setProperty("fontEncodings", fontEncodingsFile.getAbsolutePath());
      props.setProperty("familyGroup", familyGroupFile.getAbsolutePath());
      props.setProperty("fileSaveType", fileSaveType);
      
      java.util.Iterator it = FileMenu.getRecentFileList().iterator();
      int i = 1;
      while (it.hasNext())
      {
      	props.setProperty("recent" + i++, ((File)it.next()).getPath());
      }
      
      props.store(new FileOutputStream(outputFile), "Averysoft Predesigner2 configuration");
    }
    catch (Exception e)
    {
      e.printStackTrace(System.err);
    }
  }
  
  static boolean ConfigFileExists()
  {
  	return outputFile.exists();
  }
	
	public static double getDefaultBleed()
	{
		return defaultBleed;
	}
	
	static String getBatDirectory()
	{
	  return batDirectory;
	}
	
	static String getStencilsDirectory()
	{
	  return stencilsDirectory;
	}
	
	static void setStencilsDirectory(String stencilsDir)
	{
		stencilsDirectory = stencilsDir;
	}
	
	public static String getProjectHome()
	{
	  return projectHome;
	}
	
	static void setProjectHome(String home)
	{
	  projectHome = home;
	}
	
	public static String getDefaultImageGallery()
	{
	  return defaultImageGallery;
	}
  
  static String getAutoTemplatesDir()
  {
    if (!new File(autoTemplatesDir).exists())
    {
      new File(autoTemplatesDir).mkdir();
    }
    return autoTemplatesDir;
  }
  
  static String getStyleSetsDir()
  {
    if (!new File(styleSetsDir).exists())
    {
      new File(styleSetsDir).mkdir();
    }
    return styleSetsDir;
  }
  
  static String getMasterpanelsDir()
  {
    if (!new File(masterpanelsDir).exists())
    {
      new File(masterpanelsDir).mkdir();
    }
    return masterpanelsDir;
  }
  
  static String getAveryBundleDir()
  {
    if (!new File(averyBundleDir).exists())
    {
      new File(averyBundleDir).mkdir();
    }
    return averyBundleDir;
  }
	
	static String getFontDirectory()
	{
	  return fontDirectory;
	}

	static String getFileSaveType()
	{
	  return fileSaveType;
	}

	/**
	 * File | Predesigner Configuration... user action performed
	 * Allows the user to set the configuration variables, then updates the
	 * Predesigner.config file
	 */
	static void configure()
	{
	  ConfigurationDialog dlg = new ConfigurationDialog(Predesigner.frame);
	  dlg.setVisible(true);
	  
	  if (dlg.isOkay())
	  {
	  	if (new File(dlg.getBatDirectory()).exists())
	  	{
	  		batDirectory = dlg.getBatDirectory();
	  	}
	    else showErrorMessage("BAT folder" + dlg.getBatDirectory() + " doesn't exist.");
	  	
	  	if (new File(dlg.getStencilsDirectory()).exists())
	  	{
	  		stencilsDirectory = dlg.getStencilsDirectory();
	  	}
	    else showErrorMessage("Stencils folder" + dlg.getStencilsDirectory() + " doesn't exist.");
	  	
	    if (new File(dlg.getImageGallery() + "/HighRes").exists())
	    {
	      defaultImageGallery = dlg.getImageGallery();
	    }
	    else showErrorMessage(dlg.getImageGallery() + " isn't a valid image folder (no HighRes/Lowres subfolders).");
      
      File dir = new File(dlg.getAutoTemplatesDir());
      if ((dir.exists() && dir.isDirectory()) || dir.mkdir())   // auto-create
      {
        autoTemplatesDir = dir.getAbsolutePath();
      }
      else showErrorMessage("AutoTemplate folder " + dir.getAbsolutePath() + " doesn't exist.");
      
      dir = new File(dlg.getStyleSetsDir());
      if ((dir.exists() && dir.isDirectory()) || dir.mkdir())   // auto-create
      {
        styleSetsDir = dir.getAbsolutePath();
      }
      else showErrorMessage("StyleSets folder " + dir.getAbsolutePath() + " doesn't exist.");
      
      dir = new File(dlg.getMasterpanelsDir());
      if ((dir.exists() && dir.isDirectory()) || dir.mkdir())   // auto-create
      {
        masterpanelsDir = dir.getAbsolutePath();
      }
      else showErrorMessage("Masterpanels folder " + dir.getAbsolutePath() + " doesn't exist.");
      
      dir = new File(dlg.getAveryBundleDir());
      if ((dir.exists() && dir.isDirectory()) || dir.mkdir())   // auto-create
      {
        averyBundleDir = dir.getAbsolutePath();
      }
      else showErrorMessage("Masterpanels folder " + dir.getAbsolutePath() + " doesn't exist.");
	
	    if (new File(dlg.getFontDirectory()).exists())
	    {
	      fontDirectory = dlg.getFontDirectory();
	    }
	    else showErrorMessage("Fonts folder " + dlg.getFontDirectory() + " doesn't exist.");
	    
	    if (new File(dlg.getProjectsDirectory()).exists())
	    {
	      projectHome = dlg.getProjectsDirectory();
	    }
	    else showErrorMessage("Projects folder " + dlg.getProjectsDirectory() + " doesn't exist.");
      
      if (dlg.getProductGroupListFile().exists())
      {
        productGroupListFile = dlg.getProductGroupListFile();
      }
      else showErrorMessage("ProductGroupList file " + dlg.getProductGroupListFile() + " doesn't exist.");

      if (dlg.getProductListFile().exists())
      {
        productListFile = dlg.getProductListFile();
      }
      else showErrorMessage("ProductList file " + dlg.getProductListFile() + " doesn't exist.");

      if (dlg.getDesignCategoriesFile().exists())
      {
        designCategoriesFile = dlg.getDesignCategoriesFile();
      }
      else showErrorMessage("Design Categories file " + dlg.getDesignCategoriesFile() + " doesn't exist.");
      
      if (dlg.getSampleTextFile().exists())
      {
        sampleTextFile = dlg.getSampleTextFile();
      }
      else showErrorMessage("Sample Text file " + dlg.getSampleTextFile() + " doesn't exist.");
      
      if (dlg.getMergeMapFile().exists())
      {
        mergeMapFile = dlg.getMergeMapFile();
      }
      else showErrorMessage("Merge Map file " + dlg.getMergeMapFile() + " doesn't exist.");

      if (dlg.getFontEncodingsFile().exists())
      {
        if (!dlg.getFontEncodingsFile().equals(fontEncodingsFile))  // do nothing if it didn't change
        {
          fontEncodingsFile = dlg.getFontEncodingsFile();
          Predesigner.setupFontEncodings();
        }
      }
      else showErrorMessage("Font Encodings file " + dlg.getFontEncodingsFile() + " doesn't exist.");
	
      if (dlg.getFamilyGroupFile().exists())
      {
        if (!dlg.getFamilyGroupFile().equals(familyGroupFile))  // do nothing if it didn't change
        {
        	familyGroupFile = dlg.getFamilyGroupFile();
        	ProductsMultiplier.logBaseFamilyGroups();
        }
      }
      else showErrorMessage("Family Group file " + dlg.getFamilyGroupFile() + " doesn't exist.");
      
	    units = dlg.getUnits();
	    
	    defaultBleed = dlg.getDefaultBleed();
	    
	    write();
	  }
	  
	  Predesigner.getFrame().updatePanelInfo();
	} // end of configure
  
  static void showErrorMessage(String message)
  {
    JOptionPane.showMessageDialog(Predesigner.frame, message, "Warning", JOptionPane.ERROR_MESSAGE);
  }
}
