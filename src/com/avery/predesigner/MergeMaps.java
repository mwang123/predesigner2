/*
 * MergeMaps.java Created on May 25, 2005 by Bob Lee
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.avery.Averysoft;
import com.avery.utils.MergeMap;

/**
 * Predesigner's reading of the MergeMap.xml file
 */
public class MergeMaps
{
	private ArrayList mergeMaps = new ArrayList();
	
	MergeMaps(File file)
	throws FileNotFoundException, IOException, JDOMException, Exception
	{
		if (!file.exists())
			throw new FileNotFoundException();
		
    SAXBuilder builder = new SAXBuilder(true);
		builder.setFeature("http://apache.org/xml/features/validation/schema", true);
		builder.setProperty(
				"http://apache.org/xml/properties/schema/external-schemaLocation",
				Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 
		
    Element root = builder.build(file).getRootElement();
    if (root.getName().equals("merge"))
    {
    	List elements = root.getChildren("mergeMap", MergeMap.getNamespace());
    	Iterator iterator = elements.iterator();
    	while (iterator.hasNext())
    	{
    		try
				{
    			MergeMap mergeMap = new MergeMap((Element)iterator.next());
    			if (mergeMap != null)
    			{
    				mergeMaps.add(mergeMap);
    			}
    			else
    			{
    				System.err.println("null mergeMap object in " + file.getAbsolutePath());
    			}
				}
    		catch (Exception e)
				{
    			System.err.println(e.toString());
    			e.printStackTrace();
    		}
    	}
    }
    else throw new Exception("Root element of " + file.getAbsolutePath() + " isn't \"merge\"");
	}
	
	/**
	 * retrieve the relevant MergeMaps for a given language
	 * @param language
	 * @return a List of MergeMap objects, which may be empty
	 */
	List getMaps(String language)
	{
		ArrayList list = new ArrayList();
		Iterator iterator = mergeMaps.iterator();
		while (iterator.hasNext())
		{
			MergeMap map = (MergeMap)iterator.next();
			if (map.supportsLanguage(language))
			{
				list.add(map);
			}
		}
		
		if (list.isEmpty())
		{
			System.out.println("No MergeMaps for language \"" + language + "\"");
		}
		
		return list;
	}
	
	/**
	 * get a MergeMap object from a MergeMaps.xml file.  This is used by the 
	 * various field editor dialogs that need provide a selection of Lines
	 * from the current mergeMap
	 * @param file location of mergeMaps.xml
	 * @param name the map to retrieve
	 * @param language the language of interest
	 * @return a MergeMap, or <code>null</code> on failure
	 */
	public static MergeMap getMap(File file, String name, String language)
	{
		try
		{
			MergeMaps maps = new MergeMaps(file);
			Iterator iterator = maps.mergeMaps.iterator();
			while (iterator.hasNext())
			{
				MergeMap map = (MergeMap)iterator.next();
				if (map.getName().equals(name) && map.supportsLanguage(language))
				{
					return map;
				}
			}
		}
		catch (Exception e)
		{
			System.err.print(e.toString());
		}
		return null;
		
	}
}
