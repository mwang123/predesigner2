/*
 * ImageAccessory.java Created on Mar 18, 2005 by leeb Copyright 2005 Avery
 * Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

import net.sf.javavp8decoder.imageio.WebPImageReader;
import net.sf.javavp8decoder.imageio.WebPImageReaderSpi;

import com.avery.project.AveryImage;
import com.twelvemonkeys.imageio.plugins.tiff.TIFFImageReader;
import com.twelvemonkeys.imageio.plugins.tiff.TIFFImageReaderSpi;

/**
 * @author http://java.sun.com/developer/JDCTechTips/2004/tt0316.html
 */
public class ImageAccessory extends JLabel implements PropertyChangeListener
{
	private static final long serialVersionUID = 621426849958935538L;
	private static final int PREFERRED_WIDTH = 125;
	private static final int PREFERRED_HEIGHT = 100;

	public ImageAccessory(JFileChooser chooser)
	{
		setVerticalAlignment(JLabel.CENTER);
		setHorizontalAlignment(JLabel.CENTER);
		chooser.addPropertyChangeListener(this);
		setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
	}
	
	/**
	 * @param changeEvent (looking for JFileChooser.SELECTED_FILE_CHANGED_PROPERTY)
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	
	public void propertyChange(PropertyChangeEvent changeEvent)
	{
		String changeName = changeEvent.getPropertyName();
		if (changeName.equals(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY))
		{
			File file = (File) changeEvent.getNewValue();
			if (file != null)
			{
				ImageIcon icon = new ImageIcon(file.getPath());
				if (icon.getIconWidth() > PREFERRED_WIDTH)
				{
					icon = new ImageIcon(icon.getImage().getScaledInstance(PREFERRED_WIDTH, -1, Image.SCALE_DEFAULT));
					if (icon.getIconHeight() > PREFERRED_HEIGHT)
					{
						icon = new ImageIcon(icon.getImage().getScaledInstance(-1, PREFERRED_HEIGHT, Image.SCALE_DEFAULT));
					}
				}
				setIcon(icon);
			}
		}
	}
	
	// this code fails on MAC!
/*	public void propertyChange(PropertyChangeEvent changeEvent)
	{
		String changeName = changeEvent.getPropertyName();
		if (changeName.equals(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY))
		{
			File file = (File) changeEvent.getNewValue();
			String fName = file.getName().toLowerCase();  // this causes MAC to fail????
			
			if (fName.endsWith(".psd") || fName.endsWith(".pdf") || fName.endsWith(".ai") )
				return;
			
			if (file != null)
			{
				try
				{
					BufferedImage bufferedImage = null;
					if (fName.endsWith(".tif") || fName.endsWith(".tiff"))
					{
				      	TIFFImageReader tifir = new TIFFImageReader(new TIFFImageReaderSpi());
				      	ImageInputStream iis = ImageIO.createImageInputStream(file);
				      	tifir.setInput(iis);
				      	int bits = tifir.getBitsPerSample();
				      	if (bits < 1 || bits > 8)
				      	{
				      		throw new Exception();
				      	}     	
				      	
				      	bufferedImage = tifir.read(0);
				      	iis.close();						
					}
					else if (fName.endsWith(".webp"))
					{
				        WebPImageReader wpir = new WebPImageReader(new WebPImageReaderSpi());
				        ImageInputStream wpiis = ImageIO.createImageInputStream(file);
				        wpir.setInput(wpiis);
				        bufferedImage = wpir.read(0);
				        wpiis.close();						
					}
					else
					{
						FileInputStream fis = new FileInputStream(file);
						bufferedImage = ImageIO.read(fis);
						fis.close();
					}
					
					
					ImageIcon icon = new ImageIcon(bufferedImage);
					//ImageIcon icon = new ImageIcon(file.getPath());
					if (icon.getIconWidth() > PREFERRED_WIDTH)
					{
						icon = new ImageIcon(icon.getImage().getScaledInstance(PREFERRED_WIDTH, -1, Image.SCALE_DEFAULT));
						if (icon.getIconHeight() > PREFERRED_HEIGHT)
						{
							icon = new ImageIcon(icon.getImage().getScaledInstance(-1, PREFERRED_HEIGHT, Image.SCALE_DEFAULT));
						}
					}
					setIcon(icon);
				}
				catch (Exception e)
				{}
			}
		}
	}*/
	
	
}