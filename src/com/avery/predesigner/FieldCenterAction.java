package com.avery.predesigner;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: centers a field within a masterpanel</p>
 * <p>Copyright: Copyright 2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

class FieldCenterAction extends UndoableAction
{
  protected AveryPanelField field;
  protected Point originalPosition;
  protected Point newPosition = new Point();

  public FieldCenterAction(AveryMasterpanel panel, AveryPanelField field)
  {
    super("Undo Center");
    this.field = field;
    this.originalPosition = field.getPosition();

    Rectangle bounds = field.getBoundingRect();
    double centerX = ((double)bounds.x) + (((double)bounds.width) / 2.0);
    double centerY = ((double)bounds.y) + (((double)bounds.height) / 2.0);
    
    double panelCenterX = panel.getWidth().doubleValue() / 2.0;
    double panelCenterY = panel.getHeight().doubleValue() / 2.0;
    
    AffineTransform aft = AffineTransform.getTranslateInstance(panelCenterX - centerX, panelCenterY - centerY);
    aft.transform(field.getPosition(), newPosition);
  }
  
  public Point getNewPosition()
  {
  	return newPosition;
  }
  
  void execute()
  {
    field.setPosition(newPosition);
    Predesigner.getFrame().updateView();
    
  }

  void undo()
  {
    field.setPosition(originalPosition);
    Predesigner.getFrame().updateView();
  }
}