package com.avery.predesigner;

import java.awt.*;
import javax.swing.JPanel;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Frame is the base class for various views of an AveryProject</p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 1.6
 */

abstract class View extends JPanel
{
  protected Project project;

  View(Project project)
  {
    this.project = project;
  }

  void setProject(Project project)
  {
    this.project = project;
    invalidate();
  }

  protected Project getProject()
  {
    return this.project;
  }

  protected Frame1 getFrame()
  {
    return Predesigner.getFrame();
  }

  public void paint(Graphics g)
  {
    super.paint(g);

    g.setColor(Color.white);
    g.fillRect(0, 0, getWidth(), getHeight());
  }
  
  abstract void updateStatusDisplay();
}