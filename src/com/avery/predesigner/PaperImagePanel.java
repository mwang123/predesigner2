package com.avery.predesigner;


import java.awt.Color;
import java.awt.Dimension;

import java.awt.Component;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.Iterator;
import java.util.ArrayList;


import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.LineBorder;

import com.avery.product.Appearance;
import com.avery.product.Product;
import com.avery.product.ProductList;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: panel containing gadgets for the Paper Image dialog</p>
 * <p>Copyright: Copyright 2013 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
  * @version 2.0
 */

class PaperImagePanel extends JPanel implements ItemListener
{
	private static final long serialVersionUID = 3631198611332381301L;
	
	//private static ProductList productList = null;
	
 	private JComboBox paperImageComboBox;
	private ImageLabel imageLabel = new ImageLabel();

	private JComboBox paperColorComboBox;
	private ColorPanel colorPanel = new ColorPanel();
	private ArrayList paperColors;

  private ArrayList paperAppearances = new ArrayList();
  private Project project;

  PaperImagePanel(Project currentProject)
  {
  	project = currentProject;
  	
  	setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
  	setAlignmentX(0.5f);  
  	
    try
    {
      File productsFile = Config.productListFile;
      
      if (ProductList.getProductList() == null)
      	new ProductList(productsFile);
      if (project.getProductGroup() == null)
      	return;
      
      String productGroup = project.getProductGroup();
      
	    ArrayList paperImages = new ArrayList();
	    paperColors = new ArrayList();
    	int pageNumber = Predesigner.frame.getPageView().getPageNumber();
    	
      Iterator products = ProductList.getProductList().iterator();
      boolean foundPaperImage = false;
      boolean foundPaperColor = false;
      
    	while (products.hasNext())
      {
       // dlg.makeBusy(true);   // guarantee busy cursor
        
        Product product = (Product)products.next();
        // filter products that match current project
        if (product.getProductGroupName().equals(productGroup))
        {
        	// filter products that have paperImage or paperColor       	
	        Appearance appearance = product.getAppearance(pageNumber);
	        if (appearance != null)
	        {
	        	if (appearance.getImage() != null && !foundPaperColor)
	        	{
	        		foundPaperImage = true;
        			//System.out.println("sku=" + product.getSkuName() + " image=" + appearance.getImage());
	        		if (!paperImages.contains(appearance.getImage()))
	        		{
	        			paperImages.add(appearance.getImage());
	        			paperAppearances.add(appearance);
	        		}
	  		    	continue;
	        	}     	
	        	if (appearance.getColor() != null && !foundPaperImage)
	        	{
	        		foundPaperColor = true;
	        		Color newColor = appearance.getColor();
        			//System.out.println("sku=" + product.getSkuName() + " color=" + newColor.toString());
	        		if (!paperColors.contains(newColor))
	        		{
	        			paperColors.add(appearance.getColor());
	        			paperAppearances.add(appearance);
	        		}
	        	}
	        }
        }
      }
      	
	    if (paperImages.size() > 0)
	    {
	    	paperImageComboBox = new JComboBox(paperImages.toArray());
	    	paperImageComboBox.setSelectedIndex(0);
	    	paperImageComboBox.addItemListener(this);
	    	
				Dimension spacerDimension = new Dimension(190, 20);
				JLabel titleLabel = new JLabel("Select Paper Image to apply (" + paperImages.size() + ")", JLabel.CENTER);
				JPanel titlePanel = new JPanel();
				titlePanel.add(titleLabel);
	    	add(titlePanel);
	    	add(getSpacer(spacerDimension));
	    	add(paperImageComboBox);
	    	add(getSpacer(spacerDimension));
	    	
	    	JPanel imagePanel = new JPanel();
	    	imagePanel.add(imageLabel);
	    	add(imagePanel);
	    	updateImage(0);
	    }
	    else if (paperColors.size() > 0)
	    {
  			// ensure the default color white always in the list for the same page
  			Color whiteColor = Color.WHITE;
    		if (!paperColors.contains(whiteColor))
    		{
    			Appearance appearance = (Appearance)paperAppearances.get(0);
    			Appearance whiteAppearance = new Appearance(appearance.getPageNumber(), whiteColor);
    			Color whiteAlphaColor = new Color(0xff, 0xff, 0xff, 0);
    			paperColors.add(whiteAlphaColor);
    			paperAppearances.add(whiteAppearance);
    		}
	    	 	
	    	ArrayList colors = new ArrayList();
	    	Iterator i = paperColors.iterator();
	    	while (i.hasNext())
	    	{
	    		Color color = (Color)i.next();
	    		String s = color.toString();
	    		s = s.substring(s.indexOf("["));
	    		colors.add(s);
	    	}
	    	
	    	paperColorComboBox = new JComboBox(colors.toArray());
	    	paperColorComboBox.setSelectedIndex(0);
	    	paperColorComboBox.addItemListener(this);
        ComboBoxRenderer renderer = new ComboBoxRenderer(paperColorComboBox);
        renderer.setColors(paperColors);
        renderer.setStrings(colors);
        paperColorComboBox.setRenderer(renderer);

				Dimension spacerDimension = new Dimension(90, 20);
				JLabel titleLabel = new JLabel("Select Paper Color to apply (" + colors.size() + ")", JLabel.CENTER);
				JPanel titlePanel = new JPanel();
				titlePanel.add(titleLabel);
	    	add(titlePanel);
	    	add(getSpacer(spacerDimension));
	    	add(paperColorComboBox);
	    	add(getSpacer(spacerDimension));
	    	
	    	add(colorPanel);
	    	updateColor(0);    	
	    }
    }
    catch (Exception e)
    { 	
    }  
  }
  
  public static int getColorsImages(Project project)
  {
  	int count = 0;
  	
  	try
  	{
	    if (project == null || project.getProductGroup() == null)
	    	return 0;
	    
      File productsFile = Config.productListFile;
      
      if (ProductList.getProductList() == null)
      	new ProductList(productsFile);
      
	    String productGroup = project.getProductGroup();
	    
    	int pageNumber = Predesigner.frame.getPageView().getPageNumber();
    	
	    Iterator products = ProductList.getProductList().iterator();
      boolean foundPaperImage = false;
      boolean foundPaperColor = false;
      boolean foundWhiteColor = false;
      
	    while (products.hasNext())
	    {
	      Product product = (Product)products.next();
	      // filter products that match current project
	      if (product.getProductGroupName().equals(productGroup))
	      {
	      	// filter products that have paperImage or paperColor only
	        Appearance appearance = product.getAppearance(pageNumber);
	        if (appearance != null)
	        {
        		if (appearance.getImage() != null  && !foundPaperColor)
        		{
        			foundPaperImage = true;
  	        	count++;      			
          		continue;
        		}
	        
	      		if (appearance.getColor() != null && !foundPaperImage)
	      		{
	      			foundPaperColor = true;
	      			if (appearance.getColor().equals(Color.WHITE))
	      				foundWhiteColor = true;
	      			//System.out.println("sku=" + product.getSkuName() + " papercolor=" + appearance.getColor().toString());
	      			count++;
	      		}
	        }
	      }
	    }
			Color whiteColor = Color.WHITE;
  		if (foundPaperColor && !foundWhiteColor)
  		{
  			count++;
  		}
	    //System.out.println("color count=" + count);
  	}
  	catch (Exception e)
  	{
  		System.out.println(e.getMessage());
  		e.printStackTrace();
  	}
  	
    return count;
  }
  
  private JPanel getSpacer(Dimension d)
  {
  	JPanel spacer = new JPanel();
  	spacer.setMinimumSize(d);
  	spacer.setPreferredSize(d);
  	spacer.setSize(d);

		return spacer;
  }
  
  public void itemStateChanged(ItemEvent ie)
  {
  	if (ie.getStateChange() == 1)
  	{
  		if (paperColors.size() > 0)
  		{
  			int index = paperColorComboBox.getSelectedIndex();
  			updateColor(index);		
  		}
  		else if (paperAppearances.size() > 0)
  		{
  			int index = paperImageComboBox.getSelectedIndex();
  			updateImage(index);
  		}
  	}
  }
  
  public void updateAppearance()
  {
  	int index = 0;
  	if (paperColors.size() > 0)
  		index = paperColorComboBox.getSelectedIndex();
  	else
  		index = paperImageComboBox.getSelectedIndex();
		Appearance appearance = (Appearance)paperAppearances.get(index);
  	String outputFolder = Config.getBatDirectory() + "\\papers";
		int generated = project.applyAppearances(appearance, outputFolder, true);
		Predesigner.frame.updateView();
  }
  

  private void updateImage(int index)
  {
		Appearance appearance = (Appearance)paperAppearances.get(index);
  	String outputFolder = Config.getBatDirectory() + "\\papers";
		String imageFilename = outputFolder + "\\" + (String)appearance.getImage();
		imageLabel.setImage(imageFilename);
  }

  private void updateColor(int index)
  {
		colorPanel.setColor((Color)paperColors.get(index));
  }

	public class ImageLabel extends JLabel
	{
		private ImageIcon icon;
		
		public ImageLabel()
		{
			setMinimumSize(new Dimension(200, 200));
			setSize(new Dimension(200, 200));
			setPreferredSize(new Dimension(200, 200));
			setBackground(Color.WHITE);
			LineBorder border = new LineBorder(Color.GRAY, 2);
			setBorder(border);
		}

		public void setImage(String ifn)
		{
			icon = new ImageIcon(ifn);
			
			Image img = icon.getImage();
			Image newimg = img.getScaledInstance(200, 200,  java.awt.Image.SCALE_SMOOTH); 
  
	    ImageIcon biIcon = new ImageIcon(newimg);
	    this.setIcon(biIcon);
		}
  }

	public class ColorPanel extends JPanel
	{
		
		public ColorPanel()
		{
			setMinimumSize(new Dimension(100, 40));
			setSize(new Dimension(100, 40));
			setPreferredSize(new Dimension(100, 40));
			setBackground(Color.WHITE);
			LineBorder border = new LineBorder(Color.GRAY, 2);
			setBorder(border);
		}

		public void setColor(Color color)
		{
			setBackground(color);
		}
  }
	
	class ComboBoxRenderer extends JPanel implements ListCellRenderer
	{
		
		private static final long serialVersionUID = -1L;
		private ArrayList colors;
		private ArrayList strings;
		
		JPanel textPanel;
		JLabel text;
		
		public ComboBoxRenderer(JComboBox combo)
		{		
			textPanel = new JPanel();
			textPanel.add(this);
			text = new JLabel();
			text.setOpaque(true);
			text.setFont(combo.getFont());
			textPanel.add(text);
		}
		
		public void setColors(ArrayList col)
		{
			colors = col;
		}

		public void setStrings(ArrayList str)
		{
			strings = str;
		}
		
		public ArrayList getColors()
		{
			return colors;
		}
		
		public ArrayList getStrings()
		{
			return strings;
		}

	  public Component getListCellRendererComponent(JList list, Object value,
	            int index, boolean isSelected, boolean cellHasFocus)
	  {

			/*if (isSelected)
			{
				setBackground(list.getSelectionBackground());
			}
			else
			{
				setBackground(Color.WHITE);
			}*/
			
			if (colors.size() != strings.size())
			{
		    System.out.println("colors.length does not equal strings.length");
		    return this;
			}
			else if (colors == null)
			{
		    System.out.println("use setColors first.");
		    return this;
			}
			else if (strings == null)
			{
		    System.out.println("use setStrings first.");
		    return this;
			}

			text.setText(value.toString());
			if (index > -1)
			{
				Color color = (Color)colors.get(index);
				
				if (color.getAlpha() == 0)
					text.setText("Clear Color");

				if (color.getRed() < 0x20 && color.getGreen() < 0x20 && color.getBlue() < 0x20)
					text.setForeground(Color.gray);
				
				text.setBackground((Color)colors.get(index));
			}
			return text;
	  }
	}
 
}