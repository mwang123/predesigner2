/*
 * ToolsMenu.java Created on Nov 30, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.filechooser.FileFilter;

//import org.apache.commons.io.FileUtils; 


import javax.swing.filechooser.FileNameExtensionFilter;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;






import com.avery.Averysoft;
import com.avery.predesign.AutoTemplate;
import com.avery.predesign.StyleSet;
import com.avery.predesigner.MultiplierDialog.LanguageSelector;
import com.avery.predesigner.MultiplierDialog.ProductTypeSelector;
import com.avery.product.Product;
import com.avery.product.ProductGroup;
import com.avery.product.ProductGroupList;
import com.avery.product.ProductList;
import com.avery.product.ProductList.MarketingCategory;
import com.avery.project.AveryImage;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPage;
import com.avery.project.AveryPanel;
import com.avery.project.AveryPanelField;
import com.avery.project.AveryProject;
import com.avery.project.AveryTextfield;
import com.avery.project.shapes.AveryShape;
import com.avery.project.shapes.PolygonShape;
import com.avery.utils.AveryUtils;

/**
 * @author leeb
 * Nov 30, 2007
 * ToolsMenu
 */
class ToolsMenu extends JMenu
{
  private JMenuItem createStyleMenuItem = new JMenuItem("Create Style Set...");
  private JMenuItem selectStyleMenuItem = new JMenuItem("Select Style Set...");
  private JMenuItem editStyleMenuItem = new JMenuItem("Edit Current Style Set...");
  private JMenuItem usePlaceholderMenuItem = new JMenuItem("Use Placeholder Style Set");
  private JMenuItem productTypeMenuItem = new JMenuItem("Product Type Multiplier...");
  private JMenu     projectMultiplierMenu = new JMenu("Project Folder Multiplier");
  private JMenuItem styleSetMultiplierMenuItem = new JMenuItem("Current Style Set...");
  private JMenuItem languageMultiplierMenuItem = new JMenuItem("Different Language...");
  private JMenuItem productMultiplierMenuItem = new JMenuItem("Different Product...");
  private JMenuItem familyMultiplierMenuItem = new JMenuItem("Product Families...");
  private JMenuItem blankPredesignCreatorMenuItem = new JMenuItem("Blank Predesigns...");
  private JMenuItem multiplyMastersMenuItem = new JMenuItem("Multiply Masterpanels - Edit One");
  private JMenuItem unmultiplyMastersMenuItem = new JMenuItem("Unmultiply Masterpanels - Edit All");
  private JMenuItem papersCreatorMenuItem = new JMenuItem("Paper Panel Images...");
  private JMenuItem compileStyleSetManifestMenuItem = new JMenuItem("Compile Style Set Manifest");
  private JMenuItem createCustomPanelGroupsItem = new JMenuItem("Create Custom Panel Groups...");
  private JMenuItem batchPdfOutlinesMenuItem = new JMenuItem("Batch PDF (show outlines)...");
  private JMenuItem batchPdfNoOutlinesMenuItem = new JMenuItem("Batch PDF (hide outlines)...");
  private JMenuItem batchUpdateMenuItem = new JMenuItem("Batch Averysoft Update...");
  private JMenuItem batchUpdateDrawMenuItem = new JMenuItem("Batch Averysoft Draw Update...");  // normal version
  //private JMenuItem batchUpdateDrawMenuItem = new JMenuItem("Batch Averysoft Add Image...");  // add image version
  private JMenuItem batchPagePreviewsMenuItem = new JMenuItem("Batch Page Preview Thumbnails");
  private JMenuItem batchPanelPreviewsMenuItem = new JMenuItem("Batch Panel Preview Thumbnails");
  private JMenuItem imageReferencesMenuItem = new JMenuItem("Image References");
  private JMenuItem imageConvertEPSItem = new JMenuItem("Convert EPS to PNG");
  private JMenuItem prodListXrefMenuItem = new JMenuItem("Product Lists XREF");
  private JMenuItem createAIProductListMenuItem = new JMenuItem("Create AI Product List");
  private JMenu			polygonTransformMenu = new JMenu("Polygon Outline Transforms");
  private JMenuItem polygonFlipXMenuItem = new JMenuItem("Flip about X Axis");
  private JMenuItem polygonFlipYMenuItem = new JMenuItem("Flip about Y Axis");
  private JMenuItem polygonRotateLeft90MenuItem = new JMenuItem("Rotate Left 90 deg.");
  private JMenuItem polygonRotateRight90MenuItem = new JMenuItem("Rotate Right 90 deg.");
 
 /**
   * 
   */
  ToolsMenu()
  {
    super("Tools");
    
    imageReferencesMenuItem.setMnemonic('R');
    imageReferencesMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onImageReferences();
      }
    });
    
    imageConvertEPSItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onConvertEPS();
      }
    });

    prodListXrefMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onProdListXref();
      }
    });

    createAIProductListMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCreateAIProductList();
      }
    });
    
    batchPdfOutlinesMenuItem.setMnemonic('T');
    batchPdfOutlinesMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onBatchPDF(true);
      }
    });
    
    batchPdfNoOutlinesMenuItem.setMnemonic('H');
    batchPdfNoOutlinesMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onBatchPDF(false);
      }
    });
    
    batchPagePreviewsMenuItem.setMnemonic('G');
    batchPagePreviewsMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onBatchPreviews(true);
      }
    });
    
    batchPanelPreviewsMenuItem.setMnemonic('N');
    batchPanelPreviewsMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onBatchPreviews(false);
      }
    });
    productTypeMenuItem.setMnemonic('P');
    productTypeMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onProductTypeInfo(Predesigner.getProject().getProductGroup());
      }
    });
    
  	if (Config.getFileSaveType().equals("ibright"))
  		batchUpdateMenuItem.setText("Batch Simplified XML Update...");
  	
    batchUpdateMenuItem.setMnemonic('B');   
    batchUpdateMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) 
      {      	
      	if (Config.getFileSaveType().equals("ibright"))
      		onAveryFileBatchUpdate(false);
      	else
      		onFileBatchUpdate(false, false);      		
      }
    });
    
    batchUpdateDrawMenuItem.setMnemonic('D');   
    batchUpdateDrawMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileBatchUpdate(true, false);   // normal version
        //onFileBatchUpdate(false, true);			// batch image add version
        }
    });
    
    //createCustomPanelGroupsItem.setMnemonic('');   
    createCustomPanelGroupsItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCreateCustomPanelGroups();
      }
    });
    
    selectStyleMenuItem.setMnemonic('S');
    selectStyleMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onSelectStyleSet();
      }
    });
    
    createStyleMenuItem.setMnemonic('C');
    createStyleMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCreateStyleSet();
      }
    });
    
    editStyleMenuItem.setMnemonic('E');
    editStyleMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onEditStyleSet();
      }
    });
    
    usePlaceholderMenuItem.setMnemonic('P');
    usePlaceholderMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (Predesigner.getProject() != null)
        {
          // TODO: this should be an UndoableAction
          Predesigner.getProject().replaceStyle(Predesigner.styleSet, StyleSet.newStyleSet());
          Predesigner.getProject().setModified(true);
          Predesigner.frame.updateView();
        }
        Predesigner.styleSet = StyleSet.newStyleSet();
      }
    });
    
    styleSetMultiplierMenuItem.setMnemonic('Y');
    styleSetMultiplierMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { styleMultiplier(e); }
        catch (Exception ex) { JOptionPane.showMessageDialog(Predesigner.getFrame(), ex); }

      }
    });
    
    languageMultiplierMenuItem.setMnemonic('L');
    languageMultiplierMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { translationMultiplier(e); }
        catch (Exception ex) { JOptionPane.showMessageDialog(Predesigner.getFrame(), ex); }
      }
    });
    
    productMultiplierMenuItem.setMnemonic('X');
    productMultiplierMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { ProductMultiplier.multiply(); }
        catch (Exception ex) { JOptionPane.showMessageDialog(Predesigner.getFrame(), ex); }
      }
    });
    
    //familyMultiplierMenuItem.setMnemonic('Y');
    familyMultiplierMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { ProductsMultiplier.multiply(); }
        catch (Exception ex) { JOptionPane.showMessageDialog(Predesigner.getFrame(), ex); }
      }
    });
    
    blankPredesignCreatorMenuItem.setMnemonic('A');
    blankPredesignCreatorMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { blankPredesignCreator(e); }
        catch (Exception ex) { JOptionPane.showMessageDialog(Predesigner.getFrame(), ex); }
      }
    });

    papersCreatorMenuItem.setMnemonic('I');
    papersCreatorMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { papersCreator(e); }
        catch (Exception ex) { JOptionPane.showMessageDialog(Predesigner.getFrame(), ex); }
      }
    });

    multiplyMastersMenuItem.setMnemonic('K');
    multiplyMastersMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { multiplyMasters(e); }
        catch (Exception ex) { JOptionPane.showMessageDialog(Predesigner.getFrame(), ex); }
      }
    });
    
    unmultiplyMastersMenuItem.setMnemonic('U');
    unmultiplyMastersMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try { unmultiplyMasters(e); }
        catch (Exception ex)
        {
        	JOptionPane.showMessageDialog(Predesigner.getFrame(), ex);
        	ex.printStackTrace();
        }
      }
    });
    
    compileStyleSetManifestMenuItem.setMnemonic('M');
    compileStyleSetManifestMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCompileStyleSetManifest();
      }
    });
    
    polygonFlipXMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onPolygonFlipX();
      }
    });

    polygonFlipYMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onPolygonFlipY();
      }
    });
    
    polygonRotateLeft90MenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onPolygonRotateLeft90();
      }
    });
    
    polygonRotateRight90MenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onPolygonRotateRight90();
      }
    });
    
    projectMultiplierMenu.add(styleSetMultiplierMenuItem);
    projectMultiplierMenu.add(languageMultiplierMenuItem);
    projectMultiplierMenu.add(productMultiplierMenuItem);
    projectMultiplierMenu.add(familyMultiplierMenuItem);
    projectMultiplierMenu.add(blankPredesignCreatorMenuItem);
    projectMultiplierMenu.addSeparator();
    projectMultiplierMenu.add(multiplyMastersMenuItem);
    projectMultiplierMenu.add(unmultiplyMastersMenuItem);
    projectMultiplierMenu.addSeparator();
    projectMultiplierMenu.add(papersCreatorMenuItem);
    
    polygonTransformMenu.add(polygonFlipXMenuItem);
    polygonTransformMenu.add(polygonFlipYMenuItem);
    polygonTransformMenu.add(polygonRotateLeft90MenuItem);
    polygonTransformMenu.add(polygonRotateRight90MenuItem);

    add(createStyleMenuItem);
    add(selectStyleMenuItem);
    add(editStyleMenuItem);
    add(usePlaceholderMenuItem);
    addSeparator();
    add(productTypeMenuItem);
    add(projectMultiplierMenu);
    add(compileStyleSetManifestMenuItem);
    addSeparator();
    add(createCustomPanelGroupsItem);
    addSeparator();
    add(batchUpdateMenuItem);
    add(batchUpdateDrawMenuItem);
    addSeparator();
    add(batchPdfOutlinesMenuItem);
    add(batchPdfNoOutlinesMenuItem);
    addSeparator();
    add(batchPagePreviewsMenuItem);
    add(batchPanelPreviewsMenuItem);
    addSeparator();
    add(imageReferencesMenuItem);
    // Windows only - invokes C# console app to convert EPS to PNG
    if (!(System.getProperty("os.name").toLowerCase().startsWith("mac os x")))
    	add(imageConvertEPSItem);
    addSeparator();
    add(createAIProductListMenuItem);
    add(prodListXrefMenuItem);
    addSeparator();
    add(polygonTransformMenu);

    addMenuListener(new ToolsMenuListener());
  }
  
  private class ToolsMenuListener implements MenuListener
  {
    public void menuCanceled(MenuEvent event)    {    }    
    public void menuDeselected(MenuEvent event)  {    }
    
    public void menuSelected(MenuEvent event)
    {
      if (Predesigner.getProject() == null)
      {
        productTypeMenuItem.setEnabled(false);
        polygonTransformMenu.setEnabled(false);
      }
      else try
      {
        ProductGroupList list = Predesigner.getProductGroupList();
        ProductGroup productGroup = list.getProductGroup(Predesigner.getProject().getProductGroup());
        productTypeMenuItem.setEnabled(!productGroup.isComplex());
        
        AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
        if (master.getShape().getShapeString().equals("polygon"))
        {
          polygonTransformMenu.setEnabled(true);       	
        }
        else
        {
          polygonTransformMenu.setEnabled(false);
        } 		
      }
      catch (Exception ex)
      {
        productTypeMenuItem.setEnabled(false);
      }
      
      if (Predesigner.styleSet.name.equals("placeholder"))
      {
        editStyleMenuItem.setText("Edit Current Style Set...");
        editStyleMenuItem.setEnabled(false);
        usePlaceholderMenuItem.setEnabled(false);
        styleSetMultiplierMenuItem.setText("Current Style Set...");
        styleSetMultiplierMenuItem.setEnabled(false);
      }
      else // current StyleSet is not "placeholder"
      {
        editStyleMenuItem.setText("Edit \"" + Predesigner.styleSet.name + "\" Style Set...");
        editStyleMenuItem.setEnabled(true);
        usePlaceholderMenuItem.setEnabled(true);
        styleSetMultiplierMenuItem.setText("\"" + Predesigner.styleSet.name + "\" Style Set...");
        styleSetMultiplierMenuItem.setEnabled(true);
      }
    }
  }
  
  /**
   * Shows a table of all of the productGroup objects that match the current project's productType
   * @param productGroupName
   */
  void onProductTypeInfo(String productGroupName)
  {
    String productType = "productGroup " + productGroupName;    // default to something!
    try
    {
      ProductGroupList list = Predesigner.getProductGroupList();
      
      // find the matching productGroup object
      productType = list.getProductType(productGroupName);
          
      ArrayList shortList = new ArrayList();
      Iterator groups = list.iterator();
      while (groups.hasNext())
      {
        ProductGroup productGroup = (ProductGroup)groups.next();
        if (productType.equals(productGroup.getProductType()))
        {
          shortList.add(productGroup);
        }
      }

      String[] headers = new String[] { "Name", "Description", "Template", "Base", "Auto" };
      String[][] rows = new String[shortList.size()][5];
      
      int currentRow = 0;
      groups = shortList.iterator();
      while (groups.hasNext())
      {
        ProductGroup productGroup = (ProductGroup)groups.next();
        rows[currentRow][0] = productGroup.getName();
        rows[currentRow][1] = productGroup.getDescription();
        rows[currentRow][2] = productGroup.getBatName();
        rows[currentRow][3] = productGroup.getBaseSku();
        rows[currentRow][4] = productGroup.isComplex() ? " " : "yes";
          ++currentRow;
      }
      
      JTable table = new JTable(rows, headers) {
        public boolean isCellEditable(int row, int column) { return false; }
      };
      
      JLabel renderer = ((JLabel)table.getDefaultRenderer(Object.class));
      renderer.setHorizontalAlignment(SwingConstants.CENTER);
      
      table.getColumn("Base").setMaxWidth(75);
      table.getColumn("Description").setMinWidth(75);
      table.getColumn("Template").setMaxWidth(75);
      table.getColumn("Name").setMaxWidth(75);
      table.getColumn("Auto").setMaxWidth(50);
      
      JScrollPane scrollPane = new JScrollPane(table);
      
      Object[] options = { "Okay", "Auto-Generate..." };
      
      if (1 == JOptionPane.showOptionDialog(
          Predesigner.getFrame(), scrollPane, "ProductType Info: " + productType, JOptionPane.YES_NO_OPTION,
          JOptionPane.PLAIN_MESSAGE, null, options, null))
      {
        autoGenerate(productType, shortList);
      }
    }
    catch (Exception ex)  
    {
      JOptionPane.showMessageDialog(
          Predesigner.getFrame(), ex.getStackTrace(), "ProductType Info: " + productType, JOptionPane.PLAIN_MESSAGE);
    }
  }
  
  /**
   * This will generate new predesigns using the current project's masterpanel as an autoTemplate
   * @param productType - name of the productType
   * @param productGroups - the productGroups that map to this productType
   */
  private void autoGenerate(String productType, ArrayList productGroups)
  throws Exception
  {
    if (JOptionPane.showConfirmDialog(
        Predesigner.getFrame(), "Auto-Generate Predesigns for " + productType + "?", "Auto-Generate", 
        JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.NO_OPTION)
    {
      return;
    }
    
    Project sourceProject = Predesigner.getProject(); 
    
    ArrayList accomplishments = new ArrayList();
    
    Iterator groups = productGroups.iterator();
    while (groups.hasNext())
    {
      ProductGroup productGroup = (ProductGroup)groups.next();
      File batFile = new File(Config.getBatDirectory(), productGroup.getBatName());
      if (productGroup.isComplex() || !batFile.exists())
      {
        continue;   // complex projects not allowed 
      }
      
      Project project = new Project();
      project.read(batFile);

      project.setSku(productGroup.getBaseSku());
      project.setProductGroup(productGroup.getName());
      project.setDesignTheme(Predesigner.styleSet.name);
      project.setDesignCategory(sourceProject.getDesignCategory());
      project.applyAppearances(productGroup, new File(Config.getBatDirectory() + "/papers").getAbsolutePath(), false);
        
      // do the Autotemplate magic
      AutoTemplate template = new AutoTemplate((AveryMasterpanel)sourceProject.getMasterpanels().get(0));
      template.applyTo(project, (AveryMasterpanel)project.getMasterpanels().get(0));
             
      File autoFile = autoFile(project, productGroup.getDescription(), Config.getProjectHome());
        
      project.write(autoFile);
      accomplishments.add(autoFile);
    }
    
    // display generated file list
    JList list = new JList(accomplishments.toArray());
    list.setBorder(BorderFactory.createLineBorder(Color.black));
    JOptionPane.showMessageDialog(
        Predesigner.getFrame(), list, productType + " Predesigns Auto-generated", JOptionPane.PLAIN_MESSAGE);
  }
  
  static File autoFile(AveryProject project, String description, String directory)
  {
    String language = project.getLanguage();
    String groupName = "." + project.getProductGroup();
    String date = new SimpleDateFormat(".yyMM-").format(new Date());
      
    String designTheme = project.getDesignTheme();
    String styleSet = (designTheme == null || "placeholder".equals(designTheme))
        ? "" : "." + AveryUtils.alphaNumeric(designTheme); 
    String desc = (description == null)
        ? "" : "." + AveryUtils.alphaNumeric(description);
      
    String proposedName = language + groupName + desc + styleSet + date;
      
    int i = 1;
    File file;
    do
    {
      file = new File(directory, proposedName + twoDigits(i++) + ".xml");
    }
    while (file.exists());
      
    return file;
  }
  
  private static String twoDigits(int i)
  {
    if (i < 10)
    {
      return new String("0" + i);
    }
    else return Integer.toString(i);   
  }
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // This code will be obsolete once all legacy Avery.dtd-based files have been converted to Averysoft //
  
  private static String batchWarn = "This command will OVERWRITE all XML files in a selected directory."
    + "\n\nNo further warning will be given.  If these files are valuable to you, MAKE A BACKUP FIRST."
    + "\n\nDo you wish to continue?";

  private static String batchAveryWarn = "This command converts all .avery files in a selected directory to simplified xml format."
      + "\n\nIt writes the output to a .xml file with the same filename as the .avery file."
      + "\n\nDo you wish to continue?";
  
  private void onFileBatchUpdate(boolean draw, boolean logo)
  {
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), batchWarn, "Heed this warning!", JOptionPane.YES_NO_OPTION)
        != JOptionPane.YES_OPTION)
    {
      return;
    }
    
    File folder = new File(Config.getProjectHome());
    
    JFileChooser dlg = new JFileChooser(folder.getParent());
    dlg.setDialogTitle("Select a FOLDER containing XML project files");
    dlg.setApproveButtonText("Select");
    dlg.setSelectedFile(folder);
    dlg.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    if (dlg.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
    {
      File newFolder = dlg.getSelectedFile();
      if (newFolder != null && newFolder.exists() && newFolder.isDirectory())
      {
        folder = newFolder;
      }
      else
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), newFolder + " is not acceptable.  Command aborted.",
          "error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
    else
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "No files processed.",
        "Canceled", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    int errors = 0;
    int successes = 0;
    File logFile = new File(folder, "AverysoftDesignerBatch.log");
    try
    { // create log file and open log stream
      if (logFile.exists())
      {
        logFile.delete();   // start fresh each time
      }
      PrintWriter log = new PrintWriter(new FileOutputStream(logFile));
      log.println(AveryUtils.timeStamp() + " - " + folder.getAbsolutePath());
            
      File[] files = folder.listFiles(new java.io.FileFilter() {
        public boolean accept(File pathname) {
          return (pathname.getName().endsWith(".xml"));
        }
      });
      
      
      BatchAddImagePanel ui = null;
      String hPos = "center";
      String vPos = "bottom";
      File addFile = null;
      
      if (logo)
      {
        ui = new BatchAddImagePanel();
        
        if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), ui, "Batch Add Image",
          JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
          != JOptionPane.OK_OPTION)
        {
        	return;
        }
        else
        {
        	hPos = ui.getHorizontalPosition();
        	vPos = ui.getVerticalPosition();
        	addFile = getProjectImage();
        	if (addFile == null)
        		return;
        }
      }
     
      int i = 0;
      for (i = 0; i < files.length; ++i)
      {
        File xmlfile = files[i];
        boolean textSizeChanged = false;
        
        try
        {
          //  read        
          Project project = new Project();
          project.read(xmlfile);
          
          if (logo && ui != null)
          {
	          //AveryPage page = project.getPage(1);
	          //if (page.getDoubleSided())
	          {
	          	String pageIdentifier = ui.getPageOrDescription();
	          	AveryMasterpanel masterpanel = null;
	          	if (pageIdentifier.length() == 1)
	          	{
	          		int pageNumber = Integer.parseInt(pageIdentifier) - 1;
	          		masterpanel = (AveryMasterpanel)project.getMasterpanels().get(pageNumber);
	          	}
	          	else
	          	{    
	          		masterpanel = (AveryMasterpanel)(project.getMasterpanels(project.getMasterpanels(), pageIdentifier).get(0));
	          	}
	          	if (masterpanel == null)
	          		break;
	          	
	          	int minSize = Math.min(masterpanel.getWidth().intValue(), masterpanel.getHeight().intValue());
	          	AveryImage field = createImage(addFile, hPos + vPos + pageIdentifier, minSize);
	          	String fieldId = project.getNextProjectFieldTableId(); 
	          	field.setID(fieldId);
	          	field.setContentID(fieldId);
	          	placeImageOnMaster(masterpanel, field, hPos, vPos);
	          	//project.initializePanelDataLinking();
	          }
          }
          
          // the draw stuff is used to make text boxes perform overflow
          // write the xml only if the project is draw true and any text box size
          // changed as a result of drawing all pages.
          ArrayList p1Sizes = new ArrayList();
          if (draw)
          {
            Iterator m1Iterator = project.getAllMasterpanelFields().iterator();
            while (m1Iterator.hasNext())
            {
            	AveryPanelField m1pf = (AveryPanelField)m1Iterator.next();
            	if (m1pf instanceof AveryTextfield)
            	{
            		Double pt1Size = ((AveryTextfield)m1pf).getPointsize();
            		p1Sizes.add(pt1Size);
            	}
            }
            // draw pages to ensure text font size fits box
          	int j = 1;
          	Iterator pageIterator = project.getPages().iterator();
          	while (pageIterator.hasNext())
          	{
          		AveryPage page = (AveryPage)pageIterator.next();
          		int pageWidth = (int)(page.getWidth().doubleValue() * .05);
          		BufferedImage image = project.makePagePreview(j++, pageWidth);
          	}
          }
          
          if (draw)
          {
          	ArrayList p2Sizes = new ArrayList();
            Iterator m2Iterator = project.getAllMasterpanelFields().iterator();
            while (m2Iterator.hasNext())
            {
            	AveryPanelField m2pf = (AveryPanelField)m2Iterator.next();
            	if (m2pf instanceof AveryTextfield)
            	{
            		Double pt1Size = ((AveryTextfield)m2pf).getPointsize();
            		p2Sizes.add(pt1Size);
            	}
            }

            // check if any text boxes changed size
            Iterator p1 = p1Sizes.iterator();
            Iterator p2 = p2Sizes.iterator();
            while (p1.hasNext() && p2.hasNext())
            {
            	Double pt1Size = (Double)p1.next();
            	Double pt2Size = (Double)p2.next();
            	//System.out.println(pt1Size.doubleValue() + "," + pt2Size.doubleValue());
          		if (pt1Size.doubleValue() != pt2Size.doubleValue())
          		{
          			textSizeChanged = true;
          			break;
          		}
            }
          }
          
          if (!draw || textSizeChanged || logo)
          {
          	if (Config.getFileSaveType().equals("ibright"))
          		project.writeiBright(xmlfile);
          	else
          		project.write(xmlfile);
          	++successes;
          }
        }
        catch (Exception ex)  // log failure
        {
          log.println("\tERROR on - " + xmlfile.getName() + " - " + ex.toString());
          ++errors;
          continue;
        }
        //  log success
        if (!draw || textSizeChanged || logo)
        	log.println("\trewrote " + xmlfile.getName());
      }
    
      // close log
      log.flush();
      log.close();
    }
    catch (Exception ex)
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "Couldn't create log file./n/nOperation aborted.",
        "Canceled", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    switch (errors)
    {
      case 0:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "Success! " + successes + " XML files were processed.\n\nCheck " + logFile.getAbsolutePath() + " for details.",
            "No Errors", JOptionPane.INFORMATION_MESSAGE);  
        break;
      case 1:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There was an error.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "ERROR", JOptionPane.ERROR_MESSAGE);
        break;
      default:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There were " + errors + " errors.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "MULTIPLE ERRORS", JOptionPane.ERROR_MESSAGE);  
    }
  }
  
  private void onAveryFileBatchUpdate(boolean draw)
  {
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), batchAveryWarn, "Batch Conversion to Simplified XML", JOptionPane.YES_NO_OPTION)
        != JOptionPane.YES_OPTION)
    {
      return;
    }
    
    File folder = new File(Config.getProjectHome());
    
    JFileChooser dlg = new JFileChooser(folder.getParent());
    dlg.setDialogTitle("Select a FOLDER containing .avery project files");
    dlg.setApproveButtonText("Select");
    dlg.setSelectedFile(folder);
    dlg.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    if (dlg.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
    {
      File newFolder = dlg.getSelectedFile();
      if (newFolder != null && newFolder.exists() && newFolder.isDirectory())
      {
        folder = newFolder;
      }
      else
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), newFolder + " is not acceptable.  Command aborted.",
          "error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
    else
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "No files processed.",
        "Canceled", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    int errors = 0;
    int successes = 0;
    File logFile = new File(folder, "AverysoftDesignerBatch.log");
    try
    { // create log file and open log stream
      if (logFile.exists())
      {
        logFile.delete();   // start fresh each time
      }
      PrintWriter log = new PrintWriter(new FileOutputStream(logFile));
      log.println(AveryUtils.timeStamp() + " - " + folder.getAbsolutePath());
            
      File[] files = folder.listFiles(new java.io.FileFilter() {
        public boolean accept(File pathname) {
          return (pathname.getName().endsWith(".avery"));
          //return (pathname.getName().endsWith(".xml"));
        }
      });
      
      int i = 0;
      for (i = 0; i < files.length; ++i)
      {
        File averyfile = files[i];
        boolean textSizeChanged = false;
        
        try
        {
          //  read        
          Project project = new Project();
          project.loadAveryProject(averyfile);
          project = Predesigner.getProject();
          
          //String xmlFileName = averyfile.getAbsolutePath().replace(".xml", "_out.xml");
          String xmlFileName = averyfile.getAbsolutePath().replace(".avery", ".xml");
          File xmlFile = new File(xmlFileName);
          
          
          if (!draw || textSizeChanged)
          {
          	if (Config.getFileSaveType().equals("ibright"))
          		project.writeiBright(xmlFile);
          	else
          		project.write(xmlFile);
          	++successes;
          }
        }
        catch (Exception ex)  // log failure
        {
          log.println("\tERROR on - " + averyfile.getName() + " - " + ex.toString());
          ++errors;
          continue;
        }
        //  log success
        if (!draw || textSizeChanged)
        	log.println("\trewrote " + averyfile.getName());
      }
    
      // close log
      log.flush();
      log.close();
    }
    catch (Exception ex)
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "Couldn't create log file./n/nOperation aborted.",
        "Canceled", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    //Predesigner.getFrame().viewMasterPanelView(false);
    //Predesigner.getFrame().viewMasterPanelView(true);
    
    
    switch (errors)
    {
      case 0:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "Success! " + successes + " XML files were processed.\n\nCheck " + logFile.getAbsolutePath() + " for details.",
            "No Errors", JOptionPane.INFORMATION_MESSAGE);  
        break;
      case 1:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There was an error.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "ERROR", JOptionPane.ERROR_MESSAGE);
        break;
      default:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There were " + errors + " errors.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "MULTIPLE ERRORS", JOptionPane.ERROR_MESSAGE);  
    }
  }
  
 ///////////////////////////////////////////////////////////////////////////////////////////////////////
  private void onBatchPDF(boolean outlinePanels)
  {   
    File folder = new File(Config.getProjectHome());
    
    JFileChooser dlg = new JFileChooser(folder.getParent());
    dlg.setDialogTitle("Select a FOLDER containing XML project files");
    dlg.setApproveButtonText("Select");
    dlg.setSelectedFile(folder);
    dlg.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    if (dlg.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
    {
      File newFolder = dlg.getSelectedFile();
      if (newFolder != null && newFolder.exists() && newFolder.isDirectory())
      {
        folder = newFolder;
      }
      else
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), newFolder + " is not acceptable.  Command aborted.",
          "error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
    else
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "No files processed.",
        "Canceled", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    int errors = 0;
    int successes = 0;
    File logFile = new File(folder, "PdfBatch.log");
    try
    { // create log file and open log stream
      if (logFile.exists())
      {
        logFile.delete();   // start fresh each time
      }
      PrintWriter log = new PrintWriter(new FileOutputStream(logFile));
      log.println(AveryUtils.timeStamp() + " - " + folder.getAbsolutePath());
            
      File[] files = folder.listFiles(new java.io.FileFilter() {
        public boolean accept(File pathname) {
          return (pathname.getName().endsWith(".xml"));
        }
      });
      
      Predesigner.getFrame().setCursor(new Cursor(Cursor.WAIT_CURSOR));
      
      int i = 0;
      for (i = 0; i < files.length; ++i)
      {
        File xmlfile = files[i];
        String pdfname = xmlfile.getAbsolutePath();
        pdfname = pdfname.substring(0, pdfname.lastIndexOf('.'));
        pdfname += ".pdf";
        try
        {          
          Project project = new Project();
          project.read(xmlfile);
          // fixup image galleries
          int orphans = project.fixGalleries(Config.getDefaultImageGallery());

          if (orphans != 0 && new File(Config.getProjectHome(), "HighRes").exists())
          {
            orphans = project.fixGalleries(Config.getProjectHome());
          }
          
          if (orphans != 0)  // still! We've failed.
          {
            throw new Exception("image(s) not found");
          }
          
          project.makePdfFile(pdfname, Config.getFontDirectory(), outlinePanels, false);
          ++successes;
        }
        catch (Exception ex)  // log failure
        {
          log.println("\tERROR on - " + xmlfile.getName() + " - " + ex.toString());
          ex.printStackTrace();
          ++errors;
          continue;
        }
        //  log success
        log.println("\twrote " + pdfname);
      }
    
      // close log
      log.flush();
      log.close();
      
      Predesigner.getFrame().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
    catch (Exception ex)
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "Couldn't create log file./n/nOperation aborted.",
        "Canceled", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    switch (errors)
    {
      case 0:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "Success! " + successes + " XML files were processed.\n\nCheck " + logFile.getAbsolutePath() + " for details.",
            "No Errors", JOptionPane.INFORMATION_MESSAGE);  
        break;
      case 1:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There was an error.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "ERROR", JOptionPane.ERROR_MESSAGE);
        break;
      default:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There were " + errors + " errors.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "MULTIPLE ERRORS", JOptionPane.ERROR_MESSAGE);  
    }
  }
  
  private void onImageReferences()
  {   
    File folder = new File(Config.getProjectHome());
    File imageFolder = new File(Config.getDefaultImageGallery() + "/HighRes");
    
    JFileChooser dlg = new JFileChooser(folder);
    //JFileChooser dlg = new JFileChooser(folder.getParent());
    dlg.setDialogTitle("Select a FOLDER containing XML project files");
    dlg.setApproveButtonText("Select");
    dlg.setCurrentDirectory(folder);
    //dlg.setSelectedFile(folder);
    dlg.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    if (dlg.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
    {
      File newFolder = dlg.getSelectedFile();
      System.out.println(newFolder.getAbsolutePath());
      if (newFolder != null && newFolder.exists() && newFolder.isDirectory())
      {
        folder = newFolder;
        //imageFolder = new File(newFolder.getAbsolutePath() + "/HighRes");
      }
      else
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), newFolder + " is not acceptable.  Command aborted.",
          "error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
    else
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "No files processed.",
        "Canceled", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    int errors = 0;
    int successes = 0;
    String timeStamp = AveryUtils.timeStamp().replaceAll(":", ".");
    timeStamp = timeStamp.replaceAll(" ", "");
    File logFile = new File(folder, timeStamp + "_ImageFileReferences.txt");
    try
    { // create log file and open log stream
      if (logFile.exists())
      {
        logFile.delete();   // start fresh each time
      }
      PrintWriter log = new PrintWriter(new FileOutputStream(logFile));
      log.println("Image\tRefs\t" + folder.getAbsolutePath());
            
      File[] files = folder.listFiles(new java.io.FileFilter() {
        public boolean accept(File pathname) {
          return (pathname.getName().endsWith(".xml"));
        }
      });
      
      File[] highResFiles = imageFolder.listFiles(new java.io.FileFilter() {
        public boolean accept(File pathname) {
        	boolean isImage = pathname.getName().toLowerCase().endsWith(".jpg") ||
        										pathname.getName().toLowerCase().endsWith(".png") ;
          return isImage;
        }
      });
      Predesigner.getFrame().setCursor(new Cursor(Cursor.WAIT_CURSOR));
      
      // create a hash table of all image files
      Hashtable imageHash = new Hashtable();
      int i = 0;
      while (i < highResFiles.length)
      {
      	String fileName = highResFiles[i++].getName();
      	imageHash.put(fileName, new Integer(0));
      }
      
      i = 0;
      for (i = 0; i < files.length; ++i)
      {
        File xmlfile = files[i];
        try
        {          
          Project project = new Project();
          project.read(xmlfile);

          if (new File(Config.getProjectHome(), "HighRes").exists())
          {
            List images = project.getImageFields();
            
            Iterator imageIterator = images.iterator();
            while (imageIterator.hasNext())
            {
            	AveryImage image = (AveryImage)imageIterator.next();
            	String key = image.getSource();
            	Object value = imageHash.get((Object)key);
            	if (value == null)
            	{
              	imageHash.put(key, new Integer(-1));            		
            	}
            	else
            	{
            		imageHash.remove(key);
            		int refs = ((Integer)value).intValue();
            		Integer newRef;
            		if (refs < 0)
            		{
            			newRef = new Integer(--refs);
            		}
            		else
            		{
            			newRef = new Integer(++refs);            			
            		}
              	imageHash.put(key, new Integer(refs));            		
            	}
            }
          }
          ++successes;
        }
        catch (Exception ex)  // log failure
        {
          log.println("\tERROR on - " + xmlfile.getName() + " - " + ex.toString());
          ex.printStackTrace();
          ++errors;
          continue;
        }
        //  log success
        //log.println("\twrote " + pdfname);
      }
    
      //sortHashtableValue(imageHash);
      ArrayList hashList = new ArrayList(imageHash.entrySet()); 
      Collections.sort(hashList, new HashComparator());
      
      Iterator iter = hashList.iterator();
      String key = "";
      int refs = 0;
      int cnt = 0;
      while(iter.hasNext())
      { 
      	cnt++;
	      Map.Entry e=(Map.Entry)iter.next(); 
	      key = (String)e.getKey();
	      refs = ((Integer)e.getValue()).intValue();
	
	      //System.out.println(key + ", " + refs);
	      log.println(key + "\t" + refs);
      }
      
      // close log
      log.flush();
      log.close();
      
      // now copy the images
      File destinationFolder = new File(imageFolder.getAbsolutePath() + "/HighRes_Verified_" + timeStamp);
    	if (!destinationFolder.exists())
    	{
    		//FileUtils.forceMkdir(destinationFolder);
    	}
    	else
    	{
    		//FileUtils.deleteDirectory(destinationFolder);
    		//FileUtils.forceMkdir(destinationFolder);
    	}
    	
      iter = hashList.iterator();
      key = "";
      refs = 0;
      while(iter.hasNext())
      { 
	      Map.Entry e=(Map.Entry)iter.next(); 
	      key = (String)e.getKey();
	      refs = ((Integer)e.getValue()).intValue();
	      
	      if (refs > 0)
	      {
	      	String orig = imageFolder.getAbsolutePath() + "\\" + key;  
	      	String dest = destinationFolder.getAbsolutePath() + "\\" + key;   

	      	File fOrig = new File(orig);  
	      	File fDest = new File(dest); 
	      	//FileUtils.copyFile(fOrig, fDest); 
	      }
      }

      
      Predesigner.getFrame().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
    catch (Exception ex)
    {
      Predesigner.getFrame().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "Couldn't create log file./n/nOperation aborted.",
        "Canceled", JOptionPane.ERROR_MESSAGE);
      System.out.println(ex.getMessage());
      return;
    }
    
    switch (errors)
    {
      case 0:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "Success! " + successes + " XML files were processed.\n\nCheck " + logFile.getAbsolutePath() + " for details.",
            "No Errors", JOptionPane.INFORMATION_MESSAGE);  
        break;
      case 1:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There was an error.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "ERROR", JOptionPane.ERROR_MESSAGE);
        break;
      default:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There were " + errors + " errors.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "MULTIPLE ERRORS", JOptionPane.ERROR_MESSAGE);  
    }
  }
  
  private void onConvertEPS()
  {
  	// get file to convert
    //File folder = new File(Config.getProjectHome());
    File imageFolder = new File(Config.getDefaultImageGallery() + "/HighRes");
    JFileChooser dlg = new JFileChooser(imageFolder);
    FileNameExtensionFilter filter = new FileNameExtensionFilter("EPS Images", "eps");
    dlg.setFileFilter(filter);

    dlg.setDialogTitle("Select an EPS file to convert to PNG");
    dlg.setApproveButtonText("Select");
    dlg.setCurrentDirectory(imageFolder);
    dlg.setFileSelectionMode(JFileChooser.FILES_ONLY);

    if (dlg.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
    {
      File epsFile = dlg.getSelectedFile();
    	String epsFileName = epsFile.getAbsolutePath().toLowerCase();
      System.out.println(epsFileName);
      if (epsFile != null && epsFile.exists() && epsFileName.endsWith("eps"))
      {
      	// use C# console app to convert EPS to PNG in same folder
      	String pngFileName = epsFileName.replace(".eps", ".png");
      	try
      	{
      		ArrayList<String> commands = new ArrayList<String>();
      		commands.add("EPSTo.exe");
      		commands.add(epsFileName);
      		commands.add(pngFileName);
      		Process proc = new ProcessBuilder(commands).start();
      	}
      	catch (Exception e)
      	{}
      }
      else
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), "Did not select EPS file, abort.",
            "error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
    else
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "No EPS file converted.",
        "Canceled", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
  }
  
  private void onProdListXref()
  {
  	// generate simple xref tables
  	File folder = Config.productGroupListFile.getParentFile();
    String timeStamp = AveryUtils.timeStamp().replaceAll(":", ".");
    timeStamp = timeStamp.replaceAll(" ", "");
    File logFile = new File(folder, timeStamp + "_ProductGroupListXREF.txt");
    try
    { // create log file and open log stream
      PrintWriter log = new PrintWriter(new FileOutputStream(logFile));
      log.println("Product Group\tBase SKU\tDescription\tNumMasters\tPages\tShape\tWidth\tHeight\tAspect\tArea");
      	
	  	// product group to base sku - product group list
	  	ConfigFileBrowsePanel productGroupsPanel = 
	  	    new ConfigFileBrowsePanel("Product Group List:", Config.productGroupListFile);
	  	File groupsFile = productGroupsPanel.getFile();
	    SAXBuilder builder = new SAXBuilder(true);
	    builder.setFeature("http://apache.org/xml/features/validation/schema", true);
	    builder.setProperty(
	        "http://apache.org/xml/properties/schema/external-schemaLocation",
	        Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 
	
	    ProductGroupList pgList = new ProductGroupList(builder.build(groupsFile).getRootElement());
	    
	    double pageArea = 193881600;
	    if (groupsFile.getName().indexOf("US_") < 0)
	    {
	    	if (groupsFile.getName().indexOf("A4_") >= 0)
	    		pageArea = 200456390;
	    }
	    //dlg.logComment("Creating blank predesigns for ProductGroupList " + groupsFile.getName());
	          
	    Iterator groups = pgList.iterator();
	    while (groups.hasNext())
	    {
	    	try
	    	{
		      ProductGroup group = (ProductGroup)groups.next();
		      String name = group.getName();
		      String baseSku = group.getBaseSku();
		      String description = group.getDescription();
		      description = description.substring(0, Math.min(60, description.length()));
			    File batFile = new File(Config.getBatDirectory(), group.getBatName());
	        Project bat = new Project();
	        bat.read(batFile);
	        int numMasters = bat.getMasterpanels().size();
	        int numPages = bat.getNumPages();
	        AveryMasterpanel mp = (AveryMasterpanel)bat.getMasterpanels().get(0);
	        String shape = mp.getShape().getShapeString();
	        double width = mp.getWidth();
	        double height = mp.getHeight();
	        double aspect = width / height;
	        double area = width * height / pageArea; // US LETTER 193881600; A4 200456390;
	        
		      log.println(name + "\t" + baseSku + "\t" + description + "\t" + numMasters + "\t" + numPages + "\t"
		      		+ shape + "\t" + width + "\t" + height + "\t" + aspect + "\t" + area);
	    	}
	    	catch (Exception e)
	    	{}
	    }
      log.close();
	    
      logFile = new File(folder, timeStamp + "_ProductListXREF.txt");
      log = new PrintWriter(new FileOutputStream(logFile));
	  	// product group to sku - product list
      log.println("Product\t\tSKU");
	  	ConfigFileBrowsePanel productPanel = 
	  	    new ConfigFileBrowsePanel("Product List:", Config.productListFile);
	  	File productsFile = productPanel.getFile();
	    ProductList pList = new ProductList(productsFile);
	    
	    Iterator products = pList.iterator();
	    //String uniqueName = "";
      //String name = "";
	    //String skus = "";
	    
	    ArrayList results = new ArrayList();
	    
	    while (products.hasNext())
	    {
	    	Product product = (Product)products.next();
	      String name = product.getProductGroupName();
	      String sku = product.getSkuName();
	      results.add(name + "\t" + sku);
	      /*if (uniqueName.equals(""))
	      	uniqueName = name;
	        
	      if (!name.equals(uniqueName))
	      {
	      	results.add(uniqueName + "\t" + skus);
	      	//log.println(uniqueName + "\t" + skus);
	      	skus = "";
	      	uniqueName = name;
	      }
	      skus += product.getSkuName() + " ";*/
	    }
      	
    	//results.add(name + "\t" + skus);
	    //log.println(name + "\t" + skus)
  
	    
	    Collections.sort(results);
	    String[] currentNameSku = ((String)results.get(0)).split("\t");
	    ArrayList skus = new ArrayList();
	    
	    Iterator iter = results.iterator();
	    while (iter.hasNext())
	    {
		    String[] nameSku = ((String)iter.next()).split("\t");
		    if (nameSku[0].equals(currentNameSku[0]))
		    {
		    	skus.add(nameSku[1]);
		    }
		    else
		    {
		    	logNameSkus(log, currentNameSku[0], skus);
		    	currentNameSku[0] = nameSku[0];
		    	skus = new ArrayList();
		    	skus.add(nameSku[1]);
		    }
	    	//log.println((String)iter.next());
	    }
	    
    	logNameSkus(log, currentNameSku[0], skus);
	    
	    log.close();
	    
    }
    catch (Exception e)
    {}	
  }
  
  private void onCreateAIProductList()
  {
    try
    { // create log file and open log stream
    	File folder = Config.productGroupListFile.getParentFile();
      	
    	
	  	ConfigFileBrowsePanel productPanel = 
	  	    new ConfigFileBrowsePanel("Product List:", Config.productListFile);
	  	File productsFile = productPanel.getFile();
	    ProductList pList = new ProductList(productsFile);
	    


	  	// product group to base sku - product group list
	  	ConfigFileBrowsePanel productGroupsPanel = 
	  	    new ConfigFileBrowsePanel("Product Group List:", Config.productGroupListFile);
	  	File groupsFile = productGroupsPanel.getFile();
	    SAXBuilder builder = new SAXBuilder(true);
	    builder.setFeature("http://apache.org/xml/features/validation/schema", true);
	    builder.setProperty(
	        "http://apache.org/xml/properties/schema/external-schemaLocation",
	        Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 
	
	    ProductGroupList pgList = new ProductGroupList(builder.build(groupsFile).getRootElement());
	    
	    //dlg.logComment("Creating blank predesigns for ProductGroupList " + groupsFile.getName());
	          
	    
      XMLOutputter serializer = new XMLOutputter(" ", true);
      Element rootElement = new Element("productList");
      Document doc = new Document(rootElement);
      
	    ArrayList pgl = new ArrayList();

	    Iterator groups = pgList.iterator();
	    
	    while (groups.hasNext())
	    {
	      ProductGroup group = (ProductGroup)groups.next();
	      String name = group.getName();
	      String baseSku = group.getBaseSku();
	      String description = group.getDescription();
	      String productType = group.getProductType();
	      String size = "";
		    Iterator products = pList.iterator();
		    while (products.hasNext())
		    {
		    	Product product = (Product)products.next();
		      //String name = product.getProductGroupName();
		      String sku = product.getSkuName();
		      if (baseSku.equals(sku))
		      {
		      	size = product.getSizeDescription();
		      	if (size == null)
		      		size = "";
		      	break;
		      }
		    }
	      description = description.substring(0, Math.min(60, description.length()));
	      String pglstr = productType + "\t" + baseSku + "\t" + size + "\t" + description + "\t" + name;
	      pgl.add(pglstr);
	    }
	    Collections.sort(pgl);
	    
	    Iterator pg = pgl.iterator();
	    
	    String catName = "";
    	Element cat = null;
    	
	    while (pg.hasNext())
	    {
	    	String pglstr = (String)pg.next();
	    	String [] split = pglstr.split("\t");
      	if (!catName.equals(split[0]))
      	{
      		if (!catName.equals(""))
      			rootElement.addContent(cat);  	
      		cat = new Element("category");
        	cat.setAttribute("name", split[0], null);
        	catName = split[0];
      	}
      	Element base = new Element("base");
      	base.setAttribute("sku", split[1], null);
      	if (split[2].length() > 0)
      		base.setAttribute("size", split[2], null);
      	base.setAttribute("description", split[3], null);
      	cat.addContent(base);
      	
      	// go find all matching skus
	    	String productGroupName = split[4];
		    Iterator products = pList.iterator();
		    while (products.hasNext())
		    {
		    	Product product = (Product)products.next();
		      //String name = product.getProductGroupName();
		      String sku = product.getSkuName();
		      String pgn = product.getProductGroupName();
		      //System.out.println("pgn=" + pgn);
		      
		      if (!sku.equals(split[1]) && pgn.equals(productGroupName))
		      {
			      //System.out.println("sku=" + split[1]);
		      	Element skuElement = new Element("sku").addContent(sku);
		      	base.addContent(skuElement);
		      }
		    }      	
	    }
	    if (cat != null)
	    	rootElement.addContent(cat);    	
      File xmlOut = new File(folder + "/productListAI.xml");
      FileOutputStream fos = new FileOutputStream(xmlOut);
      serializer.output(doc, fos);

    }
    catch (Exception e)
    {}		
  }
  

  private void logNameSkus(PrintWriter log, String name, ArrayList skus)
  {
  	Collections.sort(skus);
  	String sSkus = "";
  	Iterator iter = skus.iterator();
  	int count = 0;
  	while (iter.hasNext())
  	{
  		if (count++ >= 12)
  		{
  			count = 0;
  	  	log.println(name + "\t" + sSkus);
  	  	sSkus = "";
  		}
  		sSkus += (String)iter.next() + ",";
  	}
  	int index = sSkus.lastIndexOf(",");
  	if (index > 0)
  		sSkus = sSkus.substring(0, index);
  	log.println(name + "\t" + sSkus);
  	log.println("");
  }
  
  private void onBatchPreviews(boolean doPages)
  {   
    File folder = new File(Config.getProjectHome());
    
    JFileChooser dlg = new JFileChooser(folder.getParent());
    dlg.setDialogTitle("Select a FOLDER containing XML project files");
    dlg.setApproveButtonText("Select");
    dlg.setSelectedFile(folder);
    dlg.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    if (dlg.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
    {
      File newFolder = dlg.getSelectedFile();
      if (newFolder != null && newFolder.exists() && newFolder.isDirectory())
      {
        folder = newFolder;
      }
      else
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), newFolder + " is not acceptable.  Command aborted.",
          "error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
    else
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "No files processed.",
        "Canceled", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    int errors = 0;
    int successes = 0;
    File logFile;
    File previewLogFile;
    
    try
    { 
      String separator = System.getProperty("file.separator");
      
      File[] files = folder.listFiles(new java.io.FileFilter() {
        public boolean accept(File pathname) {
          return (pathname.getName().endsWith(".xml"));
        }
      });
      
      Predesigner.getFrame().setCursor(new Cursor(Cursor.WAIT_CURSOR));
      
      if (files.length > 0)
      {
      	String previewpath = files[0].getPath();
      	previewpath = previewpath.substring(0, previewpath.lastIndexOf(separator)) + separator + "Preview";
      	File preview = new File(previewpath);
      	if (!(preview.exists()))
      	{
      		preview.mkdir();
      	}
      }
      
      if (doPages)
      	logFile = new File(folder + "/Preview", "PageThumbBatch.log");
      else
      	logFile = new File(folder + "/Preview", "PanelThumbBatch.log");
   
      // create log file and open log stream
      if (logFile.exists())
      {
        logFile.delete();   // start fresh each time
      }
      PrintWriter log = new PrintWriter(new FileOutputStream(logFile));
      log.println(AveryUtils.timeStamp() + " - " + folder.getAbsolutePath());
      
      previewLogFile = new File(folder + "/Preview", "PreviewFiles.tsv");
      // create log file and open log stream
      if (previewLogFile.exists())
      {
      	previewLogFile.delete();   // start fresh each time
      }
      PrintWriter previewLog = new PrintWriter(new FileOutputStream(previewLogFile));
         
      int i = 0;
      for (i = 0; i < files.length; ++i)
      {
        File xmlfile = files[i];
        String prefix = doPages ? "PG_" : "MP_";
        
        String previewfilepath = xmlfile.getPath();
        previewfilepath = previewfilepath.substring(0, previewfilepath.lastIndexOf(separator)) + separator + "Preview" + separator;
        String previewfilename = xmlfile.getName();
        
        previewfilename = prefix + previewfilename.substring(0, previewfilename.lastIndexOf('.')) + ".jpg";
        String previewname = previewfilepath + previewfilename;
        
        try
        {          
          Project project = new Project();
          project.read(xmlfile);
          // fixup image galleries
          int orphans = project.fixGalleries(Config.getDefaultImageGallery());

          if (orphans != 0 && new File(Config.getProjectHome(), "HighRes").exists())
          {
            orphans = project.fixGalleries(Config.getProjectHome());
          }
          
          if (orphans != 0)  // still! We've failed.
          {
            throw new Exception("image(s) not found");
          }
          
          // write to previewLog
          String cat = project.getDesignCategory();
          if (cat == null || cat.length() <= 0)
          	cat = "";
          String ver = project.getVersion();
          if (ver == null || ver.length() <= 0)
          	ver = "1.0.0";
          
          String previewLine = xmlfile.getName() + "\t" +
																project.getProductGroup() + "\t" + 
           											project.getLanguage() + "\t" +
           											ver + "\t" +
          	 										project.getDesignTheme() + "\t" +
          	 										cat;
          
           previewLog.println(previewLine);
          //previewLog.println(previewLine);
          
          if (doPages)
          {
          	project.savePageThumbnail(new File(previewname));
          }
          else
          {
          	project.saveMasterpanelThumbnail(new File(previewname));          	
          }
          ++successes;
        }
        catch (Exception ex)  // log failure
        {
          log.println("\tTHUMBNAIL ERROR on - " + xmlfile.getName() + " - " + ex.toString());
          ex.printStackTrace();
          ++errors;
          continue;
        }
        //  log success
        log.println("\twrote " + previewname);
      }
    
      // close logs
      log.flush();
      log.close();

      previewLog.flush();
      previewLog.close();
     
      Predesigner.getFrame().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
    catch (Exception ex)
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "Couldn't create log file./n/nOperation aborted.",
        "Canceled", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    switch (errors)
    {
      case 0:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "Success! " + successes + " XML files were processed.\n\nCheck " + logFile.getAbsolutePath() + " for details.",
            "No Errors", JOptionPane.INFORMATION_MESSAGE);  
        break;
      case 1:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There was an error.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "ERROR", JOptionPane.ERROR_MESSAGE);
        break;
      default:
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            "There were " + errors + " errors.\nCheck " + logFile.getAbsolutePath() + " for details.",
            "MULTIPLE ERRORS", JOptionPane.ERROR_MESSAGE);  
    }
  }
  
 void onSelectStyleSet()
  {
    class SSFileFilter extends javax.swing.filechooser.FileFilter
    {
      public boolean accept(File f)   {
        return (f.getName().endsWith(".ss"));
      }
      public String getDescription()    {
        return "Avery Style Set file (*.ss)";
      }
    }
    
    JFileChooser dlg = new JFileChooser(Config.getStyleSetsDir());
    FileFilter ssFilter = new SSFileFilter();
    dlg.addChoosableFileFilter(ssFilter);
    dlg.setFileFilter(ssFilter);
    dlg.setFileSelectionMode(JFileChooser.FILES_ONLY);
    dlg.setDialogTitle("Select Style Set");

    if (dlg.showOpenDialog(Predesigner.frame) == JFileChooser.APPROVE_OPTION)
    {
      File file = dlg.getSelectedFile();
      try
      {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
        Object ss = ois.readObject();
        ois.close();
        StyleSet newStyle = (StyleSet)ss;

        if (Predesigner.getProject() != null)
        {
          Predesigner.getProject().replaceStyle(Predesigner.styleSet, newStyle);
          Predesigner.frame.updateView();
          Predesigner.styleSet = newStyle;
        }
        else
        {
          Predesigner.styleSet = newStyle;
        }
      }
      catch (Exception ex)
      {
        JOptionPane.showMessageDialog(Predesigner.frame, ex.getMessage(), 
            "It didn't work!", JOptionPane.ERROR_MESSAGE);
      }
    }
  }
  
  private void onCreateStyleSet()
  {
    StyleSet ss = StyleSet.newStyleSet();
    ss.name = "Enter name";
    ss.description = "Enter description";
    StyleSetEditorPanel panel = new StyleSetEditorPanel(ss);
    while (JOptionPane.showConfirmDialog(Predesigner.frame, panel, "Create Style Set",
        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
        == JOptionPane.OK_OPTION)
    {
      // only alphanumeric chars allowed in filename
      String filename = StyleSet.constructFilename(panel.getStyleSetName());
      if (new File(Config.getStyleSetsDir(), filename).exists())
      {
        JOptionPane.showMessageDialog(Predesigner.frame, 
            filename + " already exists.  Use a different name for your Style Set.", 
            "Duplicate Style Set Filename", JOptionPane.ERROR_MESSAGE);
      }
      else if (updateStyleSetFromPanelInputs(panel))
      {
        return;   // done
      }
    }
  }
  
  private void onEditStyleSet()
  {
    StyleSetEditorPanel panel = new StyleSetEditorPanel(Predesigner.styleSet);
    while (JOptionPane.showConfirmDialog(Predesigner.frame, panel, "Edit Style Set", 
        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
        == JOptionPane.OK_OPTION)
    {
      if (updateStyleSetFromPanelInputs(panel))
        break;   // done
    }
  }
  
  /**
   * Creates a StyleSet from the editor's inputs, writes it to the StyleSets directory,
   * and sets it as the current StyleSet for the session
   * @param panel - the editor
   * @return true on success, false if something went wrong (an error message has been displayed)
   */
  private boolean updateStyleSetFromPanelInputs(StyleSetEditorPanel panel)
  {
    StyleSet ss = StyleSet.newStyleSet();
    panel.updateStyleSet(ss);
    
    try
    {
      File file = new File(Config.getStyleSetsDir(), ss.constructFilename());
      ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
      oos.writeObject(ss);
      oos.close();
      Predesigner.styleSet = ss;
      if (Predesigner.getProject() != null)
      {
        Predesigner.getProject().setDesignTheme(ss.name);
      }
      return true;
    }
    catch (IOException iox)
    {
      JOptionPane.showMessageDialog(Predesigner.frame, iox.getMessage(), 
          "It didn't work!", JOptionPane.ERROR_MESSAGE);
      
      return false;
    }
  }
  
  private void translationMultiplier(ActionEvent e)
  throws IOException, ZipException, JDOMException
  {
    MultiplierDialog dlg = new MultiplierDialog("Localized Predesign Generator"); 
    LanguageSelector selector = dlg.new LanguageSelector();
    
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), selector, 
        "Select Language Code", JOptionPane.OK_CANCEL_OPTION, 
        JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
    {
      String from = selector.getFromLanguage();
      String to = selector.getToLanguage();
      if (from.equals(to))
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), "Cannot translate to the same language");
        translationMultiplier(e);   // try again
        return;
      }
      
      // user selects source directory
      File sourceDir = new File(Config.getProjectHome());
      JFileChooser dirSelectorDialog = new JFileChooser(sourceDir);
      dirSelectorDialog.setDialogTitle("Select source projects folder.");
      dirSelectorDialog.setApproveButtonText("Select");
      dirSelectorDialog.setSelectedFile(sourceDir);
      dirSelectorDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      
      if (dirSelectorDialog.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
      {
    	  sourceDir = dirSelectorDialog.getSelectedFile();
    	  if (sourceDir == null || !sourceDir.exists() || !sourceDir.isDirectory())
    	  {
    		  JOptionPane.showMessageDialog(Predesigner.getFrame(), "Problem selecting " + sourceDir + " directory",
    				  "Error", JOptionPane.ERROR_MESSAGE);
    		  return;
    	  }
      }
      else
      {
    	  JOptionPane.showMessageDialog(Predesigner.getFrame(), "Action was canceled.",
				  "Canceled", JOptionPane.INFORMATION_MESSAGE);
    	  return;
      }
      
      // user selects output directory
      File outputDir = new File(Config.getProjectHome());
      dirSelectorDialog.setDialogTitle("Select output folder.");
      dirSelectorDialog.setApproveButtonText("Select");
      dirSelectorDialog.setSelectedFile(outputDir);
      dirSelectorDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      
      if (dirSelectorDialog.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
      {
    	  outputDir = dirSelectorDialog.getSelectedFile();
    	  if (outputDir == null || !outputDir.exists() || !outputDir.isDirectory() || !outputDir.canWrite())
    	  {
    		  JOptionPane.showMessageDialog(Predesigner.getFrame(), "Problem selecting " + outputDir + " directory",
    				  "Error", JOptionPane.ERROR_MESSAGE);
    		  return;
    	  }
      }
      else
      {
    	  JOptionPane.showMessageDialog(Predesigner.getFrame(), "Action was canceled.",
				  "Canceled", JOptionPane.INFORMATION_MESSAGE);
    	  return;
      }

      
      // language subfolder is created
      String targetDir = outputDir.getPath() + "/" + to;
      
      try
			{
      	File target = new File(targetDir);
      	if (!target.exists())
      	{
      		target.mkdir();
      	}
      	else if (!target.isDirectory() || !target.canWrite())
      	{
          JOptionPane.showMessageDialog(Predesigner.getFrame(), "Cannot use " + targetDir + " directory");
          return;
      	}
			}
      catch (SecurityException ex)
			{
      	JOptionPane.showMessageDialog(Predesigner.getFrame(), ex);
			}
      
      Hashtable translations = new SampleText().translationTable(from, to);
      ProductGroupList productGroups = Predesigner.getProductGroupList();
      
      dlg.logComment("Started " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
      dlg.logComment("New files generated in \"" + selector.getToLanguage() + "\" language");
      dlg.setVisible(true);
      dlg.makeBusy(true);
      
      File[] files = new File(sourceDir.getPath()).listFiles();
      for (int i = 0; i < files.length; ++i)
      {
        dlg.makeBusy(true);   // guarantee busy cursor
        
        File file = files[i];
        if (file.getName().toLowerCase().endsWith(".xml"))
        {
          try
          {
            Project project = new Project();
            project.read(file);
            if (!from.equals(project.getLanguage()))
            {
              continue;   // wrong language - ignore this project
            }
            
            // always attempt the translation and save the project in the new language...
            // ...even if there are custom field or translation issues
            project.setLanguage(to);
            int countTranslationFailures = project.translateText(translations);
             
						// the draw stuff is used to make text boxes perform overflow
						// write the xml only if the project is draw true and any text box size
						// changed as a result of drawing all pages.						
					  // draw pages to ensure text font size fits box
						int j = 1;
						Iterator pageIterator = project.getPages().iterator();
						while (pageIterator.hasNext())
						{
							AveryPage page = (AveryPage)pageIterator.next();
							int pageWidth = (int)(page.getWidth().doubleValue() * .05);
							project.makePagePreview(j++, pageWidth);
						}
				
            // construct outputFilename
            String outputFilename = to + ((file.getName().startsWith(from))
              ? file.getName().substring(from.length()) 
              : "." + file.getName());

            File output = new File(targetDir, outputFilename);
            project.write(output);
            
            // log results
            dlg.logProject(output);
            
            String nonLocalizableCount = countTranslationFailures + " non-localizable text field(s))";
            if (project.numberOfCustomFieldDescriptions() > 0)
            {
            	if (countTranslationFailures > 0)
            		throw new Exception("Translation failed (custom field descriptions & " + nonLocalizableCount);
              throw new Exception("Translation failed (custom field descriptions)");
            }
            
            if (countTranslationFailures > 0)
            {
              throw new Exception("Translation failed (" + nonLocalizableCount);
            }           
          }
          catch (Exception ex)
          {
            // log translation error or any I/O exception
            dlg.logError(ex.getMessage(), file);
          }
        }
      } // end files[] loop
      
      dlg.logComment("Finished " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
      dlg.makeBusy(false);
    }
  }
  
  private void styleMultiplier(ActionEvent e)
  throws JDOMException, IOException
  {
    MultiplierDialog dlg = new MultiplierDialog("Styled Predesign Generator - " + Predesigner.styleSet.name); 
    ProductTypeSelector selector = dlg.new ProductTypeSelector(Predesigner.getProductGroupList());
    
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), selector, 
        "Multiply " + Predesigner.styleSet.name, JOptionPane.OK_CANCEL_OPTION, 
        JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
    {
      Object[] prefixes = selector.getSelectedPrefixes();
      
      if (prefixes.length < 1)
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), "No ProductType was selected.");
        styleMultiplier(e);   // try again
        return;
      }
      
      
      // User selects placeholder directory
      File placeholdersDir = new File(Config.getProjectHome());
      JFileChooser dirSelectorDialog = new JFileChooser(placeholdersDir);
      dirSelectorDialog.setDialogTitle("Select placeholders folder.");
      dirSelectorDialog.setApproveButtonText("Select");
      dirSelectorDialog.setSelectedFile(placeholdersDir);
      dirSelectorDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      
      if (dirSelectorDialog.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
      {
    	  placeholdersDir = dirSelectorDialog.getSelectedFile();
    	  if (placeholdersDir == null || !placeholdersDir.exists() || !placeholdersDir.isDirectory())
    	  {
    		  JOptionPane.showMessageDialog(Predesigner.getFrame(), "Problem selecting " + placeholdersDir + " directory",
    				  "Error", JOptionPane.ERROR_MESSAGE);
    		  return;
    	  }
      }
      else
      {
    	  JOptionPane.showMessageDialog(Predesigner.getFrame(), "Action was canceled.",
				  "Canceled", JOptionPane.INFORMATION_MESSAGE);
    	  return;
      }
      
      // User selects output directory
      File outputDir = new File(Config.getProjectHome());
      dirSelectorDialog.setDialogTitle("Select output folder.");
      dirSelectorDialog.setApproveButtonText("Select");
      dirSelectorDialog.setSelectedFile(outputDir);
      dirSelectorDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      
      if (dirSelectorDialog.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
      {
    	  outputDir = dirSelectorDialog.getSelectedFile();
    	  if (outputDir == null || !outputDir.exists() || !outputDir.isDirectory() || !outputDir.canWrite())
    	  {
    		  JOptionPane.showMessageDialog(Predesigner.getFrame(), "Problem selecting " + outputDir + " directory",
    				  "Error", JOptionPane.ERROR_MESSAGE);
    		  return;
    	  }
      }
      else
      {
    	  JOptionPane.showMessageDialog(Predesigner.getFrame(), "Action was canceled.",
				  "Canceled", JOptionPane.INFORMATION_MESSAGE);
    	  return;
      }
      
      
      // Styleset subfolder is created
      String targetDir = outputDir.getPath() + "/" + AveryUtils.alphaNumeric(Predesigner.styleSet.name);      
      try
      {
        File target = new File(targetDir);
        if (!target.exists())
        {
          target.mkdir();
        }
        else if (!target.isDirectory() || !target.canWrite())
        {
          JOptionPane.showMessageDialog(Predesigner.getFrame(), "Cannot use " + targetDir + " directory");
          return;
        }
      }
      catch (SecurityException ex)
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), ex);
      }
      
      ProductGroupList productGroups = Predesigner.getProductGroupList();
      
      dlg.logComment("Started " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
      dlg.logComment("Input: placeholder folder = " + placeholdersDir.getPath());
      String comment = "ProductTypes:";
      for (int n = 0; n < prefixes.length; ++n)
      {
        comment += " " + prefixes[n];
      }
      dlg.logComment(comment);
      dlg.logComment("Output: Style Set folder = " + targetDir);
      dlg.logComment("New files generated with \"" + Predesigner.styleSet.name + "\" Style Set");
      dlg.setVisible(true);
      dlg.makeBusy(true);
      
      File[] files = new File(placeholdersDir.getPath()).listFiles(
        new java.io.FileFilter(){
          public boolean accept(File f) { return f.getName().endsWith(".xml"); }
        });
      
      StyleSet placeholder = StyleSet.newStyleSet();
      
      for (int i = 0; i < files.length; ++i)
      {
        File file = files[i];
        dlg.makeBusy(true);   // guarantee busy cursor       
        try
        {
          SAXBuilder builder = new SAXBuilder(true);
          builder.setFeature("http://apache.org/xml/features/validation/schema", true);
          builder.setProperty(
              "http://apache.org/xml/properties/schema/external-schemaLocation",
              Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 

          AveryProject project = new AveryProject(builder.build(file).getRootElement());
          
          String designTheme = project.getDesignTheme();
          if (designTheme == null || designTheme.equals("placeholder"))
          {
            ProductGroup productGroup = productGroups.getProductGroup(project.getProductGroup());
            if (productGroup == null)
            {
              // dlg.logError("not included in productGroupList - skipped", files[i]);
              continue;
            }
            
            if (selector.excludeTextOnlyProjects.isSelected())
            {
              boolean allText = true;
              Iterator fields = project.getAllPanelFields().iterator();
              while (fields.hasNext())
              {
                if ((fields.next() instanceof AveryTextfield) == false)
                {
                  allText = false;
                  break;
                }
              }
              
              if (allText)
              {
                dlg.logError("text project not converted", files[i]);
                continue;
              }
            }
            
            for (int j = 0; j < prefixes.length; ++j)
            {
              String prefix = (String)prefixes[j];
              if (prefix != null && productGroup.getProductType().startsWith(prefix))
              {
                if (!project.isTruePlaceholderPredesign())
                {
                  dlg.logError("not converted", files[i]);
                  break;  // break out of prefixes[j] loop
                }
                  // apply StyleSet
                project.replaceStyle(placeholder, Predesigner.styleSet);
                
                // output the modified file
                File output = autoFile(project, productGroup.getDescription(), targetDir);
                
                Document document = new Document();
                document.setRootElement(project.getAverysoftElement());

                FileOutputStream out = new FileOutputStream(output);
                new XMLOutputter(" ", true).output(document, out);
                out.flush();
                out.close();
                  
                  // log results
                dlg.logProject(output);
                break;  // break out of prefixes[j] loop
              }
            }
          }
        }
        catch (Exception ex)
        {
          // log any exception
          dlg.logError(ex.getMessage(), file);
          System.out.println(ex);
          System.out.println("  " + ex.getStackTrace()[0]);
          System.out.println("  " + ex.getStackTrace()[1]);
          System.out.println("  " + ex.getStackTrace()[2]);
        }        
      } // end files[] loop
      
      dlg.makeBusy(false);
    }
  }
  
  /**
   * a menu command to generate blank predesigns from a ProductGroupList,
   * a language and a directory of BATs
   * @param e
   * @throws IOException
   * @throws JDOMException
   * @throws Exception
   */
  private void blankPredesignCreator(ActionEvent e)
  throws IOException, JDOMException, Exception
  {
    BlankPredesignCreator creator = new BlankPredesignCreator();
    
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), creator, 
        "Define Batch Creation Process", JOptionPane.OK_CANCEL_OPTION, 
        JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
    {
      creator.makeBlankPredesigns();
    }
  }
  
  private void multiplyMasters(ActionEvent e)
  throws IOException, JDOMException, Exception
  {
/*    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), creator, 
        "Define Batch Creation Process", JOptionPane.OK_CANCEL_OPTION, 
        JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
    {
    }*/
  	
  	String phrase1 = "Multiply masterpanels converts page panels to masterpanels.\nUse it to edit page panels. \nUnmultiply masterpanels is used after editing is complete.\n\n";
  	String phrase2 = "Intended for blank projects, will remove all objects from predesigns.\nLimited to projects with four or fewer original masterpanels.\n\n";
  	String phrase3 = "Cannot edit projects saved after using unmultiply masterpanels.\nEdit saved multiplied project and unmultiply it when done editing the project.\n\n";
  	JOptionPane.showMessageDialog(Predesigner.getFrame(), phrase1 + phrase2 + phrase3);
  	
  	
  	Project project = Predesigner.getProject();
  	if (project != null)
  	{
  		boolean result = project.multiplyMasters();
  		if (result)
  		{
  			Predesigner.frame.rebuildMasterpanelPopupMenu(project);
  			Predesigner.frame.updateView();
  		}
  	}
  }
  
  private void unmultiplyMasters(ActionEvent e)
  throws IOException, JDOMException, Exception
  {
/*    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), creator, 
        "Define Batch Creation Process", JOptionPane.OK_CANCEL_OPTION, 
        JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
    {
    }*/
  	
  	Project project = Predesigner.getProject();
  	if (project != null)
  	{
    	String phrase1 = "Unmultiply masterpanels removes masterpanels added with Multiply masterpanels and moves all objects to their page panels.\n\n";
    	String phrase2 = "Do not apply any edits to this project, you should only save it or view its pages.\n\n";
    	String phrase3 = "IMPORTANT!! Save it with a different name than the multiply masterpanels version!\n\n";
    	JOptionPane.showMessageDialog(Predesigner.getFrame(), phrase1 + phrase2 + phrase3);
  		boolean result = project.unmultiplyMasters();
  		if (result)
  		{
  			Predesigner.frame.setCurrentMasterpanel(0);
  			Predesigner.frame.rebuildMasterpanelPopupMenu(project);
  			Predesigner.frame.updateView();
  		}
  	}
  }
  
  private void papersCreator(ActionEvent e)
  throws IOException, JDOMException, Exception
  {
    PapersCreator creator = new PapersCreator();
    
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), creator, 
        "Define Batch Creation Process", JOptionPane.OK_CANCEL_OPTION, 
        JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
    {
      creator.makePapers();
    }
  }
  
  private void onCompileStyleSetManifest()
  {
    try
    {
      File dir = new File(Config.getStyleSetsDir());
      File file = new File(Config.getStyleSetsDir(), "StyleSetCollection.txt");
      if (file.exists())
      {
        file.delete();
      }

      FileOutputStream fout = new FileOutputStream(file);
      PrintStream ps = new PrintStream(fout);
      ps.print("name\tfont\tdescription\tcolor1\tcolor2\tcolor3\t" +
          "graphic01\tgraphic02\tgraphic03\tgraphic04\tgraphic05\tgraphic06\t" +
          "graphic07\tgraphic08\tgraphic09\tgraphic10\tgraphic11\tgraphic12\n");
      
      File[] files = dir.listFiles();
      for (int i = 0; i < files.length; ++i)
      {
        if (files[i].getName().endsWith("ss"))
        {
          ObjectInputStream ois = new ObjectInputStream(new FileInputStream(files[i]));
          Object obj = ois.readObject();
          ois.close();
          StyleSet ss = (StyleSet)obj;
          ps.print(ss.name + "\t");
          ps.print(ss.font + "\t");
          ps.print(ss.description + "\t");
          ps.print(colorString(ss.color1) + "\t");
          ps.print(colorString(ss.color1) + "\t");
          ps.print(colorString(ss.color1) + "\t");
          ps.print(ss.graphic01 + "\t");
          ps.print(ss.graphic02 + "\t");
          ps.print(ss.graphic03 + "\t");
          ps.print(ss.graphic04 + "\t");
          ps.print(ss.graphic05 + "\t");
          ps.print(ss.graphic06 + "\t");
          ps.print(ss.graphic07 + "\t");
          ps.print(ss.graphic08 + "\t");
          ps.print(ss.graphic09 + "\t");
          ps.print(ss.graphic10 + "\t");
          ps.print(ss.graphic11 + "\t");
          ps.print(ss.graphic12 + "\n");
        }
      }
      ps.flush();
      ps.close();
      fout.close();
      
      JOptionPane.showMessageDialog(Predesigner.getFrame(), file.getAbsolutePath() + " generated", 
          "Saved", JOptionPane.INFORMATION_MESSAGE);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      JOptionPane.showMessageDialog(Predesigner.getFrame(), ex.getMessage(), ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
    }
  }
  
  // creates Custom Panel Groups by creating additional master panels
  // and assigning them to existing page panels
  private void onCreateCustomPanelGroups()
  {
  	Project project = Predesigner.getProject();

  	if (project != null)
  	{
	  	int numMasters = project.getMasterpanels().size();
	  	int numPages = project.getNumPages();
	  	if (numMasters < 3 && numMasters > 0 && numPages < 3 && numPages > 0 )
	  		getCustomPanelGroups(project);
  	}
  }

  // ui to gather custom panel groups
  private void getCustomPanelGroups(AveryProject project)
  {
  	int masterPagePanels = 0;
  	Iterator pageIterator = project.getPages().iterator();
  	Iterator mpIterator = project.getMasterpanels().iterator();
  	
  	String mpID = ((AveryMasterpanel)(mpIterator.next())).getID();
  	
  	while(pageIterator.hasNext())
  	{
  		AveryPage page = (AveryPage)pageIterator.next();
  		Iterator panelIterator = page.getPanels().iterator();
  		while (panelIterator.hasNext())
  		{
  			AveryPanel panel = (AveryPanel)panelIterator.next();
  			if (panel.getMasterID().equals(mpID))
  				masterPagePanels++;
  		}		
  	}
  	int mp1Panels = masterPagePanels;
  	masterPagePanels = 0;
  	int mp2Panels = 0;
  	if (mpIterator.hasNext())
  	{
  		pageIterator = project.getPages().iterator();
  		
  		mpID = ((AveryMasterpanel)(mpIterator.next())).getID();
    	while(pageIterator.hasNext())
    	{
    		AveryPage page = (AveryPage)pageIterator.next();
    		Iterator panelIterator = page.getPanels().iterator();
    		while (panelIterator.hasNext())
    		{
    			AveryPanel panel = (AveryPanel)panelIterator.next();
    			if (panel.getMasterID().equals(mpID))
    				masterPagePanels++;
    		}		
    	}
    	mp2Panels = masterPagePanels;
  	}
  	
	  CustomPanelGroupsDialog dlg = new CustomPanelGroupsDialog(Predesigner.frame, mp1Panels, mp2Panels);
	  dlg.setVisible(true);
	  
	  if (dlg.isOkay())
	  {
	  	ArrayList al1 = null;
	  	ArrayList al2 = null;
	  	
	  	if (mp1Panels > 0)
	  	{
	  		al1 = dlg.getCPG1();
	  	}
	  	if (mp2Panels > 0)
	  	{
	  		al2 = dlg.getCPG2();
	  	}
	  	if ((al1 == null || al1.size() < 2) && (al2 == null || al2.size() < 2))
	  		return;
	  	createCustomPanelGroups(al1, al2);
	  }  	
  }
  
  private void createCustomPanelGroups(ArrayList cpg1, ArrayList cpg2)
  {
  	Project project = Predesigner.getProject();
  	// this item operates on panel fields, requires panel fieldrefs to work
  	project.initializePanelDataLinking();

  	int numMasters = project.getMasterpanels().size();
  	int numPages = project.getNumPages();
  	
  	if (project != null && numMasters <= 2 && numPages <= 2 && project.isMergable())
  	{
  		ArrayList oldMasterPanels = new ArrayList(project.getMasterpanels());
  		Iterator masterIterator = oldMasterPanels.iterator();
   		
			ArrayList cpg = cpg1;
			
  		while (masterIterator.hasNext())
  		{
  			AveryMasterpanel masterPanel = (AveryMasterpanel)masterIterator.next();
  			//ArrayList cpg = getMasterPanelGroups(project, masterPanel);
  		
		    // alters the project by creating multiple master panels
  			// for the current master panel and updates
		    // updates the panel fieldref links accordingly
  			if (cpg != null && cpg.size() > 1)
  				project.customPanelGroups(cpg, masterPanel);
		    cpg = cpg2;
  		}	
    	project.setBaseLayout(project.getName());
    	
    	Iterator pageIterator = project.getPages().iterator();
    	while (pageIterator.hasNext())
  		{
  			AveryPage page = (AveryPage)pageIterator.next();
  			page.removeGridLayouts(null);
  		}
    	
    	project.setIsMergable(false);
    	
			Predesigner.frame.setCurrentMasterpanel(0);
			Predesigner.frame.rebuildMasterpanelPopupMenu(project);
			Predesigner.frame.updateView();
    	JOptionPane.showMessageDialog(Predesigner.getFrame(),
      "You MUST save this altered project with a new name\n"
    	+ "immediately before making any changes to it. Then\n"
    	+ "reopen the newly saved project and continue editing!");
  	}
  }
  
  File getProjectImage()
  {
    File file = new File(Config.getDefaultImageGallery() + "/HighRes", Predesigner.styleSet.graphic01);

    //if (!file.exists()) // styleset image doesn't exist
      // pick an image from the file system
    JFileChooser dlg = new JFileChooser(Config.getDefaultImageGallery() + "/HighRes");
    dlg.setFileFilter(new ImageFileFilter());
    dlg.setDialogTitle("Select an image file");
    dlg.setAcceptAllFileFilterUsed(false);
    
    ImageAccessory thumb = new ImageAccessory(dlg);
    dlg.setAccessory(thumb);
    dlg.addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, thumb);

    if (dlg.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
    {
      file = dlg.getSelectedFile();
    }
    else return null;  // user can't decide on an image
  	
    return file;
  }
  
  AveryImage createImage(File file, String description, int minSize)
  {
    AveryImage field = new AveryImage();
    //field.translateDescription(Predesigner.getProject().getLanguage());
    
    java.awt.image.BufferedImage bufferedImage = null;
    try
    {
      java.io.FileInputStream fis = new java.io.FileInputStream(file);
      if (file.getName().toLowerCase().endsWith(".png"))
      {
        bufferedImage = ImageIO.read(fis);
      }
      else
      {
        bufferedImage = com.sun.image.codec.jpeg.JPEGCodec.createJPEGDecoder(fis).decodeAsBufferedImage();
      }
    }
    catch (Exception e)
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      return null;
    }

    double scaleFactor = minSize / 1440;
    
    field.setSourceAndGallery(file.getName(), file.getParentFile().getParent());
    field.setWidth(new Double(bufferedImage.getWidth() * scaleFactor));
    field.setHeight(new Double(bufferedImage.getHeight() * scaleFactor));
    field.setDescription("Logo_" + description);
    field.setPromptOrder("30.0");
    field.setZLevel(30.0);
    field.setMovable(false);
    field.setEditable(false);
    
    return field;
  }
  
  void placeImageOnMaster(AveryMasterpanel masterpanel, AveryImage field, String horz, String vert)
  {
    double masterWidth = masterpanel.getWidth().doubleValue();
    double masterHeight = masterpanel.getHeight().doubleValue();
    int minSide = (int)Math.min(masterWidth, masterHeight);

    // make it actual size
    double imageAspect = (double)field.getWidth() / (double)field.getHeight();
    double height = field.getHeight() * 20; //masterHeight / 2.0;
    double width = field.getWidth() * 20; //height * imageAspect;
    
    if (width > masterWidth)
    {
      width = masterWidth / 2.0;
      height = width / imageAspect;
    }

   // default position to upper left corner
    int offset = 45 * (minSide / 1440);  //(this may be variable based on minimum dimension of panel)
    
    int x = offset;
    int y = offset;
    
    if (horz.equals("center"))
    {
    	x = (int)(((masterWidth - width) / 2));
    }
    else
    if (horz.equals("right"))
    {
    	x = (int)((masterWidth - width) - offset);
    }
 
    if (vert.equals("middle"))
    {
    	y = (int)(((masterHeight - height) / 2));
    }
    else
    if (vert.equals("bottom"))
    {
    	y = (int)(masterHeight - height - offset);
    }

    field.setWidth(new Double(width));
    field.setHeight(new Double(height));
    field.setPosition(new java.awt.Point(x, y));
    
    masterpanel.addField(field);

    /*if (editField(field, false))
    {
      // set promptOrder, ZOrder
      endOfPromptOrder(field);
      endOfZOrder(field);

      Predesigner.getProject().addFieldToMasterpanel(field, masterpanel);
      Predesigner.getFrame().onFieldChanged();
    }*/

  	
  }
    /*AveryMasterpanel masterpanel = Predesigner.getFrame().getCurrentMasterpanel();
    double masterWidth = masterpanel.getWidth().doubleValue();
    double masterHeight = masterpanel.getHeight().doubleValue();

    // calculate an appropriate size
    double imageAspect = (double)bufferedImage.getWidth() / (double)bufferedImage.getHeight();
    double height = masterHeight / 2.0;
    double width = height * imageAspect;
    if (width > masterWidth)
    {
      width = masterWidth / 2.0;
      height = width / imageAspect;
    }

    // set position to center of masterpanel
    int x = (int)(((masterWidth - width) / 2));
    int y = (int)(((masterHeight - height) / 2));
    
    if (masterpanel.getShape() instanceof PolygonShape)
    {
    	Rectangle2D rect = getPolyRect(masterpanel);
      height = rect.getHeight() / 2.0;
      width = height * imageAspect;
      if (width > rect.getWidth())
      {
        width = rect.getWidth() / 2.0;
        height = width / imageAspect;
      }
    	x = (int)(rect.getMinX() + (rect.getWidth() - width) / 2);
    	y = (int)(rect.getMinY() + (rect.getHeight() - height) / 2);
    }
    
    field.setWidth(new Double(width));
    field.setHeight(new Double(height));
    field.setPosition(new java.awt.Point(x, y));

    if (editField(field, false))
    {
      // set promptOrder, ZOrder
      endOfPromptOrder(field);
      endOfZOrder(field);

      Predesigner.getProject().addFieldToMasterpanel(field, masterpanel);
      Predesigner.getFrame().onFieldChanged();
    }
  }*/

  private void onPolygonFlipX()
  {
  	Project project = Predesigner.getFrame().getProject();
  	String productGroup = project.getProductGroup();
  	AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
  	AveryShape shape = master.getShape();
  	if (shape.getShapeString().equals("polygon") )
  	{
  		PolygonShape polygonShape = (PolygonShape)shape;
  		polygonShape.flipX(productGroup, shape.getHeight(), polygonShape.getPolypointsElementClone());
  	}
  }
  
  private void onPolygonFlipY()
  {
  	Project project = Predesigner.getFrame().getProject();
  	String productGroup = project.getProductGroup();
  	AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
  	AveryShape shape = master.getShape();
  	if (shape.getShapeString().equals("polygon") )
  	{
  		PolygonShape polygonShape = (PolygonShape)shape;
  		polygonShape.flipY(productGroup, shape.getWidth(), polygonShape.getPolypointsElementClone());
  	}
  }
  
  private void onPolygonRotateLeft90()
  {
  	Project project = Predesigner.getFrame().getProject();
  	String productGroup = project.getProductGroup();
  	AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
  	AveryShape shape = master.getShape();
  	if (shape.getShapeString().equals("polygon") )
  	{
  		PolygonShape polygonShape = (PolygonShape)shape;
  		polygonShape.rotateLeft90(productGroup, polygonShape.getPolypointsElementClone());
  	}  	
  }

  private void onPolygonRotateRight90()
  {
  	Project project = Predesigner.getFrame().getProject();
  	String productGroup = project.getProductGroup();
  	AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
  	AveryShape shape = master.getShape();
  	if (shape.getShapeString().equals("polygon") )
  	{
  		PolygonShape polygonShape = (PolygonShape)shape;
  		polygonShape.rotateRight90(productGroup, shape.getHeight(), polygonShape.getPolypointsElementClone());
  	}  	
  }

  private String colorString(Color color)
  {
  	String str = Integer.toHexString(color.getRGB());
    return str;
  }
  
  static class HashComparator implements Comparator
  {
  	public int compare(Object obj1, Object obj2)
  	{
	  	int result = 0;
	  	Map.Entry e1 = (Map.Entry)obj1 ;
	
	  	Map.Entry e2 = (Map.Entry)obj2 ;//Sort based on values.
	
	  	Integer value1 = (Integer)e1.getValue();
	  	Integer value2 = (Integer)e2.getValue();
	
	  	if (value1.compareTo(value2) == 0)
	  	{
	
	  		String word1 = (String)e1.getKey();
	  		String word2 = (String)e2.getKey();
	
	  		//Sort String in an alphabetical order
	  		result = word1.compareToIgnoreCase(word2);
	  	}
		  else
	  	{
	  		//Sort values in a descending order
	  		result = value1.compareTo( value2 );
	  	}
	
	  	return result;
  	}
  }
}
