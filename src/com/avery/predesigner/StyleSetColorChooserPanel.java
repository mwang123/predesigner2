/*
 * StyleSetColorChooserPanel.java Created on Jan 14, 2008 by leeb
 * Copyright 2008 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.colorchooser.AbstractColorChooserPanel;

import com.avery.predesign.StyleSet;

/**
 * @author leeb
 * Jan 14, 2008
 * StyleSetColorChooserPanel
 */
public class StyleSetColorChooserPanel extends AbstractColorChooserPanel
{  
  ColorPanel panel1 = new ColorPanel("Color-1", Predesigner.styleSet.color1);
  ColorPanel panel2 = new ColorPanel("Color-2", Predesigner.styleSet.color2);
  ColorPanel panel3 = new ColorPanel("Color-3", Predesigner.styleSet.color3);
  
  /**
   * default constructor
   *
   */
  StyleSetColorChooserPanel()
  { }

  /* (non-Javadoc)
   * @see javax.swing.colorchooser.AbstractColorChooserPanel#buildChooser()
   */
  protected void buildChooser()
  {
    setLayout(new BorderLayout(10, 10));
    add(panel1, BorderLayout.NORTH);
    add(panel2, BorderLayout.CENTER);
    add(panel3, BorderLayout.SOUTH);
  }

  /* (non-Javadoc)
   * @see javax.swing.colorchooser.AbstractColorChooserPanel#updateChooser()
   */
  public void updateChooser()
  {
    Color color = getColorFromModel();

    panel1.highlight(panel1.getColor().equals(color));
    panel2.highlight(panel2.getColor().equals(color));
    panel3.highlight(panel3.getColor().equals(color));
  }

  /* (non-Javadoc)
   * @see javax.swing.colorchooser.AbstractColorChooserPanel#getDisplayName()
   */
  public String getDisplayName()
  {
    return "Style Set";
  }
  
  /**
   * call this to change the colors to a specific styleSet
   * @param styleSet
   */
  void updateColors(StyleSet styleSet)
  {
    panel1.setColor(styleSet.color1);
    panel2.setColor(styleSet.color2);
    panel3.setColor(styleSet.color3);
  }

  private class ColorPanel extends JPanel
  {
    private JLabel label;
    private JButton swatch;
    
    ColorPanel(String name, Color color)
    {
      setBorder(BorderFactory.createLineBorder(Color.GRAY));
      
      label=new JLabel("     " + name);
      label.setPreferredSize(new Dimension(192,48));
      label.setHorizontalAlignment(JLabel.LEFT);
      add(label);
      
      swatch = new JButton();
      // defeat Windows XP L&F on the button
      swatch.setContentAreaFilled(false);
      swatch.setOpaque(true);
      // now, setting the background color should work on Windows XP
      swatch.setBackground(color);
      swatch.setPreferredSize(new Dimension(144, 48));
      add(swatch);
      
      swatch.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e)
        {
          getColorSelectionModel().setSelectedColor(getColor());
          updateChooser();
        }
      });
    }
    
    void highlight(boolean b)
    {
      swatch.setBorder(b ? BorderFactory.createMatteBorder(4,4,4,4,Color.RED) : null);
    }
    
    Color getColor()
    {
      return swatch.getBackground();
    }
    
    void setColor(Color c)
    {
      swatch.setBackground(c);
    }
  }
  

  /* (non-Javadoc)
   * @see javax.swing.colorchooser.AbstractColorChooserPanel#getLargeDisplayIcon()
   */
  public Icon getLargeDisplayIcon()
  {
    return null;
  }

  /* (non-Javadoc)
   * @see javax.swing.colorchooser.AbstractColorChooserPanel#getSmallDisplayIcon()
   */
  public Icon getSmallDisplayIcon()
  {
    return null;
  }
}
