package com.avery.predesigner;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import com.avery.utils.AveryUtils;
import com.borland.jbcl.layout.VerticalFlowLayout;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Configuration dialog for Predesigner</p>
 * <p>Copyright: Copyright 2003-2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

class ConfigurationDialog extends JDialog
{
	private static final long serialVersionUID = 3021373223659561337L;

	private boolean okay = false;

  private DirectoryPanel batPanel;
  private DirectoryPanel stencilsPanel;
  private DirectoryPanel projectsPanel;
  private DirectoryPanel galleryPanel;
  private DirectoryPanel autoTemplatesPanel;
  private DirectoryPanel styleSetsPanel;
  private DirectoryPanel masterpanelsPanel;
  private DirectoryPanel averyBundlePanel;
  private DirectoryPanel fontsPanel;
  
  private ConfigFileBrowsePanel productGroupsPanel;
  private ConfigFileBrowsePanel productsPanel;
  private ConfigFileBrowsePanel designCategoriesPanel;
  private ConfigFileBrowsePanel sampleTextPanel;
  private ConfigFileBrowsePanel mergeMapPanel;
  private ConfigFileBrowsePanel fontEncodingsPanel;
  private ConfigFileBrowsePanel familyGroupPanel;
  
  private ButtonGroup unitsGroup = new ButtonGroup();
  private JRadioButton twipsButton = new JRadioButton("twips");
  private JRadioButton inButton = new JRadioButton("inches");
  private JRadioButton cmButton = new JRadioButton("centimeters");
  
  private JSpinner bleedSpinner;
  private SpinnerNumberModel spinnerModel;

  ConfigurationDialog(Frame owner)
  {
    try
    {
    	batPanel = new DirectoryPanel("BATs Folder:", Config.getBatDirectory());
      batPanel.setToolTipText("location of Blank Avery Templates");
      
    	stencilsPanel = new DirectoryPanel("Stencils Folder:", Config.getStencilsDirectory());
    	stencilsPanel.setToolTipText("Stencils XML output folder");
      
      projectsPanel = new DirectoryPanel("Projects Folder:", Config.getProjectHome());
      projectsPanel.setToolTipText("Predesigner's XML output folder");
      
      galleryPanel = new DirectoryPanel("Default Images Folder:", Config.getDefaultImageGallery());
      galleryPanel.setToolTipText("parent of HighRes/LowRes image folders");
      
      autoTemplatesPanel = new DirectoryPanel("AutoTemplates Folder:", Config.getAutoTemplatesDir());
      autoTemplatesPanel.setToolTipText("Predesigner's Autotemplate input/output folder");
      
      styleSetsPanel = new DirectoryPanel("StyleSets Folder:", Config.getStyleSetsDir());
      styleSetsPanel.setToolTipText("Predesigner's StyleSet input/output folder");
      
      masterpanelsPanel = new DirectoryPanel("Masterpanels Folder:", Config.getMasterpanelsDir());
      masterpanelsPanel.setToolTipText("Predesigner's Masterpanel input/output folder");
      
      averyBundlePanel = new DirectoryPanel("Avery Bundles Folder:", Config.getAveryBundleDir());
      averyBundlePanel.setToolTipText("Predesigner's Avery Bundle output folder");
      
      fontsPanel = new DirectoryPanel("System Fonts Folder:", Config.getFontDirectory());
      fontsPanel.setToolTipText("location of TrueType fonts used for PDF output");

      productGroupsPanel = new ConfigFileBrowsePanel("Product Group List:", Config.productGroupListFile);
      
      productsPanel = new ConfigFileBrowsePanel("Product List:", Config.productListFile);

      designCategoriesPanel = new ConfigFileBrowsePanel("Design Use Categories:", Config.designCategoriesFile);
      
      sampleTextPanel = new ConfigFileBrowsePanel("Sample Text:", Config.sampleTextFile);
      sampleTextPanel.setToolTipText("an Excel 2007 spreadsheet of localizable strings");
      
      mergeMapPanel = new ConfigFileBrowsePanel("Merge Maps:", Config.mergeMapFile);
      
      fontEncodingsPanel = new ConfigFileBrowsePanel("Font Encodings:", Config.fontEncodingsFile);
      fontEncodingsPanel.setToolTipText("declares fonts to be used by Predesigner");
      
      familyGroupPanel = new ConfigFileBrowsePanel("Family Group File:", Config.familyGroupFile);
      familyGroupPanel.setToolTipText("specifies current family group multiplier input file");

      init();
      pack();
      
      Dimension dlgSize = this.getPreferredSize();
      Dimension frmSize = owner.getSize();
      Point loc = owner.getLocation();
      this.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
      this.setModal(true);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  private void init() throws Exception
  {
    this.setTitle("Predesigner Configuration");

    JPanel leftPanel = new JPanel();
    VerticalFlowLayout leftPanelLayout = new VerticalFlowLayout();
    leftPanel.setLayout(leftPanelLayout);
    leftPanelLayout.setVgap(15);
    leftPanel.setMinimumSize(new Dimension(400, 400));

    JLabel leftTitle = new JLabel("Folders");
    leftTitle.setFont(new java.awt.Font("Dialog", 1, 16));
    leftTitle.setHorizontalAlignment(SwingConstants.CENTER);  
    leftPanel.add(leftTitle);
    leftPanel.add(batPanel);
    leftPanel.add(stencilsPanel);
    leftPanel.add(projectsPanel);
    leftPanel.add(galleryPanel);
    leftPanel.add(autoTemplatesPanel);
    leftPanel.add(styleSetsPanel);
    leftPanel.add(masterpanelsPanel);
    leftPanel.add(averyBundlePanel);
    leftPanel.add(fontsPanel);
    
    JPanel rightPanel = new JPanel();
    VerticalFlowLayout rightPanelLayout = new VerticalFlowLayout();
    rightPanel.setLayout(rightPanelLayout);
    rightPanelLayout.setVgap(15);
    
    JLabel rightTitle = new JLabel("Files");
    rightTitle.setFont(new java.awt.Font("Dialog", 1, 16));
    rightTitle.setHorizontalAlignment(SwingConstants.CENTER);
    rightPanel.add(rightTitle);
    rightPanel.add(productGroupsPanel);
    rightPanel.add(productsPanel);
    rightPanel.add(designCategoriesPanel);
    rightPanel.add(sampleTextPanel);
    rightPanel.add(mergeMapPanel);
    rightPanel.add(fontEncodingsPanel);
    rightPanel.add(familyGroupPanel);

    JPanel unitsPanel = new JPanel(new VerticalFlowLayout());
    unitsPanel.add(new JLabel("Units of Measurement:"));
    twipsButton.setActionCommand("twips");
    inButton.setActionCommand("in");
    cmButton.setActionCommand("cm");
    unitsGroup.add(twipsButton);
    unitsGroup.add(inButton);
    unitsGroup.add(cmButton);
    twipsButton.setSelected(Config.units == AveryUtils.TWIPS);
    inButton.setSelected(Config.units == AveryUtils.INCHES);
    cmButton.setSelected(Config.units == AveryUtils.CENTIMETERS);

    JPanel unitsButtonPanel = new JPanel(new GridLayout(1, 3));
    unitsButtonPanel.add(twipsButton);
    unitsButtonPanel.add(inButton);
    unitsButtonPanel.add(cmButton);
    unitsPanel.add(unitsButtonPanel);
    rightPanel.add(unitsPanel);

    spinnerModel = new SpinnerNumberModel(Config.getDefaultBleed(), -720.0, +720.0, 5.0);
		bleedSpinner = new JSpinner(spinnerModel);
    JPanel bleedPanel = new JPanel();
    bleedPanel.setLayout(new BoxLayout(bleedPanel, BoxLayout.X_AXIS));
    bleedPanel.add(new JLabel(" Default Background Bleed (in twips): "));
    bleedPanel.add(bleedSpinner);
    rightPanel.add(bleedPanel);
    
    // okay/cancel buttons
    JButton okayButton = new JButton("Okay");
    JButton cancelButton = new JButton("Cancel");

    okayButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okay = true;
        dispose();
      }
    });

    cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okay = false;
        dispose();
      }
    });
    
    JPanel buttonPanel = new JPanel();    
    buttonPanel.add(okayButton);
    buttonPanel.add(cancelButton);
    
    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(1,2));
    panel.add(leftPanel);
    panel.add(rightPanel);
    
    this.getContentPane().add(panel, BorderLayout.NORTH);
    this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
  }
  
  // public interface, accessed by caller
  
  boolean isOkay()
  {
    return okay;
  }

  int getUnits()
  {
    ButtonModel button = unitsGroup.getSelection();
    if (button.getActionCommand().startsWith("t"))
    {
      return AveryUtils.TWIPS;
    }
    else if (button.getActionCommand().startsWith("i"))
    {
      return AveryUtils.INCHES;
    }
    else if (button.getActionCommand().startsWith("c"))
    {
      return AveryUtils.CENTIMETERS;
    }
    else return Config.units;
  }
  
  String getBatDirectory()
  {
  	return batPanel.getDirectory();
  }
  
  String getStencilsDirectory()
  {
  	return stencilsPanel.getDirectory();
  }
  
  String getProjectsDirectory()
  {
    return projectsPanel.getDirectory();
  }
  
  String getImageGallery()
  {
    return galleryPanel.getDirectory();
  }
  
  String getAutoTemplatesDir()
  {
    return autoTemplatesPanel.getDirectory();
  }
  
  String getStyleSetsDir()
  {
    return styleSetsPanel.getDirectory();
  }
  
  String getMasterpanelsDir()
  {
    return masterpanelsPanel.getDirectory();
  }
  
  String getAveryBundleDir()
  {
    return averyBundlePanel.getDirectory();
  }
  
  String getFontDirectory()
  {
    return fontsPanel.getDirectory();
  }
  
  double getDefaultBleed()
  {
  	Double value = (Double)bleedSpinner.getValue();
  	return value.doubleValue();
  }
  
  File getProductGroupListFile()
  {
    return productGroupsPanel.getFile();
  }
   
  File getProductListFile()
  {
    return productsPanel.getFile();
  }
   
  File getDesignCategoriesFile()
  {
    return designCategoriesPanel.getFile();
  }
  
  File getSampleTextFile()
  {
    return sampleTextPanel.getFile();
  }
  
  File getMergeMapFile()
  {
    return mergeMapPanel.getFile();
  }
  
  File getFontEncodingsFile()
  {
    return fontEncodingsPanel.getFile();
  }
  
  File getFamilyGroupFile()
  {
    return familyGroupPanel.getFile();
  }

}