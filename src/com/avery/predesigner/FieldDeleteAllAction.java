package com.avery.predesigner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

public class FieldDeleteAllAction extends UndoableAction
{
  private AveryMasterpanel panel;
  private List panelFields;
  private ArrayList currentSelections = new ArrayList();
  private ArrayList fields = new ArrayList();

  FieldDeleteAllAction(AveryMasterpanel panel, List panelFields, ArrayList currentSelections)
  {
    super("Undo Delete All");
    this.panel = panel;
    this.panelFields = panelFields;
    this.currentSelections.addAll(currentSelections);
  }
  
  void execute()
  {
		for (int i = 0; i < currentSelections.size(); i++)
		{
			int selection = (int)currentSelections.get(i);
			AveryPanelField field = (AveryPanelField)(panelFields.get(selection));
			fields.add(field);
      panel.removePanelField(field.getPrompt());
		}
		//currentSelections.clear();
  }
  
  void undo()
  {
  	Iterator iter = fields.iterator();
  	while (iter.hasNext())
  	{
  		AveryPanelField field = (AveryPanelField)iter.next();
  		Predesigner.getProject().addFieldToMasterpanel(field, panel);
  	}
  	fields.clear();
    Predesigner.getFrame().updateView();
  }
  

}
