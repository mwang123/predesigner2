/**
 * 
 */
package com.avery.predesigner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Predesigner's API to access the AveryProject class data stored in .avery files</p>
 * <p>Copyright: Copyright 2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author 
 * @version 2.0
 */
public class ProjectBundle extends Project 
{
	private File file = null;
	private final static String TEMP_DIR = "Temp";

	ProjectBundle() {
	}
	
	void initFrom(File averyFile) throws Exception
	{
		this.file = averyFile;
		
		// unzip bundle
		File xmlFile = extractContentFromZip(file, getHomeDirectory());
		
		// initialize from an bundle xml
		super.initFrom(xmlFile);
	}
	
    private File extractContentFromZip(File input, String outputDirName) throws IOException
    {
    	// create if not exists
		(new File(outputDirName)).mkdir();
		(new File(outputDirName + "/HighRes/")).mkdir();
		
    	File xmlFile = null;
    	
        // Get the data from the zip file
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(file));
        ZipEntry zipEntry = null;

        ImageFileFilter imagesFilter = new ImageFileFilter();
        
        byte[] buffer = new byte[2048];
        while ((zipEntry = zipInputStream.getNextEntry()) != null)
        {
            FileOutputStream xmlOutput = null;

        	if(imagesFilter.accept(zipEntry.getName())) {
            	// store project's images into outputDir/HighRes directory
                xmlOutput = new FileOutputStream(outputDirName + "/HighRes/" + zipEntry.getName());
        	}
        	else {
            	// store other files into outputDir
                xmlOutput = new FileOutputStream(outputDirName + "/" + zipEntry.getName());
        	}
            
            int len = 0;
            while ((len = zipInputStream.read(buffer)) > 0)
            {
                xmlOutput.write(buffer, 0, len);
            }
            xmlOutput.close();
            
            if (null == xmlFile && zipEntry.getName().endsWith(".xml")) {
            	xmlFile = new File(outputDirName + "\\" + zipEntry.getName());
            }
        }
        
        zipInputStream.close();
        
        return xmlFile;
    }
    
    String getHomeDirectory()
    {
    	return file.getParentFile().getAbsolutePath() + "\\" + TEMP_DIR + "\\";
    }
    
    String getFileDirectory()
    {
  	  return file.getParentFile().getAbsolutePath() + "\\";
    }
    
    void save() throws Exception 
    {
    	// save project data into xml and updates the .avery file
    	saveDotAvery(file);
    	
    	// save bundle's thumbnail
        String thumbnailName = file.getName();
        thumbnailName = thumbnailName.substring(0, thumbnailName.lastIndexOf('.')) + ".jpg";

        // check if output directory exists
		(new File(getFileDirectory() + "LowRes\\")).mkdir();
        
		saveMasterpanelThumbnail(new File(getFileDirectory() + "LowRes\\", thumbnailName));
		
		setModified(false);
    }
    
    String getName()
    {
    	return file.getName();
    }
}
