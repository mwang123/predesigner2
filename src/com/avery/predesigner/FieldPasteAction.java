/*
 * FieldPasteAction.java Created on May 16, 2005 by Bob Lee
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.Point;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

/**
 * @author Bob Lee
 */
class FieldPasteAction extends UndoableAction
{
  private AveryMasterpanel panel;
  private AveryPanelField field;
  
	/**
	 * @param undoString
	 */
	public FieldPasteAction(AveryMasterpanel panel)
	{
		super("Undo Paste");
    this.panel = panel;
	}

	/**
	 * @see com.avery.predesigner.UndoableAction#execute()
	 */
	void undo()
	{
    if (field != null)
    {
      panel.removePanelField(field.getPrompt());
      Predesigner.getFrame().updateView();
    }
	}

	/**
	 * @see com.avery.predesigner.UndoableAction#undo()
	 */
	void execute()
	{
    field = (AveryPanelField)((AveryPanelField)Predesigner.getFrame().getHeldObject()).deepClone();
    
    // offset new object by 1/32"
    Point p = field.getPosition();
    p.move(p.x + 45, p.y + 45);
    field.setPosition(p);
    
    String description = field.getDescription();
    
    if (panel.usesFieldDescription(description))
    {
      Object[] choices = field.getLocalizableDescriptions(Predesigner.getProject().getLanguage()).toArray();
      
      do {
        description = (String)JOptionPane.showInputDialog(Predesigner.getFrame(),
                      "Please choose a different field description.",
                      "Duplicate Field Description", JOptionPane.PLAIN_MESSAGE, null,
                      choices, field.getDescription());
        
        if (description == null)  // user canceled
        {
          field = null;
          return;
        }
      } while (panel.usesFieldDescription(description));
      
      field.setDescription(description);
    }
    
  	Predesigner.getProject().addFieldToMasterpanel(field, panel);
    Predesigner.getFrame().updateView();
    return;
	}
  
  private JComboBox comboBox;
  
  private void initComboBox(List descriptions, String current)
  {
    JComboBox comboBox = new JComboBox(descriptions.toArray());
    comboBox.setSelectedItem(current);
    if (!comboBox.getSelectedItem().equals(current))
    {
      comboBox.addItem(current);
      comboBox.setSelectedItem(current);
    }
    
    comboBox.setEditable(true);
  }
}
