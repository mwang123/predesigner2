package com.avery.predesigner;


//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.net.URL;


//import javax.swing.AbstractAction;
import java.awt.BorderLayout;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;


public class AddToolBar extends JToolBar
{
	//public AddToolBar()
	//{
		//init();
	//}
	
	public AddToolBar(int orientation)
	{
		super(null, orientation);
		init();
	}
	
	private void init()
	{
		
		String imgLocation = "toolbarImages/";
	
    ImageIcon iconTextBlock = new ImageIcon(imgLocation + "text-block.png");
    MenuAction textBlockAction = new MenuAction("Text Block", iconTextBlock);
    JButton textBlockButton = this.add(textBlockAction);
    textBlockButton.setActionCommand((String)textBlockAction.getValue(Action.NAME));
    textBlockButton.setToolTipText((String)textBlockAction.getValue(Action.NAME));

    /*ImageIcon iconTextLine = new ImageIcon(imgLocation + "text-line.png");
    MenuAction textLineAction = new MenuAction("Text Line", iconTextLine);
    JButton textLineButton = this.add(textLineAction);
    textLineButton.setActionCommand((String)textLineAction.getValue(Action.NAME));
    textLineButton.setToolTipText((String)textLineAction.getValue(Action.NAME));*/

    ImageIcon iconTextCurve = new ImageIcon(imgLocation + "text-curve.png");
    MenuAction textCurveAction = new MenuAction("Text Curve", iconTextCurve);
    JButton textCurveButton = this.add(textCurveAction);
    textCurveButton.setActionCommand((String)textCurveAction.getValue(Action.NAME));
    textCurveButton.setToolTipText((String)textCurveAction.getValue(Action.NAME));

    //this.add(new JSeparator(SwingConstants.HORIZONTAL));
    
    ImageIcon iconImage = new ImageIcon(imgLocation + "image.png");
    MenuAction imageAction = new MenuAction("Image", iconImage);
    JButton imageButton = this.add(imageAction);
    imageButton.setActionCommand((String)imageAction.getValue(Action.NAME));
    imageButton.setToolTipText((String)imageAction.getValue(Action.NAME));
    
    ImageIcon backgroundImage = new ImageIcon(imgLocation + "background.png");
    MenuAction backgroundAction = new MenuAction("Background", backgroundImage);
    JButton backgroundButton = this.add(backgroundAction);
    backgroundButton.setActionCommand((String)backgroundAction.getValue(Action.NAME));
    backgroundButton.setToolTipText((String)backgroundAction.getValue(Action.NAME));
    
    ImageIcon shapeImage = new ImageIcon(imgLocation + "heart.png");
    MenuAction shapeAction = new MenuAction("Shape", shapeImage);
    JButton shapeButton = this.add(shapeAction);
    shapeButton.setActionCommand((String)shapeAction.getValue(Action.NAME));
    shapeButton.setToolTipText((String)shapeAction.getValue(Action.NAME));

    ImageIcon barcodeImage = new ImageIcon(imgLocation + "barcode.png");
    MenuAction barcodeAction = new MenuAction("Barcode", barcodeImage);
    JButton barcodeButton = this.add(barcodeAction);
    barcodeButton.setActionCommand((String)barcodeAction.getValue(Action.NAME));
    barcodeButton.setToolTipText((String)barcodeAction.getValue(Action.NAME));

    ImageIcon zorderImage = new ImageIcon(imgLocation + "zorder.png");
    MenuAction zorderAction = new MenuAction("ZOrder", zorderImage);
    JButton zorderButton = this.add(zorderAction);
    zorderButton.setActionCommand((String)zorderAction.getValue(Action.NAME));
    zorderButton.setToolTipText((String)zorderAction.getValue(Action.NAME));
	}
	

}