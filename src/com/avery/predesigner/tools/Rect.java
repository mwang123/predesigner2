/**
 * Title:        Rect<p>
 * Description:  <p>
 * Copyright:    Copyright (c)2003 Avery Dennison<p>
 * Company:      Avery Dennison<p>
 * @author Brad Nelson
 * @version 1.0
 */
package com.avery.predesigner.tools;

import java.awt.*;

import com.avery.predesigner.Frame1;

public class Rect extends Shape
{
  public Rect(int x1, int y1, int w1, int h1, boolean maintainAspect, Frame1 frame)
  {
    super(x1, y1, w1, h1, maintainAspect, frame);
  }

  /* not used in our application
  public Rect(int x1, int y1, int w1, int h1, Color lineColor)
  {
    super(x1, y1, w1, h1, lineColor);
  }

  public Rect(int x1, int y1, int w1, int h1, Color lineColor, Color fillColor)
  {
    super(x1, y1, w1, h1, lineColor, fillColor);
  }
  */

  public void draw(Graphics g)
  {
  	Graphics2D g2d = (Graphics2D)g;
  	
  	int thickness = 2;
  	Stroke oldStroke = g2d.getStroke();
  	
    if (filled)
    {
      if (highlighted)
      {
        g.setColor(highlightColor); //fillColor.brighter());
      	g2d.setStroke(new BasicStroke(thickness));
      }
      else
        g.setColor(fillColor);
      g.fillRect(x + pageOffsetX, y + pageOffsetY, w, h);
      g2d.setStroke(oldStroke);
    }
    if (selected)
      drawHandles(x, y, w, h, g);
    else
    {
      if (!highlighted)
      {
        g.setColor(lineColor);
        g.drawRect(x + pageOffsetX, y + pageOffsetY, w, h);
        g2d.setStroke(oldStroke);
      }
    }
    if (highlighted)
    {
      g.setColor(highlightColor); //fillColor.brighter());
    	g2d.setStroke(new BasicStroke(thickness));
    	g.drawRect(x + pageOffsetX, y + pageOffsetY, w, h);
    	g2d.setStroke(oldStroke);
    }
  }
}
