/**
 * Title:        Shape<p>
 * Description:  <p>
 * Copyright:    Copyright (c)2003 Avery Dennison<p>
 * Company:      Avery Dennison<p>
 * @author Brad Nelson
 * @version 1.0
 */
package com.avery.predesigner.tools;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.*;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.avery.predesigner.Frame1;
import com.avery.predesigner.MasterpanelView;
import com.avery.predesigner.Predesigner;
import com.avery.predesigner.editors.RotationPanel;
import com.avery.predesigner.editors.ShapeEditorPanel;
import com.avery.project.AveryPanelField;

public class Shape
{

  int dragStartX, dragStartY;
  protected int x, y, w, h;
  protected int pageOffsetX, pageOffsetY;
  protected Color lineColor = Color.black;
  protected Color fillColor = Color.blue;
  protected Color highlightColor = Color.red;
  protected boolean filled = false;
  protected boolean selected = false;
  protected boolean highlighted = false;

  private int left, right, top, bottom;
  private int velocityX, velocityY;
  private Rectangle boundRect;
  private Vector handles;
  private Handle pickedHandle = null;
  private static final int LEFTTOP = 0;
  private static final int LEFTBOTTOM = 1;
  private static final int RIGHTBOTTOM = 2;
  private static final int RIGHTTOP = 3;
  private static final int LEFTMIDDLE= 4;
  private static final int MIDDLETOP = 5;
  private static final int RIGHTMIDDLE = 6;
  private static final int MIDDLEBOTTOM = 7;
  private static final int ROTATION = 8;
 
  private boolean dragging = false;
  
  boolean maintainAspect = false;
  
  public static boolean SHAPE_enterKeyPressed = false;

  private AveryPanelField field = null;
  private Frame1 frame;
  
  public Shape() {}

  public Shape(int x1, int y1, int width, int height, boolean maintainAspect, Frame1 frame)
  {
    createHandles();
    move(x1, y1);
    size(width, height);
    this.maintainAspect = maintainAspect;
    this.frame = frame;
  }

  /* not used in our application
  public Shape(int x1, int y1, int width, int height, Color lineColor)
  {
    createHandles();
    move(x1, y1);
    size(width, height);
    setLineColor(lineColor);
  }

  public Shape(int x1, int y1, int width, int height, Color lineColor, Color fillColor)
  {
    createHandles();
    move(x1, y1);
    size(width, height);
    setLineColor(lineColor);
    setFillColor(fillColor);
    filled = (fillColor != null);
  }
  */

  // move the shape
  public void move(int newX, int newY)
  {
    x = left = newX;
    y = top = newY;

//    System.out.println("move newx newy=" + newX + "," + newY);
    right = left + w;
    bottom = top + h;
    boundRect = new Rectangle (x, y, w, h);
    updateHandles();
  }

  // resize the shape
  public void size(int width, int height)
  {
    // set a positive minimum size limit on each dimension
    if (width < 3)
      width = 3;
    if (height < 3)
      height = 3;

    if (maintainAspect)
    { 
      // size to the smaller delta
      if (Math.abs(width - w) < Math.abs(height - h))
      {
        double scale = ((double)width) / ((double)w); 
        height = (int)(((double)h) * scale);
      }
      else
      {
        double scale = ((double)height) / ((double)h); 
        width = (int)(((double)w) * scale);
      }
    }
    
    w = width;
    h = height;
    right = left + w;
    bottom = top + h;
    boundRect = new Rectangle (x, y, w, h);
    updateHandles();
  }
  
  public void setPageOffsets(int offsetX, int offsetY)
  {
    pageOffsetX = offsetX;
    pageOffsetY = offsetY;
  }

  public void setLineColor( Color c )
  {
    lineColor = c;
  }

  public void setFillColor( Color c )
  {
    fillColor = c;
  }

  public void setHighlightColor( Color c )
  {
    highlightColor = c;
  }

  public void setFilled( boolean fill )
  {
    filled = fill;
  }

  public void setVelocityX( int vX )
  {
    velocityX = vX;
  }

  public void setVelocityY( int vY )
  {
    velocityY = vY;
  }

  // declare these simple get methods final
  // so they can be inlined by compiler for speed
  // final methods can't be overridden by subclasses,
  // so there is no dynamic method lookup
  public final int getX()
  {
    return x;
  }

  public final int getY()
  {
    return y;
  }

  public final int getWidth()
  {
    return w;
  }

  public void setWidth(int width)
  {
  	w = width;
  	if (w < ShapeEditorPanel.MIN_SHAPE_DIM)
  		w = ShapeEditorPanel.MIN_SHAPE_DIM;
    right = left + w;
    updateHandles();
  }
  
  public void setDragging(boolean dragging)
  {
  	this.dragging = dragging;
  	if (!dragging)
  		pickedHandle = null;
  }
  
  public final int getHeight()
  {
    return h;
  }
  
  public void setHeight(int height)
  {
  	h = height;
  	if (h < ShapeEditorPanel.MIN_SHAPE_DIM)
  		h = ShapeEditorPanel.MIN_SHAPE_DIM;
    bottom = top + h;
    updateHandles();
  }

  public final Rectangle bounds()
  {
    return boundRect;
  }

  public final boolean getFilled()
  {
    return filled;
  }

  public final int getVelocityX()
  {
    return velocityX;
  }

  public final int getVelocityY()
  {
    return velocityY;
  }

  public int getDragStartX()
  {
    return dragStartX;
  }

  public int getDragStartY()
  {
    return dragStartY;
  }

  public void setDragStartX(int startX)
  {
    dragStartX = startX;
  }

  public void setDragStartY(int startY)
  {
    dragStartY = startY;
  }

  public void draw( Graphics g )
  {
    System.out.println("Basic shapes cannot be drawn.");
  }

  public final boolean inside(int testX, int testY)
  {
    return boundRect.contains(testX - pageOffsetX, testY - pageOffsetY);
  }

  public void highlight( boolean state )
  {
    highlighted = state;
  }

  public boolean select( MouseEvent e, boolean select )
  {
    if (!select)
      selected = false;
    else if (e.isShiftDown() || e.isAltDown() || e.isControlDown() || SHAPE_enterKeyPressed)
    {
      //selected = !selected;
    	selected = select;
      highlight(true);
    }
    else
    {
      selected = select;
      highlight(false);
    }

    return selected;
  }

  void createHandles()
  {
    handles = new Vector(4);

    handles.addElement(createHandle(left, top, LEFTTOP));
    handles.addElement(createHandle(left, bottom, LEFTBOTTOM));
    handles.addElement(createHandle(right, bottom, RIGHTBOTTOM));
    handles.addElement(createHandle(right, top, RIGHTTOP));
    handles.addElement(createHandle(left, (top + bottom) / 2, LEFTMIDDLE));
    handles.addElement(createHandle((right + left) / 2, top, MIDDLETOP));
    handles.addElement(createHandle(right, (top + bottom) / 2, RIGHTMIDDLE));
    handles.addElement(createHandle((right + left) / 2, bottom, MIDDLEBOTTOM));
    handles.addElement(createHandle((right + left) / 2, top - 10, ROTATION));
  }

  Handle createHandle(int x1, int y1, int corner)
  {
    Handle handle = new Handle(x1, y1, 6, 6,
    Color.black, Color.white, corner);
    return handle;
  }

  void updateHandles()
  {
    Handle handle;

    Enumeration h = handles.elements();

    handle = (Handle) h.nextElement();
    handle.move(left, top);

    handle = (Handle) h.nextElement();
    handle.move(left, bottom);

    handle = (Handle) h.nextElement();
    handle.move(right, bottom);

    handle = (Handle) h.nextElement();
    handle.move(right, top);
    
    handle = (Handle) h.nextElement();
    handle.move(left, (top + bottom) / 2);

    handle = (Handle) h.nextElement();
    handle.move((left + right) / 2, top);
    
    handle = (Handle) h.nextElement();
    handle.move(right, (top + bottom) / 2);
    
    handle = (Handle) h.nextElement();
    handle.move((left + right) / 2, bottom); 
    
    handle = (Handle) h.nextElement();
    handle.move((left + right) / 2, top - 10); 
  }

  public void moveHandle(int x1prime, int y1prime)
  {
    int newX = 0, newY = 0, newW = 0, newH = 0;

    int x1 = x1prime - pageOffsetX;
    int y1 = y1prime - pageOffsetY;

    if (pickedHandle != null)
    {
       switch (pickedHandle.getCorner())
       {
      case LEFTTOP:
        newX = x1;
        newW = right - x1;
        newY = y1;
        newH = bottom - y1;
        break;

      case LEFTBOTTOM:
        newX = x1;
        newW = right - x1;
        newY = top;
        newH = y1 - top;
        break;

      case RIGHTBOTTOM:
        newX = left;
        newW = x1 - left;
        newY = top;
        newH = y1 - top;
        break;

      case RIGHTTOP:
        newX = left;
        newW = x1 - left;
        newY = y1;
        newH = bottom - y1;
        break;

      case LEFTMIDDLE:
        newX = x1;
        newW = right - x1;
        newY = top;
        newH = bottom - top;
        break;
        
      case MIDDLETOP:
        newX = left;
        newW = right - left;
        newY = y1;
        newH = bottom - y1;
        break;
        
      case RIGHTMIDDLE:
        newX = left;
        newW = x1 - left;
        newY = top;
        newH = bottom - top;
        break;
        
      case MIDDLEBOTTOM:
        newX = left;
        newW = right - left;
        newY = top;
        newH = y1 - top;
        break;
        
      case ROTATION:
      	return;
        
      }
      size(newW, newH);
      move(newX, newY);
    }
  }

  public void drawHandles(int x1, int y1, int w1, int h1, Graphics g)
  {
    float dash1[] = {5.0f};
    g.setColor(Color.black);
    BasicStroke dashed =
        new BasicStroke(1.0f,
                        BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER,
                        10.0f, dash1, 0.0f);
    Graphics2D g2d = (Graphics2D)g;
    g2d.setStroke(dashed);
    
    g.drawRect(x1 + pageOffsetX, y1 + pageOffsetY, w1, h1);
    
    g2d.setStroke(new BasicStroke());
    
    for (Enumeration hands = handles.elements(); hands.hasMoreElements();)
    {
      Handle handle = (Handle) hands.nextElement();
      handle.draw(g, pageOffsetX, pageOffsetY);
    }
    
    if (dragging)
    {
    	Color transBlue = new Color(0.0f, 0.0f, 1.0f, 0.75f);
    	g.setColor(transBlue);
    
    	if (pickedHandle != null)
    	{
    		switch (pickedHandle.getCorner())
    		{
    			case LEFTTOP:
    				g.drawLine(0, y1 + pageOffsetY, x1 + pageOffsetX, y1 + pageOffsetY);
    				g.drawLine(x1 + pageOffsetX, y1 + pageOffsetY, x1 + pageOffsetX, 0);
           break;

    			case LEFTBOTTOM:
    				g.drawLine(0, bottom + pageOffsetY, x1 + pageOffsetX, bottom + pageOffsetY);
    				g.drawLine(x1 + pageOffsetX, bottom + pageOffsetY, x1 + pageOffsetX, 0);
           break;

    			case RIGHTBOTTOM:
    				g.drawLine(0, bottom + pageOffsetY, right + pageOffsetX, bottom + pageOffsetY);
    				g.drawLine(right + pageOffsetX, bottom + pageOffsetY, right + pageOffsetX, 0);
           break;

    			case RIGHTTOP:
    				g.drawLine(0, y1 + pageOffsetY, right + pageOffsetX, y1 + pageOffsetY);
    				g.drawLine(right + pageOffsetX, y1 + pageOffsetY, right + pageOffsetX, 0);
           break;
           
          case LEFTMIDDLE:
    				g.drawLine(0, y1 + h / 2 + pageOffsetY, x1 + pageOffsetX, y1 + h / 2 + pageOffsetY);
    				g.drawLine(x1 + pageOffsetX, y1 + h / 2 + pageOffsetY, x1 + pageOffsetX, 0);
            break;
            
          case MIDDLETOP:
    				g.drawLine(0, y1 + pageOffsetY, x1 + w / 2 +  + pageOffsetX, y1 + pageOffsetY);
    				g.drawLine(x1 + w / 2 + pageOffsetX, y1 + pageOffsetY, x1 + w / 2 + pageOffsetX, 0);
            break;
            
          case RIGHTMIDDLE:
    				g.drawLine(0, y1 + h / 2 + pageOffsetY, right + pageOffsetX, y1 + h / 2 + pageOffsetY);
    				g.drawLine(right + pageOffsetX, y1 + h / 2 + pageOffsetY, right + pageOffsetX, 0);
            break;

          case MIDDLEBOTTOM:
    				g.drawLine(0, bottom + pageOffsetY, x1 + w / 2 + pageOffsetX, bottom + pageOffsetY);
    				g.drawLine(x1 + w / 2 + pageOffsetX, bottom + pageOffsetY, x1 + w / 2 + pageOffsetX, 0);

    		} 		
    	}
    	else
    	{
    		g.drawLine(0, y1 + pageOffsetY, x1 + pageOffsetX, y1 + pageOffsetY);
    		g.drawLine(x1 + pageOffsetX, y1 + pageOffsetY, x1 + pageOffsetX, 0);
    	}
    }
  }

  public boolean selectHandles(MouseEvent e, int x1, int y1)
  {
    boolean handleSelected = false;
    for (int i = 0; i < handles.size(); i++)
    {
      Handle handle = (Handle) handles.elementAt(i);
      if (handle.inside(x1 - pageOffsetX, y1 - pageOffsetY))
      {
        handleSelected = true;
        pickedHandle = handle;
        if (pickedHandle.getCorner() == ROTATION)
        	onRotate();
        handle.select(e, true);
      }
      else
        handle.select(e, false);
    }
    return handleSelected;
  }

  /**
   * called when the user presses the rotation button
   */
  void onRotate()
  {
  	// updates rotation when gadgets are clicked
  	AveryPanelField field = (AveryPanelField)frame.getMasterpanelView().getCurrentPanel();
    double oldRotation = field.getRotation().doubleValue();
  	
    RotationPanel ui = new RotationPanel(field, field.getRotation(), this, true); //Double.parseDouble(rotationGadget.getText()));

    if (JOptionPane.showConfirmDialog(this.frame, ui, "Set Rotation (Counter-clockwise)",
      JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
        == JOptionPane.OK_OPTION)
    {
      double newRotation = Double.parseDouble(ui.getRotation());
      if (newRotation != 0.0 && newRotation != 90.0 &&
      		newRotation != 180.0 && newRotation != 270.0)
      	applyRotation(field, oldRotation, newRotation);    	
    }
    else 
    {
      double newRotation = Double.parseDouble(ui.getRotation());
    	applyRotation(field, newRotation, oldRotation);    	
    }
  }
  
  public void applyRotation(AveryPanelField afield, double oldRotation, double newRotation)
  {
    if (oldRotation != newRotation)
    {
      // calculate a new position as if object was rotated around its center point 
      double oldtheta = oldRotation * Math.PI / 180.0;
      double newtheta = newRotation * Math.PI / 180.0;

      Rectangle bounds = afield.getBoundingRect();
      int centerX = bounds.x + (bounds.width / 2);
      int centerY = bounds.y + (bounds.height / 2);
      
      AffineTransform aft = AffineTransform.getRotateInstance(oldtheta - newtheta, centerX, centerY);
      Point2D newPosition = new Point();
      aft.transform(afield.getPosition(), newPosition);
      
      afield.setRotation(newRotation);
      afield.setPosition((Point)newPosition);
      // set the new position in the UI gadgets
      //updatePositionGadget(newPosition);
    	frame.getMasterpanelView().buildFieldSelectionTools();
    	frame.getMasterpanelView().repaint();

      // set the new rotation value in the rotation gadget
      //rotationGadget.setText(Double.toString(newRotation));
    } 	
  }

}
