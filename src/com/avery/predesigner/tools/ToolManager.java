/**
 * Title:        ToolManager<p>
 * Description:  <p>
 * Copyright:    Copyright (c)2003 Avery Dennison<p>
 * Company:      Avery Dennison<p>
 * @author Brad Nelson
 * @version 1.0
 */

// This code was adapted from applet sample "PickDraw" written by David Marsland
// Obtained from web on 12/02/03

package com.avery.predesigner.tools;

import java.awt.*;
import java.util.Vector;
import java.util.Iterator;
import java.awt.event.*;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.avery.predesigner.Frame1;
import com.avery.predesigner.Predesigner;

public class ToolManager
{
  private Vector selectionShapes = new Vector(100);
  private Shape pickedShape = null;
  private Shape oldPickedShape = null;
//  private Shape highlightedShape = null;
  private boolean reshaping = false;
  private int currentSelected = -1;
  private int downX, downY;
  
  private int step = 12;
  
  public ToolManager()
  {
  }

  public void clearSelectionShapes()
  {
    selectionShapes.clear();
  }
  
  public Shape getPickedShape()
  {
  	return pickedShape;
  }

  public void addSelectionShape(int x, int y, int width, int height, boolean maintainAspect, Frame1 frame)
  {
    Rect rect = new Rect(x, y, width, height, maintainAspect, frame);
    rect.setLineColor(Color.blue);
    rect.setPageOffsets(x, y);
    selectionShapes.add(rect);
  }

  public void setDownPosition(int x, int y)
  {
    downX = x;
    downY = y;
  }

  public void unHighLightAll()
  {
    for (int i = selectionShapes.size() - 1; i >= 0; i--)
    {
      Shape currentShape = (Shape)selectionShapes.elementAt(i);
      currentShape.highlight(false);
    }
  	
  }
  
  public int selectShape(MouseEvent e, int x, int y)
  {
    int selected = -1;

    if (pickedShape != null)
    {
      reshaping = pickedShape.selectHandles(e, x, y);
      if (reshaping)
        return currentSelected;
    }

    if (oldPickedShape != null)
    {
      oldPickedShape.select(e, false);
      oldPickedShape = null;
    }

    for (int i = selectionShapes.size() - 1; i >= 0; i--)
    {
      Shape currentShape = (Shape)selectionShapes.elementAt(i);
      if (currentShape.inside(x, y))
      {
        pickedShape = oldPickedShape = currentShape;
        pickedShape.setDragStartX(pickedShape.getX());
        pickedShape.setDragStartY(pickedShape.getY());
        if (pickedShape.select(e, true))
          selected = i;
        break;
      }
    }
    if (selected == -1)
    {
      pickedShape = null;
      currentSelected = -1;
    }

    currentSelected = selected;
    
    return selected;
  }

  public void drawOutlines(Graphics g, int x, int y)
  {
    Iterator i = selectionShapes.iterator();
    while (i.hasNext())
    {
      Rect rect = (Rect)i.next();
      rect.setPageOffsets(x, y);
      rect.draw(g);
    }
  }

  public void mouseUp()
  {
  	if (pickedShape != null)
  	{
  		pickedShape.setDragging(false);
  	}
  }
  

  public boolean dragEvent(MouseEvent e)
  {
    // boolean repaint = false;
    int dragW, dragH;
    // int newWidth, newHeight;
    int newX, newY;
    // Shape newShape;

    // if nothing's picked, there's nothing to drag
    // so get outtahere
    if (pickedShape == null)
      return false;

    int x, y;
    // int vX = 0, vY = 0;
    x = e.getX();
    y = e.getY();

    dragW = x - downX;
    dragH = y - downY;
    
    pickedShape.setDragging(true);

    if (reshaping)
    {
      if (pickedShape != null)
      {
        pickedShape.moveHandle( x, y );
        return true;
      }
      return false;
    }

    // move pickedShape
    newX = pickedShape.dragStartX + dragW;
    newY = pickedShape.dragStartY + dragH;
    
    pickedShape.move(newX, newY);

    return true;
  }

  public boolean keyPressedEvent(KeyEvent ke)
  {
    boolean result = false;
    // if nothing is picked, return immediately since
    // all of the current key strokes manipulate pickedShape
    if (pickedShape == null)
    {
      return result;
    }

    int jump = 1;
    if (ke.isShiftDown())
      jump = 10;
    else
      jump = 1;

    switch(ke.getKeyCode())
    {
      case KeyEvent.VK_LEFT:
        pickedShape.move(pickedShape.getX() - jump,
            pickedShape.getY());
        result = true;
//        repaint();
        break;

      case KeyEvent.VK_RIGHT:
        pickedShape.move(pickedShape.getX() + jump,
            pickedShape.getY());
        result = true;
//        repaint();
        break;

      case KeyEvent.VK_UP:
        pickedShape.move(pickedShape.getX(),
            pickedShape.getY() - jump);
        result = true;
//        repaint();
        break;

      case KeyEvent.VK_DOWN:
        pickedShape.move(pickedShape.getX(),
            pickedShape.getY() + jump);
        result = true;
//        repaint();
        break;

/*      case KeyEvent.VK_T :
          shapes.removeElement(pickedShape);
          shapes.addElement(pickedShape);
          repaint();
          break; */

      default:
//          if (DEBUGGING) System.out.println("in keyPressed key switch");
          break;
    }
    return result;
  }

  public int getShapeX()
  {
    if (pickedShape != null)
      return pickedShape.getX();
    else
      return -1;
  }

  public int getShapeY()
  {
    if (pickedShape != null)
      return pickedShape.getY();
    else
      return -1;
  }

  public int getShapeWidth()
  {
    if (pickedShape != null)
      return pickedShape.getWidth();
    else
      return -1;
  }

  public int getShapeHeight()
  {
    if (pickedShape != null)
      return pickedShape.getHeight();
    else
      return -1;
  }
  
  public void setShapeHeightAbsolute(int length)
  {
    if (pickedShape != null)
    {
    	pickedShape.setHeight(length);
    }
  }
  
  public void setShapeHeight(int length)
  {
    if (pickedShape != null)
    {
    	if (pickedShape.getHeight() < pickedShape.getWidth())
    		pickedShape.setHeight(length);
    	else
    		pickedShape.setWidth(length);
    		
    }
  }

}
