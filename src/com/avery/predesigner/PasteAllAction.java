package com.avery.predesigner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

public class PasteAllAction extends UndoableAction
{
  private AveryMasterpanel toMasterpanel;
  private AveryMasterpanel copyMasterpanel;
  private ArrayList fields = new ArrayList();
	
  public PasteAllAction(AveryMasterpanel copypanel, AveryMasterpanel topanel)
	{
		super("Undo Paste All");
    this.copyMasterpanel = copypanel;
    this.toMasterpanel = topanel;
	}
  
	/*public PasteAllAction(ArrayList fieldsList, AveryMasterpanel copypanel, AveryMasterpanel topanel)
	{
		super("Undo Paste Selected");
    this.fieldsList = fieldsList;
    this.copyMasterpanel = copypanel;
    this.toMasterpanel = topanel;
	}*/
  
	void execute()
	{
		/*if (!fieldsList.isEmpty())
		{
			System.out.println("paste selected");
	  	Iterator i = fieldsList.iterator();
	  	List panelList = copyMasterpanel.getPanelFields();
	  	
	  	while (i.hasNext())
	  	{
		  	int index = (int)(i.next());
				System.out.println(index);
	  		AveryPanelField apf = (AveryPanelField)(panelList.get(index));
	  		AveryPanelField cloneField = apf.deepClone();
	  		cloneField.setID("");
	  		cloneField.setContentID("");
	  		cloneField.setStyleID("");
	  		toMasterpanel.addField(cloneField);
	  	}
	  	fieldsList.clear();
	  	Predesigner.getProject().clearProjectFieldTable();
			Predesigner.getProject().initializePanelDataLinking();
  		Predesigner.getFrame().updateView();
			return;
		}*/
		
   	///if (toMasterpanel.getFieldIterator().hasNext())
  		///return;
  	
  	Iterator i = copyMasterpanel.getFieldIterator();
  	while (i.hasNext())
  	{
  		AveryPanelField apf = (AveryPanelField)i.next();
  		AveryPanelField cloneField = apf.deepClone();
  		
  		String description = cloneField.getDescription();
      
      if (toMasterpanel.usesFieldDescription(description))
      {
        Object[] choices = cloneField.getLocalizableDescriptions(Predesigner.getProject().getLanguage()).toArray();
        
        do {
          description = (String)JOptionPane.showInputDialog(Predesigner.getFrame(),
                        "Please choose a different field description.",
                        "Duplicate Field Description", JOptionPane.PLAIN_MESSAGE, null,
                        choices, cloneField.getDescription());
          
          if (description == null)  // user canceled
          {
          	cloneField = null;
            return;
          }
        } while (toMasterpanel.usesFieldDescription(description));
        
        cloneField.setDescription(description);
      }
  		Predesigner.getProject().addFieldToMasterpanel(cloneField, toMasterpanel);
  		fields.add(cloneField);

  		  	/*//cloneField.createID();
  		//cloneField.setContentID(cloneField.getID());
  		//cloneField.setStyleID(cloneField.getID());
  		cloneField.setID("");
  		cloneField.setContentID("");
  		cloneField.setStyleID("");
  		toMasterpanel.addField(cloneField);*/
  	}
  	//Predesigner.getProject().clearProjectFieldTable();
		///Predesigner.getProject().initializePanelDataLinking();
    //UndoableAction action = new FieldPasteAction(getCurrentMasterpanel());
    //Predesigner.getProject().executeAction(action);
  	Predesigner.getFrame().updateView();
    //updateStatusDisplay();
	}
	
	void undo()
	{
		Iterator iter = fields.iterator();
		while (iter.hasNext())
		{
			AveryPanelField field = (AveryPanelField)iter.next();
			toMasterpanel.removePanelField(field.getPrompt());
		}
		//toMasterpanel.getPanelFields().clear();
		//Predesigner.getProject().initializePanelDataLinking();
		Predesigner.getFrame().updateView();
	}
	

}
