/*
 * NewProjectDialog.java Created on Mar 29, 2005 by leeb
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.avery.Averysoft;
import com.avery.product.Appearance;
import com.avery.product.ProductGroup;
import com.avery.product.ProductGroupList;
import com.avery.project.AveryProject;

/**
 * @author Bob Lee 
 */
class NewProjectDialog extends JDialog
{	
	private static final long serialVersionUID = 326726400648805929L;
	// data
	private static ProductGroupList productGroupList;
	private ProductGroup selectedProductGroup;
	
	// ui objects
  private JLabel findLabel = new JLabel("Find a ProductGroup Name or a Design BaseSKU  ");
  private JTextField findTextField = new JTextField(10);
  private JButton findButton = new JButton("Go");
	private JTable table;
	private JLabel previewGadget = new JLabel();
	private JPanel previewPanel = new JPanel();
  private JButton openButton = new JButton("Open");
  private JButton cancelButton = new JButton("Cancel");
  private JButton fileButton = new JButton("Select File...");
  
	private static final int PREFERRED_PREVIEW_WIDTH = 150;
  
	/**
	 * @param owner - typically Frame
	 * @throws java.awt.HeadlessException
	 * @throws IOException if opening ProductGroupList.xml fails
	 * @throws JDOMException if ProductGroupList.xml is malformed
	 */
	NewProjectDialog(Frame owner) 
	throws HeadlessException, IOException, JDOMException
	{
		super(owner);
    productGroupList = Predesigner.getProductGroupList();
    
		this.setTitle("New Project");
		this.setModal(true);
		this.setSize(owner.getWidth() - 48, owner.getHeight() - 48);
		this.setLocationRelativeTo(owner);
		
    // setup the Find button
    findButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
      	onFind(e);
      }
    });
    
    // hitting Enter in the text field is the same as pressing the Find button
    findTextField.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
      	onFind(e);
      }
    });
		
		// create the Find panel for the top of the dialog
		JPanel findPanel = new JPanel();
		FlowLayout flow = new FlowLayout();
		flow.setHgap(10);
 		findPanel.setLayout(flow);
		findPanel.add(findLabel);
		findPanel.add(findTextField);
		findPanel.add(findButton);
		this.getContentPane().add(findPanel, BorderLayout.NORTH);
		
		// create preview panel
		previewPanel.setBorder(BorderFactory.createTitledBorder("Preview"));
		previewGadget.setPreferredSize(new Dimension(PREFERRED_PREVIEW_WIDTH, 0));
		previewGadget.setSize(PREFERRED_PREVIEW_WIDTH, 0);
		previewPanel.add(previewGadget);
		this.getContentPane().add(previewPanel, BorderLayout.EAST);
		
		// create table and a scrollPane to hold it
		JScrollPane scrollPane = new JScrollPane(getTable());
		this.getContentPane().add(scrollPane, BorderLayout.CENTER);

		// setup for double-click in table to exit
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e)
			{
				if (e.getClickCount() > 1)
				{
					setExitStatus(EXIT_OKAY);
					dispose();
				}
			}
		});
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			public void valueChanged(ListSelectionEvent event)
			{
				onSelectionChanged(event);
			}			
		});
		
		// select 0th product
		if (productGroupList != null && productGroupList.size() > 0)
		{
			table.getSelectionModel().setSelectionInterval(0,0);
			setSelectedProductGroup((ProductGroup)productGroupList.get(0));
			refreshPreview();
		}
		else
		{
			openButton.setEnabled(false);  // disabled until a row is selected
		}
		
    // setup okay/cancel buttons
    openButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
      	onOpen(e);
      }
    });

    cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
      	onCancel(e);
      }
    });
    
    fileButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
      	onFile(e);
      }
    });
    
    JPanel buttonPanel = new JPanel();
    buttonPanel.add(fileButton);
    buttonPanel.add(new JLabel("           "));		// spacer
    buttonPanel.add(openButton);
    buttonPanel.add(cancelButton); 
    this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    
    this.doLayout();
	}
	
	/**
	 * This will create the table if it doesn't exist, which is why it
	 * can throw exceptions.
	 * @return the ProductGroupList table
	 * @throws IOException if getList fails to read the list from disk
	 * @throws JDOMException if getList fails to parse the ProductGroupList.xml file
	 */
	JTable getTable()
	throws IOException, JDOMException
	{
		if (table == null)
		{
			ProductGroupList list = Predesigner.getProductGroupList();
			
			String[] headers = new String[] { "Name", "Description", "Template", "Base", "Auto" };
			String[][] rows = new String[list.size()][5];
			
			int currentRow = 0;
			Iterator iterator = list.iterator();
			while (iterator.hasNext())
			{
				ProductGroup productGroup = (ProductGroup)iterator.next();
				rows[currentRow][0] = productGroup.getName();
				rows[currentRow][1] = productGroup.getDescription();
				rows[currentRow][2] = productGroup.getBatName();
				rows[currentRow][3] = productGroup.getBaseSku();
        rows[currentRow][4] = productGroup.isComplex() ? " " : "yes";
 				++currentRow;
			}
			
			table = new JTable(rows, headers){
				private static final long serialVersionUID = -361583337304226751L;
				public boolean isCellEditable(int row, int column)
				{
					return false;
				}
			};
      
      JLabel renderer = ((JLabel)table.getDefaultRenderer(Object.class));
      renderer.setHorizontalAlignment(SwingConstants.CENTER);
			
			table.getColumn("Base").setMaxWidth(75);
			table.getColumn("Description").setMinWidth(75);
			table.getColumn("Template").setMaxWidth(75);
      table.getColumn("Name").setMaxWidth(75);
      table.getColumn("Auto").setMaxWidth(50);
		}
		
		return table;
	}
	
	/**
	 * This is called when the user presses the Find button.
	 * It will setSelectedProductGroup() and dispose() the dialog
	 * if the entered string is found.
	 * @param e standard ActionEvent parameter (not used)
	 */
	private void onFind(ActionEvent e)
	{
		System.out.println("Find button pressed");
		String text = findTextField.getText();
		if (text == null || text.length() < 1)
		{
	    JOptionPane.showMessageDialog(this, 
	    		"Search failed - no text entered!", 
					"Error", JOptionPane.ERROR_MESSAGE);
	    
	    findTextField.requestFocus();
	    return;
		}
		
		Iterator iterator = productGroupList.iterator();
		while (iterator.hasNext())
		{
			ProductGroup pg = (ProductGroup)iterator.next();
			if (pg.getBaseSku().equals(text) || pg.getName().equals(text))
			{
				 setSelectedProductGroup(pg);
				 setExitStatus(EXIT_OKAY);
				 dispose();
				 return;
			}
		}
		
    JOptionPane.showMessageDialog(this, 
    		"Text '" + text + "' not found", 
				"Error", JOptionPane.ERROR_MESSAGE);
    
    findTextField.requestFocus();
	}
	
  // exit status
  static final int EXIT_UNDEFINED = -1;
  static final int EXIT_CANCEL = 0;
  static final int EXIT_OKAY = 1;
  static final int EXIT_FILE = 2;
  
  private int exitStatus = EXIT_UNDEFINED;
  
	int getExitStatus()
	{
		return exitStatus;
	}
	
	private void setExitStatus(int exitStatus)
	{
		this.exitStatus = exitStatus;
	}
	
	private void onOpen(ActionEvent event)
	{
		setExitStatus(EXIT_OKAY);
		// cleanup UI
		dispose();
	}
	
	private void onCancel(ActionEvent event)
	{
		setExitStatus(EXIT_CANCEL);
		dispose();
	}
	
	private void onFile(ActionEvent event)
	{
		setExitStatus(EXIT_FILE);
		dispose();
	}
	
	/**
	 * called when the row selection of the list changes
	 * @param event
	 */
	private void onSelectionChanged(ListSelectionEvent event)
	{
		if (event.getValueIsAdjusting() == false)  // change is complete
		{
			int index = table.getSelectedRow();
			if (index == -1)
			{
				setSelectedProductGroup(null);
			}
			else if (event.getFirstIndex() != event.getLastIndex())	// row changed
			{
				setSelectedProductGroup((ProductGroup)productGroupList.get(index));
				refreshPreview();
			}
		}
	}
	
	private void refreshPreview()
	{
		File batFile = new File(Config.getBatDirectory(), getSelectedProductGroup().getBatName());
		Image image = getWireframeImage(batFile);
		previewPanel.remove(previewGadget);
		previewGadget.setPreferredSize(new Dimension(image.getWidth(null), image.getHeight(null)));
		previewGadget.setSize(image.getWidth(null), image.getHeight(null));
    previewGadget.setIcon(new ImageIcon(image));
    previewPanel.add(previewGadget);
    previewPanel.repaint();
	}
	
	private Image getWireframeImage(File file)
	{
    try
    {
      SAXBuilder builder = new SAXBuilder(true);
  		builder.setFeature("http://apache.org/xml/features/validation/schema", true);
			builder.setProperty(
					"http://apache.org/xml/properties/schema/external-schemaLocation",
					Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 
      Element element = builder.build(file).getRootElement();
      AveryProject project = new AveryProject(element);
      // set appearance of page 1
      Appearance appearance = getSelectedProductGroup().getAppearance(1);
      if (appearance.getImage() != null)
      {
      	File paperimageFile = new File(Config.getBatDirectory() + "/papers", appearance.getImage());
      	if (paperimageFile.exists())
      	{
  				project.getPage(1).setPaperimage(paperimageFile.getAbsolutePath());
      	}
      	else
      	{
      		System.err.println("Missing paperimage file: " + paperimageFile.getAbsolutePath());
      	}
      }
      else
      {
        project.getPage(1).setPaperColor(appearance.getColor());     	
      }
      
      return project.makePagePreview(1, PREFERRED_PREVIEW_WIDTH);
    }
    catch (Exception ex)
    {
    	System.err.println(ex.getMessage());
    	return null;
    }		
	}
	
	private void setSelectedProductGroup(ProductGroup pg)
	{
		openButton.setEnabled(pg != null);
		selectedProductGroup = pg;
	}
	
	/**
	 * provides Access to selected ProductGroup from dialog caller 
	 * @return
	 */
	ProductGroup getSelectedProductGroup()
	{
		return selectedProductGroup;
	}
}
