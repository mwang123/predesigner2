/*
 * ShapeEditorPanel.java Created on Nov 15, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner.editors;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.avery.predesigner.ColorChooser;
import com.avery.predesigner.Config;
import com.avery.predesigner.Predesigner;
import com.avery.project.AveryImage;
import com.avery.project.AveryPanelField;
import com.avery.project.Drawing;
import com.avery.project.AveryMasterpanel;
import com.avery.utils.AveryUtils;

import org.faceless.pdf2.CMYKColorSpace;
import org.faceless.pdf2.SpotColorSpace;

/**
 * @author leeb
 * Nov 15, 2007
 * ShapeEditorPanel
 */
public class ShapeEditorPanel extends FieldEditorPanel
{
	public static int MIN_SHAPE_DIM = 60;
	
  private JTextField widthGadget = new JTextField();
  private JTextField heightGadget = new JTextField();
  private JTextField positionYGadget = new JTextField();
  private JTextField positionXGadget = new JTextField();
  private JPanel fillColorPanel = new JPanel();
  private JButton fillColorButton = new JButton("Fill Color...");
  private JCheckBox noFillGadget = new JCheckBox("No Fill");
  private JCheckBox spotColorBorderGadget = new JCheckBox("Make Border Spot Color");
  private JPanel borderColorPanel = new JPanel();
  private JButton borderColorButton = new JButton("Border Color...");
  private JSpinner borderSizeGadget;
  private JPanel imageConvertPanel = new JPanel();
  private JButton imageConvertButton = new JButton("Convert to Image");
  
  private Color selectedFillColor;
  private Color selectedBorderColor;
  
  private ColorChooser colorChooser;
  
  

  /**
   * @param field
   */
  public ShapeEditorPanel(AveryPanelField field)
  {
    this.field = field;
    colorChooser = ColorChooser.getColorChooser();
    init();
  }

  /* (non-Javadoc)
   * @see com.avery.predesigner.FieldEditorPanel#getDefaultPrompt()
   */
  String getDefaultPrompt()
  {
    return "Shape 1";
  }
  
  protected void init()
  {
    super.init();
    
    Drawing drawing = (Drawing)this.field;
    
   
    int alpha = (int)(drawing.getOpacity().doubleValue() * 255.0);
    selectedFillColor = new Color(drawing.getColor().getRed(),
																	drawing.getColor().getGreen(),
																	drawing.getColor().getBlue(),
    															alpha);
    noFillGadget.setSelected(selectedFillColor.getAlpha() == 0 && drawing.getWidth().doubleValue() > 0.0);
    noFillGadget.setEnabled(drawing.getWidth().doubleValue() > 0.0);
    
    spotColorBorderGadget.setSelected(false);
    spotColorBorderGadget.setSelected(drawing.getSpotColor() != null);
 
    if (alpha <= 0)
    {
    	opacityGadget.setEnabled(false);
	    opacityGadget.setSelectedIndex(19);
    }
    
    fillColorPanel.setBackground(selectedFillColor);

    fillColorButton.setEnabled(!noFillGadget.isSelected());
  	fillColorPanel.setVisible(!noFillGadget.isSelected());

    selectedBorderColor = drawing.getOutlineColor();

    if (selectedBorderColor != null)
      borderColorPanel.setBackground(selectedBorderColor);
    else
    	selectedBorderColor = new Color(0, 0, 0, 255);
    
    JPanel panel = new JPanel();
    GridBagLayout gridbag = new GridBagLayout();
    panel.setLayout(gridbag);
     
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0;
    
    // add fillColor to panel
    c.gridwidth = 1;
    gridbag.setConstraints(panel.add(fillColorPanel), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(fillColorButton), c);
    gridbag.setConstraints(panel.add(noFillGadget), c);
    gridbag.setConstraints(panel.add(spotColorBorderGadget), c);
      
    fillColorButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onFillColorButton();
      }
    });
    
    noFillGadget.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
      	onNoFillGadget();     	
      }
    });
    
    spotColorBorderGadget.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
      	onSpotColorBorderGadget();     	
      }
    });
    
    // some space between
    c.gridwidth = 1;
    gridbag.setConstraints(panel.add(new JPanel()), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(new JPanel()), c);
    
    // add borderSize and borderColor to panel 
    
    double spinValue = drawing.getOutlineWidth()/20.0;
    double spinMaxValue = drawing.getHeight().doubleValue()/40.0;
    
    borderSizeGadget = new JSpinner(
        new SpinnerNumberModel(spinValue, 0.0, spinMaxValue, 0.5));
    c.weightx = 0.0;
    c.gridwidth = 1;
    gridbag.setConstraints(panel.add(new JLabel("   Border Size:", JLabel.CENTER)), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(borderSizeGadget), c);
    
    borderSizeGadget.addChangeListener(new ChangeListener(){
      public void stateChanged(ChangeEvent e) {
        onBorderSizeSpinner();
      }
    });

    // some space between
    c.gridwidth = 1;
    gridbag.setConstraints(panel.add(new JPanel()), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(new JPanel()), c);
    
    // add borderColor to panel
    c.gridwidth = 1;
    gridbag.setConstraints(panel.add(borderColorPanel), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(borderColorButton), c);
    
    borderColorButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onBorderColorButton();
      }
    });

    // add imageConvert to panel
   /* c.gridwidth = 1;
    gridbag.setConstraints(panel.add(imageConvertPanel), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(imageConvertButton), c);
    
    imageConvertButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onImageConvertButton();
      }
    });*/
   
    add(new JSeparator());
    add(panel);
  }

  /* (non-Javadoc)
   * @see com.avery.predesigner.FieldEditorPanel#addFieldGeometryGadgets(javax.swing.JPanel, java.awt.GridBagLayout)
   * note: this is called by super.init()
   */
  protected void addFieldGeometryGadgets(JPanel panel, GridBagLayout gridbag)
  {
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridwidth = 1;
    
    gridbag.setConstraints(panel.add(new JLabel("Width:")), c);
    widthGadget.setText(Predesigner.twipsToUnitString(field.getWidth().doubleValue()));
    gridbag.setConstraints(panel.add(widthGadget), c);
    
    gridbag.setConstraints(panel.add(new JLabel(" ")), c);

    gridbag.setConstraints(panel.add(new JLabel("Height:")), c);
    heightGadget.setText(Predesigner.twipsToUnitString(field.getHeight().doubleValue()));
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(heightGadget), c);

    c.gridwidth = 1;
    gridbag.setConstraints(panel.add(new JLabel("Position X:")), c);
    positionXGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getX()));
    gridbag.setConstraints(panel.add(positionXGadget), c);
    gridbag.setConstraints(panel.add(new JLabel(" ")), c);

    gridbag.setConstraints(panel.add(new JLabel("Position Y:")), c);
    positionYGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getY()));
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(positionYGadget), c);
  }

  /* (non-Javadoc)
   * @see com.avery.predesigner.FieldEditorPanel#updatePositionGadget(java.awt.geom.Point2D)
   */
  protected void updatePositionGadget(Point2D newPosition)
  {
    positionXGadget.setText(Predesigner.twipsToUnitString(newPosition.getX()));
    positionYGadget.setText(Predesigner.twipsToUnitString(newPosition.getY()));
  }

  protected Point getNewPosition()
  {
    double x = field.getPosition().getX();
    double y = field.getPosition().getY();
    if (!Predesigner.twipsToUnitString(x).equals(positionXGadget.getText()))
    {
      x = Predesigner.unitStringToTwips(positionXGadget.getText()).doubleValue();
    }
    if (!Predesigner.twipsToUnitString(y).equals(positionYGadget.getText()))
    {
      y = Predesigner.unitStringToTwips(positionYGadget.getText()).doubleValue();
    }
    Point point = new Point();
    point.setLocation(x, y);    
    return point;
  }
  
  protected Double getNewWidth()
  {
    String widthString = Predesigner.twipsToUnitString(field.getWidth().doubleValue());
    if (widthString.equals(widthGadget.getText()))
    {
      return field.getWidth();
    }
    //else return Predesigner.unitStringToTwips(widthGadget.getText());
    double newWidth = Predesigner.unitStringToTwips(widthGadget.getText());
    if (newWidth < MIN_SHAPE_DIM)
    	newWidth = MIN_SHAPE_DIM;
    return new Double(newWidth);
  }
  
  protected Double getNewHeight()
  {
    String heightString = Predesigner.twipsToUnitString(field.getHeight().doubleValue());
    if (heightString.equals(heightGadget.getText()))
    {
      return field.getHeight();
    }
    
    double newHeight = Predesigner.unitStringToTwips(heightGadget.getText());
    if (newHeight < MIN_SHAPE_DIM)
    	newHeight = MIN_SHAPE_DIM;
    return new Double(newHeight);
  }
  
  /**
   * Validates user input and, if everything is fine, updates the field with
   * the values entered by the user
   * @return <code>null</code> if everything's hunky-dorey, or an error
   * message if some input fails validation.
   */
  public String updateField()
  {
    String result = super.updateField();

    if (result == null)   // no error
    {
      Drawing drawing = (Drawing)this.field;
      Double outlineWidth = (Double)(borderSizeGadget.getValue());
      drawing.setOutlineWidth(outlineWidth.doubleValue() * 20.0);
      
      double opacity = (double)(opacityGadget.getSelectedIndex() + 1) / 20.0;
      
      if (noFillGadget.isSelected())
    	{
    		opacity = 0;
    	}
      
    	drawing.setColor(new Color(selectedFillColor.getRed(),
					selectedFillColor.getGreen(),
					selectedFillColor.getBlue()));

    	if (drawing.getOutlineWidth() > 0)
    	{
    		drawing.setOutlineColor(new Color(selectedBorderColor.getRed(),
										selectedBorderColor.getGreen(),
										selectedBorderColor.getBlue()));  		
    	}
    	
     	drawing.setOpacity(Double.valueOf(opacity));
    }

    return result;
  }
  
  void onFillColorButton()
  {
    colorChooser.setColor(selectedFillColor);
    
    JDialog colorDialog = ColorChooser.createDialog(this, "Fill Color", true, colorChooser, 
      new ActionListener() { public void actionPerformed(ActionEvent e) {   // OK listener 
        selectedFillColor = colorChooser.getColor(); 
        fillColorPanel.setBackground(selectedFillColor); 
        }},
      new ActionListener() { public void actionPerformed(ActionEvent e) {   // Cancel listener
        colorChooser.setColor(selectedFillColor);
      }});
    
    colorDialog.setVisible(true);
    colorDialog.dispose();
  }
  
  void onBorderColorButton()
  {
    if (selectedBorderColor != null)
    {
      colorChooser.setColor(selectedBorderColor);
    }
    
    JDialog colorDialog = ColorChooser.createDialog(this, "Border Color", true, colorChooser, 
      new ActionListener() { public void actionPerformed(ActionEvent e) {   // OK listener 
        selectedBorderColor = colorChooser.getColor(); 
        borderColorPanel.setBackground(selectedBorderColor); 
        }},
      new ActionListener() { public void actionPerformed(ActionEvent e) {   // Cancel listener
        colorChooser.setColor(selectedBorderColor);
      }});
    
    colorDialog.setVisible(true);
    colorDialog.dispose();
  }
  
  void onImageConvertButton()
  {
  	updateField();
  	
    Drawing drawing = (Drawing)this.field;
    String imageGallery = Config.getDefaultImageGallery();
		String path = imageGallery + "/HighRes/User";
		String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String name = drawing.getDescription() + "_" + timestamp + ".png";
		
		// third parameter controls antialias, should be turned on except for rectilinear objects
		
		boolean bUseHints = true;
		if ((drawing.getRotation() == 0  || drawing.getRotation() == 90 || drawing.getRotation() == 180 || drawing.getRotation() == 270) && 
				drawing.getShape().getShapeString().equals("rect"))
			bUseHints = false;
		
		drawing.convertShapeToImage(path, name, bUseHints);
	
  	/*AveryImage ai = new AveryImage();
  	ai.setDescription(drawing.getDescription() + "_Image_" + timestamp);
  	ai.setPromptOrder("1.1");
  	ai.setSourceAndGallery(name, path);
  	Point position = drawing.getPosition();
  	position.x += 144;
  	position.y += 144; 	
  	ai.setPosition(position);
  	ai.setRotation(drawing.getRotation());
  	ai.setWidth(drawing.getWidth());
  	ai.setHeight(drawing.getHeight());
  	ai.setZLevel(drawing.getZLevel());
  	ai.setImageType(4);
    Predesigner.getProject().addFieldToMasterpanel(ai, Predesigner.getFrame().getCurrentMasterpanel());*/
  }
  
  void onBorderSizeSpinner()
  {
  	double outlineWidth = ((Double)borderSizeGadget.getValue()).doubleValue();
  	noFillGadget.setEnabled(outlineWidth > 0.0);
  	if (outlineWidth <= 0.0)
  	{
  		noFillGadget.setSelected(false);
	  	selectedFillColor = new Color(selectedFillColor.getRed(), selectedFillColor.getGreen(), selectedFillColor.getBlue(), 0xff);      	
		  fillColorPanel.setBackground(selectedFillColor);
	    fillColorButton.setEnabled(true);
	  	fillColorPanel.setVisible(true);
  	}
  		
  }
  
  void onNoFillGadget()
  {
	  if (noFillGadget.isSelected())
	  {
	  	selectedFillColor = new Color(selectedFillColor.getRed(), selectedFillColor.getGreen(), selectedFillColor.getBlue(), 0 );
	  	fillColorPanel.setVisible(false);
	    fillColorButton.setEnabled(false);
	    opacityGadget.setSelectedIndex(19);
	    opacityGadget.setEnabled(false);
	  }
	  else
	  {
	  	selectedFillColor = new Color(selectedFillColor.getRed(), selectedFillColor.getGreen(), selectedFillColor.getBlue(), 0xff);      	
		  fillColorPanel.setBackground(selectedFillColor);
	    fillColorButton.setEnabled(true);
	  	fillColorPanel.setVisible(true);
	    opacityGadget.setEnabled(true);
	  }
  }
  
  void onSpotColorBorderGadget()
  {
    Drawing drawing = (Drawing)this.field;

	  if (spotColorBorderGadget.isSelected())
	  {
	  	Color spotOutlineColor = new Color(0x76, 0x37, 0x36);
	  	if (AveryMasterpanel.SPOT_COLOR_NAME.equals("HCI-White"))
	  		spotOutlineColor = new Color(0xff, 0xff, 0xff);
			drawing.setOutlineColor(spotOutlineColor);
			drawing.setSpotColor(AveryMasterpanel.SPOT_COLOR_NAME);
			selectedBorderColor = spotOutlineColor;
	  }
	  else
	  {
			drawing.setSpotColor(null);
	  }
  }

}
