package com.avery.predesigner.editors;

/* Project: Predesigner 
 * Filename: BarcodeEditorPanel.java
 * Created on Jul 7, 2004 by Bob Lee
 * Copyright 2004 by Avery Dennison Corporation, all rights reserved
 */

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import com.avery.predesigner.Config;
import com.avery.predesigner.MergeMaps;
import com.avery.predesigner.Predesigner;
import com.avery.project.AveryPanelField;
import com.avery.project.Barcode;
import com.avery.utils.MergeMap;

public class BarcodeEditorPanel extends FieldEditorPanel
{
	private static final long serialVersionUID = 5323824406056359111L;
	private JTextField widthGadget = new JTextField();
	private JTextField heightGadget = new JTextField();
	private JTextField positionYGadget = new JTextField();
	private JTextField positionXGadget = new JTextField();
	
  private JComboBox mergeFieldGadget = new JComboBox();
  private Object lastSelection;
	
	/**
	 * @param field - the Barcode field to be edited
	 */
	public BarcodeEditorPanel(AveryPanelField field)
	{
		this.field = field;
		init();
	}
	
	private JComboBox comboBox = new JComboBox(Barcode.getSupportedBarcodeTypes());
	private JTextField dataGadget = new JTextField();

	/**
	 * called by constructor to initialize the editor panel
	 */
	protected void init()
	{
		super.init();
		this.add(new JSeparator());
		
  	this.add(new JLabel("Barcode Type:"));
		comboBox.setSelectedItem(getField().getBarcodeType());
		this.add(comboBox);
		
    String mergeMapName = Predesigner.getFrame().getCurrentMasterpanel().getMergeMapName();
    if (mergeMapName != null && mergeMapName.length() > 0)
    {
  		MergeMap map = MergeMaps.getMap(Config.mergeMapFile, mergeMapName, Predesigner.getProject().getLanguage());
    
	  	if (map != null)
	  	{
	  		List lines = map.getBarcodeLines(Predesigner.getProject().getLanguage());
	  		if (lines.size() > 0)
	  		{
		  		mergeFieldGadget.addItem("non-mergable line");
		  		if (getField().getMergeKey().equals(""))
		  		{
		  			mergeFieldGadget.setSelectedIndex(0);
		  		}
		  		
		    	Iterator iterator = lines.iterator();
		    	while (iterator.hasNext())
		    	{
		    		MergeMap.Line line = (MergeMap.Line)iterator.next();
		    		mergeFieldGadget.addItem(line);
		    		if (line.getName().equals(getField().getMergeKey()))
		    		{
		    			mergeFieldGadget.setSelectedItem(line);
		    		}
		    	}
		    	
		    	this.add(new JLabel("MergeMap Field:"));
		    	this.add(mergeFieldGadget);
		    	
		    	lastSelection = mergeFieldGadget.getSelectedItem();
		    	
		      mergeFieldGadget.addActionListener( new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		          onMergeFieldChanged(e);
		        }
		      });
	  		}
	  	}
    }
		
  	this.add(new JLabel("Data:"));
		dataGadget.setText(getField().getBarcodeData());
		this.add(dataGadget);
	}
	
	private Barcode getField()
	{
		return (Barcode)field;
	}
	
	String getDefaultPrompt()
	{
		return "Barcode";
	}
	
	/**
	 * Validates user input and, if everything is fine, updates the field with
	 * the values entered by the user
	 * @return <code>null</code> if everything's hunky-dorey, or an error
	 * message if some input fails validation.
	 */
	public String updateField()
	{
		String result = super.updateField();

		if (result == null)
		{
			String type = (String)comboBox.getSelectedItem();
			try
			{
				getField().resetBarcode(type, dataGadget.getText());
				
		    if (mergeFieldGadget.getSelectedIndex() > 0)
		    {
		    	MergeMap.Line selection = (MergeMap.Line)mergeFieldGadget.getSelectedItem();
		    	getField().setMergeKey(selection.getName());
		    }
		    else
		    {
		    	getField().setMergeKey("");
		    }
			}
			catch (Exception ex)
			{
				result = ex.getMessage() + " (BFO) " + type;
			}
		}
		return result;
	}
	
	protected void addFieldGeometryGadgets(JPanel panel, GridBagLayout gridbag)
	{
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridwidth = 1;
    
    gridbag.setConstraints(panel.add(new JLabel("Width:")), c);
    widthGadget.setText(Predesigner.twipsToUnitString(field.getWidth().doubleValue()));
    gridbag.setConstraints(panel.add(widthGadget), c);
    
    gridbag.setConstraints(panel.add(new JLabel(" ")), c);

    gridbag.setConstraints(panel.add(new JLabel("Height:")), c);
    heightGadget.setText(Predesigner.twipsToUnitString(field.getHeight().doubleValue()));
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(heightGadget), c);

    c.gridwidth = 1;
    gridbag.setConstraints(panel.add(new JLabel("Position X:")), c);
    positionXGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getX()));
    gridbag.setConstraints(panel.add(positionXGadget), c);
    gridbag.setConstraints(panel.add(new JLabel(" ")), c);

    gridbag.setConstraints(panel.add(new JLabel("Position Y:")), c);
    positionYGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getY()));
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(positionYGadget), c);
	}
	
	protected void updatePositionGadget(Point2D newPosition)
	{
    positionXGadget.setText(Predesigner.twipsToUnitString(newPosition.getX()));
    positionYGadget.setText(Predesigner.twipsToUnitString(newPosition.getY()));
	}
	
	protected Point getNewPosition()
	{
    double x = getField().getPosition().getX();
    double y = getField().getPosition().getY();
    if (!Predesigner.twipsToUnitString(x).equals(positionXGadget.getText()))
    {
      x = Predesigner.unitStringToTwips(positionXGadget.getText()).doubleValue();
    }
    if (!Predesigner.twipsToUnitString(y).equals(positionYGadget.getText()))
    {
      y = Predesigner.unitStringToTwips(positionYGadget.getText()).doubleValue();
    }
		Point point = new Point();
		point.setLocation(x, y);		
		return point;
	}
	
  protected Double getNewWidth()
  {
    String widthString = Predesigner.twipsToUnitString(getField().getWidth().doubleValue());
    if (widthString.equals(widthGadget.getText()))
    {
      return getField().getWidth();
    }
    else return Predesigner.unitStringToTwips(widthGadget.getText());
  }
  
  protected Double getNewHeight()
  {
    String heightString = Predesigner.twipsToUnitString(getField().getHeight().doubleValue());
    if (heightString.equals(heightGadget.getText()))
    {
      return getField().getHeight();
    }
    else return Predesigner.unitStringToTwips(heightGadget.getText());
  }
	
  private void onMergeFieldChanged(ActionEvent e)
  {
  	Object selection = mergeFieldGadget.getSelectedItem();
  	if (!selection.equals(lastSelection))
  	{
  		if (selection instanceof MergeMap.Line)
  		{
  			dataGadget.setText(((MergeMap.Line)selection).getText());
  		}
  		
  		lastSelection = selection;
  	}
  }
}
