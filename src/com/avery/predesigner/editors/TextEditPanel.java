package com.avery.predesigner.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import com.avery.predesigner.Predesigner;
import com.avery.project.AveryTextblock;
import com.avery.project.Text;
import com.avery.utils.FontEncoding;

public class TextEditPanel extends JPanel
{

  //private JTextArea textArea = null;
  private AveryTextblock atb = null;
  private JSpinner pointsizeGadget;
  //TextBlockEditorPanel tbep = null;
  private JPanel textPanel = new JPanel();
  private JTextArea textArea = new JTextArea();
  private JButton updateButton = null;
  

	public TextEditPanel()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.setAlignmentX(LEFT_ALIGNMENT);
		this.setMaximumSize(new Dimension(1500, 200));
		this.setSize(new Dimension(600, 100));
		init();
	}
	
	private void init()
	{
  	/*textArea = new JTextArea();
    this.add(textArea);*/
    Font font = new Font("Arial", Font.PLAIN, 16);
    textArea.setFont(font);
    Border border = BorderFactory.createLineBorder(Color.red);
    textArea.setBorder(border);
    textArea.setLineWrap(true);
    //Color backcolor = new Color(0xff, 0xff, 0xff, 100);
    //textArea.setBackground(backcolor);
    JScrollPane scroll = new JScrollPane (textArea);
    scroll.setMinimumSize(new Dimension(400, 60));
    scroll.setPreferredSize(new Dimension(600, 60));
    //scroll.setMaximumSize(new Dimension(1400, 120));
    
    scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    //textArea.setSize(new Dimension(500, 150));
    //textArea.setBounds(new Rectangle(0,0,500,150));
		pointsizeGadget = new JSpinner(new SpinnerNumberModel(
    		10, 6, 200, 0.5));
		pointsizeGadget.setSize(70, 30);
		pointsizeGadget.setBounds(0,0,70,30);
		pointsizeGadget.setPreferredSize(new Dimension(70,30));
		pointsizeGadget.setMaximumSize(new Dimension(70,30));
		
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.X_AXIS));
		
		//GridBagLayout gridbag = new GridBagLayout();
    //GridBagConstraints c = new GridBagConstraints();
    //c.fill = GridBagConstraints.HORIZONTAL;
    //c.weightx = 1.0;
    //c.gridwidth = 2;
    //gridbag.setConstraints(textPanel.add(pointsizeGadget),c);
    //c.gridwidth = GridBagConstraints.REMAINDER;
    //gridbag.setConstraints(textPanel.add(textArea), c);
		JSeparator seperator = new JSeparator(SwingConstants.HORIZONTAL);
		seperator.setMaximumSize( new Dimension(3, 1) );
		seperator.setMinimumSize( new Dimension(3, 1) );
		seperator.setPreferredSize( new Dimension(3, 1) );
		textPanel.add(seperator);
		textPanel.add(scroll);
		textPanel.add(seperator);
		textPanel.add(pointsizeGadget);
		textPanel.add(seperator);
		add(textPanel);
		//pointsizeGadget = new JSpinner(new SpinnerNumberModel(
    		//getField().getPointsize().doubleValue(),
    		//minimumSize, getField().getHeight().doubleValue() / 20.0, 0.5));
		updateButton = new JButton("Update");
		updateButton.setToolTipText("Update Text");
    updateButton.addActionListener(new ActionListener()
    { 
	    public void actionPerformed(ActionEvent e)
	    {
	        //Execute when button is pressed
	        //System.out.println("You clicked the button");
	        updateText();
	    	
	    	//if (tbep != null)
	    	{
	    		//tbep.updateField();
	    		Predesigner.getFrame().repaint();
	    	}
	    }
    });   
  
    this.add(updateButton, BorderLayout.EAST);
    
    // how to make this visible??
    //this.setVisible(false);
    //MasterpanelToolbar mpt = Predesigner.getFrame().getMasterpanelToolbar();

    //Container contentPane = Predesigner.getFrame().getContentPane();
    //mpt.add(this); //, BorderLayout.EAST);
    //mpv.add(this, BorderLayout.SOUTH);
	}
	
	public void setTextLines(AveryTextblock atb)
	{
		this.atb = atb;
		SpinnerNumberModel snm = (SpinnerNumberModel)pointsizeGadget.getModel();
    String theFont = atb.getTypeface();
    // the font name can include the style, need to strip the style off if present
    theFont = getFontFamily(theFont);
    double minimumSize = FontEncoding.getMinFontSize(theFont);
    double maximumSize = atb.getHeight().doubleValue() / 20.0;
    if (maximumSize < minimumSize)
    	maximumSize = minimumSize + 4;
    double currentSize = atb.getPointsize().doubleValue();
    if (currentSize < minimumSize)
    	currentSize = minimumSize;
    else if (currentSize > maximumSize)
    	currentSize = maximumSize;
    
		snm.setMinimum(minimumSize);
		snm.setMaximum(maximumSize);
		snm.setValue(currentSize);
		
		//if (tbep != null)
			//this.remove(tbep);
    //tbep = new TextBlockEditorPanel(atb, false);
    //this.add(tbep);

    ArrayList lines = (ArrayList)atb.getLines();
		
    String text = new String();
    Iterator iterator = lines.iterator();
    
    while(iterator.hasNext())
    {
    	String line = iterator.next().toString();
      text += line;
      if (!line.endsWith("\n") && iterator.hasNext())
      {
      	text += "\n";
      }
    }
    textArea.setText(text);
    //textArea.setRows(4); //lines.size() > 5 ? lines.size() : 5);
   // textArea.setColumns(60);		
	}
	
	private void updateText()
	{
  	List lines = new ArrayList();
  	Object o = pointsizeGadget.getValue();
  	Double pointsize = (Double)pointsizeGadget.getValue();
  	atb.setPointsize(pointsize);
    for (int i = 0; i < textArea.getLineCount(); i++)
    {
	    try
	    {
	      {
	      	int start = textArea.getLineStartOffset(i);
	        int end = textArea.getLineEndOffset(i);
	        int length = end - start;
	        String content = textArea.getText(start, length);
	        if (content.endsWith("\n"))
	          content = content.substring(0, content.length() - 1);
	        lines.add(new Text(content));
	      }
	    }
	    catch (Exception e)
	    {
	      System.out.println("JTextArea problem: " + e.getMessage());
	      break;
	    }
    }
    atb.setLines(lines);
    Predesigner.getFrame().repaint();
	}
	
	public void clearText()
	{
		textArea.setText("");
	}
  // strips style strings from font names if found
  // strips bold or italic or bolditalic or even italicbold
  private String getFontFamily(String font)
  {
    String theFont = font;
    String lowFont = font.toLowerCase();
    int pos = lowFont.indexOf(" bold");
    if (pos < 2)
      pos = lowFont.indexOf(" italic");

    if (pos >= 2)
    {
      theFont = theFont.substring(0, pos);
    }
    return theFont;
  }
}
