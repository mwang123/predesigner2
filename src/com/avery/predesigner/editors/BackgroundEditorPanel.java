/*
 * BackgroundEditorPanel.java Created on May 10, 2005 by Bob Lee
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner.editors;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import com.avery.predesign.StyleSet;
import com.avery.predesigner.ColorChooser;
import com.avery.predesigner.Config;
import com.avery.predesigner.Predesigner;
import com.avery.project.AveryBackground;
import com.avery.project.AveryPanelField;
import com.avery.project.Drawing;
import com.avery.utils.AveryUtils;

/**
 * @author Bob Lee, Brad Nelson
 */
class BackgroundEditorPanel extends ImageEditorPanel
{
	private static final long serialVersionUID = -5192824042968681731L;

  private JPanel fillColorPanel = new JPanel();
  private Color selectedFillColor;
  private ColorChooser colorChooser;

	/**
	 * @param field
	 */
	BackgroundEditorPanel(AveryPanelField field)
	{
    imageListButtonSize = 88;
    previewWidth = 232;
    previewHeight = 232;
    this.field = field;
    colorChooser = ColorChooser.getColorChooser();
    init();
	}
	
	// swing Components used in the positioning UI
  private ButtonGroup buttonGroup;
  private JRadioButton bleedButton;
  private JRadioButton fitButton;
  private JRadioButton insetButton;
  private JRadioButton unscaledButton;
  private JSpinner bleedSpinner;
  private JSpinner insetSpinner;
  private JButton fillColorButton = new JButton("Fill Color...");
  private JButton textureButton = new JButton("Textures...");
  private JButton patternButton = new JButton("Patterns...");

	//private boolean orthogonal;			// if false, revert to ImageEditorPanel UI
	
	protected void addFieldGeometryGadgets(JPanel panel, GridBagLayout gridbag)
	{
		Point position = field.getPosition();
		//orthogonal = position.x == position.y;
		
		AveryBackground ab = (AveryBackground)field;
    selectedFillColor = new Color(ab.getBackgroundcolor().getRed(),
				ab.getBackgroundcolor().getGreen(),
				ab.getBackgroundcolor().getBlue(),
				0xff);
    fillColorPanel.setBackground(selectedFillColor);

    fillColorButton.setEnabled(true);
  	fillColorPanel.setVisible(true);

  	textureButton.setMaximumSize(new Dimension(140, 30));
  	textureButton.setPreferredSize(new Dimension(140,30));
  	patternButton.setMaximumSize(new Dimension(140, 30));
  	patternButton.setPreferredSize(new Dimension(140, 30));
  	JPanel tpPanel = new JPanel();
  	tpPanel.add(textureButton);
  	tpPanel.add(patternButton);
		
		//if (orthogonal)
		{
		  bleedButton = new JRadioButton("Bleed");
		  bleedButton.setActionCommand("bleed");

		  fitButton = new JRadioButton("Fit");
		  fitButton.setActionCommand("fit");
		  
		  insetButton = new JRadioButton("Inset");
		  insetButton.setActionCommand("inset");
		  
		  unscaledButton = new JRadioButton("Keep original size");
		  unscaledButton.setActionCommand("unscaled");
			
		  buttonGroup = new ButtonGroup();
			buttonGroup.add(bleedButton);
			buttonGroup.add(fitButton);
			buttonGroup.add(insetButton);
			buttonGroup.add(unscaledButton);
			
			double bleed = Predesigner.twipsToUnits(Config.getDefaultBleed());
			double inset = 0;
			if (position.x < 0)
			{
				bleed = -Predesigner.twipsToUnits(position.getX());
				bleedButton.setSelected(true);
			}
			else if (position.x > 0)
			{
				inset = Predesigner.twipsToUnits(position.getX());
				insetButton.setSelected(true);
			}
			else
			{
				fitButton.setSelected(true);
			}
			
			double spinmax = 1;
			double spinstep = 0.5;
			
			if (bleed > 1.0)
			{
				spinmax = bleed;
				spinstep = bleed / 20;
			}
	    SpinnerModel bleedSpinnerModel = new SpinnerNumberModel(
	    		bleed, 0.0, spinmax, spinstep);
			bleedSpinner = new JSpinner(bleedSpinnerModel);
			
			double insetMax = 1.0;
			double insetStep = 0.05;
			if (Config.units == AveryUtils.CENTIMETERS)
			{
				insetMax = 2.0;
				insetStep = 0.1;				
			}
			if (Config.units == AveryUtils.TWIPS)
			{
				insetMax = 1440.0;
				insetStep = 72.0;				
			}
	    SpinnerModel insetSpinnerModel = new SpinnerNumberModel(
	    		inset, 0.0, insetMax, insetStep);
			insetSpinner = new JSpinner(insetSpinnerModel);
			
	    GridBagConstraints c = new GridBagConstraints();
	    c.fill = GridBagConstraints.HORIZONTAL;
	    
	    // add fillColor to panel
	    c.gridwidth = 1;
	    gridbag.setConstraints(panel.add(fillColorPanel), c);
	    c.gridwidth = GridBagConstraints.REMAINDER;
	    //gridbag.setConstraints(panel.add(fillColorButton), c);
	    fillColorPanel.add(fillColorButton);
	    
	    c.gridwidth = GridBagConstraints.REMAINDER;
	    gridbag.setConstraints(panel.add(tpPanel), c);

      c.gridwidth = GridBagConstraints.REMAINDER;
      panel.add(new JLabel(" "), c);
	    panel.add(new JLabel("Select Size Model:"), c);
			
	    c.gridwidth = 1;
	    panel.add(new JLabel(" "), c);
	    panel.add(bleedButton, c);
	    c.gridwidth = GridBagConstraints.REMAINDER;
	    panel.add(bleedSpinner, c);
			
	    c.gridwidth = 1;
	    panel.add(new JLabel(" "), c);
      c.gridwidth = GridBagConstraints.REMAINDER;
	    panel.add(fitButton, c);

	    c.gridwidth = 1;
      panel.add(new JLabel(" "), c);
	    panel.add(insetButton, c);
	    c.gridwidth = GridBagConstraints.REMAINDER;
	    panel.add(insetSpinner, c);
	    
	    c.gridwidth = 1;
	    panel.add(new JLabel(" "), c);
      c.gridwidth = GridBagConstraints.REMAINDER;
	    panel.add(unscaledButton, c);
	    
	    fillColorButton.addActionListener(new ActionListener()
	    {
	      public void actionPerformed(ActionEvent e)
	      {
	        onFillColorButton();
	      }
	    });
	    
	    textureButton.addActionListener(new ActionListener()
	    {
	      public void actionPerformed(ActionEvent e)
	      {
	        onTextureButton((AveryBackground)field);
	      }
	    });
	    
	    patternButton.addActionListener(new ActionListener()
	    {
	      public void actionPerformed(ActionEvent e)
	      {
	        onPatternButton((AveryBackground)field);
	      }
	    });

		}
		/*else // legacy non-orthogonal background
		{
			super.addFieldGeometryGadgets(panel, gridbag);
		}*/
	}
	
	/**
	 * fetches the user's preferred position
	 * @return Point in twips suitable for AveryPanelField.setPosition
	 */
	protected Point getNewPosition()
	{
		//if (orthogonal)
		{
			Point point = new Point();
			if (bleedButton.isSelected())
			{
		  	String str = ((Double)bleedSpinner.getValue()).toString();
		  	Double d = Predesigner.unitStringToTwips(str);
		  	point.setLocation(-d.doubleValue(), -d.doubleValue());
			}
			else if (insetButton.isSelected())
			{
		  	String str = ((Double)insetSpinner.getValue()).toString();
		  	Double d = Predesigner.unitStringToTwips(str);
		  	point.setLocation(d.doubleValue(), d.doubleValue());
			}
			else if (unscaledButton.isSelected())
			{
				AveryBackground ab = (AveryBackground)field;
				point = ab.getPosition();
			}
			
			return point;
		}
		/*else // legacy non-orthogonal background
		{
			return super.getNewPosition();
		}*/
	}
	
	/**
	 * fetches the user's preferred width
	 * @return width in twips suitable for AveryPanelField.setWidth
	 */
	protected Double getNewWidth()
	{
		//if (orthogonal)
		{
			double width = Predesigner.getFrame().getCurrentMasterpanel().getWidth().doubleValue();
			if (bleedButton.isSelected())
			{
		  	String str = ((Double)bleedSpinner.getValue()).toString();
		  	Double d = Predesigner.unitStringToTwips(str);
		  	double add = d.doubleValue();
		  	width += add + add;
			}
			else if (insetButton.isSelected())
			{
		  	String str = ((Double)insetSpinner.getValue()).toString();
		  	Double d = Predesigner.unitStringToTwips(str);
		  	double sub = d.doubleValue();
		  	width -= sub + sub;
			}
			else if (unscaledButton.isSelected())
			{
				AveryBackground ab = (AveryBackground)field;
				ab.setUnscaled(true);
				width = ab.getWidth().doubleValue();
			}
			return new Double(width);
		}
		/*else // legacy non-orthogonal background
		{
			return super.getNewWidth();
		}*/
	}
	
	/**
	 * fetches the user's preferred field height
	 * @return height in twips suitable for AveryPanelField.setHeight
	 */
	protected Double getNewHeight()
	{
		//if (orthogonal)
		{
			double height = Predesigner.getFrame().getCurrentMasterpanel().getHeight().doubleValue();
			if (bleedButton.isSelected())
			{
		  	String str = ((Double)bleedSpinner.getValue()).toString();
		  	Double d = Predesigner.unitStringToTwips(str);
		  	double add = d.doubleValue();
		  	height += add + add;
			}
			else if (insetButton.isSelected())
			{
		  	String str = ((Double)insetSpinner.getValue()).toString();
		  	Double d = Predesigner.unitStringToTwips(str);
		  	double sub = d.doubleValue();
		  	height -= sub + sub;
			}
			else if (unscaledButton.isSelected())
			{
				AveryBackground ab = (AveryBackground)field;
				height = ab.getHeight().doubleValue();
			}
			return new Double(height);
		}
		/*else // legacy non-orthogonal background
		{
			return super.getNewHeight();
		}*/
	}
  
  public String updateField()
  {
    String result = super.updateField();

    if (result == null)   // no error
    {
      AveryBackground ab = (AveryBackground)this.field;
    	ab.setBackgroundcolor(new Color(selectedFillColor.getRed(),
					selectedFillColor.getGreen(),
					selectedFillColor.getBlue()));
    }
    return result;
  }
  
  void onFillColorButton()
  {
    colorChooser.setColor(selectedFillColor);
    
    JDialog colorDialog = ColorChooser.createDialog(this, "Fill Color", true, colorChooser, 
      new ActionListener() { public void actionPerformed(ActionEvent e)
      {   // OK listener 
        selectedFillColor = colorChooser.getColor(); 
        fillColorPanel.setBackground(selectedFillColor);
        // clear source and gallery
        AveryBackground ab = (AveryBackground)field;
        ab.setSourceAndGallery("", "");
        
      }},
      new ActionListener() { public void actionPerformed(ActionEvent e) {   // Cancel listener
        colorChooser.setColor(selectedFillColor);
      }});
    
    colorDialog.setVisible(true);
    colorDialog.dispose();
  }
  
  void onTextureButton(AveryBackground field)
  {
    TextureEditorPanel ui = TextureEditorPanel.getEditorPanel(field);
    if (ui.init())
    {
	    if (JOptionPane.showConfirmDialog(this, ui, "Create Texture Background",
	        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
	          == JOptionPane.OK_OPTION)
	    {
	    	field.clearCachedImage();
	    	ui.savePreview();
	      Image image = generatePreview();
	      reAspectFieldToImage(image);
	      invalidate();
	    }
    }
  }

  void onPatternButton(AveryBackground field)
  {
    PatternEditorPanel ui = PatternEditorPanel.getEditorPanel(field);
    if (ui.init())
    {
	    if (JOptionPane.showConfirmDialog(this, ui, "Create Pattern Background",
	        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
	          == JOptionPane.OK_OPTION)
	    {
	    	field.clearCachedImage();
	    	ui.savePreview();
	      Image image = generatePreview();
	      reAspectFieldToImage(image);
	      invalidate();    	
	    }
    }
  }

  ArrayList makeImageList(StyleSet ss)
  {
    ArrayList filenames = new ArrayList(5);
    filenames.add(ss.graphic02);
    filenames.add(ss.graphic04);
    filenames.add(ss.graphic05);
    filenames.add(ss.graphic06);
    filenames.add(ss.graphic07);
    return filenames;
  }
}
