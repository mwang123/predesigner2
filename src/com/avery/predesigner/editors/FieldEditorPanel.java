package com.avery.predesigner.editors;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.avery.predesigner.Predesigner;
import com.avery.project.AveryBackground;
import com.avery.project.AveryImage;
import com.avery.project.AveryPanelField;
import com.avery.project.AveryTextfield;
import com.avery.project.Barcode;
import com.avery.project.Drawing;
import com.borland.jbcl.layout.VerticalFlowLayout;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: base class for the AveryPanelField editor object</p>
 * <p>Copyright: Copyright 2016 Avery Products Corporation, all rights reserved</p>
 * <p>Company: Avery Products Corporation</p>
 * @author Bob Lee, Brad Nelson
 * @version 2.0
 */

abstract public class FieldEditorPanel extends JPanel
{
  protected FieldEditorPanel()
  {
  }

  FieldEditorPanel(AveryPanelField field)
  {
    this.field = field;
    init();
  }

  protected AveryPanelField field;

  /**
   * This is private so that derived classes can have their own versions
   * return their specific type of AveryPanelField.
   * @return the field itself
   */
  private AveryPanelField getField()
  {
    return this.field;
  }
  
  abstract String getDefaultPrompt(); 

  /**
   * Factory method.  Returns a new FieldEditorPanel derived from this class,
   * but customized to match the specific type of AveryPanelField.
   * @param field - the field that the caller wants an editor for
   * @return the appropriate editor
   */
  public static FieldEditorPanel getEditorPanel(AveryPanelField field, boolean isHTML)
  {
    if (field instanceof AveryTextfield)
    {
      return TextFieldEditorPanel.getEditorPanel((AveryTextfield)field, isHTML);
    }
    else if (field instanceof AveryBackground)
    {
    	return new BackgroundEditorPanel(field);
    }
    else if (field instanceof AveryImage)
    {
      return new ImageEditorPanel(field);
    }
    else if (field instanceof Barcode)
    {
    	return new BarcodeEditorPanel(field);
    }
    else if (field instanceof Drawing)
    {
      return new ShapeEditorPanel(field);
    }
    else  // oops
    {
      return null; //return new FieldEditorPanel(field);
    }
  }

  // UI elements needed by all FieldEditorPanels
  // private JPanel commonPanel = new JPanel();
	protected JComboBox descriptionGadget;
	private JButton rotationGadget = new JButton();
	private JCheckBox editableGadget = new JCheckBox("Editable");
  private JCheckBox printGadget = new JCheckBox("Printable");
  private JCheckBox styleableGadget = new JCheckBox("Styleable");
  private JCheckBox movableGadget = new JCheckBox("Movable");
  private JCheckBox previewableGadget = new JCheckBox("Previewable");
	protected JComboBox opacityGadget = null;

  /**
   * called by constructor to initialize the editor panel
   */
  protected void init()
  {
    setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP));
	
    // add the nicely formed gridPanel to the big panel 
    this.add(createCommonControlsPanel());
  }
  
  /**
   * creates a JPanel containing the controls common to all panelfield types
   * @return a nicely formatted JPanel of controls
   */
  JPanel createCommonControlsPanel()
  {
    JPanel gridPanel = new JPanel();
    GridBagLayout gridbag = new GridBagLayout();
    gridPanel.setLayout(gridbag);
     
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0;
    c.gridwidth = 1;

    // add the Description row to the gridPanel
    gridPanel.add(new JLabel("Description:"), c);
    
    java.util.List descriptions = field.getLocalizableDescriptions(Predesigner.getProject().getLanguage());
    initDescriptionGadget(descriptions, field.getDescription());
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridPanel.add(descriptionGadget,c);
    
    // add the geometry rows from the dervied class to the gridPanel
    addFieldGeometryGadgets(gridPanel, gridbag);

    // set up rotation
    rotationGadget.setText(field.getRotation().toString());
  
    if (field instanceof AveryBackground == false)
    { 
      // not a background - allow rotation
      rotationGadget.addActionListener( new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          onRotate();
        }
      });

      // add the rotation button to the gridPanel
      c.gridwidth = 1;
      gridPanel.add(new JLabel("Rotation:"), c);   
      gridPanel.add(rotationGadget, c);
      gridPanel.add(new JLabel(" "), c);
    }

    // add the "editable" checkbox to the gridPanel
    editableGadget.setSelected(field.isEditable()); 
    gridPanel.add(editableGadget, c);
    
    // the "print" checkbox
    printGadget.setSelected(field.getPrint()); 
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridPanel.add(printGadget, c);
    
   // Styleable and Movable
    styleableGadget.setSelected(field.isStyleable());
    movableGadget.setSelected(field.isMovable());
    previewableGadget.setSelected(field.isPreviewable());
    
    if (field instanceof AveryTextfield)
    {
      c.gridwidth = 3;
      gridPanel.add(new JPanel(), c);
      c.gridwidth = 1;
      gridPanel.add(styleableGadget, c);
      gridPanel.add(movableGadget, c);
      c.gridwidth = GridBagConstraints.REMAINDER;
      gridPanel.add(previewableGadget, c);
    }

    if (field instanceof AveryBackground ||
    		field instanceof AveryImage ||
    		field instanceof Drawing)
    {
      c.gridwidth = 3;
      gridPanel.add(new JPanel(), c);
      c.gridwidth = 1;
      gridPanel.add(movableGadget, c);
      c.gridwidth = GridBagConstraints.REMAINDER;
      gridPanel.add(previewableGadget, c);    	
    }
    
    
    if (field instanceof AveryTextfield ||
    		field instanceof AveryBackground ||
    		field instanceof AveryImage ||
    		field instanceof Drawing)
    {
	    c.gridwidth = 1;
	    gridPanel.add(new JLabel("Opacity Percent:"), c);
	    int opacityIndex = (int)(getField().getOpacity().doubleValue() * 20.0) - 1;
	    if (opacityIndex < 1)
	    	opacityIndex = 0;
	    initOpacityGadget(opacityIndex);
	    gridPanel.add(opacityGadget, c);
    }
    
    // initialize the column weights for the gridPanel's layout
    final double weights[] = { 1, 1, 1, 1, 1 };
    gridbag.columnWeights = weights;
    
    return gridPanel;
  }

  /**
   * Check the values of all of the fields.  Derived classes should override
   * this, and start by calling the super method.
   * @return <code>null</code> if everything's hunky-dorey, or an error
   * message if some input fails validation.
   */
  protected String validateUserInput()
  {
    String result = null;
    String controlName = " ";
    try
    {
      controlName = "Description";
      Iterator i = Predesigner.getFrame().getCurrentMasterpanel().getFieldIterator();
      while (i.hasNext())
      {
        AveryPanelField field = (AveryPanelField)i.next();
        if (field.equals(getField()))
          continue;
        
        if (field.getDescription().equals(descriptionGadget.getSelectedItem()))
        {
          throw new Exception(" is a duplicate");
        }
      }

      int minSize = 100;
      if (field instanceof Drawing)
      	minSize = ShapeEditorPanel.MIN_SHAPE_DIM;
      
      controlName = "Width";
      if (getNewWidth().doubleValue() < minSize)
      {
      	
        throw new Exception(" is too small");
      }

      controlName = "Height";
      if (getNewHeight().doubleValue() < minSize)
      {
        throw new Exception(" is too small");
      }
    }
    catch (Exception e)
    {
      // something failed
      result = controlName + e.getMessage();
    }

    return result;
  }

  /**
   * Validates user input and, if everything is fine, updates the field with
   * the values entered by the user
   * @return <code>null</code> if everything's hunky-dorey, or an error
   * message if some input fails validation.
   */
  public String updateField()
  {
    String result = validateUserInput();

    if (result == null)
    {
    	String description = (String)descriptionGadget.getSelectedItem();
    	if (!description.equals(getField().getDescription()))
    	{
    		getField().setDescription(description);
    	}

      getField().setWidth(getNewWidth());
      getField().setHeight(getNewHeight());
      getField().setPosition(getNewPosition());
      getField().setRotation(new Double(rotationGadget.getText()));
      getField().setEditable(editableGadget.isSelected());
      getField().setPrint(printGadget.isSelected());
      getField().setStyleable(styleableGadget.isSelected());
      getField().setMovable(movableGadget.isSelected());
      getField().setPreviewable(previewableGadget.isSelected());
      if (opacityGadget != null)
      {
      	double opacity = (double)(opacityGadget.getSelectedIndex() + 1) / 20.0;
      	getField().setOpacity(new Double(opacity));
      }
    }
    return result;
  }

  
  /**
   * called when the user presses the rotation button
   */
  void onRotate()
  {
    RotationPanel ui = new RotationPanel(field, Double.parseDouble(rotationGadget.getText()), null, false);

    if (JOptionPane.showConfirmDialog(this, ui, "Set Rotation (Counter-clockwise)",
      JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
        == JOptionPane.OK_OPTION)
    {
      double oldRotation = field.getRotation().doubleValue();
      double newRotation = Double.parseDouble(ui.getRotation());
      
      if (oldRotation != newRotation)
      {
        // calculate a new position as if object was rotated around its center point 
        double oldtheta = oldRotation * Math.PI / 180.0;
        double newtheta = newRotation * Math.PI / 180.0;

        Rectangle bounds = field.getBoundingRect();
        int centerX = bounds.x + (bounds.width / 2);
        int centerY = bounds.y + (bounds.height / 2);
        
        AffineTransform aft = AffineTransform.getRotateInstance(oldtheta - newtheta, centerX, centerY);
        Point2D newPosition = new Point();
        aft.transform(field.getPosition(), newPosition);
        
        // set the new position in the UI gadgets
        updatePositionGadget(newPosition);

        // set the new rotation value in the rotation gadget
        rotationGadget.setText(Double.toString(newRotation));
      }
    }
  }
  
	private void initDescriptionGadget(List descriptions, String current)
	{
		descriptionGadget = new JComboBox(descriptions.toArray());
		descriptionGadget.setSelectedItem(current);
		if (!descriptionGadget.getSelectedItem().equals(current))
		{
			descriptionGadget.addItem(current);
			descriptionGadget.setSelectedItem(current);
		}
		
		// check for unique description, generate one if not unique
    int tries = 0;
    boolean isUnique = false;
    String proposed = (String)descriptionGadget.getSelectedItem();
    while (!isUnique && tries++ < 200)
    {
    	isUnique = isUniqueDescription(proposed);
    	if (isUnique)
    	{
    		descriptionGadget.setSelectedItem(proposed);
    		if (!descriptionGadget.getSelectedItem().equals(proposed))
    		{
    			descriptionGadget.addItem(proposed);
    			descriptionGadget.setSelectedItem(proposed);
    		}
    		break;
    	}
    	proposed = stripProposed(proposed, tries);
    }
    
		descriptionGadget.setEditable(true);
	}
	
  private boolean isUniqueDescription(String proposed)
  {
  	Iterator i = Predesigner.getFrame().getCurrentMasterpanel().getFieldIterator();
    while (i.hasNext())
    {
      AveryPanelField field = (AveryPanelField)i.next();
      if (field.equals(getField()))
        continue;
      
      if (field.getDescription().equals(proposed))
      {
      	return false;
      }
    }
    return true;
  }
  
  private String stripProposed(String proposed, int append)
  {
  	proposed = proposed.replaceAll("\\d*$", "");
  	proposed = proposed.trim();
  	proposed += " "+ append;
  	
  	return proposed;
  }

	private void initOpacityGadget(int selectedIndex)
	{
		String opacities[] = new String[]{"5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60", "65", "70", "75", "80", "85", "90", "95", "100"};
		opacityGadget = new JComboBox(opacities);
		opacityGadget.setSelectedIndex(selectedIndex);
		opacityGadget.setEditable(false);
	}
	
	/**
	 * the derived class should create a JPanel with size and position
	 * gadgets as appropriate for the field type  
	 * @return the JPanel, which will be placed near the top of this editorPanel
	 */
	// protected abstract JPanel getFieldGeometryPanel();
	
	protected abstract void addFieldGeometryGadgets(JPanel panel, GridBagLayout gridbag);
	
	/**
	 * This method is to update the fieldGeometryPanel after the user
	 * does a rotation. 
	 * @param newPosition the position after a rotation has occurred
	 */
	protected abstract void updatePositionGadget(Point2D newPosition);
	
	
	/**
	 * fetches the user's preferred position
	 * @return Point in twips suitable for AveryPanelField.setPosition
	 */
	protected abstract Point getNewPosition();
	
	/**
	 * fetches the user's preferred width
	 * @return width in twips suitable for AveryPanelField.setWidth
	 */
	protected abstract Double getNewWidth();
	
	/**
	 * fetches the user's preferred field height
	 * @return height in twips suitable for AveryPanelField.setHeight
	 */
	protected abstract Double getNewHeight();
	
	/**
	 * A protected internal class for consistency of layout design in dervied classes.
	 * This creates a single row GridLayout JPanel with 3 components.
	 */
  protected class TriPanel extends JPanel
	{
		private static final long serialVersionUID = 6820654002692720513L;

		TriPanel(JComponent one, JComponent two, JComponent three)
		{
      setLayout(new GridLayout(1,3));
      add(one);
  		add(two);
  		add(three);
		}
		
  	TriPanel(JComponent one, JComponent two)
		{
      setLayout(new GridLayout(1,3));
      add(one);
  		add(two);
		}
	}
  
  /**
	 * A protected internal class for consistency of layout design in dervied classes.
	 * This creates a single row GridBagLayout JPanel with 2 components.  The second
	 * component uses 2/3 of the space.
	 */
  protected class OneTwoPanel extends JPanel
	{
		private static final long serialVersionUID = -5002216757888257707L;

		OneTwoPanel(JComponent one, JComponent two)
  	{
  		init(one, two);
  	}
  	
  	OneTwoPanel(String label, JComponent gadget)
  	{
  		init(new JLabel(label), gadget);
  	}
  	
  	private void init(JComponent one, JComponent two)  	
		{
      GridBagLayout gridbag = new GridBagLayout();
      setLayout(gridbag);
      
      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.HORIZONTAL;     
      
      add(one, c);
  		c.gridwidth = 1;
      gridbag.setConstraints(two, c);
  		
  		add(two);
  		c.gridwidth = 2;
  		gridbag.setConstraints(two, c);
  		
      double weights[] = { 1, 2, 2 };
      gridbag.columnWeights = weights;
		}
	}
}