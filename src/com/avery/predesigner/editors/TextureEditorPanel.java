package com.avery.predesigner.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.avery.predesigner.ColorChooser;
import com.avery.predesigner.Config;
import com.avery.predesigner.ImageFileList;
import com.avery.predesigner.Predesigner;
import com.avery.project.AveryBackground;
import com.avery.project.AveryMasterpanel;
import com.borland.jbcl.layout.VerticalFlowLayout;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright 2016 Avery Products Corporation, all rights reserved</p>
 * <p>Company: Avery Products Corporation</p>
 * @author Brad Nelson
 * @version 2.0
 */

public class TextureEditorPanel extends JPanel
{
  protected int imageListButtonSize = 64;
	private static final long serialVersionUID = -1245785865082237240L;
	
	protected  AveryBackground field;
	private String outfileName = "";
	
  private JSpinner scaleGadget;
	private JCheckBox tileStretchGadget = new JCheckBox("Stretch Tile to Fit");
	private JCheckBox tileRotateGadget = new JCheckBox("Rotate Tiles Randomly");
  private JButton applyButton = new JButton("Apply");
  private JList imageListt;
  //private JList imageListp;
  private JScrollPane scrollImageListt;
  //private JScrollPane scrollImageListp;
  private JPanel imagesPanel = null;
  private JListWithImages jlwit = null;
  //private JListWithImages jlwip = null;
  private DefaultListModel dlmt = null;
  private DefaultListModel dlmp = null;
  protected int previewWidth = 400;
  protected int previewHeight = 400;
  protected JLabel preview = new JLabel();
  
	public TextureEditorPanel(AveryBackground field)
	{
		this.field = field;
		//init();
	}
	
  protected boolean init()
  {
    JPanel scalePanel = new JPanel();
    scalePanel.add(new JLabel("Scale Tile:"));
    scaleGadget = new JSpinner(new SpinnerNumberModel(
    		1.0, 0.2, 4.0, 0.2));
    scaleGadget.setSize(70, 30);
    scaleGadget.setBounds(0,0,70,30);
    scaleGadget.setPreferredSize(new Dimension(70,30));
    scaleGadget.setMaximumSize(new Dimension(70,30));
    scalePanel.add(scaleGadget);
    add(scalePanel);
    
    tileStretchGadget.setSelected(false);
    tileRotateGadget.setSelected(false);
    JPanel checkPanel = new JPanel();
    checkPanel.add(tileStretchGadget);
    checkPanel.add(tileRotateGadget);
    add(checkPanel);
    
    JPanel applyPanel = new JPanel();
    applyButton.setPreferredSize(new Dimension(100,30));
    applyButton.setMaximumSize(new Dimension(100,30));
    applyPanel.add(applyButton);
    add(applyPanel);   
    
    applyButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onApplyButton();
      }
    });
    
    if (!initImageList())
    	return false;
    imagesPanel = new JPanel();
    imagesPanel.add(scrollImageListt);
    //imagesPanel.add(scrollImageListp);
    //scrollImageListp.setVisible(false);
    JPanel imagePanel = new JPanel();
    imagePanel.setMaximumSize(new Dimension(previewWidth, previewHeight));
    imagePanel.setPreferredSize(new Dimension(previewWidth, previewHeight));
    
    generatePreview(null);
    imagePanel.add(preview);
    imagesPanel.add(imagePanel);
    add(imagesPanel);
    
    return true;
  }
  
  boolean loadImageListT()
  {
  	ArrayList mylist = makeImageList("tiles/textures");
  	if (mylist.size() < 1)
  		return false;
  	dlmt = new DefaultListModel();
  	for (int i = 0; i < mylist.size(); i++)
  		dlmt.addElement(mylist.get(i));
  	imageListt.setModel(dlmt);
  	jlwit.makeImageList(mylist);
  	
  	return true;
  }
  
  boolean initImageList()
  {
  	setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP));
  	//ArrayList mylist = makeImageList();
  	//String [] strings = new String[mylist.size()];
  	//for (int i = 0; i < mylist.size(); i++)
  		//strings[i] = (String)mylist.get(i);
  	//imageList = new JList(strings);
  	imageListt = new JList();
  	//imageListp = new JList();
  	jlwit = new JListWithImages();
  	//jlwip = new JListWithImages();
  	//jlwip.setFolder("tiles/patterns/");
  	//ArrayList mylist = makeImageList();
  	//jlwi.makeImageList(make
  	if (!loadImageListT())
  		return false;
  	//loadImageListP();
  	imageListt.setCellRenderer(jlwit);
  	//imageListp.setCellRenderer(jlwip);
  	scrollImageListt = new JScrollPane(imageListt);
  	scrollImageListt.setMaximumSize(new Dimension(imageListButtonSize + 30, (imageListButtonSize * 6)));
   	scrollImageListt.setPreferredSize(new Dimension(imageListButtonSize + 30, (imageListButtonSize * 6)));
  	//scrollImageListp = new JScrollPane(imageListp);
  	//scrollImageListp.setMaximumSize(new Dimension(imageListButtonSize + 30, (imageListButtonSize * 4)));
   	//scrollImageListp.setPreferredSize(new Dimension(imageListButtonSize + 30, (imageListButtonSize * 4)));
   	imageListt.setSelectedIndex(0);
   	//imageListp.setSelectedIndex(0);
 	//imageList.setListData(makeImageList());
  	//imageList.setSelectedIndex(0);
	  //imageList.setSelectedValue(field.getSource(), false);
	  imageListt.addListSelectionListener(new ListSelectionListener()
	  { 
	    public void valueChanged(ListSelectionEvent e)
	    {
	      if (imageListt.isSelectionEmpty() || imageListt.getSelectedValue().equals(field.getSource()))
	      {
	        return;   // nothing to do
	      }
	      
	      //field.setSourceAndGallery((String)imageListt.getSelectedValue(), Config.getDefaultImageGallery());       
	      generatePreview(null);
	      //reAspectFieldToImage(image);
	      invalidate();
	    }
	  });
	  
	  return true;
	  
 }
  
  void onApplyButton()
  {
  	generatePreview(null);
  }
  
	ArrayList makeImageList(String foldername)
  {
  	File folder = new File(foldername);
		File[] listOfFiles = folder.listFiles();
		ArrayList filenames = new ArrayList();
		
		try
		{
			for (int i = 0; i < listOfFiles.length; i++)
			{
				if (listOfFiles[i].isFile())
				{
					if (listOfFiles[i].getName().toLowerCase().endsWith(".png"))
						filenames.add(listOfFiles[i].getName());
			  } 
	  	}
		}
		catch (Exception e)
		{}

    return filenames;
  }

  public void savePreview()
  {
  	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
  	String outName = outfileName.substring(0, outfileName.length() - 4) + timeStamp + ".png";
  	String gallery = Config.getDefaultImageGallery();
  	generatePreview(gallery + "/HighRes/" + outName);
  	field.setSourceAndGallery(outName, gallery);
  }
  
  private void generatePreview(String outputFilename)
  {
  	TexturePatternTileMaker tptm = new TexturePatternTileMaker();
  	
  	AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
  	int panelWidth = (int)(field.getWidth() / 20.0);
  	int panelHeight = (int)(field.getHeight() / 20.0);
  	Point position = field.getPosition();
  	double bleedSize = -position.getX() / 20.0;
  	String shape = master.getShape().getShapeString();
  	if (!shape.equals("rect") && !shape.equals("ellipse"))
  		shape = "rect";
  	double cornerRadius = master.getShape().getCornerRadius() / 20.0;
  	double scale = (double)scaleGadget.getValue();
  	boolean tileStretch = tileStretchGadget.isSelected();
  	boolean tileRotate = tileRotateGadget.isSelected();
  	outfileName = (String)imageListt.getSelectedValue();
  	String filename = "tiles/textures/" + outfileName;
  	
  	tptm.setShape(shape);
  	tptm.setCornerRadius(cornerRadius);
  	tptm.setImageSize(panelWidth, panelHeight);
  	tptm.setBleedSize(bleedSize);
  	tptm.setScale(scale);
  	tptm.setStretch(tileStretch);
  	tptm.setRandomize(tileRotate);
  	
  	BufferedImage bi = tptm.makePattern(filename, outputFilename);
  	ImageIcon icon = new ImageIcon(bi);
  	Image img = icon.getImage();
  	int width = previewWidth;
  	int height = previewHeight;
  	if (panelWidth >= panelHeight)
  	{
  		height = (int)((double)previewWidth * (double)panelHeight / (double)panelWidth);
  	}
  	else
  	{
  		width = (int)((double)previewHeight * (double)panelWidth / (double)panelHeight);  		
  	}
  	
  	Image newimg = img.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);
  	icon = new ImageIcon(newimg);
    preview.setIcon(icon);

  }
  
  static TextureEditorPanel getEditorPanel(AveryBackground field)
  {
  	return new TextureEditorPanel(field);
  }
}
