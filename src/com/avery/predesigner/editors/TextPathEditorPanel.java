package com.avery.predesigner.editors;

import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.*;

//import javax.swing.JTextField;
import com.avery.project.TextPath;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Brad Nelson
 * @version 1.6
 */

class TextPathEditorPanel extends TextFieldEditorPanel
{
	private static final long serialVersionUID = 5924239402595906793L;

	TextPathEditorPanel(com.avery.project.AveryPanelField field)
  {
    this.field = field;
    init();
  }

  /**
   * This is private so that derived classes can have their own versions
   * returning their specific type of AveryPanelField.
   * @return the field itself
   */
  private TextPath getField()
  {
    return (TextPath)this.field;
  }

  private JTextField startangleGadget = new JTextField();

  private JTextField textPathContentGadget = new JTextField();

  protected ButtonGroup rotatesenseGroup = new ButtonGroup();
  protected JRadioButton cwGadget = new JRadioButton("Clockwise");
  protected JRadioButton ccwGadget = new JRadioButton("Counterclockwise");

  /**
   * called by constructor to initialize the editor panel
   */
  protected void init()
  {
    super.init();
    sampleTextPanel.list.setVisibleRowCount(13);

    this.add(new JSeparator());

    // create a 3 column panel to match AveryTextfield layout
    JPanel textPathPanel = new JPanel();
    textPathPanel.setLayout(new GridLayout(0, 3));

    // convert start position to angle in degrees
    double startangle = startPositionToAngle(getField().getStartPosition());

    startangleGadget.setText(Double.toString((startangle)));
    textPathPanel.add(new JLabel("Start Angle"));
    textPathPanel.add(startangleGadget);
    textPathPanel.add(new JPanel());

    // rotate sense
    cwGadget.setActionCommand("cw");
    ccwGadget.setActionCommand("ccw");
    rotatesenseGroup.add(cwGadget);
    rotatesenseGroup.add(ccwGadget);
    cwGadget.setSelected(getField().getRotateSense().equals("cw"));
    ccwGadget.setSelected(getField().getRotateSense().equals("ccw"));
    textPathPanel.add(cwGadget);
    textPathPanel.add(ccwGadget);

    this.add(textPathPanel);
    this.add(new JSeparator());

    textPathContentGadget.setText(getField().getContent());
    this.add(textPathContentGadget);
    
    sampleTextPanel.button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        textPathContentGadget.setText((String)sampleTextPanel.list.getSelectedValue());
      }
    });
    
    // double-clicking the sampleText list is the same as clicking the button
    sampleTextPanel.list.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          textPathContentGadget.setText((String)sampleTextPanel.list.getSelectedValue());
        }
      }
    });
  }

  /**
   * Validates user input and, if everything is fine, updates the field with
   * the values entered by the user
   * @return <code>null</code> if everything's hunky-dorey, or an error
   * message if some input fails validation.
   */
  public String updateField()
  {
    String result = super.updateField();

    if (result == null)   // no error
    {
      getField().setRotateSense(rotatesenseGroup.getSelection().getActionCommand());
      Point startposition = startAngleToPosition(new Double(startangleGadget.getText()));
      getField().setStartPosition(startposition);
      getField().setContent(textPathContentGadget.getText());
    }

    return result;
  }

  /**
   * Sets text line font
   */
  protected void setFieldFont(String font)
  {
    Font fieldFont = new Font(font, Font.PLAIN, 14);
    textPathContentGadget.setFont(fieldFont);
    fieldFont = null;
  }

  /**
   * Converts starting point to angle in degrees
   */
  protected double startPositionToAngle(Point startposition)
  {
    double startx = startposition.getX();
    double starty = startposition.getY();

    // locate quadrant
    int quadrant = 0;
    if (startx > 0.0 && starty >= 0.0)
    {
      quadrant = 0;
    }
    else if (startx <= 0.0 && starty > 0.0)
    {
      quadrant = 1;
      startx = - startx;
    }
    else if (startx < 0.0 && starty <= 0.0)
    {
      quadrant = 2;
      startx = -startx;
      starty = -starty;
    }
    else if (startx >= 0.0 && starty < 0.0)
    {
      quadrant = 3;
      starty = -starty;
    }

    if (startx >= 0.0 && startx < 0.00001)
      startx = .00001;

    double startangle = Math.atan(starty / startx);

    // convert radians to degrees
    startangle *= 57.296;

    // account for quadrant
    if (quadrant == 1)
      startangle = 180 - startangle;
    else if (quadrant == 2)
      startangle = 180 + startangle;
    else if (quadrant == 3)
      startangle = 360 - startangle;

    // now round off to tenths
    startangle = (double)((int)((startangle + 0.05) * 10.0)) / 10.0;

    return startangle;
  }

  /**
   * Converts angle in degrees to starting point
   */
  protected Point startAngleToPosition(Double startAngle)
  {
    // default to 270, high noon on a clock
    // Point p = new Point(0,-90);

    // integer angle in degrees please
    double sa = startAngle.doubleValue();
    // in case they got fancy
    while (sa < 0.0)
      sa += 360.0;
    // boundary check sides
    double width = getField().getWidth().doubleValue();
    double height = getField().getHeight().doubleValue();
    if (width == 0)
    {
      width = 1;
    }
    if (height == 0)
    {
      height = 1;
    }

    // ready to convert the start angle to a point on the ellipse
    /* here are the formulae
    x = r * cos(theta)
    y = r * sin(theta)
    r = width * sqrt((1 - e^2) / (1 - e^2 * cos^2(theta)))
    e = sqrt(1 - width^2/heigth^2) */

    // convert angle to radians
    double theta = sa / 57.296;
    double costheta = Math.cos(theta);
    double sintheta = Math.sin(theta);
    double e2 = 1.0 - width * width / (height * height);
    double r = width * Math.sqrt((1.0 - e2) / (1 - e2 * costheta * costheta));

    Point p = new Point();
    p.setLocation(r*costheta, r*sintheta);
    return p;
  }
  
	protected boolean allowUnderline()
	{
		return false;
	}
}