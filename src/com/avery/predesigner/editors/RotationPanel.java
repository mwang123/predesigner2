package com.avery.predesigner.editors;

import javax.swing.*;

import com.avery.predesigner.tools.Shape;
import com.avery.project.AveryPanelField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridLayout;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: UI for panel field rotation</p>
 * <p>Copyright: Copyright 2016 Avery Products Corporation, all rights reserved</p>
 * <p>Company: Avery Products Corporation</p>
 * @author Bob Lee, Brad Nelson
 * @version 2
 */

public class RotationPanel extends JPanel
{
	private static final long serialVersionUID = -6376752506983252411L;
	private ButtonGroup rotationGroup = new ButtonGroup();
  private JRadioButton gadget0 = new JRadioButton("0 degrees");
  private JRadioButton gadget90 = new JRadioButton("90 degrees");
  private JRadioButton gadget180 = new JRadioButton("180 degrees");
  private JRadioButton gadget270 = new JRadioButton("270 degrees");
  private JTextField gadgetFree = new JTextField();
  
  private Shape shape;
  private AveryPanelField field;
  private double rotation;
  private boolean bLiveUpdate;
  
  public RotationPanel(AveryPanelField field, double rotation, Shape shape, boolean bLiveUpdate)
  {
  	this.field = field;
  	this.rotation = rotation;
  	this.shape = shape;
  	this.bLiveUpdate = bLiveUpdate;
  	
    rotationGroup.add(gadget0);
    rotationGroup.add(gadget90);
    rotationGroup.add(gadget180);
    rotationGroup.add(gadget270);
    
    gadgetFree.setText(Double.toString(rotation));
    
    gadget0.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        gadgetFree.setText("0.0");
        rotate();
      }
    });
    
    gadget90.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        gadgetFree.setText("90.0");
        rotate();
      }
    });
    
    gadget180.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        gadgetFree.setText("180.0");
        rotate();
      }
    });
    
    gadget270.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        gadgetFree.setText("270.0");
        rotate();
      }
    });
    
    setLayout(new GridLayout(4, 3));
    
    add(new JPanel());
    add(gadget0);
    add(new JPanel());
    
    add(gadget90);
    add(new JPanel());
    add(gadget270);
    
    add(new JPanel());
    add(gadget180);
    add(new JPanel());
    
    add(new JPanel());
    add(gadgetFree);
    add(new JPanel());
    
    gadget0.setSelected(rotation == 0.0);
    gadget90.setSelected(rotation == 90.0);
    gadget180.setSelected(rotation == 180.0);
    gadget270.setSelected(rotation == 270.0);
  }
  
  private void rotate()
  {
  	if (!bLiveUpdate)
  		return;
  	
  	shape.applyRotation(field, rotation, Double.parseDouble(getRotation()));
  	rotation = Double.parseDouble(getRotation());
  }
  
  public String getRotation()
  {
    return gadgetFree.getText();
  }
}