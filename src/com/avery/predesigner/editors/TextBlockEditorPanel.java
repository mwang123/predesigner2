package com.avery.predesigner.editors;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.text.JTextComponent;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.StyledDocument;


import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTML.Tag;

import org.faceless.pdf2.PDFStyle;

import com.avery.predesigner.Config;
import com.avery.predesigner.MergeMaps;
import com.avery.predesigner.Predesigner;
import com.avery.project.AveryTextblock;
import com.avery.project.RichText;
import com.avery.project.Text;
import com.avery.project.TextStyle;
import com.avery.project.richtext.RichParagraph;
import com.avery.project.richtext.RichSpan;
import com.avery.utils.MergeMap;

/**
 * <p>Title: Predesigner - the AveryProject Editor</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright 2010 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee, Brad Nelson
 * @version 1.7
 */

class TextBlockEditorPanel extends TextFieldEditorPanel
{
	private static final long serialVersionUID = -8457556325762155679L;
	
	private static Color background = Color.white;

	TextBlockEditorPanel(com.avery.project.AveryPanelField field, boolean isHTML)
  {
  	this.isHTML = isHTML;
  	
    this.field = field;
    init();
  }

  /**
   * This is private so that derived classes can have their own versions
   * returning their specific type of AveryPanelField.
   * @return the field itself
   */
  private AveryTextblock getField()
  {
    return (AveryTextblock)this.field;
  }

  private ButtonGroup overflowGroup = new ButtonGroup();
  private JRadioButton shrinkwrapGadget = new JRadioButton("Shrink-Wrap");
  private JRadioButton wrapshrinkGadget = new JRadioButton("Wrap-Shrink");
  //private JRadioButton shrinkGadget = new JRadioButton("Shrink");
  //private JRadioButton wrapGadget = new JRadioButton("Wrap");
  private JRadioButton fitGadget = new JRadioButton("Fit");
  private JTextArea textArea = new JTextArea();
  private HTMLEditorKit htmlEditor = new HTMLEditorKit();
  private JEditorPane htmlEditPane = new JEditorPane();
  private JComboBox mergeCombo = new JComboBox();
  private JButton addButton = new JButton("Add...");
  private JButton editButton = new JButton("Edit...");
  private JButton deleteButton = new JButton("Delete");
  private JList mergeList;
  private JComboBox lineSpacingGadget = new JComboBox();
  private JComboBox maxLinesGadget = new JComboBox();
  private JCheckBox backgroundGadget = new JCheckBox("Gray Editor");
  private DefaultListModel listModel;
  
  private boolean mergeable = false;
  
  
  public JEditorPane getEditorPane()
  {
  	return htmlEditPane;
  }
  
  public JTextArea getTextAreaPanel()
  {
  	return textArea;
  }
  
  /**
   * called by constructor to initialize the editor panel
   */
  protected void init()
  {
    List lines = getField().getLines();
    if (lines.size() > 0)
    	isHTML = lines.get(0) instanceof RichText;
    
    super.init();
    
    JPanel wrapPanel = new JPanel();
    wrapPanel.setLayout(new GridBagLayout());
     
    JPanel linesPanel = new JPanel();
    linesPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
     
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0;

    c.gridwidth = GridBagConstraints.REMAINDER;     
    wrapPanel.add(new JLabel("Text Wrap Behavior:"), c);
    c.gridwidth = 1;     
    shrinkwrapGadget.setActionCommand("shrinkwrap");
    wrapshrinkGadget.setActionCommand("wrapshrink");
    //shrinkGadget.setActionCommand("shrink");
    //wrapGadget.setActionCommand("wrap");
    fitGadget.setActionCommand("fit");
    overflowGroup.add(shrinkwrapGadget);
    overflowGroup.add(wrapshrinkGadget);
    //overflowGroup.add(shrinkGadget);
    //overflowGroup.add(wrapGadget);
    overflowGroup.add(fitGadget);
    if (!isHTML)
    {
	    shrinkwrapGadget.setSelected(getField().getOverflow().equals("shrinkwrap"));
	    wrapshrinkGadget.setSelected(getField().getOverflow().equals("wrapshrink"));
	    //shrinkGadget.setSelected(getField().getOverflow().equals("shrink"));
	    //wrapGadget.setSelected(getField().getOverflow().equals("wrap"));
	    fitGadget.setSelected(getField().getOverflow().equals("fit"));	    
    }
    
    //fitGadget.setEnabled(false);
    
    /*else
    {
	    shrinkwrapGadget.setSelected(false);
	    wrapshrinkGadget.setSelected(true);
	    //shrinkGadget.setSelected(false);
	    //wrapGadget.setSelected(true);    	
	    //shrinkwrapGadget.setEnabled(false);
	    //wrapshrinkGadget.setEnabled(false);
	    //shrinkGadget.setEnabled(false);
	    //wrapGadget.setEnabled(false);    	
    }*/
    c.gridwidth = 1;
    wrapPanel.add(shrinkwrapGadget,c);
    wrapPanel.add(wrapshrinkGadget,c);
   // wrapPanel.add(shrinkGadget,c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    //wrapPanel.add(wrapGadget,c);
    wrapPanel.add(fitGadget,c);

    this.add(wrapPanel);
    
    for (int ls = 80; ls <= 150; ls += 5)
    {
    	float lsf = (float)ls / 100.0f;
    	String lsfs = String.valueOf(lsf);
    	if (lsfs.length() > 4)
    		lsfs = lsfs.substring(0, 4);
    	lineSpacingGadget.addItem(lsfs);
    }
    double lineSpacing = getField().getLineSpacing().doubleValue();
    int index = ((int)(100.0 * lineSpacing) - 80) / 5;
    lineSpacingGadget.setSelectedIndex(index);
    
    linesPanel.add(new JLabel("Line Spacing: "));
    linesPanel.add(lineSpacingGadget);
    
  	maxLinesGadget.addItem("0");
    for (int ml = 2; ml <= 100; ml++)
    {
    	String mls = String.valueOf(ml);
    	maxLinesGadget.addItem(mls);
    }
    int maxLines = getField().getMaxLines().intValue();
    if (maxLines < 0)
    	maxLines = 0;
    else if (maxLines > 0)
    	maxLines--;
    maxLinesGadget.setSelectedIndex(maxLines);
    
    linesPanel.add(new JLabel("Max Lines: "));
    linesPanel.add(maxLinesGadget);
    
    this.add(linesPanel);
    this.add(new JSeparator());
    
    String mergeMapName = Predesigner.getFrame().getCurrentMasterpanel().getMergeMapName();
    if (mergeMapName != null && mergeMapName.length() > 0)
    {
  		MergeMap map = MergeMaps.getMap(Config.mergeMapFile, mergeMapName, Predesigner.getProject().getLanguage());
  		if (map != null)
  		{
  			mergeable = true;
    		
      	Iterator iterator = map.getLines(Predesigner.getProject().getLanguage()).iterator();
      	while (iterator.hasNext())
      	{
      		mergeCombo.addItem(iterator.next());
      	}
    		mergeCombo.addItem("Non-mergable Text");
        
        // turn off sampleText gadgets until user selects "Non-mergable Text"
        sampleTextPanel.list.setEnabled(false);
        sampleTextPanel.button.setEnabled(false);
        
        mergeCombo.addActionListener( new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            boolean allowSampleText = (mergeCombo.getSelectedIndex() == (mergeCombo.getItemCount() - 1));
            sampleTextPanel.list.setEnabled(allowSampleText);
            sampleTextPanel.button.setEnabled(allowSampleText);
          }
        });
    		
    		JPanel mergePanel = new JPanel();
        mergePanel.setLayout(new GridBagLayout());

        GridBagConstraints c2 = new GridBagConstraints();
        c2.fill = GridBagConstraints.BOTH;
        c2.weightx = 1.0;
        c2.gridwidth = 1;     
        mergePanel.add(new JLabel("MergeMap Line: "), c2);
        mergePanel.add(mergeCombo, c2);
        c2.gridwidth = GridBagConstraints.REMAINDER;
        mergePanel.add(addButton, c2);
      	   	
        addButton.addActionListener( new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            onAddButton(e);
          }
        });
        
        List list = getField().getLines();
        listModel = new DefaultListModel();
        listModel.setSize(list.size());
        for (int i = 0; i < list.size(); ++i)
        {
        	listModel.set(i, list.get(i));
        }
        mergeList = new JList(listModel);
        mergeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  	    
        JScrollPane scroller = new JScrollPane(mergeList);
        scroller.setPreferredSize(new Dimension(420, 80));
        mergePanel.add(scroller, c2);
        
        c2.gridwidth = 1;
        mergePanel.add(editButton, c2);
        mergePanel.add(new JPanel(), c2);
        mergePanel.add(deleteButton, c2);
        
        editButton.addActionListener( new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            onEditButton();
          }
        });
        
        deleteButton.addActionListener( new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            onDeleteButton();
          }
        });

        this.add(mergePanel);
        //JSeparator wideline = new JSeparator();
        this.add(new JSeparator());
  		}
    }

    if (mergeable == false)		// use a JTextArea for direct editing
    {
	    //List lines = getField().getLines();
	    String text = new String();
	    Iterator iterator = lines.iterator();
	    
	    //isRich = lines.get(0) instanceof RichText;
	    while(iterator.hasNext())
	    {
	    	String line = iterator.next().toString();
	      text += line;
	      if (!line.endsWith("\n") && iterator.hasNext())
	      {
	      	text += "\n";
	      }
	    }
	
	    if (!isHTML)
	    {
		    textArea.setText(text);
		    textArea.setRows(lines.size() > 5 ? lines.size() : 5);
		    textArea.setColumns(35);
	    
		    JScrollPane scroller = new JScrollPane(textArea);
		    this.add(scroller);
	    }
	    else
	    {
      	//JEditorPane editor = ((TextBlockEditorPanel)tfep).getEditorPane();
      	
        // Add a caretListener to the editor.
        /*htmlEditPane.addCaretListener(new CaretListener()
        { 
          // Each time the caret is moved, it will trigger the listener and its method caretUpdate. 
          // It will then pass the event to the update method including the source of the event (which is our textarea control) 
          public void caretUpdate(CaretEvent e)
          { 
          	JEditorPane editor = (JEditorPane)e.getSource();
          	selectionStart = editor.getSelectionStart();
          	selectionEnd = editor.getSelectionEnd();
          	System.out.println("caret start, end=" + selectionStart + "," + selectionEnd);
  				}
  			});*/

		    htmlEditPane.setPreferredSize(new Dimension(600, 300));
		    htmlEditPane.setContentType( "text/html" );
		    htmlEditPane.setEditorKit(htmlEditor);
	
		    //this.add(htmlEditPane);
		    //
		    JScrollPane htmlScroller = new JScrollPane(htmlEditPane);
		    //htmlScroller.getViewport().add(htmlEditPane);
		    this.add(htmlScroller);
		    
		    try
		    {
			    Iterator iter = getField().getLines().iterator();
			    if (iter.hasNext())
			    {
			      Object line = iter.next();
			      if (line instanceof RichText)
			      {
			      	RichText content = (RichText)line;
			      	
			        Iterator iterp = content.getParagraphs().iterator();
	
			        // getting out line sizes.
			        String html = ""; //"<html>\r\n<head>\r\n</head>\r\n<body>\r\n";
			        while (iterp.hasNext())
			        {
			          RichParagraph rpar = (RichParagraph) iterp.next();
			          Iterator spans = rpar.getSpans().iterator();
				        //System.out.println(PDFStyle.TEXTALIGN_CENTER);
			          //System.out.println(rpar.getPdfAlign());
			          horzJustify = "left";
			          if (rpar.getPdfAlign() == PDFStyle.TEXTALIGN_CENTER)
			          {
			          	horzJustify = "center";
			          	centerGadget.setSelected(true);
			          }
			          else if (rpar.getPdfAlign() == PDFStyle.TEXTALIGN_RIGHT)
			          {
			          	horzJustify = "right";
			          	rightGadget.setSelected(true);
			          }
			          String p = "<p align=\"" + horzJustify + "\" textAlign=\"left\">\r\n";
			          
			          while (spans.hasNext())
			          {
			            RichSpan span = (RichSpan) spans.next();
			            String s = convertFlex2JavaSpanTag(span);
			            p += s;
			          }
			          p += "</p>\r\n";
			          html += p;
			        }
		          StringReader sr = new StringReader(html);
		          htmlEditor.read(sr, htmlEditPane.getDocument(), 0);	        	
			       /// System.out.println("Java input=\r\n" + html);
			      }
			    }
		    }
		    catch (Exception e) 
		    {}
	    }
    }
    
    // display area background color
    backgroundGadget.setSelected(background == Color.lightGray);
    
    if (isHTML)
			htmlEditPane.setBackground(background);	
    else
    	textArea.setBackground(background);	
		
    this.add(backgroundGadget);
    
    sampleTextPanel.list.setVisibleRowCount(mergeable ? 17 : 16);
    
    sampleTextPanel.button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        appendText((String)sampleTextPanel.list.getSelectedValue());          
      }
    });
    
    // double-clicking the sampleText list is the same as clicking the button
    sampleTextPanel.list.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          appendText((String)sampleTextPanel.list.getSelectedValue());
        }
      }
    });
    
    backgroundGadget.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onBackground();
      }
    });
 }
  
  protected void restoreFocus()
  {
  	htmlEditPane.requestFocus();
  }
  
  private String processSize(String sIn)
  {
  	int index;
  	String out = "";
  	while ((index = sIn.indexOf("size=\"")) > 0)
  	{
  		out = out + sIn.substring(0, index) + "fontSize=\"";
  		int j = 0;
  		sIn = sIn.substring(index + 6);
  		while (Character.isDigit(sIn.charAt(j)))
  		{
  			out += sIn.charAt(j++);
  		}
  		while (j < sIn.length() && sIn.charAt(j) != '"')
  			j++;
  		sIn = sIn.substring(j);
  	}
  	out += sIn;
  	
  	//System.out.println("processSize=" + out);
  	return out;
  }
  
    protected String convertFlex2JavaSpanTag(RichSpan span)
  {
  	String color = TextStyle.formatColor0x(span.getFontColor());
  	color = color.substring(2);
  	
  	String font = span.getFont();
  	
  	int index = font.indexOf(" BoldItalic");
  	if (index > 0)
  	{
  		font = font.substring(0, index);
  	}
  	if (font.lastIndexOf(" Bold") > 0);
  	index = font.indexOf(" Bold");
  	if (index > 0)
  	{
  		font = font.substring(0, index);
  	}
  	index = font.indexOf(" Italic");
  	if (index > 0)
  	{
  		font = font.substring(0, index);
  	}
  	
  	String out = "<span style=\"color: #" + color + "; ";
  	out += "font-size: " + span.getSize() + "pt; ";
  	out += "font-family: " + font + "\">";
  	if (span.isBold())
  		out += "<b>";
  	if (span.isItalic())
  		out += "<i>";
  	if (span.isUnderline())
  		out += "<u>";
  	
  	out += span.getContent();

  	if (span.isBold())
  		out += "</b>";
  	out += "</span>";
  	if (span.isItalic())
  		out += "</i>";
  	if (span.isUnderline())
  		out += "</u>"; 	
  	
  	return out;
  }
  
  protected String convertJava2FlexHTML(String javaHTML)
  {
  	// strip out <body></body>
  	String guts = javaHTML.substring(javaHTML.indexOf("<body>") + 6);
  	guts = guts.substring(0, guts.indexOf("</body>") - 2);
  	
  	// throw away all span and /span tags
		guts = guts.replaceAll("<span>", "");
		guts = guts.replaceAll("</span>", "");
		// replace font with span tags
		guts = guts.replaceAll("<font", "<span");
		guts = guts.replaceAll("</font", "</span");
  	
  	
  	// get rid of redundant style end tags and cr
		guts = guts.replaceAll("</b>", "");
		guts = guts.replaceAll("</i>", "");
		guts = guts.replaceAll("</u>", "");
		guts = guts.replaceAll("\r", "");
  	
   	//System.out.println("guts=\r\n" + guts);
  	
  	String out = "<TextFlow  xmlns=\"http://ns.adobe.com/textLayout/2008\" columnCount=\"inherit\" columnGap=\"inherit\" columnWidth=\"inherit\" lineBreak=\"inherit\" paddingBottom=\"inherit\" paddingLeft=\"inherit\" paddingRight=\"inherit\" paddingTop=\"inherit\" verticalAlign=\"top\" whiteSpaceCollapse=\"preserve\">\r\n";
  	
  	// gather up the p tags
  	String[] pResult = guts.split("</p>");
  	
  	for (int i = 0; i < pResult.length; i++)
  	{
  		if (pResult[i].length() > 2)
  		{
	  		//System.out.println("p=" + pResult[i] + "," + pResult[i].length());
  			
  			out += "<p textAlign=\"" + horzJustify + "\">\r\n";
  			
  			// lose everything past </span>
  			int index = pResult[i].indexOf("</span>");
  			if (index >= 0)
  			{
  				pResult[i] = pResult[i].substring(0, index+6);
  			}
  			
  			// search for styles <b> <i> <u>
  			boolean bold = pResult[i].indexOf("<b>") >= 0;
  			if (bold)
  				pResult[i] = pResult[i].replaceAll("<b>", "");
  			boolean italic = pResult[i].indexOf("<i>") >= 0;
  			if (italic)
  				pResult[i] = pResult[i].replaceAll("<i>", "");
  			boolean underline = pResult[i].indexOf("<u>") >= 0;
  			if (underline)
  				pResult[i] = pResult[i].replaceAll("<u>", "");
  			
	  		if (pResult[i].indexOf("<span") < 0)
	  		{
	  			String tmp = pResult[i];
	  			index = tmp.indexOf(">");
	  			tmp = tmp.substring(0, index) + "<span color=\"#000000\">" + pResult[i].substring(index+1);
  				pResult[i] = tmp + "</span>";
	  		}
  			//System.out.println("pResult[i]=" + pResult[i]);
	  		// only process first span tag, since are doing entire line
	  		// only allowing one span tag per paragraph
	  		String[] spanResult = pResult[i].split("</span");
	  		int j = 0;
  			//System.out.println("sr=" + spanResult[j]);
  			if (spanResult[j].length() > 10)
  			{
  				index = spanResult[j].indexOf("<span");
  				if (index >= 0)
  					out += convertJava2FlexSpanTag(spanResult[j].substring(index), bold, italic, underline);
  			}

	    	out += "\r\n</p>\r\n";
  		}
  	}
  	
  	out += "</TextFlow>\r\n";
		///System.out.println("flex=\r\n" + out);
  	
  	return out;
  }
  
  protected String convertJava2FlexSpanTag(String spanHTML, boolean bold, boolean italic, boolean underline)
  {
  	//System.out.println("spanHTML=\r\n" + spanHTML);
  
  	// replace all Java attribute names with Flex names
  	String out = spanHTML.replaceAll("face=\"", "fontFamily=\"");
  	out = processSize(out);
  	out = out.replaceAll("><font", "");
  	out = out.replaceAll("</font>", "");
  	out = out.replaceAll("/font", "/span><span");
  	String addAttributes = 
  		"fontLookup=\"embeddedCFF\" fontStyle=\"normal\" fontWeight=\"normal\" renderingMode=\"cff\" textDecoration=\"none\" ";
  	
  	if (out.indexOf("fontSize=") < 0)
  		addAttributes = "fontSize=\"12\" " + addAttributes;
  	if (out.indexOf("fontFamily=") < 0)
  		addAttributes = "fontFamily=\"Arial\" " + addAttributes;
  	if (out.indexOf("color=") < 0)
  		addAttributes = "color=\"#000000\" " + addAttributes;
		
  	if (out.indexOf("<span ") == 0)
  		out = out.replace("<span ", "<span " + addAttributes);
  	out += "</span>";
  	//System.out.println("unclean out=\r\n" + out);
  	int start = out.indexOf(">");
  	int end = out.indexOf("</span>");
  	String content = out.substring(start+1, end);
  	String styleStart = "";
  	String styleEnd = "";
  	if (bold)
  	{
  		styleStart += "<b>";
  		styleEnd += "</b>";
  	}
  	if (italic)
  	{
  		styleStart += "<i>";
  		styleEnd += "</i>";
  	}
  	if (underline)
  	{
  		styleStart += "<u>";
  		styleEnd += "</u>";
  	}
  	
  	String span = getSpanPart(out);
  	//System.out.println("span=\r\n" + span);
  	
  	String rets[] = cleanStyleContent(content, span, styleStart, styleEnd);
  	content = rets[0];
  	if (content.length() <= 0)
  		content = " ";
  	if (rets[1] != null)
  	{
  		out = out.replaceFirst(span, rets[1]);
    	start = out.indexOf(">");
  	}
  	//System.out.println("clean content=\r\n" + content);
   	out = out.substring(0, start+1) + content + "</span>";
  	//System.out.println("clean out=\r\n" + out);
  	  	
  	return out;
  }
  
  private String[] cleanStyleContent(String in, String span, String styleStart, String styleEnd)
  {
  	String[] rets = new String[2];
  	
   	String clean = in.trim();
   	clean = clean.replaceAll("\r\n", "");
  	
  	int oldLen = -1;
  	int newLen = 0;
  	while (newLen != oldLen)
  	{
  		oldLen = clean.length();
  		clean = clean.replaceAll("  ", " ");
  		newLen = clean.length();
  	}
  	
  	String cleanCopy = clean;
  	clean = styleStart + clean + styleEnd;
  	
  	//System.out.println("clean content in=\r\n" + clean);
  	//System.out.println("clean span in=\r\n" + span);
  	// convert style tags at beginning of content
  	String newSpan = "";
  	if (clean.indexOf("<b><i><u>") == 0)
  	{
    	newSpan = span.replaceFirst("fontWeight=\"normal\"", "fontWeight=\"bold\"");
    	newSpan = newSpan.replaceFirst("fontStyle=\"normal\"", "fontStyle=\"italic\"");
    	newSpan = newSpan.replaceFirst("textDecoration=\"none\"", "textDecoration=\"underline\"");
    	clean = clean.replaceFirst("<b><i><u>", "");
    	clean = clean.replaceFirst("</b></i></u>", "");
    	rets[0] = cleanCopy;
    	rets[1] = newSpan;
    	
    	return rets;
  	}
  	if (clean.indexOf("<b><i>") == 0)
  	{
    	newSpan = span.replaceFirst("fontWeight=\"normal\"", "fontWeight=\"bold\"");
    	newSpan = newSpan.replaceFirst("fontStyle=\"normal\"", "fontStyle=\"italic\"");
    	clean = clean.replaceFirst("<b><i>", "");
    	clean = clean.replaceFirst("</b></i>", "");
    	rets[0] = cleanCopy;
    	rets[1] = newSpan;
    	
    	return rets;
  	}
  	if (clean.indexOf("<b><u>") == 0)
  	{
    	newSpan = span.replaceFirst("fontWeight=\"normal\"", "fontWeight=\"bold\"");
    	newSpan = newSpan.replaceFirst("textDecoration=\"none\"", "textDecoration=\"underline\"");
    	clean = clean.replaceFirst("<b><u>", "");
    	clean = clean.replaceFirst("</b></u>", "");
    	rets[0] = cleanCopy;
    	rets[1] = newSpan;
    	
    	return rets;
  	}
  	if (clean.indexOf("<i><u>") == 0)
  	{
    	newSpan = span.replaceFirst("fontStyle=\"normal\"", "fontStyle=\"italic\"");
    	newSpan = newSpan.replaceFirst("textDecoration=\"none\"", "textDecoration=\"underline\"");
    	clean = clean.replaceFirst("<i><u>", "");
    	clean = clean.replaceFirst("</i></u>", "");
    	rets[0] = cleanCopy;
    	rets[1] = newSpan;
    	
    	//System.out.println("<i><u> newSpan=\r\n" + newSpan);
    	
    	return rets;
  	}
  	if (clean.indexOf("<b>") == 0)
  	{
    	newSpan = span.replaceFirst("fontWeight=\"normal\"", "fontWeight=\"bold\"");
    	clean = clean.replaceFirst("<b>", "");
    	clean = clean.replaceFirst("</b>", "");
    	rets[0] = cleanCopy;
    	rets[1] = newSpan;
  		//System.out.println("bold is first\r\n" + clean);
  		
  		return rets;
  	}
  	else if (clean.indexOf("<i>") == 0)
  	{
    	newSpan = span.replaceFirst("fontStyle=\"normal\"", "fontStyle=\"italic\"");
    	clean = clean.replaceFirst(span, newSpan);
    	clean = clean.replaceFirst("<i>", "");
    	clean = clean.replaceFirst("</i>", "");
    	rets[0] = cleanCopy;
    	rets[1] = newSpan;
    	
    	return rets;
  	}
  	else if (clean.indexOf("<u>") == 0)
  	{
    	newSpan = span.replaceFirst("textDecoration=\"none\"", "textDecoration=\"underline\"");
    	clean = clean.replaceFirst(span, newSpan);
    	clean = clean.replaceFirst("<u>", "");
    	clean = clean.replaceFirst("</u>", "");
    	rets[0] = cleanCopy;
    	rets[1] = newSpan;
    	
    	return rets;
  	}
  	
  	rets[0] = clean;
  	rets[1] = null;
  	
  	return rets;
  }
  
  private String getSpanPart(String in)
  {
  	String span = "";
  	int start = in.indexOf("<span");
  	int end = in.indexOf(">");
  	span = in.substring(start, end+1);
  	
  	//System.out.println("span=\r\n" + span);
  	return span;
  }
  
  private void appendText(String newText)
  {
    if (mergeable)
    {
      if (mergeCombo.getSelectedIndex() == (mergeCombo.getItemCount()-1))
      {
        insertMergeText(new Text(newText));
      }
      return;
    }

    if (!isHTML)
    {
	    String oldText = textArea.getText();
	    if (oldText == null || oldText.length() == 0)
	    {
	      textArea.setText(newText);
	    }
	    else
	    {
	      textArea.setText(oldText + "\n" + newText);
	    }
    }
    else
    {
    	appendHTML(newText);
    }
  }
  
  private boolean appendHTML(String text)
  {
  	HTMLDocument doc = (HTMLDocument)htmlEditPane.getDocument();
		boolean res = true;
 
		try 
		{
			System.out.println("reading from string reader");
			text = "<p>" + text + "</p>";
			htmlEditor.read(new StringReader(text), doc, doc.getLength());
			System.out.println(htmlEditPane.getText());
		} catch (Exception e)
		{
			System.out.println("error inserting html: " + e);
			res = false;
		}
		return res;
	}

  protected class TextEditorPanel extends JPanel
	{
		private static final long serialVersionUID = 9200789999563628128L;
		private String mergeKey;
    private JTextComponent textComponent;
    private JLabel label;
    
    TextEditorPanel(MergeMap.Line mapLine)
		{
    	mergeKey = mapLine.getName();
    	label = new JLabel(mapLine.toString());
    	textComponent = new JTextField(mapLine.getText());
    	init();
		}
    
    TextEditorPanel(String labelText, String editableText)
		{
    	label = new JLabel(labelText);
    	textComponent = new JTextField(editableText);
    	init();
		}
    
    TextEditorPanel(Text text)
		{
    	this.mergeKey = text.getMergeKey();
    	label = new JLabel(mergeKey);
    	textComponent = new JTextField(text.getContent());
    	init();
		}
 
    
    private void init()
    {
    	GridBagLayout gridbag = new GridBagLayout();
      setLayout(gridbag);
       
      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.BOTH;
      c.weightx = 1.0;
      c.gridwidth = GridBagConstraints.REMAINDER;

      gridbag.setConstraints(add(label), c);
      if (textComponent instanceof JTextArea)
      {
      	gridbag.setConstraints(add(new JScrollPane(textComponent)), c);
      }
      else
      {
      	gridbag.setConstraints(add(textComponent), c);
      }
    }
    
    Text getTextObject()
    {
    	return new Text(textComponent.getText(), mergeKey);
    }
	}
  
  void onAddButton(ActionEvent e)
  {
  	Object selection = mergeCombo.getSelectedItem();
  	TextEditorPanel editorPanel;

 		if (selection instanceof MergeMap.Line)
 		{
 			editorPanel = new TextEditorPanel((MergeMap.Line)selection);
 		}
 		else
 		{
 			editorPanel = new TextEditorPanel("Edit non-mergeable text", "");
 		}
    
    if (JOptionPane.showConfirmDialog(this, editorPanel, "Edit Text",
      JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
      == JOptionPane.OK_OPTION)
    {
      insertMergeText(editorPanel.getTextObject());

    	if (mergeCombo.getSelectedIndex() < mergeCombo.getItemCount() - 2)
    	{
    		// advance mergeCombo to next selection
    		mergeCombo.setSelectedIndex(mergeCombo.getSelectedIndex() + 1);
    	}
    }
  }
  
  private void insertMergeText(Text text)
  {
    int insert = (mergeList.getSelectedIndex() < 0)
    ? listModel.getSize() : mergeList.getSelectedIndex() + 1;

    listModel.setSize(listModel.getSize() + 1);
    listModel.insertElementAt(text, insert);
    mergeList.setSelectedIndex(insert);
    mergeList.ensureIndexIsVisible(insert);
  }
  
  void onEditButton()
  {
  	int index = mergeList.getSelectedIndex();
  	
  	if (index < 0)
  	{
  		JOptionPane.showMessageDialog(
  				this, "Please select a line first!", "No Selection", JOptionPane.ERROR_MESSAGE);
  	}
  	else
  	{
  		Object o = listModel.getElementAt(index);
  		if (o != null && o instanceof Text)
  		{
  			TextEditorPanel editorPanel = new TextEditorPanel((Text)o);
  			
	 	    if (JOptionPane.showConfirmDialog(this, editorPanel, "Edit Text",
	 	       JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
	 	       == JOptionPane.OK_OPTION)
	 	    {
	 	    	listModel.set(index, editorPanel.getTextObject());
	 	    }
	  	}
  	}
  }
  
  void onDeleteButton()
  {
  	if (mergeList.getSelectedIndex() < 0)
  	{
  		JOptionPane.showMessageDialog(
  				this, "Please select a line first!", "No Selection", JOptionPane.ERROR_MESSAGE);
  	}
  	else
  	{
  		listModel.remove(mergeList.getSelectedIndex());
  	}
  }
  
  void onBackground()
  {
		if (backgroundGadget.isSelected())
		{
			background = Color.lightGray;
		}
		else
		{
			background = Color.white;
		}
		
		if (isHTML)
		{
			htmlEditPane.setBackground(background);			
  	}
  	else
  	{
  		textArea.setBackground(background);			 		
  	}  	
  }
  

  /**
   * Validates user input and, if everything is fine, updates the field with
   * the values entered by the user
   * @return <code>null</code> if everything's hunky-dorey, or an error
   * message if some input fails validation.
   */
  public String updateField()
  {
    String result = super.updateField();
    if (result == null)   // no error
    {
    	List list = new ArrayList();  	
    	if (mergeable && !isHTML)
    	{
    		for (int i = 0; i < listModel.size(); ++i)
    		{
    			Object o = listModel.getElementAt(i);
    			if (o != null && o instanceof Text)
    			{
    				list.add(o);
    			}
    		}
    	}
    	else
    	{
    		if (!isHTML)
    		{
		      for (int i = 0; i < textArea.getLineCount(); ++i)
		      {
		        try
		        {
		          int start = textArea.getLineStartOffset(i);
		          int end = textArea.getLineEndOffset(i);
		          int length = end - start;
		          String content = textArea.getText(start, length);
		          if (content.endsWith("\n"))
		            content = content.substring(0, content.length() - 1);
		          list.add(new Text(content));
		        }
		        catch (Exception e)
		        {
		          System.out.println("JTextArea problem: " + e.getMessage());
		          break;
		        }
		      }
    		}
    		else
    		{
    			String content = "";
    			try
    			{
	    			content = htmlEditPane.getText();
		        //System.out.println("Java output=\r\n" + content);
	    			content = this.convertJava2FlexHTML(content);
    			}
    			catch (Exception e)
    			{
    				content = "Java HTML Parse Error";
    			}
    			//System.out.println("\r\nflex content=" + content);
    			RichText richText = new RichText(content);
    			//System.out.println("content=" + richText.getContent());   			
    			list.add(richText);
    		}
    	}
    	
      getField().setLines(list);    
      getField().setOverflow(overflowGroup.getSelection().getActionCommand());
      String lineSpacing = (String)(lineSpacingGadget.getSelectedItem());
      getField().setLineSpacing(Double.valueOf(lineSpacing));
      String maxLines = (String)(maxLinesGadget.getSelectedItem());
      getField().setMaxLines(Integer.valueOf(maxLines));
    }
    return result;
  }

  /**
   * Sets text block font
   */
  protected void setFieldFont(String font)
  {
    Font fieldFont = new Font(font, Font.PLAIN, 14);
    textArea.setFont(fieldFont);
    fieldFont = null;
  }
}