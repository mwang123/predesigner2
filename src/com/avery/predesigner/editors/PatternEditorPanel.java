package com.avery.predesigner.editors;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.avery.predesigner.ColorChooser;
import com.avery.predesigner.Config;
import com.avery.predesigner.Predesigner;
import com.avery.project.AveryBackground;
import com.avery.project.AveryMasterpanel;
import com.borland.jbcl.layout.VerticalFlowLayout;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright 2016 Avery Products Corporation, all rights reserved</p>
 * <p>Company: Avery Products Corporation</p>
 * @author Brad Nelson
 * @version 2.0
 */
public class PatternEditorPanel extends JPanel
{
  protected int imageListButtonSize = 64;
	private static final long serialVersionUID = -1245785865082237240L;
	
	protected  AveryBackground field;
	private String outfileName = "";
	
  
	private Color foreColor = null;
	private Color backColor = null;
  private ColorChooser colorChooser;
  private JPanel colorsPanel;
  private JSpinner scaleGadget;
	private JCheckBox tileStretchGadget = new JCheckBox("Stretch Tile to Fit");
  private JButton foreColorButton = new JButton("Foreground Color...");
  private JButton applyButton = new JButton("Apply");
  private JButton backColorButton = new JButton("Background Color...");
  private JPanel backColorPanel = new JPanel();
  private JPanel foreColorPanel = new JPanel();
  private JList imageListp;
  private JScrollPane scrollImageListp;
  private JPanel imagesPanel = null;
  private JListWithImages jlwip = null;
  private DefaultListModel dlmp = null;
  protected int previewWidth = 400;
  protected int previewHeight = 400;
  protected JLabel preview = new JLabel();

	public PatternEditorPanel(AveryBackground field)
	{
		this.field = field;
    colorChooser = ColorChooser.getColorChooser();
	}
	
  protected boolean init()
  {
    JPanel scalePanel = new JPanel();
    scalePanel.add(new JLabel("Scale Tile:"));
    scaleGadget = new JSpinner(new SpinnerNumberModel(
    		1.0, 0.2, 4.0, 0.2));
    scaleGadget.setSize(70, 30);
    scaleGadget.setBounds(0,0,70,30);
    scaleGadget.setPreferredSize(new Dimension(70,30));
    scaleGadget.setMaximumSize(new Dimension(70,30));
    scalePanel.add(scaleGadget);
    add(scalePanel);
    
    tileStretchGadget.setSelected(false);
    JPanel checkPanel = new JPanel();
    checkPanel.add(tileStretchGadget);
    add(checkPanel);
    
    colorsPanel = new JPanel();
    foreColorPanel.setBackground(Color.black);
    foreColorPanel.add(foreColorButton);
    backColorPanel.setBackground(Color.white);
    backColorPanel.add(backColorButton);
    colorsPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP));
    colorsPanel.add(new JLabel("Black/White Pattern Change Colors"));
    JPanel colorPanel = new JPanel();
    colorPanel.add(foreColorPanel);
    colorPanel.add(backColorPanel);
    colorsPanel.add(colorPanel); 
    add(colorsPanel);
    
    JPanel applyPanel = new JPanel();
    applyButton.setPreferredSize(new Dimension(100,30));
    applyButton.setMaximumSize(new Dimension(100,30));
    applyPanel.add(applyButton);
    add(applyPanel);   
   
    foreColorButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onForeColorButton();
      }
    });
    
    backColorButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onBackColorButton();
      }
    });
    
    applyButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onApplyButton();
      }
    });
    
    if (!initImageList())
    	return false;
    imagesPanel = new JPanel();
    imagesPanel.add(scrollImageListp);
    JPanel imagePanel = new JPanel();
    imagePanel.setMaximumSize(new Dimension(previewWidth, previewHeight));
    imagePanel.setPreferredSize(new Dimension(previewWidth, previewHeight));
    
    generatePreview(null);
    imagePanel.add(preview);
    imagesPanel.add(imagePanel);
    add(imagesPanel);
    
    return true;
  }
  
  boolean loadImageListP()
  {
  	ArrayList mylist = makeImageList("tiles/patterns");
  	if (mylist.size() < 1)
  		return false;
  	dlmp = new DefaultListModel();
  	for (int i = 0; i < mylist.size(); i++)
  		dlmp.addElement(mylist.get(i));
  	imageListp.setModel(dlmp);
  		//strings[i] = (String)mylist.get(i);
  	//imageList.setListData(strings);
  	//imageList.setCellRenderer(jlwip);
  	jlwip.makeImageList(mylist);
  	
  	return true;
  }
  
  boolean initImageList()
  {
  	setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP));
  	imageListp = new JList();
  	jlwip = new JListWithImages();
  	jlwip.setFolder("tiles/patterns/");
  	if (!loadImageListP())
  		return false;
  	imageListp.setCellRenderer(jlwip);
  	scrollImageListp = new JScrollPane(imageListp);
  	scrollImageListp.setMaximumSize(new Dimension(imageListButtonSize + 30, (imageListButtonSize * 6)));
   	scrollImageListp.setPreferredSize(new Dimension(imageListButtonSize + 30, (imageListButtonSize * 6)));
   	imageListp.setSelectedIndex(0);
   	
	  imageListp.addListSelectionListener(new ListSelectionListener()
	  { 
	    public void valueChanged(ListSelectionEvent e)
	    {
	      if (imageListp.isSelectionEmpty() || imageListp.getSelectedValue().equals(field.getSource()))
	      {
	        return;   // nothing to do
	      }
	      
	      generatePreview(null);
	      invalidate();
	    }
	  });
	  return true;
	}
  
  void onApplyButton()
  {
  	generatePreview(null);
  }
  
  void onForeColorButton()
  {
    colorChooser.setColor(foreColor);
    
    JDialog colorDialog = ColorChooser.createDialog(this, "Foreground Color", true, colorChooser, 
      new ActionListener() { public void actionPerformed(ActionEvent e)
      {   // OK listener 
      	foreColor = colorChooser.getColor(); 
        foreColorPanel.setBackground(foreColor);        
      }},
      new ActionListener() { public void actionPerformed(ActionEvent e) {   // Cancel listener
        colorChooser.setColor(foreColor);
      }});
    
    colorDialog.setVisible(true);
    colorDialog.dispose();
  }

  void onBackColorButton()
  {
    colorChooser.setColor(backColor);
    
    JDialog colorDialog = ColorChooser.createDialog(this, "Background Color", true, colorChooser, 
      new ActionListener() { public void actionPerformed(ActionEvent e)
      {   // OK listener 
      	backColor = colorChooser.getColor(); 
        backColorPanel.setBackground(backColor);        
      }},
      new ActionListener() { public void actionPerformed(ActionEvent e) {   // Cancel listener
        colorChooser.setColor(backColor);
      }});
    
    colorDialog.setVisible(true);
    colorDialog.dispose();
  }

  ArrayList makeImageList(String foldername)
  {
  	File folder = new File(foldername);
		File[] listOfFiles = folder.listFiles();
		ArrayList filenames = new ArrayList();
		try
		{
			for (int i = 0; i < listOfFiles.length; i++)
			{
				if (listOfFiles[i].isFile())
				{
					if (listOfFiles[i].getName().toLowerCase().endsWith(".png"))
						filenames.add(listOfFiles[i].getName());
			  } 
	  	}
		}
		catch (Exception e)
		{}
		

    return filenames;
  }

  public void savePreview()
  {
  	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
  	String outName = outfileName.substring(0, outfileName.length() - 4) + timeStamp + ".png";
  	String gallery = Config.getDefaultImageGallery();
  	generatePreview(gallery + "/HighRes/" + outName);
  	field.setSourceAndGallery(outName, gallery);
  }
  
  private void generatePreview(String outputFilename)
  {
  	TexturePatternTileMaker tptm = new TexturePatternTileMaker();
  	
  	AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
  	int panelWidth = (int)(field.getWidth() / 20.0);
  	int panelHeight = (int)(field.getHeight() / 20.0);
  	Point position = field.getPosition();
  	double bleedSize = -position.getX() / 20.0;
  	String shape = master.getShape().getShapeString();
  	if (!shape.equals("rect") && !shape.equals("ellipse"))
  		shape = "rect";
  	double cornerRadius = master.getShape().getCornerRadius() / 20.0;
  	double scale = (double)scaleGadget.getValue();
  	boolean tileStretch = tileStretchGadget.isSelected();
  	outfileName = (String)imageListp.getSelectedValue();
  	String filename = "tiles/patterns/" + outfileName;
  	
  	tptm.setShape(shape);
  	tptm.setCornerRadius(cornerRadius);
  	tptm.setImageSize(panelWidth, panelHeight);
  	tptm.setBleedSize(bleedSize);
  	tptm.setScale(scale);
  	tptm.setStretch(tileStretch);
  	if (foreColor != null || backColor != null)
  	{
  		if (foreColor == null)
  			foreColor = Color.black;
  		if (backColor == null)
  			backColor = Color.white;
  		
  		tptm.setColors(foreColor, backColor);
  	}
  	
  	BufferedImage bi = tptm.makePattern(filename, outputFilename);
  	ImageIcon icon = new ImageIcon(bi);
  	Image img = icon.getImage();
  	int width = previewWidth;
  	int height = previewHeight;
  	if (panelWidth >= panelHeight)
  	{
  		height = (int)((double)previewWidth * (double)panelHeight / (double)panelWidth);
  	}
  	else
  	{
  		width = (int)((double)previewHeight * (double)panelWidth / (double)panelHeight);  		
  	}
  	
  	Image newimg = img.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);
  	icon = new ImageIcon(newimg);
    preview.setIcon(icon);

  }
  
  static PatternEditorPanel getEditorPanel(AveryBackground field)
  {
  	return new PatternEditorPanel(field);
  }

}
