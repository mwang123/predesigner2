package com.avery.predesigner.editors;

import java.awt.Component;
import java.awt.Image;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;


public class JListWithImages extends DefaultListCellRenderer
{
  protected int imageListButtonSize = 64;

	private Map imageMap = null;
	
	private String folder = "tiles/textures/";

  @Override
  public Component getListCellRendererComponent(
          JList list, Object value, int index,
          boolean isSelected, boolean cellHasFocus)
  {

      JLabel label = (JLabel) super.getListCellRendererComponent(
              list, value, index, isSelected, cellHasFocus);
      label.setIcon((ImageIcon)imageMap.get((String) value));
      //label.setHorizontalTextPosition(JLabel.RIGHT);
      label.setText("");
      return label;
  }

  public void setFolder(String folder)
  {
  	this.folder = folder;
  }
  
  public void makeImageList(ArrayList list)
  {
  	imageMap = createImageMap(list);
  }
  
  private Map<String, ImageIcon> createImageMap(ArrayList list)
  {
    Map<String, ImageIcon> map = new HashMap<>();
    for (int i = 0; i < list.size(); i++)
    {
    	String filename = (String)list.get(i);
    	ImageIcon icon = new ImageIcon(folder + filename);
    	Image img = icon.getImage();
    	Image newimg = img.getScaledInstance(imageListButtonSize, imageListButtonSize,  java.awt.Image.SCALE_SMOOTH);
    	icon = new ImageIcon(newimg);
   	
    	map.put(filename, icon);
    }
    return map;
  }
  
}
