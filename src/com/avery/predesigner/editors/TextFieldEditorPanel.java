package com.avery.predesigner.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;

import com.avery.predesigner.ColorChooser;
import com.avery.predesigner.FontCellRenderer;
import com.avery.predesigner.Predesigner;
import com.avery.predesigner.SampleTextPanel;
import com.avery.project.AveryPanelField;
import com.avery.project.AveryTextfield;
import com.avery.project.TextPath;
import com.avery.utils.FontEncoding;
import com.borland.jbcl.layout.VerticalFlowLayout;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Provides the UI for all
 * of the Miwok AveryTextfield derived classes</p>
 * <p>Copyright: Copyright 2016 Avery Products Corporation, all rights reserved</p>
 * <p>Company: Avery Products Corporation</p>
 * @author Bob Lee, Brad Nelson
 * @version 1.7
 */

class TextFieldEditorPanel extends FieldEditorPanel
{
	private static final long serialVersionUID = 3498024438198331743L;
	private JTextField widthGadget = new JTextField();
	private JTextField heightGadget = new JTextField();
	private JTextField positionYGadget = new JTextField();
	private JTextField positionXGadget = new JTextField();
	private JPanel fieldGeometryPanel = new JPanel(new GridLayout(0,2));
  private Color selectedColor = Color.black;
  private ColorChooser colorChooser;
  private TextFieldEditorPanel tfep;
  private TextBlockEditorPanel tbep = null;
  
 // private JList list = null;
  
  protected boolean isHTML = false;
  
	private Action boldAction = new HTMLBoldAction();
	private Action italicAction = new HTMLItalicAction();
	private Action underlineAction = new HTMLUnderlineAction();

	protected int selectionStart;
	protected int selectionEnd;
	
	protected String horzJustify = "center";

	private JCheckBox allDifferentGadget = new JCheckBox("All Different");
	
  protected TextFieldEditorPanel()
  {
    typefaceGadget = new JComboBox(fonts.toArray());
    
    /*Object[] elements = new Object[200];
    Object[] fontNames = fonts.toArray();
    for (int i = 0; i < fontNames.length; i++)
    {
    	Font f = new Font((String)fontNames[i], Font.PLAIN, 10);
    	elements[i] = f;
    }
    typefaceGadget = new JComboBox(elements);*/
    
    //typefaceGadget.getModel().
    //list = new JList(typefaceGadget.getModel());
    //list.setCellRenderer(new FontCellRenderer());
    //typefaceGadget.setModel(list);
    //JOptionPane.showMessageDialog(null, typefaceGadget);
    ListCellRenderer renderer = new FontCellRenderer();
    typefaceGadget.setRenderer(renderer);
    tfep = this;
  }

  TextFieldEditorPanel(AveryPanelField field)
  {
    this.field = field;
    init();
    tfep = this;
  }

	String getDefaultPrompt()
	{
		return "Text";
	}

  /**
   * This is private so that derived classes can have their own versions
   * returning their specific type of AveryPanelField.
   * @return the field itself
   */
  private AveryTextfield getField()
  {
    return (AveryTextfield)this.field;
  }

  /**
   * Override setFieldFont to set the field font.
   * Super version does nothing.
   */
  protected void setFieldFont(String font)
  {
  }

  /**
   * Factory method.  Returns a new TextFieldEditorPanel derived from this class,
   * but customized to match the specific type of AveryTextfield.
   * @param field - the field that the caller wants an editor for
   * @return the appropriate editor
   */
  static FieldEditorPanel getEditorPanel(AveryTextfield field, boolean isHTML)
  {
    if (field instanceof com.avery.project.AveryTextline)
    {
      return new TextLineEditorPanel(field);
    }
    if (field instanceof com.avery.project.AveryTextblock)
    {
      return new TextBlockEditorPanel(field, isHTML);
    }
    if (field instanceof com.avery.project.TextPath)
    {
      return new TextPathEditorPanel(field);
    }
    else  // just the basics
    {
      return new TextFieldEditorPanel(field);
    }
  }

  private JSpinner pointsizeGadget;
  private JCheckBox boldGadget = new JCheckBox();
  private JCheckBox italicGadget = new JCheckBox();
  protected JCheckBox underlineGadget = new JCheckBox();
  
  private JButton boldHTMLGadget = new JButton(boldAction);
  private JButton italicHTMLGadget = new JButton(italicAction);
  private JButton underlineHTMLGadget = new JButton(underlineAction);

  private java.util.List fonts = com.avery.utils.FontEncoding.getTTFFamilies();
  private JComboBox typefaceGadget = null;
  private JPanel colorPanel = new JPanel();
  private JButton colorButton = new JButton("Color...");

  protected ButtonGroup justificationGroup = new ButtonGroup();
  protected JRadioButton leftGadget = new JRadioButton("Left");
  protected JRadioButton centerGadget = new JRadioButton("Center");
  protected JRadioButton rightGadget = new JRadioButton("Right");

  protected ButtonGroup valignGroup = new ButtonGroup();
  protected JRadioButton topGadget = new JRadioButton("Top");
  protected JRadioButton middleGadget = new JRadioButton("Middle");
  protected JRadioButton bottomGadget = new JRadioButton("Bottom");
  
  protected SampleTextPanel sampleTextPanel;
  
  protected JPanel leftPanel;
  
  protected JPanel textPanel = null;
  
  protected void add(JComponent component)
  {
    leftPanel.add(component);
  }

  /**
   * called by constructor to initialize the editor panel
   */
  protected void init()
  {
    //setPromptFont();
    
    this.setLayout(new BorderLayout());

    sampleTextPanel = new SampleTextPanel();
    super.add(sampleTextPanel, BorderLayout.EAST);
    
    leftPanel = new JPanel();
    leftPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP));
    leftPanel.add(createCommonControlsPanel());
    
    leftPanel.add(new JSeparator());

    textPanel = new JPanel();
    GridBagLayout gridbag = new GridBagLayout();
    textPanel.setLayout(gridbag);
     
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0;

    // FONT row
    // grab the full font name
    String theFont = getField().getTypeface();
    // the font name can include the style, need to strip the style off if present
    theFont = getFontFamily(theFont);
    //typefaceGadget = new JComboBox(fonts.toArray());
    typefaceGadget.setSelectedIndex(fonts.indexOf(theFont));
    c.gridwidth = 1;
    gridbag.setConstraints(textPanel.add(new JLabel("Font:")), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(textPanel.add(typefaceGadget),c);

    // its ok to display the normal version of the font in the gadgets
    setFieldFont(theFont);

    // POINT SIZE & COLOR row
    if (!isHTML)
    {
    	double minimumSize = FontEncoding.getMinFontSize(theFont);
    	double maximumSize = getField().getHeight().doubleValue() / 20.0;
    	double currentSize = getField().getPointsize().doubleValue();
    	if (maximumSize <= minimumSize)
    		maximumSize = minimumSize + 4;
    	if (currentSize < minimumSize)
    		currentSize = minimumSize;
      else if (currentSize > maximumSize)
      	currentSize = maximumSize;
   	
    	//minimumSize = Math.min(minimumSize, tdSize);
			pointsizeGadget = new JSpinner(new SpinnerNumberModel(
					currentSize, minimumSize, maximumSize, 0.5));
    }
    else
    {
			pointsizeGadget = new JSpinner(new SpinnerNumberModel(
	    		getField().getPointsize().doubleValue(),
					4.0, getField().getHeight().doubleValue() / 20.0, 1.0));   	
    }
    
    if (tfep instanceof TextBlockEditorPanel)
    {
      tbep = (TextBlockEditorPanel)tfep;
    }
    
    c.gridwidth = 1;
    gridbag.setConstraints(textPanel.add(new JLabel("Point Size:")), c);
    gridbag.setConstraints(textPanel.add(pointsizeGadget),c);
    gridbag.setConstraints(textPanel.add(colorPanel), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(textPanel.add(colorButton), c);
    
    colorButton.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onColorButton();
      }
    });
    
    selectedColor = getField().getTextcolor();
    colorPanel.setBackground(selectedColor);
    colorChooser = ColorChooser.getColorChooser();
    colorChooser.setColor(selectedColor);
    
    if (isHTML)
    {
	    
	    pointsizeGadget.addChangeListener(new ChangeListener(){
	      public void stateChanged(ChangeEvent e) {
	        onSizeSpinner();
	      }
	    });
	    
	    leftGadget.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        onLeftRadioButton();
	      }
	    });
	    
	    centerGadget.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        onCenterRadioButton();
	      }
	    });
	    
	    rightGadget.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        onRightRadioButton();
	      }
	    });
    }   
    else
    {
			if (tbep != null)
			{
				tbep.getTextAreaPanel().setForeground(selectedColor);
			}
    }

    // ATTRIBUTE row
    enableStyles(theFont);

    boldGadget.setText("Bold");
    boldGadget.setSelected(getField().isBold());
    italicGadget.setText("Italic");
    italicGadget.setSelected(getField().isItalic());
    underlineGadget.setText("Underline");
    
    
    if (allowUnderline())
    {
    	underlineGadget.setSelected(getField().isUnderline());
    }
    else
    {
    	underlineGadget.setEnabled(false);
    }
    
    boldHTMLGadget.setText("Bold");
    italicHTMLGadget.setText("Italic");
    underlineHTMLGadget.setText("Underline");
    
    c.gridwidth = 1;
    gridbag.setConstraints(textPanel.add(new JLabel("Attribute:")), c);
    if (!isHTML)
    {
	    gridbag.setConstraints(textPanel.add(boldGadget),c);
	    gridbag.setConstraints(textPanel.add(italicGadget),c);
	    c.gridwidth = GridBagConstraints.REMAINDER;
	    gridbag.setConstraints(textPanel.add(underlineGadget),c);
    }
    else
    {
    	gridbag.setConstraints(textPanel.add(boldHTMLGadget),c);
    	gridbag.setConstraints(textPanel.add(italicHTMLGadget),c);
	    c.gridwidth = GridBagConstraints.REMAINDER;
    	gridbag.setConstraints(textPanel.add(underlineHTMLGadget),c);
    }
    
    // HORIZONTAL ALIGNMENT row
    leftGadget.setActionCommand("left");
    centerGadget.setActionCommand("center");
    rightGadget.setActionCommand("right");
    justificationGroup.add(leftGadget);
    justificationGroup.add(centerGadget);
    justificationGroup.add(rightGadget);
    leftGadget.setSelected(getField().getJustification().equals("left"));
    centerGadget.setSelected(getField().getJustification().equals("center"));
    rightGadget.setSelected(getField().getJustification().equals("right"));
    c.gridwidth = 1;
    gridbag.setConstraints(textPanel.add(new JLabel("Justification:")), c);
    gridbag.setConstraints(textPanel.add(leftGadget),c);
    gridbag.setConstraints(textPanel.add(centerGadget),c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(textPanel.add(rightGadget),c);

    // VERTICAL ALIGNMENT row
    topGadget.setActionCommand("top");
    middleGadget.setActionCommand("middle");
    bottomGadget.setActionCommand("bottom");
    valignGroup.add(topGadget);
    valignGroup.add(middleGadget);
    valignGroup.add(bottomGadget);
    if (!isHTML)
    {
	    topGadget.setSelected(getField().getVerticalAlignment().equals("top"));
	    middleGadget.setSelected(getField().getVerticalAlignment().equals("middle"));
	    bottomGadget.setSelected(getField().getVerticalAlignment().equals("bottom"));
    }
    else
    {
	    topGadget.setSelected(true);
	    middleGadget.setSelected(false);
	    bottomGadget.setSelected(false);    	
	    topGadget.setEnabled(false);
	    middleGadget.setEnabled(false);
	    bottomGadget.setEnabled(false);    	
    }
    if (!(getField() instanceof TextPath))
    {
      textPanel.add(topGadget);
      textPanel.add(middleGadget);
      textPanel.add(bottomGadget);
      c.gridwidth = 1;
      gridbag.setConstraints(textPanel.add(new JLabel("Alignment:")), c);
      gridbag.setConstraints(textPanel.add(topGadget),c);
      gridbag.setConstraints(textPanel.add(middleGadget),c);
      c.gridwidth = GridBagConstraints.REMAINDER;
      gridbag.setConstraints(textPanel.add(bottomGadget),c);
      allDifferentGadget.setSelected(getField().isAllDifferent()); 
      textPanel.add(allDifferentGadget);
    }

    if (isHTML)
    {
    	JEditorPane editor = ((TextBlockEditorPanel)tfep).getEditorPane();
    	
      // Add a caretListener to the editor.
      editor.addCaretListener(new CaretListener()
      { 
        // Each time the caret is moved, it will trigger the listener and its method caretUpdate. 
        // It will then pass the event to the update method including the source of the event (which is our textarea control) 
        public void caretUpdate(CaretEvent e)
        { 
        	JEditorPane editor = (JEditorPane)e.getSource();
        	selectionStart = editor.getSelectionStart();
					//JTextArea editArea = (JTextArea)e.getSource(); 
				}
			});
		}	
    
    leftPanel.add(textPanel);
    super.add(leftPanel, BorderLayout.WEST);

    typefaceGadget.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent event)
      {
        if (typefaceGadget.getSelectedIndex() >= 0)
        {
        	
        	// preserve the old font name
        	String oldFont = getField().getTypeface();
          String fontFamily = (String)typefaceGadget.getSelectedItem();
          // unfortunately must first set the AveryTextfield font to family name
          // to force the AveryTextfield encoding to be correctly updated
          getField().setTypeface(fontFamily);
          // now can fixup the font name (only needed for 8859_1 fonts but
          // getFontFull does not change name for other font encodings)
          String font = getFontFull(fontFamily);
          //System.out.println(fontFamily + "," + font);
          // finally the full font name for 8859_1 fonts can be saved
          getField().setTypeface(font);
          // if new font is UTF8 encoding update prompt font accordingly
          //setPromptFont();
          // update field font
          setFieldFont(fontFamily);
          
          enableStyles(fontFamily);
        	
          double minimumSize = FontEncoding.getMinFontSize(fontFamily);
          SpinnerNumberModel smn = (SpinnerNumberModel)pointsizeGadget.getModel();
          smn.setMinimum(minimumSize);
          double currentFontSize = ((Double)pointsizeGadget.getValue()).doubleValue();
          if (currentFontSize < minimumSize)
          	pointsizeGadget.setValue(new Double(minimumSize));
          	
          // necessary in case user cancels font change
          getField().setTypeface(oldFont);
          
          // rich text
          if (isHTML)
          {
  					SimpleAttributeSet sas = new SimpleAttributeSet();
  					sas.addAttribute(StyleConstants.FontFamily, fontFamily);
  					changeHTMLAttribute(sas);
          }
        }
      }
    });
  }

  void enableStyles(String fontFamily)
  {
  	boldGadget.setEnabled(false);
  	italicGadget.setEnabled(false);
    List fontStyles = com.avery.utils.FontEncoding.getJavaFontStyles(fontFamily);
    Iterator iter = fontStyles.iterator();
    while (iter.hasNext())
    {
    	String fontName = (String)iter.next();
    	if (fontName.indexOf("Bold") > 0)
      	boldGadget.setEnabled(true);
    	if (fontName.indexOf("Italic") > 0)
    		italicGadget.setEnabled(true);	
    }
    
    if (!boldGadget.isEnabled())
    	boldGadget.setSelected(false);
    if (!italicGadget.isEnabled())
    	italicGadget.setSelected(false);
    	
  }
  
  /**
   * Validates user input and, if everything is fine, updates the field with
   * the values entered by the user
   * @return <code>null</code> if everything's hunky-dorey, or an error
   * message if some input fails validation.
   */
  public String updateField()
  {
    String result = super.updateField();

    if (result == null && !isHTML)   // no error
    {
      getField().setPointsize((Double)pointsizeGadget.getValue());
      getField().setBold(boldGadget.isSelected());
      getField().setItalic(italicGadget.isSelected());
      getField().setUnderline(underlineGadget.isSelected());
      getField().setTextcolor(selectedColor);
      getField().setTypeface(getFontFull((String)typefaceGadget.getSelectedItem()));
      getField().setJustification(justificationGroup.getSelection().getActionCommand());
    }
    getField().setVerticalAlignment(valignGroup.getSelection().getActionCommand());
    getField().setAllDifferent(allDifferentGadget.isSelected());
    return result;
  }

  /**
   * Check the values of all of the fields.  Derived classes should override
   * this, and start by calling the super method.  Note that this will be called
   * automatically by the base class in {@link FieldEditorPanel#updateField} -
   * you should not need to call it directly.
   * @return <code>null</code> if everything's hunky-dorey, or an error
   * message if some input fails validation.
   */
  protected String validateUserInput()
  {
    String result = super.validateUserInput();
    if (result == null)
    {
      String controlName = " ";
      try
      {
        controlName = "Pointsize ";
        double pointsize = ((Double)pointsizeGadget.getValue()).doubleValue();
        if (pointsize < 2.0)
        {
          throw new Exception("is too small");
        }
      }
      catch (Exception e)
      {
        // something failed
        result = controlName + e.getMessage();
      }
    }

    return result;
  }

  // strips style strings from font names if found
  // strips bold or italic or bolditalic or even italicbold
  private String getFontFamily(String font)
  {
    String theFont = font;
    String lowFont = font.toLowerCase();
    int pos = lowFont.indexOf(" bold");
    if (pos < 2)
      pos = lowFont.indexOf(" italic");

    if (pos >= 2)
    {
      theFont = theFont.substring(0, pos);
    }
    return theFont;
  }

  // creates the full font name from the family name
  // only for 8859_1 encoded fonts
  // this makes the stark assumption that all 8859_1
  // fonts will always consist of 4 font files with names of form
  // Family, Family Bold, Family Italic, Family BoldItalic
  private String getFontFull(String font)
  {
  	//System.out.println("font=" + font);
  	String theFont = font;
    AveryTextfield atf = (AveryTextfield)field;
    if (atf.getEncoding().equals("8859_1"))
    {
      String style = "";
      if (boldGadget.isSelected())
        style = "Bold";
      if (italicGadget.isSelected())
        style += "Italic";
      if (style.length() > 0)
        theFont += " " + style;
    }
    String checkFontExists = com.avery.utils.FontEncoding.getJavaFontName(theFont);
  	//System.out.println("checkFontExists=" + checkFontExists);
    if (checkFontExists.length() < 1)
    {
    	theFont = font;
    }
  	//System.out.println("theFont=" + theFont);
    return theFont;
  }
  
	protected JPanel getFieldGeometryPanel()
	{
    // setup fieldGeometryPanel before calling super.init()
    fieldGeometryPanel.add(new JLabel(" Width"));
    widthGadget.setText(Predesigner.twipsToUnitString(field.getWidth().doubleValue()));
    fieldGeometryPanel.add(widthGadget);

    fieldGeometryPanel.add(new JLabel(" Height"));
    heightGadget.setText(Predesigner.twipsToUnitString(field.getHeight().doubleValue()));
    fieldGeometryPanel.add(heightGadget);

    fieldGeometryPanel.add(new JLabel(" Position X"));
    positionXGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getX()));
    fieldGeometryPanel.add(positionXGadget);

    fieldGeometryPanel.add(new JLabel(" Position Y"));
    positionYGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getY()));
    fieldGeometryPanel.add(positionYGadget);
		return fieldGeometryPanel;
	}
	
	protected void addFieldGeometryGadgets(JPanel panel, GridBagLayout gridbag)
	{
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridwidth = 1;
    
    gridbag.setConstraints(panel.add(new JLabel("Width:")), c);
    widthGadget.setText(Predesigner.twipsToUnitString(field.getWidth().doubleValue()));
    gridbag.setConstraints(panel.add(widthGadget), c);
    
    gridbag.setConstraints(panel.add(new JLabel(" ")), c);

    gridbag.setConstraints(panel.add(new JLabel("Height:")), c);
    heightGadget.setText(Predesigner.twipsToUnitString(field.getHeight().doubleValue()));
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(heightGadget), c);

    c.gridwidth = 1;
    gridbag.setConstraints(panel.add(new JLabel("Position X:")), c);
    positionXGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getX()));
    gridbag.setConstraints(panel.add(positionXGadget), c);
    gridbag.setConstraints(panel.add(new JLabel(" ")), c);

    gridbag.setConstraints(panel.add(new JLabel("Position Y:")), c);
    positionYGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getY()));
    c.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(panel.add(positionYGadget), c);
	}
	
	protected void updatePositionGadget(Point2D newPosition)
	{
    positionXGadget.setText(Predesigner.twipsToUnitString(newPosition.getX()));
    positionYGadget.setText(Predesigner.twipsToUnitString(newPosition.getY()));
	}
	
	protected Point getNewPosition()
	{
    double x = getField().getPosition().getX();
    double y = getField().getPosition().getY();
    if (!Predesigner.twipsToUnitString(x).equals(positionXGadget.getText()))
    {
      x = Predesigner.unitStringToTwips(positionXGadget.getText()).doubleValue();
    }
    if (!Predesigner.twipsToUnitString(y).equals(positionYGadget.getText()))
    {
      y = Predesigner.unitStringToTwips(positionYGadget.getText()).doubleValue();
    }
    Point point = new Point();
		point.setLocation(x, y);		
		return point;
	}
	
	protected Double getNewWidth()
	{
    String widthString = Predesigner.twipsToUnitString(getField().getWidth().doubleValue());
    if (widthString.equals(widthGadget.getText()))
    {
      return getField().getWidth();
    }
    else return Predesigner.unitStringToTwips(widthGadget.getText());
	}
	
	protected Double getNewHeight()
	{
    String heightString = Predesigner.twipsToUnitString(getField().getHeight().doubleValue());
    if (heightString.equals(heightGadget.getText()))
    {
      return getField().getHeight();
    }
    else return Predesigner.unitStringToTwips(heightGadget.getText());
	}
	
	protected boolean allowUnderline()
	{
		return true;
	}
 
	protected void setPromptFont()
  {
    // if font is UTF8 display prompt using field font
    AveryTextfield atf = (AveryTextfield)field;
    String encoding = atf.getEncoding();
    if (encoding.equals("UTF8"))
    {
      Font promptFont = new Font(atf.getTypeface(), Font.PLAIN, 14);
      descriptionGadget.setFont(promptFont);
      promptFont = null;
    }
  }
  
  void onColorButton()
  {
  	JDialog colorDialog = null;
  	
  	if (isHTML)
  	{
    	colorDialog = ColorChooser.createDialog(this, "Text Color", true, colorChooser,
   				new ActionListener() { public void actionPerformed(ActionEvent e) {   // OK listener 
  					selectedColor = colorChooser.getColor(); 
  					colorPanel.setBackground(selectedColor);
  					
  					SimpleAttributeSet sas = new SimpleAttributeSet();
  					sas.addAttribute(StyleConstants.ColorConstants.Foreground, selectedColor);
  					changeHTMLAttribute(sas);
        }},
      new ActionListener() { public void actionPerformed(ActionEvent e) {   // Cancel listener
        colorChooser.setColor(selectedColor);
      }});
    }
  	else
  	{
  		colorDialog = ColorChooser.createDialog(this, "Text Color", true, colorChooser, 
  				new ActionListener() { public void actionPerformed(ActionEvent e) {   // OK listener 
  					selectedColor = colorChooser.getColor(); 
  					colorPanel.setBackground(selectedColor);
  					if (tbep != null)
  					{
  						tbep.getTextAreaPanel().setForeground(selectedColor);
  					}
        }},
      new ActionListener() { public void actionPerformed(ActionEvent e) {   // Cancel listener
        colorChooser.setColor(selectedColor);
      }});
  		
  	}
    
    colorDialog.setVisible(true);
    colorDialog.dispose();

  }
  
  void onSizeSpinner()
  {
    float fontSize = Float.parseFloat(pointsizeGadget.getValue().toString());
    //System.out.println((int)fontSize);
		SimpleAttributeSet sas = new SimpleAttributeSet();
		sas.addAttribute(StyleConstants.FontSize, String.valueOf((int)fontSize) + "pt");
		changeHTMLAttribute(sas);
  }
   
  void onLeftRadioButton()
  {
  	//System.out.println("clicked left");
  	horzJustify = "left";
		SimpleAttributeSet sas = new SimpleAttributeSet();
		sas.addAttribute(StyleConstants.Alignment, new Integer(StyleConstants.ALIGN_LEFT));
		changeHTMLParagraphAttribute(sas);
  }
  
  void onCenterRadioButton()
  {
  	//System.out.println("clicked center");
  	horzJustify = "center";
		SimpleAttributeSet sas = new SimpleAttributeSet();
		sas.addAttribute(StyleConstants.Alignment, new Integer(StyleConstants.ALIGN_CENTER));
		changeHTMLParagraphAttribute(sas);
  }
  
  void onRightRadioButton()
  {
  	//System.out.println("clicked right");
  	horzJustify = "right";
		SimpleAttributeSet sas = new SimpleAttributeSet();
		sas.addAttribute(StyleConstants.Alignment, new Integer(StyleConstants.ALIGN_RIGHT));
		changeHTMLParagraphAttribute(sas);
  }
  
  private void changeHTMLAttribute(SimpleAttributeSet sas)
  {
  	selectLine();
    JEditorPane editor = tbep.getEditorPane();
		StyledDocument doc = (StyledDocument)editor.getDocument();			
		int posStart = editor.getSelectionStart();
		int posEnd =  editor.getSelectionEnd();
		int selLength = posEnd - posStart;      
  	//System.out.println("set start, end=" + posStart + "," + posEnd);
		doc.setCharacterAttributes(posStart, selLength, sas, false);
		
		tbep.restoreFocus();
  }
  
  private void changeHTMLParagraphAttribute(SimpleAttributeSet sas)
  {	
    JEditorPane editor = tbep.getEditorPane();
		StyledDocument doc = (StyledDocument)editor.getDocument();			
		int posStart = 0; //editor.getSelectionStart();
		int posEnd =  editor.getText().length(); //getSelectionEnd();
		int selLength = posEnd - posStart;      
  	//System.out.println("set start, end=" + posStart + "," + posEnd);
		doc.setParagraphAttributes(posStart, selLength, sas, false);
		
		tbep.restoreFocus(); 	
  }
  
  private void selectLine()
  {
    JEditorPane editor = tbep.getEditorPane();
		StyledDocument doc = (StyledDocument)editor.getDocument();			
		int posStart = editor.getSelectionStart();
		int posEnd = editor.getSelectionEnd();
  	//System.out.println("selectLine start, end=" + posStart + "," + posEnd);
	
		try
		{
			int textLength = doc.getLength();
			String text = doc.getText(0, textLength);
			//System.out.println("text=" + text + "," + text.length());
			//System.out.println("start, end=" + posStart + "," + posEnd);
			// find start of line
			while (posStart > 0 && text.charAt(posStart) != '\n')
			{
				posStart--;
		  	//System.out.println("selectLine decrement start=" + posStart);
			}
			// find end of line
			while (posEnd < textLength && text.charAt(posEnd) != '\n')
			{
				posEnd++;
		  	//System.out.println("selectLine increment end=" + posEnd);
			}
			editor.setSelectionStart(posStart);
			editor.setSelectionEnd(posEnd);
		}
		catch (Exception e){}
  }
 
  class HTMLBoldAction extends StyledEditorKit.BoldAction
  {
		public HTMLBoldAction()
		{
			super();
		}
		
		public void actionPerformed(ActionEvent ae)
		{
			selectLine();
			super.actionPerformed(ae);
			//System.out.println("bold performed");
			tbep.restoreFocus();
		} 	
  }
  
  class HTMLItalicAction extends StyledEditorKit.ItalicAction
  {
		public HTMLItalicAction()
		{
			super();
		}
		
		public void actionPerformed(ActionEvent ae)
		{
			selectLine();
			super.actionPerformed(ae);
			//System.out.println("italic performed");
			tbep.restoreFocus();
		} 	
  }
  
  class HTMLUnderlineAction extends StyledEditorKit.UnderlineAction
  {
		public HTMLUnderlineAction()
		{
			super();
		}
		
		public void actionPerformed(ActionEvent ae)
		{
			selectLine();
			super.actionPerformed(ae);
			//System.out.println("underline performed");
			tbep.restoreFocus();
		} 	
  }
    
}

