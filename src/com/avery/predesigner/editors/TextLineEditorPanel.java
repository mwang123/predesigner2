package com.avery.predesigner.editors;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import com.avery.predesigner.Config;
import com.avery.predesigner.MergeMaps;
import com.avery.predesigner.Predesigner;
import com.avery.project.AveryTextline;
import com.avery.utils.MergeMap;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 1.6
 */

class TextLineEditorPanel extends TextFieldEditorPanel
{
	private static final long serialVersionUID = -2728963845564936112L;

	TextLineEditorPanel(com.avery.project.AveryPanelField field)
  {
    this.field = field;
    init();
  }

  /**
   * This is private so that derived classes can have their own versions
   * returning their specific type of AveryPanelField.
   * @return the field itself
   */
  private AveryTextline getField()
  {
    return (AveryTextline)this.field;
  }

  private JTextField textlineContentGadget = new JTextField();
  private JComboBox mergeFieldGadget = new JComboBox();
  
  private MergeMap map;
  private Object lastSelection;
  
  /**
   * called by constructor to initialize the editor panel
   */
  protected void init()
  {
    super.init();
    
    this.add(new JSeparator());

    String mergeMapName = Predesigner.getFrame().getCurrentMasterpanel().getMergeMapName();
    if (mergeMapName != null && mergeMapName.length() > 0)
    {
  		map = MergeMaps.getMap(Config.mergeMapFile, mergeMapName, Predesigner.getProject().getLanguage());
    }
    
  	if (map != null)
  	{
  		mergeFieldGadget.addItem("non-mergable line");
  		if (getField().getMergeKey().equals(""))
  		{
  			mergeFieldGadget.setSelectedIndex(0);
  		}
  		
    	Iterator iterator = map.getLines(Predesigner.getProject().getLanguage()).iterator();
    	while (iterator.hasNext())
    	{
    		MergeMap.Line line = (MergeMap.Line)iterator.next();
    		mergeFieldGadget.addItem(line);
    		if (line.getName().equals(getField().getMergeKey()))
    		{
    			mergeFieldGadget.setSelectedItem(line);
    		}
    	}
    	
    	this.add(new JLabel("MergeMap Field Line"));
    	this.add(mergeFieldGadget);
    	
    	lastSelection = mergeFieldGadget.getSelectedItem();
    	
      mergeFieldGadget.addActionListener( new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          onMergeFieldChanged(e);
        }
      });
  	}

   	textlineContentGadget.setText(getField().getContent());
    this.add(textlineContentGadget);
    
    sampleTextPanel.button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        textlineContentGadget.setText((String)sampleTextPanel.list.getSelectedValue());
      }
    });
    
    // double-clicking the sampleText list is the same as clicking the button
    sampleTextPanel.list.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        if (sampleTextPanel.list.isEnabled() && e.getClickCount() == 2)
            textlineContentGadget.setText((String)sampleTextPanel.list.getSelectedValue());
      }
    });
    
    sampleTextPanel.list.setVisibleRowCount((map == null) ? 9 : 12);
  }
  
  private void onMergeFieldChanged(ActionEvent e)
  {
  	Object selection = mergeFieldGadget.getSelectedItem();
  	if (!selection.equals(lastSelection))
  	{
  		if (selection instanceof MergeMap.Line)
  		{
  			textlineContentGadget.setText(((MergeMap.Line)selection).getText());
  		}
  		
  		lastSelection = selection;
  	}
    
  	// turn off sampleText gadgets unless user has selected "non-mergable line"
    boolean allowSampleText = (mergeFieldGadget.getSelectedIndex() == 0);
    sampleTextPanel.list.setEnabled(allowSampleText);
    sampleTextPanel.button.setEnabled(allowSampleText);
  }

  /**
   * Validates user input and, if everything is fine, updates the field with
   * the values entered by the user
   * @return <code>null</code> if everything's hunky-dorey, or an error
   * message if some input fails validation.
   */
  public String updateField()
  {
    String result = super.updateField();

    if (result == null)   // no error
    {
      getField().setContent(textlineContentGadget.getText());
    }
    
    if (mergeFieldGadget.getSelectedIndex() > 0)
    {
    	MergeMap.Line selection = (MergeMap.Line)mergeFieldGadget.getSelectedItem();
    	getField().setMergeKey(selection.getName());
    }
    else
    {
    	getField().setMergeKey(null);
    }

    return result;
  }

  /**
   * Sets text line font
   */
  protected void setFieldFont(String font)
  {
    Font fieldFont = new Font(font, Font.PLAIN, 14);
    textlineContentGadget.setFont(fieldFont);
    fieldFont = null;
  }
  
}