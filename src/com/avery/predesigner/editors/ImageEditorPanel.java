package com.avery.predesigner.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.avery.predesign.StyleSet;
import com.avery.predesigner.Config;
import com.avery.predesigner.ImageAccessory;
import com.avery.predesigner.ImageFileFilter;
import com.avery.predesigner.ImageFileList;
import com.avery.predesigner.Predesigner;
import com.avery.project.AveryBackground;
import com.avery.project.AveryImage;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;
import com.avery.project.Drawing;
import com.avery.project.RightRes;
import com.borland.jbcl.layout.VerticalFlowLayout;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright 2016 Avery Products Corporation, all rights reserved</p>
 * <p>Company: Avery Products Corporation</p>
 * @author Bob Lee
 * @version 1.6
 */

class ImageEditorPanel extends FieldEditorPanel
{
	private static final long serialVersionUID = -1245785865082237240L;
	private JTextField widthGadget = new JTextField();
	private JTextField heightGadget = new JTextField();
	private JTextField positionYGadget = new JTextField();
	private JTextField positionXGadget = new JTextField();
	
  protected ImageEditorPanel()
  {
  }

  ImageEditorPanel(AveryPanelField field)
  {
    this.field = field;
    init();
  }

  /**
   * This is private so that derived classes can have their own versions
   * returning their specific type of AveryPanelField.
   * @return the field itself
   */
  private AveryImage getField()
  {
    return (AveryImage)this.field;
  }

  protected int imageListButtonSize = 64;
  protected int previewWidth = 320;
  protected int previewHeight = 320;
  protected JLabel preview = new JLabel();
  protected JButton browseButton = new JButton("Browse...");
  private ImageFileList imageList;
  private JPanel imageClipMaskPanel = new JPanel();
  private JButton imageClipButton = new JButton("Clip Shape");
  private JButton imageMaskButton = new JButton("Mask Shape");

  /**
   * called by constructor to initialize the editor panel
   */
  protected void init()
  {
    this.setLayout(new BorderLayout(4,0));
    
    imageList = new ImageFileList(makeImageList(Predesigner.styleSet), imageListButtonSize);
    imageList.setSelectedValue(getField().getSource(), false);
    imageList.addListSelectionListener(new ListSelectionListener()
    { 
      public void valueChanged(ListSelectionEvent e)
      {
        if (imageList.isSelectionEmpty() || imageList.getSelectedValue().equals(getField().getSource()))
        {
          return;   // nothing to do
        }
        
        getField().setSourceAndGallery((String)imageList.getSelectedValue(), Config.getDefaultImageGallery());       
        Image image = generatePreview();
        reAspectFieldToImage(image);
        invalidate();
      }
    });
    this.add(imageList, BorderLayout.EAST);
    
    JPanel leftPanel = new JPanel();
    leftPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP));
    leftPanel.add(createCommonControlsPanel());
    
    leftPanel.add(new JSeparator());

    generatePreview();
    leftPanel.add(preview);

    browseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onBrowseButton(e);
      }
    });

    leftPanel.add(browseButton);
    
    //imageClipMaskPanel.add(imageClipButton);
    //imageClipMaskPanel.add(imageMaskButton);
    
    this.add(leftPanel, BorderLayout.WEST);
    this.add(imageClipMaskPanel, BorderLayout.SOUTH);
    
    imageClipButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onClipMaskButton(e, "c");
      }
    });
    
    imageMaskButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onClipMaskButton(e, "m");
      }
    });
  }
  
	String getDefaultPrompt()
	{
		return (this.field instanceof AveryBackground) ? "Background" : "Picture";
	}
  

  /**
   * calculate size for preview and build the preview image
   * @return image - the image used for the preview
   */
  protected Image generatePreview()
  {       
    Image image = new RightRes().getImage(getField(), previewWidth, previewHeight);

    BufferedImage centeredImage = new BufferedImage(previewWidth+2, previewHeight+2, BufferedImage.TYPE_INT_ARGB);
    Graphics gr = centeredImage.getGraphics();
    gr.setColor(new Color(0,0,0,0));  // transparent
    gr.fillRect(0, 0, previewWidth+1, previewHeight+1);
    int x = 1 + ((previewWidth - image.getWidth(null)) / 2);
    int y = 1 + ((previewHeight - image.getHeight(null)) / 2);
    gr.drawImage(image, x, y, null);
    gr.dispose();
    
    preview.setIcon(new ImageIcon(centeredImage));
    preview.setHorizontalAlignment(SwingConstants.CENTER);
    preview.setBorder(BorderFactory.createTitledBorder("File: " + getField().getSource()));
    return image;
  }
  
  /**
   * If the field is not an AveryBackground, this method sets the
   * width and height UI gadgets to match the aspect of the image
   * @param image - the image to scale into the field dimensions
   */
  protected void reAspectFieldToImage(Image image)
  {
    if (!(getField() instanceof AveryBackground))
    {
      double imageWidth = image.getWidth(null);
      double imageHeight = image.getHeight(null);
      double fieldArea = getField().getWidth().doubleValue() * getField().getHeight().doubleValue();
      double imageArea = imageWidth * imageHeight;
      double scalar = java.lang.Math.sqrt(fieldArea / imageArea);
      
      widthGadget.setText(Predesigner.twipsToUnitString(imageWidth * scalar));
      heightGadget.setText(Predesigner.twipsToUnitString(imageHeight * scalar));
    }
  }

  /**
   * browse for a replacement graphic
   * @param event - standard parameter
   */
  protected void onBrowseButton(ActionEvent event)
  {
    JFileChooser dlg = new JFileChooser(getField().getAssociatedFilename());
    dlg.setFileFilter(new ImageFileFilter());
    dlg.setDialogTitle("Select an image file");
    dlg.setAcceptAllFileFilterUsed(false);
    
    ImageAccessory thumb = new ImageAccessory(dlg);
    dlg.setAccessory(thumb);
    dlg.addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, thumb);

    if (dlg.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
    {
      File file = dlg.getSelectedFile();
      getField().setSourceAndGallery(file.getName(), file.getParentFile().getParent());
      Image image = generatePreview();
      reAspectFieldToImage(image);
      invalidate();
      imageList.clearSelection();
      imageList.setSelectedValue(file.getName(), false);
    }
  }
  
  protected void onClipMaskButton(ActionEvent event, String command)
  {
  	AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
  	Iterator ipf = master.getFieldIterator();
  	while (ipf.hasNext())
  	{
  		AveryPanelField apf = (AveryPanelField)ipf.next();
  		if (apf instanceof Drawing)
  		{
  			Drawing drawingField = (Drawing)apf;
  			
  			int imagex = (int)((float)field.getPosition().x / 20f);
  			int imagey = (int)((float)field.getPosition().y / 20f);
  			int imageWidth = (int)(field.getWidth().doubleValue() / 20f);
  			int imageHeight = (int)(field.getHeight().doubleValue() / 20f);
  			
  			AveryImage ai = (AveryImage)field;
  			int imagesourceWidth = ai.getSrcWidth();
  			int imagesourceHeight= ai.getSrcHeight();
  			  			
  			int shapex = (int)((float)drawingField.getPosition().x / 20f);
  			int shapey = (int)((float)drawingField.getPosition().y / 20f);
  			int shapeWidth = (int)(drawingField.getWidth().doubleValue() / 20f);
  			int shapeHeight = (int)(drawingField.getHeight().doubleValue() / 20f);

  			Double shapeRotation = new Double(360.0 -drawingField.getRotation().doubleValue());
  			Double shapeRotationReverse = new Double(360.0 -shapeRotation.doubleValue());
  			
  			int lrix = imagex + imageWidth;
  			int lriy = imagey + imageHeight;
  			int lrsx = shapex + shapeWidth;
  			int lrsy = shapey + shapeHeight;
  			
  			// check for overlap
  			/*if (((shapex >= imagex && shapex <= lrix) && 
  					(shapey >= imagey && shapey <= lriy)) ||
  				 ((lrsx >= imagex && lrsx <= lrix) && 
  	  					(lrsy >= imagey && lrsy <= lriy)))*/
  			{
  				try
  				{
  	  			float scaleFactorX = (float)imageWidth / (float)imagesourceWidth;
  	  			float scaleFactorY = (float)imageHeight / (float)imagesourceHeight;
  	  			int trueX= (int)((float)(shapex - imagex) / scaleFactorX);
  	  			int trueY = (int)((float)(shapey - imagey)/ scaleFactorY);
  	  			int trueWidth = (int)((float)shapeWidth / scaleFactorX);
  	  			int trueHeight = (int)((float)shapeHeight / scaleFactorY);
  	  			
  	  			if (shapeRotation.doubleValue() != 0.0)
  	  			{
  	  				double theta = Math.toRadians(shapeRotation.doubleValue());
  	  				double thetaReverse = Math.toRadians(shapeRotationReverse.doubleValue());
  	  				double costheta = Math.cos(theta);
  	  				double sintheta = Math.sin(theta);
  	  				double costhetaReverse = Math.cos(thetaReverse);
  	  				double sinthetaReverse = Math.sin(thetaReverse);
  	  				
  	  				// have to translate and rotate about center then recalc start point
  	  				double xtc = (double)shapeWidth / 2;
  	  				double ytc = (double)shapeHeight / 2;
  	  				double xc = shapex + (double)shapeWidth / 2;
  	  				double yc = shapey + (double)shapeHeight / 2;
  	  				// rotate center about shapex, shapey
  	  				double xrc = xtc * costheta - ytc * sintheta + shapex;
  	  				double yrc = (xtc * sintheta + ytc * costheta) + shapey;
  	  				// unrotate shapex, shapey about rotated center
  	  				double xts = (double)shapex - xrc;
  	  				double yts = (double)shapey - yrc;
  	  				double xrs = xts * costhetaReverse - yts * sinthetaReverse + xrc;
  	  				double yrs = xts * sinthetaReverse + yts * costhetaReverse + yrc;
  	  				
  	  				trueX = (int)((xrs - imagex) / scaleFactorX);
  	  				trueY = (int)((yrs - imagey) / scaleFactorY);
  	  			}
  	  			  	
	  		  	String description = drawingField.getDescription();
	  		  	if (description.indexOf("_") > 0)
	  		  		description = description.substring(0, description.lastIndexOf("_"));

	  		  	String imagePath = field.getAssociatedFilename();
	  		    
	  		  	String imageGallery = Config.getDefaultImageGallery();
	  				String path = imageGallery + "/HighRes/User";
	  				String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	  				String name = drawingField.getDescription() + "_" + timestamp + ".png";
	  		  	File outputFile = new File(path + File.separatorChar + name);
	  		  	
	  		  	String parms = command + ",";
	  		  	parms += description + "," + trueWidth + "," + trueHeight + "," + trueX + "," + trueY + "," + shapeRotation + ","+ imagePath + ",";
	  	  		parms += outputFile;
	
	  	      ProcessBuilder pb=new ProcessBuilder("java", "-jar", "RenderShapes.jar", parms);
	  	      //pb.redirectOutput(Redirect.appendTo(new File("c:/JavaImaging/JavaFX/output.txt")));
	  	      //pb.redirectError(Redirect.appendTo(new File("c:/JavaImaging/JavaFX/output.txt")));
	  	      String userDir = System.getProperty("user.dir");
	  	      pb.directory(new File(userDir));
	  	      Process p = pb.start();
	  	      p.waitFor();
  				}
  				catch(Exception e)
  				{}
  				
  			}
  		}
  	}
  }
  
	protected void addFieldGeometryGadgets(JPanel panel, GridBagLayout gridbag)
	{
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.HORIZONTAL;
    c.insets = new Insets(4,0,0,0);
    c.gridwidth = 1;
    
    panel.add(new JLabel("Width:"), c);
    widthGadget.setText(Predesigner.twipsToUnitString(field.getWidth().doubleValue()));
    panel.add(widthGadget, c);
    
    panel.add(new JLabel("  "), c);

    panel.add(new JLabel("Height:"), c);
    heightGadget.setText(Predesigner.twipsToUnitString(field.getHeight().doubleValue()));
    c.gridwidth = GridBagConstraints.REMAINDER;
    panel.add(heightGadget, c);

    c.gridwidth = 1;
    c.insets = new Insets(4,0,4,0);
    panel.add(new JLabel("Position X:"), c);
    positionXGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getX()));
    panel.add(positionXGadget, c);
    panel.add(new JLabel("  "), c);

    panel.add(new JLabel("Position Y:"), c);
    positionYGadget.setText(Predesigner.twipsToUnitString(field.getPosition().getY()));
    c.gridwidth = GridBagConstraints.REMAINDER;
    panel.add(positionYGadget, c);
	}
	
	protected void updatePositionGadget(Point2D newPosition)
	{
    positionXGadget.setText(Predesigner.twipsToUnitString(newPosition.getX()));
    positionYGadget.setText(Predesigner.twipsToUnitString(newPosition.getY()));
	}
	
	protected Point getNewPosition()
	{
    double x = getField().getPosition().getX();
    double y = getField().getPosition().getY();
    if (!Predesigner.twipsToUnitString(x).equals(positionXGadget.getText()))
    {
      x = Predesigner.unitStringToTwips(positionXGadget.getText()).doubleValue();
    }
    if (!Predesigner.twipsToUnitString(y).equals(positionYGadget.getText()))
    {
      y = Predesigner.unitStringToTwips(positionYGadget.getText()).doubleValue();
    }
		Point point = new Point();
		point.setLocation(x, y);		
		return point;
	}
	
  protected Double getNewWidth()
  {
    String widthString = Predesigner.twipsToUnitString(getField().getWidth().doubleValue());
    if (widthString.equals(widthGadget.getText()))
    {
      return getField().getWidth();
    }
    else return Predesigner.unitStringToTwips(widthGadget.getText());
  }
  
  protected Double getNewHeight()
  {
    String heightString = Predesigner.twipsToUnitString(getField().getHeight().doubleValue());
    if (heightString.equals(heightGadget.getText()))
    {
      return getField().getHeight();
    }
    else return Predesigner.unitStringToTwips(heightGadget.getText());
  }
  
  ArrayList makeImageList(StyleSet ss)
  {
    ArrayList filenames = new ArrayList(7);
    filenames.add(ss.graphic01);
    filenames.add(ss.graphic03);
    filenames.add(ss.graphic08);
    filenames.add(ss.graphic09);
    filenames.add(ss.graphic10);
    filenames.add(ss.graphic11);
    filenames.add(ss.graphic12);
    return filenames;
  }
}