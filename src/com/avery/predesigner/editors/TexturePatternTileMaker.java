package com.avery.predesigner.editors;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

public class TexturePatternTileMaker
{
	//private static double BLEED_SIZE = 1.0 / 8.0;
	
	private double scale = 1.0;
	// expect sizes in points
	private double targetWidth = 612.0;
	private double targetHeight = 792.0;
	private double cornerRadius = 0.0;
	private double bleedSize = 0.0;
	
	private String shape = "rect";
	private boolean stretch = true;
	private boolean randomize = false;
	private Color foreColor = null;
	private Color backColor = null;

	public TexturePatternTileMaker()
	{
	}
	
	public BufferedImage makePattern(String filename, String outputFilename)
	{
		try
		{
      // read pattern image file
      BufferedImage image = ImageIO.read(new File(filename));
      BufferedImage image1 = null;
      BufferedImage image2 = null;
      BufferedImage image3 = null;
      
      int w = image.getWidth();
      int h = image.getHeight();
      
      randomize = randomize && (w == h);
      if (randomize)
      {
	      image1 =  rotate(image, 1);
	      image2 =  rotate(image, 2);
	      image3 =  rotate(image, 3);
      }
      
      if (foreColor != null && backColor != null)
      {
      	changeColors(image, foreColor, backColor);
      }
      
      if (scale != 1.0)
      {
      	int dw = (int)((double)w * scale);
      	int dh = (int)((double)h * scale);
      	image = rescale(image, dw, dh, scale);
      	if (randomize)
      	{
	      	image1 = rescale(image1, dw, dh, scale);
	      	image2 = rescale(image2, dw, dh, scale);
	      	image3 = rescale(image3, dw, dh, scale);
      	}
      	w = dw;
      	h = dh;
      }
      
      if (bleedSize != 0)
      {
      	targetWidth += 2.0 * bleedSize;
      	targetHeight += 2.0 * bleedSize;
      }
      
      // create image with integral row/column tiling
      double xtiles = targetWidth / w;
      double ytiles = targetHeight / h;
      if (xtiles <= 0)
      	xtiles = 1;
      if (ytiles < 0)
      	ytiles = 1;
      
      int xitiles = (int)Math.ceil(xtiles);
      int yitiles = (int)Math.ceil(ytiles);

      Random rn = new Random(210);
      int k = rn.nextInt(4);
      
      BufferedImage outImage = new BufferedImage(xitiles * w, yitiles * h, BufferedImage.TYPE_INT_ARGB);
      Graphics2D g2d = outImage.createGraphics();
      for (int i = 0; i < xitiles; i++)
      {
      	for (int j = 0; j < yitiles; j++)
      	{
      		//System.out.println(k);
      		BufferedImage bi = image;
      		if (randomize)
      		{
	      		if (k == 1)
	      			bi = image1;
	      		else if (k == 2)
	      			bi = image2;
	      		else if (k == 3)
	      			bi = image3;
      		}
      		
      		g2d.drawImage(bi, i * w, j * h, (i+1) * w, (j+1) * h, 0, 0, w, h, null, null);
      		
      		k = rn.nextInt(4);
      	}
      }
      g2d.dispose();
      
      // save full tile image for debugging only
      ///ImageIO.write(outImage, "png", new FileOutputStream("tiledfull.png"));
      
      // trim full tile to project dimensions
      // centers full tile on trimmed tile to give even appearance on the perimeter
      
      // clip size
      int xclip = (int)Math.round(targetWidth);
      int yclip = (int)Math.round(targetHeight);
      BufferedImage clippedImage = new BufferedImage(xclip, yclip, BufferedImage.TYPE_INT_ARGB);
      int xcenter = 0;
      int ycenter = 0;
      
      if (!stretch)
      {
	      //System.out.println("xclip, yclip=" + xclip + "," + yclip);
	      //System.out.println("full xclip, yclip=" + outImage.getWidth() + "," + outImage.getHeight());
	     
	      double xdiff = ((double)outImage.getWidth() - (double)xclip) / 2.0;
	      if (xdiff > 0)
	      	xcenter = (int)(xdiff);
	      
	      double ydiff = ((double)outImage.getHeight() - (double)yclip) / 2.0;
	      if (ydiff > 0)
	      	ycenter = (int)(ydiff);
      }
      
      //System.out.println("xc, yc=" + xcenter + "," + ycenter);
      g2d = clippedImage.createGraphics();
      
      if (shape.equals("rect") && cornerRadius > 0.0)
      {
      	RoundRectangle2D.Double roundedRect = new RoundRectangle2D.Double();
      	if (bleedSize != 0)
      	{
        	int dWidth = xclip + 2 * (int)(bleedSize);
        	double scaleFactor = (double)dWidth / (double)xclip;
        	cornerRadius *= scaleFactor;
      	}
      	int cRadius = (int)cornerRadius;
      	roundedRect.setRoundRect(0, 0, xclip, yclip, cRadius, cRadius);
      	g2d.setClip(roundedRect);
      }
      else if (shape.equals("ellipse"))
      {
      	Ellipse2D.Double ellipse = new Ellipse2D.Double();
      	ellipse.setFrame(0, 0, xclip, yclip);
      	g2d.setClip(ellipse);    	
      }
      
      if (stretch)
      {
      	//double scaleFactor = (double)xclip / (double)outImage.getWidth();
      	//outImage = rescale(outImage, xclip, yclip, scaleFactor);
      	g2d.drawImage(outImage, 0, 0, xclip, yclip, 0, 0, outImage.getWidth(), outImage.getHeight(), null, null);
      }
      else
      {
      	g2d.drawImage(outImage, 0, 0, xclip, yclip, xcenter, ycenter, xclip + xcenter, yclip + ycenter, null, null);
      }

      // questionable try clean up seams
      //if (randomize)
      //{
      	//clippedImage = randomSwap(clippedImage, w, h, xcenter, ycenter);
      //}
      // save clipped pattern
      if (outputFilename != null)
      	ImageIO.write(clippedImage, "png", new FileOutputStream(outputFilename));
      return clippedImage;
		}
    catch (IOException e)
    {
      // log the exception
      // re-throw if desired
    }
		return null;
	}
	
	public void setScale(double scale)
	{
		this.scale = scale;
	}
	
	public void setImageSize(double width, double height)
	{
		targetWidth = width;
		targetHeight = height;
	}
	
	public void setCornerRadius(double cornerRadius)
	{
		this.cornerRadius = cornerRadius;
	}
	
	public void setShape(String shape)
	{
		this.shape = shape;
	}
	
	public void setBleedSize(double bleedSize)
	{
		this.bleedSize = bleedSize;
	}
	
	public void setStretch(boolean stretch)
	{
		this.stretch = stretch;
	}
	
	public void setRandomize(boolean randomize)
	{
		this.randomize = randomize;
	}
	
	public void setColors(Color foreColor, Color backColor)
	{
		this.foreColor = foreColor;
		this.backColor = backColor;		
	}
	
	private BufferedImage rescale(BufferedImage sbi, int dWidth, int dHeight, double scaleFactor)
	{
    BufferedImage dbi = null;
    if (sbi != null)
    {
      dbi = new BufferedImage(dWidth, dHeight, BufferedImage.TYPE_INT_ARGB);
      Graphics2D g = dbi.createGraphics();
      AffineTransform at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor);
      g.drawRenderedImage(sbi, at);
      g.dispose();
    }
    return dbi;
	}
	
	private BufferedImage rotate(BufferedImage sbi, int quadrant)
	{
		BufferedImage rotated = null;
    if (sbi != null)
    {
    	int width = sbi.getWidth();
    	int height = sbi.getHeight();
    	/*rbi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
      Graphics2D g = rbi.createGraphics();
      AffineTransform at = AffineTransform.getQuadrantRotateInstance(1);
      g.drawRenderedImage(sbi, at);
      g.dispose();*/
      
      rotated = new AffineTransformOp(
          AffineTransform.getQuadrantRotateInstance(quadrant, width / 2,
              height / 2), AffineTransformOp.TYPE_BILINEAR).filter(sbi, null);
    }
    return rotated;
	}

	private void changeColors(BufferedImage bi, Color foreColor, Color backColor)
	{
		int fore = 0xff000000 + (foreColor.getRed() << 16) + (foreColor.getGreen() << 8) + foreColor.getBlue();
		int back = 0xff000000 + (backColor.getRed() << 16) + (backColor.getGreen() << 8) + backColor.getBlue();
		for (int i = 0; i < bi.getWidth(); i++)
		{
			for (int j = 0; j < bi.getHeight(); j++)
			{
				int pixel = bi.getRGB(i, j);
				int red = (pixel & 0xff000000) >> 16;
				int green = (pixel & 0x00ff0000) >> 8;
				int blue = pixel & 0xff;
				if (red < 128 && green < 128 && blue < 128)
				{
					// its white make it backColor
					bi.setRGB(i, j, fore);
				}
				else
				{
					bi.setRGB(i, j, back);
				}			
			}
		}
	}

	private BufferedImage randomSwap(BufferedImage bi, int targetW, int targetH, int xcenter, int ycenter)
	{
		Random random = new Random(210);
		//Random random1 = new Random(210);
		int w = bi.getWidth();
		int h = bi.getHeight();
		int windex = xcenter;
		int black = 0xff000000 + (Color.BLACK.getRed() << 16) + (Color.BLACK.getGreen() << 8) + Color.BLACK.getBlue();
		
		BufferedImage obi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for (int i = 0; i < w; i++)
		{
			int i1 = random.nextInt(w);
			int hindex = ycenter;
			
			for (int j = 0; j < h; j++)
			{
				// method 1 randomize entire image
				/*int j1 = random.nextInt(h);
				int colorswap = bi.getRGB(i, j);
				obi.setRGB(i, j, bi.getRGB(i, j1));
				obi.setRGB(i, j1, colorswap);*/
				
				// method 2 averages the tile seams
				obi.setRGB(i, j, bi.getRGB(i, j));
				if ((hindex++ >= (targetH - 1)) || (hindex < 1)
						|| (windex >= (targetW - 1)) || (windex < 1))
				{
					
					if (i > 0 && i < (w-1) && j > 0 && j < (h-1))
					{
						//System.out.println("w, hindex=" + windex + "," + hindex);
						int pixel = averageColor(bi, i, j);				
						obi.setRGB(i, j, pixel); //black); //pixel);
					}
				}
				// end methods
				if (hindex == (targetH-1))
					hindex = 0;
			}
			windex++;
			if (windex == (targetW-1))
				windex = 0;
		}
		return obi;
	}
	
	private int averageColor(BufferedImage bi, int col, int row)
	{
		long redsum = 0;
		long greensum = 0;
		long bluesum = 0;
		
		for (int i = col-1; i < col+2; i++)
		{
			for (int j = row-1; j < row+2; j++)
			{
				int color = bi.getRGB(i, j);
				redsum += (color >> 16) & 0xff;
				greensum += (color >> 8) & 0xff;
				bluesum += color & 0xff;		
			}
		}
		int red = (int)(redsum / 9);
		int green = (int)(greensum / 9);
		int blue = (int)(bluesum / 9);
		
		int avgcolor =  0xff000000 + (red << 16) + (green << 8) + blue;

		return avgcolor;
	}

}
