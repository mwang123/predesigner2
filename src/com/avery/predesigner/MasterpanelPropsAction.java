package com.avery.predesigner;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;
import com.avery.project.Mergeable;
import com.avery.project.Text;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: UndoableAction to change masterpanel properties</p>
 * <p>Copyright: Copyright 2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

class MasterpanelPropsAction extends UndoableAction
{
  private Project project;
  private AveryMasterpanel panel;
  private String oldDescription;
  private String newDescription;
  private boolean reorient;
  private boolean clockwise;
  private boolean mergeMapChanged;
  private String oldMergeMap;
  private String newMergeMap;
  private boolean editable;
  private boolean previewable;
   private Hashtable mergeContent = new Hashtable();
  
  MasterpanelPropsAction(Project project, AveryMasterpanel panel, 
  		String newDescription, boolean reorient, boolean clockwise,
			boolean mergeMapChanged, String newMergeMap,boolean editable, boolean previewable)
  {
    super("Undo Masterpanel Properties");
    this.project = project;
    this.panel = panel;
    this.oldDescription = panel.getDescription();
    this.newDescription = newDescription;
    this.reorient = reorient;
    this.clockwise = clockwise;
    this.mergeMapChanged = mergeMapChanged;
    if (mergeMapChanged)
    {
    	this.oldMergeMap = panel.getMergeMapName();
    	this.newMergeMap = newMergeMap;
    }
    this.editable = editable;
    this.previewable = previewable;
  }
  
  void undo()
  {
    if (!oldDescription.equals(newDescription))
    {
      panel.setDescription(oldDescription);
      Predesigner.getFrame().rebuildMasterpanelPopupMenu(project);
    }

    if (reorient)
    {
    	RotationManager.rotateMasterpanel(project, panel.getID(), !clockwise);
    }
    
    if (mergeMapChanged)
    {
    	panel.setMergeMapName(oldMergeMap);
    	if (mergeContent.size() > 0)
    	{
    		// restore mergeKey lines in content
      	Iterator iterator = panel.getFieldIterator();
      	while (iterator.hasNext())
      	{
      		AveryPanelField pf = (AveryPanelField)iterator.next();
      		if (pf instanceof com.avery.project.Mergeable
      				&& mergeContent.containsKey(pf.getID()))
      		{
      			((Mergeable)pf).setTextContent((List)mergeContent.get(pf.getID()));
      		}
      	}
    	}
    }
    
    if (reorient || (mergeContent.size() > 0))
    {
      Predesigner.getFrame().updateView(true);
    }
    panel.setEditable(!editable);
    panel.setPreviewable(!previewable);
  }
  
  void execute()
  {
    if (!oldDescription.equals(newDescription))
    {
      panel.setDescription(newDescription);
      Predesigner.getFrame().rebuildMasterpanelPopupMenu(project);
    }
    
    if (reorient)
    {
    	RotationManager.rotateMasterpanel(project, panel.getID(), clockwise);
    }
    
    if (mergeMapChanged)
    {
    	panel.setMergeMapName(newMergeMap);
    	Iterator iterator = panel.getFieldIterator();
    	while (iterator.hasNext())
    	{
    		AveryPanelField pf = (AveryPanelField)iterator.next();
    		if (pf instanceof com.avery.project.Mergeable)
    		{
    			List oldlist = ((Mergeable)pf).getTextContent();
    			List newlist = new ArrayList();
    			// create a new content list that contains none of the mergeKey lines
    			Iterator textIterator = oldlist.iterator();
    			while (textIterator.hasNext())
    			{
    				Text text = (Text)textIterator.next();
    				if (text.hasMergeKey() == false)
    				{
    					newlist.add(text);   // plain text content
    				}
    			}
    			if (oldlist.size() != newlist.size())
    			{	
    				// stash oldlist for undo
    				mergeContent.put(pf.getID(), oldlist);
    				// set new content that contains no mergeKey lines
    				((Mergeable)pf).setTextContent(newlist);
    			}
    		}
    	}
    }
    
    if (reorient || (mergeContent.size() > 0))
    {
      Predesigner.getFrame().updateView(true);
    }
    
    panel.setEditable(editable);
    panel.setPreviewable(previewable);
  }
}