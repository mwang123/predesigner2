/*
 * MultiplierDialog.java Created on Mar 7, 2008 by leeb
 * Copyright 2008 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileFilter;

import org.jdom.JDOMException;

import com.avery.product.ProductGroupList;
import com.avery.project.Descriptions;

/**
 * @author leeb
 * Mar 7, 2008
 * MultiplierDialog
 */
class MultiplierDialog extends JDialog
{
  private JTextArea logArea;
  private JScrollPane scrollPane;
  private JButton okayButton = new JButton("OK");
  private JButton saveButton = new JButton("Save...");
  private JTextField sumField = new JTextField("0");
  private int sum = 0;
  
  MultiplierDialog(String title)
  {
    // modeless dialog
    super(Predesigner.getFrame(), title, false);
    
    logArea = new JTextArea(title);
    
    okayButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onOkay(e);
      }
    });
    
    saveButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onSave();
      }
    });
    
    buildUI();
  }
  
  void buildUI()
  {
    Frame1 owner = Predesigner.getFrame();
    super.setSize(owner.getWidth() - 48, owner.getHeight() - 48);
    super.setLocationRelativeTo(owner);
    
    scrollPane = new JScrollPane(logArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

    JPanel buttonPanel = new JPanel(new GridLayout(1, 7));
    sumField.setHorizontalAlignment(JTextField.CENTER);
    sumField.setEditable(false);
    sumField.setFocusable(false);
    buttonPanel.add(sumField);
    buttonPanel.add(new JLabel(" projects created"));
    buttonPanel.add(new JPanel());
    buttonPanel.add(okayButton);
    buttonPanel.add(new JPanel());
    buttonPanel.add(new JPanel());
    buttonPanel.add(saveButton);
    
    BorderLayout borderLayout = new BorderLayout();
    borderLayout.setVgap(8);
    super.getContentPane().setLayout(borderLayout);    
    super.getContentPane().add(scrollPane, BorderLayout.CENTER);
    super.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
  }
  
  private void onOkay(ActionEvent event)
  {
    super.setVisible(false);
    dispose();
  }
  
  void logComment(String str)
  {
    logArea.append("\n" + str);
    logArea.invalidate();
    scrollPane.update(scrollPane.getGraphics());
  }
  
  void logProject(File file)
  {
    logArea.append("\n" + file);
    logArea.invalidate();
    scrollPane.update(scrollPane.getGraphics());
    
    sumField.setText(Integer.toString(++sum));
    sumField.invalidate();
    sumField.update(sumField.getGraphics());
  }
  
  void logError(String str, File file)
  {
    logArea.append("\n" + str + " - " + file.getName());
    logArea.invalidate();
    scrollPane.update(scrollPane.getGraphics());
  }
  
  void makeBusy(boolean busy)
  {
    if (busy)
    {
      super.setCursor(new Cursor(Cursor.WAIT_CURSOR));
      logArea.setCursor(new Cursor(Cursor.WAIT_CURSOR));
      okayButton.setEnabled(false);
      saveButton.setEnabled(false);
    }
    else
    {
      super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      logArea.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      okayButton.setEnabled(true);
      saveButton.setEnabled(true);
    }
  }
  
  private void onSave()
  {
    JFileChooser dlg = new JFileChooser(Config.getProjectHome());
    dlg.setDialogTitle("Save Multiplier Log File");
    dlg.setApproveButtonText("Save Log");
    dlg.addChoosableFileFilter(new FileFilter()
    {
      public boolean accept(File f)   { return (f.isDirectory() || f.getName().endsWith(".txt"));  }
      public String getDescription()  { return "Text files (*.txt)";  }
    });
    dlg.setSelectedFile(new File("log." + new SimpleDateFormat("yyyy-MM-dd.HHmm").format(new Date()) + ".txt"));
    
    if (dlg.showSaveDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
      File file = dlg.getSelectedFile();
      if (file.exists())
      {
        if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), 
            "Overwrite " + file.getName() + "?", "confirm", JOptionPane.YES_NO_OPTION)
            != JOptionPane.YES_OPTION)
        {
          onSave();    // recurse
          return;
        }
      }
      
      try
      {
        PrintStream p = new PrintStream(new FileOutputStream(file));
        p.print(logArea.getText());
        p.close();
      }
      catch (Exception e)
      {
        System.err.println ("Error writing to file " + file);
      }
    }   
  }
  
  class LanguageSelector extends JPanel
  {   
    JComboBox fromComboBox = new JComboBox();
    JComboBox toComboBox = new JComboBox();
    
    LanguageSelector()
    throws JDOMException, IOException
    {
      initComboBoxes();
      
      GridBagLayout gridbag = new GridBagLayout();
      setLayout(gridbag);
      GridBagConstraints c = new GridBagConstraints();
      c.gridwidth = GridBagConstraints.REMAINDER;

      add(new JLabel("Apply the following language translation"), c);
      add(new JLabel("to all projects in your Projects folder?"), c);

      c.fill = GridBagConstraints.HORIZONTAL;
     
      c.insets = new Insets(10, 10, 0, 10);
      c.gridwidth = 1;     
      add(new JLabel("From:"), c);
      c.gridwidth = GridBagConstraints.REMAINDER;
      add(fromComboBox, c);
      
      c.insets = new Insets(10, 10, 10, 10);
      c.gridwidth = 1;
      add(new JLabel("To:"), c);
      c.gridwidth = GridBagConstraints.REMAINDER;
      add(toComboBox, c);
    }
    
    private void initComboBoxes()
    throws JDOMException, IOException
    {
      SampleText sampleText = new SampleText();   // can throw exceptions
      
      List sampleTextLanguages = sampleText.getSupportedLanguages();
      Enumeration languages = Descriptions.getSupportedLanguages();
      while (languages.hasMoreElements())
      {
        String language = (String)languages.nextElement();
        if (sampleTextLanguages.contains(language))
        {
          fromComboBox.addItem(language);
          toComboBox.addItem(language);
        }
      }
    }
    
    String getFromLanguage()
    {
      return (String)fromComboBox.getSelectedItem();
    }
    
    String getToLanguage()
    {
      return (String)toComboBox.getSelectedItem();
    }
  }
  
  class ProductTypeSelector extends JPanel
  {
    JList list;
    JCheckBox excludeTextOnlyProjects = new JCheckBox("Exclude Text Only Projects");
    
  	ProductTypeSelector(ProductGroupList pgl)
		{
      String text = "Apply '" + Predesigner.styleSet.name + "' Style Set to placeholder" 
        + "\nprojects matching the selected product type(s): \n";
      JTextPane textPane = new JTextPane();
      textPane.setText(text);
      textPane.setOpaque(false);
      textPane.setEnabled(false);
      textPane.setDisabledTextColor(Color.black);
      
  		List l = pgl.getProductTypePrefixes();
  		java.util.Collections.sort(l);
      list = new JList(l.toArray());
      list.setBorder(BorderFactory.createTitledBorder("Product Type Prefixes"));
      
      setLayout(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();
      c.gridwidth = GridBagConstraints.REMAINDER;
      c.fill = GridBagConstraints.HORIZONTAL;
      add(textPane,c);
  		add(list,c);
      
      excludeTextOnlyProjects.setSelected(true);
      add(excludeTextOnlyProjects,c);
		}
    
    public Object[] getSelectedPrefixes()
    {
      return list.getSelectedValues();
    }
  }
}
