/*
 * ConfigFileBrowsePanel.java Created on Dec 5, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

class ConfigFileBrowsePanel extends BrowsePanel
{
  File theFile;

  ConfigFileBrowsePanel(String description, File file)
  {
    super(description, file.getAbsolutePath());
    theFile = file;
  }
  
  File getFile()
  {
    File file = new File(textField.getText());
    if (!theFile.equals(file) && file.exists() && !file.isDirectory())
      return file;
    else
      return theFile;
  }

  /**
   * @override This is the action that takes place when the user clicks 
   * the "Browse..." button.  On success, it calls textField.setText()
   * with the user-selected file path, and sets theFile to the new file.
   */
  protected void browse()
  {  
    JFileChooser dlg = new JFileChooser(theFile);
    dlg.setDialogTitle(label.getText());
    dlg.setApproveButtonText("Select");
    dlg.setSelectedFile(theFile);
    dlg.setFileSelectionMode(JFileChooser.FILES_ONLY);
    
    if (dlg.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
    {
      File newFile = dlg.getSelectedFile();
      if (newFile != null && newFile.exists() && !newFile.isDirectory())
      {
        textField.setText(newFile.getAbsolutePath());
        theFile = newFile;
      }
      else
      {
        JOptionPane.showMessageDialog(getParent(), newFile + " is not a valid selection", "Error", JOptionPane.ERROR_MESSAGE);
        browse();   // try again
      }
    }
  }
}
