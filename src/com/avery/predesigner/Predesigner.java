/**
 * Title:         Predesigner - AveryProject Editor application
 * Copyright:     Copyright (c)2013-2016 AveryProducts Corp. All Rights Reserved
 * Company:       Avery Dennison
 * @author        Bob Lee; Brad Nelson
 * @version       2.0
 */

package com.avery.predesigner;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.UIManager;

import org.faceless.pdf2.PDF;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.avery.Averysoft;
import com.avery.predesign.StyleSet;
import com.avery.product.ProductGroup;
import com.avery.product.ProductGroupList;
import com.avery.project.Descriptions;
import com.avery.utils.AveryUtils;

public class Predesigner
{
  public static Frame1 frame = null;
  public static StyleSet styleSet = StyleSet.newStyleSet();
  
  static 
  {	// setup for Mac happens before the windowing system initializes   
		if (System.getProperty("os.name").toLowerCase().startsWith("mac os x"))
		{
		  System.setProperty("com.apple.mrj.application.apple.menu.about.name","Predesigner2");
		  System.setProperty("apple.laf.useScreenMenuBar", "true");
		}
  }
  
  
  /**Construct the application*/
  public Predesigner()
  {
	/*    JFrame jframe = new JFrame();
    //frame = new Frame();
    
    // Always validate frames that have preset sizes
    // Pack frames that have useful preferred size info, e.g. from their layout
    frame.validate();
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    // Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height) {
      frameSize.height = screenSize.height;
    }
    if (frameSize.width > screenSize.width) {
      frameSize.width = screenSize.width;
    }
    frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    
    if (!Config.ConfigFileExists())
    {
    	Config.configure();		// initial configuration from user
    }
    
    frame.setVisible(true);*/
  }

  public void createFrame()
  {
	 //JFrame jframe = new JFrame();
	  //frame = new Frame();
	  frame.createFrame();
	  
	  // Always validate frames that have preset sizes
	  // Pack frames that have useful preferred size info, e.g. from their layout
	  frame.validate();
	  frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	
	  // Center the window
	  Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	  Dimension frameSize = frame.getSize();
	  if (frameSize.height > screenSize.height) {
	    frameSize.height = screenSize.height;
	  }
	  if (frameSize.width > screenSize.width) {
	    frameSize.width = screenSize.width;
	  }
	  frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
	  
	  if (!Config.ConfigFileExists())
	  {
	  	Config.configure();		// initial configuration from user
	  }
	  
	  frame.setVisible(true);
	  
  }

  /**
   * Predesigner main
   * @param args - unused
   */
  public static void main(String[] args)
  {
    try
    {
    	 	
    	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      Predesigner predesigner = new Predesigner();
      frame = new Frame1("Predesigner2");

      // use this key when license expires in August 2016  5A386E208E4BE8F
      // use this key when license expires in August 2015  5556G835752710H
      PDF.setLicenseKey("5A386E208E4BE8F");  // extended edition license
      //PDF.setLicenseKey("5046D3EC77744EA");  // extended edition license
      //PDF.setLicenseKey("GH34B7C5EBFFH73");
    	
      // Configuration
      Config.read();
      
      // Font initialization
      setupFontEncodings();
      
      // Localization initialization
      Descriptions.initialize(new File("./Translations.xml"));            
    
	  predesigner.createFrame();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
   
    if (args.length == 1)
    {
    	try
    	{
    		String projectFile = Config.getProjectHome() + "/" + args[0];
    		Project project = openProject(new File(projectFile));
        int orphans = project.fixGalleries(Config.getDefaultImageGallery());

        if (orphans != 0 && new File(Config.getProjectHome(), "HighRes").exists())
        {
          orphans = project.fixGalleries(Config.getProjectHome());
        }
        boolean foundImage = project.updatePapers(Config.getProjectHome() + "/papers");
        if (!foundImage)
        {
        	ProductGroup group = Predesigner.getProductGroupList().getProductGroup(project.getProductGroup());
		      project.applyAppearances(group,	new File(Config.getBatDirectory() + "/papers").getAbsolutePath(), false);
        }
                
        // open the designated style set
        Predesigner.styleSet = project.getStyleSet();
        
        // now save it right away with _out filename
        projectFile = projectFile.replace(".xml", "_out.xml");
        project.saveAverysoft(new File(projectFile));
        
        getFrame().dispose();
    	}
    	catch(Exception e)
      {
        e.printStackTrace();
      }
    }
  }
  


  public static Frame1 getFrame()
  {
    return frame;
  }
  
  public static Project getProject()
  {
    return Frame1.getProject();
  }
  
  /**
   * Font encodings are typically set in a file called FontEncodings.txt.
   */
  static void setupFontEncodings()
  {
    File file = Config.fontEncodingsFile;
    if (file.exists())
    {
      com.avery.utils.FontEncoding.setEncodingFile(file.getAbsolutePath());
      System.out.println("Font encodings in: " + file.getAbsolutePath());
      System.out.println("TrueType fonts: " + com.avery.utils.FontEncoding.getTTFFamilies().toString());
    }
  }
  
  private static ProductGroupList productGroupList;
  private static File pgFile = Config.productGroupListFile;
  /**
   * The class maintains a static ProductGroupList.  If it's null or empty,
   * this method will attempt to read the file "BATs/ProductGroupList.xml".
   * It then validates that the BATs in the list exist and populates the 
   * static list with only the ones that are present.
   * 
   * @return the static ProductGroupList
   * @throws IOException on file error
   * @throws JDOMException on XML error
   */
  static ProductGroupList getProductGroupList()
  throws IOException, JDOMException
  {
    if (productGroupList == null || productGroupList.isEmpty() || !pgFile.equals(Config.productGroupListFile))
    {
      // create a temporary list from the XML file
      SAXBuilder builder = new SAXBuilder(true);
      builder.setFeature("http://apache.org/xml/features/validation/schema", true);
      builder.setProperty(
          "http://apache.org/xml/properties/schema/external-schemaLocation",
          Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 

      ProductGroupList pgList = new ProductGroupList(builder.build(Config.productGroupListFile).getRootElement());
      
      // create the list that we will actually use
      productGroupList = new ProductGroupList();
      Iterator iterator = pgList.iterator();
      while (iterator.hasNext())
      {
        ProductGroup pg = (ProductGroup)iterator.next();
        File batFile = new File(Config.getBatDirectory(), pg.getBatName());
        if (batFile.exists())
        {
          productGroupList.add(pg);
        }
        else // note missing BAT to stderr
        {
          System.err.println("BAT not found: " + batFile.getAbsolutePath());
        }
      }
    }
    
    return productGroupList;
  }
  
  /**
   * The method creates new instance of Project class (.xml files) or
   *  ProjectBundle class (.avery files) and initialize it from file.
   * 
   * @return Project
   * @throws Exception on file read error
   */
  static Project openProject(File file) throws Exception
  {
	  if (null == file) return null;
	  
	  Project project = null;
	  
	  int lastDotPosition = file.getName().lastIndexOf(".");
	  if (file.getName().indexOf("xml", lastDotPosition) > -1) 
	  {
		  project = new Project();
	  }
	  else if (file.getName().indexOf("avery", lastDotPosition) > -1) {
		  project = new ProjectBundle();
	  }

	  if (null != project) 
	  {
		  project.initFrom(file);
		  getFrame().setProject(project);
	  }

	  return project;
  }
  
  static Project openProjectInvisibly(File file) throws Exception
  {
	  if (null == file) return null;
	  
	  Project project = null;
	  
	  int lastDotPosition = file.getName().lastIndexOf(".");
	  if (file.getName().indexOf("xml", lastDotPosition) > -1) 
	  {
		  project = new Project();
	  }
	  else if (file.getName().indexOf("avery", lastDotPosition) > -1) {
		  project = new ProjectBundle();
	  }

	  if (null != project) 
	  {
		  project.initFrom(file);
		  getFrame().setProjectInvisibly(project);
	  }

	  return project;
  }
  
  public static String twipsToUnitString(double twips)
  {
	  return AveryUtils.twipsToUnitString(twips, Config.units);
  }
  
  public static Double unitStringToTwips(String str)
  {
	  return AveryUtils.unitStringToTwips(Config.units, str);
  }
  
  public static double twipsToUnits(double twips)
  {
	  return AveryUtils.twipsToUnits(twips, Config.units);
  }
}