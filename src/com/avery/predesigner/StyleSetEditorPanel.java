/*
 * StyleSetEditorPanel.java Created on Jan 3, 2008 by leeb
 * Copyright 2008 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.avery.predesign.StyleSet;
import com.avery.project.AveryImage;
import com.avery.project.RightRes;
import com.avery.utils.AveryUtils;

/**
 * @author leeb
 * Jan 3, 2008
 * StyleSetEditorPanel
 */
class StyleSetEditorPanel extends JPanel
{
  private StyleSet placeholderSS = StyleSet.newStyleSet();
  private StyleSet targetSS;
  
  private JTextField nameField = new JTextField(20);
  private JTextArea descriptionArea = new JTextArea();
  
  private java.util.List fonts = com.avery.utils.FontEncoding.getTTFFamilies();
  private JTextField placeholderFontField = new JTextField();
  private JComboBox fontComboBox;

  private ColorPanel colorPanel1;
  private ColorPanel colorPanel2;
  private ColorPanel colorPanel3;
  static JColorChooser colorChooser = new JColorChooser();
  
  private GraphicPanel graphicPanel1;
  private GraphicPanel graphicPanel2;
  private GraphicPanel graphicPanel3;
  private GraphicPanel graphicPanel4;
  private GraphicPanel graphicPanel5;
  private GraphicPanel graphicPanel6;
  private GraphicPanel graphicPanel7;
  private GraphicPanel graphicPanel8;
  private GraphicPanel graphicPanel9;
  private GraphicPanel graphicPanel10;
  private GraphicPanel graphicPanel11;
  private GraphicPanel graphicPanel12;
  
  StyleSetEditorPanel(StyleSet ss)
  {
    targetSS = ss;
    // show busy cursor while we construct the layout
    Component frame = Predesigner.getFrame();
    Cursor cursor = frame.getCursor();
    try {
      frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      initLayout();
    }
    finally {
      frame.setCursor(cursor);
    }
  }
  
  void initLayout()
  {
    setLayout(new GridBagLayout());
    GridBagConstraints outer = new GridBagConstraints();
    outer.fill = GridBagConstraints.HORIZONTAL;
    
    JLabel nameLabel = new JLabel("Style Set Name: ");
    nameLabel.setHorizontalAlignment(JLabel.RIGHT);
    outer.gridwidth = 1;
    outer.weightx = 0;
    outer.anchor = GridBagConstraints.NORTH;
    outer.insets = new Insets(2,0,0,2);
    this.add(nameLabel, outer);
    
    nameField.setText(targetSS.name);
    nameField.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    nameField.setFont(new Font("sans", Font.PLAIN, 11));
    outer.insets = new Insets(0,0,0,0);
    this.add(nameField, outer);
    
    JLabel descriptionLabel = new JLabel("Description: ");
    descriptionLabel.setHorizontalAlignment(JLabel.RIGHT);
    outer.insets = new Insets(2,64,0,2);
    this.add(descriptionLabel, outer);
    
    descriptionArea.setText(targetSS.description);
    descriptionArea.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    descriptionArea.setFont(new Font("sans", Font.PLAIN, 11));
    descriptionArea.setRows(2);
    outer.gridwidth = GridBagConstraints.REMAINDER;
    outer.weightx = 1;
    outer.insets = new Insets(0,0,0,0);

    this.add(descriptionArea, outer);
    
    // font
    JPanel fontPanel = new JPanel();
    fontPanel.setBorder(BorderFactory.createTitledBorder("Font"));
    fontPanel.setLayout(new GridBagLayout());
    GridBagConstraints inner = new GridBagConstraints();
    inner.insets = new Insets(5,5,5,5);
    inner.fill = GridBagConstraints.HORIZONTAL;

    inner.gridwidth = 1;
    inner.anchor = GridBagConstraints.NORTHWEST;
    inner.weightx = .35;
    fontPanel.add(new JLabel(" Placeholder Font: "), inner);
      
    placeholderFontField.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    placeholderFontField.setFont(new Font("sans", Font.PLAIN, 11));
    placeholderFontField.setText(placeholderSS.font);
    if (targetSS.placeholderFont.length() > 0 && !targetSS.placeholderFont.equals(placeholderSS.font))
    	placeholderFontField.setText(targetSS.placeholderFont);
    placeholderFontField.setEnabled(true);
    inner.gridwidth = GridBagConstraints.REMAINDER;
    inner.weightx = .65;
    fontPanel.add(placeholderFontField, inner);
    
    inner.gridwidth = 1;
    inner.anchor = GridBagConstraints.WEST;
    inner.weightx = .25;
    fontPanel.add(new JLabel(" Style Set Font: "), inner);
    
    fontComboBox = new JComboBox(fonts.toArray());
    fontComboBox.setSelectedIndex(fonts.indexOf(targetSS.font));
    inner.gridwidth = GridBagConstraints.REMAINDER;
    inner.weightx = .65;
    fontPanel.add(fontComboBox, inner);

    outer.gridwidth = 1;
    outer.weightx = 0.0;
    outer.insets = new Insets(5,0,0,0);
    this.add(fontPanel, outer);
    
    // three colors
    colorPanel1 = new ColorPanel("Color-1", placeholderSS.color1, targetSS.color1);
    colorPanel2 = new ColorPanel("Color-2", placeholderSS.color2, targetSS.color2);
    colorPanel3 = new ColorPanel("Color-3", placeholderSS.color3, targetSS.color3);

    JPanel colorPanels = new JPanel();
    colorPanels.add(colorPanel1);
    colorPanels.add(colorPanel2);
    colorPanels.add(colorPanel3);
    outer.gridwidth = GridBagConstraints.REMAINDER;
    outer.weightx = 0.0;
    outer.insets = new Insets(0,0,0,0);
    this.add(colorPanels, outer);
    
    // twelve graphics
    graphicPanel1 = new GraphicPanel("Graphic-1", placeholderSS.graphic01, targetSS.graphic01);
    graphicPanel2 = new GraphicPanel("Graphic-2", placeholderSS.graphic02, targetSS.graphic02);
    graphicPanel3 = new GraphicPanel("Graphic-3", placeholderSS.graphic03, targetSS.graphic03);
    graphicPanel4 = new GraphicPanel("Graphic-4", placeholderSS.graphic04, targetSS.graphic04);
    graphicPanel5 = new GraphicPanel("Graphic-5", placeholderSS.graphic05, targetSS.graphic05);
    graphicPanel6 = new GraphicPanel("Graphic-6", placeholderSS.graphic06, targetSS.graphic06);
    graphicPanel7 = new GraphicPanel("Graphic-7", placeholderSS.graphic07, targetSS.graphic07);
    graphicPanel8 = new GraphicPanel("Graphic-8", placeholderSS.graphic08, targetSS.graphic08);
    graphicPanel9 = new GraphicPanel("Graphic-9", placeholderSS.graphic09, targetSS.graphic09);
    graphicPanel10 = new GraphicPanel("Graphic-10", placeholderSS.graphic10, targetSS.graphic10);
    graphicPanel11 = new GraphicPanel("Graphic-11", placeholderSS.graphic11, targetSS.graphic11);
    graphicPanel12 = new GraphicPanel("Graphic-12", placeholderSS.graphic12, targetSS.graphic12);
    
    JPanel graphicPanels = new JPanel();
    graphicPanels.setLayout(new GridLayout(3, 4));
    graphicPanels.add(graphicPanel1);
    graphicPanels.add(graphicPanel2);
    graphicPanels.add(graphicPanel3);
    graphicPanels.add(graphicPanel4);
    graphicPanels.add(graphicPanel5);
    graphicPanels.add(graphicPanel6);
    graphicPanels.add(graphicPanel7);
    graphicPanels.add(graphicPanel8);
    graphicPanels.add(graphicPanel9);
    graphicPanels.add(graphicPanel10);
    graphicPanels.add(graphicPanel11);
    graphicPanels.add(graphicPanel12);
    this.add(graphicPanels, outer);
  }
  
  /**
   * Called after the dialog closes, this updates a StyleSet with the results from the UI controls
   * @param ss the StyleSet to fill
   */
  void updateStyleSet(StyleSet ss)
  {
    ss.name = AveryUtils.alphaNumeric(nameField.getText());
    ss.description = descriptionArea.getText();
    ss.font = (String)fontComboBox.getSelectedItem();
    ss.placeholderFont = placeholderFontField.getText();
    ss.color1 = colorPanel1.getColor();
    ss.color2 = colorPanel2.getColor();
    ss.color3 = colorPanel3.getColor();
    ss.graphic01 = graphicPanel1.getImagename();
    ss.graphic02 = graphicPanel2.getImagename();
    ss.graphic03 = graphicPanel3.getImagename();
    ss.graphic04 = graphicPanel4.getImagename();
    ss.graphic05 = graphicPanel5.getImagename();
    ss.graphic06 = graphicPanel6.getImagename();
    ss.graphic07 = graphicPanel7.getImagename();
    ss.graphic08 = graphicPanel8.getImagename();
    ss.graphic09 = graphicPanel9.getImagename();
    ss.graphic10 = graphicPanel10.getImagename();
    ss.graphic11 = graphicPanel11.getImagename();
    ss.graphic12 = graphicPanel12.getImagename();
  }
  
  String getStyleSetName()
  {
    return nameField.getText();
  }
  
  private class ColorPanel extends JPanel
  {
    private Color placeholderColor;
    private Swatch colorSwatch;
    private Button button = new Button(" Color... ");
    
    ColorPanel(String title, Color placeholderColor, Color color)
    {
      this.setBorder(BorderFactory.createTitledBorder(title));
      this.placeholderColor = placeholderColor;
      this.colorSwatch = new Swatch(color);
      
      button.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          onColorButton();
        }
      });

      setLayout(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(0, 5, 5, 0);
      
      c.gridwidth = 1;
      c.anchor = GridBagConstraints.LINE_START;
      c.ipadx = 10;
      add(new Swatch(placeholderColor), c);
      
      c.gridwidth = GridBagConstraints.REMAINDER;
      add(new JLabel("  Placeholder"), c);
      
      c.gridwidth = GridBagConstraints.REMAINDER;           
      add(new Panel(), c);  // row spacer
      
      c.gridwidth = 2;
      c.ipadx = 60;
      c.ipady = 20;
      add(colorSwatch, c);
      
      c.gridwidth = GridBagConstraints.REMAINDER; 
      c.anchor = GridBagConstraints.CENTER;
      c.ipadx = c.ipady = 0;
      add(button, c);
    }
    
    Color getColor()
    {
      return colorSwatch.color;
    }
    
    private class Swatch extends java.awt.Label
    {
      Color color;
      Swatch(Color c)
      {
        color = c;
      }
      
      public void paint(Graphics g)
      {
        g.setColor(color);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
      }
    }
    
    void onColorButton()
    {
      colorChooser.setColor(getColor());
      
      JDialog colorDialog = JColorChooser.createDialog(this, "Fill Color", true, colorChooser, 
        new ActionListener() { public void actionPerformed(ActionEvent e) {   // OK listener 
          colorSwatch.color = colorChooser.getColor();
          colorSwatch.repaint(); 
          }},
        new ActionListener() { public void actionPerformed(ActionEvent e) {   // Cancel listener
        }});
      
      colorDialog.setVisible(true);
      colorDialog.dispose();
    }
  } // end of private ColorPanel class
  
  private class GraphicPanel extends JPanel
  {
    private Button button;
    private Thumb imageThumb;
    private Label imageLabel;
    private Label placeholderLabel;

    GraphicPanel(String title, String placeholderImagename, String imagename)
    {
      this.imageThumb = new Thumb(imagename, 72);
      this.button = new Button("Browse...");
      
      button.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          onBrowseButton();
        }
      });
      
      Thumb placeholderThumb = new Thumb(placeholderImagename, 36);
      placeholderLabel = new Label(placeholderImagename.substring(0, placeholderImagename.lastIndexOf('.')));
      imageLabel = new Label(imagename);

      this.setBorder(BorderFactory.createTitledBorder(title));
      
      setLayout(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(0, 5, 5, 5);
      
      c.gridwidth = 1;
      c.anchor = GridBagConstraints.NORTHWEST;
      add(placeholderThumb, c);
      c.gridwidth = GridBagConstraints.REMAINDER;
      c.anchor = GridBagConstraints.CENTER;
      add(placeholderLabel, c);
      
      c.gridwidth = 2;
      c.anchor = GridBagConstraints.SOUTHWEST;
      add(this.imageThumb, c);
      
      c.gridwidth = GridBagConstraints.REMAINDER; 
      c.anchor = GridBagConstraints.CENTER;
      c.fill = GridBagConstraints.NONE;
      JPanel panel = new JPanel(new GridBagLayout());
      c.insets = new Insets(5, 0, 5, 0);
      panel.add(imageLabel, c);
      panel.add(button, c);
      add(panel, c);
    }
    
    String getImagename()
    {
      return imageThumb.aImage.getSource();
    }
    
    private class Thumb extends JLabel
    {
      AveryImage aImage;
      private int size;
      
      Thumb(String filename, int size)
      {
        this.size = size;
        generate(filename);
      }
      
      void generate(String filename)
      {
        aImage = new AveryImage();
        aImage.setSourceAndGallery(filename, Config.getDefaultImageGallery());
        Image image = new RightRes().getImage(aImage, size, size);

        BufferedImage centeredImage = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
        Graphics gr = centeredImage.getGraphics();
        gr.setColor(new Color(0,0,0,0));  // transparent
        gr.fillRect(0, 0, size-1, size-1);
        int x = (size - image.getWidth(null)) / 2;
        int y = (size - image.getHeight(null)) / 2;
        gr.drawImage(image, x, y, null);
        gr.dispose();
        gr = null;

        setIcon(new ImageIcon(centeredImage));
      }
    }
    
    void onBrowseButton()
    {
      class imageFileFilter extends javax.swing.filechooser.FileFilter
      {
        public boolean accept(File f) { return (f.getName().endsWith(".jpg") || f.getName().endsWith(".png")); }
        public String getDescription() {  return "Image files (*.jpg, *.png)"; }
      }

      JFileChooser dlg = new JFileChooser(Config.getDefaultImageGallery() + "/HighRes");
      dlg.setFileFilter(new imageFileFilter());
      dlg.setFileSelectionMode(JFileChooser.FILES_ONLY);
      dlg.setDialogTitle("Select " + placeholderLabel.getText() + " Replacement Image");
      dlg.setAcceptAllFileFilterUsed(false);
      
      ImageAccessory acc = new ImageAccessory(dlg);
      dlg.setAccessory(new ImageAccessory(dlg));
      dlg.addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, acc);

      if (dlg.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
      {
        String filename = dlg.getSelectedFile().getName();
        imageLabel.setText(filename);
        imageThumb.generate(filename);
      }
    }
  } // end of private GraphicPanel class
}
