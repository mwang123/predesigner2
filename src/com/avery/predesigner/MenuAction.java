package com.avery.predesigner;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

public class MenuAction extends AbstractAction
{

	public MenuAction(String text, Icon icon)
	{
		super(text, icon);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		//System.out.println(e.getActionCommand());
		
		if (Predesigner.getProject() == null)
			return;
		
		Predesigner.getFrame().clearSelection();
		
		switch (e.getActionCommand())
		{
			case "Text Block":
				Predesigner.getFrame().getAddMenu().addTextBlockMenuItem.doClick();
				break;
				
			//case "Text Line":
				//Predesigner.getFrame().getAddMenu().addTextLineMenuItem.doClick();
				//break;

			case "Text Curve":
				Predesigner.getFrame().getAddMenu().addCurvedTextMenuItem.doClick();
				break;

			case "Image":
				Predesigner.getFrame().getAddMenu().addImageMenuItem.doClick();
				break;
			
			case "Background":
				Predesigner.getFrame().getAddMenu().addBackgroundMenuItem.doClick();
				break;
				
			case "Shape":
				Predesigner.getFrame().getAddMenu().addShapeMenuItem.doClick();
				break;
				
			case "Barcode":
				Predesigner.getFrame().getAddMenu().addBarcodeMenuItem.doClick();
				break;

			case "ZOrder":
				Predesigner.getFrame().getFieldsMenu().zOrderMenuItem.doClick();
				break;
		}
	}
}
