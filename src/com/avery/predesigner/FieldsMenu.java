package com.avery.predesigner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import com.avery.project.AveryBackground;
import com.avery.project.AveryPanelField;
import com.avery.project.AverySuperpanel;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: ApeFrames "Fields" menu</p>
 * <p>Copyright: Copyright 2003-2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2
 */

class FieldsMenu extends JMenu
{
	private static final long serialVersionUID = -835078968734573047L;
	private JMenuItem deleteMenuItem = new JMenuItem();
  public JMenuItem zOrderMenuItem = new JMenuItem();
  //private JMenuItem promptOrderMenuItem = new JMenuItem();
  private AveryPanelField selectedField = null;
  
  FieldsMenu()
  {
    // this is CTRL on PCs.
    int shortcutMask = java.awt.Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    setText("Fields");

    deleteMenuItem.setText("Delete...");
    deleteMenuItem.setMnemonic('D');
    deleteMenuItem.setAccelerator(KeyStroke.getKeyStroke('D', shortcutMask, false));
    deleteMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFieldsDelete();
      }
    });
    
    zOrderMenuItem.setText("Z Order...");
    zOrderMenuItem.setMnemonic('Z');
    zOrderMenuItem.setAccelerator(KeyStroke.getKeyStroke('9', shortcutMask, false));
    zOrderMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onZOrder();
      }
    });

    /*promptOrderMenuItem.setText("Prompt Order...");
    promptOrderMenuItem.setMnemonic('P');
    promptOrderMenuItem.setAccelerator(KeyStroke.getKeyStroke('0', shortcutMask, false));
    promptOrderMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onPromptOrder();
      }
    });*/

    addMenuListener(new AddMenuListener());
  }

  public void setSelectedField(AveryPanelField selectedField)
  {   
  	this.selectedField = selectedField;
  }
  
  class AddMenuListener implements MenuListener
  {
    public void menuCanceled(MenuEvent event)
    {   }
    public void menuDeselected(MenuEvent event)
    {   }
    
    /**
     * Constructs the menu with an edit item for each field of the current 
     * masterpanel, plus some other commands. 
     * @param event - standard argument, not used
     */
    public void menuSelected(MenuEvent event)
    {
      removeAll();
      int numberOfFields = 0;

      if (Predesigner.getFrame().allowMasterpanelEditing())
      {
        Iterator iterator = Predesigner.getFrame().getCurrentMasterpanel().getFieldIterator();
  
        while (iterator.hasNext())
        {
          AveryPanelField panelField = (AveryPanelField)iterator.next();
          FieldMenuItem menuItem = new FieldMenuItem(panelField, Predesigner.getFrame());
          add(menuItem);
          ++numberOfFields;
        }
        
        if (numberOfFields > 0)
        {
          addSeparator();
        }
      }
      
      deleteMenuItem.setEnabled(numberOfFields > 0);
      add(deleteMenuItem);
      
      zOrderMenuItem.setEnabled(numberOfFields > 1);
      add(zOrderMenuItem);

      //promptOrderMenuItem.setEnabled(numberOfFields > 1);
      //add(promptOrderMenuItem);
    }
  }

  /**
   * Called when the user selects the Fields|Delete... menu command
   */
  private void onFieldsDelete()
  {
    AverySuperpanel masterPanel = Predesigner.getFrame().getCurrentMasterpanel();

    FieldDeletePanel ui = new FieldDeletePanel(masterPanel);

    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), ui, "Delete Fields",
      JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
      == JOptionPane.OK_OPTION)
    {
      java.util.Iterator iterator = ui.getDeleteList().iterator();
      while (iterator.hasNext())
      {
        masterPanel.removePanelField((String)iterator.next());
      }

      Predesigner.getFrame().onFieldChanged();
    }
  }
  
  void onPromptOrder()
  {
    try 
    {
      OrderFieldPrompts ui = new OrderFieldPrompts();
      ui.fillListModel(Predesigner.getFrame().getCurrentMasterpanel().getPromptSortedPanelFields());
  
      if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), ui, "Change Prompt Order",
        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
        == JOptionPane.OK_OPTION)
      {
        Object[] fields = ui.getReorderedList();
        //for (int i = 0; i < fields.length; ++i)
        for (int i = fields.length - 1; i >= 0 ; i--)
        {
          ((AveryPanelField)(fields[i])).setPromptOrder(Integer.toString(i + 1));
        }
        
        Predesigner.getFrame().onFieldChanged();
      }
    }
    catch (Exception e) 
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
  
  /**
   * Called when the user selects the Fields/Z Order... menu command.  Note that
   * background fields are exempt from reordering.
   */
  void onZOrder()
  {
    try 
    {
			AveryBackground background = null;			
			ArrayList sourceFields = new ArrayList();
    	Iterator iterator = Predesigner.getFrame().getCurrentMasterpanel().getZLevelIterator();
    	
    	while (iterator.hasNext())
    	{
    		AveryPanelField field = (AveryPanelField)iterator.next();
    		if (field instanceof AveryBackground && background == null)
    		{
    			background = (AveryBackground)field;
    		}
    		else
    		{
    			sourceFields.add(field);
    		}
    	}
 
      OrderFieldZ ui = new OrderFieldZ(sourceFields, selectedField);
      
      if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), ui, "Change Z-Order (drawing order)",
        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
        == JOptionPane.OK_OPTION)
      {
      	// guarantee that the background has ZLevel = 1.0
      	if (background != null)
      	{
      		background.setZLevel(1.0);
      	}
      	
        Object[] fields = ui.getReorderedList();
        
        // 1.0 is taken by background, whether it exists or not
        int nextLevel = 2;
        int nextPromptLevel = 2;
        
        //for (int i = 0; i < fields.length; ++i)
        for (int i = fields.length - 1; i >= 0 ; i--)
        {
          AveryPanelField field = (AveryPanelField)fields[i];
          if (field.equals(ui.foregroundField))
          {
            field.setZLevel(OrderFieldZ.FOREGROUND_Z);
            field.setZLocked(true);
          }
          else
          {
            field.setZLevel(nextLevel++);
            field.setZLocked(false);
          }
          field.setPromptOrder(Integer.toString(nextPromptLevel++));
        }
        
        Predesigner.getFrame().onFieldChanged();
      }
    }
    catch (Exception e) 
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
}