/*
 * FieldCutAction.java Created on May 16, 2005 by Bob Lee
 * Copyright 2005 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

/**
 * @author Bob Lee
 *
 */
class FieldCutAction extends UndoableAction
{
  private AveryMasterpanel panel;
  private AveryPanelField field;
  private Object oldHold;
  
	public FieldCutAction(AveryMasterpanel panel, AveryPanelField field)
	{
		super("Undo Cut");
    this.panel = panel;
    this.field = field;
    oldHold = Predesigner.getFrame().getHeldObject();
	}

	/**
	 * @see com.avery.predesigner.UndoableAction#execute()
	 */
	void execute()
	{
    panel.removePanelField(field.getPrompt());
    // place on "clipboard"
    Predesigner.getFrame().setHeldObject(field);
    Predesigner.getFrame().updateView();
	}

	/**
	 * @see com.avery.predesigner.UndoableAction#undo()
	 */
	void undo()
	{
  	Predesigner.getProject().addFieldToMasterpanel(field, panel);
  	// repair "clipboard"
  	Predesigner.getFrame().setHeldObject(oldHold);
    Predesigner.getFrame().updateView();
	}
}
