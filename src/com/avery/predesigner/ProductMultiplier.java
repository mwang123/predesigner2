/*
 * ProductMultiplier.java Created on Jan 14, 2010 by leeb
 * Copyright 2010 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jdom.JDOMException;

import com.avery.predesign.AutoTemplate;
import com.avery.product.ProductGroup;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPage;

/**
 * @author leeb
 * Jan 14, 2010
 * ProductMultiplier
 */
class ProductMultiplier
{
  // hold these variables for duration of session
  static private String inputFolder;
  static private String outputFolder;
  
  static void multiply()
  throws IOException, JDOMException
  {
    SelectorPanel selector = new ProductMultiplier.SelectorPanel(
      inputFolder == null ? Config.getProjectHome() : inputFolder, 
      outputFolder == null ? Config.getProjectHome() : outputFolder ); 
    
    selector.autoComplete();
    
    if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), selector, 
        "Select Products", JOptionPane.OK_CANCEL_OPTION, 
        JOptionPane.PLAIN_MESSAGE) != JOptionPane.OK_OPTION)
    {
      return;
    }
    
    ProductGroup inputGroup = selector.getSourceProductGroup();    
    inputFolder = selector.inputFolderPanel.getDirectory();
    
    ProductGroup outputGroup = selector.getTargetProductGroup();
    outputFolder = selector.outputFolderPanel.getDirectory();
    
    // TODO: validate user inputs
    
    boolean generatePreviews = false;
    
    int yesNo = JOptionPane.showConfirmDialog(
        null,
        "Would you like to generate page previews?",
        "Generate Page Previews",
        JOptionPane.YES_NO_OPTION);

    if (yesNo == JOptionPane.YES_OPTION)
    {
    	generatePreviews = true;
    }
    File batFile = new File(Config.getBatDirectory(), outputGroup.getBatName());
    
    MultiplierDialog dlg = new MultiplierDialog("Product Predesign Generator");    
    dlg.logComment("Started " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
    dlg.logComment("Using ProductGroup " + inputGroup.getName() + " files from " + inputFolder);
    dlg.logComment("Making ProductGroup " + outputGroup.getName() + " files in " + outputFolder);
    dlg.setVisible(true);
    dlg.makeBusy(true);
    
    File[] files = new File(inputFolder).listFiles();
    
    
    for (int i = 0; i < files.length; ++i)
    {
      dlg.makeBusy(true);   // guarantee busy cursor
      
      File file = files[i];
      String filename = file.getName();
      if (filename.endsWith(".xml") && filename.indexOf(inputGroup.getName()) >= 0)
      {
        try
        {
          Project source = new Project();
          source.read(file);
          if (!inputGroup.getName().equals(source.getProductGroup()))
          {
            dlg.logError("misleading ProductGroup in filename", file);
            continue;
          }
          
          Project target = new Project();
          target.read(batFile);
          
          // upgrade BAT to template
          target.setSku(outputGroup.getBaseSku());
          target.setProductGroup(outputGroup.getName());
          // apply paperImage, color, etc. from ProductGroup
          target.applyAppearances(outputGroup, new File(Config.getBatDirectory() + "/papers").getAbsolutePath(), false);
          
          // copy project-level attributes from source
          target.setLanguage(source.getLanguage());
          target.setCopyright(source.getCopyright());
          target.setDescription(source.getDescription());
          target.setDesignTheme(source.getDesignTheme());
          target.setDesignCategory(source.getDesignCategory());

          // do the Autotemplate magic
          int masters = Math.min(source.getMasterpanels().size(), target.getMasterpanels().size());
          for (int n=0; n < masters; ++n)
          {
            AutoTemplate autoTemplate = new AutoTemplate((AveryMasterpanel)source.getMasterpanels().get(n));
            autoTemplate.applyTo(target, (AveryMasterpanel)target.getMasterpanels().get(n));
          }
          
          target.fixGalleries(inputFolder + "/HighRes");
          
          File output = ToolsMenu.autoFile(target, outputGroup.getDescription(), outputFolder);

          // always make page previews to ensure text font size fits box
        	int j = 1;
        	Iterator pageIterator = target.getPages().iterator();
        	while (pageIterator.hasNext())
        	{
      			AveryPage page = (AveryPage)pageIterator.next();
      			int pageWidth = (int)(page.getWidth().doubleValue() * .05);
      			BufferedImage image = target.makePagePreview(j++, pageWidth);
        		if (generatePreviews)
        		{
        			String outfilename = output.getName();
        			String pgNumberExtension = ".pg" + String.valueOf(j - 1) + ".png";
        			String pngFileName = outfilename.replace(".xml", pgNumberExtension);
        			ImageIO.write((RenderedImage)image,"png", new File(outputFolder + "/" + pngFileName));
        		}
        	}
        	
          target.write(output);
          
          // log results
          dlg.logProject(output);
        }
        catch (Exception ex)
        {
          // log error or any I/O exception
          dlg.logError(ex.toString(), file);
          ex.printStackTrace();
        }
      }
    } // end files[] loop
    
    dlg.logComment("Finished " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
    dlg.makeBusy(false);
  }
  
  // JPanel for user input dialog
  static private class SelectorPanel extends JPanel
  {   
    JComboBox sourceProductGroupComboBox = new JComboBox();
    DirectoryPanel inputFolderPanel;
    JComboBox targetProductGroupComboBox = new JComboBox();
    DirectoryPanel outputFolderPanel;
    
    SelectorPanel(String inputs, String outputs)
    throws JDOMException, IOException
    {        
      inputFolderPanel = new DirectoryPanel("Input Folder:", inputs);
      outputFolderPanel = new DirectoryPanel("Output Folder:", outputs);
      
      Iterator iterator = Predesigner.getProductGroupList().iterator();
      while (iterator.hasNext())
      {
        ProductGroup productGroup = (ProductGroup)iterator.next();
        sourceProductGroupComboBox.addItem(productGroup);
        targetProductGroupComboBox.addItem(productGroup);
      }
     
      setLayout(new GridBagLayout());
      GridBagConstraints constraints = new GridBagConstraints();
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      add(new JLabel("Use the design content from one product's predesigns" +
                     " to generate new predesigns for a different product."), constraints);
      constraints.fill = GridBagConstraints.HORIZONTAL;
      constraints.insets = new Insets(10, 10, 0, 10);
      constraints.gridwidth = 1;     
      add(new JLabel("Use all predesigns from :"), constraints);
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      add(sourceProductGroupComboBox, constraints);
      add(inputFolderPanel, constraints);
      constraints.gridwidth = 1;
      add(new JLabel("Generate predesigns for :"), constraints);
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      add(targetProductGroupComboBox, constraints);
      add(outputFolderPanel, constraints);
    }
    
    public void autoComplete()
    {
      AutoCompletion.enable(sourceProductGroupComboBox);
      AutoCompletion.enable(targetProductGroupComboBox);   	
    }
    
    ProductGroup getSourceProductGroup()
    {
      return (ProductGroup)sourceProductGroupComboBox.getSelectedItem();
    }
    
    ProductGroup getTargetProductGroup()
    {
      return (ProductGroup)targetProductGroupComboBox.getSelectedItem();
    }
  }
}
  


