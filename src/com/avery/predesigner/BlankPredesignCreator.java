/*
 * BlankCreatorDialog.java Created on Oct 10, 2008 by leeb
 * Copyright 2008 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.avery.Averysoft;
import com.avery.product.ProductGroup;
import com.avery.product.ProductGroupList;
import com.avery.project.Descriptions;

/**
 * @author leeb
 * Oct 10, 2008
 * BlankCreatorDialog
 */
public class BlankPredesignCreator extends JPanel
{
  JComboBox languageCombo = new JComboBox();
  ConfigFileBrowsePanel productGroupsPanel = 
    new ConfigFileBrowsePanel("Product Group List:", Config.productGroupListFile);
  DirectoryPanel batFolderPanel =
    new DirectoryPanel("BAT Folder:", Config.getBatDirectory());
  DirectoryPanel outputFolderPanel =
    new DirectoryPanel("Output Folder:", Config.getProjectHome());

  BlankPredesignCreator()
  throws JDOMException, IOException
  {
    initLanguageCombo();
    
    GridBagLayout gridbag = new GridBagLayout();
    setLayout(gridbag);
    GridBagConstraints c = new GridBagConstraints();
    c.gridwidth = GridBagConstraints.REMAINDER;

    add(new JLabel("Generate \"blank\" predesigns from the following inputs:"), c);

    c.fill = GridBagConstraints.HORIZONTAL;
   
    c.insets = new Insets(10, 10, 0, 10);
    c.gridwidth = 1;     
    add(new JLabel("Language:"), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    add(languageCombo, c);
    
    c.insets = new Insets(10, 10, 0, 10);
    c.gridwidth = GridBagConstraints.REMAINDER;
    add(productGroupsPanel, c);
    add(batFolderPanel, c);
    add(outputFolderPanel, c);
  }
  
  private void initLanguageCombo()
  throws JDOMException, IOException
  {
    SampleText sampleText = new SampleText();   // can throw exceptions
    
    List sampleTextLanguages = sampleText.getSupportedLanguages();
    Enumeration languages = Descriptions.getSupportedLanguages();
    while (languages.hasMoreElements())
    {
      String language = (String)languages.nextElement();
      if (sampleTextLanguages.contains(language))
      {
        languageCombo.addItem(language);
      }
    }
  }
  
  String getLanguage()
  {
    return (String)languageCombo.getSelectedItem();
  }
  
  File getProductGroupListFile()
  {
    return productGroupsPanel.getFile();
  }
  
  File getBatFolder()
  {
    return new File(batFolderPanel.textField.getText());
  } 
  
  File getOutputFolder()
  {
    return new File(outputFolderPanel.textField.getText());
  }
  
  void makeBlankPredesigns()
  throws JDOMException, IOException
  {
    String language = getLanguage();
    File groupsFile = getProductGroupListFile();
    File inputFolder = getBatFolder();
    File outputFolder = getOutputFolder();
    String papersDir = new File(Config.getBatDirectory() + "/papers").getAbsolutePath();
    
    MultiplierDialog dlg = new MultiplierDialog("Localized Predesign Generator"); 

    dlg.logComment("Started " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
    dlg.setVisible(true);
    dlg.makeBusy(true);
    
    SAXBuilder builder = new SAXBuilder(true);
    builder.setFeature("http://apache.org/xml/features/validation/schema", true);
    builder.setProperty(
        "http://apache.org/xml/properties/schema/external-schemaLocation",
        Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 

    ProductGroupList pgList = new ProductGroupList(builder.build(groupsFile).getRootElement());
    
    dlg.logComment("Creating blank predesigns for ProductGroupList " + groupsFile.getName());
          
    Iterator groups = pgList.iterator();
    while (groups.hasNext())
    {
      dlg.makeBusy(true);   // guarantee busy cursor
      
      ProductGroup group = (ProductGroup)groups.next();
      
      File bat = new File(inputFolder, group.getBatName());
      if (!bat.exists())
      {
        dlg.logComment("Couldn't find BAT " + bat.getName() + " for productGroup " + group.getName());
        continue;
      }
      try
      {
        Project project = new Project();
        project.read(bat);

        // convert to a bonafide predesign
        project.setLanguage(language);
        project.setSku(group.getBaseSku());
        project.setProductGroup(group.getName());
        project.setDesignTheme("Blank");
        // apply paperImage, color, etc. from ProductGroup
        project.applyAppearances(group, papersDir, false);

        File output = new File(outputFolder, language + "." + group.getName() + ".xml");
        project.write(output);       
        // log results
        dlg.logProject(output);
      }
      catch (Exception ex)
      {
        dlg.logComment(ex.getMessage());
      }
    }
    
    dlg.logComment("Finished " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
    dlg.makeBusy(false);
  }
}
