package com.avery.predesigner;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.filechooser.FileFilter;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.KeyStroke;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.jdom.input.SAXBuilder;

import com.avery.Averysoft;
import com.avery.predesign.AutoTemplate;
import com.avery.product.Appearance;
import com.avery.product.ProductGroup;
import com.avery.product.ProductGroupList;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanelField;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: The File menu, connected to Frame</p>
 * <p>Copyright: Copyright 2014 Avery Products Corporation, all rights reserved</p>
 * <p>Company: Avery Products Corporation</p>
 * @author Bob Lee
 * @version 2
 */

class FileMenu extends JMenu
{
	private static final String version = "2.0.0"; // note change to this when updated pdl links to dpo 7 "2.1.0";
	private static final long serialVersionUID = -7659649195859601789L;
	private JMenuItem newMenuItem = new JMenuItem("New...");
  private JMenuItem openMenuItem = new JMenuItem("Open Project...");
  private JMenuItem openStencilsMenuItem = new JMenuItem("Open Stencil...");
  private JMenuItem saveMenuItem = new JMenuItem("Save");
  private JMenuItem saveAsMenuItem = new JMenuItem("Save As...");
  private JMenuItem printMenuItem = new JMenuItem("Print...");
  private JMenuItem printPDFMenuItem = new JMenuItem("Print PDF");
  private JMenuItem generatePDFNoOutlinesMenuItem = new JMenuItem("No Outlines");
  private JMenuItem generatePDFPanelOutlinesMenuItem = new JMenuItem("Panel Outlines");
  private JMenuItem generatePDFFieldOutlinesMenuItem = new JMenuItem("Field Outlines");
  private JMenuItem exportDotAveryMenuItem = new JMenuItem("Avery Bundle...");
  private JMenuItem exportDotAveryDPOMenuItem = new JMenuItem("Launch ADPO...");
  private JMenuItem exportMasterMenuItem = new JMenuItem("Masterpanel...");
  private JMenuItem importMasterMenuItem = new JMenuItem("Masterpanel Fields...");
  private JMenuItem importDotAveryMenuItem = new JMenuItem("Avery Bundle...");
  private JMenuItem createAutoTemplateMenuItem = new JMenuItem("AutoTemplate...");
  private JMenuItem applyAutoTemplateMenuItem = new JMenuItem("AutoTemplate...");
  private JMenuItem configurationMenuItem = new JMenuItem("Configure...");
  private JMenuItem exitMenuItem = new JMenuItem("Exit");
  private JMenu generatePDFMenu = new JMenu("PDF");
  private JMenu exportMenu = new JMenu("Export");
  private JMenu importMenu = new JMenu("Import");

  private boolean bOpenedProject = true;
  
  public FileMenu()
  {
    // this is CTRL on PCs.
    int shortcutMask = java.awt.Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

    newMenuItem.setEnabled(true);
    newMenuItem.setMnemonic('N');
    newMenuItem.setAccelerator(KeyStroke.getKeyStroke('N', shortcutMask, false));
    newMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileNew();
      }});
    
    openMenuItem.setEnabled(true);
    openMenuItem.setMnemonic('O');
    openMenuItem.setAccelerator(KeyStroke.getKeyStroke('O', shortcutMask, false));
    openMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileOpen();
      }});

    openStencilsMenuItem.setEnabled(true);
    openStencilsMenuItem.setMnemonic('K');
    openStencilsMenuItem.setAccelerator(KeyStroke.getKeyStroke('K', shortcutMask, false));
    openStencilsMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileOpenStencils();
      }});

    saveMenuItem.setMnemonic('S');
    saveMenuItem.setAccelerator(KeyStroke.getKeyStroke('S', shortcutMask, false));
    saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileSave();
      }});

    saveAsMenuItem.setMnemonic('A');
    saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileSaveAs();
      }});
    
    printMenuItem.setMnemonic('P');
    printMenuItem.setAccelerator(KeyStroke.getKeyStroke('P', shortcutMask, false));
    printMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onPrint();
      }
    });
    
    printPDFMenuItem.setMnemonic('F');
    printPDFMenuItem.setAccelerator(KeyStroke.getKeyStroke('F', shortcutMask, false));
    printPDFMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onPrintPDF();
      }
    });

    generatePDFNoOutlinesMenuItem.setMnemonic('G');
    generatePDFNoOutlinesMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileGeneratePDF(false, false);
      }
    });

    generatePDFPanelOutlinesMenuItem.setMnemonic('L');
    generatePDFPanelOutlinesMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileGeneratePDF(true, false);
      }
    });

    generatePDFFieldOutlinesMenuItem.setMnemonic('D');
    generatePDFFieldOutlinesMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileGeneratePDF(false, true);
      }
    });
    
    exportDotAveryMenuItem.setMnemonic('V');
    exportDotAveryMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onExportDotAveryFile();
      }
    });
    
    //exportDotAveryDPOMenuItem.setMnemonic('V');
    exportDotAveryDPOMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onExportDotAveryDPO();
      }
    });


    exportMasterMenuItem.setMnemonic('M');
    exportMasterMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileExportMaster();
      }
    });

    importMasterMenuItem.setMnemonic('I');
    importMasterMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onFileImportMaster();
      }
    });

    importDotAveryMenuItem.setMnemonic('V');
    importDotAveryMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onImportDotAveryFile();
      }
    });

    createAutoTemplateMenuItem.setMnemonic('H');
    createAutoTemplateMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCreateAutoTemplate();
      }
    });
    
    applyAutoTemplateMenuItem.setMnemonic('U');
    applyAutoTemplateMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onImportAutoTemplate();
      }
    });

    configurationMenuItem.setEnabled(true);
    configurationMenuItem.setMnemonic('C');
    configurationMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Config.configure();
      }
    });

    exitMenuItem.setEnabled(true);
    exitMenuItem.setMnemonic('x');
    exitMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onFileExit();
      }
    });

    // fill popup menus
    generatePDFMenu.setMnemonic('F');
    generatePDFMenu.add(generatePDFNoOutlinesMenuItem);
    generatePDFMenu.add(generatePDFPanelOutlinesMenuItem);
    generatePDFMenu.add(generatePDFFieldOutlinesMenuItem);
    
    importMenu.setMnemonic('I');
    importMenu.add(applyAutoTemplateMenuItem);
    importMenu.add(importDotAveryMenuItem);
    importMenu.add(importMasterMenuItem);
    
    exportMenu.setMnemonic('E');
    exportMenu.add(createAutoTemplateMenuItem);
    exportMenu.add(exportDotAveryMenuItem);
    exportMenu.add(exportDotAveryDPOMenuItem);
    exportMenu.add(exportMasterMenuItem);
    
    // fill the menu
    setText("File");
    add(newMenuItem);
    add(openMenuItem);
    add(openStencilsMenuItem);
    add(saveMenuItem);
    add(saveAsMenuItem);
    addSeparator();
    addSeparator();
    add(printMenuItem);
    add(printPDFMenuItem);
    add(generatePDFMenu);
    addSeparator();
    add(importMenu);
    add(exportMenu);
    add(configurationMenuItem);
    addSeparator();
    add(exitMenuItem);

    addMenuListener(new FileMenuListener());
  }

  /**
   * MenuListener.menuSelected disables commands that are inappropriate for the
   * current state of the program.
   */
  class FileMenuListener implements MenuListener
  {
    public void menuCanceled(MenuEvent event)
    {   }
    public void menuDeselected(MenuEvent event)
    {   }
    public void menuSelected(MenuEvent event)
    {
      boolean hasProject = (Predesigner.getProject() != null);

      saveMenuItem.setEnabled(hasProject && getProjectFile() != null && getProjectFile().canWrite());
      saveAsMenuItem.setEnabled(hasProject);
      printMenuItem.setEnabled(hasProject);
      generatePDFNoOutlinesMenuItem.setEnabled(hasProject);
      generatePDFPanelOutlinesMenuItem.setEnabled(hasProject);
      generatePDFFieldOutlinesMenuItem.setEnabled(hasProject);
      exportDotAveryMenuItem.setEnabled(hasProject);
      exportMasterMenuItem.setEnabled(hasProject && Predesigner.getFrame().allowMasterpanelEditing());
      importMasterMenuItem.setEnabled(hasProject && Predesigner.getFrame().allowMasterpanelEditing());
      createAutoTemplateMenuItem.setEnabled(hasProject && Predesigner.getFrame().allowMasterpanelEditing());
      applyAutoTemplateMenuItem.setEnabled(hasProject && Predesigner.getFrame().allowMasterpanelEditing());

      if (recentFiles.size() != recentMenuItems.size())
      {
      	updateRecentFileMenuItems();
      }
    }
  }

  /**
   * a filechooser.FileFilter for Legacy XML files
   */
  class LegacyXmlFileFilter extends javax.swing.filechooser.FileFilter
  {
    public boolean accept(File f)   {
      return (f.isDirectory() || f.getName().endsWith(".xml"));
    }
    public String getDescription()    {
      return "Legacy Avery XML files (*.xml)";
    }
  }
  
  /**
   * a filechooser.FileFilter for Averysoft XML files
   */
  class AverysoftXmlFileFilter extends javax.swing.filechooser.FileFilter
  {
    public boolean accept(File f)   {
      return (f.isDirectory() || f.getName().endsWith(".xml"));
    }
    public String getDescription()    {
      return "Averysoft XML files (*.xml)";
    }
  }
  
  /**
   * a filechooser.FileFilter for Averysoft XML files
   */
  class DotAveryFileFilter extends javax.swing.filechooser.FileFilter
  {
    public boolean accept(File f)   {
      return (f.isDirectory() || f.getName().endsWith(".avery"));
    }
    public String getDescription()    {
      return "Avery project bundle (*.avery)";
    }
  }
  
  /**
   * a filechooser.FileFilter for XML files
   */
  class AnyXmlFileFilter extends javax.swing.filechooser.FileFilter
  {
    public boolean accept(File f)   {
      return (f.isDirectory() || f.getName().endsWith(".xml"));
    }
    public String getDescription()    {
      return "XML files (*.xml)";
    }
  }

  /**
   * a filechooser.FileFilter for PDF files
   */
  class AnyPdfFileFilter extends javax.swing.filechooser.FileFilter
  {
    public boolean accept(File f)   {
      return (f.isDirectory() || f.getName().endsWith(".pdf"));
    }
    public String getDescription()    {
      return "Averysoft XML files (*.pdf)";
    }
  }
  
  File getProjectFile()
  {
    return Predesigner.getProject().getFile();
  }

 /**
   * File | New... action performed
   */
  private void onFileNew()
  {
    Predesigner.getFrame().showPanelInfo(false);

    promptSaveModified();
    
  	try
		{
  		NewProjectDialog dlg = new NewProjectDialog(Predesigner.getFrame());
  		dlg.setVisible(true);
  		
  		switch (dlg.getExitStatus())
			{
  			case NewProjectDialog.EXIT_FILE:
  		    onFileOpen();
  		    if (Predesigner.getProject() != null)
  		    {
  		      // turn project into a template
  		    	Predesigner.getProject().templatize();
  		    }
  		    break;
  			
  			case NewProjectDialog.EXIT_OKAY:
  				ProductGroup group = dlg.getSelectedProductGroup();
					File batFile = new File(Config.getBatDirectory(), group.getBatName());
	        Project project = Predesigner.openProject(batFile);
  		    if (project != null)
  		    {
  		      // inform Predesigner that this is a template
  		      project.templatize();
  		      project.setSku(group.getBaseSku());
  		      project.setProductGroup(group.getName());
  					ProductGroupList list = Predesigner.getProductGroupList();
  					ProductGroup product = list.getProductGroup(project.getProductGroup());
  					String customProof = product.getCustomProof();
  					if (customProof != null && customProof.length() > 0)
  					{
  				    Iterator iMaster = project.getMasterpanels().iterator();
  				    while (iMaster.hasNext())
  				    {
  				    	AveryMasterpanel master = (AveryMasterpanel)iMaster.next();
  				    	master.setCustomProof(customProof);
  				    }				
  					}
  		      
  		      // apply paperImage, color, etc. from ProductGroup
  		      project.applyAppearances(group,	new File(Config.getBatDirectory() + "/papers").getAbsolutePath(), true);
            
            if (!Predesigner.styleSet.name.equals("placeholder"))
            {
              project.setDesignTheme(Predesigner.styleSet.name);
            }
            
            project.setModified(false);
  		    }
  				break;
  			
  			default:
  				break;
			}
		}
  	catch (Exception ex)
		{
  		System.err.print(ex.getMessage());
		}
  }
  
  
  /**
   * File | Open... action performed
   */
  private void onFileOpen()
  {
    Predesigner.getFrame().showPanelInfo(false);
    promptSaveModified();

    // get last-used project directory
    String sLastProjectsDirectory = Config.getProjectHome();

    if (sLastProjectsDirectory == null || sLastProjectsDirectory == "")
      sLastProjectsDirectory = getProjectFile().getParent();  // use default or previously used

    JFileChooser dlg = new JFileChooser(sLastProjectsDirectory);
    dlg.setFileFilter(new AnyXmlFileFilter());
    dlg.setDialogTitle("Select an Avery Project XML file");

    MasterpanelAccessory thumb = new MasterpanelAccessory(dlg);
    dlg.setAccessory(thumb);
    dlg.addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, thumb);

    if (dlg.showOpenDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
    	bOpenedProject = true;
    	File file = dlg.getSelectedFile();
      loadAveryProject(file);
      addToRecentFileList(file);
    }
  } // end onFileOpen

  private void onFileOpenStencils()
  {
    Predesigner.getFrame().showPanelInfo(false);
    promptSaveModified();

    // get last-used project directory
    String sLastStencilsDirectory = Config.getStencilsDirectory();

    if (sLastStencilsDirectory == null || sLastStencilsDirectory == "")
    	sLastStencilsDirectory = getProjectFile().getParent();  // use default or previously used

    JFileChooser dlg = new JFileChooser(sLastStencilsDirectory);
    dlg.setFileFilter(new AnyXmlFileFilter());
    dlg.setDialogTitle("Select an Avery Stencils XML file");

    MasterpanelAccessory thumb = new MasterpanelAccessory(dlg);
    dlg.setAccessory(thumb);
    dlg.addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, thumb);

    if (dlg.showOpenDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
    	bOpenedProject = false;
      File file = dlg.getSelectedFile();
      loadAveryProject(file);
      addToRecentFileList(file);
      if (Predesigner.getProject() != null)
      	Predesigner.getProject().resetProject();
    }
  } // end onFileOpenStencils

  /**
   * File | Save menu command
   */
  private void onFileSave()
  {
  	if ((Predesigner.getProject().isLegacyProject() == false)
  			&& abortForIncompleteMergeMap())
  	{
  		return;
  	}
  	
    // trap template saves
    if (projectIsUnsaved())
    {
      onFileSaveAs();
    }
    else try
    {
    	Predesigner.getProject().setVersion(version);
      Predesigner.getProject().save();
    }
    catch (Exception ex)
    {
      JOptionPane.showMessageDialog(Predesigner.getFrame(), "Couldn't save project: " + ex.getMessage(), "Save", JOptionPane.ERROR_MESSAGE);
      ex.printStackTrace();
    }
  } // end onFileSave

  // closely held saveAsDir
  static private File saveAsDir = null;

  static private File getSaveAsDir()
  {
      return saveAsDir;
  }

  static private void setSaveAsDir(File file)
  {
      saveAsDir = file;
  }

  /**
   * File | Save As... menu command
   */
  private void onFileSaveAs()
  {
  	Predesigner.getProject().setVersion(version);
  	
  	if (abortForIncompleteMergeMap())
  	{
  		return;
  	}
  	
    if (getSaveAsDir() == null)
    {
    	if (getProjectFile() == null || !bOpenedProject)
      {
        setSaveAsDir(new File(Config.getProjectHome()));
      }
    	else
      {
        setSaveAsDir(getProjectFile().getParentFile());
      }
      
    }
  	
    JFileChooser dlg = new JFileChooser(getSaveAsDir());
    
    FileFilter averysoft = new AverysoftXmlFileFilter();
    FileFilter legacy = new LegacyXmlFileFilter();
    FileFilter dotavery = new DotAveryFileFilter();
    dlg.addChoosableFileFilter(averysoft);

    dlg.addChoosableFileFilter(legacy);
    dlg.addChoosableFileFilter(dotavery);
    dlg.setFileFilter(Predesigner.getProject().isLegacyProject() ? legacy : averysoft);
    dlg.setDialogTitle("Save Project XML File");
    dlg.setApproveButtonText("Save");
    dlg.setSelectedFile(new File(getProposedXmlFilename(getSaveAsDir())));

    if (dlg.showSaveDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
      File file = dlg.getSelectedFile();
      if (file.exists())
      {
        if (file.canWrite())
        {
          if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), "Overwrite " + file.getName() + "?", "confirm", JOptionPane.YES_NO_OPTION)
            != JOptionPane.YES_OPTION)
          {
            onFileSaveAs();    // recurse
            return;
          }
        }
        else
        {
          JOptionPane.showMessageDialog(Predesigner.getFrame(), file.getName() + " is read-only!", "error", JOptionPane.ERROR_MESSAGE);
          onFileSaveAs();    // try again
          return;
        }
      }

      // save the document
      try
      {
        if (dlg.getFileFilter().equals(dotavery))
        {
          if (!file.getName().endsWith(".avery"))
          {
            file = new File(file.getAbsolutePath() + ".avery");
          }
          
          Predesigner.getProject().saveDotAvery(file);
        }
        else // save XML file
        {
          if (file.getName().toLowerCase().endsWith(".xml") == false)
          {
            file = new File(file.getAbsolutePath() + ".xml");
          }
          
        	if (dlg.getFileFilter().equals(legacy))
        	{
        		Predesigner.getProject().saveLegacy(file);
          }
        	else
        	{
        		Predesigner.getProject().saveAverysoft(file);
        	}
          
          addToRecentFileList(file);
          setSaveAsDir(file.getParentFile());
        }

        JOptionPane.showMessageDialog(Predesigner.getFrame(), "Project saved as " + file.getName(), "Saved", JOptionPane.INFORMATION_MESSAGE);
      }
      catch (Exception ex)
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), "Couldn't save project: " + ex.getMessage(), "Save As", JOptionPane.ERROR_MESSAGE);
        ex.printStackTrace();
      }
    }
  } // end onFileSaveAverysoftAs()
  
  void onPrint()
  {
    PrinterJob job = PrinterJob.getPrinterJob();
    job.setPrintable(Predesigner.getProject());
    if (job.printDialog()) 
    { try 
      {
        job.print();
      } 
      catch (PrinterException ex) 
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), ex.getStackTrace(), "Print Error", JOptionPane.ERROR_MESSAGE);
      }
    }    
  }

  void onPrintPDF()
  {
    JFileChooser dlg = new JFileChooser(pdfDir);
    dlg.setFileFilter(new AnyPdfFileFilter());
    dlg.setDialogTitle("Select a PDF file to print");
    
    Printing print = new Printing();
    JComboBox cb = new JComboBox();
    ArrayList printerList = print.enumeratePrinters(print.getDefaultPrinterName());
    
    Iterator i = printerList.iterator();
    while (i.hasNext())
    	cb.addItem(i.next());
    cb.setSize(new Dimension(80, 10));
    
    JPanel jPrintPanel = new JPanel();
    
    JLabel text = new JLabel();    
    text.setText("Select printer:");
    text.setAlignmentX(JLabel.LEFT_ALIGNMENT);
   
    JPanel spacePanel = new JPanel();
    spacePanel.setSize(new Dimension(100, 50));
    jPrintPanel.add(spacePanel);
    
    jPrintPanel.setLayout(new BoxLayout(jPrintPanel, BoxLayout.Y_AXIS));
    //jPrintPanel.setSize(new Dimension(110, 60));
    jPrintPanel.add(text);
    jPrintPanel.add(cb);
    jPrintPanel.add(spacePanel);
    
    Component[] comps = dlg.getComponents();
    JComponent nextComponent = null;
    for (int j = 0; j < comps.length; j++)
    {
    	JComponent jc = (JComponent)comps[j];
    	String compName = jc.getClass().toString();
     	//System.out.println("TC " + compName);
    	if (compName.indexOf("JPanel") > 0)
    	{
    		nextComponent = jc;
    		break;
    	}
    }
    comps = nextComponent.getComponents();
    for (int j = 0; j < comps.length; j++)
    {
    	JComponent jc = (JComponent)comps[j];
    	String compName = jc.getClass().toString();
     	//System.out.println("TC " + compName);
    	if (jc.getComponents().length == 5)
    	{
    		jc.add(jPrintPanel, 4);
    		break;
    	}
    }
    

    if (dlg.showOpenDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
	    File file = dlg.getSelectedFile();
	    String printerName = (String)cb.getSelectedItem();
	
	    try
	    {
	    	print.printPDF(file.getAbsolutePath(), "PDF", 1, printerName);
	    }
	    catch (IOException ioe)
	    {}
	    catch (PrinterException pe)
	    {}
	  }
  }

  // closely held pdfDir - where PDF files are saved to
  static private File pdfDir = null;
  static private File getPdfDir()
  {
      return pdfDir;
  }
  static private void setPdfDir(File file)
  {
      pdfDir = file;
  }

  /**
   * File | Generate PDF... action performed
   */
  private void onFileGeneratePDF(boolean outlinePanels, boolean outlineFields)
  {
    class PDFFileFilter extends javax.swing.filechooser.FileFilter
    {
      public boolean accept(File f) {
        return (f.isDirectory() || f.getName().endsWith(".pdf"));
      }
      public String getDescription() {
        return "PDF files (*.pdf)";
      }
    }

    // if pdfDir hasn't been set, provide a reasonable default
    if (getPdfDir() == null)
    {
    	if (getProjectFile() == null)
    	{
    		if (bOpenedProject)
    			setPdfDir(new File(Config.getProjectHome()));
    		else
    			setPdfDir(new File(Config.getStencilsDirectory()));
    	}
    	else
    	{
        setPdfDir(getProjectFile().getParentFile());   		
    	}
    }

    JFileChooser dlg = new JFileChooser(getPdfDir());
    dlg.setFileFilter(new PDFFileFilter());
    dlg.setSelectedFile(new File(getProposedPdfFilename()));
    dlg.setDialogTitle("Generate a PDF file");
    dlg.setApproveButtonText("Save");

    if (dlg.showSaveDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
      try
      {
        File file = dlg.getSelectedFile();
        if (file.getName().toLowerCase().endsWith(".pdf") == false)
        {
          file = new File(file.getAbsolutePath() + ".pdf");
        }
        
        if (file.exists())
        {
          if (file.canWrite())
          {
            if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), "Overwrite " + file.getName() + "?", "confirm", JOptionPane.YES_NO_OPTION)
              != JOptionPane.YES_OPTION)
            {
              setPdfDir(dlg.getSelectedFile().getParentFile());
              onFileGeneratePDF(outlinePanels, outlineFields);    // recurse
              return;
            }
          }
          else
          {
            JOptionPane.showMessageDialog(Predesigner.getFrame(), file.getName() + " is read-only!", "error", JOptionPane.ERROR_MESSAGE);
            setPdfDir(dlg.getSelectedFile().getParentFile());
            onFileGeneratePDF(outlinePanels, outlineFields);    // try again
            return;
          }
        }
        
        // include all fields, includeing those that are not marked as printable
        Averysoft.includeAllPanelFieldsInPDF = true;
        
        Project project = Predesigner.getProject();
        boolean outlines = (project.hasMasterpanelFields() == false);
               
        project.makePdfFile(file.getAbsolutePath(), Config.getFontDirectory(), outlinePanels, outlineFields);
        setPdfDir(dlg.getSelectedFile().getParentFile());
        
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
            file.getAbsolutePath(), "Adobe PDF File Created", JOptionPane.INFORMATION_MESSAGE);
      }
      catch (Exception ex)
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        ex.printStackTrace();
      }
    }
  } // end onFileGeneratePDF()
  
  /**
   * File | Export Masterpanel... menu command
   * <p>Prompt the user with a filename for the masterpanel in the ./masters
   * directory, then save it..</p>
   */
  private void onFileExportMaster()
  {
    JFileChooser dlg = new JFileChooser(Config.getMasterpanelsDir());
    dlg.setAcceptAllFileFilterUsed(false);
    dlg.setFileFilter(new AnyXmlFileFilter());
    dlg.setDialogTitle("Save AveryMasterpanel XML file");
    dlg.setApproveButtonText("Save");

    if (dlg.showSaveDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
      File file = dlg.getSelectedFile();
      if (file.exists())
      {
        if (file.canWrite())
        {
          if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), "Overwrite " + file.getName() + "?", "confirm", JOptionPane.YES_NO_OPTION)
            != JOptionPane.YES_OPTION)
          {
            // user dowsn't want to overwrite
            onFileExportMaster();    // recurse
            return;
          }
        }
        else  // file not writable
        {
          JOptionPane.showMessageDialog(Predesigner.getFrame(), file.getName() + " is read-only!", "error", JOptionPane.ERROR_MESSAGE);
          onFileExportMaster();    // try again
          return;
        }
      }

      if (file.getName().toLowerCase().endsWith(".xml") == false)
      {
        file = new File(file.getAbsolutePath() + ".xml");
      }

      try
      { // the action we've been waiting for
        Predesigner.getFrame().getCurrentMasterpanel().writeMasterpanelFile(file);
      }
      catch (Exception e)
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
          "Couldn't save masterpanel as " + file.getName() + "\n" + e.getMessage(),
          "Save Masterpanel", JOptionPane.ERROR_MESSAGE);
      }
    }
  } // end onFileExportMaster

  private void onFileImportMaster()
  {
  	JFileChooser dlg = new JFileChooser(Config.getMasterpanelsDir());
    dlg.setAcceptAllFileFilterUsed(false);
    dlg.setFileFilter(new AnyXmlFileFilter());
    dlg.setDialogTitle("Import Fields from Masterpanel File");
    dlg.setApproveButtonText("Import");
    
    MasterpanelAccessory thumb = new MasterpanelAccessory(dlg);
    dlg.setAccessory(thumb);
    dlg.addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, thumb);

    if (dlg.showOpenDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
      File file = dlg.getSelectedFile();
      try
      {
        SAXBuilder builder = new SAXBuilder(true);
        builder.setFeature("http://apache.org/xml/features/validation/schema", true);
        builder.setProperty(
            "http://apache.org/xml/properties/schema/external-schemaLocation",
            Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 
        
        org.jdom.Document document = builder.build(file);
        org.jdom.Element element =  document.getRootElement();

        // if it's not a masterpanel file, use the first masterpanel
        if (element.getName().equals("project"))
        {
          element = element.getChild("masterpanel");
        } 
        else if (element.getName().equals("Avery.project")) // legacy file
        {
          element = element.getChild("Avery.masterpanel");
        }

        AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
        int duplicates = 0;

        java.util.Iterator iterator = new AveryMasterpanel(element).getFieldIterator();
        while (iterator.hasNext())
        {
          AveryPanelField field = (AveryPanelField)iterator.next();
          if (master.usesFieldDescription(field.getDescription()))
          {
            ++duplicates;
          }
          else
          {
            Predesigner.getProject().addFieldToMasterpanel(field, master);
          }
        }

        // update the view
        Predesigner.getFrame().onFieldChanged();
        
        // report errors
        if (duplicates > 0)
        {
          String title, message;
          if (duplicates > 1)
          {
            title = "Duplicate Field Descriptions";
            message = "Note: " + duplicates + " fields were not imported.";
          }
          else
          {
            title = "Duplicate Field Description";
            message = "Note: one field was not imported.";
          }
          
          JOptionPane.showMessageDialog(Predesigner.getFrame(), message, title, JOptionPane.WARNING_MESSAGE);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        JOptionPane.showMessageDialog(Predesigner.getFrame(), ex.getMessage(), ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
      }
    }
  }  // end onFileImportMaster
  

  /**
   * File | Exit action performed
   */
  private void onFileExit()
  {
    Predesigner.getFrame().exitApe(0);
  }

  private final String sErrLoadingProject = "An error occured when trying to " +
    "load the project.  Either the project is not correct format or " +
    "Avery.DTD does not exist in the projects directory.\n";

  private boolean loadAveryProject(File file)
  {
    boolean success = false;
    try
    {
        Project project = Predesigner.openProject(file);
        if (project != null)
        {
          // Set the projectHome directory to whatever the user selected in file open dialog
        	if (bOpenedProject)
        		Config.setProjectHome(project.getHomeDirectory());

          // fixup image galleries
          int orphans = project.fixGalleries(Config.getDefaultImageGallery());

          if (orphans != 0 && new File(Config.getProjectHome(), "HighRes").exists())
          {
            orphans = project.fixGalleries(Config.getProjectHome());
          }

          if (orphans /* still */ != 0)
          {
            // warn about unfound images
            String text = "There are " + orphans + " unresolved image references in this project.";
						JOptionPane.showMessageDialog(Predesigner.getFrame(), text, "Error", JOptionPane.ERROR_MESSAGE);
          }

          // fixup paperimage references
          boolean foundImage = project.updatePapers(Config.getProjectHome() + "/papers");
          if (!foundImage)
          {
          	ProductGroup group = Predesigner.getProductGroupList().getProductGroup(project.getProductGroup());
  		      project.applyAppearances(group,	new File(Config.getBatDirectory() + "/papers").getAbsolutePath(), false);
          }
                  
          // open the designated style set
          Predesigner.styleSet = project.getStyleSet();
          success = true;
        }
        else // Error in loading project
        {
          System.err.println("Error in loading project: " + file.getAbsolutePath());
          Predesigner.getFrame().displayErrorMessage( sErrLoadingProject );
        }
    }
    catch (Exception ex)
    {
      Predesigner.getFrame().displayErrorMessage(ex.toString() + "\nFile: " + file.getAbsolutePath());
      ex.printStackTrace();
    }

    return success;
  }

  void promptSaveModified()
  {
    if (Predesigner.getProject() != null && Predesigner.getProject().isModified()
      && (JOptionPane.showConfirmDialog(Predesigner.getFrame(), "Save current project first?", "modified", JOptionPane.YES_NO_OPTION)
         == JOptionPane.YES_OPTION))
    {
      if (getProjectFile() != null && getProjectFile().canWrite())
      {
        onFileSave();
      }
      else
      {
      	onFileSaveAs();
      }
    }
  }

  /**
   * This embedded class is used to create MenuItems that open project XML files from the
   * recent files list
   */
  class RecentFileMenuItem extends JMenuItem
  {
		private static final long serialVersionUID = -3262214999239334921L;
		private File file;

  	RecentFileMenuItem(File newFile)
  	{
  	  file = newFile;
  	  setText(file.getName());

  	  addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent e) {
					onAction();
				}
			});
  	}

  	private void onAction()
  	{
  		// give them a chance to save or cancel
	  	if (Predesigner.getProject() != null && Predesigner.getProject().isModified())
	  	{
	    	switch (JOptionPane.showConfirmDialog(Predesigner.getFrame(), "Save current project first?", "modified", JOptionPane.YES_NO_CANCEL_OPTION))
	    	{
		    	case JOptionPane.YES_OPTION:
		      	if (getProjectFile().canWrite())
		      	{
							onFileSave();
		  	  	}
		  	  	else
		  	  	{
		  	  		onFileSaveAs();
		  	  	}
		  	  	break;
		  		case JOptionPane.CANCEL_OPTION:
		  	  	return;
		  	  case JOptionPane.NO_OPTION:
		  	  default:
		  	  	break;
	    	}
	  	}

	  	// open the file associated with this menu item
	  	loadAveryProject(file);

	  	// bump it to the top of the list
			addToRecentFileList(file);
  	}
  }

	// static data used for the list of recent project files
  static final int MAXRECENT = 5;
  private static java.util.LinkedList recentFiles = new java.util.LinkedList();
  private static java.util.LinkedList recentMenuItems = new java.util.LinkedList();

  /**
   * This is used to initialize or re-initialize the recentFiles list.
   * @param list
   */
  static void setRecentFileList(java.util.List list)
  {
  	recentFiles.clear();
  	java.util.Iterator it = list.iterator();
  	while (it.hasNext())
  	{
  		recentFiles.addLast(it.next());
  	}
  }

  static java.util.List getRecentFileList()
  {
  	return recentFiles;
  }

  private void addToRecentFileList(File file)
  {
  	if (recentFiles.contains(file))
  	{
  		recentFiles.remove(file);
  	}

  	recentFiles.addFirst(file);

  	while (recentFiles.size() > MAXRECENT)
  	{
  	  recentFiles.removeLast();
  	}

  	updateRecentFileMenuItems();
  }

  private void updateRecentFileMenuItems()
  {
  	java.util.Iterator it = recentMenuItems.iterator();
  	while (it.hasNext())
  	{
  	  remove((JMenuItem)it.next());
  	}
  	recentMenuItems.clear();

  	remove(exitMenuItem);  // we'll add it back later.

  	// take the opportunity to notice any files that have been deleted
  	ArrayList missing = new ArrayList();

  	// create a MenuItem for each valid File in the recentFiles list
  	it = recentFiles.iterator();
  	while (it.hasNext())
  	{
  	  File file = (File)it.next();
  	  if (file.exists())
  	  {
  	  	RecentFileMenuItem menuItem = new RecentFileMenuItem(file);
  	  	add(menuItem);
  	  	recentMenuItems.addLast(menuItem);
  	  }
  	  else missing.add(file);
  	}

  	if (missing.size() != 0)
  	{
  		// remove missing files from the list
  		it = missing.iterator();
  		while (it.hasNext())
  		{
  			recentFiles.remove(it.next());
  		}
  	}

  	add(exitMenuItem);   // exit is always at the bottom of the menu
  }
  
  private String getProposedXmlFilename(File directory)
  {
    return getProposedFilename(directory, ".xml");
  }
  
  /**
   * @param directory where the proposed file is expected to be created
   * @param suffix includes the dot
   * @return a filename proposal
   */
  private String getProposedFilename(File directory, String suffix)
  {
  	if (getProjectFile() != null)
  	{
  	  String s = getProjectFile().getName();
  	  if (!s.endsWith(suffix))
  	  {
  	    s = s.substring(0, s.lastIndexOf('.')) + suffix;
  	  }
  		return s;
  	}
  	
    String language = Predesigner.getProject().getLanguage() + ".";
		String groupName = Predesigner.getProject().getProductGroup();
    String description = ".noDescription";
    String date = new SimpleDateFormat(".yyMM-").format(new Date());
    
		try
		{	// look up product description in the ProductGroupList
			ProductGroupList list = Predesigner.getProductGroupList();
			Iterator iterator = list.iterator();
			while (iterator.hasNext())
			{
				ProductGroup group = (ProductGroup)iterator.next();
				if (group.getName().equals(groupName))
				{
          // remove chars that aren't alphanumeric from ProductGroup.description
	  			description = "." + group.getDescription().replaceAll("[\\W]", "");  			
					break;
				}
			}
		}
		catch (Exception ex) 	// ProductGroupList error  
    { ex.printStackTrace(); }
    
    String designTheme = Predesigner.getProject().getDesignTheme();
    String styleSet = (designTheme == null || "placeholder".equals(designTheme))
      ? "" : "." + designTheme.replaceAll("[\\W]", ""); 
    
    String proposedName = language + groupName + description + styleSet + date;
    // remove additional unwanted characters
    proposedName = proposedName.replaceAll("\\(\\)", "");
    proposedName = proposedName.replaceAll("\\[\\]", "");
    proposedName = proposedName.replaceAll("\\{\\}", "");
    proposedName = proposedName.replaceAll("<>", "");
    
    int i = 1;
    while (new File(directory, proposedName + twoDigits(i) + suffix).exists())
      ++i;
		
		return proposedName + twoDigits(i) + suffix;
  }
  
  private String getProposedPdfFilename()
  {
    String name;
    if (getProjectFile() != null)
    {
      name = getProjectFile().getName();
      name = name.substring(0, name.lastIndexOf('.'));
    }
    else  // no project file name yet
    {
      name = Predesigner.getProject().getProductGroup();
    }
    return name + ".pdf";
  }
  
  private boolean projectIsUnsaved()
  {
  	if (getProjectFile() == null)
  	{
  		return true;
  	}
  	return getProjectFile().getName().equals(Predesigner.getProject().getProductGroup());
  }
  
  /**
   * @return <code>true</code> if the mergeMaps were missing required lines
   * and the user choose to abort because of it.
   */
  private boolean abortForIncompleteMergeMap()
  {
  	Hashtable warnings = Predesigner.getProject().getIncompleteMergemapWarnings();
  	if (warnings.isEmpty())
  	{
  		return false;
  	}
  	else
  	{
  		String formattedWarning = 
  			"Masterpanel(s) are missing required MergeMap lines.\n";
  		
  		Enumeration masters = warnings.keys();
  		while (masters.hasMoreElements())
  		{
  			String master = (String)masters.nextElement();
  			formattedWarning += "\n" + master + ":";
  			Enumeration lines = ((Hashtable)warnings.get(master)).elements();
  			while (lines.hasMoreElements())
  			{
    			formattedWarning += "\n    " + lines.nextElement().toString(); 				
  			}
  			formattedWarning += "\n";
  		}
  		
  		formattedWarning += "\nSave anyway?";
  		
      return JOptionPane.showConfirmDialog(
      		Predesigner.getFrame(), formattedWarning, "Incomplete MergeMap Assignments", 
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)
          == JOptionPane.NO_OPTION;
  	}
  }
  
  private static File dotAveryDir;
  
  
  private void onExportDotAveryFile()
  {
    if (abortForIncompleteMergeMap())
    {
      return;
    }
    
    // if current project was imported from .avery,
    // then just save changes into the same file
    if(Predesigner.getProject() instanceof ProjectBundle)
    {
    	ProjectBundle bundle = (ProjectBundle)Predesigner.getProject();
    	try
    	{
        	bundle.save();
            JOptionPane.showMessageDialog(Predesigner.getFrame(), 
            				"Project saved as " + bundle.getName(), 
            				"Saved", 
            				JOptionPane.INFORMATION_MESSAGE);
    	}
		catch (Exception ex)
		{
		  JOptionPane.showMessageDialog(Predesigner.getFrame(), "Couldn't save project: " + ex.getMessage(), "Save As", JOptionPane.ERROR_MESSAGE);
		  ex.printStackTrace();
		}
      return;
    }
    
    // promptSaveModified();
    
    ProjectPropertiesDialog propsDialog = new ProjectPropertiesDialog(Predesigner.getFrame(), Predesigner.getProject());
    propsDialog.setVisible(true);
    if (propsDialog.okay)
    {
      Predesigner.getProject().setModified(true);
    }
    
    // if dotAveryDir hasn't been set, provide a reasonable default
    if (dotAveryDir == null)
    {
      dotAveryDir = new File(Config.getAveryBundleDir());      
    }
    
    JFileChooser dlg = new JFileChooser(dotAveryDir);
    
    FileFilter dotAveryFilter = new DotAveryFileFilter();
    dlg.addChoosableFileFilter(dotAveryFilter);
    dlg.setFileFilter(dotAveryFilter);
    dlg.setDialogTitle("Export a \'.Avery\' Bundle File");
    dlg.setApproveButtonText("Export");
    
    String proposedName = getProposedFilename(dotAveryDir, ".avery");
    String baseLayoutName = proposedName;
    if (proposedName.endsWith(".xml"))
    {
      proposedName = proposedName.substring(0, proposedName.lastIndexOf(".xml")) + ".avery";
    }
    else
    {
    	baseLayoutName = proposedName.substring(0, proposedName.lastIndexOf(".avery")) + ".xml";
    }
    
    dlg.setSelectedFile(new File(proposedName));    

    if (dlg.showSaveDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
      File file = dlg.getSelectedFile();
      if (!file.getName().endsWith(".avery"))
      {
        file = new File(file.getAbsolutePath() + ".avery");
      }
      
      if (file.exists())
      {
        if (file.canWrite())  // prompt to overwrite
        {
          if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), 
              "Overwrite " + file.getName() + "?", "confirm", JOptionPane.YES_NO_OPTION)
            != JOptionPane.YES_OPTION)
          {
            onExportDotAveryFile();    // recurse
            return;
          }
        }
        else
        {
          JOptionPane.showMessageDialog(Predesigner.getFrame(), 
              file.getName() + " is read-only!", "error", JOptionPane.ERROR_MESSAGE);
          
          onExportDotAveryFile();    // try again
          return;
        }
      }

      try   // do the thing
      { 
      	Project project = Predesigner.getProject();
      	if (project.getFile() != null && project.getName().length() > 0)
      	{
      		baseLayoutName = project.getName();
      	}
      		
      	project.setBaseLayout(baseLayoutName);
        Predesigner.getProject().saveDotAvery(file);
        JOptionPane.showMessageDialog(Predesigner.getFrame(), "Project saved as " + file.getName(), "Saved", JOptionPane.INFORMATION_MESSAGE);
        dotAveryDir = file.getParentFile();
        
        if (Config.panelThumbnailSize > 0)
        {
          String panelJpgName = file.getName();
          panelJpgName = panelJpgName.substring(0, panelJpgName.lastIndexOf('.')) + "_mp.jpg";
          Predesigner.getProject().saveMasterpanelThumbnail(new File(dotAveryDir, panelJpgName));
        }
        
        if (Config.pageThumbnailSize > 0)
        {
          String pageJpgName = file.getName();
          pageJpgName = pageJpgName.substring(0, pageJpgName.lastIndexOf('.')) + "_fp.jpg";
          Predesigner.getProject().savePageThumbnail(new File(dotAveryDir, pageJpgName));
        }
      }
      catch (Exception ex)
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(), "Couldn't save project: " + ex.getMessage(), "Save As", JOptionPane.ERROR_MESSAGE);
        ex.printStackTrace();
      }
    }
  } // end onExportDotAveryFile()
  
  private void onExportDotAveryDPO()
  {
		// get last-used project directory
		/*Project lastProject = Predesigner.getProject(); 
		String sLastProjectsDirectory = (null != lastProject) ? lastProject.getFileDirectory() : Config.getAveryBundleDir();
		
		if (sLastProjectsDirectory == null || sLastProjectsDirectory == "")
		  sLastProjectsDirectory = getProjectFile().getParent();  // use default or previously used
		
		JFileChooser dlg = new JFileChooser(sLastProjectsDirectory);
		dlg.setFileFilter(new DotAveryFileFilter());
		dlg.setDialogTitle("Select an Avery Project bundle to open in Design&Print");
		
		if (dlg.showOpenDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
		{
		  File file = dlg.getSelectedFile();*/
		  if(Desktop.isDesktopSupported())
		  {
		  	try
		  	{
		  		String uri = "http://www.avery.com/avery/en_us/Templates-%26-Software/Software/Avery-Design--Print-Online.htm?int_id=homepage-adpo";
		  		//String uri = "http://dpotest.print.avery.com/dpo7/app/US_en/createProject/?averyFileName=http://54.221.229.243/MergeTests/";
		  		//uri += file.getName();
		  		Desktop.getDesktop().browse(new URI(uri));
		  	}
		  	catch (Exception e)
		  	{}
		  //}
		}
  	
  } // end onExportDotAveryDPO()

  private void onImportDotAveryFile()
  {
		promptSaveModified();
		
		// get last-used project directory
		Project lastProject = Predesigner.getProject(); 
		String sLastProjectsDirectory = (null != lastProject) ? lastProject.getFileDirectory() : Config.getAveryBundleDir();
		
		if (sLastProjectsDirectory == null || sLastProjectsDirectory == "")
		  sLastProjectsDirectory = getProjectFile().getParent();  // use default or previously used
		
		JFileChooser dlg = new JFileChooser(sLastProjectsDirectory);
		dlg.setFileFilter(new DotAveryFileFilter());
		dlg.setDialogTitle("Select an Avery Project bundle file");
		
		if (dlg.showOpenDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
		{
		  File file = dlg.getSelectedFile();
		  loadAveryProject(file);
		  addToRecentFileList(file);
		}
  }

  private void onCreateAutoTemplate()
  {
    JFileChooser dlg = new JFileChooser(Config.getAutoTemplatesDir());
    dlg.setAcceptAllFileFilterUsed(false);
    dlg.setFileFilter(new AnyXmlFileFilter());
    dlg.setSelectedFile(generateAutoTemplateFilespec());
    dlg.setDialogTitle("Create AutoTemplate file");
    dlg.setApproveButtonText("Create");

    if (dlg.showSaveDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
      File file = dlg.getSelectedFile();

      if (file.getName().toLowerCase().endsWith(".xml") == false)
      {
        file = new File(file.getAbsolutePath() + ".xml");
      }
      
      if (file.exists())
      {
        if (file.canWrite())
        {
          if (JOptionPane.showConfirmDialog(Predesigner.getFrame(), "Overwrite " + file.getName() + "?", "confirm", JOptionPane.YES_NO_OPTION)
            != JOptionPane.YES_OPTION)
          {
            // user dowsn't want to overwrite
            onCreateAutoTemplate();    // recurse
            return;
          }
        }
        else  // file not writable
        {
          JOptionPane.showMessageDialog(Predesigner.getFrame(), file.getName() + " is read-only!", "error", JOptionPane.ERROR_MESSAGE);
          onCreateAutoTemplate();    // try again
          return;
        }
      }

      try   // write the AutoTemplate
      { 
        AveryMasterpanel master = Predesigner.getFrame().getCurrentMasterpanel();
        String designTheme = Predesigner.getProject().getDesignTheme();
        if (designTheme != null && designTheme.length() > 0)
        {
          master.addHint("designTheme", designTheme);
        }
        AutoTemplate autoTemplate = new AutoTemplate(master);
        autoTemplate.writeMasterpanelFile(file);
      }
      catch (Exception e)
      {
        JOptionPane.showMessageDialog(Predesigner.getFrame(),
          "Couldn't create AutoTemplate " + file.getName() + "\n" + e.getMessage(),
          "Create AutoTemplate", JOptionPane.ERROR_MESSAGE);
      }
    }
  }
  
  private File generateAutoTemplateFilespec()
  {  
    Project project = Predesigner.getProject();
    
    // elements of the filename
    String language = project.getLanguage();
    String productType = "unknown";
    String designTheme = Predesigner.getProject().getDesignTheme();
    String styleSetName = (designTheme == null) ? "none" : designTheme.replaceAll("[\\W]", ""); 
    String date = new SimpleDateFormat("yyMM-").format(new Date());
    
    try
    { // find productType
      Iterator iterator = Predesigner.getProductGroupList().iterator();
      while (iterator.hasNext())
      {
        ProductGroup group = (ProductGroup)iterator.next();
        if (group.getName().equals(project.getProductGroup()))
        {
          productType = group.getProductType() == null ? group.getBaseSku() : group.getProductType();
          break;
        }
      }
    }
    catch (Exception ex) { }
    
    String prefix = language + "." + productType + "." + styleSetName + "." + date;
    int i = 1;
    while (new File(Config.getAutoTemplatesDir(), prefix + twoDigits(i) + ".xml").exists())
      ++i;
       
    return new File(Config.getAutoTemplatesDir(), prefix + twoDigits(i) + ".xml");
  }
  
  private String twoDigits(int i)
  {
    if (i < 10)
    {
      return new String("0" + i);
    }
    else return Integer.toString(i);   
  }
  
  private void onImportAutoTemplate()
  {
    JFileChooser dlg = new JFileChooser(Config.getAutoTemplatesDir());
    dlg.setAcceptAllFileFilterUsed(false);
    dlg.setFileFilter(new AnyXmlFileFilter());
    dlg.setDialogTitle("Apply Fields from AutoTemplate File");
    dlg.setApproveButtonText("Apply");
    
    MasterpanelAccessory thumb = new MasterpanelAccessory(dlg);
    dlg.setAccessory(thumb);
    dlg.addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY, thumb);

    if (dlg.showOpenDialog(Predesigner.getFrame()) == JFileChooser.APPROVE_OPTION)
    {
      File file = dlg.getSelectedFile();
      try
      {
        SAXBuilder builder = new SAXBuilder(true);
        builder.setFeature("http://apache.org/xml/features/validation/schema", true);
        builder.setProperty(
            "http://apache.org/xml/properties/schema/external-schemaLocation",
            Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 
        
        // apply autotemplate to current masterpanel of current project
        new AutoTemplate(builder.build(file).getRootElement()).applyTo(
            Predesigner.getProject(), 
            Predesigner.getFrame().getCurrentMasterpanel());
        
        // update the session StyleSet
        Predesigner.styleSet = Predesigner.getProject().getStyleSet();
       
        // update the view
        Predesigner.getFrame().onFieldChanged();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        JOptionPane.showMessageDialog(Predesigner.getFrame(), ex.getMessage(), ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
      }
    }
    
  }
  
}