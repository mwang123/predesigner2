package com.avery.predesigner;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Base class for ZOrderPanel, PromptOrderPanel</p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2
 */

class OrderFieldPrompts extends JPanel
{
	protected JList list;
  protected DefaultListModel listModel;
  protected JButton upButton = new JButton("Move Up");
  protected JButton downButton = new JButton("Move Down");
  
  OrderFieldPrompts()
  {
    listModel = new DefaultListModel();
    list = new JList(listModel);
    list.setBorder(new BevelBorder(BevelBorder.LOWERED));
    
    upButton.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onUpButton();
      }
    });

    downButton.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onDownButton();
      }
    });

    GridLayout buttonLayout = new GridLayout(4,1);
    buttonLayout.setVgap(5);
    JPanel buttonPanel = new JPanel(buttonLayout);
    buttonPanel.add(upButton);
    buttonPanel.add(downButton);
        
    GridLayout gridLayout = new GridLayout(1, 2);
    gridLayout.setHgap(10);
    JPanel grid = new JPanel(gridLayout);
    grid.add(list);
    grid.add(buttonPanel);
    this.add(grid);
  }
  
  void onUpButton()
  {
    int index = list.getSelectedIndex();
    if (index > 0)
    {
      Object element = listModel.getElementAt(index - 1);
      listModel.setElementAt(listModel.getElementAt(index), index - 1);
      listModel.setElementAt(element, index);
      
      list.setSelectedIndex(index - 1);
      list.invalidate();
      list.repaint();
    }
  }
  
  void onDownButton()
  {
    int size = listModel.getSize(); 
    int index = list.getSelectedIndex();
    if (index < (size - 1) && index > -1)
    {
      Object element = listModel.getElementAt(index + 1);
      listModel.setElementAt(listModel.getElementAt(index), index + 1);
      listModel.setElementAt(element, index);
      
      list.setSelectedIndex(index + 1);
      list.invalidate();
      list.repaint();
    }
  }

  void fillListModel(java.util.List list)
  {
    java.util.Iterator iterator = list.iterator();
    while (iterator.hasNext())
    {
      listModel.addElement(iterator.next());
    }
  }
  
  Object[] getReorderedList()
  {
    return listModel.toArray();
  }
  
  void onAdvancedButton()
  {
    JOptionPane.showMessageDialog(this, "advanced");
  }
}