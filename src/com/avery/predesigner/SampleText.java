/*
 * SampleText.java Created on Feb 11, 2008 by leeb
 * Copyright 2008 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipException;

import org.jdom.JDOMException;

import com.avery.office.XlWorkbook;
import com.avery.office.XlWorksheet;

/**
 * @author leeb
 * Feb 11, 2008
 * SampleText
 */
class SampleText
{
  private static XlWorkbook workbook = null;

  SampleText()
  throws IOException, JDOMException, ZipException
  {
    if (workbook == null || !workbook.file.equals(Config.sampleTextFile))
    {
      XlWorkbook workbook = new XlWorkbook(Config.sampleTextFile);
      if (workbook != null)
      {
        SampleText.workbook = workbook;
      }
    }
  }
  
  /**
   * This method is designed to feed AveryProject.translateText()
   * @param oldLanguage - the language to translate from
   * @param newLanguage - the language to translate to
   * @return oldLanguage strings converted to lowercase are keys, newLanguage strings are values
   */
  Hashtable translationTable(String oldLanguage, String newLanguage)
  {
    Hashtable translations = new Hashtable();
    try 
    {
      Iterator sheetNames = workbook.getSheetNames().iterator();
      while (sheetNames.hasNext())
      {
      	//String sname = (String)sheetNames.next();
      	//System.out.println("sheet name=" + sname);
        XlWorksheet sheet = workbook.getWorksheet((String)sheetNames.next());
        List keys = sheet.getColumn(oldLanguage);
        List values = sheet.getColumn(newLanguage);
        if (keys != null && values != null)
        {
          Iterator k = keys.iterator();
          Iterator v = values.iterator();
          while (k.hasNext() && v.hasNext())
          {
            Object key = k.next();
            Object value = v.next();
            // ignore nulls and empty strings
            if (null == k || "".equals(k) || null == v || "".equals(v))
            {
              continue;
            }
            // we have a winner! 
            //System.out.println("winner value=" + (String)key + "," + value);
            translations.put(((String)key).toLowerCase(), value);
          }
        }
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    return translations;
  }
  
  /**
   * @return a list of language strings
   */
  List getSupportedLanguages()
  {
    try
    {
      XlWorksheet sheet = workbook.getWorksheet((String)workbook.getSheetNames().get(1));
      return new ArrayList(sheet.getRow(0));
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      return new ArrayList();   // new, but empty list
    }
  }
  
  /**
   * @return a list of the SampleText categories (sheet names starting at second sheet)
   */
  List getCategories()
  {
    List sheetnames = new ArrayList(workbook.getSheetNames());
    sheetnames.remove(0);
    return sheetnames;
  }
  
  /**
   * @param productType
   * @return the default SampleText category for the given productType (mapped from the first sheet)
   */
  String getDefaultCategory(String productType)
  {
    return (String)workbook.getWorksheet("productType").getRow(productType).get(1);
  }
  
  /**
   * @param category - the SampleText category of interest
   * @param language - the language code
   * @return List of strings matching the category and language
   */
  List getTextStrings(String category, String language)
  {
    XlWorksheet sheet = workbook.getWorksheet(category);
    List column = new ArrayList(sheet.getColumn(language));
    column.remove(0);
    return column;
  }
}
