package com.avery.predesigner;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFormattedTextField;
import java.text.NumberFormat;

class LabelTextPanel extends JPanel
{
  protected JLabel label = new JLabel();
  protected JFormattedTextField amountField;
  
	LabelTextPanel(String labelText)
	{
    label.setText(labelText);
    
    NumberFormat amountFormat = NumberFormat.getNumberInstance();
    amountField = new JFormattedTextField(amountFormat);
    amountField.setValue(new Double(0));
    amountField.setColumns(10);
     
    java.awt.BorderLayout layout = new java.awt.BorderLayout();
    layout.setHgap(6);
    this.setLayout(layout);
    this.add(label, "West");
    this.add(amountField, "South");
    

	}

}
