package com.avery.predesigner;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.avery.project.AveryPanelField;
import com.avery.project.AveryTextblock;
import com.avery.project.AveryTextfield;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright 2016 Avery Products Corporation, all rights reserved</p>
 * <p>Company: Avery Products Corporation</p>
 * @author Bob Lee, Brad Nelson
 * @version 1.6
 */

class FieldMouseAction extends UndoableAction
{
  private AveryPanelField field;
  private Point originalPosition;
  private Double originalWidth;
  private Double originalHeight;
  private Point newPosition;
  private Point deltaPos;
  private Double newWidth;
  private Double newHeight;
  private ArrayList currentSelections = new ArrayList();
  private int currentSelection;
  private ArrayList pickedShapes = new ArrayList();
	private List panelFields = null;
	private double twipsPerPixel = 20.0;
	
	private ArrayList cs = null;
	private ArrayList ps = null;
  
  private static int MIN_BOX_HEIGHT = 100;
  
  FieldMouseAction(AveryPanelField field, Point position, double width, double height)
  {
    super("Undo Move/Resize");
    this.field = field;
    this.newPosition = position;
    this.newWidth = new Double(width);
    this.newHeight = new Double(height);
    this.originalPosition = field.getPosition();
    this.originalWidth = field.getWidth();
    this.originalHeight = field.getHeight();
  }
  
  FieldMouseAction(AveryPanelField field, Point position, double width, double height, ArrayList currentSelections,
  		int currentSelection, ArrayList pickedShapes, List panelFields, double twipsPerPixel)
  {
    super("Undo Move/Resize All");
    this.field = field;
    this.newPosition = position;
    this.newWidth = new Double(width);
    this.newHeight = new Double(height);
    this.originalPosition = field.getPosition();
    this.originalWidth = field.getWidth();
    this.originalHeight = field.getHeight();
    this.currentSelections.addAll(currentSelections);
    cs = currentSelections;
    this.currentSelection = currentSelection;
    this.pickedShapes.addAll(pickedShapes);
    ps = pickedShapes;
    this.panelFields = panelFields;
    this.twipsPerPixel = twipsPerPixel;
  }

  FieldMouseAction(AveryPanelField field, Point position)
  {
    super("Undo Move");
    this.field = field;
    this.newPosition = position;
    this.originalPosition = field.getPosition();
    this.originalWidth = field.getWidth();
    this.originalHeight = field.getHeight();
    // no change in width or height
    newWidth = originalWidth;
    newHeight = originalHeight;
  }
  
  void execute()
  {	
  	if (currentSelections.size() > 1)
  	{
      AveryPanelField apf = (AveryPanelField)panelFields.get(currentSelection);
      deltaPos = new Point(newPosition.x - originalPosition.x, newPosition.y - originalPosition.y);
    	for (int i = 0; i < currentSelections.size(); i++)
    	{
        if (currentSelection > -1)
        {
          //deltaPos.x -= apf.getPosition().x;
          //deltaPos.y -= apf.getPosition().y;
          
  	  		int selection = (int)currentSelections.get(i);
  	  		if (selection != currentSelection)
  	  		{
  	  			apf = (AveryPanelField)panelFields.get(selection);
  	  			Point curPos = apf.getPosition();
  	  			apf.setPosition(new Point(curPos.x + deltaPos.x, curPos.y + deltaPos.y));
  	  			//updatePanelField(apf, deltaPos);
  	  			com.avery.predesigner.tools.Shape s = (com.avery.predesigner.tools.Shape)pickedShapes.get(i);
  	  			int dX = (int)(deltaPos.x / twipsPerPixel);
  	  			int dY = (int)(deltaPos.y / twipsPerPixel);
  	  			s.move(s.getX() + dX, s.getY() + dY);   		
  	  		}
        }
    	}
  		
  	}
    field.setPosition(newPosition);
    field.setWidth(newWidth);
    field.setHeight(newHeight);
    if (field instanceof AveryTextfield)
    {
      // special case - don't allow height of a text field to get below the minimum box size
      //double minHeight = ((AveryTextfield)field).getPointsize().doubleValue() * 20.0;
      //System.out.println("minHeight=" + minHeight);
      
    	int lines = 1;
    	if (field instanceof AveryTextblock)
    	{
    		lines = ((AveryTextblock)field).getLines().size();
    	}
      if (newHeight.doubleValue() < MIN_BOX_HEIGHT)
      {
        field.setHeight(lines * new Double(MIN_BOX_HEIGHT));
        // Predesigner.getFrame().updateView(true);
        // return;
      }
    }
    Predesigner.getFrame().updateView(false);
  }
  
  void undo()
  {
  	
  	if (currentSelections.size() > 1)
  	{
      AveryPanelField apf = (AveryPanelField)panelFields.get(currentSelection);
      Point deltaPos = new Point(newPosition.x - originalPosition.x, newPosition.y - originalPosition.y);
    	for (int i = 0; i < currentSelections.size(); i++)
    	{
        if (currentSelection > -1)
        {
  	  		int selection = (int)currentSelections.get(i);
  	  		if (selection != currentSelection)
  	  		{
  	  			apf = (AveryPanelField)panelFields.get(selection);
  	  			Point curPos = apf.getPosition();
  	  			apf.setPosition(new Point(curPos.x - deltaPos.x, curPos.y - deltaPos.y));
  	  			//updatePanelField(apf, deltaPos);
  	  			com.avery.predesigner.tools.Shape s = (com.avery.predesigner.tools.Shape)pickedShapes.get(i);
  	  			int dX = (int)(deltaPos.x / twipsPerPixel);
  	  			int dY = (int)(deltaPos.y / twipsPerPixel);
  	  			s.move(s.getX() - dX, s.getY() - dY);   		
  	  		}
        }
    	}
    	cs.clear();
    	ps.clear();
 		
  	}

    field.setPosition(originalPosition);
    field.setWidth(originalWidth);
    field.setHeight(originalHeight);    
    Predesigner.getFrame().updateView(true);
  }
}