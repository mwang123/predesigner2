package com.avery.predesigner;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;

import javax.swing.JScrollPane;

import com.avery.project.AveryPage;
import com.avery.project.AveryPanel;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Provides the full page view of an AveryProject</p>
 * <p>Copyright: Copyright 2003-2004 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2.0
 */

class PageView extends View
{
	private static final long serialVersionUID = -1998876265397913834L;
	private int pageNumber = 1;
  private static final int PANEL_BORDER = 60;
  private static boolean zooming = false;
  
  private static int trueWidth = 1;
  private static int trueHeight = 1;

  PageView(Project project)
  {
    super(project);
  }

  void setPageNumber(int pageNumber)
  {
    if (getProject() != null)
    {
      if (pageNumber > 0 && pageNumber <= project.getPages().size())
      {
        this.pageNumber = pageNumber;
        invalidate();
      }
    }
  }

  int getPageNumber()
  {
    return this.pageNumber;
  }

  void setProject(Project project)
  {
    super.setProject(project);
    this.pageNumber = 1;
    invalidate();
    
    zoomScale = 0;
  }

  private double zoomScale = 0.;
  
  //private int zoomNumerator = 2;
  //private int zoomDenominator = 2;

  public double getTwipsPerPixel()
  {
    //return (15. * zoomNumerator) / (double)zoomDenominator;
  	return 1.0 + zoomScale;
  }

  private void noZoom()
  {
    //zoomNumerator = zoomDenominator = 2;
  	zoomScale = 0.;
  }

  private void zoomin()
  {
    /*if (zoomNumerator > 2)
    {
      zoomNumerator--;
      zoomDenominator = 2;
    }
    else
    {
      zoomNumerator = 2;
      if (zoomDenominator < 20)
        ++zoomDenominator;
    }*/
  	zoomScale += 0.5;
  	//System.out.println("zoomScale=" + zoomScale);
  }
  
  private void zoomout()
  {
    /*if (zoomDenominator > 2)
    {
      zoomNumerator = 2;
      --zoomDenominator;
    }
    else
    {
      if (zoomNumerator < 20)
        ++zoomNumerator;
      zoomDenominator = 2;
    }*/
  	zoomScale -= 0.5;
  	if (zoomScale < 0.)
  		zoomScale = 0;
  	
  	//System.out.println("zoomScale=" + zoomScale);
  }
  
  void zoomIn()
  {
  	zooming = true;
    zoomin();
    
  	AveryPage page = getProject().getPage(getPageNumber());
    double pageAspect = ((double)page.getWidth()) / ((double)page.getHeight());
    
    // assume width fits
    double w = trueWidth - 90;
    double h = w / pageAspect;
    if (h > (trueHeight - 30))
    {
    	h = trueHeight - 30;
    	w = h * pageAspect;
    }
    
  	//System.out.println("in  width, height=" + getWidth() + "," + getHeight());
    double scalar = getTwipsPerPixel();
    w = w * scalar;
    h = h * scalar;
  	//System.out.println("w, h, scalar=" + getWidth() + "," + getHeight() + "," + scalar);

    getFrame().resizeCard2((int)w + 2 * PANEL_BORDER, (int)h + 2 * PANEL_BORDER);
    //getFrame().resizeCard2((int)w, (int)h);

    //buildFieldSelectionTools();
    updateStatusDisplay();
  }
  
  void zoomOut()
  {
  	zooming = true;
    zoomout();
    
  	AveryPage page = getProject().getPage(getPageNumber());
    double pageAspect = ((double)page.getWidth()) / ((double)page.getHeight());
    
    // assume width fits
    double w = trueWidth - 90;
    double h = w / pageAspect;
    if (h > (trueHeight - 30))
    {
    	h = trueHeight - 30;
    	w = h * pageAspect;
    }
    
  	//System.out.println("out width, height=" + getWidth() + "," + getHeight());
    double scalar = getTwipsPerPixel();
    w = w * scalar;
    h = h * scalar;
  	//System.out.println("w, h, scalar=" + getWidth() + "," + getHeight() + "," + scalar);
    getFrame().resizeCard2((int)w + 2 * PANEL_BORDER, (int)h + 2 * PANEL_BORDER);
    //getFrame().resizeCard2((int)w, (int)h);

    updateStatusDisplay();
  }
  
  int getZoomPercent()
  {
  	return (int)(1500.0 / getTwipsPerPixel());
  }

  public void paint(Graphics g)
  {
    super.paint(g);
    
    if (getProject() != null)
    {
    	AveryPage page = getProject().getPage(getPageNumber());
      double pageAspect = ((double)page.getWidth()) / ((double)page.getHeight());
      int x = 0;
      int y = 0;
      
      // assume width fits
      double w = getWidth() - 90;
      double h = w / pageAspect;
      if (h > (getHeight() - 30))
      {
      	h = getHeight() - 30;
      	w = h * pageAspect;
      }
      
      double scalar = 1.0; //getTwipsPerPixel();
      w = w * scalar;
      h = h * scalar;

      x = (getWidth() - (int)w - 90) / 2;
      y = (getHeight() - (int)h - 30) / 2;

      try
      {
        Image image = getProject().makePagePreview(getPageNumber(), (int)w, false);
        g.drawImage(image, x, y, null);
        g.setColor(Color.black);
        g.drawRect(x, y, (int)w, (int)h);
      }
      catch (Exception e)
      {
        g.setColor(Color.black);
        g.drawString(e.toString(), getInsets().left, getInsets().top + 100);
      }
    }
    if (!zooming)
    {
    	zoomScale = 0;
    	trueWidth = getWidth();
    	trueHeight = getHeight();
    }
    zooming = false;
  }
  
  void updateStatusDisplay()
  {
  	Frame1 frame = Predesigner.getFrame();
  	Project project = getProject();
  	
  	if (frame != null && project != null)
  	{
  		if ( showPanelInfo ) {
  			frame.updateStatus(RotationManager.getViewOrientationFor(project));
  		}
  		else {
  	  		frame.updateStatus(project.getPage(getPageNumber()).getDescription());
  		}
  	}
  }
  
  boolean showPanelInfo = false;
  
  void showPanelInfo(boolean show)
  {
	AveryPanel.setShowUnits(Config.units);
	
	// show/hide panel info 
	AveryPanel.showHiddenInfo(show);
	
	showPanelInfo = show;

	updateStatusDisplay();
	repaint();
  }
}