package com.avery.predesigner;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.borland.jbcl.layout.VerticalFlowLayout;

public class CustomPanelGroupsDialog extends JDialog
{
	private boolean okay = false;
	
	private int mp1Panels;
	private int mp2Panels;

	private LabelTextPanel mp1g1;
  private LabelTextPanel mp1g2;
  private LabelTextPanel mp1g3;
  private LabelTextPanel mp1g4;
  private LabelTextPanel mp1g5;
  private LabelTextPanel mp1g6;
  private LabelTextPanel mp1g7;
  private LabelTextPanel mp1g8;
  private LabelTextPanel mp1g9;
  private LabelTextPanel mp1g10;

  private LabelTextPanel mp2g1;
  private LabelTextPanel mp2g2;
  private LabelTextPanel mp2g3;
  private LabelTextPanel mp2g4;
  private LabelTextPanel mp2g5;
  private LabelTextPanel mp2g6;
  private LabelTextPanel mp2g7;
  private LabelTextPanel mp2g8;
  private LabelTextPanel mp2g9;
  private LabelTextPanel mp2g10;
  
  private LabelTextPanel mp1total;
  private LabelTextPanel mp2total;

  CustomPanelGroupsDialog(Frame owner, int mp1s, int mp2s)
  {
  	mp1Panels = mp1s;
   	mp2Panels = mp2s;
 	
    try
    {
    	mp1g1 = new LabelTextPanel("Group 1");
    	mp1g2 = new LabelTextPanel("Group 2");
    	mp1g3 = new LabelTextPanel("Group 3");
    	mp1g4 = new LabelTextPanel("Group 4");
    	mp1g5 = new LabelTextPanel("Group 5");
    	mp1g6 = new LabelTextPanel("Group 6");
    	mp1g7 = new LabelTextPanel("Group 7");
    	mp1g8 = new LabelTextPanel("Group 8");
    	mp1g9 = new LabelTextPanel("Group 9");
    	mp1g10 = new LabelTextPanel("Group 10");
    	mp1total = new LabelTextPanel("Total MP1");
    	
    	mp2g1 = new LabelTextPanel("Group 1");
    	mp2g2 = new LabelTextPanel("Group 2");
    	mp2g3 = new LabelTextPanel("Group 3");
    	mp2g4 = new LabelTextPanel("Group 4");
    	mp2g5 = new LabelTextPanel("Group 5");
    	mp2g6 = new LabelTextPanel("Group 6");
    	mp2g7 = new LabelTextPanel("Group 7");
    	mp2g8 = new LabelTextPanel("Group 8");
    	mp2g9 = new LabelTextPanel("Group 9");
    	mp2g10 = new LabelTextPanel("Group 10");
    	mp2total = new LabelTextPanel("Total MP2");
    	
    	init();
    	
    	this.setSize(new Dimension(200, 700));
    	
      Dimension dlgSize = this.getPreferredSize();
      Dimension frmSize = owner.getSize();
      Point loc = owner.getLocation();
      this.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
      this.setModal(true);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    } 
  }
  
  private void init() throws Exception
  {
    this.setTitle("Custom Panel Groups");
    
    JPanel leftPanel = new JPanel();
    VerticalFlowLayout leftPanelLayout = new VerticalFlowLayout();
    leftPanel.setLayout(leftPanelLayout);
    leftPanelLayout.setVgap(15);
    leftPanel.setMinimumSize(new Dimension(80, 400));

    JLabel leftTitle = new JLabel("MP 1: " + mp1Panels);
    leftTitle.setFont(new java.awt.Font("Dialog", 1, 16));
    leftTitle.setHorizontalAlignment(SwingConstants.CENTER);  
    leftPanel.add(leftTitle);
    leftPanel.add(mp1g1);
    leftPanel.add(mp1g2);
    leftPanel.add(mp1g3);
    leftPanel.add(mp1g4);
    leftPanel.add(mp1g5);
    leftPanel.add(mp1g6);
    leftPanel.add(mp1g7);
    leftPanel.add(mp1g8);
    leftPanel.add(mp1g9);
    leftPanel.add(mp1g10);
    leftPanel.add(mp1total);
    
    JPanel rightPanel = new JPanel();
    VerticalFlowLayout rightPanelLayout = new VerticalFlowLayout();
    rightPanel.setLayout(rightPanelLayout);
    rightPanelLayout.setVgap(15);
    
    JLabel rightTitle = new JLabel("MP 2: " + mp2Panels);
    rightTitle.setFont(new java.awt.Font("Dialog", 1, 16));
    rightTitle.setHorizontalAlignment(SwingConstants.CENTER);
    rightPanel.add(rightTitle);
    rightPanel.add(mp2g1);
    rightPanel.add(mp2g2);
    rightPanel.add(mp2g3);
    rightPanel.add(mp2g4);
    rightPanel.add(mp2g5);
    rightPanel.add(mp2g6);
    rightPanel.add(mp2g7);
    rightPanel.add(mp2g8);
    rightPanel.add(mp2g9);
    rightPanel.add(mp2g10);
    rightPanel.add(mp2total);
    
    // totals button
    JButton totalsButton = new JButton("Totals");
    totalsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
      	getTotals();
      }
    });
    // copy button
    JButton copyButton = new JButton("Copy MP1 to MP2");
    copyButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        mp2g1.amountField.setValue(mp1g1.amountField.getValue());
        mp2g2.amountField.setValue(mp1g2.amountField.getValue());
        mp2g3.amountField.setValue(mp1g3.amountField.getValue());
        mp2g4.amountField.setValue(mp1g4.amountField.getValue());
        mp2g5.amountField.setValue(mp1g5.amountField.getValue());
        mp2g6.amountField.setValue(mp1g6.amountField.getValue());
        mp2g7.amountField.setValue(mp1g7.amountField.getValue());
        mp2g8.amountField.setValue(mp1g8.amountField.getValue());
        mp2g9.amountField.setValue(mp1g9.amountField.getValue());
        mp2g10.amountField.setValue(mp1g10.amountField.getValue());
        
        getTotals();
      }
    });

    
    // okay/cancel buttons
    JButton okayButton = new JButton("Okay");
    JButton cancelButton = new JButton("Cancel");
    
    okayButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okay = true;
        dispose();
      }
    });

    cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okay = false;
        dispose();
      }
    });
    
    JPanel totalsCopyPanel = null;
    totalsCopyPanel = new JPanel();    
    totalsCopyPanel.add(totalsButton);
    if (mp2Panels > 0)
    {
    	totalsCopyPanel.add(copyButton);
    }
    
    JPanel okayPanel = new JPanel();    
    okayPanel.add(okayButton);
    okayPanel.add(cancelButton);
    
    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(1,2));
    panel.add(leftPanel);
    if (mp2Panels > 0)
    {
    	panel.add(rightPanel);
    }
    
    this.getContentPane().add(panel, BorderLayout.NORTH);
    this.getContentPane().add(totalsCopyPanel, BorderLayout.CENTER);
    this.getContentPane().add(okayPanel, BorderLayout.SOUTH);
  }
  
  public ArrayList getCPG1()
  {
		ArrayList cpg = new ArrayList();
		
		// mp1 group
		int total = 0;
		int count = fromLTP(mp1g1).intValue();
		if (count < mp1Panels)
		{
			// the first two must have numbers in them
			if (count <= 0)
				return null;
			cpg.add(Integer.valueOf(count));
			total += count;
			
			count = fromLTP(mp1g2).intValue();
			if (count <= 0)
				return null;
			total += count;
			if (total >= mp1Panels)
			{
				if (total > mp1Panels)
					count -= (total - mp1Panels);
				cpg.add(Integer.valueOf(count));
				return cpg;
			}
			cpg.add(Integer.valueOf(count));
			
			// if any have zero fill up the total with that group and are done
			total = nextLTP(mp1g3, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp1g4, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp1g5, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp1g6, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp1g7, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp1g8, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp1g9, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp1g10, cpg, mp1Panels, total, true);
		}	
		return cpg;
  }
  
  public ArrayList getCPG2()
  {
		ArrayList cpg = new ArrayList();
		
		// mp1 group
		int total = 0;
		int count = fromLTP(mp2g1).intValue();
		if (count < mp1Panels)
		{
			// the first two must have numbers in them
			if (count <= 0)
				return null;
			cpg.add(Integer.valueOf(count));
			total += count;
			
			count = fromLTP(mp2g2).intValue();
			if (count <= 0)
				return null;
			total += count;
			if (total >= mp1Panels)
			{
				if (total > mp1Panels)
					count -= (total - mp1Panels);
				cpg.add(Integer.valueOf(count));
				return cpg;
			}
			cpg.add(Integer.valueOf(count));
			
			// if any have zero fill up the total with that group and are done
			total = nextLTP(mp2g3, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp2g4, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp2g5, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp2g6, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp2g7, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp2g8, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp2g9, cpg, mp1Panels, total, false);
			if (total >= mp1Panels)
				return cpg;
			
			total = nextLTP(mp2g10, cpg, mp1Panels, total, true);
		}	
		return cpg;
  }
  
  private Number fromLTP(LabelTextPanel ltp)
  {
  	return (Number)(ltp.amountField.getValue());
  }
  
  private int nextLTP(LabelTextPanel ltp, ArrayList cpg, int mpPanels, int total, boolean bLast)
  {
		int count = fromLTP(ltp).intValue();
		if (count < 0)
			count = 0;
		//System.out.println("count a=" + count);
		if (count == 0 || bLast)
		{
			cpg.add(Integer.valueOf(mpPanels - total));
			return mpPanels;
		}
		total += count;
		if (total >= mpPanels)
		{
			if (total > mpPanels)
				count -= (total - mpPanels);
			//System.out.println("count b=" + count);
			cpg.add(Integer.valueOf(count));
			return mpPanels;
		}
		cpg.add(Integer.valueOf(count));
		return total;
  }
  
  private void getTotals()
  {
  	int total = fromLTP(mp1g1).intValue();
  	total += fromLTP(mp1g2).intValue(); 
  	total += fromLTP(mp1g3).intValue(); 
  	total += fromLTP(mp1g4).intValue(); 
  	total += fromLTP(mp1g5).intValue(); 
  	total += fromLTP(mp1g6).intValue(); 
  	total += fromLTP(mp1g7).intValue(); 
  	total += fromLTP(mp1g8).intValue(); 
  	total += fromLTP(mp1g9).intValue(); 
  	total += fromLTP(mp1g10).intValue();  	

  	mp1total.amountField.setValue(Integer.valueOf(total));
  	
  	total = fromLTP(mp2g1).intValue();
  	total += fromLTP(mp2g2).intValue(); 
  	total += fromLTP(mp2g3).intValue(); 
  	total += fromLTP(mp2g4).intValue(); 
  	total += fromLTP(mp2g5).intValue(); 
  	total += fromLTP(mp2g6).intValue(); 
  	total += fromLTP(mp2g7).intValue(); 
  	total += fromLTP(mp2g8).intValue(); 
  	total += fromLTP(mp2g9).intValue(); 
  	total += fromLTP(mp2g10).intValue(); 

  	mp2total.amountField.setValue(Integer.valueOf(total));
   }
 
  boolean isOkay()
  {
    return okay;
  }
}
