/*
 * BlankCreatorDialog.java Created on Oct 10, 2008 by leeb
 * Copyright 2008 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdom.JDOMException;

import com.avery.product.Appearance;
import com.avery.product.Product;
import com.avery.product.ProductList;
import com.avery.project.Descriptions;

/**
 * @author nelsonb
 * Feb 1, 2012
 * PaperCreatorDialog
 */
public class PapersCreator extends JPanel
{
  //JComboBox languageCombo = new JComboBox();
  ConfigFileBrowsePanel productsPanel = 
    new ConfigFileBrowsePanel("Product List:", Config.productListFile);
  DirectoryPanel batFolderPanel =
    new DirectoryPanel("BAT Folder:", Config.getBatDirectory());
  DirectoryPanel outputFolderPanel =
    new DirectoryPanel("Output Folder:", Config.getBatDirectory() + "\\papers");

  PapersCreator()
  throws JDOMException, IOException
  {
    //initLanguageCombo();
    
    GridBagLayout gridbag = new GridBagLayout();
    setLayout(gridbag);
    GridBagConstraints c = new GridBagConstraints();
    c.gridwidth = GridBagConstraints.REMAINDER;

    add(new JLabel("Generate \"master panel papers\" from the following inputs:"), c);

    c.fill = GridBagConstraints.HORIZONTAL;
   
    /*c.insets = new Insets(10, 10, 0, 10);
    c.gridwidth = 1;     
    add(new JLabel("Language:"), c);
    c.gridwidth = GridBagConstraints.REMAINDER;
    add(languageCombo, c);*/
    
    c.insets = new Insets(10, 10, 0, 10);
    c.gridwidth = GridBagConstraints.REMAINDER;
    add(productsPanel, c);
    add(batFolderPanel, c);
    add(outputFolderPanel, c);
  }
  
  /*private void initLanguageCombo()
  throws JDOMException, IOException
  {
    SampleText sampleText = new SampleText();   // can throw exceptions
    
    List sampleTextLanguages = sampleText.getSupportedLanguages();
    Enumeration languages = Descriptions.getSupportedLanguages();
    while (languages.hasMoreElements())
    {
      String language = (String)languages.nextElement();
      if (sampleTextLanguages.contains(language))
      {
        languageCombo.addItem(language);
      }
    }
  }
  
  String getLanguage()
  {
    return (String)languageCombo.getSelectedItem();
  }*/
  
  File getProductListFile()
  {
    return productsPanel.getFile();
  }
  
  File getBatFolder()
  {
    return new File(batFolderPanel.textField.getText());
  } 
  
  File getOutputFolder()
  {
    return new File(outputFolderPanel.textField.getText());
  }
  
  void makePapers()
  throws JDOMException, IOException
  {
    //String language = getLanguage();
    File productsFile = getProductListFile();
    File inputFolder = getBatFolder();
    File outputFolder = getOutputFolder();
    String papersDir = new File(Config.getBatDirectory() + "\\papers").getAbsolutePath();
    
    MultiplierDialog dlg = new MultiplierDialog("Master Panel Papers Generator"); 

    dlg.logComment("Started " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
    dlg.setVisible(true);
    dlg.makeBusy(true);
    
    //SAXBuilder builder = new SAXBuilder(true);
    //builder.setFeature("http://apache.org/xml/features/validation/schema", true);
    //builder.setProperty(
        //"http://apache.org/xml/properties/schema/external-schemaLocation",
        //Averysoft.getAverysoftURI() + " " + Averysoft.getSchemaURL()); 

    ProductList pgList = new ProductList(productsFile);
    
    dlg.logComment("Creating master panel papers from ProductList " + productsFile.getName());
          
    Iterator products = pgList.iterator();
    while (products.hasNext())
    {
      dlg.makeBusy(true);   // guarantee busy cursor
      
      Product product = (Product)products.next();
      
      // filter for products that have paperImage only
      Appearance appearance = product.getAppearance(1);
      if (appearance == null || appearance.getImage() == null)
      	continue;
      
      File bat = new File(inputFolder, product.getBatName());
      if (!bat.exists())
      {
        dlg.logComment("Couldn't find BAT " + bat.getName() + " for product " + product.getSkuName());
        continue;
      }
      try
      {
        Project project = new Project();
        project.read(bat);

        // setup project enough to generate the saster paper image
        //project.setLanguage(language);
        project.setSku(product.getSkuName());
        project.setProductGroup(product.getProductGroupName());
        project.setDesignTheme("Blank");
        // apply paperImage, color, etc. from ProductGroup
        //project.generateMasterBackdrops(papersDir);
        int generated = project.applyAppearances(appearance, outputFolder.getAbsolutePath(), false);

       // File output = new File(outputFolder, language + "." + group.getName() + ".xml");
        //project.write(output);       
        // log results
        if (generated > 0)
        	dlg.logComment("Created masterpanel paper image for " + product.getSkuName());
        else
        	dlg.logComment("Failed to create masterpanel paper image for " + product.getSkuName());
        	
      }
      catch (Exception ex)
      {
        dlg.logComment(ex.getMessage());
      }
    }
    
    dlg.logComment("Finished " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(new Date()));
    dlg.makeBusy(false);
  }
}
