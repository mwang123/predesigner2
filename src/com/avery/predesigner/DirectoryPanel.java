package com.avery.predesigner;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: UI Element contains a text entry box and a Browse button,
 *    For browsing to specify a directory.</p>
 * <p>Copyright: Copyright 2003-2007 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 */

class DirectoryPanel extends BrowsePanel
{
  DirectoryPanel(String description, String directory)
  {
    super(description, directory);
  }
  
  String getDirectory()
  {
    return textField.getText();
  }
  
  /**
   * @override This is the action that takes place when the user clicks 
   * the "Browse..." button.  On success, it calls textField.setText()
   * with the user-selected directory path.
   */
  protected void browse()
  {
    File folder = new File(getDirectory());
    
    JFileChooser dlg = new JFileChooser(folder.getParent());
    dlg.setDialogTitle("Select a Folder");
    dlg.setApproveButtonText("Select");
    dlg.setSelectedFile(folder);
    dlg.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    if (dlg.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
    {
      File newFolder = dlg.getSelectedFile();
      if (newFolder != null && newFolder.exists() && newFolder.isDirectory())
      {     
        textField.setText(newFolder.getAbsolutePath());
      }
      else
      {
        JOptionPane.showMessageDialog(getParent(), newFolder + " is not a valid selection", "Error", JOptionPane.ERROR_MESSAGE);
        browse();   // try again
      }
    }
  }
}