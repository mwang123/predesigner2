package com.avery.predesigner;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.RepaintManager;
import javax.swing.border.Border;

import com.avery.predesigner.editors.FieldEditorPanel;
import com.avery.predesigner.tools.Rect;
import com.avery.predesigner.tools.ToolManager;
import com.avery.project.AveryBackground;
import com.avery.project.AveryImage;
import com.avery.project.AveryMasterpanel;
import com.avery.project.AveryPanel;
import com.avery.project.AveryPanelField;
import com.avery.project.AveryTextblock;
import com.avery.project.Drawing;
import com.avery.project.AveryPage;
import com.avery.project.shapes.PolygonShape;
import com.avery.utils.AveryUtils;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: Provides the Masterpanel view of an AveryProject</p>
 * <p>Copyright: Copyright 2013-2016 Avery Products Corporation, all rights reserved</p>
 * @author Bob Lee, Brad Nelson
 * @version 2.0
 */

public class MasterpanelView extends View implements KeyListener,
  MouseListener, MouseMotionListener
{
	private static final long serialVersionUID = -4057968904860886138L;

	private static final int MIN_DPI = 72;
	private static final double TWIPS_PER_PIXEL = 20;

  private int currentMasterpanelIndex = 0;
  
  // panel field selection tool manager
  private ToolManager tm = new ToolManager();
  private int currentSelection = -1;
  
  // these two support multiple selection
  private ArrayList currentSelections = new ArrayList();
  private ArrayList pickedShapes = new ArrayList();
  
  private java.util.List panelFields;
  private boolean outlines = true;
  private boolean textDefaultOutlines = false;
  private boolean safeOn = true;

  private static final int PANEL_BORDER = 60;

  private Point lastPosition = new Point(0,0);
  
  //TextEditPanel textEditPanel = null;

  AveryMasterpanel copyMasterpanel = null;
  

  MasterpanelView(Project project)
  {
    super(project);
    // add listeners for keyboard, mouse clicks and moves
    addKeyListener(this);
    addMouseListener(this);
    addMouseMotionListener(this);
  }

  void setCurrentMasterpanelIndex(int index)
  {
    currentMasterpanelIndex = index;
    // fix zoom level
    zoomOutToFit();
    buildFieldSelectionTools();
    updateStatusDisplay();
  }

  int getCurrentMasterpanelIndex()
  {
    return currentMasterpanelIndex;
  }

  public AveryPanelField getCurrentPanel()
  {
    AveryPanelField apf = (AveryPanelField)panelFields.get(currentSelection);
    return apf;
  }

  private AveryMasterpanel getMasterpanel(int index)
  {
    return (AveryMasterpanel)this.getProject().getMasterpanels().get(index);
  }

  void setProject(Project project)
  {
    noZoom();
    super.setProject(project);
    currentMasterpanelIndex = 0;
    // fix zoom level
    zoomOutToFit();
    // initializes panel field selection tool
    buildFieldSelectionTools();
    updateStatusDisplay();
    //if (textEditPanel == null)
    	//textEditPanel = new TextEditPanel();
  }

  AveryMasterpanel getCurrentMasterpanel()
  {
    return (AveryMasterpanel)this.getProject().getMasterpanels().get(
      getCurrentMasterpanelIndex());
  }

  // view menu item toggles outlines
  void enableOutlines(boolean state)
  {
    if (outlines != state)
    {
      outlines = state;
      repaint();
    }
  }
  
  // view menu item toggles textDefaultOutlines
  void enableTextDefaultOutlines(boolean state)
  {
    if (textDefaultOutlines != state)
    {
      textDefaultOutlines = state;
      repaint();
    }
  }
  
  void enableSafe(boolean state)
  {
  	if (safeOn != state)
  	{
  		getCurrentMasterpanel().setSafeOn(state);
  		safeOn = state;
  		repaint();
  	}
  }

  void enableQuickText(boolean state)
  {
		getFrame().getMasterpanelToolbar().getTextEditPanel().setVisible(state);      	  		
		repaint();
  }

  // view menu item toggles textDefaultOutlines
  void displayPromptOrder(boolean state)
  {
    AveryMasterpanel masterpanel = getMasterpanel(getCurrentMasterpanelIndex());
    masterpanel.setDisplayPromptOrder(state);
  	repaint();
  }
  
  public void paint(Graphics g)
  {
  	super.paint(g);
  	
    if (this.getProject() != null)
    {
    	int pageNumber = Predesigner.frame.getPageView().getPageNumber();
    	AveryPage page = (AveryPage)project.getPage(pageNumber);
    	
    	if (page.getPaperimage() == null)
    	{
     		Rectangle2D rect = this.getVisibleRect();
     		Graphics2D graphics = (Graphics2D)g;

     		graphics.setBackground(page.getPaperColor() == null ? Color.white : page.getPaperColor());
    		graphics.clearRect(0, 0, (int)rect.getWidth(), (int)rect.getHeight());
    	}
     	
    	try
      {
        AveryMasterpanel masterpanel = getMasterpanel(getCurrentMasterpanelIndex());

        // draw the panel at the top of the window with a 30 pixel border
        int x = PANEL_BORDER;
        int y = PANEL_BORDER;
        
        double scalar = 1.0 / getTwipsPerPixel();
        double w = masterpanel.getWidth().doubleValue() * scalar;
        double h = masterpanel.getHeight().doubleValue() * scalar;
        
        int units = Config.units;
        double tickSize = 1440.0 / getTwipsPerPixel() / 4.0;      
        if (units == AveryUtils.CENTIMETERS)
        {
        	tickSize = 567.0 / getTwipsPerPixel() / 4.0;
        }

        g.translate(x, y);
        masterpanel.drawGuides((Graphics2D)g, scalar, Color.pink);
        //AveryMasterpanel.panelStrokeWidth = 1;
        
        // multiple functions called here set clip to null :(
        masterpanel.draw((Graphics2D)g, (int)w, (int)h, null);
        
        if (textDefaultOutlines)
        {
          masterpanel.drawTextDefaults(g, scalar, new Color(128,255,128));  // light green outlines
        }      
        g.translate(-x, -y);

        if (outlines)
        {
          // draw panel field outlines
          tm.drawOutlines(g, x, y);
        }
         
        // clips the white area the panel lies on
        setClippingPath(g, masterpanel);
        
        /*GeneralPath masterShape = masterpanel.getShape().scaledGraphicsPath(scalar);
        float strokeWidth =  0f;
        if (masterpanel.isBleedable())
        {
        	// if polygon clip to polygon bleed
        	if (masterpanel.getShape().getShapeString().equals("polygon"))
        	{
      			PolygonShape ps = (PolygonShape)masterpanel.getShape();
      			// masterShape is now polygon bleed outline path
      			masterShape = ps.scaledBleedGraphicsPath(scalar);
        	}
        	else
        	{
        		// for all other shapes with bleed scale path up by twice the bleed dimension
	          strokeWidth =  AveryPanel.BLEED_DIMENSION * 20f * (float)scalar;
	        	double widthScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().width) / masterShape.getBounds().width);
	        	double heightScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().height) / masterShape.getBounds().height);
	        	masterShape = masterpanel.getShape().scaledGraphicsPath(widthScalar, heightScalar);
        	}     	
        }
        
        // create view path and substract masterShape using Areas
        GeneralPath viewPath = new GeneralPath(this.getVisibleRect());
        Area areaView = new Area(viewPath);
        Area area1 = new Area(masterShape);
        area1.transform(new AffineTransform(1, 0, 0, 1, PANEL_BORDER - strokeWidth, PANEL_BORDER - strokeWidth));
        areaView.subtract(area1);
        GeneralPath gp = new GeneralPath(areaView);
        g.setClip(gp);
        Color clipColor = new Color(0x00, 0x00, 0xff, 0x7f); // debug - useful to change color
        //Color clipColor = new Color(0xff, 0xff, 0xff, 0x7f);
        g.setColor(clipColor);
        ((Graphics2D)g).fill(viewPath); */
        
        // rulers
        int wT = (int)(w / tickSize);
        int rulerWidth = (int)((double)wT * tickSize);
        if ((double)wT * tickSize != w)
        {
        	rulerWidth += (int)tickSize;
        }
        
        int hT = (int)(h / tickSize);
        int rulerHeight = (int)((double)hT * tickSize);
        if ((double)hT * tickSize != h)
        {
        	rulerHeight += (int)tickSize;
        }
        
        // make them extend a bit further
        rulerWidth = (int)(1.1 * rulerWidth);
        rulerHeight = (int)(1.1 * rulerHeight);
        
        drawRuler(g, tickSize, PANEL_BORDER, 6, rulerWidth + PANEL_BORDER + 1, 6);
        int xOffset = 20;
        drawRuler(g, tickSize, xOffset, PANEL_BORDER, xOffset, rulerHeight + PANEL_BORDER + 1); 
        
      	getFrame().getMasterpanelToolbar().repaint();
      }
      catch (Exception e)
      {
        g.setColor(Color.black);
        g.drawString(e.toString(), getInsets().left, getInsets().top + 100);
      }
    }
  }
  
  // masterShape is panel outline path
  private void setClippingPath(Graphics g, AveryMasterpanel masterpanel)
  {
    double scalar = 1.0 / getTwipsPerPixel();
    // masterShape is panel outline path
    GeneralPath masterShape = masterpanel.getShape().scaledGraphicsPath(scalar);
    float strokeWidth =  0f;
    if (masterpanel.isBleedable())
    {
    	// if polygon clip to polygon bleed
    	if (masterpanel.getShape().getShapeString().equals("polygon"))
    	{
  			PolygonShape ps = (PolygonShape)masterpanel.getShape();
  			// masterShape is now polygon bleed outline path
  			masterShape = ps.scaledBleedGraphicsPath(scalar);
    	}
    	else
    	{
    		// for all other shapes with bleed scale path up by twice the bleed dimension
        strokeWidth =  AveryPanel.BLEED_DIMENSION * 20f * (float)scalar;
      	double widthScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().width) / masterShape.getBounds().width);
      	double heightScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().height) / masterShape.getBounds().height);
      	masterShape = masterpanel.getShape().scaledGraphicsPath(widthScalar, heightScalar);
    	}     	
    }
    
    // create view path and substract masterShape using Areas
    GeneralPath viewPath = new GeneralPath(this.getVisibleRect());
    Area areaView = new Area(viewPath);
    Area area1 = new Area(masterShape);
    area1.transform(new AffineTransform(1, 0, 0, 1, PANEL_BORDER - strokeWidth, PANEL_BORDER - strokeWidth));
    areaView.subtract(area1);
    GeneralPath gp = new GeneralPath(areaView);
    g.setClip(gp);
    //Color clipColor = new Color(0x00, 0x00, 0xff, 0x7f); // debug - useful to change color
    Color clipColor = new Color(0xff, 0xff, 0xff, 0x7f);
    g.setColor(clipColor);
    ((Graphics2D)g).fill(viewPath);
  	
  }
  
/*  public void paint(Graphics g)
  {
  	super.paint(g);
    if (this.getProject() != null)
    {
    	int pageNumber = Predesigner.frame.getPageView().getPageNumber();
    	AveryPage page = (AveryPage)project.getPage(pageNumber);
    	
    	if (page.getPaperimage() == null)
    	{
     		Rectangle2D rect = this.getVisibleRect();
     		Graphics2D graphics = (Graphics2D)g;

     		graphics.setBackground(page.getPaperColor() == null ? Color.white : page.getPaperColor());
    		graphics.clearRect(0, 0, (int)rect.getWidth(), (int)rect.getHeight());
    	}
     	
    	try
      {
        AveryMasterpanel masterpanel = getMasterpanel(getCurrentMasterpanelIndex());

        // draw the panel at the top of the window with a 30 pixel border
        int x = PANEL_BORDER;
        int y = PANEL_BORDER;
        
        double scalar = 1.0 / getTwipsPerPixel();
        double w = masterpanel.getWidth().doubleValue() * scalar;
        double h = masterpanel.getHeight().doubleValue() * scalar;
        
        int units = Config.units;
        double tickSize = 1440.0 / getTwipsPerPixel() / 4.0;      
        if (units == AveryUtils.CENTIMETERS)
        {
        	tickSize = 567.0 / getTwipsPerPixel() / 4.0;
        }

        g.translate(x, y);
        masterpanel.drawGuides((Graphics2D)g, scalar, Color.pink);
        masterpanel.draw((Graphics2D)g, (int)w, (int)h, null);
        if (textDefaultOutlines)
        {
          masterpanel.drawTextDefaults(g, scalar, new Color(128,255,128));  // light green outlines
        }      
        g.translate(-x, -y);

         if (outlines)
        {
          // draw panel field outlines
          tm.drawOutlines(g, x, y);
        }
         
         // lets have some fun and transparently clip outside of bleed or non bleed area
         float strokeWidth = 0f;
         
         GeneralPath masterShape = masterpanel.getShape().scaledGraphicsPath(scalar);     	
         if (masterpanel.isBleedable())
         {
         	// zoom master shape to match bleed
          	int bleedSizeTwips = (int)(AveryPanel.BLEED_DIMENSION * 20.0);
         	if (masterpanel.getShape().getShapeString().equals("polygon"))
				 	{
						PolygonShape ps = (PolygonShape)masterpanel.getShape();
				 		if (ps.hasCurves())
				 		{
				      strokeWidth = 0f; //(float)bleedSizeTwips * (float)scalar;
				    	//double widthScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().width) / masterShape.getBounds().width);
				    	//double heightScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().height) / masterShape.getBounds().height);
				    	double widthScalar = scalar;
				    	double heightScalar = scalar;
				 			
				    	// this path may have segments to exclude from bleed path
				    	masterShape = ps.scaledBleedGraphicsPath(widthScalar, heightScalar);     			
				 			masterShape = AveryPanel.getStraightSegmentPolygonBleedPath(masterShape, scalar, bleedSizeTwips, ps.getRotationSense());
				 		}
				 		else
				 		{
				 			// for paths without any curves
				 			masterShape = AveryPanel.getStraightSegmentPolygonBleedPath(masterShape, scalar, bleedSizeTwips, ps.getRotationSense());
				 		}
				 	}
				 	else
				 	{
				    strokeWidth = (float)bleedSizeTwips * (float)scalar;
				  	double widthScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().width) / masterShape.getBounds().width);
				  	double heightScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().height) / masterShape.getBounds().height);
				  	masterShape = masterpanel.getShape().scaledGraphicsPath(widthScalar, heightScalar);
				 	}     	
         }
        // create view path and substract masterShape using Areas
        GeneralPath viewPath = new GeneralPath(this.getVisibleRect());
        Area area1 = new Area(viewPath);
        Area area2 = new Area(masterShape);
        // translate masterShape
        area2.transform(new AffineTransform(1, 0, 0, 1, PANEL_BORDER - strokeWidth, PANEL_BORDER - strokeWidth));
        area1.subtract(area2);
        GeneralPath gp = new GeneralPath(area1);
        g.setClip(gp);
        //Color clipColor = new Color(0x00, 0x00, 0xff, 0x7f); // good for debugging
        Color clipColor = new Color(0xff, 0xff, 0xff, 0x7f);
        g.setColor(clipColor);
        ((Graphics2D)g).fill(viewPath);
        
        // rulers
        int wT = (int)(w / tickSize);
        int rulerWidth = (int)((double)wT * tickSize);
        if ((double)wT * tickSize != w)
        {
        	rulerWidth += (int)tickSize;
        }
        
        int hT = (int)(h / tickSize);
        int rulerHeight = (int)((double)hT * tickSize);
        if ((double)hT * tickSize != h)
        {
        	rulerHeight += (int)tickSize;
        }
        
        drawRuler(g, tickSize, PANEL_BORDER, 6, rulerWidth + PANEL_BORDER + 1, 6);
        int xOffset = 20;
        drawRuler(g, tickSize, xOffset, PANEL_BORDER, xOffset, rulerHeight + PANEL_BORDER + 1);
      }
      catch (Exception e)
      {
        g.setColor(Color.black);
        g.drawString(e.toString(), getInsets().left, getInsets().top + 100);
      }
    }
  }*/
 /* public void paint(Graphics g)
  {
  	super.paint(g);
    if (this.getProject() != null)
    {
    	int pageNumber = Predesigner.frame.getPageView().getPageNumber();
    	AveryPage page = (AveryPage)project.getPage(pageNumber);
    	
    	if (page.getPaperimage() == null)
    	{
     		Rectangle2D rect = this.getVisibleRect();
     		Graphics2D graphics = (Graphics2D)g;

     		graphics.setBackground(page.getPaperColor() == null ? Color.white : page.getPaperColor());
    		graphics.clearRect(0, 0, (int)rect.getWidth(), (int)rect.getHeight());
    	}
     	
    	try
      {
        AveryMasterpanel masterpanel = getMasterpanel(getCurrentMasterpanelIndex());

        // draw the panel at the top of the window with a 30 pixel border
        int x = PANEL_BORDER;
        int y = PANEL_BORDER;
        
        double scalar = 1.0 / getTwipsPerPixel();
        double w = masterpanel.getWidth().doubleValue() * scalar;
        double h = masterpanel.getHeight().doubleValue() * scalar;
        
        int units = Config.units;
        double tickSize = 1440.0 / getTwipsPerPixel() / 4.0;      
        if (units == AveryUtils.CENTIMETERS)
        {
        	tickSize = 567.0 / getTwipsPerPixel() / 4.0;
        }

        g.translate(x, y);
        masterpanel.drawGuides((Graphics2D)g, scalar, Color.pink);
        masterpanel.draw((Graphics2D)g, (int)w, (int)h, null);
        if (textDefaultOutlines)
        {
          masterpanel.drawTextDefaults(g, scalar, new Color(128,255,128));  // light green outlines
        }      
        g.translate(-x, -y);

         if (outlines)
        {
          // draw panel field outlines
          tm.drawOutlines(g, x, y);
        }
         
        // lets have some fun and transparently clip outside of bleed or non bleed area
         // this will not work for some polygons - expands internal 'tabs' to 
        float strokeWidth = 0f;
        GeneralPath masterShape = masterpanel.getShape().scaledGraphicsPath(scalar);       	
        if (masterpanel.isBleedable())
        {
        	// zoom master shape to match bleed
         	int bleedSizeTwips = 20 * (int)AveryPanel.BLEED_DIMENSION;
          strokeWidth = (float)bleedSizeTwips * (float)scalar;
        	double widthScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().width) / masterShape.getBounds().width);
        	double heightScalar = scalar * ((double)(2 * strokeWidth + masterShape.getBounds().height) / masterShape.getBounds().height);
        	masterShape = masterpanel.getShape().scaledGraphicsPath(widthScalar, heightScalar);
        }
        // create view path and substract masterShape using Areas
        GeneralPath viewPath = new GeneralPath(this.getVisibleRect());
        Area area1 = new Area(viewPath);
        Area area2 = new Area(masterShape);
        // translate masterShape
        area2.transform(new AffineTransform(1, 0, 0, 1, PANEL_BORDER - strokeWidth, PANEL_BORDER - strokeWidth));
        area1.subtract(area2);
        GeneralPath gp = new GeneralPath(area1);
        g.setClip(gp);
        Color clipColor = new Color(0xff, 0xff, 0xff, 0x7f);
        g.setColor(clipColor);
        ((Graphics2D)g).fill(viewPath);
        
        // rulers
        int wT = (int)(w / tickSize);
        int rulerWidth = (int)((double)wT * tickSize);
        if ((double)wT * tickSize != w)
        {
        	rulerWidth += (int)tickSize;
        }
        
        int hT = (int)(h / tickSize);
        int rulerHeight = (int)((double)hT * tickSize);
        if ((double)hT * tickSize != h)
        {
        	rulerHeight += (int)tickSize;
        }
        
        drawRuler(g, tickSize, PANEL_BORDER, 6, rulerWidth + PANEL_BORDER + 1, 6);
        int xOffset = 20;
        drawRuler(g, tickSize, xOffset, PANEL_BORDER, xOffset, rulerHeight + PANEL_BORDER + 1);
      }
      catch (Exception e)
      {
        g.setColor(Color.black);
        g.drawString(e.toString(), getInsets().left, getInsets().top + 100);
      }
    }
  }*/

  void drawRuler(Graphics g1, double tickSize, int x1, int y1, int x2, int y2)
  {
  	Graphics2D g = (Graphics2D)g1;
  	
  	g.setColor(Color.black);
  	
  	Font rulerFont = new Font("Arial", Font.PLAIN, 10);
  	g.setFont(rulerFont);
  	
  	AffineTransform oldTransform = g.getTransform();
  	
    double dx = x2 - x1, dy = y2 - y1;
    double len = Math.sqrt(dx*dx + dy*dy);
    AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
    at.concatenate(AffineTransform.getRotateInstance(Math.atan2(dy, dx)));
    g.transform(at);

    // Draw horizontal ruler starting in (0, 0)
    g.drawLine(0, 0, (int) len, 0);
    int j = 0;
    int majors = 0;
    
    for (double i = 0; i < len; i += tickSize)
    {
    	if (j != 0)
    	{
      	g.setColor(Color.black);
    		g.drawLine((int) i, -3, (int) i, 3);
    	}
    	else
    	{
      	g.setColor(Color.red);
    		g.drawLine((int) i, -6, (int) i, 6);
    		g.drawString(String.valueOf(majors++), (int)i-2, 18);
    	}
    		
    	j++;
    	if (j == 4)
    		j = 0;
    }
    
  	g.setTransform(oldTransform);
  }

  // creates a list of selection rectangles for current master panel
  public void buildFieldSelectionTools()
  {
    // clear matching lists
    tm.clearSelectionShapes();
    if (panelFields != null)
      panelFields = null;
    panelFields = new ArrayList();
    currentSelection = -1;

    try
    {
      AveryMasterpanel masterpanel = getCurrentMasterpanel();

      java.util.List pfs = masterpanel.getPromptSortedPanelFields();
      Iterator i = pfs.iterator();
      while (i.hasNext())
      {
        // weed out backgrounds
        AveryPanelField field = (AveryPanelField)i.next();

        if (!(field instanceof AveryBackground))
        {
          panelFields.add(field);
        }
      }
      
      double twipsPerPixel = getTwipsPerPixel();

      i = panelFields.iterator();
      while (i.hasNext())
      {
        AveryPanelField field = (AveryPanelField)i.next();
        int pixelX = (int)(field.getPosition().getX() / twipsPerPixel);
        int pixelY = (int)(field.getPosition().getY() / twipsPerPixel);
        int pixelWidth = (int)(field.getWidth().doubleValue() / twipsPerPixel);
        int pixelHeight = (int)(field.getHeight().doubleValue() / twipsPerPixel);
        // extra work if non zero rotation
        double rotation = field.getRotation().doubleValue();
        if (rotation != 0.0)
        {
          Dimension pixelSize = new Dimension(pixelWidth, pixelHeight);
          // find bounding rect and pivot point
          Point pixelPosition = rotateFieldRect(rotation, pixelSize);
          pixelX = pixelX + pixelPosition.x;
          pixelY = pixelY + pixelPosition.y;
          pixelWidth = pixelSize.width;
          pixelHeight = pixelSize.height;
        }
        
        boolean maintainAspect = false;
        if (field instanceof Drawing)
        {
          maintainAspect = ((Drawing)field).isMaintainAspect();
        }
        
        tm.addSelectionShape(pixelX, pixelY, pixelWidth, pixelHeight, maintainAspect, getFrame());
      }

      // panel dimension
      int w = (int)(masterpanel.getWidth().doubleValue() / twipsPerPixel);
      int h = (int)(masterpanel.getHeight().doubleValue() / twipsPerPixel);
      // resize masterpanelView card to make scrolling work
      // implies a border of 30 pixels around the panel
      getFrame().resizeCard1(w + 2 * PANEL_BORDER, h + 2 * PANEL_BORDER);
    }
    catch (Exception e)
    {
    }
  }

  // when selection changes location or size update panel field accordingly
  private void updateCurrentPanelField()
  {
  	/*if (currentSelections.size() > 0)
  	{
  		UndoableAction moveAllAction = new FieldMoveAllAction(panelFields, pickedShapes, currentSelections, 
  				this, tm, currentSelection); 	
  		Predesigner.getProject().executeAction(moveAllAction);
  		return;
  	}*/
  	
  	/*for (int i = 0; i < currentSelections.size(); i++)
  	{
      if (currentSelection > -1)
      {
        AveryPanelField apf = (AveryPanelField)panelFields.get(currentSelection);
        Point deltaPos = new Point((int)(tm.getShapeX() * getTwipsPerPixel()), (int)(tm.getShapeY() * getTwipsPerPixel()));
        deltaPos.x -= apf.getPosition().x;
        deltaPos.y -= apf.getPosition().y;
        
	  		int selection = (int)currentSelections.get(i);
	  		if (selection != currentSelection)
	  		{
	  			apf = (AveryPanelField)panelFields.get(selection);
	  			updatePanelField(apf, deltaPos);
	  			com.avery.predesigner.tools.Shape s = (com.avery.predesigner.tools.Shape)pickedShapes.get(i);
	  			int dX = (int)(deltaPos.x / getTwipsPerPixel());
	  			int dY = (int)(deltaPos.y / getTwipsPerPixel());
	  			s.move(s.getX() + dX, s.getY() + dY);   		
	  		}
      }
  	}*/

    if (currentSelection > -1)
    {
      AveryPanelField apf = (AveryPanelField)panelFields.get(currentSelection);
      
      if (!apf.isMovable())
      	return;

      int x = (int)(tm.getShapeX() * getTwipsPerPixel());
      int y = (int)(tm.getShapeY() * getTwipsPerPixel());
      Point p = new Point(x, y);
      int width = (int)(tm.getShapeWidth() * getTwipsPerPixel());
      int height = (int)(tm.getShapeHeight() * getTwipsPerPixel());
      double rotation = apf.getRotation().doubleValue();
      //System.out.println("width, height=" + width + "," + height);
      
      // will only allow resize of 90 degree increment rotations
      boolean bRotate0_180 = (rotation == 0.0 || rotation == 180.00);
      boolean bRotate90_270 = (rotation == 90.0 || rotation == 270.00);
      boolean bRotate = (bRotate90_270 || rotation == 180.0);
      
      double newX = p.getX();
      double newY = p.getY();
      
      double pX = apf.getPosition().x;
      double pY = apf.getPosition().y;
      double pWidth = apf.getWidth().doubleValue();
      double pHeight = apf.getHeight().doubleValue();
      
      /*if (pX > (p.getX() + pWidth))
      {
      	newX = -pWidth;
      }
      else if ((pX + pWidth) < p.getX())
      {
      	newX = pX + pWidth;
      }
      if (pY > (p.getY() + pHeight))
      {
      	newY = -pHeight;
      }
      else if ((pY + pHeight) < p.getY())
      {
      	newY = pY + pHeight;
      }
      p = new Point((int)newX, (int)newY);*/
      
      /*if (apf instanceof Drawing)
      {
      	Drawing drawing = (Drawing)apf;
      	if (drawing.getLineThickness() > 0)
      	{
      		if (bRotate90_270)
        		width = (int)(drawing.getLineThickness() * 20);
      		else if (bRotate0_180)
      		{
      			height = (int)(drawing.getLineThickness() * 20);
      		}
      		
      		if (bRotate0_180 || bRotate90_270)
      			tm.setShapeHeight((int)(drawing.getLineThickness() * 20 / getTwipsPerPixel()));
      	}
      }*/

      if (bRotate)
      {
        // extra work only for 90, 180, and 270
        Dimension size = new Dimension(width, height);
        Point position = rotateFieldRect(-rotation, size);

        width = size.width;
        height = size.height;

        //System.out.println("bRotate width, height=" + width + "," + height);
        // 90 degree integral hocus pocus
        if ((rotation > 45.0 && rotation < 135.0) ||
            (rotation > 225.0 && rotation < 315.0))
        {
          p.x = p.x - position.y;
          p.y = p.y - position.x;
        }
        else
        {
          p.x = p.x - position.x;
          p.y = p.y - position.y;
        }
      }
      
      // set absolute limits on size
      if (apf instanceof Drawing)
      {
	      if (width < 60)
	      	width = 60;
	      if (height < 60)
	      	height = 60; 	
      }
      else
      {
	      if (width < 81)
	      	width = 81;
	      if (height < 81)
	      	height = 81;
      }
      
      if (rotation == 0.0 || bRotate)
      {
        // allow resize and/or move
      	if (ctrlPressed && !bRotate && apf instanceof AveryImage && !(apf instanceof AveryBackground))
      	{
      		ctrlPressed = false;
      		
      		// make aspect correct?
      		///AveryImage ai = (AveryImage)apf;
      		///double scalar = ai.getWidth() / ai.getHeight();
        	//System.out.println(scalar);
      		///height = (int)(width / scalar);
      		///tm.setShapeHeightAbsolute((int)((double)height / getTwipsPerPixel()));
      	}
      	if (currentSelections.size() <= 0)
      	{
      		UndoableAction action = new FieldMouseAction(apf, p, width, height);
      		Predesigner.getProject().executeAction(action);
      	}
      	else
      	{
      		UndoableAction action = new FieldMouseAction(apf, p, width, height, currentSelections,
      				currentSelection, pickedShapes, panelFields, getTwipsPerPixel());
      		Predesigner.getProject().executeAction(action);     		
      	}
      }
      else
      {
        // not a 90 degree integral angle - only allow moving object
        Point last = apf.getPosition();
        p.x = last.x + (p.x - lastPosition.x);
        p.y = last.y + (p.y - lastPosition.y);
        
        // don't change width and height, only position
      	//if (currentSelections.size() <= 0)
      	//{
      		UndoableAction action = new FieldMouseAction(apf, p);
      		Predesigner.getProject().executeAction(action);
      	//}
      }
      
      if (currentSelections.size() > 0)
      {
    		//currentSelections.clear();
    		//pickedShapes.clear();
  	    //Predesigner.getFrame().updateView();    	
      }
      updateStatusDisplay();
    }
  }

  public void updatePanelField(AveryPanelField apf, Point deltaPos)
  {
    //AveryPanelField apf = (AveryPanelField)panelFields.get(currentSelection);

    int x = (int)apf.getPosition().x + deltaPos.x; //(int)(tm.getShapeX() * getTwipsPerPixel());
    int y = (int)apf.getPosition().y + deltaPos.y; //(int)(tm.getShapeY() * getTwipsPerPixel());
    Point p = new Point(x, y);
    int width = apf.getWidth().intValue(); //tm.getShapeWidth() * (int)getTwipsPerPixel();
    int height = apf.getHeight().intValue(); //tm.getShapeHeight() * (int)getTwipsPerPixel();
    double rotation = apf.getRotation().doubleValue();
    
    // will only allow resize of 90 degree increment rotations
    boolean bRotate0_180 = (rotation == 0.0 || rotation == 180.00);
    boolean bRotate90_270 = (rotation == 90.0 || rotation == 270.00);
    boolean bRotate = (bRotate90_270 || rotation == 180.0);
      
    if (apf instanceof Drawing)
    {
    	Drawing drawing = (Drawing)apf;
    	if (drawing.getLineThickness() > 0)
    	{
    		if (bRotate90_270)
      		width = (int)(drawing.getLineThickness() * 20);
    		else if (bRotate0_180)
    		{
    			height = (int)(drawing.getLineThickness() * 20);
    		}
    		
    		//if (bRotate0_180 || bRotate90_270)
    			//tm.setShapeHeight((int)(drawing.getLineThickness() * 20 / getTwipsPerPixel()));
    	}
    }

    if (bRotate)
    {
      // extra work only for 90, 180, and 270
      Dimension size = new Dimension(width, height);
      Point position = rotateFieldRect(-rotation, size);

      width = size.width;
      height = size.height;

      // 90 degree integral hocus pocus
      if ((rotation > 45.0 && rotation < 135.0) ||
          (rotation > 225.0 && rotation < 315.0))
      {
        p.x = p.x - position.y;
        p.y = p.y - position.x;
      }
      else
      {
        p.x = p.x - position.x;
        p.y = p.y - position.y;
      }
    }
      
    if (rotation == 0.0 || bRotate)
    {
      // allow resize and/or move
    	if (ctrlPressed && !bRotate && apf instanceof AveryImage && !(apf instanceof AveryBackground))
    	{
    		ctrlPressed = false;
    		
    		// make aspect correct?
    		///AveryImage ai = (AveryImage)apf;
    		///double scalar = ai.getWidth() / ai.getHeight();
      	//System.out.println(scalar);
    		///height = (int)(width / scalar);
    		//tm.setShapeHeightAbsolute((int)((double)height / getTwipsPerPixel()));
    	}
      UndoableAction action = new FieldMouseAction(apf, p, width, height);
      Predesigner.getProject().executeAction(action);
    }
    else
    {
      // not a 90 degree integral angle - only allow moving object
      Point last = apf.getPosition();
      p.x = last.x + (p.x - lastPosition.x);
      p.y = last.y + (p.y - lastPosition.y);
      
      // don't change width and height, only position
      UndoableAction action = new FieldMouseAction(apf, p);
      Predesigner.getProject().executeAction(action);
    }
    
    updateStatusDisplay();
 
  }
  
  public void mouseClicked(MouseEvent e)
  {
  }

  public void mouseEntered(MouseEvent e)
  {
  }
  
  public void mouseExited(MouseEvent e)
  {
  }

  public void clearSelection()
  {
  	tm.selectShape(null, -100, -100);
  	currentSelection = -1;
  	repaint();
  }
  
  public void mousePressed(MouseEvent e)
  {
    if (e.getClickCount() == 1) /// && outlines)
    {   	
    	// request keyboard focus for the master panel view so arrow keys will work
    	this.requestFocusInWindow();
    	
      currentSelection = tm.selectShape(e, e.getX(), e.getY());
      
      if (currentSelection > -1)
      {
      	AveryPanelField selectedField = (AveryPanelField)panelFields.get(currentSelection);
      	getFrame().getFieldsMenu().setSelectedField(selectedField);
      	if (selectedField instanceof AveryTextblock)
      	{
      		getFrame().getMasterpanelToolbar().getTextEditPanel().setTextLines((AveryTextblock)selectedField);
      	}
      }
      else
      {
    		getFrame().getMasterpanelToolbar().getTextEditPanel().clearText();      	
      }
      
    	if (e.isShiftDown() || e.isAltDown() || e.isControlDown() || com.avery.predesigner.tools.Shape.SHAPE_enterKeyPressed)
    	{
    		//System.out.println("shift is down");
    		
        Iterator iter = currentSelections.iterator();
        boolean bFound = false;
        while (iter.hasNext())
        {
        	int entry = (int)iter.next();
        	if (entry == currentSelection)
        	{
        		bFound = true;
        		break;
        		/*iter.remove();
        		tm.getPickedShape().highlight(false);  // todo remove entry from pickedshape
          	Iterator iter1 = pickedShapes.iterator();
            while (iter1.hasNext())
            {
            	com.avery.predesigner.tools.Shape nextShape = (com.avery.predesigner.tools.Shape)(iter1.next());
            	if (nextShape.equals(tm.getPickedShape()))
            	{
            		iter1.remove();
            		break;
            	}
            }
        		break;*/
        	}
        }
        if (!bFound)
        {
      		//System.out.println("not found currentSelection=" + currentSelection);
      		currentSelections.add(currentSelection);
      		pickedShapes.add(tm.getPickedShape());
        }    
    	}
    	else
      {
      	tm.unHighLightAll();
    		//System.out.println("clear selections");
      	currentSelections.clear();
      	pickedShapes.clear();
      }
      
      tm.setDownPosition(e.getX(), e.getY());
      if (currentSelection >= 0)
      {
        lastPosition.x = (int)(tm.getShapeX() * getTwipsPerPixel());
        lastPosition.y = (int)(tm.getShapeY() * getTwipsPerPixel());
        
/*        if (Predesigner.getFrame().getExtendedState() == JFrame.MAXIMIZED_BOTH)
        {
	        AveryPanelField field = (AveryPanelField)panelFields.get(currentSelection);
	        if (field instanceof AveryTextblock)
	        { 
	        	if (textEditPanel == null)
	        	{
	        		textEditPanel = new TextEditPanel();
	        	}
	        	AveryTextblock atb = (AveryTextblock)field;
	        	textEditPanel.setTextLines(atb);
	        	textEditPanel.setVisible(true);   
	        }
	        //else
	        //{
	        	//if (textEditPanel != null)
	        		//textEditPanel.setVisible(false);
	        //}
        }*/
      }
      updateStatusDisplay();
      repaint();
      return;
    }

    if (e.getClickCount() == 2 && hasSelection()) // && outlines)
    {
    	// request keyboard focus for the master panel view so arrow keys will work
    	this.requestFocusInWindow();
    	
    	//if (textEditPanel != null)
    		//textEditPanel.setVisible(false);
    	
      AveryPanelField field = (AveryPanelField)panelFields.get(currentSelection);

      // 12/04/03 the rest of this method is an exact rubberstamp of the
      // FieldMenuItem onClick() method
      FieldEditorPanel editor = FieldEditorPanel.getEditorPanel(field, false);

      // loop until they get it right or cancel
      while (JOptionPane.showConfirmDialog(getFrame(), editor, field.getClass().getName(),
        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
        == JOptionPane.OK_OPTION)
      {
        String errorMessage = editor.updateField();
        if (errorMessage == null)   // this is good!
        {
          getFrame().onFieldChanged();
          updateStatusDisplay();
          if (field instanceof AveryTextblock)
          {
          	getFrame().getMasterpanelToolbar().getTextEditPanel().setTextLines((AveryTextblock)field);
          }
          break;
        }
        // show the error message and try again
        JOptionPane.showMessageDialog(getFrame(), errorMessage, "Field Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  private boolean dragging = false;
  private boolean ctrlPressed = false;

  public void mouseReleased(MouseEvent e)
  {
    if (dragging) /// && outlines)
    {
      AveryPanelField apf = (AveryPanelField)panelFields.get(currentSelection);
      if ((e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK)
      {
      	ctrlPressed = true;
      	//System.out.println("ctrl is down");
      }
      
      updateCurrentPanelField();
      //System.out.println(getTwipsPerPixel());
      //System.out.println(apf.getPosition().toString());
      //System.out.println(getFrame().getWidth() + "," + getFrame().getHeight());
      //System.out.println(this.getX() + "," + this.getY());
      //System.out.println(this.getWidth() + "," + this.getHeight());
      
      tm.mouseUp();
      
      dragging = false;
      ctrlPressed = false;
      
      
      
/*      if (apf instanceof AveryImage || apf instanceof AveryBackground)
      {
      	if (isDPITooSmall((AveryImage)apf))
        //if (((AveryImage)apf).isDPITooSmall())
      	{
      		// show error...
      		System.out.println("bad resolution");
      	}
      }*/
    }
  }

  public void mouseDragged(MouseEvent e)
  {
    ///if (outlines)
    {
      if (tm.dragEvent(e))
      {
        AveryPanelField apf = (AveryPanelField)panelFields.get(currentSelection);
        if (!apf.isMovable())
        {
        	clearSelection();
        	return;
        }
        dragging = true;   // panel will be updated in mouseReleased
        repaint();   // need to paint new position of rubberband box
      }
    }
  }

  public void mouseMoved(MouseEvent e)
  {
  }
  
  public void keyPressed(KeyEvent ke)
  {
    if (outlines)
    {
      boolean validKey = tm.keyPressedEvent(ke);
      if (validKey)
      {
      	if (currentSelections.size() < 2)
      		updateCurrentPanelField();
      }
      //System.out.println("2 times=" + ke.getKeyCode());
      if (ke.getKeyCode() == KeyEvent.VK_ENTER)
      {
        //System.out.println(ke.getKeyCode());
        com.avery.predesigner.tools.Shape.SHAPE_enterKeyPressed = true;
      }
    }
  }

  public void keyReleased(KeyEvent ke)
  {
    //System.out.println("released");
    com.avery.predesigner.tools.Shape.SHAPE_enterKeyPressed = false;
  }

  public void keyTyped(KeyEvent ke)
  {
  }

  // method must exist and return true to enable keyboard events
  public boolean isFocusTraversable()
  {
    return true;
  }

  /**
   * gets the bounding box of a rotated rectangle
   * (assumes rect origin at 0,0)
   * @param theta - angle of rotation
   * @param size - size of rect, will be altered on return
   * @return position of rotated rect's bounding box
   */
  Point rotateFieldRect(double theta, java.awt.Dimension size)
  {
    Point position = new Point(0,0);
    double angle = theta * Math.PI / 180.0;

    // find the rectangular coordinates of the rotated field,
    // rotating about upper left hand corner
    AffineTransform at1 = new AffineTransform();
    at1.rotate(-angle);
    
    // Create a rotated rectangle shape
    Shape shape = at1.createTransformedShape(
    		new Rectangle2D.Double(0.0, 0.0, size.getWidth(), size.getHeight()));
    // get its bounding rect
    Rectangle2D r2d = shape.getBounds2D();
    
    // return size for caller
    size.width = (int)r2d.getWidth();
    size.height = (int)r2d.getHeight();
    
    // calculate the upper left from the rotated rectangle
    position.x = (int)(r2d.getMinX());
    position.y = (int)(r2d.getMinY());

    return position;
  }
  
  boolean hasSelection()
  {
    return currentSelection > -1;
  }
  
  void cutSelection()
  {
    if (hasSelection())
    {
      AveryPanelField field = (AveryPanelField)panelFields.get(currentSelection);
      UndoableAction action = new FieldCutAction(getCurrentMasterpanel(), field);
      Predesigner.getProject().executeAction(action);    
      updateStatusDisplay();
    }  	
  }
  
  void copySelection()
  {
    if (hasSelection())
    {
      AveryPanelField field = (AveryPanelField)panelFields.get(currentSelection);
      UndoableAction action = new FieldCopyAction(field);
      Predesigner.getProject().executeAction(action);    
    }  	
  }
  
  void paste()
  {
    UndoableAction action = new FieldPasteAction(getCurrentMasterpanel());
    Predesigner.getProject().executeAction(action);
    updateStatusDisplay();
  }
  
  // copyMasterpanel is a temp that will contain the fields to paste
  void copyAll(JMenuItem pasteAllMenuItem)
  {
		AveryMasterpanel amp = getCurrentMasterpanel();
  	if (getCurrentMasterpanel().getFieldIterator().hasNext())
  	{
    	if (!currentSelections.isEmpty())
    	{
    		AveryMasterpanel mp = amp.cloneMaster();
    		for (int i = 0; i < currentSelections.size(); i++)
    		{
    			int cs = (int)currentSelections.get(i);
    			//System.out.println("cs=" + cs);
    			AveryPanelField field = (AveryPanelField)panelFields.get(cs);
    			AveryPanelField cloneField = field.deepClone();
    			//cloneField.setID("");
    			//cloneField.setContentID("");
    			//cloneField.setStyleID("");
    			mp.addField(cloneField);
    			//mp.removePanelField(field.getDescription());
    		}
    		copyMasterpanel = mp;
      }
    	else
    	{
    		copyMasterpanel = amp;
    	}
  		pasteAllMenuItem.setEnabled(true);
  	}
  }
  
  void pasteAll()
  {
  	AveryMasterpanel toMasterpanel = getCurrentMasterpanel();
  	
  	UndoableAction action = new PasteAllAction(copyMasterpanel, toMasterpanel);
    Predesigner.getProject().executeAction(action);
    
		currentSelections.clear();
		pickedShapes.clear();
    updateStatusDisplay();	
  }
  
  void deleteSelection()
  {
  	if (currentSelections.size() > 0)
  	{
      UndoableAction action = new FieldDeleteAllAction(getCurrentMasterpanel(), panelFields, currentSelections);
      Predesigner.getProject().executeAction(action);
      
  		currentSelections.clear();
  		pickedShapes.clear();
	    Predesigner.getFrame().updateView();
  		return;
  	}
  	
    if (hasSelection())
    {
      AveryPanelField field = (AveryPanelField)panelFields.get(currentSelection);
      UndoableAction action = new FieldDeleteAction(getCurrentMasterpanel(), field);
      Predesigner.getProject().executeAction(action);    
      updateStatusDisplay();
    }
  }
  
  void centerSelection()
  {
  	if (currentSelections.size() > 0)
  	{
  		UndoableAction action = new FieldCenterAllAction(getCurrentMasterpanel(), currentSelections, 0);
      Predesigner.getProject().executeAction(action);
      
  		currentSelections.clear();
  		pickedShapes.clear();
	    Predesigner.getFrame().updateView();
      updateStatusDisplay();
  		return;
  	}
  	
    if (hasSelection())
    {
      AveryPanelField field = (AveryPanelField)panelFields.get(currentSelection);
      UndoableAction action = new FieldCenterAction(getCurrentMasterpanel(), field);
      Predesigner.getProject().executeAction(action);
      updateStatusDisplay();
    }
  }
  
  void centerSelectionHorz()
  {
  	if (currentSelections.size() > 0)
  	{
  		UndoableAction action = new FieldCenterAllAction(getCurrentMasterpanel(), currentSelections, 1);
      Predesigner.getProject().executeAction(action);
      
  		currentSelections.clear();
  		pickedShapes.clear();
	    Predesigner.getFrame().updateView();
  		return;
  	}
  	
    if (hasSelection())
    {
      AveryPanelField field = (AveryPanelField)panelFields.get(currentSelection);
      UndoableAction action = new FieldCenterHorzAction(getCurrentMasterpanel(), field);
      Predesigner.getProject().executeAction(action);
      updateStatusDisplay();
    }
  }
  
  void centerSelectionVert()
  {
  	if (currentSelections.size() > 0)
  	{
  		UndoableAction action = new FieldCenterAllAction(getCurrentMasterpanel(), currentSelections, 2);
      Predesigner.getProject().executeAction(action);
      
  		//currentSelections.clear();
  		//pickedShapes.clear();
	    Predesigner.getFrame().updateView();
  		return;
  	}
  	
    if (hasSelection())
    {
      AveryPanelField field = (AveryPanelField)panelFields.get(currentSelection);
      UndoableAction action = new FieldCenterVertAction(getCurrentMasterpanel(), field);
      Predesigner.getProject().executeAction(action);
      updateStatusDisplay();
    }
  }
  
  void alignLeft()
  {
  	if (currentSelections.size() > 1)
  	{
  		UndoableAction action = new FieldAlignAction(getCurrentMasterpanel(), currentSelections, currentSelection, 0);
      Predesigner.getProject().executeAction(action);
      
  		currentSelections.clear();
  		pickedShapes.clear();
	    Predesigner.getFrame().updateView();
      updateStatusDisplay();
  		return;
  	}
  }
  
  void alignRight()
  {
  	if (currentSelections.size() > 1)
  	{
  		UndoableAction action = new FieldAlignAction(getCurrentMasterpanel(), currentSelections, currentSelection, 1);
      Predesigner.getProject().executeAction(action);
      
  		currentSelections.clear();
  		pickedShapes.clear();
	    Predesigner.getFrame().updateView();
      updateStatusDisplay();
  		return;
  	}
  }
  
  void alignTop()
  {
  	if (currentSelections.size() > 1)
  	{
  		UndoableAction action = new FieldAlignAction(getCurrentMasterpanel(), currentSelections, currentSelection, 2);
      Predesigner.getProject().executeAction(action);
      
  		currentSelections.clear();
  		pickedShapes.clear();
	    Predesigner.getFrame().updateView();
      updateStatusDisplay();
  		return;
  	}
  }
  
  void alignBottom()
  {
  	if (currentSelections.size() > 1)
  	{
  		UndoableAction action = new FieldAlignAction(getCurrentMasterpanel(), currentSelections, currentSelection, 3);
      Predesigner.getProject().executeAction(action);
      
  		currentSelections.clear();
  		pickedShapes.clear();
	    Predesigner.getFrame().updateView();
      updateStatusDisplay();
  		return;
  	}
  }
  
  void alignWidth()
  {
  	if (currentSelections.size() > 1)
  	{
  		UndoableAction action = new FieldAlignDimensionAction(getCurrentMasterpanel(), currentSelections, currentSelection, 0);
      Predesigner.getProject().executeAction(action);
      
  		currentSelections.clear();
  		pickedShapes.clear();
	    Predesigner.getFrame().updateView();
      updateStatusDisplay();
  		return;
  	}
  }
  
  void alignHeight()
  {
  	if (currentSelections.size() > 1)
  	{
  		UndoableAction action = new FieldAlignDimensionAction(getCurrentMasterpanel(), currentSelections, currentSelection, 1);
      Predesigner.getProject().executeAction(action);
      
  		currentSelections.clear();
  		pickedShapes.clear();
	    Predesigner.getFrame().updateView();
      updateStatusDisplay();
  		return;
  	}
  }
  
  private int zoomNumerator = 2;
  private int zoomDenominator = 2;

  public double getTwipsPerPixel()
  {
    return (15. * zoomNumerator) / (double)zoomDenominator;
  }
  
  private void noZoom()
  {
    zoomNumerator = zoomDenominator = 2;
  }

  private void zoomin()
  {
    if (zoomNumerator > 2)
    {
      zoomNumerator--;
      zoomDenominator = 2;
    }
    else
    {
      zoomNumerator = 2;
      if (zoomDenominator < 20)
        ++zoomDenominator;
    }
  }
  
  private void zoomout()
  {
    if (zoomDenominator > 2)
    {
      zoomNumerator = 2;
      --zoomDenominator;
    }
    else
    {
      if (zoomNumerator < 20)
        ++zoomNumerator;
      zoomDenominator = 2;
    }
  }
  
  void zoomIn()
  {
    zoomin();
    buildFieldSelectionTools();
    updateStatusDisplay();
  }
  
  void zoomOut()
  {
    zoomout();
    buildFieldSelectionTools();
    updateStatusDisplay();
  }
  
  int getZoomPercent()
  {
  	return (int)(1500.0 / getTwipsPerPixel());
  }
  
  void updateStatusDisplay()
  {
  	if (getProject() == null)
  	{
  		return;
  	}
  	
    String status = "Zoom " +	getZoomPercent() + "%   ";
    
    if (outlines && hasSelection())
    {
      AveryPanelField field = (AveryPanelField)panelFields.get(currentSelection);
      status += field.getDescription();
    	status += "  x:" + Predesigner.twipsToUnitString(field.getPosition().getX());
    	status += "  y:" + Predesigner.twipsToUnitString(field.getPosition().getY());
    	status += "  w:" + Predesigner.twipsToUnitString(field.getWidth().doubleValue());
    	status += "  h:" + Predesigner.twipsToUnitString(field.getHeight().doubleValue());
    }

  	Predesigner.getFrame().updateStatus(status);
  }
  
  private void zoomOutToFit()
  {
    noZoom();
  	int masterWidth = getCurrentMasterpanel().getWidth().intValue();
  	int maxWidth = Predesigner.getFrame().getContentPane().getWidth();
  	int retries = 0;
  	
  	while (masterWidth / getTwipsPerPixel() > maxWidth && retries++ < 10)
  	{
  		zoomout();
  	}
  	
  	int masterHeight = getCurrentMasterpanel().getHeight().intValue();
  	int maxHeight = Predesigner.getFrame().getContentPane().getHeight();
  	retries = 0;
  	while (masterHeight / getTwipsPerPixel() > maxHeight && retries++ < 10)
  	{
  		zoomout();
  	}
  }
  
  public boolean isDPITooSmall(AveryImage aImage)
  {
  	AveryMasterpanel master = this.getCurrentMasterpanel();
  	
  	int srcWidth = aImage.getSrcWidth();
  	int srcHeight = aImage.getSrcHeight();	
  	int srcMaxDim = srcWidth > srcHeight ? srcWidth : srcHeight;
  	int panelWidth = master.getWidth().intValue(); 
  	int panelHeight = master.getHeight().intValue(); 
  	int panelMaxDim = panelWidth > panelHeight ? panelWidth : panelHeight;
  	
  	if (srcWidth <= 0 || srcHeight <= 0)
  		return false;
  	if (panelWidth <= 0 || panelHeight <= 0)
  		return false;
  	
  	int dpi  = MIN_DPI * (int)((double)srcMaxDim / ((double)panelMaxDim / TWIPS_PER_PIXEL));
  	
  	if (dpi< MIN_DPI)
  		return true;
  	
  	return false;
  }
}