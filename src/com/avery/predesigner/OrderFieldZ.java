/*
 * OrderFieldZ.java Created on Nov 20, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import com.avery.project.AveryPanelField;

/**
 * @author leeb
 * Nov 20, 2007
 * OrderFieldZ
 */
class OrderFieldZ extends JPanel
{
  private JList jList;
  private DefaultListModel listModel = new DefaultListModel();
  private JButton upButton = new JButton("Move Up");
  private JButton downButton = new JButton("Move Down");
  private JButton topButton = new JButton("Move Top");
  private JButton bottomButton = new JButton("Move Bottom");
  private JButton foregroundButton = new JButton("Foreground...");
  
  static final double FOREGROUND_Z = 32000;
  AveryPanelField foregroundField = null;
  
  private int selectedIndex = 0;
  
  OrderFieldZ(List fieldList, AveryPanelField selectedField)
  {
    //Iterator iterator = fieldList.iterator();
    int index = 0;
    //while (iterator.hasNext())
    for (int i = fieldList.size()-1; i > -1; i--)
    {
      AveryPanelField field = (AveryPanelField)fieldList.get(i);
      //AveryPanelField field = (AveryPanelField)iterator.next();
      if (field.getZLevel() == FOREGROUND_Z)
      {
        foregroundField = field;
      }
      
      if (selectedField != null && field.getDescription().equals(selectedField.getDescription()))
      	selectedIndex = index;
      
      listModel.addElement(field);
      index++;
    }
    
    jList = new JList(listModel);
    jList.setBorder(new BevelBorder(BevelBorder.LOWERED));
    jList.setSelectedIndex(selectedIndex);
    
    upButton.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (onUpButton())
        {
          jList.invalidate();
          jList.repaint();
        }
      }
    });

    downButton.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (onDownButton())
        {
          jList.invalidate();
          jList.repaint();
        }
      }
    });
    
    topButton.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (onTopButton())
        {
          jList.invalidate();
          jList.repaint();
        }
      }
    });
      
    bottomButton.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (onBottomButton())
        {
          jList.invalidate();
          jList.repaint();
        }
      }
    });
    
    foregroundButton.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onForegroundButton();
      }
    });

    GridLayout buttonLayout = new GridLayout(6,1);
    buttonLayout.setVgap(5);
    JPanel buttonPanel = new JPanel(buttonLayout);
    buttonPanel.add(upButton);
    buttonPanel.add(downButton);
    buttonPanel.add(topButton);
    buttonPanel.add(bottomButton);
    buttonPanel.add(new JPanel());
    buttonPanel.add(foregroundButton);
        
    GridLayout gridLayout = new GridLayout(1, 2);
    gridLayout.setHgap(10);
    JPanel grid = new JPanel(gridLayout);
    grid.add(jList);
    grid.add(buttonPanel);
    this.add(grid);
  }
  
  Object[] getReorderedList()
  {
    return listModel.toArray();
  }
  
  private boolean onUpButton()
  {
    int index = jList.getSelectedIndex();
    if (index > 0)
    {
      if (listModel.elementAt(index).equals(foregroundField))
      {
        if (JOptionPane.showConfirmDialog(this, "Remove Foreground Lock?", "Foreground", JOptionPane.YES_NO_OPTION)
            == JOptionPane.YES_OPTION)
        {
          foregroundField = null;   // byebye foreground
        }
        else  // keep foreground lock, don't move element up
        {
          return false;
        }
      }
      
      Object element = listModel.getElementAt(index - 1);

      listModel.setElementAt(listModel.getElementAt(index), index - 1);
      listModel.setElementAt(element, index);
      
      jList.setSelectedIndex(index - 1);
      return true;
    }
    return false;
  }
  
  /**
   * @return true if the current selection was moved down, false at end of list
   */
  private boolean onDownButton()
  {
    int size = listModel.getSize(); 
    int index = jList.getSelectedIndex();
    if (index < (size - 1) && index > -1)
    {
      Object element = listModel.getElementAt(index + 1);
      listModel.setElementAt(listModel.getElementAt(index), index + 1);
      listModel.setElementAt(element, index);
      
      jList.setSelectedIndex(index + 1);
      return true;
    }
    return false;
  }
  /**
   * @return true if the current selection was moved down, false at end of list
   */
  private boolean onTopButton()
  {
    int size = listModel.getSize(); 
    int index = jList.getSelectedIndex();
    if (index < size && index > 0)
    {
      Object element = listModel.getElementAt(index);
      listModel.removeElementAt(index);
      listModel.insertElementAt(element, 0);      
      jList.setSelectedIndex(0);
      return true;
    }
    return false;
  }
  
  /**
   * @return true if the current selection was moved down, false at end of list
   */
  private boolean onBottomButton()
  {
    int size = listModel.getSize(); 
    int index = jList.getSelectedIndex();
    if (index < (size - 1) && index > -1)
    {
      Object element = listModel.getElementAt(index);
      listModel.removeElementAt(index);
      listModel.insertElementAt(element, size-1);      
      jList.setSelectedIndex(size-1);
      return true;
    }
    return false;
  }
  
  private void onForegroundButton()
  {
    if (JOptionPane.showConfirmDialog(this, "Lock to Foreground?", "Foreground", JOptionPane.YES_NO_OPTION)
        == JOptionPane.YES_OPTION)
    {
      boolean listChanged = false;
      while (onDownButton())
      {
        listChanged = true;
      }
      
      foregroundField = (AveryPanelField)listModel.getElementAt(listModel.getSize() - 1);
      
      if (listChanged)
      {
        jList.invalidate();
        jList.repaint();
      }
    }
  }
}
