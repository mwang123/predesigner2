/*
 * ColorChooser.java Created on Jan 16, 2008 by leeb
 * Copyright 2008 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.predesigner;

import java.awt.Color;

import javax.swing.JColorChooser;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.colorchooser.ColorSelectionModel;

import com.avery.predesign.StyleSet;

/**
 * @author leeb
 * Jan 16, 2008
 * ColorChooser - a singleton JColorChooser used by several classes,
 * includes a color picker tab with the 3 colors from a StyleSet.
 */
public class ColorChooser extends JColorChooser
{
  static ColorChooser colorChooser = null;
  static StyleSetColorChooserPanel styleSetColorPicker = null;
  
  /**
   * private method constructs the singleton object
   */
  static private void makeTheColorChooser()
  {
    colorChooser = new ColorChooser();

    styleSetColorPicker = new StyleSetColorChooserPanel();
      
    AbstractColorChooserPanel originals[] = colorChooser.getChooserPanels();
    AbstractColorChooserPanel newpanels[] = 
      { styleSetColorPicker, originals[0], originals[1], originals[2], originals[3] };
      
    colorChooser.setChooserPanels(newpanels);
  }

  /**
   * @return Our colorChooser with the StyleSet panel set to the current Predesigner.styleSet colors
   */
  public static ColorChooser getColorChooser()
  {   
    return getColorChooser(Predesigner.styleSet);
  }
  
  /**
   * 
   * @param styleSet
   * @return Our colorChooser with the StyleSet panel set to the specified StyleSet's colors
   */
  public static ColorChooser getColorChooser(StyleSet styleSet)
  {
    if (colorChooser == null)
    {
      makeTheColorChooser();
    }
    
    styleSetColorPicker.updateColors(styleSet);
    
    return colorChooser;
  }
  
  public void setColor(Color color)
  {
    super.setColor(color);
    styleSetColorPicker.updateChooser();
  }
  
  /***********************************************************************************/
  /**
   * super constructors are private - access through singleton getColorChooser methods
   */
  private ColorChooser()
  {
    super();
  }

  /**
   * overridden to hide from application
   * @param initialColor
   */
  private ColorChooser(Color initialColor)
  {
    super(initialColor);
  }

  /**
   * overridden to hide from application
   * @param model
   */
  private ColorChooser(ColorSelectionModel model)
  {
    super(model);
  }

}
