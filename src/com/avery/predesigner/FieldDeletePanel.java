package com.avery.predesigner;

import java.util.*;
import javax.swing.*;
import com.borland.jbcl.layout.*;
import com.avery.project.AveryPanelField;
import com.avery.project.AverySuperpanel;

/**
 * <p>Title: APE - the AveryProject Editor</p>
 * <p>Description: panel provides UI for deleting fields from a masterpanel</p>
 * <p>Copyright: Copyright 2003 Avery Dennison Corporation, all rights reserved</p>
 * <p>Company: Avery Dennison Corporation</p>
 * @author Bob Lee
 * @version 2
 */

class FieldDeletePanel extends JPanel
{
	private static final long serialVersionUID = 4235675472020119282L;
	private AverySuperpanel averyPanel;
  private List checkboxes = new ArrayList();
  
  FieldDeletePanel(AverySuperpanel averyPanel)
  {
    this.averyPanel = averyPanel;
    init();
  }
  
  void init()
  {
    setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP));

    this.add(new JLabel("Select the field(s) to be deleted:"));
    
    Iterator iterator = averyPanel.getFieldIterator();
    while (iterator.hasNext())
    {
      AveryPanelField field = (AveryPanelField)iterator.next();
      JCheckBox checkbox = new JCheckBox(field.getPrompt());
      
      // keep checkboxes in a list
      checkboxes.add(checkbox);
      
      // add to the display panel
      this.add(checkbox);
    }
  }
  
  List getDeleteList()
  {
    List deleteList = new ArrayList();
    Iterator iterator = checkboxes.iterator();
    while (iterator.hasNext())
    {
      JCheckBox check = (JCheckBox)iterator.next();
      if (check.isSelected())
      {
        deleteList.add(check.getText());
      }
    }
    return deleteList;
  }
}