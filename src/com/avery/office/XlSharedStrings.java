/*
 * XlSharedStrings.java Created on Nov 27, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.office;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Element;

/**
 * @author leeb
 * Nov 27, 2007
 * XlSharedStrings - hold the string table for an Excel Workbook
 */
class XlSharedStrings
{
  private List strings;
  
  /**
   * Construct from a JDOM XML element
   * @param stringTableElement named "sst"
   */
  XlSharedStrings(Element stringTableElement)
  {
    List siList = stringTableElement.getChildren("si", XlWorkbook.spreadsheetml);
    
    strings = new ArrayList(siList.size());
    
    Iterator iterator = siList.iterator();
    while (iterator.hasNext())
    {
      strings.add(((Element)iterator.next()).getChild("t", XlWorkbook.spreadsheetml).getText());
    }
  }
  
  /**
   * Returns the text of an Excel Xlsx worksheet cell
   * @param cellElement named "c"
   * @return text content of the cell (may be an empty string)
   */
  String getCellString(Element cellElement)
  {
    try
    { if ("c".equals(cellElement.getName()))  // is a cell element
      {
    		//System.out.println(cellElement.getAttributeValue("t"));
    		String childText = cellElement.getChild("v", XlWorkbook.spreadsheetml).getText();
    		//System.out.println(childText);
        if ("s".equals(cellElement.getAttributeValue("t")))   // is string type
        {
          return get(Integer.parseInt(childText));
        }
        else	// assume is number
        {
        	return childText;
        }
      }
    }
    catch (Exception x) { }
    return "";
  }
  
  /**
   * @param index into the shared strings table
   * @return the string, or an empty string if out of range
   */
  String get(int index)
  {
    if (index > -1 && index < strings.size())
      return (String)strings.get(index);
    else
      return "";
  }
}
