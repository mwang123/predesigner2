/*
 * XlWorksheet.java Created on Nov 27, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.office;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Element;

/**
 * @author leeb
 * Nov 27, 2007
 * XlWorksheet
 */
public class XlWorksheet
{
  private List rowData;
  
  /**
   * Construct from a JDOM XML element
   * @param worksheetElement - named "worksheet"
   * @param strings - the string table for this worksheet
   */
  XlWorksheet(Element worksheetElement, XlSharedStrings strings)
  {
    List rowList = worksheetElement.getChild("sheetData", XlWorkbook.spreadsheetml).getChildren("row", XlWorkbook.spreadsheetml);
    
    rowData = new ArrayList(rowList.size());
    
    Iterator rows = rowList.iterator();
    while (rows.hasNext())
    {
      List cells = ((Element)rows.next()).getChildren("c", XlWorkbook.spreadsheetml);
      List data = new ArrayList(cells.size());
      Iterator iterator = cells.iterator();
      while (iterator.hasNext())
      {
        data.add(strings.getCellString((Element)iterator.next()));
      }
      rowData.add(data);
    }
  }
  
  public List getColumn(int index)
  {
    List column = new ArrayList(rowData.size());
    
    Iterator rows = rowData.iterator();
    while (rows.hasNext())
    {
      column.add(((List)rows.next()).get(index));
    }

    return column;
  }
  
  /**
   * @param header - string to match in 0th (top) cell of column
   * @return the matching column, or null
   */
  public List getColumn(String header)
  {
    List headers = (List)rowData.get(0);    
    for (int columnIndex = 0; columnIndex < headers.size(); columnIndex++)
    {
      if (header.equals(headers.get(columnIndex)))
      {
        return getColumn(columnIndex);
      }
    } 
    return null;
  }
  
  public List getRow(int index)
  {
    return (List)rowData.get(index);
  }
 
  /**
   * 
   * @param header - string to match in 0th (leftmost) cell of the row
   * @return the matching row, or null
   */
  public List getRow(String header)
  {
    Iterator rows = rowData.iterator();
    while (rows.hasNext())
    {
      List row = (List)rows.next();
      if (header.equals(row.get(0)))
      {
        return row;
      }
    }
    return null;
  }
  
  /**
   * 
   * @return an Iterator of List objects, one for each row of the sheet
   */
  public Iterator getRowIterator()
  {
    return rowData.iterator();
  }
}
