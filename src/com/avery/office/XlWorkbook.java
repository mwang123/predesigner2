/*
 * XlWorkbook.java Created on Nov 27, 2007 by leeb
 * Copyright 2007 Avery Dennison Corp., All Rights reserved.
 */
package com.avery.office;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

/**
 * @author leeb
 * Nov 27, 2007
 * XlWorkbook
 */
public class XlWorkbook
{
  private Hashtable worksheets = null;
  private ArrayList sheetnames = null;
  public File file = null;
  
  static final Namespace spreadsheetml = Namespace.getNamespace("http://schemas.openxmlformats.org/spreadsheetml/2006/main");

  /**
   * constructor
   * @param file - a zip package containing an Excel workbook 
   */
  public XlWorkbook(File file)
  throws IOException, JDOMException, ZipException
  {
    readWorkbook(new ZipFile(file, ZipFile.OPEN_READ));
    this.file = file;
  }
  
  public List getSheetNames()
  {
    return sheetnames;
  }
  
  public XlWorksheet getWorksheet(String name)
  {
    return (XlWorksheet)worksheets.get(name);
  }
  
  /**
   * The workbook is normally stored in the zipFile as "/xl/workbook.xml", but there's no guarantee.
   * To be sure, we open [Content_Types].xml and find the file assignment that matches the spreadsheetml type.
   * @param zipFile containing all of the parts
   * @throws IOException
   * @throws JDOMException
   */
  private void readWorkbook(ZipFile zipFile)
  throws IOException, JDOMException
  {
    final String workbookType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml";
    final String sharedStringsType ="application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml";
    final Namespace rSpace = Namespace.getNamespace("http://schemas.openxmlformats.org/officeDocument/2006/relationships");

    String workbookFilename      = "/xl/workbook.xml";       // default= the name that was normally used in Excel 2007
    String sharedStringsFilename = "/xl/sharedStrings.xml";

    // The workbook is normally stored in the zipFile as "/xl/workbook.xml", but there's no guarantee.
    // To be sure, we open [Content_Types].xml and find the file assignment that matches the spreadsheet type.    
    Element types = getRootElement(zipFile, "[Content_Types].xml");
    
    Iterator overrides = types.getChildren("Override").iterator();
    while (overrides.hasNext())
    {
      Element override = (Element)overrides.next();
      String contentType = override.getAttributeValue("ContentType");
      if (workbookType.equals(contentType))
      {
        workbookFilename = override.getAttributeValue("PartName");
      }
      else if (sharedStringsType.equals(contentType))
      {
        sharedStringsFilename = override.getAttributeValue("PartName");
      }
    }
    
    // read the shared strings table from the Zipfile
    XlSharedStrings sharedStrings = new XlSharedStrings(getRootElement(zipFile, sharedStringsFilename));
    
    // wookbook contains sheet names and relationship Ids
    Element workbookElement = getRootElement(zipFile, workbookFilename);
    // rels file resolves sheet relationship Ids into filenames
    Element relsElement = getRootElement(zipFile, relsName(workbookFilename));
        
    List sheetChildren = workbookElement.getChild("sheets", spreadsheetml).getChildren("sheet", spreadsheetml);
    sheetnames = new ArrayList(sheetChildren.size());
    worksheets = new Hashtable(sheetChildren.size());
    
    Iterator sheets = sheetChildren.iterator();
    while (sheets.hasNext())
    {
      Element sheet = (Element)sheets.next();
      String rId = sheet.getAttributeValue("id", rSpace);
      if (rId != null)
      {
        String worksheetFilename = this.findRelationshipTarget(relsElement, rId);
        // construct XlWorksheet
        XlWorksheet worksheet = new XlWorksheet(getRootElement(zipFile, worksheetFilename), sharedStrings);
        // add to hashtable with name as key
        String sheetname = sheet.getAttributeValue("name");
        worksheets.put(sheetname, worksheet);
        sheetnames.add(sheetname);
      }
      else System.err.println(workbookFilename + ": No r:id for sheet \"" + sheet.getAttributeValue("name") + "\"");
    }
  }
   
  private Element getRootElement(ZipFile zipFile, String filename)
  throws IOException, JDOMException
  {
    ZipEntry entry = getZipEntry(zipFile, filename);
    return new SAXBuilder().build(zipFile.getInputStream(entry)).getRootElement();
  }
  
  private ZipEntry getZipEntry(ZipFile zipFile, String filename)
  {
    ZipEntry entry = zipFile.getEntry(filename);
    if (entry == null)
    {
      // this is a bad hack to get around pathing issues
      String shortname = filename.substring(filename.lastIndexOf('/'));
      Enumeration entries = zipFile.entries();
      while (entries.hasMoreElements())
      {
        ZipEntry next = (ZipEntry)entries.nextElement();
        // System.out.println(next.toString());
        if (next.getName().endsWith(shortname))
        {
          return next;
        }
      }
    }
    return entry;
  }
  
  private String relsName(String name)
  {
    File file = new File(name);
    String rels = file.getParent() + "/_rels/";
    rels += file.getName();
    rels += ".rels";
    return rels;
  }
  
  private String findRelationshipTarget(Element relationships, String rId)
  {
    List children = relationships.getChildren();
    Iterator iterator = children.iterator();
    while (iterator.hasNext())
    {
      Element relationship = (Element)iterator.next();
      String id = relationship.getAttributeValue("Id");
      if (rId.equals(id))
      {
        return relationship.getAttributeValue("Target");
      }
    }
    return null;
  }
}
